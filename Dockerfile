FROM alpine
RUN apk update && apk upgrade && apk add bash git zip apache2 && \
    apk add php7-common php7-session php7-iconv php7-json php7-gd php7-curl php7-simplexml php7-xml php7-mysqli \
    php7-imap php7-cgi fcgi php7-pdo php7-pdo_mysql php7-soap php7-xmlrpc php7-posix php7-mcrypt php7-gettext \
    php7-ldap php7-ctype php7-dom php7-apache2 php7 php7-phar php7-json php7-iconv php7-openssl
WORKDIR /var/www/localhost/htdocs
COPY gem /var/www/localhost/htdocs
RUN chown -R apache:apache /var/www/localhost/htdocs/
RUN chmod 755 /var/www/localhost/htdocs/prepare.sh
EXPOSE 80
ENTRYPOINT ["/bin/sh","./prepare.sh"]
CMD ["resources/application.stage.properties"]