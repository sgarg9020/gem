#!/bin/bash

# Check if arguments are supplied
if [ $# -ne 1 ]
then
echo -e "ERROR: No arguments supplied\nScript usage: build.sh <build-number>\n"; 
exit 1
fi

# Remove files created during previous build
rm -fr gem.zip
rm -rf enterprise*

# Initialize variables
export cdnpath=/gemassets
export GIT_COMMIT=$(git rev-parse HEAD)
export BUILD_NUMBER=$1

# Create versiondetails.json file
echo "{\"buildno\":\"$BUILD_NUMBER\",\"date\":\"$(date +"%Y-%m-%d_%H-%M-%S")\",\"static_revision\":\"$GIT_COMMIT\"}" > versiondetails.json

# Install dependencies
npm install
# Run grunt tasks
grunt --cdnPath=$cdnpath --buildNumber=$BUILD_NUMBER

# Copy dist contents to enterprise-cdn* folder
cp -rf dist enterprise-cdn$BUILD_NUMBER/
# Copy .ebextensions to enterprise-cdn* folder
cp -rf .ebextensions enterprise-cdn$BUILD_NUMBER/
# Remove bulky node_modules from enterprise-cdn* folder
rm -fr enterprise-cdn$BUILD_NUMBER/node_modules
# Copy created versiondetails.json to enterprise-cdn* folder
cp versiondetails.json enterprise-cdn$BUILD_NUMBER/

# Make prepare.sh executable in enterprise-cdn* folder
chmod 0744 enterprise-cdn$BUILD_NUMBER/prepare.sh
# Go into enterprise-cdn* folder
cd enterprise-cdn$BUILD_NUMBER
# Zip all the contents of the enterprise-cdn* folder
zip -r ../gem.zip . 
# Go one folder up
cd ../
# Remove created enterprise-cdn* folder
rm -fr enterprise-cdn$BUILD_NUMBER
# gem.zip is the final artifact

