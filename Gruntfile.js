"use strict";

module.exports = function(grunt) {

    require('time-grunt')(grunt);

    grunt.file.defaultEncoding = 'utf8';

    var cdnURL = grunt.option('cdnPath');
    var buildNum  = grunt.option('buildNumber');

    if((cdnURL == undefined) || (buildNum == undefined)){
        grunt.fail.fatal('Invalid arguments!! \n run grunt as grunt --cdnPath=cdnPath --buildNumber=buildNumber');
    }

    cdnURL = cdnURL+'/'+buildNum;

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            build: {
                cwd: '.',
                src: ['**']  ,
                dest: 'dist',
                expand: true
            },
            build_cdnassets: {
                cwd:'dist',
                src:['app/**','!app/data/**'],
                dest:'dist/gemassets/'+buildNum,
                expand: true
            },
            build_apiconfig:{
                files:[
                    { src:['dist/app/assets/config/apiconfig.js'], dest: 'dist/gemassets/'+buildNum+'/app/assets/config/apiconfig-p.js'},
                    { src:['dist/app/assets/config/apiconfig.js'], dest: 'dist/gemassets/'+buildNum+'/app/assets/config/apiconfig-cs.js'}
                ]
            }
        },
        clean: {
            build: {
                src: ['dist','.tmp']
            }
        },
        uglify: {
            options : {
                beautify : {
                    ascii_only : true
                }
            },
            minify_app_individual: {
                files:[{
                    src:['app/assets/**/*.js', '!app/assets/**/apiconfig.js'],
                    dest:'dist/gemassets/'+buildNum,
                    expand:true
                }]
            }
        },
        cdn:{
            options:{
                cdn: cdnURL,
                flatten: true
            },
            build:{
                cwd:'dist',
                src:['index.html'],
                dest:'dist'
            }
        },
        'string-replace':{
            'config-prod':{
                src:['dist/app/assets/config/apiconfig.js'],
                dest: 'dist/gemassets/'+buildNum+'/app/assets/config/apiconfig-p.js',
                options:{
                    replacements:[
                        {
                            pattern:/var[ \t]*LAZY_LOAD_BASE[ \t]*=[ \t]*.*;/,
                            replacement:'var LAZY_LOAD_BASE =\''+cdnURL+'/\';'
                        },
                        {
                            pattern:/var[ \t]*ENTERPRISE_ENVIRONMENT[ \t]*=[ \t]*.*;/,
                            replacement:'var ENTERPRISE_ENVIRONMENT =\'p\';'
                        }
                    ]
                }
            },
            'config-stage':{
                src:['dist/app/assets/config/apiconfig.js'],
                dest: 'dist/gemassets/'+buildNum+'/app/assets/config/apiconfig-cs.js',
                options:{
                    replacements:[
                        {
                            pattern:/var[ \t]*LAZY_LOAD_BASE[ \t]*=[ \t]*.*;/,
                            replacement:'var LAZY_LOAD_BASE =\''+cdnURL+'/\';'
                        },
                        {
                            pattern:/var[ \t]*ENTERPRISE_ENVIRONMENT[ \t]*=[ \t]*.*;/,
                            replacement:'var ENTERPRISE_ENVIRONMENT =\'cs\';'
                        }
                    ]
                }
            }

        },
        useminPrepare:{
            html:'dist/index.html',
            options:{
                dest:'dist/gemassets/'+buildNum,
                flow:{
                    steps:{
                        js:['concat','uglify'],
                        css:['concat','cssmin']
                    },
                    post:{}
                }
            }
        },
        usemin:{
            html:['dist/index.html']
        }

    });


    // load the tasks
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-cdn');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-usemin');

    // define the tasks
    grunt.registerTask('default', ['clean','copy','useminPrepare','concat','uglify','cssmin','usemin','string-replace','cdn']);

};