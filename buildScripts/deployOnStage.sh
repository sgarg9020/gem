echo "Hold on.. Do not deploy till the demo is over";
exit 1;

if [[ $# < 1 ]]; then
echo " Require Following arguments "
echo " -i<Path to PEM file>";
exit 1;
fi

pem=""

while getopts ":i:" option; do
case $option in
i)
pem=$OPTARG ;;
\?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
 :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
esac
done;

rm -fr ent
svn export ../ ent --force
tar -zcf ent.tgz ent
echo 'sudo mv ent.tgz /var/www/gigsky/' > b.sh
echo 'cd /var/www/gigsky/' >> b.sh
echo 'sudo rm -fr ent' >> b.sh
echo 'sudo tar -zxf ent.tgz' >> b.sh
echo 'sudo rm -fr gem/ent' >> b.sh
echo 'sudo mv ent gem/' >> b.sh
echo 'sudo rm -fr ent.tgz' >> b.sh
echo 'cd gem/' >> b.sh
echo 'sudo sed -i.bkup "s/var[ \t]*ENTERPRISE_ENVIRONMENT[ \t]*=[ \t]*.*;/var ENTERPRISE_ENVIRONMENT = \"s\";/g" ent/app/assets/scripts/config.js' >>b.sh

chmod 0777 b.sh

echo 'rm -fr ~/ent' > c.sh
echo 'echo "" | sudo -S /etc/init.d/bratislava stop' >> c.sh
echo 'cd ~' >> c.sh
echo 'tar -zxf ent.tgz' >> c.sh
echo 'cp -rf ent/test/enterpriseapi/* ~/enterpriseserver/enterpriseapi/' >> c.sh
echo 'cd ~/enterpriseserver/enterpriseapi/demoData/'  >> c.sh
echo './initializedb.sh' >> c.sh
echo 'echo "" | sudo -S /etc/init.d/bratislava start' >> c.sh
echo 'rm -fr ~/ent' >> c.sh
echo 'rm -fr ~/ent.tgz' >> c.sh

chmod 0777 c.sh

scp -i $pem ent.tgz ubuntu@54.148.187.230:/home/ubuntu/
scp -i $pem b.sh ubuntu@54.148.187.230:/home/ubuntu/
scp -i $pem c.sh ubuntu@54.148.187.230:/home/ubuntu/

ssh -i $pem ubuntu@54.148.187.230 'scp -i GS-Stage-US-VPC.pem ent.tgz ubuntu@10.0.1.162:/home/ubuntu/'
ssh -i $pem ubuntu@54.148.187.230 'scp -i GS-Stage-US-VPC.pem ent.tgz ubuntu@10.0.0.97:/home/ubuntu/'
ssh -i $pem ubuntu@54.148.187.230 'scp -i GS-Stage-US-VPC.pem b.sh ubuntu@10.0.1.162:/home/ubuntu/'
ssh -i $pem ubuntu@54.148.187.230 'scp -i GS-Stage-US-VPC.pem c.sh ubuntu@10.0.0.97:/home/ubuntu/'
ssh -i $pem ubuntu@54.148.187.230 'ssh -i GS-Stage-US-VPC.pem ubuntu@10.0.1.162 ~/./b.sh'
ssh -i $pem ubuntu@54.148.187.230 'ssh -i GS-Stage-US-VPC.pem ubuntu@10.0.0.97 ~/./c.sh'
wget https://staging.gigsky.com/api/v1/sims/assign
rm -fr assign
rm -fr ent
rm -fr b.sh
rm -fr c.sh