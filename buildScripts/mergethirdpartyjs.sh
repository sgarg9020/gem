file=$0
cd=`pwd`
cdnhost=$2
appversion=$3
dstfile=$4
if [[ $# != 4 ]] ; then
echo "Invalid arguments"
echo "Give cdn host name and version number of app js as input argument"
echo "mergethirdparty.sh <thirdparty js> conffile <cdnhost> <versionno> <destinationfileprefix>"
exit 1
fi


function removeThirdPartyScriptTag
{
     escfile=$1
     htmllist=`find . -name \*.html -maxdepth 1`
      for resource in $htmllist
      do
        sed -E -i .bk "/.*<script.*$escfile.*/d" $resource
        rm $resource.bk
      done

}
echo >> $1

while read file;
do
if [[ -f "$file" ]];then
    escfile=${file//\//\\\/}    #Add escape chars to path seperators.
    echo "FILE IS "$file
    if [[ $file =~ .*\.min\.$fileType ]];then
        echo "File $file Is already minified."
    else
        java -jar ./buildScripts/yuicompressor-2.4.8.jar --line-break 0 $file -o '.js:.js'
    fi
    cat $file >> "$dstfile.js"
    #sed -E -i .bk "/.*<script.*$escfile.*/d" index.html
    removeThirdPartyScriptTag $escfile
else
if [[ "$file" != "" ]];then
echo "Third party asset: $file is missing in repository"
#exit 1
fi
fi
done < $1
[ -d "./cdnassets/scripts" ] || mkdir -p ./cdnassets/scripts
mv $dstfile.js ./cdnassets/scripts/$dstfile.$appversion.js
gzip -9k ./cdnassets/scripts/$dstfile.$appversion.js
vfile=scripts/$dstfile.$appversion.js
cdnvfile="//$cdnhost/$vfile" #Absolute file path including version in CDN Assets folder
esccdnvfile=${cdnvfile//\//\\\/} #Add escape chars

function updateHTMLScriptTags
{
  echo "File is JS file"
  htmllist=`find . -name \*.html -maxdepth 1`
  for resource in $htmllist
  do
               sed -i .bk "s/<\/body>/<script src=\"$esccdnvfile\"><\/script><\/body>/g" $resource

    rm $resource.bk
  done
}


updateHTMLScriptTags

