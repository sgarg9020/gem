file=$0
cd=`pwd`
cdnhost=$2
appversion=$3
dstfile=$4
if [[ $# != 4 ]] ; then
echo "Invalid arguments"
echo "Give cdn host name and version number of app js as input argument"
echo "mergethirdparty.sh <thirdparty js> conffile <cdnhost> <versionno> <destinationfileprefix>"
exit 1
fi

cdnhostwihtbasepath="//"$cdnhost"/";
esccdnpath=${cdnhostwihtbasepath//\//\\\/}

sed -i.bkup 's/var[ \t]*LAZY_LOAD_BASE[ \t]*=[ \t]*.*;/var LAZY_LOAD_BASE = "'"$esccdnpath"'";/g' app/assets/config/apiconfig.js

while read file;
do
if [[ -f "$file" ]];then
    escfile=${file//\//\\\/}    #Add escape chars to path seperators.
    echo "FILE IS "$file
    if [[ $file =~ .*\.min\.$fileType ]];then
        echo "File $file Is already minified."
    else
        java -jar ./buildScripts/yuicompressor-2.4.8.jar --line-break 0 $file -o '.js:.js'
    fi
else
if [[ "$file" != "" ]];then
echo "Third party asset: $file is missing in repository"
#exit 1
fi
fi
done < $1
#test -d "./cdnassets/app/assets" || mkdir -p "./cdnassets/app/assets"
cp -rf app ./cdnassets/