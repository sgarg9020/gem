#!/bin/bash

command=""

# Prepare sed replace command for the specified key value pair
function prepare()
{
  local KEY=$1
  local VAL=$2

  if [ ! -z "$VAL" ] ; then
  find="\"$KEY\":\([^\,|\}]*\)"
  replace="\"$KEY\":\"$VAL\""
    if [ ! -z "$command" ] ; then
      command="${command} ; s~$find~$replace~"
    else
      command="s~$find~$replace~"
    fi
  fi
}

# Check if argument is supplied
if [ $# -ne 1 ] ; then
  echo -e "ERROR: No arguments supplied\nScript usage: prepare.sh <properties-file>\n"; 
  exit 1;
fi

# Read file from path
. $1

prepare "baseApiURL" $baseApiURL
prepare "umbsApiURL" $umbsApiURL
prepare "pmbsApiURL" $pmbsApiURL
prepare "embsApiURL" $embsApiURL
prepare "absApiURL" $absApiURL

export BUILD_NUMBER=`cat versiondetails.json | sed -n 's/.*buildno":"\([0-9]*\)\".*$/\1/p'`
sed -i.backup "$command" gemassets/$BUILD_NUMBER/app/assets/config/apiconfig.js
/usr/sbin/httpd -D FOREGROUND