'use strict';

/* Directive */
(function(){
	angular.module('app')
		.directive('gsDashboardGraph', GsDashboardGraph);

	GsDashboardGraph.$inject = [ 'debounce', 'loadingState'];
	
	/**
		@function gsDashboardGraph()
		@param {object} debounce - Factory Method
	 	@param {object} loadingState - Factory Method
	*/	
	function GsDashboardGraph( debounce, loadingState) {

		var link = {
			link: linkFn
		};
		return link;

		/**
			@function link()
			@param {object} $scope - directive scope
			@param {object} element - directive HTML element
			@param {object} attrs - attributes on the directive HTML element
			
		*/
		function linkFn($scope, element, attrs) {
			
			var resizeDebounce;
			var isCallbackDuringInit = true;
			
			/**
				@function addCallBack()
				@desc - addCallBack is responsible for adding a call back function
				to each charts plugin options.
			**/
			function addCallBack(){
				// this function can be invoked twice if options and data update
				$scope.$parent.graphObj.options.chart.callback = function(){
					//toggleLoadingState(false);
					if(isCallbackDuringInit){
						loadingState.hide();
						isCallbackDuringInit = false;}
				}
			}
			
			if(typeof $scope.$parent.graphObj.updateGraphOptions == "function"){ // Set debounce with function from parent and add as a a resize event callback
				$scope.$parent.graphObj.updateGraphOptions();
				addCallBack();
				$scope.graphObj.requestData(null,$scope.$parent.dashboard.includeSubAccountDU);

				resizeDebounce = debounce.set($scope.$parent.graphObj.updateGraphOptions,25);
				window.addEventListener('resize', resizeDebounce);
				$("button.stick-menu").click(function(event){
					resizeDebounce();
				});
			}
			
		}
	}
})();
