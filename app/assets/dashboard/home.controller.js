'use strict';

/* Controllers */
(function(){
	angular.module('app')
	    .controller('HomeCtrl', Home);

	Home.$inject = ['$rootScope','$scope','SIMService','UserService','requestGraphData','commonUtilityService','AccountService','dataUsageService','SettingsService','AuthenticationService'];

    /**
	 * @func home()
 	 * @param $rootScope
	 * @param $scope
	 * @param SIMService
	 * @param UserService
	 * @param requestGraphData
	 * @param commonUtilityService
	 * @param AccountService
	 * @param dataUsageService
	 * @param SettingsService
     * @constructor
     */
	function Home($rootScope,$scope,SIMService,UserService,requestGraphData,commonUtilityService,AccountService,dataUsageService,SettingsService,authenticationService){
		
		var dashboard = this;
		var getMonthlyUsageListener;

		/**
		 * Start up logic for controller
		 */
		function activate(){
			dashboard.pagename = "dashboard";
			dashboard.formatDataCost =formatDataCost;
			dashboard.formatDataSize =formatDataSize;
			dashboard.getEnterpriseTimezoneINMilliSec =getEnterpriseTimezoneINMilliSec;
			dashboard.getCurrentTimezoneDate =getCurrentTimezoneDate;

            setIncludeSubAccounts();
            dashboard.simList = [];
            dashboard.userList = [];

			var accountSubscriptionType = sessionStorage.getItem("accountSubscriptionType");

			if(accountSubscriptionType == 'ENTERPRISE_LITE'){
				$scope.isGemLite = true;
			}

			dashboard.getUsageInMB = commonUtilityService.getUsageInMB();
			dashboard.getAccountName = commonUtilityService.getAccountName();

			AccountService.getBillingCycle().success(function (data) {
				$scope.billingStart = data.billingStart;
				$scope.billingEnd = data.billingEnd;
                getUserAndSIMData($scope.billingStart, $scope.billingEnd, dashboard.includeSubAccountDU);
                getTodaysDataUsage(dashboard.includeSubAccountDU);

			}).error(function (data) {
				commonUtilityService.showErrorNotification(data.errorStr);
			});
		}

		function getTodaysDataUsage(consolidated) {
            var today = getEnterpriseTimezoneDate(new Date());
            var currentDateStart = today.getFullYear() + '-' + ("0" + (today.getMonth() + 1)).slice(-2) + '-' + ("0" + today.getDate()).slice(-2) + ' 00:00:00';
            var currentDateEnd = today.getFullYear() + '-' + ("0" + (today.getMonth() + 1)).slice(-2) + '-' + ("0" + today.getDate()).slice(-2) + ' 23:59:59';

            //to get today's data usage
            requestGraphData.getMonthlyDataUsageOfZones(currentDateStart, currentDateEnd,'DAY',consolidated).success(function (data) {
                var todaysUsage = data.totalDataUsedInBytes;
                var todaysSpend = data.totalDataUsageCost;
                dashboard.todaysUsage = formatDataSize(todaysUsage);
                dashboard.todaysSpend = isNaN(todaysSpend) ? 0 : todaysSpend;
            }).error(function (data) {
                commonUtilityService.showErrorNotification(data.errorStr)
            });
        }

		function getUserAndSIMData(startDay, endDay, consolidated) {

            requestGraphData.getMonthlyHighestDataUsageBySIM(startDay, endDay, null, consolidated).success(function (response) {
                dashboard.simList = response.list;
            }).error(function(data,status){
                commonUtilityService.showErrorNotification(data.errorStr);
            });

            requestGraphData.getMonthlyHighestDataUsageByUser(startDay, endDay, null, consolidated).success(function (response) {
                dashboard.userList = response.list;
            }).error(function(data,status){
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

		dashboard.usageFilter = function (item) {
			return item.totalDataUsedInBytes > 0;
		};


		//reusing the data,fetched from the service in zones-donut.js
		getMonthlyUsageListener = $rootScope.$on('onGetMonthlyDataUsagesOfZones', function(event,response, consolidated) {
			dashboard.currency = sessionStorage.getItem('gs-enterpriseCurrency');
			dashboard.enterpriseCurrency = commonUtilityService.getLocalCurrencySign(dashboard.currency);
			dashboard.zonesDataList = dataUsageService.prepareZoneUsageForEstimations(response.data);

			var totalCurrentUsage = response.data.totalDataUsedInBytes;
			dashboard.totalCurrentUsage = isNaN(totalCurrentUsage) ? 0 : totalCurrentUsage;

			requestGraphData.getEstimatedDataCost(consolidated)
				.success(function (estimatedInfo) {
					var estimatedUsage = estimatedInfo.totalDataUsedInBytes;
					dashboard.totalEstimatedUsage = isNaN(estimatedUsage) ? 0 : estimatedUsage;

					dashboard.zonesEstimatedDataList = dataUsageService.prepareEstimateZoneUsageData(dashboard.zonesDataList,estimatedInfo.list);
					getEstimatedDataUsages(response.startDate,response.endDate,dashboard.zonesEstimatedDataList);
				})
				.error(function (data) {
					dashboard.totalEstimatedUsage = 0;
					getEstimatedDataUsages(response.startDate,response.endDate,dashboard.zonesDataList);
					console.log(data.errorStr);
				})

		});

		function getEstimatedDataUsages(startDate, endDate, zones) {
			//Get 1 day in milliseconds
			var one_day = 86400000;
			dashboard.totalCurrentSpend =0;
			dashboard.totalEstimatedSpend =0;

			var startDate_ms = getCurrentTimezoneDate(startDate,'timezone-specific').getTime();
			var endDate_ms = getCurrentTimezoneDate(endDate,'timezone-specific').getTime();

			var difference_ms = endDate_ms - startDate_ms;

			var totalNoOfDays = Math.round(difference_ms / one_day);
			var noOfDaysTillToday = (new Date().getTime() - startDate_ms) / one_day;
			noOfDaysTillToday = noOfDaysTillToday > 0 ? noOfDaysTillToday : 1;
			var remainingNoOfDays = totalNoOfDays - noOfDaysTillToday;

			for (var i = 0; i < zones.length; i++) {
				var zone = zones[i];

				var spend = isNaN(zone.spend)?0:zone.spend;
				var minimumCommitment = isNaN(zone.minimumCommitment)?0:zone.minimumCommitment;
				var estimatedSpend = isNaN(zone.estimatedCost)?0:zone.estimatedCost;

				var zoneSpend = Math.max(spend,minimumCommitment);
				var zoneEstimatedSpend = Math.max(estimatedSpend,minimumCommitment);

				dashboard.totalCurrentSpend += zoneSpend;
				dashboard.totalEstimatedSpend += zoneEstimatedSpend;

				zone.dataUsage = (zone.dataUsage / (1024 * 1024)).toFixed(2);
				zone.estimatedDataUsage = (zone.estimatedDataUsage / (1024 * 1024)).toFixed(2);
			}

			dashboard.zonesList = zones;
			dashboard.currentBillingMonth = getDateObj(startDate).getMonthName();

			var dA = dashboard.totalEstimatedUsage / totalNoOfDays;
			dashboard.dailyAverage = isNaN(dA) ? 0 : dA;

			var dailyAverageSpend = dashboard.totalEstimatedSpend / totalNoOfDays;
			dashboard.dailyAverageSpend = isNaN(dailyAverageSpend) ? 0: dailyAverageSpend;

			dashboard.dailyAverage = formatDataSize(dashboard.dailyAverage);
			dashboard.totalCurrentUsage = formatDataSize(dashboard.totalCurrentUsage);
			dashboard.totalEstimatedUsage = formatDataSize(dashboard.totalEstimatedUsage);

			dashboard.dailyAverageSpend = formatDataCost(dashboard.dailyAverageSpend);
			dashboard.totalCurrentSpend = formatDataCost(dashboard.totalCurrentSpend);
			dashboard.totalEstimatedSpend = formatDataCost(dashboard.totalEstimatedSpend);
			dashboard.todaysSpend = formatDataCost(dashboard.todaysSpend);
		}

        function getCurrentTimezoneDate(date, format) {

            if (format == 'timezone-specific') {
                date = getDateObj(date+" GMT");
                date.setTime(date.getTime() + (-getEnterpriseTimezoneINMilliSec(sessionStorage.getItem("timezone"))));
                return date;

            } else {
                return getDateObj(date);
            }
        }
        function getEnterpriseTimezoneDate(date){
            date.setTime(date.getTime()+getEnterpriseTimezoneINMilliSec(sessionStorage.getItem("timezone")));
            return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),date.getUTCHours(),date.getUTCMinutes(),date.getUTCSeconds(),date.getUTCMilliseconds());

        }
        function getEnterpriseTimezoneINMilliSec(timezone){
            var timezoneArr = timezone.split(':');
            var sign = parseInt(timezoneArr[0]) ? parseInt(timezoneArr[0]) < 0 ? -1 : 1 : 0;
            return sign*(Math.abs(parseInt(timezoneArr[0]) * 60 * 60 * 1000) + Math.abs(parseInt(timezoneArr[1]) * 60 * 1000))
        }
		function formatDataSize(dataUsedInBytes) {
			var usageUnit = commonUtilityService.getDataUsageUnit(dataUsedInBytes);
			var usageInUnit = (commonUtilityService.getUsageInUnit(dataUsedInBytes, usageUnit)==0)?commonUtilityService.getUsageInUnit(dataUsedInBytes, usageUnit):(commonUtilityService.getUsageInUnit(dataUsedInBytes, usageUnit)).toFixed(2);
			return usageInUnit + ' ' + usageUnit;
		}

		function formatDataCost(cost){
			if(dashboard.currency == 'JPY'){
				cost = cost.toFixed();
			}else if(cost && cost != 0){
				cost = cost.toFixed(2);
			}
			return cost;
		}

		$scope.$on('$destroy', function(){
			//unregistering listener
			if(getMonthlyUsageListener)
				getMonthlyUsageListener();
		});

		if(sessionStorage.getItem('accountId') === sessionStorage.getItem('rootAccountId') && sessionStorage.getItem('isNavigatedThroughAdmin') === 'false'){
			sessionStorage.setItem('accountId',sessionStorage.getItem('viewed-acc-detail'));
			sessionStorage.setItem('isNavigatedThroughAdmin','true');
			$rootScope.isNavigatedThroughAdmin = true;
			var queryParam = {};
			queryParam.details = 'alertVersionSupported|pricingModelVersionSupported';

			/* get enterprise details */
			// need to pass query param once the defect is fixed from backend
			authenticationService.getEnterpriseAccountDetails(sessionStorage.getItem('accountId')/*,queryParam*/).success(function (response) {
				authenticationService.setAlertsFeatureEnabled(response.alertVersionSupported);
				authenticationService.setPricingModelVersion(response.pricingModelVersionSupported);
                $scope.setHierarchicalData(response.accountType == RESELLER_ACCOUNT_TYPE, response.accountType == RESELLER_ACCOUNT_TYPE, response.accountType == SUB_ACCOUNT_TYPE);
                if(response.accountType == RESELLER_ACCOUNT_TYPE){
                    sessionStorage.setItem('rootResellerAccountId', response.accountId);
				}
				activate();
			}).error(function (data) {
				console.log("error while fetching enterprise details");
				companies.errorMessage = data.errorStr;
			});
		}else{
			activate();
		}

		function setIncludeSubAccounts() {
			var includeSubAccountDU = sessionStorage.getItem("includeSubAccountDU");
			dashboard.includeSubAccountDU = includeSubAccountDU ? ( includeSubAccountDU === 'true' ? true : false ) : true;
		}

		function saveIncludeSubAccountsInStorage(){
            sessionStorage.setItem("includeSubAccountDU", dashboard.includeSubAccountDU);
        }

        dashboard.onConsolidatedDataUsageChanged = function() {
            saveIncludeSubAccountsInStorage();
			$rootScope.$broadcast('onConsolidatedDataUsageChanged', dashboard.includeSubAccountDU);
            getUserAndSIMData($scope.billingStart, $scope.billingEnd, dashboard.includeSubAccountDU);
            getTodaysDataUsage(dashboard.includeSubAccountDU);
		}

		$scope.$on('$destroy', function(){
			sessionStorage.removeItem('includeSubAccountDU');
		});

	}
})();
