'use strict';

(function(){
    angular.module('app')
        .controller('SIMListModalCtrl', SIMListModal);

    SIMListModal.$inject=['$scope','$rootScope', '$state','modalDetails', 'analytics','SIMService','SIMConstants','AuthenticationService'];

    /**
     * @param $scope
     * @param $rootScope
     * @param modalDetails
     * @param analytics
     * @constructor
     */
    function SIMListModal($scope,$rootScope, $state, modalDetails, analytics, SIMService,SIMConstants,AuthenticationService){
        var simListModal = this;
        /**
         * Start up logic for controller
         */
        function activate(){
            modalDetails.scope(simListModal, true);
            modalDetails.attributes({
                id: 'simListModal',
                submit: 'Close'
            });

            $scope.$parent.simsModalObj = $scope.$parent.simsModalObj || {};
            $scope.$parent.simsModalObj.showSIMs = showSIMs;
            $scope.simCDRReadAllowed = AuthenticationService.isOperationAllowed(['SIM_CDR_READ']);
        }

        activate();

        function fetchSIMList(userId) {
            SIMService.getSIMsByUserId(userId).success(function(response){
                prepareSIMList(response.list);
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

        function prepareSIMList(simList) {
            simListModal.sims = simList.map(function (sim) {
                sim.displyableDeviceType = SIMConstants.deviceType[sim.deviceType].label;
                return sim;
            })
            $('#simListModal').modal('show');
        }

        function showSIMs(userId, sims) {
            if(sims){
                prepareSIMList(sims);
            }else{
                fetchSIMList(userId);
            }
        }

        $scope.navigateToSimDetails = function (simId) {
            if($rootScope.isSupport && !$rootScope.isNavigatedThroughAdmin){
                $state.go('support.sim.simDetail',{accountId: $scope.enterpriseId, iccId: simId});
            }else{
                $state.go('app.simDetail',{iccId: simId});
            }
        }
    }
})();



