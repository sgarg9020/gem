'use strict';

/* Controllers */
(function(){
	angular.module('app')
	    .controller('AddUserCtrl', AddUser);

	AddUser.$inject=['$scope', '$sce', 'modalDetails', 'notifications','formMessage'];
	 
	/**
		@constructor AddUser()
		@param {object} $scope - Angular.js $scope
		@param {object} $sce - Angular.js strict contextual escaping service
		@param {object} modalDetails - Factory object
		@param {object} GroupsService - Factory object
	*/   
	function AddUser($scope, $sce, modalDetails,notifications,formMessage){
		
		var addUser = this;

		/**
		 * Start up logic for controller
		 */
		function activate(){
			/* Form elements object */
			addUser.user = {};
			$scope.generalObj = {};

			/* ModalDetails factory */
			modalDetails.scope(addUser, true);
			modalDetails.attributes({
				id: 'addOrEditUserModal',
				formName: 'addOrEditUser',
				cancel: 'Cancel',
				submit: 'Add User'
			});
			modalDetails.cancel(remove);

			addUser.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};
			$scope.$parent.userModalObj.updateUserModal = updateModal; /* Attach to parent */
			$scope.$parent.userModalObj.resetUserModal = removeData;
		}

		function hideModal() {
			angular.element('#' + $scope.addUser.modal.id).modal('hide');
		}

		/**
			@function updateModal()
			@param {object} user - optional - user's data to auto populate the form
			@param {object} obj - object to change modal attributes in directive
			@param {function} submit - function to execute on submit 
			 - Allow a directive to pass data for the modal to update
		*/ 
		function updateModal(user, groups,rolesList, obj, submit){
			//clear old form data
			addUser.user = {};
			addUser.groups = groups;

			$scope.rolesList = (rolesList && rolesList.length)?(rolesList).map(function(role){return {name:role.replace( "ENTERPRISE_", ""),dbRef:role}}):rolesList;

			addUser.errorMessage = ($scope.rolesList && $scope.rolesList.length)? '':'Unable to process the request. Please reload the page.';

			addUser.errorMessage = '';
			modalDetails.scope(addUser, false);
			modalDetails.attributes(obj);
			
			//Update the form data
			if(user){
				updateData(user);
				var expando = user.$$hashKey;
				$scope.generalObj.previousData = angular.copy(addUser.user);
				modalDetails.submit(function(addUserForm){
					addUser.errorMessage = '';
					if($scope.isFormValid(addUserForm,addUser.user,true)) {
						submit(addUser.user, expando);
						hideModal();
					}else if($scope.generalObj.isNoChangeError){
						addUser.errorMessage = "There is no change in the data to update.";
						$scope.generalObj.isNoChangeError = false;
					}
				});
			}

			else {
				$scope.generalObj.previousData = null;
				if(!addUser.user.role)
				{
					addUser.user.role = {dbRef:'ENTERPRISE_SIM_USER'};
				}
				addUser.user.role.name = (addUser.user.role.dbRef)?(addUser.user.role.dbRef).replace( "ENTERPRISE_", ""):'SIM_USER';
				modalDetails.submit(function(addUserForm){
					addUser.errorMessage = '';
					if($scope.isFormValid(addUserForm,addUser.user,true)) {
						submit(addUser.user);
						hideModal();
					}

				});
			}

		}

		function removeData(){
			$scope.addUser.user ={};
			formMessage.scope($scope.addUser);
			formMessage.message(false, "");
		}

		/**
		 @function updateData()
		 @param {object} user - users data to add to the form
		 */
		function updateData(user) {
			if (user.userId) {
				$scope.addUser.user.userId = user.userId;
			}

			if (user.firstName) {
				$scope.addUser.user.firstName = user.firstName;
			}

			if (user.lastName) {
				$scope.addUser.user.lastName = user.lastName;
			}

			if (user.emailId) {
				$scope.addUser.user.emailId = user.emailId;
			}
			if (user.accountDetails ) {

				$scope.addUser.user.accountDetails = user.accountDetails;

				var parentAcc = user.accountDetails.enterpriseAccountDetails[0].parentAccountDetails;
				if (parentAcc !== undefined) {
					for (var i = 0; i < parentAcc.length; i++) {
						var accObj = parentAcc[i];
						if (accObj.accountType == "GROUP" && accObj.role == "ENTERPRISE_SIM_USER") {
							if (typeof accObj.accountId != "undefined" && typeof accObj.accountName != "undefined") {
								$scope.addUser.user.group = {
									"type": accObj.type,
									"accountName": accObj.accountName,
									"accountId": accObj.accountId
								};
							}
							else {
								$scope.addUser.user.group = null;
							}
							break;
						}
					}
				}
				else {
					$scope.addUser.user.group = null;
				}


			}
			if (user.accountDetails ) {

				addUser.user.accountDetails = user.accountDetails;

				for( var n = 0; n < user.accountDetails.enterpriseAccountDetails.length; n++ ) {

					var parentAcc = user.accountDetails.enterpriseAccountDetails[n].parentAccountDetails;
					for (var i = 0; parentAcc !== undefined, i < parentAcc.length; i++) {
						var accObj = parentAcc[i];
						if (accObj.accountId == sessionStorage.getItem('accountId') && (accObj.accountType == 'ENTERPRISE' || accObj.accountType == RESELLER_ACCOUNT_TYPE || accObj.accountType == SUB_ACCOUNT_TYPE)) {
							addUser.user.role = {dbRef:accObj.role};
						}
					}

				}

				if(!addUser.user.role)
				{
					addUser.user.role = {dbRef:'ENTERPRISE_SIM_USER'};
				}
				addUser.user.role.name = (addUser.user.role.dbRef)?(addUser.user.role.dbRef).replace( "ENTERPRISE_", ""):'SIM_USER';


			}
			if(user.location){
				$scope.addUser.user.location = user.location;
			}
		}




		/**
			@function remove()
			 - call a directive level function
		*/
		function remove(){
			removeData();
		}

		activate();

	}
})();
