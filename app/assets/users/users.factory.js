'use strict';

(function(){
    angular
        .module('app')
        .factory('UserService',['commonUtilityService',UserService]);

    /**
     @function debounce()
     @param {object} $timeout - angular timeout service
     - Factory method for functions to be executed after a delay
     */
    function UserService(commonUtilityService){

        var factory = {

            addUser: addUser,
            getUsers:getUsers,
            updateUser:updateUser,
            updateUsers:updateUsers,
            deleteUsers:deleteUsers,
            refreshDataUsage:refreshDataUsage,
            getExportDataUrl:getExportDataUrl
        };

        return factory;


        function addUser(user,accountId) {

            var accId = (accountId)?accountId:sessionStorage.getItem('accountId');
            if( typeof user.list[0].group != "undefined" ){
                 accId = user.list[0].group.accountId;
                delete user.list[0].group;
            }
            var headerAdd=(user.roleName)?['CREATE_USER']:['USER_CREATE'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE  + "accounts/account/" + accId  + "/users", 'POST',headerAdd,false,user);
        }

        function getUsers(queryParam,accountId) {
            var enterpriseId = accountId ? accountId : sessionStorage.getItem("accountId");
            var headerAdd=['USER_READ'];
            var config = {
                cancellable : 'searchApi'
            };
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+enterpriseId+"/users", 'GET',headerAdd,queryParam,undefined,config);
        }

        function updateUser(user) {
            user.type = "User";
            var headerAdd=['USER_PROFILE_EDIT'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_API_BASE + 'users/' + user.userId, 'PUT',headerAdd,false,user);
        }

        function updateUsers(users, accountId) {
            //var headerAdd=['USER_UPDATE_ASSOCIATION'];

            var headerAdd = (users.accountRoleAssociationList && users.accountRoleAssociationList.length)? ['EDIT_USER'] : null;
            var accId = (accountId)?accountId:(sessionStorage.getItem('rootAccountId')?sessionStorage.getItem('rootAccountId'):sessionStorage.getItem('accountId'));

            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accId+"/users", 'PUT',headerAdd,false,users);
        }

        function deleteUsers(usersList,accountId){
            var accId = (accountId)?accountId:sessionStorage.getItem('accountId');
            var headerAdd=['USER_DELETE'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accId+"/users?uid="+usersList, 'DELETE',headerAdd);
        }

        function refreshDataUsage(){
            var headerAdd=['REFRESH_DATAUSAGE'];
            var accId = sessionStorage.getItem('accountId');
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE  + "accounts/account/" + accId  + "/users/datausage/refresh", 'POST',headerAdd);
        }

        function getExportDataUrl(accId){
            var accountId = accId ? accId : sessionStorage.getItem('accountId');
            return ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/users?";
        }
    }
})();
