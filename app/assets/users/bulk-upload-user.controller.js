'use strict';

/* Controllers */
(function(){
	angular.module('app')
	    .controller('BulkUploadUserCtrl', BulkUploadUsers);

	BulkUploadUsers.$inject=['$scope','$rootScope', 'modalDetails', 'analytics'];

	/**
	 * @func BulkUploadUsers()
	 * @param $scope
	 * @param $rootScope
	 * @param modalDetails
	 * @param analytics
     * @constructor
     */
	function BulkUploadUsers($scope,$rootScope, modalDetails, analytics){

		var bulkUploadUsers = this;

		/**
		 * Start up logic for controller
		 */
		function activate(){
			Dropzone.autoDiscover = false;

			bulkUploadUsers.refreshDatatable = $scope.$parent.dataTableObj.refreshDataTable;
			$scope.dropzoneConfig = {
				autoProcessQueue: false,
				maxFiles: 1,
				parallelUploads: 1,
				maxFilesize: 25,
				addRemoveLinks: false,
				clickable: true,
				acceptedFiles: ".csv",
				headers:{ "token" : sessionStorage.getItem( "token") },
				url:ENTERPRISE_GIGSKY_BACKEND_BASE+"accounts/account/" + sessionStorage.getItem("accountId") + "/users/upload",   //ENTERPRISE_API_BASE+'users/bulk',
				dictInvalidFileType:'File type not supported'
			};
			/* ModalDetails factory */
			modalDetails.scope(bulkUploadUsers, true);
			modalDetails.attributes({
				id: 'bulkUploadUsersModal',
				formName: 'file',
				cancel: 'Clear',
				submit: 'Upload'
			});
			modalDetails.cancel(reset);
			modalDetails.submit(attemptToUpload);
			bulkUploadUsers.cancelUpload = cancelUpload;
		}

		/**
			@function reset()
			 - call a directive level function
		*/
		function reset(){
			$rootScope.$broadcast('clearDropzoneFiles');
		}

		/**
			@function attemptToUpload()
			 - call a directive level function
		*/
		function attemptToUpload(){
			analytics.sendEvent({
				category: "Operations",
				action: "bulk upload",
				label: "User Management"
			});
			$rootScope.$broadcast('submitDropzoneFiles');
		}

		/**
			  @function attemptToUpload()
			 - call a directive level function
		 */
		function cancelUpload(){
			$rootScope.$broadcast('cancelDropzoneUpload');
		}

		activate();

	}
})();
