'use strict';

/* Controller */
(function(){
	angular.module('app')
	    .controller('UsersCtrl', Users);

	Users.$inject = ['$scope','$rootScope','$compile', 'UserService', 'dataTableConfigService', 'GroupsService', 'commonUtilityService','loadingState','$sanitize', 'analytics','AuthenticationService','AccountService','$q','SettingsService','$location','$sce'];

	/**
	 * @func Users()
 	 * @param $scope
	 * @param $rootScope
	 * @param $compile
	 * @param UserService
	 * @param dataTableConfigService
	 * @param GroupsService
	 * @param commonUtilityService
	 * @param loadingState
     * @param $sanitize
	 * @param analytics
     * @constructor
     */

	function Users($scope,$rootScope, $compile,UserService, dataTableConfigService, GroupsService, commonUtilityService,loadingState,$sanitize, analytics,AuthenticationService,AccountService,$q,SettingsService,$location,$sce){
		
		var users = this;

		/**
		 * Start up logic for controller
		 */
		function activate(){
			clearDataTableStatus(['DataTables_usersDataTable_']);
			$scope.emptyTableMessage = "No Users available.";
			users.addUser = addUserModal;
			users.verifyDeletion = verifyDeletion;
			users.bulkGroupUpdate = bulkGroupUpdate;
			users.bulkGroupRemove = bulkGroupRemove;
			users.refreshDataUsage = refreshDataUsage;
			users.resetPassword = resetPassword;


			$scope.dataTableObj = {};
			$scope.userModalObj = {};
			$scope.groupModalObj = {};
			$scope.graphObj = {};

			$scope.dataTableObj.tableID = "usersDataTable";
			var filtersConfig = { selectedDisplayLength: {type:'local', dbParam: 'count', defaultValue:25} , roleName: {type:'url', defaultValue: { name:'ALL', dbRef:'',skipQueryParam:true} }};
			dataTableConfigService.configureFilters($scope.dataTableObj, filtersConfig);
			users.exportData={
				url: UserService.getExportDataUrl()
			};

			$scope.groupModalObj.updateGroups = updateGroups;
			SettingsService.getEnterpriseDetails(sessionStorage.getItem("accountId")).success(function (response) {
				$scope.accountStatus = response.status;

				AccountService.getRoles(response.accountType,response.accountSubscriptionType).success(function (response) {

					$scope.rolesList = response.list;
					$scope.userRoleFilters = prepareUserRoleFilters(response.list);

				}).error(function (data) {
					commonUtilityService.showErrorNotification(data.errorStr);
					console.log(data.errorStr);
				});

			}).error(function (data) {
				commonUtilityService.showErrorNotification(data.errorStr);
				console.log(data.errorStr);
			});

			users.trustAsHtml = function (value){return $sce.trustAsHtml(value);};

			$scope.dataTableObj.tableHeaders = [
				{"value": "","DbValue":""},
				{"value": "Name","DbValue":"fullName"},
				{"value": "Email","DbValue":"emailId"},
				{"value": "Group","DbValue":"accountName"},
				{"value": "Role","DbValue":'role'},
				{"value": "Location","DbValue":"location"},
				{"value": "SIM(s)","DbValue":"totalSimsAssociated"},
				{"value": "Usage","DbValue":"dataUsage"},
				{"value": "Password","DbValue":""}
			];
			
			$scope.dataTableObj.dataTableOptions = {
				order: [[ 1, "asc" ]],
				createdRow: function (row,data) {
					$(row).attr("id",'UID_'+data.userId);
					$compile(row)($scope);
				},
				columns: [
					{
						width:"60px",
						"orderable":false
					},
					{
						"orderable":true
					},
					{
						"orderable":true
					},
					{
						"orderable":true
					},
					{
						"orderable":false
					},
					{
						"orderable":true
					},
					{
						width:"75px",
						"orderable":true
					},
					{
						width:"75px",
						"orderable":true
					},{
						width:"75px",
						"orderable":true
					}
				],
				columnDefs: [{
					targets: 0,
					className: "checkbox-table-cell",
					render: function ( data, type, row, meta) {
						return '<div gs-has-permission="USER_UPDATE_ASSOCIATION,USER_DELETE" class="checkbox check-success large no-label-text hideInReadOnlyMode"> <input type="checkbox" value="1" id="checkbox_'+row.userId+'" gs-index="'+meta.row+'"><label ng-click="checkBoxClicked($event)" id="label_'+row.userId+'"></label></div>';
					}
				},
					{
						targets: 1,
						render: function (data, type, row, meta) {
							var fName = $sanitize(row.firstName);
							var lName = $sanitize(row.lastName);
							var permissionList = ['USER_PROFILE_EDIT'];
							var isEditingAllowed = AuthenticationService.isOperationAllowed(permissionList);

							return isEditingAllowed ? '<a id="edit_user_'+row.userId+'" class="nowrap" ng-click="editUser('+meta.row+');" href="javascript:void(0)" title="Click to edit user '+fName + ' ' + lName+'"><i class="fa fa-pencil edit-ico"></i>'+ ' ' + fName + ' ' + lName+'</a>' : fName + ' ' + lName;
						}
					},
					{
						targets: 2,
						render: function (data, type, row) {
							var emailId = $sanitize(row.emailId);
							return 	'<a href="mailto:'+emailId+'" title="Send an email to '+emailId+'" class="nowrap">'+emailId+'</a>'
						}
					}, {
						targets: 3,
						render: function( data, type, row){


							var acc = getGroupDetails( row.accountDetails );

							if( acc ) {
								var accountName = $sanitize(acc.accountName);
								return "<span class='nowrap'>"+ ( typeof accountName != "undefined" ? accountName  : 'Default' ) +"</span>";
							}


							else return "<span class='nowrap'>Default</span>";

						},
						defaultContent: 'Default'
					}, {
						targets: 4,
						render: function( data, type, row){
							var enterpriseId = sessionStorage.getItem('accountId');
							var role = getUserRole(row.accountDetails,enterpriseId);

							var userRole = (role)?role.replace("ENTERPRISE_", ""):'SIM_USER';

							return "<span class='nowrap'>{{'"+userRole+"' | underscoreless}}</span>";
						}
					},{
						targets: 5,
						render: function( data, type, row){
							var location = $sanitize(row.location);
							return "<span class='nowrap'>"+location+"</span>";
						},
						defaultContent: ''
					},

					{
						targets: 6,
						render:function( data, type, row, meta ) {
							return (row.totalSimsAssociated ==0)?'0':'<a ng-click=\'showAssociatedSims('+row.userId+');\'><span>' + row.totalSimsAssociated + '</span></a>';
						},
						defaultContent: '0'

					},

					{
						targets: 7,
						render: function (data, type, row) {
							if($rootScope.isGSAdmin || !$rootScope.isGemLite){
								return  '<a href="javascript:void(0)" ng-click=\'setUser(\"'+ row.userId + '\",\"'+escape(row.firstName + '_' +row.lastName)+'\");\' class="usage-value nowrap"><i class="fa fa-bar-chart"></i><span>'+((row.dataUsedInBytes!==undefined)?row.dataUsedInBytes === +row.dataUsedInBytes && row.dataUsedInBytes !== (row.dataUsedInBytes|0) ? commonUtilityService.getUsageInMB()(row.dataUsedInBytes) : commonUtilityService.getUsageInMB()(row.dataUsedInBytes) :0)+'</span></a>';
							}
							else{
								return  ((row.dataUsedInBytes!==undefined)?row.dataUsedInBytes === +row.dataUsedInBytes && row.dataUsedInBytes !== (row.dataUsedInBytes|0) ? commonUtilityService.getUsageInMB()(row.dataUsedInBytes) : commonUtilityService.getUsageInMB()(row.dataUsedInBytes) :0);
							}

						},
						defaultContent: '0'

					}, {
						targets: 8,
						render: function (data, type, row, meta) {
							var enterpriseId = sessionStorage.getItem('accountId');
							var userRole = getUserRole(row.accountDetails,enterpriseId);
							return (userRole == 'ENTERPRISE_SIM_USER')?'<span class="opacity-2">Reset</span>':'<a id="reset_user_pwd_' + row.userId + '" class="nowrap"  ng-click="users.resetPassword(' + meta.row + ')" href="javascript:void(0)">Reset</a>';
						}
					}


				],
				oLanguage: {
					sLengthMenu: "_MENU_ ",
					sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
					sEmptyTable: $scope.emptyTableMessage,
					sZeroRecords:$scope.emptyTableMessage
				},
				processing: true,
				rowCallback: function( row, data ) {
					$scope.dataTableObj.showPreviousSelectedRows(data.userId, row);
				},
				fnServerData: function (sSource, aoData, fnCallback) {
					if(aoData){
						users.searchValue = aoData[5].value.value;
					}
					$scope.isDisable = false;

					var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);

					users.exportData.startIndex = queryParam.startIndex;
					users.exportData.count = queryParam.count;
					users.exportData.sortBy = queryParam.sortBy;
					users.exportData.sortDirection = queryParam.sortDirection;
					users.exportData.search = queryParam.search;
					if(queryParam.roleName){
						users.exportData.roleName = queryParam.roleName;
					}

					$scope.isDisableRefreshBtn = false;
					$scope.isDisable = false;
					UserService.getUsers(queryParam).success(function (response) {
						angular.element('#checkbox_action').prop('checked', false);
						var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
						var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
						$scope.usersList = datTabData.aaData;

						if($scope.usersList.length == 0){
							$scope.isDisable = true;
							$scope.isDisableRefreshBtn = true;
						}
						if(oSettings != null) {
							fnCallback(datTabData);
						}else{
							loadingState.hide();
						}

					}).error(function(data){
						$scope.isDisable = !$scope.usersList || $scope.usersList.length == 0;
						var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
						dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"users");
						console.log('get users failure' + data);
						commonUtilityService.showErrorNotification(data.errorStr);
					});

				},

				fnDrawCallback: function( oSettings ) {

					var $table = $( "#" + $scope.dataTableObj.tableID );

					if($scope.usersList)
						dataTableConfigService.disableTableHeaderOps( $scope.usersList.length, oSettings );

					// this is for hiding the data usage for GEM LITE user
					if( $( ".usage-value").eq(0).css( "display") === "none"){
						$table.DataTable().column(7).visible( false );
					}

					var permissionList = ['CREATE_ENTERPRISE_SUPPORT_ADMIN'];
					var createSupportAdminAllowed = AuthenticationService.isOperationAllowed(permissionList);
					if(!createSupportAdminAllowed){
						$table.DataTable().column(4).visible( false );
						$table.DataTable().column(8).visible( false );
					}

                    $table.DataTable().column(3).visible(!($scope.resellerOrSubAccount));

					commonUtilityService.preventDataTableActivePageNoClick( $scope.dataTableObj.tableID );

				}
			};
			$scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

			// get the groups list and make it available for add user and add to group modals
			/* Get the groups data */
			GroupsService.getGroups({
				count: 100000,
				startIndex: 0,
				sortBy: "accountName",
				sortDirection: "ASC"
			}).success(function (response) {
				users.groups = ( response.list !== undefined ? response.list : [] );
			}).error(function () {
				console.log("error while fetching groups");
			});

		}

		$scope.isDisableComponent = function(){
			return $scope.isDisable;
		};

		$scope.isDisableRefreshComponent = function(){
			return $scope.isDisableRefreshBtn;
		};

		$scope.checkBoxClicked = function(event){
			$scope.dataTableObj.selectRow(event);
		};
		$scope.editUser = function(index){
			editUserModal($scope.usersList[index]);
		};

		$scope.setUser = function(userId,userName){
			$scope.graphObj.setUser(userId,userName);

			analytics.sendEvent({
				category: "Operations",
				action: "View User Data usage",
				label: "User Management"
			});
		};

		$scope.showAssociatedSims = function(userId) {
			$scope.simsModalObj.showSIMs(userId);
		};




		/**
		 @function addUserModal()
		 - Open the addUserModal
		 */
		function addUserModal() {
			/* Attributes for new user */
			var obj = {
				header: "Add New User",
				submit: "Add"
			};
			if(ENABLE_1_4_1){
				$scope.userModalObj.updateUserModal(null, users.groups,$scope.rolesList, obj, $scope.onAddNewUser);
			}else{
				$scope.userModalObj.updateUserModal(null, users.groups,$scope.rolesList, obj, $scope.onAddEditNewUser);
			}

			$('#addOrEditUserModal').modal('show');
		}

		/**
		 @function editUserModal()
		 @param {obj} user - user object to update
		 - Open the addUserModal
		 */
		function editUserModal(user) {
			/* Attributes for existing user */
			var obj = {
				header: "Update User",
				submit: "Update"
			};
			$scope.userModalObj.updateUserModal(user, users.groups,$scope.rolesList, obj, $scope.onEditExistingUser);

			$('#addOrEditUserModal').modal('show');

		}


		function verifyDeletion(rows){

			var msg = "Deleting users cannot be undone! All SIMs associated to the users will be unassigned.";
			var attr = {
				header: 'Confirm Deletion',
				cancel: 'Cancel',
				submit: 'Delete'
			};
			var func = function(){$scope.processTableRowDeletion(rows);};

			$scope.verificationModalObj.updateVerificationModel(attr,func);
			$scope.verificationModalObj.addVerificationModelMessage(msg);

			$('#verificationModal').modal('show');
		}

		function bulkGroupUpdate(rows) {

			$scope.groupModalObj.vfUpdateModal(rows, users.groups, processGroupsUpdate);

			$('#updateGroupsModal').modal('show');

		}

		function getGroupDetails(accDetails) {
			for( var n = 0; n < accDetails.enterpriseAccountDetails.length; n++ ) {

				var parentAcc = accDetails.enterpriseAccountDetails[n].parentAccountDetails;
				if (parentAcc !== undefined) {

					// case: only one parent Account, return that irrespective of the role
					if( parentAcc.length == 1 ) {
						return parentAcc[0];
					}

					for (var i = 0; i < parentAcc.length; i++) {
						var accObj = parentAcc[i];
						if (accObj.role == "ENTERPRISE_SIM_USER") {
							return accObj;
						}
					}
					return accDetails.enterpriseAccountDetails[n].rootAccountDetails;
				}
			}
		}

		function bulkGroupRemove(rows) {

			var msg = "Removing users from a group will not keep any association with the group.";
			var attr = {
				id: 'verificationModal',
				header: 'Remove Group Association',
				cancel: 'Cancel',
				submit: 'Remove'
			};
			var originalLen = rows.length;

			commonUtilityService.filterSelectedRows(rows,function(row){

				var acc= getGroupDetails(row.accountDetails );

				if( acc.accountType == "ENTERPRISE" ) return true;

			});

			if( rows.length == 0 && originalLen == 1 ) {
				commonUtilityService.showErrorNotification("Selected User can't be removed from Enterprise Group!!");
				return;
			}

			if( rows.length ==0 && originalLen > 1 ) {
				commonUtilityService.showErrorNotification("Selected Users can't be removed from Enterprise Group!!");
				return;
			}


			var func = function(){processGroupsUpdate({},rows);};

			$scope.verificationModalObj.updateVerificationModel(attr,func);
			$scope.verificationModalObj.addVerificationModelMessage(msg);

			$('#verificationModal').modal('show');
		}

		/**
		 @function processGroupsUpdate()
		 @param {object} group - Groups object to be passed in
		 @param {object} group.group - group specific object
		 @param {array} [rows] - row of selected users
		 - Update each selected users group to the parameter passed in

		 */
		function processGroupsUpdate(group,rows){

			var originalLen = rows.length;
			var newAccId = null;

			/* Clean up our groups object */
			if(Object.getOwnPropertyNames(group).length == 0){
				group = null;

			} else {
				delete group.group.$$hashKey;
				newAccId = group.group.accountId;
			}

			commonUtilityService.filterSelectedRows(rows,function(row){
				var acc = getGroupDetails( row.accountDetails );
				if( acc ) {
					return(  newAccId  == acc.accountId );
				}
			});

			if (rows.length == 0 && originalLen == 1) {
				commonUtilityService.showErrorNotification("Selected User is already associated with the same Group!!");
				return;
			}
			if( rows.length == 0 && originalLen > 1 ){
				commonUtilityService.showErrorNotification("All Selected Users are already associated with the same Group!!");
				return;
			}




			var userList = { type:"UserList", list:getSelectedUsersListForUpdate(rows, group) };
			var enterpriseId = sessionStorage.getItem('accountId');

			UserService.updateUsers(userList,enterpriseId).success(function () {

				$scope.dataTableObj.refreshDataTable(false);
				if(userList.list.length == 1){
					commonUtilityService.showSuccessNotification("Selected user has been updated.");
				}else{
					commonUtilityService.showSuccessNotification("All selected users have been updated.");
				}
			}).error(function(data){
				console.log('update users failure' + data);
				commonUtilityService.showErrorNotification(data.errorStr);
			});

			if(group !== null){
				analytics.sendEvent({
					category: "Operations",
					action: "Add to group",
					label: "User Management"
				});
			} else {
				analytics.sendEvent({
					category: "Operations",
					action: "Remove from group",
					label: "User Management"
				});
			}

		}

		/**
		 @function processTableRowDeletion()
		 @return {boolean} - truthy value to determine if delete request was successful
		 */
		$scope.processTableRowDeletion = function(rows){

			var len = rows.length;

			var userList = getSelectedUsersListForDeletion(rows);
			UserService.deleteUsers(userList).success(function() {

			    $scope.dataTableObj.refreshDataTable(false,len);
				var action = 'Delete';
				if( len == 1){
					action = action + ' User';
					commonUtilityService.showSuccessNotification("Selected user has been deleted.");
				}else{
					action = action + ' Users';
					commonUtilityService.showSuccessNotification("All selected users have been deleted.");
				}

				analytics.sendEvent({
					category: "Operations",
					action: action,
					label: "User Management"
				});
				return true;
			}).error(function(data){
				commonUtilityService.showErrorNotification(data.errorStr);
			});

		};

		/**
		 @function processTableRowAdd()
		 @param {obj} user - user object
		 @param {obj} user.firstName - first name
		 @param {obj} user.lastName - last name
		 
		 */
		$scope.onAddNewUser = function (user) {

			var userProcessed = angular.copy(user);

			delete userProcessed.roleName;
			delete userProcessed.role;
			userProcessed.type = "User";
			if (user.role.dbRef != 'ENTERPRISE_SIM_USER') {
				var enterpriseId = sessionStorage.getItem('accountId');

				var accountRoleAssociationList = [
					{
						"type": "accountRoleAssociation",
						"accountId": enterpriseId,
						"role": user.role.dbRef
					}
				];
				if (user.group) {
					accountRoleAssociationList.push({
						"type": "accountRoleAssociation",
						"accountId":user.group.accountId,
						"role": "ENTERPRISE_SIM_USER"
					});
				}
				userProcessed.accountRoleAssociationList = accountRoleAssociationList;
			}

			var list = [userProcessed];

			var userList = {type: "UserList", list: list};

			UserService.addUser(userList).success(function (response) {

				$scope.dataTableObj.refreshDataTable(false);
				commonUtilityService.showSuccessNotification("An account has been created for " + user.firstName + " " + " " + user.lastName); //+ " and details have been sent to " + user.emailId + ".");
				$scope.userModalObj.resetUserModal();

				analytics.sendEvent({
					category: "Operations",
					action: "Add User",
					label: "User Management"
				});
			}).error(function (data) {
				console.log('add user failure' + data);
				commonUtilityService.showErrorNotification(data.errorStr);
				$scope.userModalObj.resetUserModal();

			});


		};

		/**
		 @function processTableRowAdd()
		 @param {obj} user - user object
		 @param {obj} user.firstName - first name
		 @param {obj} user.lastName - last name

		 */
		$scope.onAddEditNewUser = function(user) {

			user.type = "User";

			var userProcessed = angular.copy(user);

			delete userProcessed.roleName;
			delete userProcessed.role;
			userProcessed.type = "User";
			var list = [ userProcessed ];


			var userList = { type:"UserList", list:list };

			UserService.addUser(userList).success(function(response){

				if(user.role.dbRef != 'ENTERPRISE_SIM_USER') {
					var enterpriseId = sessionStorage.getItem('accountId');
					var userRoleProcessed = {
						type: "User",
						userId: response.list[0],
						accountRoleAssociationList: [
							{
								"type": "accountRoleAssociation",
								"accountId": enterpriseId,
								"role": user.role.dbRef
							}
						]
					};

					var roleList = [ userRoleProcessed ];

					var userRoleList = {type: "UserList", list: roleList};

					UserService.updateUsers(userRoleList, enterpriseId).success(function () {

						analytics.sendEvent({
							category: "Operations",
							action: "Add User",
							label: "User Management"
						});

						$scope.dataTableObj.refreshDataTable(false);
						commonUtilityService.showSuccessNotification("An account has been created for " + user.firstName + " " + " " + user.lastName); //+ " and details have been sent to " + user.emailId + ".");
						$scope.userModalObj.resetUserModal();

					}).error(function (data) {
						console.log('add user failure' + data);
						commonUtilityService.showErrorNotification(data.errorStr);
						$scope.userModalObj.resetUserModal();

					});
				}else{
					analytics.sendEvent({
						category: "Operations",
						action: "Add User",
						label: "User Management"
					});

					$scope.dataTableObj.refreshDataTable(false);
					commonUtilityService.showSuccessNotification("An account has been created for " + user.firstName + " " + " " + user.lastName); //+ " and details have been sent to " + user.emailId + ".");
					$scope.userModalObj.resetUserModal();
				}

			}).error(function(data){
				console.log('add user failure' + data);
				commonUtilityService.showErrorNotification(data.errorStr);
				$scope.userModalObj.resetUserModal();

			});


		};


		/**
		 @function processTableRowUpdate()
		 @param {obj} user - user object
		 @param {obj} user.userId - user's ID
		 @param {obj} user.firstName - first name
		 @param {obj} user.lastName - last name
		 @param {string} expando - expando $$hashKey value
		 */
		$scope.onEditExistingUser =function(user, expando) {

			/* Add the expando back to the user object */
			user.$hashKey = expando;
			var userList = { type:"UserList", list: [getUserForUpdate( user ) ] };
			var enterpriseId = sessionStorage.getItem('accountId');

			UserService.updateUsers(userList,enterpriseId).success(function () {

				analytics.sendEvent({
					category: "Operations",
					action: "Update User",
					label: "User Management"
				});

				$scope.dataTableObj.refreshDataTable(false);
				if(user.userId == sessionStorage.getItem('userId')){
					var data = {firstName: user.firstName,lastName : user.lastName};
					$rootScope.$emit('onUpdateAccountInfo',data);
				}

				commonUtilityService.showSuccessNotification("User " + user.firstName + " " + user.lastName + " details have been updated successfully.");
				$scope.userModalObj.resetUserModal();

			}).error(function(data){
				console.log('update user failure' + data);
				commonUtilityService.showErrorNotification(data.errorStr);
				$scope.userModalObj.resetUserModal();

			});

		};

		function getUserForUpdate( user ) {

			var ref= {};
			ref.type =  ( user.type !== undefined  ? user.type : "User" );
			ref.userId = user.userId;
			ref.emailId = user.emailId;
			ref.firstName = user.firstName;
			ref.lastName = user.lastName;
			ref.location = user.location;
			var currAccId;

			if( user.group ){

				// group association
				var acc = getGroupDetails( user.accountDetails );
				if( acc ){

					currAccId = acc.accountId;
				}


				if( currAccId != user.group.accountId ) {
					ref.fromAccount = currAccId;
					ref.toAccount = user.group.accountId;
				}
			}
			if( user.role ) {
				var enterpriseRole = getEnterpriseRole(user.accountDetails);
				if(user.role.dbRef != enterpriseRole) {
					ref.accountRoleAssociationList = [
						{
							"type": "accountRoleAssociation",
							"accountId": sessionStorage.getItem('accountId'),
							"role": user.role.dbRef
						}
					]
				}

			}

		return ref;


		}

		function getEnterpriseRole(accDetails){
			for( var n = 0; n < accDetails.enterpriseAccountDetails.length; n++ ) {

				var parentAcc = accDetails.enterpriseAccountDetails[n].parentAccountDetails;
				if (parentAcc !== undefined) {

					for (var i = 0; i < parentAcc.length; i++) {
						var accObj = parentAcc[i];
						if (accObj.accountId == sessionStorage.getItem('accountId')) {
							return accObj.role;
						}
					}
					return 'ENTERPRISE_SIM_USER';
				}
			}

		}

		function getSelectedUsersListForUpdate(rows, group) {
			var userList = [];
			for (var i = 0; i < rows.length; i++) {

				var ref = {};

				ref.type =  rows[i].type;
				ref.userId =  rows[i].userId;


				var acc = getGroupDetails( rows[i].accountDetails);
				if( acc ){
					ref.fromAccount = acc.accountId;
				}



				if( group != null ) {
					ref.toAccount = group.group.accountId;
				}else {
					ref.toAccount = -1;
				}
				userList.push(ref);
			}


			return userList;


		}

		function getSelectedUsersListForDeletion(rows) {
			var userList = "";
			for (var i = 0; i < rows.length-1; i++) {

				userList += rows[i].userId + ",";
			}

			if(rows.length)
				userList += rows[i].userId;

			return userList;
		}

		function refreshDataUsage(){

			UserService.refreshDataUsage().success(function(response){

				analytics.sendEvent({
					category: "Operations",
					action: "Refresh Datausage",
					label: "User Management"
				});

				commonUtilityService.showSuccessNotification("Refresh data usage is successfully triggered");
				$scope.isDisableRefreshBtn = true;
				commonUtilityService.refreshDataUsageMonitor.monitor(response.sessionId);

			}).error(function(data){
				console.log('Refresh data usage is falied' + data);
				commonUtilityService.showErrorNotification(data.errorStr);

			});
		}
		function resetPassword(index){

			var user = $scope.usersList[index];

			var enterpriseId = sessionStorage.getItem('accountId');
			var userRole = getUserRole(user.accountDetails,enterpriseId);
			if(userRole != 'ENTERPRISE_ADMIN' && userRole !=  'ENTERPRISE_SUPPORT_ADMIN'){
				commonUtilityService.showErrorNotification('The password can only be reset for ADMIN role users.');
				return;
			}

			var msg = "A reset password link will be sent to "+user.emailId;
			var attr = {
				header: 'Reset Password',
				cancel: 'Cancel',
				submit: 'Reset'
			};
			var func = function(){
				AuthenticationService.forgotPassword(user.emailId).success(function () {
					commonUtilityService.showSuccessNotification("Instructions to reset the password have been sent to " + user.emailId);
				})
					.error(function (data, status) {
						commonUtilityService.showErrorNotification(data.errorStr);

					});
			};

			$scope.verificationModalObj.updateVerificationModel(attr,func);
			$scope.verificationModalObj.addVerificationModelMessage(msg);

			$('#verificationModal').modal('show');
		}

		function getUserRole(accDetails,enterpriseId){
			var userRole = 'ENTERPRISE_SIM_USER';

			for( var n = 0; n < accDetails.enterpriseAccountDetails.length; n++ ) {

				var parentAcc = accDetails.enterpriseAccountDetails[n].parentAccountDetails;
				for (var i = 0; parentAcc !== undefined, i < parentAcc.length; i++) {
					var accObj = parentAcc[i];
					if (accObj.accountId == enterpriseId && accObj.role == 'ENTERPRISE_ADMIN' || accObj.role == 'ENTERPRISE_SUPPORT_ADMIN' || accObj.role == 'ENTERPRISE_RESELLER_ADMIN' || accObj.role == 'ENTERPRISE_READ_ONLY_USER') {
						return accObj.role;
					}else if(accObj.role == 'ENTERPRISE_ADMIN' && accObj.accountType == 'GROUP'){
						userRole = accObj.role + ' on '+ accObj.accountName;
					}
				}

			}

			return userRole;
		}

		function prepareUserRoleFilters(userRoles){

			var userRoleFilters = [ { name:'ALL', dbRef:'',skipQueryParam:true} ];

			for(var i=0; i< userRoles.length; i++){

				var userRoleFilterObj =  {};
				userRoleFilterObj.dbRef = userRoles[i];
				userRoleFilterObj.name = userRoles[i].replace( "ENTERPRISE_", "").replace(/_/g, ' ');

				userRoleFilters.push(userRoleFilterObj);
			}

			return userRoleFilters;

		}

		function updateGroups(groups) {
			users.groups = groups;
		}
		activate();

	}
})();
