'use strict';

(function(){
	angular
		.module('app')
		.factory('GroupsService',['commonUtilityService', GroupsService]);
	/**
		@function GroupsService()
		- Factory method for getting groups data
	*/
	function GroupsService(commonUtilityService){

	    var factory = {
	        getGroups: getGroups,
			deleteGroups: deleteGroups,
			addGroup:addGroup,
			updateGroup:updateGroup
	    };
	    
	    return factory;
	    
	    /**
			@function request()
			- Check local storage before making an http request for groups data
		*/
	    function getGroups(queryParam ){
			var headerAdd=['ACCOUNT_READ'];
			var config = {
				cancellable : 'searchApi'
			};
			return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE  + "accounts/account/"+sessionStorage.getItem("accountId")+"/accounts", 'GET',headerAdd,queryParam,undefined,config);
		}

		function addGroup(group) {
			var headerAdd=['ACCOUNT_CREATE'];
			return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE  + 'accounts/account/', 'POST',headerAdd,false,group);
		}

		function updateGroup( group ) {
			group.type = "Account";
			group.accountType = "Group";
			var headerAdd=['ACCOUNT_EDIT'];
			return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + 'accounts/account/' + group.accountId, 'PUT',headerAdd,false,group);
		}


		function deleteGroups(groupsList ){
			var headerAdd=['ACCOUNT_DELETE'];
			return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account?aid="+groupsList, 'DELETE',headerAdd);
		}
	}	
})();