'use strict';

/* Controller */
(function(){
	angular.module('app').controller('GroupsCtrl', Groups);

	Groups.$inject = ['$scope','$compile','GroupsService', 'commonUtilityService', 'dataTableConfigService','loadingState','$sanitize','$rootScope', 'analytics','AuthenticationService'];

	/**
	 * @function Groups()
 	 * @param $scope - Angular.js $scope
	 * @param $compile - Angular.js $compile
	 * @param GroupsService - Factory service
	 * @param commonUtilityService - Factory service
	 * @param dataTableConfigService - Factory service
	 * @param loadingState - Factory service
	 * @param $sanitize - Angular.js $sanitize
	 * @param $rootScope - Angular.js $rootScope
	 * @param analytics - Factory service
     * @constructor
     */
	function Groups($scope, $compile,  GroupsService, commonUtilityService, dataTableConfigService,loadingState,$sanitize, $rootScope, analytics, AuthenticationService){
		
		var groups = this;

		/**
		 * Start up logic for controller
		 */
		function activate(){
			clearDataTableStatus(['DataTables_groupsListDataTable_']);
			$scope.emptyTableMessage = "No Groups available.";
			groups.verifyDeletion = verifyDeletion;
			groups.addGroup = addGroupModal;

			$scope.dataTableObj = {};
			$scope.groupModalObj = {};

			$scope.dataTableObj.tableID = "groupsListDataTable";
			var filtersConfig = { selectedDisplayLength: {type:'local', dbParam: 'count', defaultValue:25} };
			dataTableConfigService.configureFilters($scope.dataTableObj, filtersConfig);

			$scope.dataTableObj.tableHeaders = [
				{"value": "","DbValue":""},
				{"value": "Group Name","DbValue":"accountName"},
				{"value": "Actions", "DbValue":""}
				// {"value": "User Count","DbValue": "userCount"},
				// {"value": "Sim Count","DbValue": "simCount"}
			];

			$scope.dataTableObj.dataTableOptions = {
				"order": [[ 1, "asc" ]],
				"createdRow": function (row,data) {
					$(row).attr("id",'AID_'+data.accountId);
					$compile(row)($scope);
				},
				"columnDefs": [{
					"targets": 0,
					className: "checkbox-table-cell",
					"render": function ( data, type, row, meta) {
						return '<div gs-has-permission="ACCOUNT_DELETE" class="checkbox check-success large no-label-text hideInReadOnlyMode"> <input type="checkbox" value="1" id="checkbox_'+row.accountId+'" gs-index="'+meta.row+'"><label ng-click="checkBoxClicked($event)" id="label_'+row.accountId+'"></label></div>';
					}
				}, {
					"targets": 1,
					"render": function (data, type, row, meta) {
						var accountName = $sanitize(row.accountName);

						var permissionList = ['ACCOUNT_EDIT'];
						var isEditingAllowed = AuthenticationService.isOperationAllowed(permissionList);
						return isEditingAllowed ? '<a id="edit_group_'+row.accountId+'" ng-click="editGroup('+meta.row+');" href="javascript:void(0)" title="Click to edit group '+accountName+'" class="nowrap"><i class="fa fa-pencil edit-ico"></i>'+ ' ' + accountName +'</a>' : accountName;
					}
				}, {
					"targets": 2,
					"render": function (data, type, row) {
						var accountName = $sanitize(row.accountName);
						return '<a id="alerts_group_'+row.accountId+'" ng-click=\'openView(\"/app/alerts/group/'+row.accountId+'/per-sim-defaults\");\' href="javascript:void(0)" title="Click to add or edit alerts for group '+accountName+'" class="nowrap"><i class="fa fa-gear edit-ico"></i> Configure</a>';
					}
				}/*{
				 "targets": 2,
				 "render": function (data, type, row, meta) {

				 return 	typeof row.userCount != "undefined" ? Number(row.userCount): 0;
				 },
				 }, {
				 "targets": 3,
				 "render": function (data, type, row, meta) {
				 return 	typeof row.simCount != "undefined" ? Number(row.simCount) : 0;
				 }
				 } */ ],
				"oLanguage": {
					"sLengthMenu": "_MENU_ ",
					"sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
					"sEmptyTable": $scope.emptyTableMessage,
					"sZeroRecords":$scope.emptyTableMessage
				},
				"processing": true,
				"rowCallback": function( row, data ) {
					$scope.dataTableObj.showPreviousSelectedRows(data.accountId, row);
				},
				fnServerData: function (sSource, aoData, fnCallback) {
					if(aoData){
						groups.searchValue = aoData[5].value.value;
					}
					$scope.isDisable = false;
					var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);

					GroupsService.getGroups(queryParam).success(function (response) {
						angular.element('#checkbox_action').prop('checked', false);
						$scope.groupsList = ( response.list !== undefined ? response.list : [] );
						if($scope.groupsList.length == 0){
							$scope.isDisable = true;
						}
						var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
						var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
						if (oSettings != null) {
							fnCallback(datTabData);
						}else{
							loadingState.hide();
						}

					}).error(function(data){
						$scope.isDisable = !$scope.groupsList ||  $scope.groupsList.length == 0;
						var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
						dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"groups");
						console.log('get groups failure' );
						commonUtilityService.showErrorNotification(data.errorStr);
					});
				},

				"fnDrawCallback": function( oSettings ) {
					if($scope.groupsList)
						dataTableConfigService.disableTableHeaderOps( $scope.groupsList.length, oSettings );

					if($rootScope.isGemLite || !$rootScope.isAlertEnabled){
						$( "#" + $scope.dataTableObj.tableID ).DataTable().column(2).visible( false );
					}

					commonUtilityService.preventDataTableActivePageNoClick( $scope.dataTableObj.tableID );


				}

			};
			$scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

			$scope.dataTableObj.dataTableOptions.columns = [
				{"width": "60px", "orderable": false},
				null,
				{"width": "75px", "orderable": false}
				//null,
				// null
			];
		}




		$scope.checkBoxClicked = function(event){
			$scope.dataTableObj.selectRow(event);
		};

		$scope.isDisableComponent = function(){
			return $scope.isDisable;
		};

		$scope.editGroup = function(index){
			editGroupModal($scope.groupsList[index]);
		};

		/**
		 @function addGroupModal()
		 - Open the addGroupModal
		 */
		function addGroupModal() {
			/* Attributes for new user */
			var obj = {
				header: "Add New Group",
				submit: "Add"
			};
			$scope.groupModalObj.updateGroupModal(null, obj, $scope.onAddNewGroup);

			$('#addOrEditGroupModal').modal('show');
		}



		/**
		 @function editGroupModal()
		 @param {obj} group - group object to update
		 - Open the addGrouprModal
		 */
		function editGroupModal(group) {
			$scope.currentGroup = group;
			/* Attributes for existing user */
			var obj = {
				header: "Update Group",
				submit: "Update"
			};
			$scope.groupModalObj.updateGroupModal(group, obj, $scope.onEditExistingGroup);

			$('#addOrEditGroupModal').modal('show');

		}

		function verifyDeletion(rows){

			var msg = "Deleting groups cannot be undone! All SIMs and Users associated with deleted groups will be disassociated.";

			var attr = {
				header: 'Confirm Deletion',
				cancel: 'Cancel',
				submit: 'Delete'
			};
			var func = function(){$scope.processTableRowDeletion(rows);};

			$scope.verificationModalObj.updateVerificationModel(attr,func);
			$scope.verificationModalObj.addVerificationModelMessage(msg);

			$('#verificationModal').modal('show');
		}


		/**
		 @function processTableRowDeletion()
		 @return {boolean} - truthy value to determine if delete request was successful
		 */
		$scope.processTableRowDeletion = function(rows){

			/*

			 Successful = execute the code below, return true
			 Failure    = display error message, return false

			 - On the front end, we need to update our dataTable
			 and also update our scope reference ($scope.groups.usersData)
			 to our users data in the table.
			 */

			var groupsList = getSelectedGroupsListForDeletion(rows);
			var len = rows.length;

			GroupsService.deleteGroups(groupsList).success(function() {

				$scope.dataTableObj.refreshDataTable(false,len);
				var action = 'Delete';
				if(len == 1){
					action = action + ' Group';
					commonUtilityService.showSuccessNotification("Selected group has been deleted.");
				}else{
					action = action + ' Groups';
					commonUtilityService.showSuccessNotification("All selected groups have been deleted.");
				}
				analytics.sendEvent({
					category: "Operations",
					action: action,
					label: "Group Management"
				});

				return true;
			}).error(function(data){
				commonUtilityService.showErrorNotification(data.errorStr);
			});

		};

		$scope.onAddNewGroup = function(group) {

			/*

			 Successful = take the returned group ID
			 and execute the code below, return true

			 Failure    = display error message, return false
			 */
			group = { type:"AccountList", list: [ getGroupForAdd( group ) ] };

			GroupsService.addGroup(group).success(function() {
				$scope.dataTableObj.refreshDataTable(false);
				commonUtilityService.showSuccessNotification("Group "+group.list[0].accountName+ " has been created.");
				$scope.groupModalObj.resetGroupModal();

				analytics.sendEvent({
					category: "Operations",
					action: "Add Group",
					label: "Group Management"
				});

			}).error(function(data){
				console.log('add group failure' + data);
				$scope.groupModalObj.resetGroupModal();
				commonUtilityService.showErrorNotification(data.errorStr);
			});

		};

		/**
		 @function onEditExistingUser()
		 @param {obj} group - group object
		 @param {string} expando - expando $$hashKey value
		 */
		$scope.onEditExistingGroup =function(group, oldName) {

			if(Object.getOwnPropertyNames(group).length == 0){
				group = null;

			} else {
				delete group.$$hashKey;
			}

			if($scope.currentGroup.accountName  == oldName ){
				commonUtilityService.showErrorNotification("New group name cannot be the same as existing group name.");
				$scope.groupModalObj.resetGroupModal();
				console.log( "Error!!!");
				return;
			}

			GroupsService.updateGroup(group).success(function () {

				$scope.dataTableObj.refreshDataTable(false);
				commonUtilityService.showSuccessNotification("Group " + group.accountName + " has been updated successfully.");
				$scope.groupModalObj.resetGroupModal();
				analytics.sendEvent({
					category: "Operations",
					action: "Update Group",
					label: "Group Management"
				});

			}).error(function(data){
				commonUtilityService.showErrorNotification(data.errorStr);
				$scope.groupModalObj.resetGroupModal();
			});

		};

		function getGroupForAdd( group ){
			return { "type" : "Account",  "accountType":"GROUP", "accountName": group.accountName, "parentAccountId":  sessionStorage.getItem( "accountId" ) };
		}

		function getSelectedGroupsListForDeletion(rows) {
			var groupList = "";
			for (var i = 0; i < rows.length-1; i++) {

				groupList += rows[i].accountId + ",";
			}

			if(rows.length)
				groupList += rows[i].accountId;

			return groupList;

			//return groupList.replace(/,\s*$/, "");
		}

		activate();
	}
})();
















































