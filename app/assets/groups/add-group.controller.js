'use strict';

/* Controllers */
(function(){
	angular.module('app')
	    .controller('AddGroupCtrl',  AddGroup);

	AddGroup.$inject = ['$scope', '$sce', 'modalDetails', 'GroupsService','notifications','formMessage'];
	 
	/**
		@constructor AddGroup()
		@param {object} $scope - Angular.js $scope
		@param {object} $sce - Angular.js strict contextual escaping service
		@param {object} modalDetails - Factory object
	*/   
	function AddGroup($scope, $sce, modalDetails, GroupsService,notifications,formMessage){
		
		var addGroup = this;

		/**
		 * Start up logic for controller
		 */
		function activate(){
			/* Form elements object */
			addGroup.group = {};

			/* ModalDetails factory */
			modalDetails.scope(addGroup, true);
			modalDetails.attributes({
				id: 'addOrEditGroupModal',
				formName: 'addOrEditGroup',
				cancel: 'Cancel',
				submit: 'Add Group'
			});
			modalDetails.cancel(remove);
			modalDetails.submit(validate);

			addGroup.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};
			$scope.$parent.groupModalObj.updateGroupModal = updateModal; /* Attach to parent */
			$scope.$parent.groupModalObj.resetGroupModal = removeData;
		}

		/**
			@function updateModal()
			@param {object} group - optional - group's data to auto populate the form
			@param {object} obj - object to change modal attributes in directive
			@param {function} submit - function to execute on submit 
			@param {int} apiId - integer value to determine which API to update
			 - Allow a directive to pass data for the modal to update
		*/ 
		function updateModal(group, obj, submit ){

			//clear old form data
			addGroup.group = {};


			modalDetails.scope(addGroup, false);
			modalDetails.attributes(obj);

			//Update the form data
			if(group){
				updateData(group);
				var expando = group.$$hashKey;
				modalDetails.submit(function(addGroupForm){

					if($scope.isFormValid(addGroupForm,addGroup.group,true)) {

						console.log( addGroupForm.g_name.$viewValue );
						submit(addGroup.group, addGroupForm.g_name.$viewValue);
						hideModal();
					}
					else {

					}
				});
			}

			else {
				modalDetails.submit(function(addGroupForm){
					if($scope.isFormValid(addGroupForm,addGroup.group,true)) {
						submit(addGroup.group);
						hideModal();
					}

				});
			}

		}

		/**
		 @function updateData()
		 @param {object} group - group data to add to the form
		 */
		function updateData(group){


			if(group.accountId){
				$scope.addGroup.group.accountId = group.accountId;
			}

			if(group.accountName){
				$scope.addGroup.group.accountName = group.accountName;
			}

			if( group.type ){
				$scope.addGroup.group.type = group.type;
			}

		}

		function hideModal() {
			angular.element('#' + $scope.addGroup.modal.id).modal('hide');
		}


		/**
			@function remove()
			 - call a directive level function
		*/
		function remove(){
			removeData();
		}

		function removeData(){
			$scope.addGroup.group = {};
			formMessage.scope($scope.addGroup);
			formMessage.message(false, "");
		}
		
		/**
			@function validate()
			 - call a directive level function
		*/
		function validate(){
			addGroup.validateForm();
		}

		activate();
	}
})();
