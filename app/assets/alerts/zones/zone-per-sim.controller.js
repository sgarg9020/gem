'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('ZonePerSIMAlertsCtrl', ZonePerSIMAlerts);

    ZonePerSIMAlerts.$inject = ['$scope', '$compile', 'loadingState', 'dataTableConfigService', 'alertsService', 'commonUtilityService','alertsCommonFactory','$location', 'analytics','pollingAPIService','$interval'];

    /**
     @constructor ZonePerSIMAlerts()
     @param {object} $scope - Angular.js $scope
     @param {function} $compile - Angular.js $compile provider
     @param {object} loadingState - Factory Method
     @param {object} dataTableConfigService - Factory Method
     @param {object} alertsService - Factory Method
     @param {object} commonUtilityService - Factory Method
     @param {object} alertsCommonFactory - Factory Method
     @param {object} $location - Angular.js $location
     @param {object} analytics - Factory Method
     */
    function ZonePerSIMAlerts($scope, $compile, loadingState, dataTableConfigService, alertsService, commonUtilityService,alertsCommonFactory,$location, analytics,pollingAPIService,$interval) {

        var zonePerSIMAlerts = this;
        var navAlertPresent = false;
        var currentQueryParam = {};
        var promise;

        /**
         * Start up logic for controller
         */
        function activate(){
            $scope.emptyTableMessage = "No actions available.";
            // Functions for bulk options
            zonePerSIMAlerts.confirmAlertsEnable = alertsCommonFactory.confirmAlertsEnable;
            zonePerSIMAlerts.confirmAlertsDisable = alertsCommonFactory.confirmAlertsDisable;
            zonePerSIMAlerts.confirmAlertsDeletion = alertsCommonFactory.confirmAlertsDeletion;

            // DataTables
            addDataTablesObject($scope);
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

            zonePerSIMAlerts.alertId = $location.search().alertId;
            zonePerSIMAlerts.zoneId = $location.search().zoneId;
            // Adding/Editing Alerts
            $scope.alertModalObj = {};
            zonePerSIMAlerts.showAlertModal = alertsCommonFactory.showAlertModal;

            // Prepare alertsService with controller scope
            alertsCommonFactory.setScope($scope);

            // Searchable data-binding
            zonePerSIMAlerts.searchValue = "";
            $scope.alerts = {};
            $scope.isDisableComponent = {};

            promise = $interval(function() {alertsCommonFactory.monitorAlertStatus(currentQueryParam);}, 60000);

        }

        $scope.$on('$destroy', function(){
            //cancelling the interval to monitor alert status when controller is getting destroyed
            $interval.cancel(promise);
        });

        activate();

        /**
         * @function addDataTablesObject
         * @param {object} scope - controller $scope
         * @desc adds the dataTable object to the controller as object
         *
         * TODO - Create the functionality for dataTableOptions.fnServerData dynamically.
         */
        function addDataTablesObject(scope){
            scope.dataTableObj = {
                tableID: "zonePerSIMAlertsDataTable",
                tableHeaders: [
                    {
                        value: "",
                        dbValue: ""
                    },
                    {
                        value: "Zone",
                        dbValue: ""
                    },
                    {
                        value: "Usage",
                        dbValue: ""
                    },
                    {
                        value: "Action Outcome",
                        dbValue: ""
                    },
                    {
                        value: "Triggered",
                        dbValue: ""
                    },
                    {
                        value: "Status",
                        dbValue: ""
                    },
                    {
                        value: "",
                        dbValue: ""
                    }
                ],
                dataTableOptions: {
                    columns: [
                        {
                            width: "60px",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        },
                        {
                            width: "80px",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            className: "checkbox-table-cell",
                            render: function ( data, type, row) {
                                return '<div gs-has-permission="EDIT_ALERT,DELETE_ALERT" class="checkbox check-success large no-label-text hideInReadOnlyMode"><input type="checkbox" value="1" id="checkbox_'+row.alertId+'"><label ng-click="dataTableObj.selectRow($event)" id="label_'+row.alertId+'"></label></div>';
                            }
                        },
                        {
                            targets: 1,
                            render: function ( data, type, row) {
                                return row.zoneNickName || row.zoneName ;
                            }
                        },
                        {
                            targets: 2,
                            render: function (data, type, row) {
                                return row.alertSpecification.limitValue;
                            }
                        },
                        {
                            targets: 3,
                            render: function (data, type, row) {
                                var action = alertsCommonFactory.getActionName(row.alertSpecification.actions[0]);
                                return action;
                            }
                        },
                        {
                            targets: 4,
                            render: function (data, type, row) {
                                $scope.alerts[row.alertId] = row.triggerCount;
                                return '{{alerts['+row.alertId+']}}<i class="fa fa-spinner fa-pulse" style="margin-left: 5px" ng-show="isDisableComponent['+row.alertId+']"></i>';
                            }
                        },
                        {
                            targets: 5,
                            render: function (data, type, row) {
                                if(row.alertSpecification.enable)
                                    return '<div class="nowrap"><span class="status active"></span><span class="status-text">Enabled</span></div>';
                                return '<div class="nowrap"><span class="status inactive"></span><span class="status-text">Disabled</span></div>';
                            }
                        },
                        {
                            targets: 6,
                            render: function (data, type, row, meta) {
                                return '<a gs-has-permission="EDIT_ALERT" id="edit_alert_'+row.alertId+'" ng-click="showEditAlertModal('+meta.row+')" href="javascript:void(0)" title="Click to edit the alert" class="alerts-edit nowrap hideInReadOnlyMode"><i class="fa fa-pencil edit-ico"></i> Edit</a>';
                            }
                        }
                    ],
                    createdRow: function ( row, data ) {
                        $scope.isDisableComponent[data.alertId] = false;
                        if(data.alertId == zonePerSIMAlerts.alertId){
                            navAlertPresent = true;
                            $(row).addClass('highlighted-row');
                        }
                        $compile(row)($scope);
                        if(data.alertHistoryActionStatus== "IN_PROGRESS") {
                            $scope.isDisableComponent[data.alertId] = true;
                            pollingAPIService.monitor(data.alertId, alertsService.getAlert, alertsCommonFactory.pollStop, alertsCommonFactory.pollCallback);
                        }
                    },
                    fnServerData: function (sSource, aoData, fnCallback) {

                        var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                        queryParam.alertType= 'PER_SIM_PER_ZONE';

                        if(zonePerSIMAlerts.zoneId)
                            queryParam.zoneId = zonePerSIMAlerts.zoneId;
                        currentQueryParam = queryParam;
                        alertsService.getAlerts(queryParam).success(function(response){

                            var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                            //angular.element('#checkbox_action').prop('checked', false);
                            var tableData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);

                            // Store the data to the scope
                            $scope.zonePerSIMAlertsList = tableData.aaData;

                            // Call the dataTable with the appropriate data
                            if(oSettings != null)
                                fnCallback(tableData);
                            else
                                loadingState.hide();

                        }).error(function(data){
                            var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                            dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"actions");
                            commonUtilityService.showErrorNotification(data.errorStr);
                        });
                    },
                    "fnDrawCallback": function(oSettings) {
                        if(zonePerSIMAlerts.alertId && !navAlertPresent){
                            commonUtilityService.showErrorNotification("For selected alert, action is not available.Clearing the corresponding alert is in progress.");
                        }
                       // dataTableConfigService.disableTableHeaderOps($scope.zonePerSIMAlertsList.length, oSettings);
                        commonUtilityService.preventDataTableActivePageNoClick( $scope.dataTableObj.tableID );
                    },
                    "oLanguage": {
                        "sLengthMenu": "_MENU_ ",
                        "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                        "sEmptyTable": $scope.emptyTableMessage,
                        "sZeroRecords":$scope.emptyTableMessage
                    }
                }
            };
        }

        $scope.showEditAlertModal = function(index){
            var config = {
                alertType: 'PER_SIM_PER_ZONE',
                isZoneAlert: true
            };
            zonePerSIMAlerts.showAlertModal(config, $scope.zonePerSIMAlertsList[index]);
        };

        $scope.addAlert = function(alert){

            var alertObj = alertsCommonFactory.getAlertRequestObj(alert,'PER_SIM_PER_ZONE','DATA');

            var alertConfig = {};
            alertConfig.type = 'AlertConfigurationList';
            alertConfig.list = [alertObj];

            alertsService.addAlert(alertConfig).success(function(){

                $scope.dataTableObj.refreshDataTable(false);
                commonUtilityService.showSuccessNotification("Action has been added");

                analytics.sendEvent({
                    category: "Operations",
                    action: "Add Per Zone Per SIM Default Action",
                    label: "Actions"
                });


            }).error(function(data){
                console.log('add alert failure' + data);
                alertsCommonFactory.showError(data,'PER_SIM_PER_ZONE');

            });
        };

        $scope.editAlert = function(alert){
            var alertObj = alertsCommonFactory.getAlertRequestObj(alert,'PER_SIM_PER_ZONE','DATA');
            alertObj.alertId = alert.id;

            var alertConfig = {};
            alertConfig.type = 'EditAlertsRequest';
            alertConfig.list = [alertObj];

            alertsService.editAlert(alertConfig).success(function(){

                $scope.dataTableObj.refreshDataTable(false);
                commonUtilityService.showSuccessNotification("Action has been updated");

                analytics.sendEvent({
                    category: "Operations",
                    action: "Edit Per Zone Per SIM Default Action",
                    label: "Actions"
                });


            }).error(function(data){
                console.log('update alert failure' + data);
                alertsCommonFactory.showError(data,'PER_SIM_PER_ZONE');

            });
        };

        $scope.clearFilters = function(){
            $location.search('');
            zonePerSIMAlerts.alertId = undefined;
            zonePerSIMAlerts.zoneId = undefined;
            $scope.dataTableObj.refreshDataTable(false);
        };

    }
})();