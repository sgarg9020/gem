'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('GroupZonePerSIMAlertsCtrl', GroupZonePerSIMAlerts);

    GroupZonePerSIMAlerts.$inject = ['$scope', '$compile', '$stateParams', 'loadingState', 'dataTableConfigService', 'alertsService', 'commonUtilityService','alertsCommonFactory','SettingsService','analytics','pollingAPIService','$location','$interval'];

    /**
     * @function GroupZonePerSIMAlerts()
     * @param $scope
     * @param $compile
     * @param $stateParams
     * @param loadingState
     * @param dataTableConfigService
     * @param alertsService
     * @param commonUtilityService
     * @param alertsCommonFactory
     * @param settingsService
     * @param analytics
     * @constructor
     */
    function GroupZonePerSIMAlerts($scope, $compile, $stateParams, loadingState, dataTableConfigService, alertsService, commonUtilityService,alertsCommonFactory,settingsService,analytics,pollingAPIService,$location,$interval) {

        var groupZonePerSIMAlerts = this;
        var navAlertPresent = false;
        var currentQueryParam = {};
        var promise;

        /**
         * Start up logic for controller
         */
        function activate(){
            $scope.emptyTableMessage = "No actions available.";
            // Functions for bulk options
            groupZonePerSIMAlerts.confirmAlertsEnable = alertsCommonFactory.confirmAlertsEnable;
            groupZonePerSIMAlerts.confirmAlertsDisable = alertsCommonFactory.confirmAlertsDisable;
            groupZonePerSIMAlerts.confirmAlertsDeletion = alertsCommonFactory.confirmAlertsDeletion;
            groupZonePerSIMAlerts.displayNotCustomizedError = alertsCommonFactory.displayNotCustomizedError;


            // DataTables
            addDataTablesObject($scope);
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

            groupZonePerSIMAlerts.alertId = $location.search().alertId;
            groupZonePerSIMAlerts.zoneId = $location.search().zoneId;

            // Adding/Editing Alerts
            $scope.alertModalObj = {};
            $scope.alertCustomized  = false;
            groupZonePerSIMAlerts.showAlertModal = alertsCommonFactory.showAlertModal;
            groupZonePerSIMAlerts.confirmAlertCustomize = alertsCommonFactory.confirmAlertCustomize;
            groupZonePerSIMAlerts.confirmAlertRevert    = alertsCommonFactory.confirmAlertRevert;

            $scope.alertCustomized  = false;
            $scope.alertType = "PER_SIM_PER_ZONE";



            // Prepare alertsService with controller scope
            alertsCommonFactory.setScope($scope);

            // Searchable data-binding
            groupZonePerSIMAlerts.searchValue = "";
            $scope.groupId = $stateParams.groupId;

            var queryParam = {
                details:"accountName"
            };
            settingsService.getEnterpriseDetails($scope.groupId,queryParam).success(function(data){
                $scope.accountName = data.accountName;
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });

            $scope.alerts = {};
            $scope.isDisableComponent = {};

            promise = $interval(function() {alertsCommonFactory.monitorAlertStatus(currentQueryParam);}, 60000);
        }

        $scope.$on('$destroy', function(){
            //cancelling the interval to monitor alert status when controller is getting destroyed
            $interval.cancel(promise);
        });

        activate();

        /**
         * @function addDataTablesObject
         * @param {object} scope - controller $scope
         * @desc adds the dataTable object to the controller as object
         *
         * TODO - Create the functionality for dataTableOptions.fnServerData dynamically.
         */
        function addDataTablesObject(scope){
            scope.dataTableObj = {
                tableID: "groupZonePerSIMAlertsDataTable",
                tableHeaders: [
                    {
                        value: "",
                        dbValue: ""
                    },
                    {
                        value: "Zone",
                        dbValue: ""
                    },
                    {
                        value: "Usage",
                        dbValue: ""
                    },
                    {
                        value: "Action Outcome",
                        dbValue: ""
                    },
                    {
                        value: "Triggered",
                        dbValue: ""
                    },
                    {
                        value: "Status",
                        dbValue: ""
                    },
                    {
                        value: "",
                        dbValue: ""
                    }
                ],
                dataTableOptions: {
                    columns: [
                        {
                            width: "60px",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        },
                        {
                            width: "80px",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            className: "checkbox-table-cell",
                            render: function ( data, type, row) {
                                return '<div gs-has-permission="EDIT_ALERT,DELETE_ALERT" class="checkbox check-success large no-label-text hideInReadOnlyMode"><input type="checkbox" value="1" id="checkbox_'+row.alertId+'"><label ng-click="dataTableObj.selectRow($event)" id="label_'+row.alertId+'"></label></div>';
                            }
                        },
                        {
                            targets: 1,
                            render: function ( data, type, row) {
                                return row.zoneNickName || row.zoneName;
                            }
                        },
                        {
                            targets: 2,
                            render: function (data, type, row) {
                                return row.alertSpecification.limitValue;
                            }
                        },
                        {
                            targets: 3,
                            render: function (data, type, row) {
                                var action = alertsCommonFactory.getActionName(row.alertSpecification.actions[0]);
                                return action;

                            }
                        },
                        {
                            targets: 4,
                            render: function (data, type, row) {
                                $scope.alerts[row.alertId] = row.triggerCount;
                                return '{{alerts['+row.alertId+']}}<i class="fa fa-spinner fa-pulse" style="margin-left: 5px" ng-show="isDisableComponent['+row.alertId+']"></i>';
                            },
                            "defaultContent":"0"
                        },
                        {
                            targets: 5,
                            render: function (data, type, row) {
                                if(row.alertSpecification.enable)
                                    return '<div class="nowrap"><span class="status active"></span><span class="status-text">Enabled</span></div>';
                                return '<div class="nowrap"><span class="status inactive"></span><span class="status-text">Disabled</span></div>';
                            }
                        },
                        {
                            targets: 6,
                            render: function (data, type, row, meta) {
                                return '<a gs-has-permission="EDIT_ALERT" id="edit_alert_'+row.alertId+'" ng-click="alertCustomized ? showEditAlertModal( '+ meta.row + '): groupZonePerSIMAlerts.displayNotCustomizedError()" href="javascript:void(0)" title="Click to edit the alert" class="alerts-edit nowrap hideInReadOnlyMode"><i class="fa fa-pencil edit-ico"></i> Edit</a>';
                            }
                        }
                    ],
                    createdRow: function (row,data) {
                        $scope.isDisableComponent[data.alertId] = false;
                        if(data.alertId == groupZonePerSIMAlerts.alertId){
                            navAlertPresent = true;
                            $(row).addClass('highlighted-row');
                        }
                        $compile(row)($scope);
                        if(data.alertHistoryActionStatus== "IN_PROGRESS") {
                            $scope.isDisableComponent[data.alertId] = true;
                            pollingAPIService.monitor(data.alertId, alertsService.getAlert, alertsCommonFactory.pollStop, alertsCommonFactory.pollCallback);
                        }
                    },
                    fnServerData: function (sSource, aoData, fnCallback) {

                        var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                        $scope.row =[];

                      queryParam.alertType =  $scope.alertType;
                        if(groupZonePerSIMAlerts.zoneId)
                            queryParam.zoneId = groupZonePerSIMAlerts.zoneId;
                        currentQueryParam = queryParam;
                        alertsService.getAlerts(queryParam,$scope.groupId ).success(function(response){

                            var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                            //angular.element('#checkbox_action').prop('checked', false);
                            var tableData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);

                            // Store the data to the scope
                            $scope.groupPerZonePerSIMAlertsList = tableData.aaData;
                            alertsCommonFactory.setAlertCustomized($scope.groupPerZonePerSIMAlertsList);

                            // Call the dataTable with the appropriate data
                            if(oSettings != null)
                                fnCallback(tableData);
                            else
                                loadingState.hide();

                        }).error(function(data){
                            var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                            dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"actions");
                            commonUtilityService.showErrorNotification(data.errorStr);
                        });
                    },
                    "processing": true,
                    "oLanguage": {
                        "sLengthMenu": "_MENU_ ",
                        "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                        "sEmptyTable": $scope.emptyTableMessage,
                        "sZeroRecords":$scope.emptyTableMessage
                    },
                    "fnDrawCallback": function(oSettings) {
                        if(groupZonePerSIMAlerts.alertId && !navAlertPresent){
                            commonUtilityService.showErrorNotification("For selected alert, action is not available.Clearing the corresponding alert is in progress.");
                        }

                       // dataTableConfigService.disableTableHeaderOps($scope.groupPerZonePerSIMAlertsList.length, oSettings);
                        commonUtilityService.preventDataTableActivePageNoClick( $scope.dataTableObj.tableID );
                    }
                }
            };
        }

        $scope.showEditAlertModal = function(index){

            var data = $scope.groupPerZonePerSIMAlertsList[index];
            data.action = data.alertSpecification.actions[0].charAt(0).toUpperCase();
            var config = {
                alertType: 'PER_SIM_PER_ZONE',
                isZoneAlert: true
            };
            groupZonePerSIMAlerts.showAlertModal(config, data);
        };

        $scope.editAlert = function(alert ){
            var alertObj = alertsCommonFactory.getAlertRequestObj(alert,'PER_SIM_PER_ZONE','DATA',$scope.groupId);
            alertObj.alertId = alert.id;

            var alertConfig = {};
            alertConfig.type = 'EditAlertsRequest';
            alertConfig.list = [alertObj];

            alertsService.editAlert(alertConfig).success(function(){

                $scope.dataTableObj.refreshDataTable(false);
                commonUtilityService.showSuccessNotification( "Action has been updated");

                loadingState.hide();

                analytics.sendEvent({
                    category: "Operations",
                    action: "Edit Per Zone Per SIM Default Action for Group",
                    label: "Actions"
                });
            }).error(function(data){
                alertsCommonFactory.showError(data,'PER_SIM_PER_ZONE');
            });

        };
        $scope.addAlert = function( alert ){
            var alertObj = alertsCommonFactory.getAlertRequestObj(alert,'PER_SIM_PER_ZONE','DATA',$scope.groupId);

            var alertConfig = {};
            alertConfig.type = 'AlertConfigurationList';
            alertConfig.list = [alertObj];

            alertsService.addAlert(alertConfig).success(function(){

                $scope.dataTableObj.refreshDataTable(false);
                commonUtilityService.showSuccessNotification( "Action has been added");
                $scope.dataTableObj.refreshDataTable(false);
                loadingState.hide();

                analytics.sendEvent({
                    category: "Operations",
                    action: "Add Per Zone Per SIM Default Action for Group",
                    label: "Actions"
                });

            }).error(function(data){
                alertsCommonFactory.showError(data,'PER_SIM_PER_ZONE');
            });
        };

        $scope.clearFilters = function(){
            $location.search('');
            groupZonePerSIMAlerts.alertId = undefined;
            groupZonePerSIMAlerts.zoneId = undefined;
            $scope.dataTableObj.refreshDataTable(false);
        };

    }
})();