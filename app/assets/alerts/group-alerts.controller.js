'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('GroupAlertsCtrl', GroupAlerts);

    GroupAlerts.$inject = ['$scope', 'loadingState'];

    /**
     @constructor GroupAlerts()
     @param {object} $scope - Angular.js $scope
     @param {object} loadingState - Factory Method
     */
    function GroupAlerts($scope, loadingState) {

        var groupAlerts = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            // ng-if to show information portlet
            groupAlerts.showPortlet = true;
        }
        activate();

    }
})();