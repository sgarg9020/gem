'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('AccountAlertsCtrl', AccountAlerts);

    AccountAlerts.$inject = ['$location'];

    /**
     @constructor AccountAlerts()
     */
    function AccountAlerts($location) {

        var accountAlerts = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            // ng-if to show information portlet
            accountAlerts.showPortlet = true;
            accountAlerts.setMessages();
        }
        accountAlerts.setMessages = function(event){
            var path;
            if(event){
                path = event.currentTarget.href;
            }else{
                path = $location.path();
            }

            if(path.indexOf('/enterprise-spend') > -1) {
                accountAlerts.alertMessage = 'Account actions affect all SIMs and are triggered by spend allocations.If more flexibility is required, actions can be added/edited at the group and/or individual SIM levels.';
            }else if(path.indexOf('/zone-spend') > -1){
                accountAlerts.alertMessage = 'Zone action settings affect all SIMs and are triggered by spend allocations in a specific zone. If more flexibility is required, actions can be added/edited at the group and/or individual SIM levels.';
            }else if(path.indexOf('/per-sim-defaults') > -1){
                accountAlerts.alertMessage = 'SIM action defaults apply to all SIMs unless individual SIM data usage allocations are configured.';
            }else if(path.indexOf('/per-sim-by-zone-defaults') > -1){
                accountAlerts.alertMessage = 'Per SIM by Zone action defaults apply to all SIMs in a single zone unless individual SIM data usage allocations are configured.';
            }
        };
        activate();

    }
})();