'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('AlertModalCtrl', AlertModal);
    AlertModal.$inject = ['$scope', '$sce', 'modalDetails','alertsService','commonUtilityService','SettingsService','alertsCommonFactory'];

    /**
     @constructor AlertModal()
     @param {object} $scope - Angular.js $scope
     @param {object} $sce - Angular.js strict contextual escaping service
     @param {object} modalDetails - Factory object
     @param {object} formMessage - Factory object
     @param {object} groupsService - Factory object
     */
    function AlertModal($scope, $sce, modalDetails,alertsService,commonUtilityService,SettingsService,alertsCommonFactory) {

        var alertModal = this;

        /**
         * Start up logic for controller
         */
        function activate(){

            // ModalDetails factory
            modalDetails.scope(alertModal, true);
            modalDetails.attributes({
                id: 'alertModal',
                formName: 'alertModalForm'
            });

            // Strict contextual escaping
            alertModal.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};

            // Hooks for the parent
            $scope.$parent.alertModalObj = $scope.$parent.alertModalObj || {};
            $scope.$parent.alertModalObj.updateAlertModal = updateModal;
            $scope.$parent.alertModalObj.setShowSpend = setShowSpend;
            $scope.$parent.alertModalObj.setShowZones = setShowZones;
            $scope.$parent.alertModalObj.setPatterns = setPatterns;
            $scope.$parent.alertModalObj.informativeMessage = setInformativeMessage;

            // Binding object for form data
            alertModal.alert = {};

            alertModal.costPattern = /[1-9].*/;
            alertModal.spendPattern = /[1-9].*/;

            $scope.generalObj = {};

            var enterpriseCurrency = getLocalCurrencySign(sessionStorage.getItem( "gs-enterpriseCurrency"));
            // AutoNumeric Plugin options for spend input
            alertModal.autoNumeric = {
                spend: {
                    aSign: enterpriseCurrency,
                    vMin: '0',
                    vMax: '100000000',
                    mDec: '0',
                    aPad: false,
                    wEmpty: 'sign',
                    lZero: 'deny'
                },
                dataUsage: {
                    vMin: '0',
                    vMax: '100000000',
                    mDec: '0',
                    aSep: '',
                    aPad: false,
                    lZero: 'deny',
                    pSign: 's'
                }
            };
            alertsService.getAllowedAlertActionType().success(function(data){
                alertModal.actions = data.supportedAlertActionList;
                _updateAlertActionNames(alertModal.actions);
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });

            alertModal.alertMessage = "<i class='fa fa-warning'></i> Choosing a <bold>deactivate</bold> action outcome will deactivate SIMs when a specified allocation level is reached.";

        }

        function _updateAlertActionNames(actions){
                for(var i = 0; i < actions.length; i++){
                    actions[i] = alertsCommonFactory.getActionName(actions[i]);
                }
        }

        function hideModal() {
            angular.element('#' + $scope.alertModal.modal.id).modal('hide');
        }

        /**
         @function updateModal()
         @param {object} obj - object to change modal attributes in directive
         @param {function} submit - function to invoke on form submit
         */
        function updateModal(alert,obj, submit){

            //clear old form data
            alertModal.alert = {};
            alertModal.errorMessage = '';
            alertModal.isEdit = false;
            // Add new modal data
            modalDetails.scope(alertModal, false);
            modalDetails.attributes(obj);

            if(alert){
                updateData(alert);
                alertModal.isEdit = true;
                $scope.generalObj.previousData = angular.copy(alertModal.alert);
                modalDetails.submit(function(alertModalForm){
                    alertModal.errorMessage = '';
                    var inputVal = $('#input_alertAllocation').val()?$('#input_alertAllocation').val():$('#input_alertAllocationInCost').val();
                    if(inputVal !== '$' && inputVal !== undefined){
                        if (alertModal.showZones) {
                            if(!alertModal.showSpend)
                                $('#input_alertAllocation').attr('requiredError', 'Setting 0 MB limit for warning action is not supported');
                            else
                                $('#input_alertAllocationInCost').attr('requiredError', 'Setting 0 limit for warning action is not supported');

                        }else{
                            if(alertModal.showSpend)
                                $('#input_alertAllocationInCost').attr('requiredError', '');
                            else
                                $('#input_alertAllocation').attr('requiredError', 'Setting of 0 limit for individual SIMs is not supported');
                        }
                    }
                    if($scope.isFormValid(alertModalForm,alertModal.alert,true)) {
                        if(alertModal.showSpend){
                            alertModal.alert.allocation = $scope.getUnformattedDataForAutonumericField(alertModalForm,'input_alertAllocationInCost');
                        }
                        submit(alertModal.alert);
                        hideModal();
                    }else if($scope.generalObj.isNoChangeError){
                        alertModal.errorMessage = "There is no change in the data to update.";
                        $scope.generalObj.isNoChangeError = false;
                    }
                });
            }else{
                //clearing the view value using jquery, as $watch was not getting triggered if previous value was 0.
                $('[id^="input_alertAllocation"]').autoNumeric('set', "");

                $scope.generalObj.previousData = null;
                modalDetails.submit(function(alertModalForm){
                    alertModal.errorMessage = '';
                    var inputVal = $('#input_alertAllocation').val()?$('#input_alertAllocation').val():$('#input_alertAllocationInCost').val();
                    if(inputVal !== '$' && inputVal !== undefined){
                        if (alertModal.showZones) {
                            if(!alertModal.showSpend)
                                $('#input_alertAllocation').attr('requiredError', 'Setting 0 MB limit for warning action is not supported');
                            else
                                $('#input_alertAllocationInCost').attr('requiredError', 'Setting 0 limit for warning action is not supported');

                        }else{
                            if(alertModal.showSpend)
                                $('#input_alertAllocationInCost').attr('requiredError', '');
                            else
                                $('#input_alertAllocation').attr('requiredError', 'Setting of 0 limit for individual SIMs is not supported');
                        }
                    }
                    if($scope.isFormValid(alertModalForm,alertModal.alert,true)) {
                        if(alertModal.showSpend){
                            alertModal.alert.allocation = $scope.getUnformattedDataForAutonumericField(alertModalForm,'input_alertAllocationInCost');
                        }
                        submit(alertModal.alert);
                        hideModal();
                    }
                });
            }
        }

        /**
         @function updateData()
         @param {object} alert - alert data to add to the form
         */
        function updateData(alert) {
            if (alert.alertId) {
                $scope.alertModal.alert.id = alert.alertId;
            }

            if(alert.alertSpecification){
                if (alert.alertSpecification.limitValue >=0) {
                    $scope.alertModal.alert.allocation = alert.alertSpecification.limitValue;
                }

                if (alert.alertSpecification.actions) {
                    var action = alert.alertSpecification.actions[0];

                    $scope.alertModal.alert.action = alertsCommonFactory.getActionName(action);
                }

                if (alert.alertSpecification.triggerCount) {
                    $scope.alertModal.alert.triggerCount = alert.alertSpecification.triggerCount;
                }

                if( alert.zoneId ){
                    $scope.alertModal.alert.zone = {
                        zoneId:alert.zoneId,
                        name:alert.zoneName
                    };
                }
                alertModal.actionOutcomeChanged();
                $scope.alertModal.alert.isEnabled = alert.alertSpecification.enable;
            }
        }
        alertModal.actionOutcomeChanged = function(){
            if(alertModal.showZones){
                if(alertModal.alert.action === "Warning"){
                    setPatterns(/[1-9].*/,/[1-9].*/);
                }  else{
                    setPatterns(/[0-9].*/,/[0-9].*/);
                }
            }
        };
        function setPatterns(costPattern,spendPattern){
            alertModal.costPattern = costPattern;
            alertModal.spendPattern = spendPattern;
        }
        /**
         * @function setShowSpend
         * @param {boolean} bool - value to set alertModal.showSpend
         * @desc mutator for alertModal.showSpend
         */
        function setShowSpend(bool){
            alertModal.showSpend = bool;
        }

        /**
         * @function setShowZones
         * @param {boolean} bool - value to set alertModal.showZones
         * @desc mutator for alertModal.showZones
         */
        function setShowZones(bool){
            alertModal.showZones = bool;

            if(bool && !alertModal.zones)
                _requestZones();
        }

        /**
         * @private _requestZones()
         * @desc gets a list of zones to be used for the zone ui select
         */
        function _requestZones(){

            var details = 'name';

            SettingsService.getPlanDetails(details).success(function (response) {
                alertModal.zones = response;
            }).error(function (data, status) {
                console.log("Error while fetching all zones");
            });

        }

        /**
         * @function setInformativeMessage()
         * @param {string|node} message - message to display in the modal.
         * @desc updates the informative message inside of the alert modal
         */
        function setInformativeMessage(message){
            alertModal.alertMessage = message;
        }

        activate();
    }
})();