'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('AlertSidebarCtrl', AlertSidebarCtrl);

    AlertSidebarCtrl.$inject = ['$scope', '$timeout', 'alertsService', 'commonUtilityService','$rootScope','$interval'];

    /**
     @constructor AlertSidebarCtrl()
     @param {object} $scope - Angular.js $scope
     @param {function} $timeout - Angular.js $timeout
     @param {object} alertsService - Factory Method
     @param {object} commonUtilityService - Factory methods
     @param {object} $rootScope - Angular.js $rootScope
     */
    function AlertSidebarCtrl($scope, $timeout, alertsService, commonUtilityService,$rootScope,$interval) {

        var promise;
        var brodcastListener;
        /**
         * Start up logic for controller
         */
        function activate() {
            $scope.currencyFormat = getLocalCurrencySign(sessionStorage.getItem( "gs-enterpriseCurrency"));
            getTriggeredAlertsCount();

            promise = $interval(function(){getTriggeredAlertsCount(true);}, 60000);
        }

        function updateTriggeredAlerts() {
            getTriggeredAlertsCount(false);
            getTopAccountAlerts();
            getTopZoneAlerts();
            getTopSIMAlerts();
        }

        function getTriggeredAlertsCount(isBackgroundCall){
            var queryParam = {};
            queryParam.startIndex=0;
            queryParam.count=0;
            queryParam.noPace = true;
            alertsService.getAlertHistory(queryParam,undefined,isBackgroundCall).success(function (response) {

                $rootScope.triggeredAlertsCount = response.totalCount;

            }).error(function (data) {
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }
        function getTopAccountAlerts() {
            var queryParam = {};
            queryParam.startIndex=0;
            queryParam.count=5;
            queryParam.filter="ACCOUNT";
            queryParam.noPace = true;
            toggleLoadingState(true);
            alertsService.getAlertHistory(queryParam).success(function (response) {

                // Store the data to the scope
                $scope.topAccountAlerts = response;
                toggleLoadingState(false);

            }).error(function (data) {
                commonUtilityService.showErrorNotification(data.errorStr);
                toggleLoadingState(false);
            });
        }

        function getTopZoneAlerts() {
            var queryParam = {};
            queryParam.startIndex=0;
            queryParam.count=5;
            queryParam.filter="ZONE";
            queryParam.noPace = true;
            toggleLoadingState(true);
            alertsService.getAlertHistory(queryParam).success(function (response) {

                // Store the data to the scope
                $scope.topZoneAlerts = response;
                toggleLoadingState(false);

            }).error(function (data) {
                commonUtilityService.showErrorNotification(data.errorStr);
                toggleLoadingState(false);
            });
        }

        function getTopSIMAlerts() {
            var queryParam = {};
            queryParam.startIndex=0;
            queryParam.count=5;
            queryParam.filter="SIM";
            queryParam.noPace = true;
            toggleLoadingState(true);
            alertsService.getAlertHistory(queryParam).success(function (response) {
                if($scope.currencyFormat === null)
                    $scope.currencyFormat = getLocalCurrencySign(sessionStorage.getItem( "gs-enterpriseCurrency"));
                // Store the data to the scope
                $scope.topSIMAlerts = response;
                toggleLoadingState(false);

            }).error(function (data) {
                commonUtilityService.showErrorNotification(data.errorStr);
                toggleLoadingState(false);
            });
        }

        function toggleLoadingState(show){
            if(show)
                angular.element('.alertSideBarLoading').show();
            else
                angular.element('.alertSideBarLoading').hide();
        }

        brodcastListener = $rootScope.$on('onUpdateAlertHistory', function(event,data) {
            updateTriggeredAlerts();
        });

        $scope.$on('$destroy', function(){
            //cancelling the interval to monitor alert status when controller is getting destroyed
            var success = $interval.cancel(promise);
            if(brodcastListener)
                brodcastListener();
            
            toggleLoadingState(false);
        });


        $timeout(activate);
    }
})();