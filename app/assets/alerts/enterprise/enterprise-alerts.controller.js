'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('EnterpriseSpendAlertsCtrl', EnterpriseSpendAlerts);
     EnterpriseSpendAlerts.$inject = ['$scope', '$compile', 'loadingState', 'dataTableConfigService', 'alertsService', 'commonUtilityService','alertsCommonFactory','$location', 'analytics','pollingAPIService','$interval'];

    /**
     @constructor EnterpriseSpendAlerts()
     @param {object} $scope - Angular.js $scope
     @param {object} $compile - Angular.js $compile
     @param {object} loadingState - Factory Method
     @param {object} dataTableConfigService - Factory Method
     @param {object} alertsService - Factory Method
     @param {object} commonUtilityService - Factory Method
     @param {object} alertsCommonFactory - Factory Method
     @param {object} $location - Angular.js $location
     @param {object} analytics - Factory Method
     */
    function EnterpriseSpendAlerts($scope, $compile, loadingState, dataTableConfigService, alertsService, commonUtilityService,alertsCommonFactory,$location,analytics,pollingAPIService,$interval) {

        var enterpriseSpendAlerts = this;
        var navAlertPresent = false;
        var currentQueryParam = {};
        var promise;

        /**
         * Start up logic for controller
         */
        function activate(){
            $scope.emptyTableMessage = "No actions available.";

            // Functions for bulk options
            enterpriseSpendAlerts.confirmAlertsEnable = alertsCommonFactory.confirmAlertsEnable;
            enterpriseSpendAlerts.confirmAlertsDisable = alertsCommonFactory.confirmAlertsDisable;
            enterpriseSpendAlerts.confirmAlertsDeletion = alertsCommonFactory.confirmAlertsDeletion;

            // DataTables
            addDataTablesObject($scope);
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

            enterpriseSpendAlerts.alertId = $location.search().alertId;

            // Adding/Editing Alerts
            $scope.alertModalObj = {};
            enterpriseSpendAlerts.showAlertModal = alertsCommonFactory.showAlertModal;

            // Prepare alertsService with controller scope
            alertsCommonFactory.setScope($scope);
            $scope.alerts = {};
            $scope.isDisableComponent = {};

            promise = $interval(function() {alertsCommonFactory.monitorAlertStatus(currentQueryParam);}, 60000);
        }

        $scope.$on('$destroy', function(){
            //cancelling the interval to monitor alert status when controller is getting destroyed
            $interval.cancel(promise);
        });

        activate();

        /**
         * @function addDataTablesObject
         * @param {object} scope - controller $scope
         * @desc adds the dataTable object to the controller as object
         */
        function addDataTablesObject(scope){
            scope.dataTableObj = {
                tableID: "enterpriseSpendAlertsDataTable",
                tableHeaders: [
                    {
                        value: "",
                        dbValue: ""
                    },
                    {
                        value: "Spend",
                        dbValue: ""
                    },
                    {
                        value: "Action Outcome",
                        dbValue: ""
                    },
                    {
                        value: "Triggered",
                        dbValue: ""
                    },
                    {
                        value: "Status",
                        dbValue: ""
                    },
                    {
                        value: "",
                        dbValue: ""
                    }
                ],
                dataTableOptions: {
                    bPaginate: false,
                    bInfo: false,
                    columns: [
                        {
                            width: "60px",
                            orderable: false
                        },
                        {
                            width: "80px",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        },
                        {
                            width: "",
                            orderable: false
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            className: "checkbox-table-cell",
                            render: function ( data, type, row) {
                                return '<div gs-has-permission="EDIT_ALERT,DELETE_ALERT" class="checkbox check-success large no-label-text hideInReadOnlyMode"><input type="checkbox" value="1" id="checkbox_'+row.alertId+'"><label ng-click="dataTableObj.selectRow($event)" id="label_'+row.alertId+'"></label></div>';
                            }
                        },
                        {
                            targets: 1,
                            render: function (data, type, row) {
                                var rowData = '';
                                if(row.alertSpecification.limitType === "COST"){
                                    rowData = getLocalCurrencySign(sessionStorage.getItem( "gs-enterpriseCurrency"));
                                }
                                rowData += row.alertSpecification.limitValue;
                                return rowData;
                            }
                        },
                        {
                            targets: 2,
                            render: function (data, type, row) {
                                var action = alertsCommonFactory.getActionName(row.alertSpecification.actions[0]);
                                return action;
                            }
                        },
                        {
                            targets: 3,
                            render: function (data, type, row) {
                                $scope.alerts[row.alertId] = row.triggerCount;
                                return '{{alerts['+row.alertId+']> 0 ? "YES" : "NO"}}<i class="fa fa-spinner fa-pulse" style="margin-left: 5px" ng-show="isDisableComponent['+row.alertId+']"></i>';
                            }
                        },
                        {
                            targets: 4,
                            render: function (data, type, row) {
                                if(row.alertSpecification.enable)
                                    return '<div class="nowrap"><span class="status active"></span><span class="status-text">Enabled</span></div>';
                                return '<div class="nowrap"><span class="status inactive"></span><span class="status-text">Disabled</span></div>';
                            }
                        },
                        {
                            targets: 5,
                            render: function (data, type, row, meta) {
                                return '<a  gs-has-permission="EDIT_ALERT" id="edit_alert_'+row.alertId+'" ng-click="showEditAlertModal('+meta.row+')" href="javascript:void(0)" title="Click to edit the alert" class="alerts-edit nowrap hideInReadOnlyMode"><i class="fa fa-pencil edit-ico"></i> Edit</a>';
                            }
                        }
                    ],
                    createdRow: function ( row, data ) {
                        $(row).attr("id",'ALERTID_'+data.alertId);
                        $scope.isDisableComponent[data.alertId] = false;
                        if(data.alertId == enterpriseSpendAlerts.alertId){
                            navAlertPresent = true;
                            $(row).addClass('highlighted-row');
                        }
                        $compile(row)($scope);
                        if(data.alertHistoryActionStatus== "IN_PROGRESS") {
                            $scope.isDisableComponent[data.alertId] = true;
                            pollingAPIService.monitor(data.alertId, alertsService.getAlert, alertsCommonFactory.pollStop, alertsCommonFactory.pollCallback);
                        }
                    },
                    fnServerData: function (sSource, aoData, fnCallback) {

                        var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                        queryParam.alertType= 'ACCOUNT_CUMULATIVE';
                        currentQueryParam = queryParam;
                        alertsService.getAlerts(queryParam).success(function(response){

                            var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                            //angular.element('#checkbox_action').prop('checked', false);
                            var tableData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);

                            // Store the data to the scope
                            $scope.enterpriseSpendAlertsList = tableData.aaData;

                            // Call the dataTable with the appropriate data
                            if(oSettings != null)
                                fnCallback(tableData);
                            else
                                loadingState.hide();

                        }).error(function(data){
                            var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                            dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"actions");
                            commonUtilityService.showErrorNotification(data.errorStr);
                        });
                    },
                    "fnDrawCallback": function(oSettings) {
                        if(enterpriseSpendAlerts.alertId && !navAlertPresent){
                            commonUtilityService.showErrorNotification("For selected alert, action is not available.Clearing the corresponding alert is in progress.");
                        }
                        //commenting it as sorting is disabled for all columns
                       // dataTableConfigService.disableTableHeaderOps($scope.enterpriseSpendAlertsList.length, oSettings);
                    },
                    "oLanguage": {
                        "sEmptyTable": $scope.emptyTableMessage,
                        "sZeroRecords":$scope.emptyTableMessage
                    }
                }
            };
        }

        $scope.showEditAlertModal = function(index){
            var config = {
                alertType: 'ACCOUNT_CUMULATIVE',
                isSpendAlert: true
            };
            enterpriseSpendAlerts.showAlertModal(config, $scope.enterpriseSpendAlertsList[index]);
        };

        $scope.addAlert = function(alert){

            var alertObj = alertsCommonFactory.getAlertRequestObj(alert,'ACCOUNT_CUMULATIVE','COST');

            var alertConfig = {};
            alertConfig.type = 'AlertConfigurationList';
            alertConfig.list = [alertObj];

            alertsService.addAlert(alertConfig).success(function(){

                $scope.dataTableObj.refreshDataTable(false);
                commonUtilityService.showSuccessNotification("Action has been added");

                analytics.sendEvent({
                    category: "Operations",
                    action: "Add Enterprise Spend Action",
                    label: "Actions"
                });

            }).error(function(data){
                console.log('add alert failure' + data);
                alertsCommonFactory.showError(data,'ACCOUNT_CUMULATIVE');
            });
        };

        $scope.editAlert = function(alert){
            var alertObj = alertsCommonFactory.getAlertRequestObj(alert,'ACCOUNT_CUMULATIVE','COST');
            alertObj.alertId = alert.id;

            var alertConfig = {};
            alertConfig.type = 'EditAlertsRequest';
            alertConfig.list = [alertObj];

            alertsService.editAlert(alertConfig).success(function(){

                $scope.dataTableObj.refreshDataTable(false);
                commonUtilityService.showSuccessNotification("Action has been updated");

                analytics.sendEvent({
                    category: "Operations",
                    action: "Edit Enterprise Spend Action",
                    label: "Actions"
                });

            }).error(function(data){
                console.log('update alert failure' + data);
                alertsCommonFactory.showError(data,'ACCOUNT_CUMULATIVE');
            });
        };
    }
})();