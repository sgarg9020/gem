'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('SIMAlertsCtrl', SIMAlerts);

    SIMAlerts.$inject = ['$scope', 'loadingState'];

    /**
     @constructor SIMAlerts()
     @param {object} $scope - Angular.js $scope
     @param {object} loadingState - Factory Method
     */
    function SIMAlerts($scope, loadingState) {

        var SIMAlerts = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            // ng-if to show information portlet
            SIMAlerts.showPortlet = true;
        }
        activate();

    }
})();