'use strict';

(function(){
    angular
        .module('app')
        .factory('alertsService', AlertsService);

    AlertsService.$inject = ["commonUtilityService"];

    /**
     @function AlertsService()
     */
    function AlertsService(commonUtilityService){

        return {
            getAlerts: getAlerts,
            getAlert:getAlert,
            deleteAlerts:deleteAlerts,
            addAlert: addAlert,
            editAlert: editAlert,
            getAlertsForCustomization:getAlertsForCustomization,
            getAllowedAlertActionType:getAllowedAlertActionType,
            getAlertHistory:getAlertHistory
        };

        /**
         * @function getAlerts()
         * @param {string} url - URL to get data from
         * @returns {future}
         */
        function getAlerts(queryParam,accountId,isBackgroundCall) {

            var accountId  = ( accountId !== undefined ) ? accountId : sessionStorage.getItem('accountId');
            var url = ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+ accountId +"/alerts";

            var headerAdd=['GET_ALERT'];
            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParam,undefined,undefined,isBackgroundCall);
        }

        function getAlert(alertId,isBackgroundCall){
            var accountId  = sessionStorage.getItem('accountId');
            var url = ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+ accountId +"/alerts/"+alertId;

            var headerAdd=['GET_ALERT'];
            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,undefined,undefined,undefined,isBackgroundCall);
        }
        /**
         * @function deleteAlerts()
         * @desc this function will delete all the selected alerts.
         */
        function deleteAlerts( alertIds, accountId ){

            var accountId  = ( accountId !== undefined  ) ? accountId : sessionStorage.getItem('accountId');
            var headerAdd=['DELETE_ALERT'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+sessionStorage.getItem('accountId')+"/alerts?alertIDs="+alertIds, 'DELETE',headerAdd);
        }

        /**
         * @function addAlert()
         * @desc validates and adds an alert to the account
         */
        function addAlert(alert){

            var accountId  = sessionStorage.getItem('accountId');
            var headerAdd=['ADD_ALERT'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE  + "accounts/account/" + accountId +"/alerts", 'POST',headerAdd,false,alert);
        }

        /**
         * @function editAlert()
         * @desc validates and edits an existing alert for the account
         */
        function editAlert(alert){

            var accountId  = sessionStorage.getItem('accountId');
            var headerAdd=['EDIT_ALERT'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE  + "accounts/account/" + accountId +"/alerts", 'PUT',headerAdd,false,alert);
        }


        /**
         * @function getAlertsForCustomization()
         * @desc get the cloned default alerts with new alert ids for the given accountId for customization
         */
        function getAlertsForCustomization(copyRequest){
            var headerAdd=['ADD_ALERT'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/" + sessionStorage.getItem( "accountId") + "/alerts/copy", 'POST',headerAdd,false,copyRequest);
        }

        function getAllowedAlertActionType(){
            var accountId  = accountId ? accountId : sessionStorage.getItem('accountId');
            var url = ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+ accountId +"/alertActions";
            var headerAdd=['GET_SUPPORTED_ALERT_ACTIONS'];

            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd);
        }

        function getAlertHistory(queryParam,accountId,isBackgroundCall){

            accountId  = ( accountId !== undefined ) ? accountId : sessionStorage.getItem('accountId');
            var url = ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/alertHistory";
            var headerAdd=['GET_ALERT'];
            var config = {
                noCancelOnRouteChange : true
            };

            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParam,undefined,config,isBackgroundCall);
        }
    }
})();