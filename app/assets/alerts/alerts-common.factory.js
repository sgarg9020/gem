'use strict';

(function(){
    angular
        .module('app')
        .factory('alertsCommonFactory', AlertsCommonFactory);

    AlertsCommonFactory.$inject = ['alertsService','commonUtilityService', 'analytics','pollingAPIService'];

    /**
     * @function AlertsCommonFactory()
     * @param alertsService
     * @param commonUtilityService
     * @param analytics
     * @returns {{setScope: setScope, getScope: getScope, confirmAlertsEnable: confirmAlertsEnable, confirmAlertsDisable: confirmAlertsDisable, confirmAlertsDeletion: confirmAlertsDeletion, confirmAlertCustomize: confirmAlertCustomize, confirmAlertRevert: confirmAlertRevert, showAlertModal: showAlertModal, getAlertRequestObj: getAlertRequestObj, showError: showError, getAlertTargetHTML: getAlertTargetHTML, getAlertDescHTML: getAlertDescHTML, getReconfigureHTML: getReconfigureHTML, setAlertCustomized: setAlertCustomized}}
     * @constructor
     */
    function AlertsCommonFactory(alertsService,commonUtilityService, analytics,pollingAPIService){

        var scope = {};
        var accountId = sessionStorage.getItem('accountId');
        return {
            setScope: setScope,
            getScope: getScope,
            confirmAlertsEnable: confirmAlertsEnable,
            confirmAlertsDisable: confirmAlertsDisable,
            confirmAlertsDeletion: confirmAlertsDeletion,
            confirmAlertCustomize:confirmAlertCustomize,
            confirmAlertRevert:   confirmAlertRevert,
            showAlertModal: showAlertModal,
            getAlertRequestObj: getAlertRequestObj,
            showError: showError,
            getAlertTargetHTML:getAlertTargetHTML,
            getAlertDescHTML:getAlertDescHTML,
            getReconfigureHTML:getReconfigureHTML,
            setAlertCustomized: setAlertCustomized,
            monitorAlertStatus:monitorAlertStatus,
            pollStop:pollStop,
            pollCallback:pollCallback,
            getActionName:getActionName,
            displayNotCustomizedError:displayNotCustomizedError
        };

        /**
         * @function setScope()
         * @param {object} obj - scope from controller
         * @desc mutator for scope public variable
         */
        function setScope(obj){
            if(typeof obj !== "object")
                return console.error("AlertsService: Please pass a valid scope for method setScope");
            scope = obj;
        }

        /**
         * @function getScope()
         * @desc accessor for scope public variable
         * @returns {object} scope - scope variable from AlertsService
         */
        function getScope(){
            return scope;
        }

        /**
         * @desc formats error message based on errorInt and shows notification
         * @param data
         * @param alertType
         */
        function showError(data,alertType,isGroupAlert){
            if(data.errorInt == 4024){
                if(alertType == 'DEFAULT_PER_SIM_PER_ACCOUNT' || alertType == 'ACCOUNT_CUMULATIVE' || alertType == 'OVERRIDE_PER_SIM'){
                    commonUtilityService.showErrorNotification("You already have 3 actions created for this " + ((alertType == 'OVERRIDE_PER_SIM') ? 'sim.' : isGroupAlert ? 'group.' : 'account.') + "You must delete an existing action in order to create a new one.");
                }else if(alertType == 'PER_SIM_PER_ZONE'){
                    commonUtilityService.showErrorNotification("You already have 3 actions created for this zone. You must delete an existing action in order to create a new one.");
                }else if(alertType == 'ZONE_CUMULATIVE'){
                    commonUtilityService.showErrorNotification("You already have 3 actions created for this zone. You must delete an existing action in order to create a new one.");
                }
            }else if(data.errorInt == 4041){
                commonUtilityService.showErrorNotification("You can have only one DEACTIVATE action.");
            }else if(data.errorInt == 4022){
                if(data.conflictingAlertList){
                    if(data.conflictingAlertList.length == 1){
                        var alert = data.conflictingAlertList[0].alertSpecification;
                        var msg = "This is in conflict with action(s): <br/> - " + getActionName(alert.actions[0]) + " : Limit is ";

                        if(alertType == 'ACCOUNT_CUMULATIVE' || alertType == 'ZONE_CUMULATIVE'){
                            msg = msg + getLocalCurrencySign(sessionStorage.getItem( "gs-enterpriseCurrency")) + alert.limitValue;
                        }else{
                            msg = msg +  alert.limitValue + "MB";
                        }
                    }else if(data.conflictingAlertList.length == 2){
                        var conflictingAlert1 = data.conflictingAlertList[0].alertSpecification;
                        var conflictingAlert2 = data.conflictingAlertList[1].alertSpecification;

                        var msg = "This is in conflict with action(s): <br/> - " + getActionName(conflictingAlert1.actions[0]) + " : Limit is ";

                        msg = getMsgWithLimitUnit(alertType, msg, conflictingAlert1);

                        msg = msg + " <br/> - " + getActionName(conflictingAlert2.actions[0]) + " : Limit is ";

                        msg = getMsgWithLimitUnit(alertType, msg, conflictingAlert2);
                    }

                    commonUtilityService.showErrorNotification(msg);

                }
            }else{
                commonUtilityService.showErrorNotification(data.errorStr);
            }
        }

        /**
         * Formats error message with proper limit unit based on alert type
         * @param alertType
         * @param msg
         * @param alert
         * @returns {*}
         */
        function getMsgWithLimitUnit(alertType, msg, alert) {
            if (alertType == 'ACCOUNT_CUMULATIVE' || alertType == 'ZONE_CUMULATIVE') {
                msg = msg + getLocalCurrencySign(sessionStorage.getItem( "gs-enterpriseCurrency")) + alert.limitValue;
            } else {
                msg = msg + alert.limitValue + "MB";
            }
            return msg;
        }

        function displayNotCustomizedError(){
            commonUtilityService.showErrorNotification("Default actions can't be altered.<br/> In order to override or add new action, click on customize button shown below.");
        }

        /**
         * @function confirmAlertsEnable()
         * @desc this function will set the verification modal and verify
         * that the user wanted to enable the selected alerts.
         */
        function confirmAlertsEnable(rows){

            var originalLen = rows.length;

            //  check whether the alerts are already enabled
            commonUtilityService.filterSelectedRows(rows,function(row){

                return row.alertSpecification.enable;

            });

            if( rows.length == 0 ) {
                if (originalLen > 1) {
                    commonUtilityService.showErrorNotification("Selected actions are already enabled");
                    return;
                }

                else if (originalLen == 1) {
                    commonUtilityService.showErrorNotification("Selected action is already enabled");
                    return;
                }
            }


            var func = function(){_enableAlerts(rows);};

            // Verification Modal
            scope.verificationModalObj.updateVerificationModel({
                header: 'Enable Actions',
                cancel: 'Cancel',
                submit: 'Enable'
            }, func);
            if(rows.length>1){
                scope.verificationModalObj.addVerificationModelMessage("Enabling these actions can affect multiple SIMs, possibly deactivating them or triggering warning thresholds. If you are unsure, press cancel and review user's guide for more details.");
            }else{
                scope.verificationModalObj.addVerificationModelMessage("Enabling this action can affect multiple SIMs, possibly deactivating them or triggering warning thresholds. If you are unsure, press cancel and review user's guide for more details.");
            }

            $("#verificationModal").modal("show");
        }

        /**
         * @function confirmAlertsDisable()
         * @desc this function will set the verification modal and verify
         * that the user wanted to disable the selected alerts.
         */
        function confirmAlertsDisable(rows){

            var originalLen = rows.length;

            //  check whether the alerts are already disabled
            commonUtilityService.filterSelectedRows(rows,function(row){

                return !row.alertSpecification.enable;

            });

            if( rows.length == 0 ) {
                if (originalLen > 1) {
                    commonUtilityService.showErrorNotification("Selected actions are already disabled");
                    return;
                }

                else if (originalLen == 1) {
                    commonUtilityService.showErrorNotification("Selected action is already disabled");
                    return;
                }
            }


            var func = function(){_disableAlerts(rows);};

            // Verification Modal
            scope.verificationModalObj.updateVerificationModel({
                header: 'Disable Actions',
                cancel: 'Cancel',
                submit: 'Disable'
            }, func);
            if(rows.length>1){
                scope.verificationModalObj.addVerificationModelMessage("Disabling these actions can affect multiple SIMs, possibly reactivating them if previously deactivated by the action(s) or eliminating warning thresholds. If you are unsure, press cancel and review user's guide for more details.");
            }else{
                scope.verificationModalObj.addVerificationModelMessage("Disabling this action can affect multiple SIMs, possibly reactivating them if previously deactivated by the action(s) or eliminating warning thresholds. If you are unsure, press cancel and review user's guide for more details.");
            }

            $("#verificationModal").modal("show");
        }

        /**
         * @function confirmAlertsDeletion()
         * @desc this function will set the verification modal and verify
         * that the user wanted to delete the selected alerts.
         */
        function confirmAlertsDeletion(rows){

            var func = function(){_deleteAlerts(rows);};

            // Verification Modal
            scope.verificationModalObj.updateVerificationModel({
                header: 'Delete Actions',
                cancel: 'Cancel',
                submit: 'Delete'
            },func);
            if(rows.length>1){
                scope.verificationModalObj.addVerificationModelMessage("Deleting these actions can affect multiple SIMs, possibly reactivating them if previously deactivated by the action(s) or eliminating warning thresholds. If you are unsure, press cancel and review user's guide for more details.");
            }else {
                scope.verificationModalObj.addVerificationModelMessage("Deleting this action can affect multiple SIMs, possibly reactivating them if previously deactivated by the action(s) or eliminating warning thresholds. If you are unsure, press cancel and review user's guide for more details.");
            }


            $("#verificationModal").modal("show");
        }

        function confirmAlertCustomize(isGroupAlert){

            var func = function(){ _customizeAlerts(); };

            scope.verificationModalObj.updateVerificationModel({

                header:"Customize Actions",
                cancel: "Cancel",
                submit:"Customize"

            }, func);

            var message = isGroupAlert ? 'This allows you to change actions for the chosen group only.' : 'This allows you to change actions for the chosen SIM only.';
            scope.verificationModalObj.addVerificationModelMessage(message);
            $("#verificationModal").modal("show");
        }

        function confirmAlertRevert(alertList,isGroupAlert){
            var func = function(){ _revertCustomAlerts(alertList); };

            scope.verificationModalObj.updateVerificationModel({

                header: "Revert Actions",
                cancel: "Cancel",
                submit: "Revert"
            }, func);
            var message = isGroupAlert ? 'This reverts all SIMs in the chosen Group to default action configuration. (If defaults not set, all action settings will be removed.)' : 'This reverts chosen SIMs to default action configuration. (If defaults not set, all action settings will be removed.)';
            scope.verificationModalObj.addVerificationModelMessage(message);
            $("#verificationModal").modal("show");

        }

        function _revertCustomAlerts(alertList) {

            _deleteAlerts(alertList, true);

        }

        /**
         * @function showAlertModal()
         * @param {boolean} isNew - truthy value which determines to add or edit an alert
         * @param {boolean} isSpendAlert - truthy value which determines to show spend or usage inputs
         * @param {boolean} isZoneAlert - truthy value which determines to show the zone dropdown
         * @param {boolean} isGroupAlert - truthy value which determines to show the group dropdown
         * @param {object} editData - payload for the specific alert to be edited.
         * @desc opens a modal which allows the user to add/edit a new modal.
         */
        function showAlertModal(config, editData){

            if(!config.isNew){

                scope.alertModalObj.updateAlertModal(editData,{
                    header: 'Edit Action',
                    cancel: 'Cancel',
                    submit: 'Edit Action'
                },scope.editAlert);
            }

            else{
                if(config.alertType == 'ACCOUNT_CUMULATIVE' || config.alertType == 'DEFAULT_PER_SIM_PER_ACCOUNT' || config.alertType == 'OVERRIDE_PER_SIM'){

                    var table = $("#"+scope.dataTableObj.tableID+"").DataTable();
                    var rowsLength = table.data().length;

                    if(rowsLength >= 3 ){
                        commonUtilityService.showWarningNotification("You already have 3 actions created for this " + ((config.alertType == 'OVERRIDE_PER_SIM') ? 'sim.' : config.isGroupAlert ? 'group.' : 'account.') + "You must delete an existing action in order to create a new one.");
                        return;
                    }
                }

                scope.alertModalObj.updateAlertModal(null,{
                    header: 'Add Action',
                    cancel: 'Cancel',
                    submit: 'Add Action'
                }, scope.addAlert);
            }

            if((config.isZoneAlert && !editData) || (config.isZoneAlert && editData && editData.alertSpecification && getActionName(editData.alertSpecification.actions[0]) !== 'Warning')){
                scope.alertModalObj.setPatterns(/[0-9].*/,/[0-9].*/);
            }
            // Show Spend
            scope.alertModalObj.setShowSpend(config.isSpendAlert||false);

            // Show Zones
            scope.alertModalObj.setShowZones(config.isZoneAlert||false);
            $("#alertModal").modal("show");
        }




        /**
         * Maps ui modal with request object and returns it to save in db
         * @param alert
         * @param alertType
         * @param limitType
         * @returns {{}}
         */
        function getAlertRequestObj(alert,alertType,limitType,groupId) {
            var alertObj = {};
            alertObj.type = 'AlertConfiguration';
            alertObj.accountId = groupId ? groupId : sessionStorage.getItem('accountId');
            alertObj.alertType = alertType;
            if(alert.zone){
                alertObj.zoneId = alert.zone.zoneId;
            }
            if(alert.simId){
                alertObj.simId = alert.simId;
            }
            var action = _getActionNameForService(alert.action);
            alertObj.alertSpecification = {
                'type' : 'AlertSpecification',
                'enable': (alert.isEnabled !=undefined) ? alert.isEnabled : true,
                'limitValue': alert.allocation,
                'limitType': limitType,
                'actions': [action]
            };

            return alertObj;
        }

        function _getActionNameForService(action){
            if(action == 'Warning'){
                action = 'EMAIL'
            }else if(action == 'Deactivate'){
                action = 'DISABLE'
            }
            return action;
        }

        function getActionName(action){
            if(action == 'EMAIL'){
                action = 'Warning'
            }else if(action == 'DISABLE'){
                action = 'Deactivate'
            }
            return action;
        }

        /**
         * Returns update request list for enable/disable alerts api
         * @param rows
         * @param enableKey
         * @returns {Array}
         */
        function _processRowsToEnableOrDisable(rows,enableKey){
            var processedList = [];
            for( var i =0; i < rows.length; i++ ){

               var row = rows[i];

                var alertObj = {};
                alertObj.type = 'AlertConfiguration';
                alertObj.alertId = row.alertId;
                alertObj.alertSpecification = {
                    'type' : 'AlertSpecification',
                    'enable': enableKey
                };
                processedList.push(alertObj);
            }
            return processedList;
        }

        /**
         * Returns update request list for delete alerts api
         * @param rows
         * @returns {*}
         */
        function _getSelectedAlertsListForDeletion(rows){
            var alertIds = rows[0].alertId;

            for(var i=1; i<rows.length ; i++){
                alertIds = alertIds + ',' + rows[i].alertId;
            }

            return alertIds;
        }


        function _enableAlerts(rows){

            var processedList = _processRowsToEnableOrDisable(rows,true);

            var alertConfig = {type: "EditAlertsRequest", list: processedList};

            alertsService.editAlert(alertConfig).success(function(){
                var action = 'Enable ' + _getUserReadableActionName(rows[0].alertType) ;
                if(rows.length>1){
                    action = action + " Actions"
                    commonUtilityService.showSuccessNotification("Actions have been enabled");
                }else{
                    action = action + " Action"
                    commonUtilityService.showSuccessNotification("Action has been enabled");
                }
                scope.dataTableObj.refreshDataTable();

                analytics.sendEvent({
                    category: "Operations",
                    action: action,
                    label: "Actions"
                });


            }).error(function(data,status){
                console.log('enable alert failure' + data);
                commonUtilityService.showErrorNotification(data.errorStr);

            });
        }

       function _disableAlerts(rows){

           var processedList = _processRowsToEnableOrDisable(rows,false);

            var alertConfig = {type: "EditAlertsRequest", list: processedList};

            alertsService.editAlert(alertConfig).success(function(response){
                var action = 'Disable ' + _getUserReadableActionName(rows[0].alertType) ;
                if(rows.length>1){
                    action = action + " Actions"
                    commonUtilityService.showSuccessNotification("Actions have been disabled");
                }else {
                    action = action + " Action"
                    commonUtilityService.showSuccessNotification("Action has been disabled");
                }
                scope.dataTableObj.refreshDataTable();

                analytics.sendEvent({
                    category: "Operations",
                    action: action,
                    label: "Actions"
                });


            }).error(function(data,status){
                console.log('disable alert failure' + data);
                commonUtilityService.showErrorNotification(data.errorStr);

            });
        }

        function _deleteAlerts(rows, revertFlag ){

            var revertFlag = revertFlag ? revertFlag : false;

            var alertIds = _getSelectedAlertsListForDeletion(rows);

            var accId = scope.accountId ? scope.accountId : sessionStorage.getItem('accountId');
            alertsService.deleteAlerts( alertIds,  accId  ).success(function(){
                var actionName = _getUserReadableActionName(rows[0].alertType) ;
                var action = '';
                if( revertFlag ) {
                    scope.alertCustomized = false;
                    commonUtilityService.showSuccessNotification("Default values have been applied");
                    action = "Revert to default " + actionName + " actions";
                }
                else if(rows.length>1){
                    action = 'Delete ' + actionName + " Actions"
                    commonUtilityService.showSuccessNotification("Actions have been deleted");
                }else {
                    action = 'Delete ' + actionName + " Action"
                    commonUtilityService.showSuccessNotification("Action has been deleted");
                }
                scope.dataTableObj.refreshDataTable();

                analytics.sendEvent({
                    category: "Operations",
                    action: action,
                    label: "Actions"
                });
            }).error(function(data,status){
                console.log('delete alerts failure' + data);
                commonUtilityService.showErrorNotification(data.errorStr);

            });
        }

        function _customizeAlerts(){

            var alertType = (scope.alertType == 'OVERRIDE_PER_SIM') ? 'DEFAULT_PER_SIM_PER_ACCOUNT' : scope.alertType;

            var copyRequest = {
                "type" : "AlertCopyRequest",
                "alertTypeList" : [alertType],
                "sourceDetails" :   {
                    "type" : "CopyDetails",
                },
                "destinationDetails" : {
                    "type" : "CopyDetails"
                }
            }
            if(scope.alertType == 'OVERRIDE_PER_SIM'){
                copyRequest.sourceDetails.accountId = scope.SIMDetail.account.accountId;
                copyRequest.destinationDetails.simIccId = scope.simId;
            }else{
                copyRequest.sourceDetails.accountId = sessionStorage.getItem('accountId')
                copyRequest.destinationDetails.accountId = scope.groupId;
            }

            alertsService.getAlertsForCustomization(copyRequest).success(function(response ) {
                scope.alertCustomized = true;
                scope.dataTableObj.refreshDataTable();
                commonUtilityService.showSuccessNotification("Actions have been customized");
                var actionName = _getUserReadableActionName(scope.alertType) ;
                var action = "Customize " + actionName + " actions";
                analytics.sendEvent({
                    category: "Operations",
                    action: action,
                    label: "Actions"
                });

            }).error( function( data ) {
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

        /**
         * It will set the boolean variable alertCustomized based on alertList
         * @param alertList
         */
        function setAlertCustomized(alertList) {
            scope.alertCustomized = false;
            if (alertList.length >= 1) {
                var alertAccountId = alertList[0].accountId;

                var accountId = sessionStorage.getItem("accountId");

                if (accountId != alertAccountId) {
                    scope.alertCustomized = true;
                }
            }
        }

        function getAlertTargetHTML(data, type, row) {
            var alertLevel = "";
            var specType;
            var specItem;

            switch(row.alertType){
                case "ACCOUNT_CUMULATIVE":
                case "ZONE_CUMULATIVE":
                    alertLevel = "Account";
                    if(row.zoneId)
                        specItem = row.zoneNickName || row.zoneName;
                    else
                        specItem = row.accountName;
                    break;
                case "DEFAULT_PER_SIM_PER_ACCOUNT":
                    specType = "CID";
                    specItem = row.cId;
                    // Is this a group alert?
                    if(row.accountId != accountId){
                        alertLevel = "Group - "+row.accountName;
                    }
                    else{
                        alertLevel = "Account";
                    }
                    break;
                case "PER_SIM_PER_ZONE":
                    specType = "CID";
                    specItem = row.cId;
                    // Is this a group alert?
                    if(row.accountId != accountId)
                        alertLevel = "Group - "+row.accountName;
                    else
                        alertLevel = "Account";
                    alertLevel += " - "+(row.zoneNickName || row.zoneName);
                    break;
                case "OVERRIDE_PER_SIM":
                    specType = "CID";
                    specItem = row.cId;
                    alertLevel = "SIM";
                    break;
            }

            if(specItem && specType)
                specType += " - "+specItem;
            else if(!specType)
                specType = specItem;
            return "<div class='alert-level-id'>"+alertLevel+"</div><div class='alert-target-id'>"+specType+"</div>";
        }

        function _getUserReadableActionName(alertType) {
            var action = "";

            switch (alertType) {
                case "ACCOUNT_CUMULATIVE":
                    action = "Enterprise Spend";
                    break;
                case "ZONE_CUMULATIVE":
                    action = "Zone Spend";
                    break;
                case "DEFAULT_PER_SIM_PER_ACCOUNT":
                    action = "Per SIM Default";
                    break;
                case "PER_SIM_PER_ZONE":
                    action = "Per Zone Per SIM Default";
                    break;
                case "OVERRIDE_PER_SIM":
                    action = "Customized SIM Action";
                    break;
            }

            return action;
        }

        function getAlertDescHTML(data, type, row) {

            return _getUserReadableActionName(row.alertType);
        }

        function getReconfigureHTML(data, type, row) {

            var link = "";
            var queryParam = "'alertId="+row.alertId;

            switch(row.alertType){
                case "ACCOUNT_CUMULATIVE":
                    link = "/app/alerts/account/enterprise-spend";
                    break;
                case "ZONE_CUMULATIVE":
                    link = "/app/alerts/account/zone-spend";
                    queryParam += '&zoneId='+row.zoneId;
                    break;
                case "DEFAULT_PER_SIM_PER_ACCOUNT":
                    if(row.accountId != accountId)
                        link = "/app/alerts/group/"+row.accountId+"/per-sim-defaults";
                    else
                        link = "/app/alerts/account/per-sim-defaults";
                    break;
                case "PER_SIM_PER_ZONE":
                    if(row.accountId != accountId)
                        link = "/app/alerts/group/"+row.accountId+"/per-sim-by-zone-defaults";
                    else
                        link = "/app/alerts/account/per-sim-by-zone-defaults";
                    queryParam += '&zoneId='+row.zoneId;
                    break;
                case "OVERRIDE_PER_SIM":
                    link = "/app/alerts/sim/"+row.simId;
                    break;
            }
            return "<a class='cursor' style=margin-left:40% ng-click=\"openView('"+link+"',"+queryParam+"')\"><i class='fa fa-gear'></i></a>";
        }

        /**
         * This function registers getalerts api with interval of 1 min and checks for every alert alertHistoryActionStatus.
         * If any alert alertHistoryActionStatus is in progress,then polling service will be called.
         * @param currentQueryParam
         * @param subPath
         */
        function monitorAlertStatus(currentQueryParam,subPath){
            currentQueryParam.noPace = true;
            alertsService.getAlerts(currentQueryParam,undefined,true).success(function(response){

                for(var i=0 ; i < response.list.length ; i++){
                    var alert = response.list[i];

                    if(alert.alertHistoryActionStatus== "IN_PROGRESS"){
                        scope.isDisableComponent[alert.alertId] = true;
                        pollingAPIService.monitor(alert.alertId, alertsService.getAlert, pollStop, pollCallback);
                    }
                }
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

        function pollStop(response) {
            return response.alertHistoryActionStatus !== "IN_PROGRESS";
        }

        function pollCallback(response) {
            if (response.alertHistoryActionStatus !== "IN_PROGRESS") {
                scope.isDisableComponent[response.alertId] = false;
            }
            scope.alerts[response.alertId] = response.triggerCount;
        }
    }
})();
