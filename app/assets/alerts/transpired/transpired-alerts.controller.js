'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('TranspiredAlertsCtrl', TranspiredAlerts);

    TranspiredAlerts.$inject = ['$scope', '$sce', '$compile', 'loadingState', 'dataTableConfigService', 'alertsService', 'commonUtilityService','alertsCommonFactory','$filter','$location'];

    /**
     @constructor TranspiredAlerts()
     @param {object} $scope - Angular.js $scope
     @param {object} $sce - Angular.js $sce
     @param {object} $compile - Angular.js $compile provider
     @param {object} loadingState - Factory Method
     @param {object} dataTableConfigService - Factory Method
     @param {object} alertsService - Factory Method
     @param {object} commonUtilityService - Factory Method
     */
    function TranspiredAlerts($scope, $sce, $compile, loadingState, dataTableConfigService, alertsService, commonUtilityService,alertsCommonFactory,$filter,$location) {

        var transpiredAlerts = this;
        var alertId,simId;
        /**
         * Start up logic for controller
         */
        function activate(){

            // Filters for the table
            $scope.alertTypeFilters = [{
                name: "All Alerts",
                dbRef:"",
                skipQueryParam:true
            },{
                name: "Account Alerts",
                dbRef:"ACCOUNT"
            },{
                name: "Zone Alerts",
                dbRef:"ZONE"
            },{
                name: "SIM Alerts",
                dbRef:'SIM'
            }];

            // Functions for bulk options
            transpiredAlerts.confirmAlertsEnable = alertsCommonFactory.confirmAlertsEnable;
            transpiredAlerts.confirmAlertsDisable = alertsCommonFactory.confirmAlertsDisable;
            transpiredAlerts.confirmAlertsDeletion = alertsCommonFactory.confirmAlertsDeletion;

            // Strict contextual escaping
            transpiredAlerts.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};

            // Prepare alertsService with controller scope
            alertsCommonFactory.setScope($scope);

            var searchObj = $location.search();
            var alertTypeFilter = null;

            if(searchObj.alertTypeIndex){
                alertTypeFilter = $scope.alertTypeFilters[searchObj.alertTypeIndex];
            }else if(searchObj.alertType){
                for(var i=0;i<$scope.alertTypeFilters.length;i++){
                    if($scope.alertTypeFilters[i].dbRef == searchObj.alertType)
                    {
                        alertTypeFilter = $scope.alertTypeFilters[i];
                        break;
                    }
                }
            }

            var defaultAlertTypeFilter = alertTypeFilter?alertTypeFilter: $scope.alertTypeFilters[0];

            if(searchObj.alertId){
                alertId = searchObj.alertId;
            }
            if(searchObj.simId){
                simId = searchObj.simId;
            }

            // DataTables
            $scope.emptyTableMessage = "No alerts available.";
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = "transpiredAlertsDataTable";
            $scope.dataTableObj.tableHeaders = [
                {
                    value: "Action Target",
                    dbValue: ""
                },
                {
                    value: "Action Type",
                    dbValue: ""
                },
                {
                    value: "Limit",
                    dbValue: ""
                },
                {
                    value: "Action Outcome",
                    dbValue: ""
                },
                {
                    value: "Date",
                    dbValue: ""
                },
                {
                    value: "Reconfigure",
                    dbValue: ""
                }
            ];


            $scope.refreshAlerts = function(){
                var selectedFilter = $scope.dataTableObj.filters.alertType;
                var storedFilter = dataTableConfigService.getFilter('alertType','url',$scope.dataTableObj.tableID);

                if(selectedFilter.dbRef != storedFilter.dbRef || alertId){
                    $location.search('alertTypeIndex', null);
                    $location.search('alertType', null);
                    $location.search('alertId', null);
                    $location.search('simId', null);
                    alertId = null;
                    simId = null;
                    $scope.dataTableObj.refreshDataTable(false);
                    dataTableConfigService.setFilter('alertType', selectedFilter, 'url',$scope.dataTableObj.tableID);

                }

            };

            var filtersConfig = { alertType: {type:'url', dbParam: 'filter', defaultValue:defaultAlertTypeFilter ,handleFilter:$scope.refreshAlerts}};
            dataTableConfigService.configureFilters($scope.dataTableObj, filtersConfig);

            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);


        }
        activate();


        function _setDataTableOptions(){
            return {
                columns: [
                    {
                        width: "",
                        orderable: false
                    },
                    {
                        width: "",
                        orderable: false
                    },
                    {
                        width: "",
                        orderable: false
                    },
                    {
                        width: "",
                        orderable: false
                    },
                    {
                        width: "",
                        orderable: false
                    },
                    {
                        width: "20px",
                        orderable: false
                    }
                ],
                ordering: false,
                columnDefs: [
                    {
                        targets: 0,
                        render: alertsCommonFactory.getAlertTargetHTML
                    },
                    {
                        targets: 1,
                        render: alertsCommonFactory.getAlertDescHTML
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            var rowData = '';
                            if (row.limitType === "COST") {
                                rowData = getLocalCurrencySign(sessionStorage.getItem("gs-enterpriseCurrency")) + row.limitValue;
                            } else {
                                rowData = row.limitValue + ' MB';
                            }
                            return '<span>' + rowData + '</span>';
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            // Array of actions - Last action is the one to display
                            var action = alertsCommonFactory.getActionName(row.action);
                            var actualAction = row.action.toLowerCase();
                            return '<div class="nowrap"><span class="status ' + actualAction + '"></span><span class="status-text">' + action + '</span></div>';
                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return $filter('gsDate')(row.occurredTime, "MM/dd/yyyy 'at' h:mma");
                        }
                    },
                    {
                        targets: 5,
                        render: alertsCommonFactory.getReconfigureHTML
                    }
                ],
                createdRow: function (row, data, index) {
                    $(row).attr("id", 'ALERTID_' + data.alertId);

                    $compile(row)($scope);
                    if (data.alertId == alertId) {
                        $(row).addClass('highlighted-row');
                    }
                },
                fnServerData: function (sSource, aoData, fnCallback) {

                    var queryParam = dataTableConfigService.prepareRequestParams(aoData, $scope.dataTableObj);

                    if (simId) {
                        queryParam.simId = simId;
                    }
                    $scope.row = [];

                    alertsService.getAlertHistory(queryParam).success(function (response) {

                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        //angular.element('#checkbox_action').prop('checked', false);
                        var tableData = dataTableConfigService.prepareResponseData(response, oSettings, $scope.emptyTableMessage);

                        // Store the data to the scope
                        $scope.transpiredAlertsList = tableData.aaData;

                        // Call the dataTable with the appropriate data
                        if (oSettings != null)
                            fnCallback(tableData);
                        else
                            loadingState.hide();

                    }).error(function (data, status) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings, fnCallback, "alerts");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });
                },
                "fnDrawCallback": function (oSettings) {
                    // dataTableConfigService.disableTableHeaderOps($scope.transpiredAlertsList.length, oSettings);
                    commonUtilityService.preventDataTableActivePageNoClick($scope.dataTableObj.tableID);
                },
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ",
                    "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    "sEmptyTable": $scope.emptyTableMessage,
                    "sZeroRecords": $scope.emptyTableMessage
                }
            };
        }
    }
})();