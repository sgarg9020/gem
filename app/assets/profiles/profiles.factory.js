'use strict';

(function () {
    angular
        .module('app')
        .factory('ProfileService', ['commonUtilityService', ProfileService]);

    function ProfileService(commonUtilityService) {

        var factory = {
            getProfilesOfAccount: getProfilesOfAccount,
            getEuiccProfilesOfAnEid: getEuiccProfilesOfAnEid,
            getExportDataUrl: getExportDataUrl,
            getProfileDetails: getProfileDetails,
            getDisplayableProfileType: getDisplayableProfileType,
            getDisplayableProfilePackage: getDisplayableProfilePackage,
            getDisplayableDeploymentStatus: getDisplayableDeploymentStatus,
            getDisplayableOperationType: getDisplayableOperationType,
            enableProfile: enableProfile,
            disableProfile: disableProfile,
            changeProfileStatusOfEsim: changeProfileStatusOfEsim
        };

        var profileTypes = {
            SINGLE_IMSI : "Single-IMSI",
            MULTI_IMSI : "Multi-IMSI"
        };
        var profilePackages = {
            eSIM_M2M : "eSIM - M2M",
            eSIM_CONSUMER : "eSIM - Consumer",
            pSIM_FIXED: "pSIM - Fixed",
            pSIM_REMOVABLE: "pSIM - Removable"
        };
        var deploymentStatus = {
            ASSIGNED : "Assigned",
            AVAILABLE : "Available",
            DOWNLOADED : "Downloaded",
            DOWNLOAD_FAILED : "Download - Failed",
            INSTALLED : "Installed",
            INSTALLED_ENABLED : "Installed - Enabled",
            INSTALLED_DISABLED : "Installed - Disabled",
            INSTALL_FAILED : "Install - Failed",
            DELETED : "Deleted"
        };
        var operationTypes = {
            AUDIT_EIS: "Audit EIS",
            ENABLE_PROFILE: "Enable Profile",
            DISABLE_PROFILE: "Disable Profile"
        }

        return factory;

        function getDisplayableOperationType(type) {
            return operationTypes[type];
        }

        function getDisplayableProfileType(type) {
            return profileTypes[type];
        }

        function getDisplayableProfilePackage(profilePackage) {
            return profilePackages[profilePackage];
        }

        function getDisplayableDeploymentStatus(status) {
            return deploymentStatus[status];
        }

        function getProfilesOfAccount(queryParam, accountId) {
            var accountId = (accountId !== undefined) ? accountId : sessionStorage.getItem('accountId');
            var headerAdd = ['PROFILE_READ'];
            var config = {
                cancellable: 'searchApi'
            };
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/" + accountId + "/profiles", 'GET', headerAdd, queryParam, undefined, config);
        }

        function getEuiccProfilesOfAnEid(aId, eid) {
            var accountId = (aId !== undefined) ? aId : sessionStorage.getItem('accountId');
            var headerAdd = ['PROFILE_READ'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/" + accountId + "/sim/" + eid + "/profiles", 'GET', headerAdd);
        }

        function getExportDataUrl(accId) {
            var accountId = accId ? accId : sessionStorage.getItem('accountId');
            var exportDataUrl = ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/profiles?";
            return exportDataUrl;
        }

        function getProfileDetails(accountId, iccid) {
            var headerAdd = ['PROFILE_READ'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/" + accountId + "/profiles/" + iccid, 'GET', headerAdd);
        }

        function enableProfile(accountId, eid, iccid) {
            return updateStatus(accountId, eid, iccid, 'ENABLE');
        }

        function disableProfile(accountId, eid, iccid) {
            return updateStatus(accountId, eid, iccid, 'DISABLE');
        }
        function updateStatus(accountId, eid, iccid, operationType) {
            var headerAdd = ['PROFILE_STATUS_EDIT'];
            var data = {
                type : 'ProfileStatus',
                status : operationType,
                iccId : iccid
            };
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/" + accountId + "/sim/" + eid + "/operation/status", 'PUT', headerAdd, false, data);
        }

        function changeProfileStatusOfEsim(accountId, simId) {
            var headerAdd = ['PROFILE_STATUS_EDIT'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/" + accountId + "/sim/" + simId, 'PUT', headerAdd, false, null);
        } 

    }
})();

