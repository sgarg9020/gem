'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('ProfilesCtrl', Profiles);

    Profiles.$inject = ['$scope','$rootScope','$compile','dataTableConfigService','ProfileService', 'commonUtilityService','loadingState','$sanitize','analytics'];

    function Profiles($scope,$rootScope,$compile,dataTableConfigService,ProfileService, commonUtilityService,loadingState,$sanitize,analytics) {

        var profiles = this;
        var filterValue;
        var statusMap = {INACTIVE:'inactive',ACTIVE:'active',BLOCKED:'blocked',UNBLOCKED:'unblocked'};
        /**
         * Start up logic for controller
         */
        function activate(){
            clearDataTableStatus('DataTables_profilesDataTable_');
            $scope.emptyTableMessage = "No Profiles available.";

            $scope.dataTableObj = {};

            $scope.dataTableObj.tableID = "profilesDataTable";
            $scope.dataTableObj.controlID = "datatable-controls";

            var filtersConfig = { selectedDisplayLength: {type:'local', dbParam: 'count', defaultValue:25} };
            dataTableConfigService.configureFilters($scope.dataTableObj, filtersConfig);

            profiles.exportData={
                url: ProfileService.getExportDataUrl()
            };

            $scope.dataTableObj.tableHeaders = [
                {"value": "ICCID", "DbValue":"iccid"},
                {"value": "Profile Type", "DbValue":"profileType"},
                {"value": "Profile Package", "DbValue":"profilePackage"},
                {"value": "Deployment Status", "DbValue":"deploymentStatus"},
                {"value": "Bootstrap Status","DbValue":"bootstrapStatus"},
                {"value": "EID","DbValue":"installed_eid"}
            ];
            $scope.dataTableObj.dataTableOptions = {
                order: [[ 0, "desc" ]],
                search: {
                    search: profiles.searchValue
                },
                createdRow: function (row,data) {
                    $(row).attr("id",'PID_'+data.iccid);
                    $compile(row)($scope);
                },
                fnServerData: function (sSource, aoData, fnCallback) {

                    if(aoData){
                        profiles.searchValue = aoData[5].value.value;
                    }
                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    $scope.row =[];

                    if(filterValue && filterValue != profiles.searchValue){
                        profiles.exportData.url = ProfileService.getExportDataUrl();
                    }

                    profiles.exportData.startIndex = queryParam.startIndex;
                    profiles.exportData.count = queryParam.count;
                    profiles.exportData.sortBy = queryParam.sortBy;
                    profiles.exportData.sortDirection = queryParam.sortDirection;
                    profiles.exportData.search = queryParam.search;

                    $scope.isDisable = false;
                    var profileResponse;
                    profileResponse = ProfileService.getProfilesOfAccount(queryParam);

                    profileResponse.success(function(response){
                        mapResponseToDataTable(response, queryParam, fnCallback);
                    }).error(function(data){
                        $scope.isDisable = !$scope.profilesList ||  $scope.profilesList.length == 0;
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"profiles");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });

                },
                columns:[
                    {
                        width:"100px",
                        orderable:true
                    },
                    {
                        width:"80px",
                        orderable:false
                    },
                    {
                        width:"80px",
                        orderable:false
                    },
                    {
                        width:"140px",
                        orderable:false
                    },
                    {
                        width:"80px",
                        orderable:false
                    },{
                        width:"150px",
                        orderable:false
                    }
                ],
                columnDefs: [{
                    "targets": 0,
                    "render": function ( data, type, row, meta) {
                        return row.iccid;
                    }
                },{
                    "targets": 1,
                    "render": function (data, type, row) {
                        var profileType = row.profileType ? ProfileService.getDisplayableProfileType(row.profileType) : '';
                        return profileType;
                    }
                },{
                    "targets": 2,
                    "render": function (data, type, row) {
                        var profilePackage = row.profilePackage ? ProfileService.getDisplayableProfilePackage(row.profilePackage) : '';
                        return profilePackage;
                    }
                },{
                    "targets": 3,
                    "render": function (data, type, row, meta) {
                        var deploymentStatus = row.deploymentStatus ? ProfileService.getDisplayableDeploymentStatus(row.deploymentStatus) : '';
                        return deploymentStatus;
                    }
                },{
                    "targets": 4,
                    "render": function (data, type, row) {
                        return row.bootstrapStatus ? 'Yes' : 'No';

                    }
                },{
                    "targets": 5,
                    "render": function (data, type, row) {
                        var eid = row.installed_eid ? row.installed_eid : '';
                        return eid;
                    }
                }],
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ",
                    "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    "sEmptyTable": $scope.emptyTableMessage,
                    "sZeroRecords":$scope.emptyTableMessage
                },
                "processing": true,
                "fnDrawCallback": function( oSettings ) {

                    if($scope.profilesList)
                        dataTableConfigService.disableTableHeaderOps( $scope.profilesList.length, oSettings );

                    commonUtilityService.preventDataTableActivePageNoClick( $scope.dataTableObj.tableID );

                }

            };

            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

        }

        function mapResponseToDataTable(response, queryParam, fnCallback) {
            var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
            var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);

            $scope.profilesList = datTabData.aaData;
            if ($scope.profilesList.length == 0) {
                $scope.isDisable = true;
            }
            if (oSettings != null) {
                fnCallback(datTabData);
            } else {
                loadingState.hide();
            }
        }

        $scope.isDisableComponent = function(){
            return $scope.isDisable;
        };

        activate();
        loadingState.hide();
    }
})();





