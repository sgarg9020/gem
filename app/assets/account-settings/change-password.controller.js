'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('ChangePasswordCtrl', ChangePassword);

    ChangePassword.$inject = ['$scope', '$location', 'commonUtilityService', 'loadingState', 'AccountService', 'AuthenticationService','analytics'];

    /**
     @constructor ChangePassword()
     */
    function ChangePassword($scope, $location, commonUtilityService, loadingState, AccountService, AuthenticationService,analytics){

        var changePassword = this;

        /**
         * @func activate()
         * @desc controller start up logic
         */
        function activate(){

            $scope.generalObj = {};
            
            changePassword.updatePW = {};
            changePassword.updatePW.errorMessage = '';
            changePassword.updatePW.updatePassword = updatePassword;
            changePassword.generalInfo = {};
            changePassword.isGSAdmin = sessionStorage.getItem( "isGSAdmin");

            _resetPasswordFields();

            $scope.generalObj.previousData = null;
            if (typeof $scope.resetInvalidFieldErrorStr == 'function') {
                $scope.resetInvalidFieldErrorStr();
            }

            loadingState.hide();
        }

        activate();

        /**
         * @func updatePassword()
         * @param {object} updatePWForm - password form object
         * @param event
         */
        function updatePassword(updatePWForm,event) {

            if(event && event.which != 13){
                return;
            }

            changePassword.updatePW.errorMessage = '';
            var path = $location.path();

            if ($scope.isFormValid(updatePWForm)) {
                var oldPassword = changePassword.updatePW.oldPassword;
                var newPassword = changePassword.updatePW.newPassword;
                AccountService.updatePassword(oldPassword,newPassword).success(function (response) {

                    sessionStorage. setItem("token",response.token);
                    commonUtilityService.storeUserTokenExpiryTime(response);
                    var userId = sessionStorage.getItem('userId');

                    if(path == '/auth/force-change-password'){
                        //success notification is shown from login controller in this case, coz as soon as the notification is
                        // called we have path change & the notification is never shown
                        analytics.sendEvent({
                            category: "Operations",
                            action: "Force Change Password",
                            label: "Profile Settings"
                        });
                        $location.path('auth/loginInternal');
                        $location.replace();
                    }
                    else{

                        var enterpriseId = sessionStorage.getItem("rootAccountId")?sessionStorage.getItem("rootAccountId"):sessionStorage.getItem("accountId");
                        AuthenticationService.enterpriseLogin(enterpriseId, userId).success(function (enterpriseLoginResp) {
                            commonUtilityService.storeEnterpriseTokenExpiryTime(enterpriseLoginResp);
                            commonUtilityService.showSuccessNotification("Password has been changed successfully.");

                            analytics.sendEvent({
                                category: "Operations",
                                action: "Change Password",
                                label: "Profile Settings"
                            });
                        }).error(function (enterpriseLoginResp) {
                            changePassword.updatePW.errorMessage = enterpriseLoginResp.errorStr;
                            console.log('authentication failure' + enterpriseLoginResp.errorStr);
                        });
                    }

                }).error(function (data) {
                    changePassword.updatePW.errorMessage = data.errorStr;
                });

                _resetPasswordFields();
            }
        }

        /**
         * @func _resetPasswordFields()
         * @desc resets password data binding objects
         * @private
         */
        function _resetPasswordFields() {
            changePassword.updatePW.newPassword = '';
            changePassword.updatePW.oldPassword = '';
            changePassword.updatePW.verifyPassword = '';
        }
    }
})();