'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('AccountInfoCtrl', AccountInfo);

    AccountInfo.$inject = ['$rootScope', '$scope', '$q', '$sce', 'commonUtilityService', 'loadingState', 'AccountService', 'UserService', 'analytics'];

    /**
     @constructor AccountInfo()
     */
    function AccountInfo($rootScope, $scope, $q, $sce, commonUtilityService, loadingState, AccountService, UserService, analytics){

        var accountInfo = this;

        /**
         * @func activate()
         * @desc controller start up logic
         */
        function activate(){

            $scope.generalObj = {};
            $scope.disableUISelect=false;
            accountInfo.profile = {};
            accountInfo.profile.user = {};
            accountInfo.profile.trustAsHtml = function (value) { return $sce.trustAsHtml(value); };
            accountInfo.profile.errorMessage = '';
            accountInfo.profile.updateAccountDetails = updateAccountDetails;

            var getCountriesPromise = commonUtilityService.getCountries();
            var getAccountDetailsPromise = AccountService.getAccountDetails();

            $q.all([getCountriesPromise, getAccountDetailsPromise]).then(function (response) {
                accountInfo.profile.countries = response[0].data.countryList;
                var account = response[1].data;
                _setAccountDetails(account);
            }, function (data) {
                commonUtilityService.showErrorNotification((data&&data.data)?(data.data.errorStr):null);
            });

            loadingState.hide();
        }

        activate();

        /**
         * @func updateAccountDetails()
         * @param {object} editAccountForm - form object
         */
        function updateAccountDetails(editAccountForm) {
            accountInfo.profile.errorMessage = '';
            if ($scope.isFormValid(editAccountForm,accountInfo.profile.user,true)) {
                var userRequest = _getAccountDetailsRequest(accountInfo.profile.user);
                var userList = { type:"UserList", list: [ userRequest ]};
                UserService.updateUsers(userList).success(function () {
                    commonUtilityService.showSuccessNotification(userRequest.firstName + " " + userRequest.lastName + " account details have been updated successfully.");
                    var data = {firstName: userRequest.firstName,lastName : userRequest.lastName};
                    $scope.generalObj.previousData = angular.copy(accountInfo.profile.user);
                    $rootScope.$emit('onUpdateAccountInfo',data);

                    analytics.sendEvent({
                        category: "Operations",
                        action: "Update Account Details",
                        label: "Profile Settings"
                    });

                }).error(function (data) {
                    accountInfo.profile.errorMessage = data.errorStr;
                });
            }else if($scope.generalObj.isNoChangeError){
                accountInfo.profile.errorMessage = "There is no change in the data to update.";
                $scope.generalObj.isNoChangeError = false;
            }
        }

        /**
         * @func _getAccountDetailsRequest()
         * @param {object} account - GEM account object
         * @returns {{type: string, userId: (*|number), firstName: string, lastName: string, emailId: string, address: {type: string, addressId: (*|number), country: string}}}
         * @private
         */
        function _getAccountDetailsRequest(account){
            return {
                type: "User",
                userId: account.userId,
                firstName: account.firstName,
                lastName: account.lastName,
                emailId: account.emailId,
                address:{
                    type:"Address",
                    addressId: account.addressId,
                    country:account.country ? account.country.code : ''
                }
            };
        }

        /**
         * @func setAccountDetails()
         * @param {object} account - GEM account object
         * @desc updates data binding objects used in the account details form.
         * @private
         */
        function _setAccountDetails(account){
            if(account){
                accountInfo.profile.user.firstName = account.firstName;
                accountInfo.profile.user.lastName = account.lastName;
                accountInfo.profile.user.emailId = account.emailId;
                if(account.address && account.address.country){
                    accountInfo.profile.user.addressId =  account.address.addressId;
                    accountInfo.profile.user.country = commonUtilityService.getCountryObject(accountInfo.profile.countries,account.address.country);
                }else{
                    accountInfo.profile.user.country = '';
                }
                accountInfo.profile.user.userId = account.userId;

                $scope.generalObj.previousData = angular.copy(accountInfo.profile.user);
            }
        }

    }
})();