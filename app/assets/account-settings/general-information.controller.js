'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('GeneralInfoCtrl', GeneralInfo);

    GeneralInfo.$inject = ['$scope', '$location', 'commonUtilityService', 'loadingState', 'SIMService', 'AccountService','$state'];

    /**
     @constructor GeneralInfo()
     */
    function GeneralInfo($scope, $location, commonUtilityService, loadingState, SIMService, AccountService,$state){

        var generalInfo = this;

        /**
         * @func activate()
         * @desc controller start up logic
         */
        function activate(){

            $scope.generalObj = {};
            generalInfo.generalInfo = {};
            generalInfo.generalInfo.showMoreSims = showMoreSIMs;

            var userId = sessionStorage.getItem('userId');
            SIMService.getSIMsByUserId(userId).success(function (response) {
                generalInfo.generalInfo.sims = response.list;
            }).error(function (data) {
                commonUtilityService.showErrorNotification(data.errorStr);
            });

            loadingState.hide();
        }

        /**
         * @func showMoreSIMs()
         * @desc shows all the SIMs for a specific user
         */
        function showMoreSIMs(){
            $scope.simsModalObj.showSIMs(null,generalInfo.generalInfo.sims);
        }

        activate();
    }
})();
