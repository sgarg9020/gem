'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('ProfileCtrl', Profile);

    Profile.$inject = ['$scope','$rootScope', '$sce', '$q','$location', 'AccountService', 'SIMService', 'commonUtilityService','UserService','AuthenticationService','loadingState'];

    /**
     @constructor Profile()
     @param {object} $scope - Angular.js $scope
     @param {object} $sce - Angular.js strict contextual escaping service
     @param {string|Object} response - The response body from data resolve
     */
    function Profile($scope,$rootScope, $sce, $q,$location, AccountService, SIMService, commonUtilityService,UserService,authenticationService,loadingState) {

        var appSettings = this;

        /*appSettings.onEditAccount = function () {
            $location.search ({ct: 1});
            $('#accountSettingsTab a[href="#editAccount"]').tab('show');
            $location.replace($location.url);

            appSettings.profile.errorMessage = '';

            var getCountriesPromise = commonUtilityService.getCountries();
            var getAccountDetailsPromise = AccountService.getAccountDetails();

            $q.all([getCountriesPromise, getAccountDetailsPromise]).then(function (response) {
                appSettings.profile.countries = response[0].data.countryList;
                var account = response[1].data;
                setAccountDetails(account);
            }, function (data) {
                commonUtilityService.showErrorNotification((data&&data.data)?(data.data.errorStr):null);
            });
        };

        appSettings.onGeneralInfo = function(){
            $location.search ({ct: 3});
            $('#accountSettingsTab a[href="#generalInfo"]').tab('show');
            $location.replace($location.url);
            var userId = sessionStorage.getItem('userId');
            SIMService.getSIMsByUserId(userId).success(function (response) {
                appSettings.generalInfo.sims = response.list;
            }).error(function (data, status) {
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        };

        appSettings.onChangePassword = function(){
            $location.search ({ct: 2});
            $('#accountSettingsTab a[href="#updatePassword"]').tab('show');
            $location.replace($location.url);
            resetPasswordFields();
            appSettings.updatePW.errorMessage = '';
            $scope.generalObj.previousData = null;
            if (typeof $scope.resetInvalidFieldErrorStr == 'function') {
                $scope.resetInvalidFieldErrorStr();
            }

        };*/


        /**
         * Start up logic for controller
         */
        function activate(){
            /* Edit Account
            appSettings.profile = {};
            appSettings.profile.user = {};
            appSettings.profile.trustAsHtml = function (value) {
                return $sce.trustAsHtml(value);
            };*/
            /* Update Password
            appSettings.updatePW = {};
            appSettings.generalInfo = {};
            appSettings.isGSAdmin = sessionStorage.getItem( "isGSAdmin");*/

            /*$scope.generalObj = {};

            var currentActiveTab = $location.search();
            if(currentActiveTab.ct === '2'){
                appSettings.onChangePassword();
            }else if(currentActiveTab.ct === '3'){
                appSettings.onGeneralInfo();
            }else{
                appSettings.onEditAccount();
            }*/
        }

        activate();

        /*function setAccountDetails(account){
            if(account){
                appSettings.profile.user.firstName = account.firstName;
                appSettings.profile.user.lastName = account.lastName;
                appSettings.profile.user.emailId = account.emailId;
                if(account.address && account.address.country){
                    appSettings.profile.user.addressId =  account.address.addressId;
                    appSettings.profile.user.country = commonUtilityService.getCountryObject(appSettings.profile.countries,account.address.country);
                }else{
                    appSettings.profile.user.country = '';
                }
                appSettings.profile.user.userId = account.userId;

                $scope.generalObj.previousData = angular.copy(appSettings.profile.user);
            }
        }*/

        /*appSettings.profile.updateAccountDetails = function (editAccountForm) {
            appSettings.profile.errorMessage = '';
            if ($scope.isFormValid(editAccountForm,appSettings.profile.user,true)) {
                var userRequest = getAccountDetailsRequest(appSettings.profile.user);
                var userList = { type:"UserList", list: [ userRequest ]};
                UserService.updateUsers(userList).success(function (response) {
                    commonUtilityService.showSuccessNotification("Account " + userRequest.firstName + " " + userRequest.lastName + " details have been updated successfully.");
                    var data = {firstName: userRequest.firstName,lastName : userRequest.lastName};
                    $scope.generalObj.previousData = angular.copy(appSettings.profile.user);
                    $rootScope.$emit('onUpdateAccountInfo',data);

                }).error(function (data, status) {
                    appSettings.profile.errorMessage = data.errorStr;
                });
            }else if($scope.generalObj.isNoChangeError){
                appSettings.profile.errorMessage = "There is no change in the data to update.";
                $scope.generalObj.isNoChangeError = false;
            }
        };

        function getAccountDetailsRequest(account){
            var accountRequest = {
                type: "User",
                userId: account.userId,
                firstName: account.firstName,
                lastName: account.lastName,
                emailId: account.emailId,
                address:{
                    type:"Address",
                    addressId: account.addressId,
                    country:account.country ? account.country.code : ''
                }
            };
            return accountRequest;
        }*/


        /*appSettings.generalInfo.showMoreSims = function(){
            if(appSettings.generalInfo.sims.length > 3){
                var queryParam = {};
                queryParam.details = 'firstName';
                AccountService.getAccountDetails(queryParam).success(function (response) {
                    var uid = sessionStorage.getItem('userId');
                    var data = {
                        userId : uid,
                        firstName : response.firstName
                    }
                    sessionStorage.setItem("gs-showSimsAssignedToCurrentUser",JSON.stringify(data));
                    $location.path('/app/sims');
                }).error(function (data, status) {
                    commonUtilityService.showErrorNotification(data.errorStr);
                });
            }
        };*/

        /*function resetPasswordFields() {
            appSettings.updatePW.newPassword = '';
            appSettings.updatePW.oldPassword = '';
            appSettings.updatePW.verifyPassword = '';
        }*/

        /*appSettings.updatePW.updatePassword = function (updatePWForm) {
            appSettings.updatePW.errorMessage = '';
            if ($scope.isFormValid(updatePWForm)) {
                var oldPassword = appSettings.updatePW.oldPassword;
                var newPassword = appSettings.updatePW.newPassword;
                AccountService.updatePassword(oldPassword,newPassword).success(function (response) {
                    sessionStorage. setItem("token",response.token);
                    sessionStorage. setItem("userToken",response.token);
                    var userId = sessionStorage.getItem('userId');
                    var enterpriseId = sessionStorage.getItem("rootAccountId")?sessionStorage.getItem("rootAccountId"):sessionStorage.getItem("accountId");
                    authenticationService.enterpriseLogin(enterpriseId, userId).success(function (enterpriseLoginResp) {
                        sessionStorage. setItem("token",enterpriseLoginResp.token);
                        commonUtilityService.showSuccessNotification("Password has been changed successfully.");
                    }).error(function (enterpriseLoginResp) {
                        $location.path("#/auth/login");
                        console.log('authentication failure' + enterpriseLoginResp);
                    });
                }).error(function (data, status) {
                    appSettings.updatePW.errorMessage = data.errorStr;
                });

                resetPasswordFields();
            }
        };*/

        loadingState.hide();
    }
})();