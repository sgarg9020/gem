'use strict';

(function () {
    angular.module('app')
        .controller('CallDateRecordsCtrl', CDR);

    CDR.$inject = ['$scope', '$stateParams', '$compile', 'SIMService', 'dataTableConfigService', 'commonUtilityService','tooltips'];

    /**
     * @param $scope
     * @param $stateParams
     * @param $compile
     * @param SIMService
     * @param dataTableConfigService
     * @param commonUtilityService
     * @param tooltips
     * @constructor CDR
     */
    function CDR($scope, $stateParams, $compile, SIMService, dataTableConfigService, commonUtilityService,tooltips) {
        var cdrVm = this;

        /**
         * @function activate
         * @description All the activation code goes here.
         */
        function activate() {
            $scope.emptyTableMessage = "No CDRs available.";
            $scope.dataTableObj = {};
            cdrVm.accountId = sessionStorage.getItem("accountId");
            cdrVm.simId = $stateParams.iccId;

            $scope.dataTableObj.controlID = 'datatable-controls-cdr-list';
            $scope.dataTableObj.tableID = 'cdrListDataTable';
            $scope.dataTableObj.searchValue = '';
            cdrVm.exportData = {
                // url:ADMIN_API_BASE + 'account/' + $stateParams.userId + '/SIM/' + $stateParams.simId + '/callDataRecords?'
            };

            $scope.dataTableObj.tableHeaders = [
                {"value": "Date (UTC)", "DbValue": "recordStartTime"},
                {"value": "Usage", "DbValue": "dataInBytes"},
                {"value": "Country", "DbValue": "countryList"}
            ];
            $scope.dataTableObj.dataTableOptions = {
                "createdRow": function (row, data, index) {
                    $compile(row)($scope);
                },
                bSort : false,
                "columnDefs": [
                    {
                        "targets": 0,
                        "render": function (data, type, row, meta) {
                            return "<span class='nowrap'>" + row.recordStartTime + "</span>";
                        }
                    },
                    {
                        "targets": 1,
                        "render": function (data, type, row, meta) {
                            // Always show in MB
                            var dataUsed= commonUtilityService.calculateByteRange(row.dataInBytes, "MB");
                            var dataUsedFormatted = commonUtilityService.calculateByteRange(row.dataInBytes);
                            return '<span class="cursor-pointer font-inconsolata text-right w-60 ds-inline" data-toggle="tooltip" data-placement="top" title="'+dataUsedFormatted.data+' '+dataUsedFormatted.measure+'">'+dataUsed.data+'</span>';
                        }
                    },{
                        "targets": 2,
                        "render": function (data, type, row, meta) {
                            return "<span class='nowrap'>" + commonUtilityService.getCountriesString(row.countryList) + "</span>";
                        }
                    }
                ],
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ",
                    "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    "sEmptyTable": $scope.emptyTableMessage,
                    "sZeroRecords": $scope.emptyTableMessage
                },
                "processing": true,
                fnServerData: function (sSource, aoData, fnCallback) {
                    tooltips.destroyTooltips('[data-toggle="tooltip"]');
                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    $scope.isPageSizeFilterDisabled = false;

                    SIMService.getSIMCdrs(cdrVm.accountId, cdrVm.simId, queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response, oSettings, $scope.emptyTableMessage);
                        $scope.cdrs = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ($scope.cdrs.length == 0);
                        fnCallback(datTabData);
                        tooltips.initTooltips('[data-toggle="tooltip"]');
                    }).error(function (data, status) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.cdrs || $scope.cdrs.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings, fnCallback, "CDRs");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });

                },columns:[
                    {
                        width:"30%",
                        orderable:false
                    },
                    {
                        width:"30%",
                        orderable:false
                    },
                    {
                        width:"30%",
                        orderable:false
                    }
                ],
                "fnDrawCallback": function( oSettings ) {
                    if($scope.cdrs )
                        dataTableConfigService.disableTableHeaderOps( $scope.cdrs.length, oSettings );

                }
            };
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

            $scope.$on('$destroy', function () { tooltips.destroyTooltips('[data-toggle="tooltip"]'); });
        }

        activate();
    }
})();