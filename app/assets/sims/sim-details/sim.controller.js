/* Controllers */
(function () {
    "use strict";

    angular.module('app')
        .controller('SIMCtrl', SIM);

    SIM.$inject = ['$scope','loadingState','$stateParams','AuthenticationService','$rootScope'];

    /**
     @constructor SIM()
     */
    function SIM($scope,loadingState,$stateParams,authenticationService,$rootScope) {

        var SIMVm = this;

        /**
         * Start up logic for controller
         */
        function activate() {

            clearDataTableStatus(['DataTables_locationHistoryDataTable_','DataTables_cdrListDataTable_','DataTables_OTAHistoryDataTable_']);

            $scope.iccId = $stateParams.iccId;
            $scope.enterpriseId = sessionStorage.getItem("accountId");
            //getEnterpriseDetails();

            loadingState.hide();
        }

        /*//fetching accountName to show in breadcrumb
        function getEnterpriseDetails(){
            var queryParam = {};
            queryParam.details = 'accountName';
            /!* get enterprise details *!/
            authenticationService.getEnterpriseAccountDetails($scope.enterpriseId,queryParam).success(function (response) {
                $rootScope.currentAccountName = response.accountName;
            }).error(function (data) {
                console.log("error while fetching enterprise details");
            });
        }*/

        activate();

    }
})();
