'use strict';

(function () {
    angular.module('app')
        .controller('SIMDetailCtrl', SIMDetail);

    SIMDetail.$inject = ['$scope', '$rootScope', '$stateParams','SIMService', 'commonUtilityService','AuthenticationService', 'SIMConstants', 'ProfileService'];

    /**
     * @param $scope
     * @param SIMService
     * @param commonUtilityService
     * @param $stateParams
     * @constructor SIMDetail
     */
    function SIMDetail($scope, $rootScope, $stateParams, SIMService, commonUtilityService,AuthenticationService, SIMConstants, ProfileService) {
        var SIMDetailVM = this;

        SIMDetailVM.getSIMDetails = getSIMDetails;
        SIMDetailVM.hasOperations = hasOperations;
        SIMDetailVM.markESimAsDeleted = markESimAsDeleted;
        SIMDetailVM.hasAccess = {
            markESimAsDeleted: function () {
                return SIMDetailVM.isConsumerEsim && AuthenticationService.isOperationAllowed(['PROFILE_STATUS_EDIT'])
                    && SIMDetailVM.consumerEsimInfo && SIMDetailVM.consumerEsimInfo.consumerEsimProfileStatus !== 'DELETED';
            }
        };

        /**
         * @function activate
         * @description All the activation code goes here.
         */
        function activate() {
            $scope.imsiModalObj = {};
            SIMDetailVM.simId = $stateParams.iccId;
            SIMDetailVM.accountId = sessionStorage.getItem("accountId");
            SIMDetailVM.connectionStatus = null;
            getSIMDetails();
        }

        /**
         * @function getSIMDetails
         * @description fetches the SIM Details
         */
        function getSIMDetails() {
            var queryParam = {
                details:"account|addedToAccountOn|nickName|user|deviceType|eUICCDetails"
            };

            SIMService.getSimDetail(SIMDetailVM.accountId,SIMDetailVM.simId,queryParam).success(function(response){                
                SIMDetailVM.SIM = response;
                SIMDetailVM.user = response.user ? response.user.firstName + " " + response.user.lastName : 'Unassigned';
                SIMDetailVM.group = response.account ? response.account.accountName : '';
                if(response.eUICCDetails){
                    SIMDetailVM.eUICCDetails = response.eUICCDetails;
                }
                if(response.consumerEsimInfo){
                    SIMDetailVM.consumerEsimInfo = response.consumerEsimInfo;
                }
                configureSectionsAndLabels();
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

        function configureSectionsAndLabels() {
            SIMDetailVM.status = SIMConstants.status[SIMDetailVM.SIM.status] || {};
            SIMDetailVM.deviceType = SIMConstants.deviceType[SIMDetailVM.SIM.deviceType] || {};
            switch (SIMDetailVM.SIM.deviceType) {
                case 'pSIM':
                    SIMDetailVM.isPhysicalSim = true;
                    SIMDetailVM.simIdLabel = 'Gigsky ICCID';
                    break;
                case 'ESIM_CONSUMER':
                    SIMDetailVM.isConsumerEsim = true;
                    SIMDetailVM.simIdLabel = 'Gigsky ICCID';
                    SIMDetailVM.installationStatus = SIMConstants.installationStatus[SIMDetailVM.consumerEsimInfo.consumerEsimProfileStatus] || {};
                    break;
                case 'IOT_M2M':
                    SIMDetailVM.isM2mEsim = true;
                    SIMDetailVM.canShowEuiccProfiles = true;
                    SIMDetailVM.simIdLabel = 'Gigsky EID';
                    break;
            }
        }

        function markESimAsDeleted() {
            var msg = "Warning! Selected eSIMs will be deleted and CANNOT be used again.";
            var attr = {
                header: 'Mark eSIM as Deleted',
                cancel: 'Cancel',
                submit: 'Confirm'
            };
            var func = function(){
                ProfileService.changeProfileStatusOfEsim(SIMDetailVM.accountId, SIMDetailVM.simId).success(function(response){
                    commonUtilityService.showSuccessNotification("The eSIM has been marked as deleted successfully.");
                    getSIMDetails();
                }).error(function(data){
                    commonUtilityService.showErrorNotification(data.errorStr);
                });
            };

            $scope.verificationModalObj.updateVerificationModel(attr,func);
            $scope.verificationModalObj.addVerificationModelMessage(msg);

            $('#verificationModal').modal('show');
        }

        function hasOperations() {
            return SIMDetailVM.hasAccess.markESimAsDeleted();
        }

        activate();

    }
})();
