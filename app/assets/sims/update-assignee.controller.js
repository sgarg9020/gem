'use strict';

/* Controllers */
(function(){
	angular.module('app')
	    .controller('UpdateAssigneeCtrl', UpdateAssignee);

	UpdateAssignee.$inject = ['$scope', '$sce', 'modalDetails','notifications','UserService','dataTableConfigService','$sanitize','$compile'];
	 
	/**
		@constructor UpdateAssignee()
		@param {object} $scope - Angular.js $scope
		@param {object} $sce - Angular.js strict contextual escaping service
		@param {object} modalDetails - Factory object
		@param {object} APIRequests - Factory object
		@param {object} notifications - Factory object
	*/   
	function UpdateAssignee($scope, $sce, modalDetails, notifications,UserService,dataTableConfigService,$sanitize,$compile){
		
		var updateAssigneeModalObj = this;

		/**
		 * Start up logic for controller
		 */
		function activate(){
			/* Form elements object */
			updateAssigneeModalObj.users = [{}];

			/* modalDetails factory */
			modalDetails.scope(updateAssigneeModalObj, true);
			modalDetails.attributes({
				id: 'updateAssigneeModal',
				formName: 'updateAssignee',
				header: 'Update Assignee',
				cancel: 'Cancel',
				submit: 'Assign'
			});
			modalDetails.cancel(function () {
				clearModalData();
			});
			$scope.$parent.updateAssigneeModalObj.updateAssigneeModalSubmit = updateSubmit;
			$scope.$parent.updateAssigneeModalObj.updateAssigneeModalAttributes = updateModalAttributes;
			$scope.$parent.updateAssigneeModalObj.getGroupModalAssignee = getSelectedAssignee;
			updateAssigneeModalObj.trustAsHtml = function (value) {
				return $sce.trustAsHtml(value);
			};

			initTables();
		}

		function initTables() {
			$scope.currentPageRowIds = [];
			clearDataTableStatus(['DataTables_assignUserTable_']);

			$scope.emptyAssignUserTableMessage = "No Users available.";
			$scope.dataTableObj = {};

			$scope.dataTableObj.tableHeaders = [
				//{"value": "","DbValue":""},
				{value: "User Name", DbValue: "fullName"},
				{value: "Email", DbValue: "emailId"}
			];

			$scope.dataTableObj.tableID = "assignUserTable";
			$scope.selectedUsersList = [];
			$scope.dataTableObj.dataTableOptions = _setDataTableOptions();

			$('#search_input').keyup(function(){
				$scope.dataTableObj.dataTableRef.fnFilter($scope.user_search)
			})
		}

		function _setDataTableOptions(){
			var columnsConfig = [
				{ orderable:false},
				{ orderable:false},
				{ orderable:false}
			];
			var columnsDef = [{
				targets: 0,
				className: "checkbox-table-cell",
				render: function ( data, type, row, meta) {
					return '<div class="checkbox check-success large no-label-text"> <input type="checkbox" value="1" id="checkbox_'+row.userId+'" gs-index="'+meta.row+'"><label ng-click="checkBoxClicked($event)" id="label_'+row.userId+'"></label></div>';
				}
			},{
				targets: 1,
				render: function (data, type, row, meta) {
					var fName = $sanitize(row.firstName);
					var lName = $sanitize(row.lastName);

					return fName + ' ' + lName;
				}
			},{
					targets: 2,
					render: function (data, type, row) {
						var emailId = $sanitize(row.emailId);
						return 	'<a href="mailto:'+emailId+'" title="Send an email to '+emailId+'" class="nowrap">'+emailId+'</a>'
					}
			}];

			return {
				data:  $scope.assignUsersList,
				sDom: 'tip',
				pageLength: 10,
				lengthChange: false,
				createdRow: function ( row, data, index ) {
                    $(row).attr("id",'UID_'+data.userId);
                    $compile(row)($scope);
				},
				columns: columnsConfig,
				columnDefs: columnsDef,
				oLanguage: {
					sLengthMenu: "_MENU_ ",
					sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
					sEmptyTable: $scope.emptyAssignUserTableMessage,
					sZeroRecords:$scope.emptyAssignUserTableMessage
				},
				"rowCallback": function( row, data ) {
					$scope.currentPageRowIds.push(data.userId);
				},
				"fnDrawCallback": function( oSettings ) {
					$scope.dataTableObj.showPreviousSelectAll($scope.currentPageRowIds);
					$scope.currentPageRowIds = [];
				}
			};
		}

		function getUsers() {
			if(!$scope.assignUsersList ){
				UserService.getUsers( { count:100000, startIndex:0, details:"fullName", sortBy:"fullName", sortDirection:"ASC"} ).success(function (response) {
					$scope.assignUsersList = response.list;

					if($scope.dataTableObj.dataTableRef){
						$scope.dataTableObj.dataTableRef.fnClearTable();
						if(response.list.length > 0){
							$scope.dataTableObj.dataTableRef.fnAddData(response.list, true);
						}
					}
					angular.element('#' + $scope.updateAssigneeModalObj.modal.id).modal('show');
				}).error(function(data,status){
					console.log('get users failure');
					commonUtilityService.showErrorNotification(data.errorStr);
				});
			}else{
				$scope.dataTableObj.refreshDataTable();
				angular.element('#' + $scope.updateAssigneeModalObj.modal.id).modal('show');
			}
		}

		$scope.checkBoxClicked = function(event){
			$scope.dataTableObj.selectRow(event,true);
			updateSelectedUsersCount();
		};

		$scope.selectAllRows = function(event){
			$scope.dataTableObj.selectAll(event, true);
			updateSelectedUsersCount();
		}

		function updateSelectedUsersCount() {
			var selectedRows = $scope.dataTableObj.getSelectedRows();
			$scope.noOfUsersSelected = selectedRows.length;
		}

		/**
			@func updateModalAttributes()
			@param {object} obj - the object to pass to modalDetails factory
			@desc - Expose access to change the attributes of the modal for directive gs-verification-modal	
		*/
		function updateModalAttributes(obj){
			modalDetails.scope(updateAssigneeModalObj, false);
			modalDetails.attributes(obj);
		}				
		
		/**
			@func updateSubmit()
			@param {function} func - the function the execute on submit
			@desc - Expose access to change the submit function for directive gs-verification-modal.
		*/
		function updateSubmit(selectedSIMs, func, attrs) {
			if (attrs)
				modalDetails.attributes(attrs);
				modalDetails.scope(updateAssigneeModalObj, false);

				var simsCount = selectedSIMs.length;
				$scope.selectedSimMsg = simsCount > 1 ? simsCount + ' SIMs' : simsCount + ' SIM';
				getUsers();
				modalDetails.submit(function () {

				if (selectedSIMs.length != $scope.noOfUsersSelected) {
					$scope.updateAssigneeModalObj.errorMessage = "The number of Users selected does not equal the number of SIMs to be assigned!";
					return false;
				} else {
					var selectedUsers = $scope.dataTableObj.getSelectedRows();
					func(selectedUsers, selectedSIMs);

					clearModalData();
					angular.element('#' + $scope.updateAssigneeModalObj.modal.id).modal('hide');
				}
			});
		}

		function clearModalData() {
			$scope.dataTableObj.unselectAllProcessedRows();
			$scope.noOfUsersSelected = 0;
			if($scope.user_search){
				$scope.dataTableObj.dataTableRef.fnFilter('');
				delete $scope.user_search;
			}
			delete $scope.updateAssigneeModalObj.errorMessage;

		}
		/**
			@func getSelectedAssignee()
			@return - the userId key value from updateAssigneeModalObj.user (updateAssigneeModalObj.user.userId)
			@desc - Expose access to the updateAssigneeModalObj.user.userId value for directive gs-verification-modal	
		*/
		function getSelectedAssignee(){
			return updateAssigneeModalObj.users.user;
		}

		activate();

	}
})();
