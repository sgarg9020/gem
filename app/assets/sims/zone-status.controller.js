'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('ZoneStatusCtrl', ZoneStatus);

    ZoneStatus.$inject = ['modalDetails','$scope','$timeout'];

    /**
     @constructor ZoneStatus()
     @param {object} modalDetails - Factory
     */
    function ZoneStatus(modalDetails,$scope,$timeout) {

        var zoneStatus = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            zoneStatus.simZonesStaus = {};


            modalDetails.scope(zoneStatus, true);
            modalDetails.attributes({
                id: 'zoneStatusModal',
                header: 'SIM - Zone Status',
                cancel: 'Close',
                submit: 'Alerts'
            });
            modalDetails.cancel(function(){console.log("This should do nothing");});
            // Todo - create logic for anonymous function
            modalDetails.submit(function(){console.log("This should take the user to the triggered alerts list page");});

            $scope.$parent.zoneStatusModalObj.updateZonesStatus = updateModal; /* Attach to parent */
        }

        function updateModal(simStatus){


            $timeout(function(){
                $('div.zoneStatusModal').scrollTop(0);
            },300);

            zoneStatus.simZonesStaus.zoneAccess = simStatus.zoneAccess;
            zoneStatus.simZonesStaus.showFullAccessMsg = false;
            zoneStatus.simZonesStaus.noAccessMsg = false;
            if(zoneStatus.simZonesStaus.zoneAccess == "Full Access"){
                zoneStatus.simZonesStaus.showFullAccessMsg = true;
            }else if(zoneStatus.simZonesStaus.zoneAccess == "No Access"){
                zoneStatus.simZonesStaus.noAccessMsg = true;
                zoneStatus.simZonesStaus.simId = simStatus.simId;
            }else{
                zoneStatus.simZonesStaus.simId = simStatus.simId;

                if(simStatus.zoneConnectivityStatus){
                    var disabledZonesList = simStatus.zoneConnectivityStatus.disabledZoneList ? simStatus.zoneConnectivityStatus.disabledZoneList.zoneList : [];
                    var throttledZonesList = simStatus.zoneConnectivityStatus.throttledZoneList ? simStatus.zoneConnectivityStatus.throttledZoneList.zoneList : [];

                    var allZones = simStatus.allZones;
                    zoneStatus.allZones = simStatus.allZones;
                    for(var i=0 ; i < allZones.length; i++){
                        var zone = allZones[i];
                        zone.status = "ENABLED";
                        var isDisabled = isExistInList(disabledZonesList,zone.zoneId);
                        if(isDisabled){
                            zone.status = "DISABLED";
                        }else{
                            var isThrottled = isExistInList(throttledZonesList,zone.zoneId);
                            if(isThrottled){
                                zone.status = "THROTTLED";
                            }
                        }


                    }

                }
            }
        }

        function isExistInList(zonesList,zoneId){

            for(var i=0 ; i < zonesList.length; i++){
                var zone = zonesList[i];

                if(zone.zoneId == zoneId){
                    return true;
                }
            }

            return false;
        }

        activate();
    }
})();





