'use strict';

/* Directive */
(function(){
	angular.module('app')
		.directive('gsUpdateAssignee',UpdateAssignee);

	UpdateAssignee.$inject = ['formMessage','$timeout'];
	
	/**
		@function updateAssignee
		@param {object} formMessage - Factory object
	 	@param {object} $timeout - Angular JS timeout service
	*/		
	function UpdateAssignee(formMessage, $timeout) {
		
		var link = {
			link: link
		};
		
		return link;
		
		/**
			@function link()
			@param {object} $scope - directive $scope
			@param {object} element - directive HTML element
			@param {object} attrs - attributes on the directive HTML element	
		*/
		function link($scope, element, attrs) {
			$scope.$parent.updateAssigneeModalObj.asneAddModalMessage = setMessage;

			$timeout(function(){
				$("#"+attrs.id).on("shown.bs.modal" ,function(){
					if($("body").hasClass("desktop")){
						// get the ui select controller
						$timeout(function(){
							var uiSelect = angular.element("#assignee-user").controller('uiSelect');
							// Open the select and focus
							uiSelect.activate();
						},0);
					}
				});
			},0);

			/**
				@function setMessage()
				@param {string} msg - message to use for formMessage factory
				@param {string} className - the class name to use for the formMessage
			*/
			function setMessage(msg, className){
				/* Enable a formMessage factory */
				formMessage.scope($scope.updateAssigneeModalObj);
				formMessage.className(className);
				formMessage.message(true, msg);
			}
		}
	}
})();


/* Directive to reset the previously selected user when invalid user name is entered */

(function(){

	//TODO - This needs to be moved to it's own file - Curtis 03/14/2016

	angular.module('app').directive('gsResetSelectedUser', function() {
		return {
			restrict: 'AE',
			link:function ($scope, element, attrs) {

				if( typeof $scope.$parent.updateAssigneeModalObj.users.user != "undefined" ) {
					if (typeof $scope.$parent.updateAssigneeModalObj.users.user != "undefined") {
						delete $scope.$parent.updateAssigneeModalObj.users.user;
					}

				}

			}

		};
	});
})();


