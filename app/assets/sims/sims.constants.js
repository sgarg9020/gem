'use strict';

(function () {
    angular.module('app')
        .constant('SIMConstants', {
            deviceType: {
                pSIM: {
                    label: 'Physical SIM',
                    value: 'pSIM'
                },
                IOT_M2M: {
                    label: 'M2M eSIM',
                    value: 'IOT_M2M'
                },
                ESIM_CONSUMER: {
                    label: 'eSIM',
                    value: 'ESIM_CONSUMER'
                }
            },
            installationStatus: {
                ALL: {
                    label: 'All',
                    value: 'ALL'
                },
                AVAILABLE: {
                    label: 'Available',
                    value: 'AVAILABLE',
                    class: 'available'
                },
                ASSIGNED: {
                    label: 'Assigned',
                    value: 'ASSIGNED',
                    class: 'assigned'
                },
                DOWNLOADED: {
                    label: 'Downloaded',
                    value: 'DOWNLOADED',
                    class: 'downloaded'
                },
                DOWNLOAD_FAILED: {
                    label: 'Download Failed',
                    value: 'DOWNLOAD_FAILED',
                    class: 'failed'
                },
                INSTALLED: {
                    label: 'Installed',
                    value: 'INSTALLED',
                    class: 'installed'
                },
                INSTALL_FAILED: {
                    label: 'Install Failed',
                    value: 'INSTALL_FAILED',
                    class: 'failed'
                },
                DELETED: {
                    label: 'Deleted',
                    value: 'DELETED',
                    class: 'deleted'
                }
            },
            status: {
                INACTIVE: {
                    label: 'Inactive',
                    value: 'INACTIVE',
                    class: 'inactive'
                },
                ACTIVE: {
                    label: 'Active',
                    value: 'ACTIVE',
                    class: 'active'
                },
                BLOCKED: {
                    label: 'Blocked',
                    value: 'BLOCKED',
                    class: 'blocked'
                },
                UNBLOCKED: {
                    label: 'Unblocked',
                    value: 'UNBLOCKED',
                    class: 'unblocked'
                }
            }
        });

})();
