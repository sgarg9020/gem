'use strict';

(function(){
    angular
        .module('app')
        .factory('simsCommonFactory', SimsCommonFactory);

    SimsCommonFactory.$inject = ['SettingsService','SIMService','commonUtilityService','analytics','SIMConstants'];

    /**
     *
     * @param alertsService
     * @param commonUtilityService
     * @param analytics
     * @param pollingAPIService
     * @returns {{setScope: setScope}}
     * @constructor
     */
    function SimsCommonFactory(SettingsService,SIMService,commonUtilityService,analytics,SIMConstants){

        var scope = {};

        return {
            getZoneStatusForSim: getZoneStatusForSim,
            showZoneStatusModal: showZoneStatusModal,
            setScope: setScope,
            getInstallationStatusFilters: getInstallationStatusFilters
        };

        /**
         * @function setScope()
         * @param {object} obj - scope from controller
         * @desc mutator for scope public variable
         */
        function setScope(obj){
            if(typeof obj !== "object")
                return console.error("AlertsService: Please pass a valid scope for method setScope");
            scope = obj;
        }

        function getInstallationStatusFilters() {
            var installationStatusList =  SIMConstants.installationStatus;
            var filters = [];

            Object.entries(installationStatusList).forEach(function (entry) {
                var value = entry[1];
                var filter = {
                    name : value.label,
                    dbRef: value.value,
                    skipQueryParam: value.value === 'ALL' ? true : false
                }
                filters.push(filter);
            });

            return filters;
        }

        function getZoneStatusForSim(zoneConnectivityStatus,count){
            var zoneStatus = "Full Access";

            if(zoneConnectivityStatus){

                var disabledZonesCount = zoneConnectivityStatus.disabledZoneList ? zoneConnectivityStatus.disabledZoneList.zoneList.length : 0;
                var totalCount = disabledZonesCount + (zoneConnectivityStatus.throttledZoneList ? zoneConnectivityStatus.throttledZoneList.zoneList.length : 0);
                if(disabledZonesCount == count){
                    zoneStatus = "No Access";
                }else if(totalCount > 0){
                    zoneStatus = "Partial Access";
                }
            }
            return zoneStatus;
        }

        function showZoneStatusModal(sim,count,accountId){
            var simZonesStatus = {
                simId : sim.simId,
                zoneConnectivityStatus : sim.zoneConnectivityStatus
            };

            var details = 'name';
            var enterpriseId = accountId ? accountId : sessionStorage.getItem("accountId");
            SettingsService.getPlanDetails(details,enterpriseId).success(function (response) {
                simZonesStatus.allZones = response.list;
                simZonesStatus.zoneAccess = getZoneStatusForSim(sim.zoneConnectivityStatus,count);
                scope.zoneStatusModalObj.updateZonesStatus(simZonesStatus);

                $('#zoneStatusModal').modal("show");

                analytics.sendEvent({
                    category: "Operations",
                    action: "View zone access details",
                    label: "Sim Management"
                });
            }).error(function (data) {
                console.log("Error while fetching all zones");
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

    }
})();
