'use strict';

(function () {
    angular.module('app')
        .controller('LocationHistoryCtrl', ['$scope', 'commonUtilityService', '$stateParams', 'dataTableConfigService', '$compile', 'SIMService', LocationHistory]);

    function LocationHistory($scope, commonUtilityService, $stateParams, dataTableConfigService, $compile, SIMService) {
        var locationHistory = this;
        var refreshRequired = false;

        function activate() {
            $scope.emptyTableMessage = "No Location history Available.";
            $scope.dataTableObj = {};
            $scope.dataTableObj.controlID = 'datatable-controls-location-history';
            $scope.dataTableObj.tableID = 'locationHistoryDataTable';
            locationHistory.accountId = sessionStorage.getItem("accountId");
            locationHistory.simId = $stateParams.iccId;
            locationHistory.exportData = {
                //  url:ADMIN_API_BASE + 'account/' + locationHistory.userId + '/SIM/' + locationHistory.simId + '/locationHistory?'
            };

            $scope.dataTableObj.tableHeaders = [
                {"value": "Date (UTC)", "DbValue": "timeAtGSBackend"},
                {"value": "Country", "DbValue": "countryList"},
                {"value": "Connection Type", "DbValue": "connectionType"}
            ];
            $scope.dataTableObj.dataTableOptions = {
                bSort: false,
                columnDefs: [{
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        return "<span class='nowrap'>" + row.locationTime + "</span>";
                    }
                },{
                        "targets": 1,
                        "render": function (data, type, row, meta) {
                                return "<span class='nowrap'>" + commonUtilityService.getCountriesString(row.countryList) + "</span>";
                        }
                    },
                    {
                        "targets": 2,
                        "render": function (data, type, row, meta) {
                            if (row.connectionType)
                                return '<span class="status-text nowrap">' + (row.connectionType === 'CA'?'Connection Accepted':row.connectionType === 'RJ'?'Rejected':row.connectionType) + '</span>';
                            return '<span class="status-text nowrap">Not Available</span>';
                        }
                    }
                ],
                "createdRow": function (row, data, index) {
                    $compile(row)($scope);
                },
                "fnServerData": function (sSource, aoData, fnCallback) {
                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    $scope.isPageSizeFilterDisabled = false;

                    SIMService.getSIMLocationHistory(locationHistory.accountId, locationHistory.simId, queryParam).success(function (response) {
                        locationHistory.simType = response.simType;
                        var tableForCol = $('#locationHistoryDataTable').DataTable();
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
                        $scope.locationHistoryList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ($scope.locationHistoryList.length == 0);
                        fnCallback(datTabData);
                    }).error(function (data, status) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.locationHistoryList || $scope.locationHistoryList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"location history");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });

                },columns:[
                    {
                        width:"30%",
                        orderable:false
                    },
                    {
                        width:"30%",
                        orderable:false
                    },
                    {
                        width:"30%",
                        orderable:false
                    }
                ],
                "fnDrawCallback": function (oSettings) {
                    if($scope.locationHistoryList )
                        dataTableConfigService.disableTableHeaderOps( $scope.locationHistoryList.length, oSettings );
                },
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ",
                    "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    "sEmptyTable": $scope.emptyTableMessage,
                    "sZeroRecords": $scope.emptyTableMessage
                }
            };


            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

        }

        activate();

    }
})();