'use strict';

(function(){
    angular
        .module('app')
        .factory('SIMService',['commonUtilityService','analytics',SIMService]);

    /**
     @function debounce()
     @param {object} $timeout - angular timeout service
     - Factory method for functions to be executed after a delay
     */
    function SIMService(commonUtilityService,analytics){

        var bulkActiomap = {
            Block: {
                msg:  "Blocking a SIM will disable the the SIM's data connectivity. This operation can take a few minutes to complete.",
                attr:{
                    id: 'verificationModal',
                    header: 'Block SIM',
                    cancel: 'Cancel',
                    submit: 'Block'
                },
                analyticObj:{
                    category: "Operations",
                    action: "Bulk blocking sims",
                    label: "Sim Management"
                },
                replicationMsgPlural:"Selected SIMs are already blocked!!",
                replicationMsgSingular:'Selected SIM is already blocked!!',
                updateStatus:'BLOCK',
                simStatus:'BLOCKED',
                simProfileStatus: 'DELETED'
            },
            Unblock: {
                msg:  "Unblocking a SIM will enable SIMs network connectivity. Once SIMs are unblocked, they should be activated to enable data access.",
                attr:{
                    id: 'verificationModal',
                    header: 'Unblock SIM',
                    cancel: 'Cancel',
                    submit: 'UnBlock'
                },
                analyticObj:{
                    category: "Operations",
                    action: "Bulk unblocking sims",
                    label: "Sim Management"
                },
                replicationMsgPlural:"Selected SIMs are already unblocked!",
                replicationMsgSingular:'Selected SIM is already unblocked!',
                updateStatus:'UNBLOCK',
                simStatus:'INACTIVE',
                simProfileStatus: 'DELETED'
            },
            Deactivate: {
                msg:  "Deactivating a SIM will block the SIM's access to the network. Deactivation can take a few minutes to complete.",
                attr:{
                    id: 'verificationModal',
                    header: 'Deactivate SIM',
                    cancel: 'Cancel',
                    submit: 'Deactivate'
                },
                analyticObj:{
                    category: "Operations",
                    action: "Bulk deactivate sims",
                    label: "Sim Management"
                },
                replicationMsgPlural:"Selected SIMs are already deactivated!!",
                replicationMsgSingular:'Selected SIM is already deactivated!!',
                updateStatus:'DEACTIVATE',
                simStatus:'INACTIVE',
                simProfileStatus: 'DELETED'
            },
            Activate: {
                msg:  "Activating SIM",
                attr:{
                    id: 'verificationModal',
                    header: 'Activate SIM',
                    cancel: 'Cancel',
                    submit: 'Activate'
                },
                analyticObj:{
                    category: "Operations",
                    action: "Bulk activate sims",
                    label: "Sim Management"
                },
                replicationMsgPlural:"Selected SIMs are already activated!",
                replicationMsgSingular:'Selected SIM is already activated!',
                updateStatus:'ACTIVATE',
                simStatus:'ACTIVE',
                simProfileStatus: 'DELETED'
            },
            Delete: {
                msg:  "Deleted SIM(s) will no longer be visible in the enterprise account. Deletion can take a few minutes to complete.",
                attr:{
                    id: 'verificationModal',
                    header: 'Delete SIM(s)',
                    cancel: 'Cancel',
                    submit: 'Delete'
                },
                analyticObj:{
                    category: "Operations",
                    action: "Bulk delete sims",
                    label: "Sim Management"
                },
                updateStatus:'DELETE'
            },
            ResendActivationLink: {
                msg:  "Resending Activation Link",
                attr:{
                    id: 'verificationModal',
                    header: 'Resend Activation Link',
                    cancel: 'Cancel',
                    submit: 'Resend'
                },
                analyticObj:{
                    category: "Operations",
                    action: "Bulk resend activation link",
                    label: "Sim Management"
                },
                updateStatus:'RESEND_LINK',
                simProfileStatus: 'ASSIGNED',
                replicationMsgPlural:"Activation link can be sent only if the SIMs are in ASSIGNED state",
                replicationMsgSingular:"Activation link can be sent only if the SIM is in ASSIGNED state",
            },
            InvalidOperation: {
                replicationMsgPlural:"This operation is not allowed on blocked SIMs!",
                replicationMsgSingular:'This operation is not allowed on blocked SIM!',
            },
            InvalidConsumerSIMOperation: {
                replicationMsgPlural:"This operation is not allowed on Deleted eSIMs!",
                replicationMsgSingular:'This operation is not allowed on Deleted eSIM!',
            }

        };
        var factory = {

            getSIMs:getSIMs,
            getSIMsByUserId:getSIMsByUserId,
            updateSIM:updateSIM,
            updateSIMs:updateSIMs,
            bulkSIMactions:bulkSIMactions,
            updateSIMsStatus:updateSIMsStatus,
            refreshDataUsage:refreshDataUsage,
            getExportDataUrl:getExportDataUrl,
            getSimDetail:getSimDetail,
            getSIMLocationHistory:getSIMLocationHistory,
            getSIMCdrs:getSIMCdrs,
            switchIMSIMode:switchIMSIMode,
            switchIMSI:switchIMSI,
            getSIMOTAHistory:getSIMOTAHistory,
            getSIMOperationsHistory:getSIMOperationsHistory,
            getSIMVersions:getSIMVersions,
            getExportCDRdataUrl:getExportCDRdataUrl,
            getExportLHdataUrl:getExportLHdataUrl,
            auditEIS: auditEIS
        };

        return factory;

        function auditEIS(accountId, SIMId) {
            var headerAdd=['PROFILE_READ_ADVANCED'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/sim/"+SIMId+"/operation/auditEIS", 'GET',headerAdd);
        }
        function getExportCDRdataUrl (accId,iccId,startDate,endDate) {
            return ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accId+ "/sim/" + iccId +"/callDataRecords?startDate=" + startDate+ "&endDate=" +endDate+"&";
        }
        function getExportLHdataUrl (accId,iccId,startDate,endDate) {
            return ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accId+ "/sim/" + iccId + "/locations?startDate=" + startDate+ "&endDate=" +endDate+"&";
        }
        function getSIMs(queryParam,accountId) {
            var accountId  = ( accountId !== undefined ) ? accountId : sessionStorage.getItem('accountId');
            var headerAdd=['SIM_READ'];
            var config = {
                cancellable : 'searchApi'
            };
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+ accountId +"/sims", 'GET',headerAdd,queryParam,undefined,config);
        }

        function getSIMsByUserId(userId,queryParam) {
            var headerAdd=['SIM_READ'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+(sessionStorage.getItem('rootAccountId')?sessionStorage.getItem('rootAccountId'):sessionStorage.getItem('accountId'))+"/user/"+userId+'/sims/', 'GET',headerAdd,queryParam);
        }

        function updateSIM(SIM) {
            SIM.type = "Sim";
            var headerAdd=['SIM_UPDATE_ASSOCIATION'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_API_BASE + "account/"+ sessionStorage.getItem('accountId') + "/sims", 'PUT',headerAdd,false,SIM);
        }

        function updateSIMs(SIMs) {
            var headerAdd=['SIM_UPDATE_ASSOCIATION'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + 'accounts/account/'+sessionStorage.getItem('accountId')+'/sims', 'PUT',headerAdd,false,SIMs);
        }


        function updateSIMsStatus( SIMs, accountId, baseversion2) {
            var accId = (accountId) ? accountId : sessionStorage.getItem('accountId');

            if (baseversion2) {
                var headerAdd = ['PROVISIONING_SIMS_BLOCK_UNBLOCK'];
                return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE_2 + "accounts/account/" + accId+ "/sims/status", 'PUT', headerAdd, false, SIMs);
            } else {
                var headerAdd = ['SIM_ACTIVATION'];
                return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/" + accId + "/sims/status", 'PUT', headerAdd, false, SIMs);
            }
        }

        function deleteSIMs(data, accountId) {
            var accId = (accountId) ? accountId : sessionStorage.getItem('accountId');
            var headerAdd = ['PROVISIONING_SIMS_DELETE'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/" + accId + "/sims/delete", 'POST', headerAdd, false, data);
        }

        function refreshDataUsage(){
            var headerAdd=['REFRESH_DATAUSAGE'];
            var accId = sessionStorage.getItem('accountId');
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE  + "accounts/account/" + accId  + "/sims/datausage/refresh", 'POST',headerAdd);
        }

        function getExportDataUrl(uid, accId){
            var accountId = accId ? accId : sessionStorage.getItem('accountId');
            var exportDataUrl = ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/sims?";

            if(uid){
                exportDataUrl = ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+(sessionStorage.getItem('rootAccountId')?sessionStorage.getItem('rootAccountId'):accountId)+"/user/"+uid+'/sims?'
            }

            return exportDataUrl;
        }
        function getSimDetail(accountId,SIMId,queryParam){
            var headerAdd=['SIM_READ'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/sim/"+SIMId, 'GET',headerAdd,queryParam);
        }

        function getSIMLocationHistory(accountId,SIMId,queryParam) {
            var headerAdd=['SIM_LOCATION_READ'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/sim/"+SIMId+"/locations",'GET',headerAdd,queryParam);
        }

        function getSIMCdrs(accountId,SIMId,queryParam) {
            var headerAdd=['SIM_CDR_READ'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/sim/"+SIMId+"/callDataRecords",'GET',headerAdd,queryParam);
        }
        function switchIMSIMode(accountId,simsConf){
            var headerAdd=['SIM_EDIT_ADVANCED'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+ accountId +"/sims/conf", 'PUT',headerAdd,false,simsConf);
        }

        function switchIMSI(accountId,SIMId,imsiConf){
            var headerAdd=['SIM_EDIT_ADVANCED'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+ accountId +"/sim/"+SIMId+"/switchImsi", 'POST',headerAdd,false,imsiConf);
        }
        function getSIMOTAHistory(accountId,SIMId,queryParam){
            var headerAdd=['SIM_READ_ADVANCED'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/sim/"+SIMId+"/otaMessages",'GET',headerAdd,queryParam);
        }
        function getSIMOperationsHistory(accountId,SIMId,queryParam){
            var headerAdd=['PROFILE_READ_ADVANCED'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/sim/"+SIMId+"/operation/history",'GET',headerAdd,queryParam);
        }

        function getSIMVersions(accountId){
            var headerAdd=['GET_ROAMING_PROFILE'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/sims/versions",'GET',headerAdd);

        }

        function updateSIMListStatus(sims, status){
            var SIMList = [];

            for (var i = 0; i < sims.length; i++) {
                var ref = {};
                ref.status = status;
                ref.type = "SimStatus";
                ref.simId = sims[i].simId;
                SIMList.push(ref);
            }
            return SIMList;
        }

        function updateStatus(status,sims,scope,accountId,fromSuport){

            var SIMList = updateSIMListStatus(sims, status);
            var SIMListReq = {type: "SimStatusList", list: SIMList};

            updateSIMsStatus(SIMListReq,accountId, fromSuport).success(function () {
                scope.dataTableObj.refreshDataTable(false);

                ( SIMList.length > 1 )?commonUtilityService.showSuccessNotification("All selected SIMs have been updated.") :commonUtilityService.showSuccessNotification("Selected SIM has been updated.");
            }).error(function (response) {
                commonUtilityService.showErrorNotification(response.errorStr);
            });
        }

        function prepareDeleteSimList(sims){
            var SIMList = [];

            for (var i = 0; i < sims.length; i++) {
                var ref = {};
                ref.type = "DeleteSimInfo";
                ref.simId = sims[i].simId;
                SIMList.push(ref);
            }
            return SIMList;
        }

        function deleteSimOperation(sims,scope,accountId) {
            var simList = prepareDeleteSimList(sims)
            var deleteSimListRequest = {type: 'DeleteSimInfoList', list: simList};

            deleteSIMs(deleteSimListRequest, accountId).success(function () {
                scope.dataTableObj.refreshDataTable(false);

                ( simList.length > 1 )?commonUtilityService.showSuccessNotification("All selected SIMs have been submitted for delete operation.") :commonUtilityService.showSuccessNotification("Selected SIM has been submitted for delete operation.");
            }).error(function (response) {
                commonUtilityService.showErrorNotification(response.errorStr);
            });
        }

        function resendActivationLink(sims, scope) {
            var SIMList = [];

            for (var i = 0; i < sims.length; i++) {
                var ref = {};
                var sim = sims[i];
                ref.type =   sim.type !== undefined ? sim.type : "Sim";
                ref.simId =  sim.simId;
                ref.toUserId = sim.user.userId;
                SIMList.push(ref);
            }
            var SIMListReq = {type: "SimList", list: SIMList};
            updateSIMs(SIMListReq).success(function () {
                scope.dataTableObj.refreshDataTable(false);
                if( sims.length > 1 ){
                    commonUtilityService.showSuccessNotification("Activation link has been resent for all selected SIMs");
                } else {
                    commonUtilityService.showSuccessNotification("Activation link has been resent for selected SIM");
                }
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });

        }

        function showVerificationModal(model0bj, sims, scope, accountId, fromSupport) {
            var func;
            if(model0bj.updateStatus == 'DELETE'){
                func = function(){
                    deleteSimOperation(sims,scope, accountId);
                    analytics.sendEvent(model0bj.analyticObj);
                };
            }else if(model0bj.updateStatus == 'RESEND_LINK'){
                func = function(){
                    resendActivationLink(sims, scope);
                    analytics.sendEvent(model0bj.analyticObj);
                };
            }else {
                func = function(){
                    updateStatus(model0bj.updateStatus,sims,scope, accountId,fromSupport);
                    analytics.sendEvent(model0bj.analyticObj);
                };
            }

            scope.verificationModalObj.updateVerificationModel(model0bj.attr,func);
            scope.verificationModalObj.addVerificationModelMessage(model0bj.msg);

            $('#verificationModal').modal('show');

        }

        function bulkSIMactions(sims, scope, modelKey, accountId,fromSupport) {
            var model0bj = bulkActiomap[modelKey];
            var originalLen = sims.length;
            var invalidOperation = 0;
            var invalidConsumerEsimOperation = 0;

            /**
             * In case of delete operation, all prerequisites are being verified by backend.
             * For delete, sim should not have any data usage and alerts and it should be inactive.
             */
            if(model0bj.updateStatus == 'DELETE'){
                showVerificationModal(model0bj, sims, scope, accountId, fromSupport);
            }else{
                //  check whether the sims are already in blocked state
                commonUtilityService.filterSelectedRows(sims,function(sim){
                    if(sim.deviceType === 'ESIM_CONSUMER' && model0bj.updateStatus === 'RESEND_LINK'){
                        return sim.consumerEsimInfo.consumerEsimProfileStatus !== model0bj.simProfileStatus;
                    }else{
                        if(sim.deviceType === 'ESIM_CONSUMER' && sim.consumerEsimInfo.consumerEsimProfileStatus === model0bj.simProfileStatus){
                            invalidConsumerEsimOperation++;
                            return true;
                        }
                        else if((sim.status=='ACTIVE')&&(model0bj.updateStatus=='UNBLOCK')) {
                            return true;
                        }
                        else if((sim.status=='BLOCKED')&&(!((model0bj.updateStatus=='UNBLOCK') || (model0bj.updateStatus=='BLOCK') ))) {
                            invalidOperation++;
                            return true;
                        }
                        else {
                            return sim.status == model0bj.simStatus;
                        }
                    }

                });

                if((sims.length == 0) && (invalidConsumerEsimOperation > 0)){
                    return (originalLen > 1)? commonUtilityService.showErrorNotification(bulkActiomap.InvalidConsumerSIMOperation.replicationMsgPlural): commonUtilityService.showErrorNotification(bulkActiomap.InvalidConsumerSIMOperation.replicationMsgSingular);
                }
                if( (sims.length == 0) && (invalidOperation > 0)) {
                    return (originalLen > 1)? commonUtilityService.showErrorNotification(bulkActiomap.InvalidOperation.replicationMsgPlural): commonUtilityService.showErrorNotification(bulkActiomap.InvalidOperation.replicationMsgSingular);
                } else if(sims.length == 0) {
                    return (originalLen > 1)? commonUtilityService.showErrorNotification(model0bj.replicationMsgPlural): commonUtilityService.showErrorNotification(model0bj.replicationMsgSingular);
                }

                showVerificationModal(model0bj, sims, scope, accountId, fromSupport);
            }

        }


    }
})();

