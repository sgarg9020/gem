'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('SIMsCtrl', SIMs);

    SIMs.$inject = ['$scope','$rootScope','$compile','$state','dataTableConfigService','SIMService', 'GroupsService', 'commonUtilityService','loadingState','$sanitize','SettingsService', 'analytics','AuthenticationService','simsCommonFactory','$location','SIMConstants'];

    /**
     * @func SIMs()
     * @param $scope
     * @param $rootScope
     * @param $compile
     * @param dataTableConfigService
     * @param SIMService
     * @param GroupsService
     * @param commonUtilityService
     * @param loadingState
     * @param $sanitize
     * @param SettingsService
     * @param analytics
     * @constructor
     */
    function SIMs($scope,$rootScope,$compile,$state,dataTableConfigService,SIMService,GroupsService, commonUtilityService,loadingState,$sanitize,SettingsService,analytics,AuthenticationService,simsCommonFactory,$location,SIMConstants) {

        var sims = this;
        var uid;
        var filterValue;
        $scope.installationStatusFilters = simsCommonFactory.getInstallationStatusFilters();
        /**
         * Start up logic for controller
         */
        function activate(){
            clearDataTableStatus(['DataTables_simsDataTable_']);
            $scope.emptyTableMessage = "No SIMs available.";
            sims.bulkGroupUpdate = bulkGroupUpdate;
            sims.bulkGroupRemove = bulkGroupRemove;
            sims.bulkAction = bulkAction;
            sims.assignSIM = assignSIM;
            sims.unassignSIM = unassignSIM;
            sims.refreshDataUsage = refreshDataUsage;
            sims.getSimCreditLimit = getSimCreditLimit;
            sims.moveSIMsToParent = moveSIMsToParent;
            sims.moveSIMsToAccount = moveSIMsToAccount;

            $scope.dataTableObj = {};
            $scope.groupModalObj = {};
            $scope.graphObj = {};
            $scope.updateAssigneeModalObj = {};
            $scope.zoneStatusModalObj = {};

            $scope.dataTableObj.tableID = "simsDataTable";
            $scope.dataTableObj.controlID = "datatable-controls";

            $scope.deviceType = $state.current.data.deviceType;
            // Prepare simsCommonFactory with controller scope
            simsCommonFactory.setScope($scope);
            $scope.sim = [];
            var simCDRReadAllowed = AuthenticationService.isOperationAllowed(['SIM_CDR_READ']);
            var isEditingAllowed = AuthenticationService.isOperationAllowed(['SIM_UPDATE_ASSOCIATION']);

            var userData = JSON.parse(sessionStorage.getItem("gs-showSimsAssignedToCurrentUser"));
            if(userData){
                filterValue = userData.firstName;
                uid = userData.userId;
                sims.searchValue = filterValue;
            }else{
                uid = null;
            }

            var filtersConfig;
            if($scope.deviceType === SIMConstants.deviceType.ESIM_CONSUMER.value){
                filtersConfig = { selectedDisplayLength: {type:'local', dbParam: 'count', defaultValue:25}, installationStatus: {type:'local', dbParam: 'consumerEsimProfileStatus', defaultValue: 'All' }  };
            }else{
                filtersConfig = { selectedDisplayLength: {type:'local', dbParam: 'count', defaultValue:25} };
            }

            dataTableConfigService.configureFilters($scope.dataTableObj, filtersConfig);

            sims.exportData={
                url: SIMService.getExportDataUrl(uid)
            };

            sims.zoneStatus = function(index){
                var sim = $scope.simsList[index];
                simsCommonFactory.showZoneStatusModal(sim,sims.zonesCount);
            };

            var simIDLabel = ($scope.deviceType === SIMConstants.deviceType.pSIM.value) ? 'ICCID' : 'EID';

            $scope.dataTableObj.tableHeaders = [
                {"value": "", "DbValue":""},
                {"value": "CID", "DbValue":"cId"},
                {"value": simIDLabel, "DbValue":"simId"},
                {"value": "No. of Installed Profiles", "DbValue":"installedEuiccProfilesCount"},
                {"value": "Status", "DbValue":"simStatus"},
                {"value": "Installation Status", "DbValue":"consumerEsimProfileStatus"},
                {"value": "Zone(s) Access", "DbValue":""},
                {"value": "Allocation", "DbValue":"limitKB"},
                {"value": "Nickname","DbValue":"nickName"},
                {"value": "Assigned User","DbValue":"firstName"},
                {"value": "Group","DbValue":"accountName"},
                {"value": "Usage", "DbValue":"datausage"},
                {"value": "Actions", "DbValue":""}
            ];
            $scope.dataTableObj.dataTableOptions = {
                order: [[ 11, "desc" ]],
                search: {
                    search: sims.searchValue
                },
                createdRow: function (row,data) {
                    $(row).attr("id",'SID_'+data.simId);
                    $compile(row)($scope);
                },
                fnServerData: function (sSource, aoData, fnCallback) {

                    if(aoData){
                        sims.searchValue = aoData[5].value.value;
                    }
                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    queryParam.deviceType = $scope.deviceType;
                    if(queryParam.consumerEsimProfileStatus && queryParam.consumerEsimProfileStatus.toUpperCase() === 'ALL'){
                       delete queryParam.consumerEsimProfileStatus;
                    }
                    $scope.row =[];

                    if(filterValue && filterValue != sims.searchValue){
                        uid = null;
                        sessionStorage.removeItem("gs-showSimsAssignedToCurrentUser");
                        sims.exportData.url = SIMService.getExportDataUrl(uid);
                    }

                    sims.exportData.startIndex = queryParam.startIndex;
                    sims.exportData.count = queryParam.count;
                    sims.exportData.sortBy = queryParam.sortBy;
                    sims.exportData.sortDirection = queryParam.sortDirection;
                    sims.exportData.search = queryParam.search;
                    sims.exportData.deviceType = queryParam.deviceType;

                    var simResponse;
                    $scope.isDisableRefreshBtn = false;
                    $scope.isDisable = false;
                    AuthenticationService.getEnterpriseAccountDetails(sessionStorage.getItem('accountId')).success(function (response) {
                        $scope.setHierarchicalData(response.accountType == RESELLER_ACCOUNT_TYPE, $scope.isResellerRoot || response.accountType == RESELLER_ACCOUNT_TYPE, response.accountType == SUB_ACCOUNT_TYPE);
                        if (response.accountType == RESELLER_ACCOUNT_TYPE){
                            sessionStorage.setItem('rootResellerAccountId', response.accountId);
                        }
                    });
                    if(uid){
                        queryParam.search = '';
                        simResponse = SIMService.getSIMsByUserId(uid,queryParam);
                    }else{
                        simResponse = SIMService.getSIMs(queryParam);
                    }

                    simResponse.success(function(response){
                        mapResponseToDataTable(response, queryParam, fnCallback);
                    }).error(function(data){
                        $scope.isDisable = !$scope.simsList ||  $scope.simsList.length == 0;
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"sims");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });

                },
                columns:[
                    {
                        width:"60px",
                        orderable:false
                    },
                    {
                        width:"170px",
                        orderable:true
                    },
                    {
                        width:"170px",
                        orderable:true
                    },
                    {
                        width:"80px",
                        orderable:false
                    },
                    {
                        width:"80px",
                        orderable:true
                    },
                    {
                        width:"80px",
                        orderable:false
                    },
                    {
                        width:"80px",
                        orderable:false
                    },
                    {
                        width:"150px",
                        orderable:true
                    },{
                        width:"150px",
                        orderable:true
                    },
                    {
                        orderable:true
                    },
                    {
                        orderable:true
                    },
                    {
                        width:"75px",
                        orderable:true
                    },
                    {
                        width:"75px",
                        orderable:false
                    }
                ],
                columnDefs: [{
                    "targets": 0,
                    className: "checkbox-table-cell",
                    "render": function ( data, type, row, meta) {
                        return '<div gs-has-permission="SIM_ACTIVATION,SIM_UPDATE_ASSOCIATION" class="checkbox check-success large no-label-text hideInReadOnlyMode"><input type="checkbox" value="1" id="checkbox_'+row.simId+'" gs-index="'+meta.row+'"><label ng-click="checkBoxClicked($event)" id="label_'+row.simId+'"></label></div>';
                    }
                },{
                    "targets": 1,
                    "render": function (data, type, row) {
                        if(simCDRReadAllowed) {
                            return '<a ui-sref="app.simDetail({iccId:\'' + row.simId + '\'})" class="cursor">' + row.cId + '</a>';
                        }
                        else{
                            return row.cId;
                        }
                    }
                },{
                    "targets": 2,
                    "render": function (data, type, row) {
                        var secondaryId = ($scope.deviceType === SIMConstants.deviceType.ESIM_CONSUMER.value) ? row.consumerEsimInfo.mappedConsumerEid : row.simId;
                        return secondaryId ? secondaryId : '';
                    }
                },{
                    "targets": 3,
                    "render": function (data, type, row) {
                        var eUICCDetails = row.eUICCDetails;
                        var noOfInstalledProfiles = eUICCDetails ? eUICCDetails.installedEuiccProfilesCount : '';
                        return "<span class='nowrap'>"+noOfInstalledProfiles+"</span>";
                    }
                },{
                    "targets": 4,
                    "render": function (data, type, row) {
                        var simStatus = SIMConstants.status[row.status].label;
                        return '<div class="status-wrapper"><span class="status '+( row.status  == "ACTIVE" ? "active" : "inactive" )+'"></span><span class="status-text">'+(  simStatus  ) +'</span></div>';
                    }
                },{
                    "targets": 5,
                    "render": function (data, type, row) {
                        var installationStatus = row.consumerEsimInfo ? SIMConstants.installationStatus[row.consumerEsimInfo.consumerEsimProfileStatus] : '';
                        var status = installationStatus ? installationStatus.class : '';
                        var displayableStatus = installationStatus ? installationStatus.label : '';
                        return '<div class="status-wrapper"><span class="status '+( status )+'"></span><span class="status-text">'+(  displayableStatus  ) +'</span></div>';
                    }
                },{
                    "targets": 6,
                    "render": function (data, type, row, meta) {
                        var zoneStatus = simsCommonFactory.getZoneStatusForSim(row.zoneConnectivityStatus,sims.zonesCount);
                        return ($rootScope.isAlertEnabled) ? '<div class="status-wrapper status-as-link cursor" ng-click="sims.zoneStatus('+ meta.row +')"><span class="status '+( zoneStatus  == "Full Access" ? "full-access" : (zoneStatus  == "Partial Access" ? "partial-access" : "no-access") )+'"></span><span class="status-text">'+zoneStatus+'</span></div>' : '';
                    }
                },{
                    "targets": 7,
                    "render": function (data, type, row) {
                        $scope.sim[ row.simId ] =  row;
                        return  ($rootScope.isAlertEnabled) ? '' : isEditingAllowed ? '<div class="allocationContainer"><gs-edit-tool id=\'"allocation-'+ row.simId  +'"\' target-field="\'allocation\'" alloc=\'"'+  getSimCreditLimit(row ) +'"\' on-save="updateSIMAllocation" on-error="showErrMsg" on-success="showSuccessMsg" /></div>' : getSimCreditLimit(row);

                    },
                    "defaultContent": '350'

                    },{
                    "targets": 8,
                    "render": function (data, type, row) {
                        $scope.sim[row.simId] =  row;
                        var nickname = row.nickName;
                        return isEditingAllowed ? '<div class="nicknameContainer"><gs-edit-tool id=\'"nickname-'+ row.simId +'"\' target-field="\'name\'" name="' + nickname +'" on-save="updateSIMNickname" on-error="showErrMsg" on-success="showSuccessMsg" /></div>' : nickname;
                    }
                },{
                    "targets": 9,
                    "render": function (data, type, row) {
                        if(row.user){
                            var firstName = $sanitize(row.user.firstName);
                            var lastName = $sanitize(row.user.lastName);
                        }

                        return( !$.isEmptyObject(row.user) ? "<span class='nowrap'>"+firstName+' '+lastName+"<span>":'Unassigned');
                    },
                    "defaultContent": 'Unassigned'
                },{
                    "targets": 10,
                    "render": function (data, type, row) {
                        var accountName = $sanitize(row.account.accountName);
                        return( accountName ? "<span class='nowrap'>"+accountName+"</span>" : 'Default');
                    },
                    "defaultContent": 'Default'
                },{
                    "targets": 11,
                    "render": function (data, type, row) {
                        if($rootScope.isGSAdmin || !$rootScope.isGemLite){
                            return  '<a href="javascript:void(0)" ng-click=\'setSIM(\"'+ row.simId + '\");\' class="usage-value nowrap"><i class="fa fa-bar-chart"></i><span>'+((row.dataUsedInBytes!==undefined)?row.dataUsedInBytes === +row.dataUsedInBytes && row.dataUsedInBytes !== (row.dataUsedInBytes|0)? commonUtilityService.getUsageInMB()(row.dataUsedInBytes) : commonUtilityService.getUsageInMB()(row.dataUsedInBytes): 0)+'</span></a>';
                        }
                        else{
                            return  '<span>'+((row.dataUsedInBytes!==undefined)?row.dataUsedInBytes === +row.dataUsedInBytes && row.dataUsedInBytes !== (row.dataUsedInBytes|0)? commonUtilityService.getUsageInMB()(row.dataUsedInBytes) : commonUtilityService.getUsageInMB()(row.dataUsedInBytes): 0)+'</span>';
                        }
                    },
                    "defaultContent": '0'

                },{
                    "targets": 12,
                    "render": function (data, type, row) {
                        var installationStatus = row.consumerEsimInfo ? row.consumerEsimInfo.consumerEsimProfileStatus : '';
                        if(installationStatus === SIMConstants.installationStatus.DELETED.value){
                            return  '<i class="fa fa-gear edit-ico"></i> Configure';
                        }else{
                            return  '<a class="nowrap" href="javascript:void(0)" ng-click=\'openView(\"/app/alerts/sim/'+row.simId+'\");\'><i class="fa fa-gear edit-ico"></i> Configure</a>';
                        }
                    },
                    "defaultContent": '0'

                }],
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ",
                    "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    "sEmptyTable": $scope.emptyTableMessage,
                    "sZeroRecords":$scope.emptyTableMessage
                },
                "processing": true,
                "rowCallback": function( row, data ) {
                    $scope.dataTableObj.showPreviousSelectedRows(data.simId, row);
                },
                "fnDrawCallback": function( oSettings ) {

                    if($scope.simsList)
                        dataTableConfigService.disableTableHeaderOps( $scope.simsList.length, oSettings );

                    var $table = $( "#" + $scope.dataTableObj.tableID );

                    // this is for hiding the allocation field for GEM LITE user
                    if($rootScope.isGemLite){
                        $table.DataTable().column(7).visible( false );

                        $table.DataTable().column(6).visible( false );
                        $table.DataTable().column(12).visible( false );
                    }else if(!$rootScope.isAlertEnabled){
                        $table.DataTable().column(6).visible( false );
                        $table.DataTable().column(12).visible( false );
                    }else{
                        $table.DataTable().column(7).visible( false );
                    }

                    //Hiding DeviceType and NoOfInstalledProfiles count columns(which are applicable only in case of IOT subscription)
                    if($scope.deviceType !== SIMConstants.deviceType.IOT_M2M.value){
                        $table.DataTable().column(3).visible( false );
                    }
                    if($scope.deviceType != SIMConstants.deviceType.ESIM_CONSUMER.value){
                        $table.DataTable().column(5).visible( false );
                    }
                    $table.DataTable().column(10).visible(!$scope.resellerOrSubAccount);

                    commonUtilityService.preventDataTableActivePageNoClick( $scope.dataTableObj.tableID );

                }

            };

            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

// get the groups list and make it available for  add to group modals
            /* Get the groups data */

            if(!$scope.resellerOrSubAccount){
                GroupsService.getGroups({
                    count: 100000,
                    startIndex: 0,
                    sortBy: "accountName",
                    sortDirection: "ASC"
                }).success(function (response) {
                    sims.groups = ( response.list !== undefined ? response.list : [] );
                }).error(function () {
                    console.log("error while fetching groups");
                });
            }


            if($rootScope.isAlertEnabled){
                SettingsService.getZonesCount().success(function (response) {
                    sims.zonesCount = response;
                }).error(function () {
                    console.log("Error while fetching zones count");
                });
            }


        }

        function mapResponseToDataTable(response, queryParam, fnCallback) {
            angular.element('#checkbox_action').prop('checked', false);
            var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
            var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
            //  if( datTabData.aaData.length == 0 ) {
            $scope.simsList = datTabData.aaData;
            if ($scope.simsList.length == 0) {
                $scope.isDisable = true;
                $scope.isDisableRefreshBtn = true;
            }
            if (oSettings != null) {
                fnCallback(datTabData);
            } else {
                loadingState.hide();
            }
        }

        // Problem - 1
        $scope.checkBoxClicked = function(event){
            $scope.dataTableObj.selectRow(event);
        };

        $scope.isDisableComponent = function(){
            return $scope.isDisable;
        };

        $scope.isDisableRefreshComponent = function(){
            return $scope.isDisableRefreshBtn;
        };

        $scope.setSIM = function(simId){
            $scope.graphObj.setSIM(simId);
            analytics.sendEvent({
                category: "Operations",
                action: "View SIM Data usage",
                label: "Sim Management"
            });
        };

        $scope.updateSIMNickname = function( simId, newNickname ){
            simId = simId.split( "-")[1];
            var sim = {  type:"SimList", list:updateNickname( simId, newNickname ) };
            analytics.sendEvent({
                category: "Operations",
                action: "Update SIM Nickname",
                label: "Sim Management"
            });
            return SIMService.updateSIMs( sim );
        };

        $scope.updateSIMAllocation = function( simId, newAllocation ){
            simId = simId.split( "-")[1];
            var sim = { type:"SimList", list:updateAllocation( simId, newAllocation ) };
            analytics.sendEvent({
                category: "Operations",
                action: "Update Allocation",
                label: "Sim Management"
            });
            return SIMService.updateSIMs( sim );
        };

        $scope.showErrMsg = function( errorMsg ) {
            commonUtilityService.showErrorNotification( errorMsg );
        };

        $scope.showSuccessMsg= function( msg ){
            commonUtilityService.showSuccessNotification( msg );

        };

        function getSimCreditLimit( sim ){

            if( sim.creditLimitList && sim.creditLimitList.list  && sim.creditLimitList.list[0].limitKB !== undefined ) {
                return commonUtilityService.getUsageInMB()( sim.creditLimitList.list[0].limitKB * 1024 );
            }
            return commonUtilityService.getUsageInMB()(sessionStorage.getItem( "defaultCreditLimitForSims") * 1024 );
        }

        $scope.updateSIMAssociation = function(rows, accId) {
            var SIMList = updateGroupDetail(rows, accId );
            var SIMListReq = {type: "SimList", list: SIMList};
            SIMService.updateSIMs(SIMListReq).success(function () {
                $scope.dataTableObj.refreshDataTable(false);
                if( SIMList.length > 1 ) commonUtilityService.showSuccessNotification("All selected SIMs have been updated.");
                else commonUtilityService.showSuccessNotification("Selected SIM has been updated.");
                $('#subAccountsModal').modal('hide');
            }).error(function(data){
                console.log('update SIMs failure' + data);
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

        /**
         @function processGroupsUpdate()
         @param {object} group - Groups object to be passed in
         @param {array} rows - Selected rows
         @desc - Update each selected SIM group to the parameter passed in

         */
        function processGroupsUpdate(group,rows){
            var accId = -1;

            /* Clean up our groups object */
            if(Object.getOwnPropertyNames(group).length == 0){
                group = null;

            } else {
                delete group.$$hashKey;
            }

            if( group != null ) {
                var newGName = group.group.accountName;
                accId = group.group.accountId;

                var len;
                var originalLen;
                len = rows.length;
                originalLen = len;

                commonUtilityService.filterSelectedRows(rows,function(row){
                    return (row.account !== null && newGName == row.account.accountName);
                });

                if( rows.length == 0 ) {

                    if (originalLen == 1) {
                        commonUtilityService.showErrorNotification("Selected SIM is already associated with the same Group!!");
                        return;
                    }
                    else if (originalLen > 1) {
                        commonUtilityService.showErrorNotification("All Selected SIMs are already associated with the same Group!!");
                        return;
                    }
                }

            } else {
                analytics.sendEvent({
                    category: "Operations",
                    action: "Remove From group",
                    label: "Sim Management"
                });
            }
            $scope.updateSIMAssociation(rows, accId);
        }

        function processUsersUpdate(users,sims){
            if(users != null){
                if(sims.length === 1){
                    var sim = sims[0];
                    if(sim.user){
                        var selectedUser = users[0];

                        if( sim.user.userId === selectedUser.userId){
                            commonUtilityService.showErrorNotification("Selected SIM is already associated with the same User!!");
                            return;
                        }
                    }
                }
                analytics.sendEvent({
                    category: "Operations",
                    action: "Assign User",
                    label: "Sim Management"
                });
            }else{
                analytics.sendEvent({
                    category: "Operations",
                    action: "Unassign User",
                    label: "Sim Management"
                });
            }


            var SIMList = updateUserDetail(sims, users);
            var SIMListReq = {type: "SimList", list: SIMList};
            SIMService.updateSIMs(SIMListReq).success(function () {
                $scope.dataTableObj.refreshDataTable(false);
                if( SIMList.length > 1 ) commonUtilityService.showSuccessNotification("All selected SIMs have been updated.");
                else commonUtilityService.showSuccessNotification("Selected SIM has been updated.");

            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });

        }


        function bulkGroupUpdate(rows) {
            $scope.groupModalObj.vfUpdateModal(rows, sims.groups, processGroupsUpdate);

            $('#updateGroupsModal').modal('show');

        }

        function checkSIMActiveStatus(sims) {
            if(!sims){
                return false;
            }

            for(var i=0; i <sims.length; i++){
                if(sims[i].status  === "ACTIVE"){
                    return true;
                }
            }

            return false;

        }

        function moveSIMsToParent(rows) {

            if(checkSIMActiveStatus(rows)){
                commonUtilityService.showErrorNotification((rows.length> 1)? 'Active SIMs cannot be moved.' : 'Active SIM cannot be moved.');
                return;
            }

            var msg = "Moving SIMs to the parent account";
            var attr = {
                header: 'Move SIMs to Parent',
                cancel: 'Cancel',
                submit: 'Move'
            };
            var func = function(){
                $scope.updateSIMAssociation(rows, -1);
                analytics.sendEvent({
                    category: "Operations",
                    action: "Move SIMs to Reseller account",
                    label: "Sim Management"
                });
            };

            $scope.verificationModalObj.updateVerificationModel(attr,func);
            $scope.verificationModalObj.addVerificationModelMessage(msg);

            $('#verificationModal').modal('show');
        }




        function moveSIMsToAccount(rows) {
            if(checkSIMActiveStatus(rows)){
                commonUtilityService.showErrorNotification((rows.length> 1)? 'Active SIMs cannot be moved.' : 'Active SIM cannot be moved.');
                return;
            }

            var obj = {
                header: "Moving SIMs to the account",
                submit: "Move",
                cancel: "Cancel"
            };
            $scope.subAccountsModalObj.subAccountsModalAttributes(obj, function (account) {
                var accId = account ? account.accountId : -1;
                $scope.updateSIMAssociation(rows, accId);
                analytics.sendEvent({
                    category: "Operations",
                    action: "Move SIMs to sub account",
                    label: "Sim Management"
                });
            });
            $rootScope.$emit("$onOpenAccountsModal", {});
            $('#subAccountsModal').modal('show');
        }


        function bulkGroupRemove(rows) {


            var msg = "The selected SIMs won't have any association with the group after the removal.";
            var attr = {
                id: 'verificationModal',
                header: 'Remove Group Association',
                cancel: 'Cancel',
                submit: 'Remove'
            };

            var originalLen = rows.length;
            commonUtilityService.filterSelectedRows(rows,function(row){

                if( row.account == null || typeof row.account == "undefined" ){
                    return true;
                }

                else {
                    if( row.account.type == "ENTERPRISE" ) {

                        return true;
                    }

                    else if( row.account.accountId === undefined ){
                        return true;
                    }
                }
            });



            if( rows.length == 0 ) {
                if (originalLen == 1) {
                    commonUtilityService.showErrorNotification("Selected SIM can't be removed from Enterprise Group!!");
                    return;
                }

                else if (originalLen > 1) {
                    commonUtilityService.showErrorNotification("Selected SIMs can't be removed from Enterprise Group!!");
                    return;

                }
            }

            var func = function(){processGroupsUpdate({},rows);};

            $scope.verificationModalObj.updateVerificationModel(attr,func);
            $scope.verificationModalObj.addVerificationModelMessage(msg);

            $('#verificationModal').modal('show');

        }

        function bulkAction(rows,modelKey) {

            return SIMService.bulkSIMactions(rows,$scope,modelKey, $scope.enterpriseId,false);

        }

        function updateGroupDetail(rows, accId ) {
            var SIMList = [];
            for (var i = 0; i < rows.length; i++) {
                var ref = {};

                ref.type =  ( rows[i].type !== undefined ? rows[i].type : "Sim" );
                ref.simId =  rows[i].simId;


                ref.toAccount = accId;
                SIMList.push(ref);
            }
            return SIMList;
        }

        function updateUserDetail(sims, users ) {
            var simRequestList = [];
            for (var i = 0; i < sims.length; i++) {
                var ref = {};

                ref.type =  ( sims[i].type !== undefined ? sims[i].type : "Sim" );
                ref.simId =  sims[i].simId;

                if( users != null ) {
                    ref.toUserId = users[i].userId;
                }else {
                    ref.toUserId = -1;
                }
                simRequestList.push(ref);
            }
            return simRequestList;
        }

        function updateAllocation( simId, allocation ){

            var simList = [];
                var ref = {};
                var row = $scope.sim[ simId ];
                ref.type =  ( row.type !== undefined ? row.type : "Sim" );
                ref.simId =  row.simId;
                ref.nickName= row.nickName;

                if( allocation != null ) {
                    ref.creditLimitList = row.creditLimitList;
                    ref.creditLimitList.list[0].limitKB  = Number( allocation ) * 1024;
                }
                simList.push(ref);

            return simList;


        }

        function updateNickname(simId, nickName ) {
            var simList = [];
                var row = $scope.sim[ simId ];

                var ref = {};

                ref.type =  ( row.type !== undefined ? row.type : "Sim" );
                ref.simId =  row.simId;

                if( nickName != null ) {

                    ref.nickName  = nickName;
                }
                simList.push(ref);


            return simList;


        }

        /**
         @func updateSIMsAssignee()
         @param {array) rows - Selected Rows
         @desc - updateSIMsAssignee will update the user associated for selected SIMs if update is true. updateSIMsAssignee will
         remove the user assosication for selected SIMs if update is false.
         */
        function assignSIM(rows){
                $scope.updateAssigneeModalObj.updateAssigneeModalAttributes({header: "Assign User"});
                $scope.updateAssigneeModalObj.updateAssigneeModalSubmit(rows,processUsersUpdate);
        }

        function unassignSIM(rows){
                //VERIFICATION MODAL
                var msg = "Unassigning a SIM will remove any association from user.";
                var attr = {
                    header: 'Remove User Association',
                    cancel: 'Cancel',
                    submit: 'Remove'
                };

            var originalLen = rows.length;


                commonUtilityService.filterSelectedRows(rows,function(row){

                    if(row.user == null || typeof row.user == "undefined"){
                        return true;
                    }
                    else {
                        if( typeof row.user.userId == "undefined") {
                            return true;
                        }
                    }


                });

                if( rows.length == 0 ) {
                    if (originalLen == 1) {
                        commonUtilityService.showErrorNotification("Selected SIM is not associated with any User!!");
                        return;
                    }

                    else if (originalLen > 1) {
                        commonUtilityService.showErrorNotification("Selected SIMs are not associated with any User!!");
                        return;

                    }
                }

            var func = function () {
                processUsersUpdate(null,  rows);

            };

            $scope.verificationModalObj.updateVerificationModel(attr, func);
            $scope.verificationModalObj.addVerificationModelMessage(msg);

            $('#verificationModal').modal('show');

        }

        function refreshDataUsage(){

            analytics.sendEvent({
                category: "Operations",
                action: "Refresh Data Usage",
                label: "Sim Management"
            });

            SIMService.refreshDataUsage().success(function(response){
                commonUtilityService.showSuccessNotification("Refresh data usage is successfully triggered ");
                $scope.isDisableRefreshBtn = true;
                commonUtilityService.refreshDataUsageMonitor.monitor(response.sessionId);

            }).error(function(data){
                console.log('Refresh data usage is falied' + data);
                commonUtilityService.showErrorNotification(data.errorStr);

            });
        }

        activate();
        loadingState.hide();
    }
})();





