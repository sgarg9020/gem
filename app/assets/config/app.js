(function() {
    "use strict";

    angular.module('app', [
        'ui.router',
        'ui.utils',
        'oc.lazyLoad',
        'ngSanitize',
        'bckrueger.angular-currency',
        'ncy-angular-breadcrumb'
        // 'ngDropzone'
    ]);
})();

(function(){
    angular.module('app').config(['$sceDelegateProvider',function($sceDelegateProvider) {
        $sceDelegateProvider.resourceUrlWhitelist([
            // Allow same origin resource loads.
            'self',
            // Allow loading from CDN.
            '*://cdn.gigsky.com**',
            '*://cdn-test.gigsky.com**',
            '*://cdn-prod.gigsky.com**',
            '*://cdn-stage.gigsky.com**'
        ]);
        console.log($sceDelegateProvider.resourceUrlWhitelist());
    }]);

    angular
        .module('app')
        .controller('AppCtrl', AppController);
    angular
        .module('app')
        .factory('EPHttpInterceptors',['$location','$q','$rootScope','loadingState','$window','$injector',EPHttpInterceptors]);

    angular
        .module('app').filter('encodeURIComponent', function() {
        return window.encodeURIComponent;
    });
    angular
        .module('app').filter('gsDate',['$filter',function ($filter) {
        return function (input,format) {
            if (input == null) {
                return "";
            }
            var timezone = sessionStorage.getItem('timezone');
            var timezoneArr = timezone.split(':');
            var sign = parseInt(timezoneArr[0])?parseInt(timezoneArr[0])<0?-1:1:0;
            var inputDate = getDateObj(input);
            inputDate.setTime(inputDate.getTime()+sign*(Math.abs(parseInt(timezoneArr[0])*60*60*1000)+Math.abs(parseInt(timezoneArr[1])*60*1000)));

            sessionStorage.getItem('timezone');
            var todaysDate = new Date();

            if(new Date(inputDate).setHours(0,0,0,0) == todaysDate.setHours(0,0,0,0))
            {
                return 'Today';
            }
            var _date = $filter('date')(new Date(inputDate), format || 'MM/dd/yyyy \'at\' h:mma');
            return _date;
        };
    }]);
    angular.module('app')
        .controller('ErrCtrl',ErrCtrl);

    // Add $state to the $rootscope so it can be referenced to hide markup in nested views
    angular.module('app')
        .run(['$state','$rootScope',function ($state,$rootScope) {
            $rootScope.$state = $state;
        }]);


    angular.module('app')
        .factory('HttpPendingRequestsService',PendingRequestsService);

    PendingRequestsService.$inject = ['$q','loadingState'];
    /**
     * Factory to return new timeout promises and stores references to them.
     * @param $q
     * @returns {{newTimeout: newTimeout, cancelAll: cancelAll}}
     * @constructor
     */
    function PendingRequestsService($q,loadingState){
        var pendingPromises = { general:[] };
        var factory = {
            newTimeout: newTimeout,
            cancelAll: cancelAll,
            excludeSuccededPromise: excludeSuccededPromise
        };

        /**
         * creates new timeout promise and store it in an array.
         * @param cancellable
         * @returns {*}
         */
        function newTimeout(cancellable) {
            var cancelPromise = $q.defer();
            if(cancellable){
                if(!pendingPromises.hasOwnProperty(cancellable)){
                    pendingPromises[cancellable] = [];
                }
                pendingPromises[cancellable].push(cancelPromise);
            }else{
                pendingPromises.general.push(cancelPromise);
            }
            return cancelPromise.promise;
        }

        /**
         * Cancels all timeout promises based on state change
         * @param cancellable
         */
        function cancelAll(cancellable) {
            if(cancellable){
                cancelAllPromises(pendingPromises[cancellable]);
                delete pendingPromises[cancellable];
            }else{
                var pendingPromisesLength = 0;
                for(var property in pendingPromises){
                    pendingPromisesLength = pendingPromisesLength + pendingPromises[property].length;
                    cancelAllPromises(pendingPromises[property]);
                    if(property != 'general'){
                        delete pendingPromises[property];
                    }
                }
                (pendingPromisesLength > 0) ? loadingState.hide() : '';
            }
        }

        function excludeSuccededPromise(promise,cancellable){
            if(cancellable){
                var promiseList = pendingPromises[cancellable];
                spliceAnObjectFromArray(promise,promiseList);
            }else{
                var promiseList = pendingPromises.general;
                spliceAnObjectFromArray(promise,promiseList);
            }

        }

        function spliceAnObjectFromArray(promise,promiseList){
            for(var i=0;i<promiseList.length;i++){
                var promiseObj = promiseList[i];
                if(promise == promiseObj.promise){
                    promiseList.splice(i, 1);
                }
            }
        }

        /**
         * Resolves all timeout promises (Resolving will abort the request)
         * @param cancelPromises - array of timeout promises
         */
        function cancelAllPromises(cancelPromises) {
            angular.forEach(cancelPromises, function (cancelPromise) {
                cancelPromise.promise.isGloballyCancelled = true;
                cancelPromise.resolve();
            });
            if(cancelPromises){
                cancelPromises.length = 0;
            }
        }

        return factory;
    }


    AppController.$inject = ['$scope', '$rootScope', '$state','$location', '$window', 'AuthenticationService','notifications','$q','loadingState', 'analytics','pollingAPIService','HttpPendingRequestsService', '$timeout'];

    function AppController($scope, $rootScope, $state,$location, $window, AuthenticationService,notifications,$q,loadingState, analytics,pollingAPIService,httpPendingRequestsService,$timeout){

        // controllerAs with appObj
        var appObj = this;
        $rootScope.lazyLoadURL = LAZY_LOAD_BASE;


        /**
         * Start up logic for controller
         */
        function activate(){

            resetAppState();
            // Bindable members
            appObj.is = is;
            appObj.includes = includes;
            if(localStorage.getItem('menu-pin') === 'true'){
                $('body').addClass('menu-pin');
            }
            appObj.app = {
                layout: {
                    menuPin: false,
                    menuBehind: false,
                    gemLite: $rootScope.isGemLite
                }
            };
        }

        /**
         * resets all the rootScope variables by validating value stored in the sessionStorage
         */
        function resetAppState() {
            if (sessionStorage.getItem('alertSupported') === undefined) {
                $rootScope.isAlertEnabled = ALERTS_ENABLED;
            } else {
                $rootScope.isAlertEnabled = sessionStorage.getItem('alertSupported') == 'true' ? true : false;
            }

            $rootScope.pricingModelVersion = sessionStorage.getItem('pricingModelVersion');
            if ($rootScope.pricingModelVersion == undefined)
                $rootScope.pricingModelVersion = '1.1';

            $rootScope.isStorageNotSupported = false;
            try {
                var key = "test";
                sessionStorage.setItem(key, "test");
                sessionStorage.removeItem(key);
            } catch (exception) {
                $rootScope.isStorageNotSupported = true;
            }

            $rootScope.servicesDown = false;
            $rootScope.pageLoadingFailed = false;
            $rootScope.hasMultiAccount = sessionStorage.getItem('hasMultiAccount') === 'true' ? true : false;
            $rootScope.isGSAdmin = sessionStorage.getItem('isGSAdmin') === 'true' ? true : false;
            $rootScope.isGemLite = sessionStorage.getItem('accountSubscriptionType') === 'ENTERPRISE_LITE' ? true : false;
            $rootScope.isIotReseller = sessionStorage.getItem('accountSubscriptionType') === 'IOT' ? true : false;
            $rootScope.isSupport = sessionStorage.getItem('isSupport') === 'true' ? true : false;
            $rootScope.isNavigatedThroughAdmin = sessionStorage.getItem('isNavigatedThroughAdmin') === 'true' ? true : false;
            $rootScope.pageTitle = $rootScope.isSupport ? ADMIN_PORTAL_TITLE : ENTERPRISE_PORTAL_TITLE;

            var usageDimensionVal = sessionStorage.getItem('usageDimensionAnalytics');
            if(usageDimensionVal)
                analytics.setDimension(1,usageDimensionVal);

        }

        $scope.getCurrentYearForCopyright = function() {
            return new Date().getFullYear();
        };

        $scope.openView = function(path,q){
            $location.path(path);
            if(q)
                $location.search(q);
            else
                $location.search({});
        };

        $scope.goToState = function(state){
            $state.go(state);
        };

        $scope.getActivityState = function () {
            var cdrPermissions = ['READ_CDR_METRICS','SIM_CDR_READ_ADVANCED'];
            var isCdrTabAllowed = AuthenticationService.isOperationAllowed(cdrPermissions);
            var locationUpdatePermissions = ['READ_LOCATION_METRICS','SIM_LOCATION_READ_ADVANCED'];
            var isLocationTabAllowed = AuthenticationService.isOperationAllowed(locationUpdatePermissions);

            if (isCdrTabAllowed){
                return 'support.sim-activity.cdr';
            } else if(isLocationTabAllowed){
                return 'support.sim-activity.location-updates';
            }
        };

        function setResellerData(){
            $scope.resellerOrSubAccount = sessionStorage.getItem("isSubAccount") == 'true' || sessionStorage.getItem("isResellerAccount") == 'true';
            $scope.isResellerAccount = sessionStorage.getItem("isResellerAccount") == 'true' || (sessionStorage.getItem("rootResellerAccountId") && sessionStorage.getItem("rootResellerAccountId") == sessionStorage.getItem('accountId'));
            $scope.isSubAccount = sessionStorage.getItem("isSubAccount") == 'true';
            $scope.isResellerRoot = sessionStorage.getItem("isResellerRoot") == 'true';
        }

        setResellerData();

        $scope.setHierarchicalData = function(isResellerAccount, isResellerRoot, isSubAccount){
            if(isResellerAccount != undefined){
                sessionStorage.setItem("isResellerAccount", isResellerAccount);
            }
            if(isResellerRoot != undefined){
                sessionStorage.setItem("isResellerRoot", isResellerRoot);
            }
            if(isSubAccount != undefined){
                sessionStorage.setItem("isSubAccount", isSubAccount);
            }
            setResellerData();
        }

        $scope.onGemViewExit = function(){
            $rootScope.isNavigatedThroughAdmin = false;
            sessionStorage.setItem('isNavigatedThroughAdmin', false);

            var rootAccountId = sessionStorage.getItem("rootAccountId");
            var rootResellerAccountId = sessionStorage.getItem("rootResellerAccountId");
            var exitToState=sessionStorage.getItem("exit-to-state");
            $scope.setHierarchicalData(false, false, false);
            if (rootAccountId) {
                sessionStorage.setItem("accountId", rootAccountId);
                if(exitToState) {
                    var url = $state.href(exitToState).substr(1);
                    $location.path(url).search({saveState: true});
                } else
                    $location.path('/support/accounts/companies').search({saveState: true});;
            } else if(rootResellerAccountId) {
                sessionStorage.setItem("accountId", rootResellerAccountId);
                if(exitToState)
                    $state.go(exitToState);
                else
                    $state.go('app.sub-accounts');
            }
        }

        $rootScope.$on('onChangeAccount', function(event, args){
            changeAccount(args.accountId, args.updateDetail);
        });

        var invalidSubAccountRoutes = ['app/sub-accounts', '/info', 'reports/', '/data-usage-historical'];
        /**
         * @func changeAccount
         * @desc navigates to the GEM account
         * @param accountId - accountId of the account to which it needs to navigate
         */
        function changeAccount(accountId, updateDetail){
            AuthenticationService.getEnterpriseAccountDetails(accountId).success(function (response) {
                $scope.setHierarchicalData(response.accountType == RESELLER_ACCOUNT_TYPE, true, response.accountType == SUB_ACCOUNT_TYPE);

                AuthenticationService.setAlertsFeatureEnabled(response.alertVersionSupported);
                AuthenticationService.setPricingModelVersion(response.pricingModelVersionSupported);
                $('#subAccountsModal').modal('hide');
                var p = $location.absUrl();
                var navigateToSubAccountHome = false;
                for (var i=0; i<invalidSubAccountRoutes.length; i++){
                    if(p.indexOf(invalidSubAccountRoutes[i]) != -1){
                        navigateToSubAccountHome = true;
                        break;
                    }
                }
                if(navigateToSubAccountHome){
                    $location.path('/app/home');
                    //Header will not be re-activated on navigation. Use the function below to update details in header.
                    updateDetail();
                } else{
                    $state.reload();
                }
            }).error(function (data) {
                console.log("error while fetching enterprise details");
                dashboard.errorMessage = data.errorStr;
            });
        }


        $scope.loadState = function(state, q){
            var manualRefresh = false;
            if (q){
                if (state == $state.current.name && !angular.equals($state.params,q)) {
                    manualRefresh = true;
                }
            }
            else{
                q={};
            }

            if(manualRefresh){
                $state.go(state, q);
            }
            else{
                $state.go(state, q);
            }
            //Fix to hide alert notification
            $("#quickview").removeClass( "open");
        };


        $scope.goToHomePage = function()
        {
            if((window.location.hash).indexOf('#/auth/')!=-1 && !$rootScope.isSupport)
            {
                if(window.location.host.indexOf("stage")!=-1)
                    window.location.href =  "https://www-stage.gigsky.com/gigsky-enterprise-manager/";
                else
                    window.location.href =  "https://www.gigsky.com/gigsky-enterprise-manager/";
            }
        }
        $scope.clickLogo = function(){

            if($rootScope.isUserInGemPortal()){
                var locUrl  = "/";

                if((window.location.hash).indexOf('#/auth/') == 0){
                    var lh = window.location.hostname;
                    if (lh.indexOf('enterprise.gigsky.com') != -1) {
                        locUrl = '//gigsky.com';
                    }else if(lh.indexOf('enterprise-stage.gigsky.com') != -1){
                        locUrl = '//www-stage.gigsky.com';
                    }

                    if(ENTERPRISE_ENVIRONMENT == "p"){
                        locUrl = "//gigsky.com";
                    }else if(ENTERPRISE_ENVIRONMENT == "cs"){
                        locUrl = "//www-stage.gigsky.com";
                    }else {
                        locUrl = "/";
                    }
                    // redirect to the static page
                }
                else {
                    if($('body').hasClass('sidebar-open'));{
                        $('body').removeClass('sidebar-open')
                        $('.page-sidebar').removeClass('visible');
                    }

                    locUrl = $location.absUrl().split( "#")[0]+"#/app/home";
                    // redirect to homepage within the application
                }

                window.location.href =  locUrl;
            }

        };

        appObj.logout = function () {
            var userAccountLogoutPromise = AuthenticationService.userAccountLogout();
            var enterpriseAccountLogoutPromise = AuthenticationService.enterpriseAccountLogout();

            $q.all([userAccountLogoutPromise, enterpriseAccountLogoutPromise]).then(function (response) {
                clearDetails();
                notifications.closeNotifications();
            }, function (data) {
                clearDetails();
            });
        };

        $rootScope.$on('onLogOut',function(){
            AuthenticationService.clearSession();
            AuthenticationService.clearHttpHeaders();
        });

        function clearDetails(){
            $rootScope.$broadcast('onLogOut');

            if($rootScope.isSupport){
                var absUrl =  $location.absUrl();
                var basePath = $location.host();

                var gemIndex = absUrl.indexOf("/gem/");
                var baseIndex = absUrl.indexOf(basePath);

                var location = absUrl.substring(baseIndex+basePath.length,gemIndex);
                if(location.length == 0 && window.location.hostname.indexOf("gigsky.com")!=-1)
                    location = window.location.origin;
                window.location.href = location.length == 0 ? '/' : location;
            }else{
                $location.path("#/");
            }

        }

        // Checks if the given state is the current state
        function is(name){
            return $state.is(name);
        }

        // Checks if the given state/child states are present
        function includes(name) {
            return $state.includes(name);
        }

        /**
         * @func clearDataTableStatus
         * @param {array} tableIds //array of dataTableIds that needs to be cleared
         * @desc clears dataTables present in tableIds or all the dataTables from sessionStorage when the path has no saveState.
         *  also adds the saveState to that path when not present
         */
        window.clearDataTableStatus = function (tableIds,forceClear) {
            if (!$location.search().saveState || forceClear) {
                if(tableIds && tableIds.length){
                    var pathName = window.location.pathname;
                    for(var j=0; j<tableIds.length; j++){
                        var tableName = tableIds[j]+pathName;
                        sessionStorage.removeItem(tableName);
                    }
                }else{
                    this.console.warn('clearDataTableStatus should always be called by passing atleast one tableId. If not, all the table states in session are destroyed which might affect screens where table state has to be persisted.');
                    var i = sessionStorage.length;
                    while (i--) {
                        var key = sessionStorage.key(i);
                        if (key.indexOf('DataTables_') != -1) {
                            sessionStorage.removeItem(key);
                        }
                    }
                }
                $location.search({saveState: true});
            }
        }

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams)
        {
            $rootScope.$qparam = fromState;
            $(document).scrollTop(0);
            pollingAPIService.quit();
            $rootScope.pageLoadingFailed = false;
            $rootScope.servicesDown = false;
            loadingState.show();
            if(window.nv){
                window.nv.graphs = [];
            }



            if(toState.name.indexOf("auth.login")!=-1)
            {
                if(fromState.name.indexOf("auth.select-account") != -1){
                    AuthenticationService.clearSession();
                    AuthenticationService.clearHttpHeaders();
                }else if(AuthenticationService.isSessionValid()){
                    loadingState.hide();
                    event.preventDefault();
                    $state.go('app.home', {}, {reload: true, location: true});
                }
            }

            if((toState.name.indexOf('auth')==-1 && !AuthenticationService.isSessionValid()) || ((toState.name.indexOf('auth.select-account')!=-1) && !AuthenticationService.isUserSessionValid()))
            {
                loadingState.hide();
                event.preventDefault();
                $state.go('auth.login', {}, {reload: true, location: true});
            }



            if (toState !== fromState) {
                httpPendingRequestsService.cancelAll();
            }
        });

        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            var p = $location.path();
            loadingState.hide();
            if((p.indexOf('/app/sims') == -1) && sessionStorage.getItem("gs-showSimsAssignedToCurrentUser")){
                sessionStorage.removeItem("gs-showSimsAssignedToCurrentUser");
            }
            if((p.indexOf('/sim-cards') == -1) && sessionStorage.getItem("gs-showSimsAssignedToCurrentGapUser")){
                sessionStorage.removeItem("gs-showSimsAssignedToCurrentGapUser");
            }
            $("#enterpriseMsgAlert").hide();
            $rootScope.pageLoadingFailed = false;
            $rootScope.servicesDown = false;
            if(toState && toState.name && toState.name.indexOf("support.") == -1)
                analytics.trackPageView();
        });

        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
            loadingState.hide();
        });

        //to scroll graph modal content to top
        $('body').on('shown.bs.modal',function(){
            $('.donutOrPieModalContent').scrollTop(0);
        });

        $scope.onRequestDemoFormSubmit = function(form, callback ) {

            $scope.formSubmitSuccessMsg = null;
            $scope.formSubmitErrorMsg = null;

            console.log("Budget is "+$scope.budget);
            $scope.nameRequired =      $scope.emailRequired =  $scope.phoneRequired = null;

            //Form validation..
            var postData = {
                "type" : "ContactUsBean",
                "emailId" : form['email'],
                "name":$scope.contactName,
                //"company" : "N/A",
                "messageSubject":"GigSky Enterprise Solution Demo request - TESTing Gigsky enterprise demo request",  //solution for Business",
                "messageContent": "I am interested in Gigsky Enterprise Demo Request"
            }; //Prepare post data,,,

            //ensuant.toovia.ui.Utils.displayProgress();

            $http.post( "https://services.gigsky.com/api/v4/contactus", postData,{timeout:120000}).success(function (response) {

                //ensuant.toovia.ui.Utils.hideProgress();


                callback( "Message has been sent successfully.");


            }).error(function (data, status) {
                ensuant.toovia.ui.Utils.hideProgress();
                if(data && data.errorInt)
                {
                    callback( "", data.userDisplayErrorStr?data.userDisplayErrorStr:data.errorStr );
                }
                else {
                    callback( "", "Not able to send the message. Please try after some time." );
                }
            });
        };

        $rootScope.isUserInGemPortal = function(){
            var currentState = $state.current.name;
            var p = $location.absUrl();
            if(currentState.indexOf("support.") != -1 || (currentState.indexOf("auth.login") != -1 &&
                ((p.indexOf('admin.gigsky.com') != -1 || p.indexOf('admin-stage.gigsky.com') != -1 || p.indexOf('admin-beta.gigsky.com') != -1) || (p.indexOf('localhost') != -1 && p.indexOf('/gem') != -1)))){
                return false;
            }
            return true;
        }

        activate();

    }




    function EPHttpInterceptors($location,$q,$rootScope,loadingState,$window,$injector){

        var msie = parseInt(((/msie (\d+)/.exec(angular.lowercase(navigator.userAgent)) || [])[1]),10);
        if (isNaN(msie)) {
            msie = parseInt(((/trident\/.*; rv:(\d+)/.exec(angular.lowercase(navigator.userAgent)) || [])[1]),10);
        }

        return {

            request:function(config){
                config.timeout = 60000;
                var headers = config.headers;
                if(RBAC_ENABLED && config.url.indexOf('api/')!=-1 && config.url.indexOf('login')==-1){

                    var resp={};

                    var accessCheck=headers['accessCheck'];
                    delete config.headers.accessCheck;
                    if(accessCheck !== undefined && accessCheck !== null){
                        if (accessCheck.constructor === Array) {
                            var AuthService=$injector.get('AuthenticationService');
                            var isAllowed = AuthService.isOperationAllowed(accessCheck);
                            if(!isAllowed){
                                resp.data = {errorStr:"User doesn't have permission to access this feature",errorInt:100000};
                                console.log("User doesn't have permission to access this feature"+accessCheck);
                                resp.config = config;
                                return $q.reject(resp);
                            }
                        }
                    }
                }
                if(config.url.indexOf('api/')!=-1)
                {
                    var pendingRequestService=$injector.get('HttpPendingRequestsService');
                    if (!(config.config && config.config.noCancelOnRouteChange)) {
                        if (config.config && config.config.cancellable) {
                            config.timeout = pendingRequestService.newTimeout(config.config.cancellable);
                        }else{
                            config.timeout = pendingRequestService.newTimeout();
                        }
                    }



                    //Fix for IE caching issue.. This can be resolved on server end by disabling caching for specific set of queries.
                    if(config.method == "GET"){
                        var separator = config.url.indexOf('?') === -1 ? '?' : '&';
                        config.url = config.url+separator+'noCache=' + new Date().getTime();
                    }

                    $rootScope.authenticationFailure = false;
                    if(!((config.url.indexOf(ENTERPRISE_ANALYTICS_BASE) != -1 && ( config.url.indexOf('/users/user') != -1 || config.url.indexOf('/sims/sim') != -1 )) ||  (config.url.indexOf('/users/datausage/refresh/') != -1 || config.url.indexOf('/alerts/') != -1 && config.method == "GET")) && !(config.params && config.params.noPace)){
                        loadingState.show(true);
                    }

                    var headers = config.headers;
                    var token = sessionStorage.getItem("token");

                    if((config.url.indexOf('/login')!=-1) || (config.url.indexOf('/logout')!=-1) && (config.url.indexOf('/account') == -1)){
                        token = sessionStorage.getItem("userToken");
                    }
                    if(!token){
                        token = sessionStorage.getItem('tempToken');
                    }
                    if(!headers.token && token){
                        headers.token = token;
                    }
                    var lang = sessionStorage.getItem("currentLanguageCode");
                    if(lang)
                        angular.extend(headers,{"Accept-Language":lang});
                    if(config.config && config.config.contentType)
                        angular.extend(headers,{"Content-Type":config.config.contentType});

                }
                else
                if(config.url.indexOf('app/assets')!=-1)
                {
                    loadingState.show();
                }

                return config;
            },
            response:function(resp){
                if(resp.config.url.indexOf('app/assets')!=-1)
                {
                    loadingState.hide();
                }
                else
                if(resp.config.url.indexOf('api/')!=-1 && !(resp.config.url.indexOf(ENTERPRISE_ANALYTICS_BASE) != -1 && ( resp.config.url.indexOf('/users/user') != -1 || resp.config.url.indexOf('/sims/sim') != -1 )) && !(resp.config.params && resp.config.params.noPace)){
                    loadingState.hide();

                    var pendingRequestService=$injector.get('HttpPendingRequestsService');
                    if (!(resp.config.config && resp.config.config.noCancelOnRouteChange)) {
                        if (resp.config.config && resp.config.config.cancellable) {
                            pendingRequestService.excludeSuccededPromise(resp.config.timeout,resp.config.config.cancellable);
                        }else{
                            pendingRequestService.excludeSuccededPromise(resp.config.timeout);
                        }
                    }
                }

                return resp;
            },
            responseError:function(resp){


                if(resp.errorCode == 100000){
                    return $q.reject(resp);
                }
                if(resp.config.url.indexOf('api/')!=-1 && !(resp.config.url.indexOf(ENTERPRISE_ANALYTICS_BASE) != -1 && ( resp.config.url.indexOf('/users/user') != -1 || resp.config.url.indexOf('/sims/sim') != -1 )) && !(resp.config.params && resp.config.params.noPace)){
                    loadingState.hide();
                }
                else if(resp.config.url.indexOf('app/assets')!=-1)
                {
                    loadingState.hide();
                }
                //$rootScope.pageLoadingFailed = false;
                //$rootScope.servicesDown = false;
                if (resp.config.timeout && resp.config.timeout.isGloballyCancelled) {
                    resp.data = {errorInt:2,errorStr:'Cancelled on state change'};
                    return $q.reject(resp);
                }
                if(resp.status==0 && resp.config.method != "GET")
                {
                    resp.data = {errorInt:1,errorStr:'Unable to connect to GigSky\'s system.'};
                    return $q.reject(resp);
                }
                else if(resp.status == 0 && (resp.config.method == "GET"))
                {
                    loadingState.hide(true);
                    $rootScope.pageLoadingFailed = true;
                    $rootScope.servicesDown = false;
                    resp.data = {errorInt:1,errorStr:'Unable to connect to GigSky\'s system.'};
                    return $q.reject(resp);
                }

                var $state = $injector.get('$state');

                if(resp.data && resp.data.errorInt)
                {
                    if(resp.data.errorInt == 8000){
                        $rootScope.servicesDown = true;
                        $rootScope.pageLoadingFailed = false;
                        return $q.reject(resp);
                    }else if(resp.data.errorInt == 100000){
                        return $q.reject(resp);
                    }else if(resp.data.errorInt == '4708' && !$rootScope.isSupport){
                        //Operations are not allowed for accounts in suspended state & should go to the login screen with appropriate error
                        sessionStorage.removeItem( "token");
                        loadingState.hide(true);
                        $state.go('auth.login',{}, {reload: true,location:true});
                        $rootScope.formErrorMsg = resp.data.errorStr;
                        $rootScope.$broadcast('onLogOut');
                        return $q.reject(resp);
                    }

                }

                if ($state.current.name == "auth.unblockAccount" || $state.current.name == "auth.resetPassword"|| $state.current.name == "auth.forgotPassword"|| $state.current.name == "auth.changePassword")
                {
                    return $q.reject(resp);
                }

                var uid = sessionStorage.getItem('userId');

                if(resp.config.url.indexOf('api/')!=-1 && (resp.status==401 || uid == null ))
                {


                    if($state.current.name !="auth.login" &&  !$rootScope.authenticationFailure)
                    {
                        $rootScope.authenticationFailure = true;
                        sessionStorage.removeItem( "token");
                        loadingState.hide(true);
                        $state.go('auth.login',{}, {reload: true,location:true});
                        resp.data = {errorInt:1,errorStr:'Session has expired.'};

                        //to display message in login screen when CONCURRENT_LOGIN_COUNT has crossed
                        $rootScope.formErrorMsg = resp.data.errorStr;

                        //clearing session details and unregistering timers
                        $rootScope.$broadcast('onLogOut');
                    }
                    else{
                        if(resp.config.url.indexOf("login")==-1)
                        {
                            resp.data = {errorInt:1,errorStr:'Do not show this message as session is already expired and we are in login page.'};
                        }
                    }
                }

                return $q.reject(resp);

            }

        };
    }


    ErrCtrl.$inject = ['$scope', '$rootScope', '$location', '$window', 'loadingState', '$state', '$stateParams'];

    /**
     @constructor Error
     @param {object} $scope - Angular.js $scope
     */
    function ErrCtrl($scope, $rootScope, $location, $window, loadingState, $state, $stateParams) {

        $scope.retryPageLoad = function () {
            $state.go($state.current, $stateParams, {reload: true, location: false});

        };

    }

    Date.prototype.getMonthName = function() {
        var monthNames = [ "January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December" ];
        return monthNames[this.getMonth()];
    };
})();

(function() {

    // Expose this accessor function to the window object
    window.getDateObj = function(date){

        var ua = navigator.userAgent.toLowerCase();

        // for Safari & IE
        if ((ua.indexOf('safari') != -1 || ua.indexOf('firefox') != -1) && ua.indexOf('chrome') <= -1 || document.documentMode ) {
            return new Date(date.replace(/-/g, "/"));
        }
        return new Date(date);
    };

    //Fix to hide alert notification on click outside alert window.
    function hideQuickView(e ){
        //e.stopPropagation();
        $("#quickview").removeClass( "open");

    }

    $(document).on("click touchstart", ".page-content-wrapper", hideQuickView); // Page content
    $(document).on("click touchstart", ".toggle-sidebar", hideQuickView); // Navigation sidebar menu icon class

})();
