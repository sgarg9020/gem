/* ============================================================
 * File: config.lazyload.js
 * Configure modules for ocLazyLoader. These are grouped by 
 * vendor libraries. 
 * ============================================================ */

(function(){
	angular.module('app')
	    .config(['$ocLazyLoadProvider', ConfigLazyLoader]);
	    
	function ConfigLazyLoader($ocLazyLoadProvider){

	    $ocLazyLoadProvider.config({
	        debug: true,
	        events: true,
	        modules: [{
	                name: 'isotope',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/imagesloaded/imagesloaded.pkgd.min.js',
	                    LAZY_LOAD_BASE+'app/plugins/jquery-isotope/isotope.pkgd.min.js'
	                ]
	            }, {
	                name: 'codropsDialogFx',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/codrops-dialogFx/dialogFx.js',
	                    LAZY_LOAD_BASE+'app/plugins/codrops-dialogFx/dialog.css',
	                    LAZY_LOAD_BASE+'app/plugins/codrops-dialogFx/dialog-sandra.css'
	                ]
	            }, {
	                name: 'metrojs',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/jquery-metrojs/MetroJs.min.js',
	                    LAZY_LOAD_BASE+'app/plugins/jquery-metrojs/MetroJs.css'
	                ]
	            }, {
	                name: 'owlCarousel',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/owl-carousel/owl.carousel.min.js',
	                    LAZY_LOAD_BASE+'app/plugins/owl-carousel/app/assets/owl.carousel.css'
	                ]
	            }, {
	                name: 'noUiSlider',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/jquery-nouislider/jquery.nouislider.min.js',
	                    LAZY_LOAD_BASE+'app/plugins/jquery-nouislider/jquery.liblink.js',
	                    LAZY_LOAD_BASE+'app/plugins/jquery-nouislider/jquery.nouislider.css'
	                ]
	            },
				{
					name: 'd3map',
					files: [

						LAZY_LOAD_BASE + 'app/plugins/d3/d3.min.js',
						LAZY_LOAD_BASE + 'app/plugins/datamap/topojson.min.js',
						LAZY_LOAD_BASE + 'app/plugins/datamap/datamaps.world.hires.min.js'
					],
					serie: true
				},
				{
	                name: 'nvd3',
	                files: [
	                    //LAZY_LOAD_BASE+'app/plugins/nvd3/lib/d3.v3.js',
	                    //LAZY_LOAD_BASE+'app/plugins/nvd3/nv.d3.min.js',
                        //LAZY_LOAD_BASE+'app/plugins/nvd3/src/utils.js',
                        //LAZY_LOAD_BASE+'app/plugins/nvd3/src/tooltip.js',
                        //LAZY_LOAD_BASE+'app/plugins/nvd3/src/interactiveLayer.js',
                        //LAZY_LOAD_BASE+'app/plugins/nvd3/src/models/axis.js',
                        //LAZY_LOAD_BASE+'app/plugins/nvd3/src/models/line.js',
                        //LAZY_LOAD_BASE+'app/plugins/nvd3/src/models/lineWithFocusChart.js',
                        //LAZY_LOAD_BASE+'app/plugins/angular-nvd3/angular-nvd3.js',
	                    //LAZY_LOAD_BASE+'app/plugins/nvd3/nv.d3.min.css'
                        //
						LAZY_LOAD_BASE+'app/plugins/plugins-merged/css/nvd3.css',
						LAZY_LOAD_BASE+'app/plugins/plugins-merged/js/nvd3.js',
						LAZY_LOAD_BASE+'app/plugins/angular-nvd3/angular-nvd3.js'

	                ],
	                serie: true // load in the exact order
	            }, {
	                name: 'rickshaw',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/nvd3/lib/d3.v3.js',
	                    LAZY_LOAD_BASE+'app/plugins/rickshaw/rickshaw.min.js',
	                    LAZY_LOAD_BASE+'app/plugins/angular-rickshaw/rickshaw.js',
	                    LAZY_LOAD_BASE+'app/plugins/rickshaw/rickshaw.min.css',
	                ],
	                serie: true
	            }, {
	                name: 'sparkline',
	                files: [
	                LAZY_LOAD_BASE+'app/plugins/jquery-sparkline/jquery.sparkline.min.js',
	                LAZY_LOAD_BASE+'app/plugins/angular-sparkline/angular-sparkline.js'
	                ]
	            }, {
	                name: 'mapplic',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/mapplic/js/hammer.js',
	                    LAZY_LOAD_BASE+'app/plugins/mapplic/js/jquery.mousewheel.js',
	                    LAZY_LOAD_BASE+'app/plugins/mapplic/js/mapplic.js',
	                    LAZY_LOAD_BASE+'app/plugins/mapplic/css/mapplic.css'
	                ]
	            }, {
	                name: 'skycons',
	                files: [LAZY_LOAD_BASE+'app/plugins/skycons/skycons.js']
	            }, {
	                name: 'switchery',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/switchery/js/switchery.min.js',
	                    LAZY_LOAD_BASE+'app/plugins/ng-switchery/ng-switchery.js',
	                    LAZY_LOAD_BASE+'app/plugins/switchery/css/switchery.min.css',
	                ]
	            }, {
	                name: 'menuclipper',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/jquery-menuclipper/jquery.menuclipper.css',
	                    LAZY_LOAD_BASE+'app/plugins/jquery-menuclipper/jquery.menuclipper.js'
	                ]
	            }, {
	                name: 'wysihtml5',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css',
	                    LAZY_LOAD_BASE+'app/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js'
	                ]
	            }, {
	                name: 'stepsForm',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/codrops-stepsform/css/component.css',
	                    LAZY_LOAD_BASE+'app/plugins/codrops-stepsform/js/stepsForm.js'
	                ]
	            }, {
	                name: 'jquery-ui',
	                files: [LAZY_LOAD_BASE+'app/plugins/jquery-ui-touch/jquery.ui.touch-punch.min.js']
	            }, {
	                name: 'moment',
	                files: [LAZY_LOAD_BASE+'app/plugins/moment/moment.min.js',
	                    LAZY_LOAD_BASE+'app/plugins/moment/moment-with-locales.min.js'
	                ]
	            }, {
	                name: 'hammer',
	                files: [LAZY_LOAD_BASE+'app/plugins/hammer.min.js']
	            }, {
	                name: 'sieve',
	                files: [LAZY_LOAD_BASE+'app/plugins/jquery.sieve.min.js']
	            }, {
	                name: 'line-icons',
	                files: [LAZY_LOAD_BASE+'app/plugins/simple-line-icons/simple-line-icons.css']
	            }, {
	                name: 'ionRangeSlider',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/ion-slider/css/ion.rangeSlider.css',
	                    LAZY_LOAD_BASE+'app/plugins/ion-slider/css/ion.rangeSlider.skinFlat.css',
	                    LAZY_LOAD_BASE+'app/plugins/ion-slider/js/ion.rangeSlider.min.js'
	                ]
	            }, {
	                name: 'navTree',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/angular-bootstrap-nav-tree/abn_tree_directive.js',
	                    LAZY_LOAD_BASE+'app/plugins/angular-bootstrap-nav-tree/abn_tree.css'
	                ]
	            }, {
	                name: 'nestable',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/jquery-nestable/jquery.nestable.css',
	                    LAZY_LOAD_BASE+'app/plugins/jquery-nestable/jquery.nestable.js',
	                    LAZY_LOAD_BASE+'app/plugins/angular-nestable/angular-nestable.js'
	                ]
	            }, {
	                //https://github.com/angular-ui/ui-select
	                name: 'select',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/bootstrap-select2/select2.css',
	                    LAZY_LOAD_BASE+'app/plugins/angular-ui-select/select.min.css',
	                    LAZY_LOAD_BASE+'app/plugins/angular-ui-select/select.min.js'

	                ]
	            }, {
	                name: 'datepicker',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/bootstrap-datepicker/css/datepicker3.css',
	                    LAZY_LOAD_BASE+'app/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
	                ]
	            }, {
	                name: 'daterangepicker',
	                files: [
						LAZY_LOAD_BASE+'app/plugins/moment/moment-with-locales.min.js',
	                    LAZY_LOAD_BASE+'app/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
	                    LAZY_LOAD_BASE+'app/plugins/bootstrap-daterangepicker/daterangepicker.js',
						LAZY_LOAD_BASE+'app/plugins/angular-daterangepicker/js/angular-daterangepicker.js'
	                ],
				serie: true // load in the exact order
	            }, {
	                name: 'timepicker',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css',
	                    LAZY_LOAD_BASE+'app/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js'
	                ]
	            }, {
	                name: 'inputMask',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/jquery-inputmask/jquery.inputmask.min.js'
	                ]
	            }, {
	                name: 'autonumeric',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/jquery-autonumeric/autoNumeric.js'
	                ]
	            }, {
	                name: 'summernote',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/summernote/css/summernote.css',
	                    LAZY_LOAD_BASE+'app/plugins/summernote/js/summernote.min.js',
	                    LAZY_LOAD_BASE+'app/plugins/angular-summernote/angular-summernote.min.js'
	                ],
	                serie: true // load in the exact order
	            }, {
	                name: 'tagsInput',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/bootstrap-tag/bootstrap-tagsinput.css',
	                    LAZY_LOAD_BASE+'app/plugins/bootstrap-tag/bootstrap-tagsinput.min.js'
	                ]
	            }, {
	                name: 'dropzone',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/dropzone/css/dropzone.css',
	                    LAZY_LOAD_BASE+'app/plugins/dropzone/dropzone.min.js',
	                    LAZY_LOAD_BASE+'app/plugins/angular-dropzone/angular-dropzone.js'
	                ],
				  serie: true // load in the exact order
	            }, {
	                name: 'wizard',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/lodash/lodash.min.js',
	                    LAZY_LOAD_BASE+'app/plugins/angular-wizard/angular-wizard.min.css',
	                    LAZY_LOAD_BASE+'app/plugins/angular-wizard/angular-wizard.min.js'
	                ]
	            }, {
	                name: 'dataTables',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/jquery-datatable/media/css/jquery.dataTables.css',
	                    LAZY_LOAD_BASE+'app/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css',
	                    LAZY_LOAD_BASE+'app/plugins/datatables-responsive/css/datatables.responsive.css',
	                    LAZY_LOAD_BASE+'app/plugins/jquery-datatable/media/js/jquery.dataTables.min.js',
	                    LAZY_LOAD_BASE+'app/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js',
	                    LAZY_LOAD_BASE+'app/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js',
	                    LAZY_LOAD_BASE+'app/plugins/datatables-responsive/js/datatables.responsive.js',
	                    LAZY_LOAD_BASE+'app/plugins/datatables-responsive/js/lodash.min.js',
						LAZY_LOAD_BASE+"app/assets/common/directives/datatable-controls.directive.js",
						LAZY_LOAD_BASE+"app/assets/common/factories/datatable-config.factory.js",
						LAZY_LOAD_BASE+"app/assets/common/directives/data-table.directive.js"

	                ],
	                serie: true // load in the exact order
	            }, {
	                name: 'google-map',
	                files: [
	                    LAZY_LOAD_BASE+'app/plugins/angular-google-map-loader/google-map-loader.js',
	                    LAZY_LOAD_BASE+'app/plugins/angular-google-map-loader/google-maps.js'
	                ]
	            }, {
					name: 'qrcode',
					files: [

						LAZY_LOAD_BASE + 'app/plugins/angular-qrcode/qrcode.js',
						LAZY_LOAD_BASE + 'app/plugins/angular-qrcode/qrcode_UTF8.js',
						LAZY_LOAD_BASE + 'app/plugins/angular-qrcode/angular-qrcode.js'
					],
					serie: true
				}
	        ]
	    });
	}
})();
