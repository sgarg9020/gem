
var MOCK_CONFIG = [
    {
        'src':'commonUtilityService',
        'target':'commonUtilityServiceMock',
        url:LAZY_LOAD_BASE + "test/unit/assets/scripts/factory/mock-services/common-utility.factory.mock.js"
    },
        {
        'src': 'AccountService',
        'target':'AccountServiceMock',
        url: LAZY_LOAD_BASE + "test/unit/assets/scripts/factory/mock-services/account.factory.mock.js"
    }
        ,{
        'src': 'SIMService',
        'target':'SIMServiceMock',
         url: LAZY_LOAD_BASE + "test/unit/assets/scripts/factory/mock-services/sims.factory.mock.js"
        },
        {
        'src': 'roamingProfileService',
        'target':'roamingProfileServiceMock',
        url: LAZY_LOAD_BASE + "test/unit/assets/scripts/factory/mock-services/roaming-profile.factory.mock.js"
    }
];

(function() {

    if (!ENABLE_MOCK_API) {
        MOCK_CONFIG = [];
        bootStrapApp();
        return;
    }

    function bootStrapApp(){
        angular.element(document).ready(function () {
            angular.bootstrap(document, ['app']);
        });
    }
    var scripts =  document.getElementsByTagName('script');
    var m =scripts[scripts.length-1];
    var loadedMockFileCount = 0;
    for (var i = 0; i < MOCK_CONFIG.length; i++) {
        var a = document.createElement('script');
        a.src = MOCK_CONFIG[i].url;
        m.parentNode.insertBefore(a, m);
        a.onload = function(){
            loadedMockFileCount++;
            if(loadedMockFileCount == MOCK_CONFIG.length)
            {
                bootStrapApp();
            }
        }
    }

    var factoryMethod = angular.module('app').factory;
    var argMap = {};
    angular.module('app').factory = handleFactoryMethod;
    function handleFactoryMethod(serviceName, arguments) {
        var mockConfig = MOCK_CONFIG;
        for (var i = 0; i < mockConfig.length; i++) {
            var serviceCfg = mockConfig[i];
            if (serviceCfg.src == serviceName) {
                argMap[serviceCfg.src] = arguments;
                return;
            }
        }
        factoryMethod(serviceName, arguments);
    }
    angular.module('app').config(['$provide', '$injector', function ($provide, $injector) {
    var mockConfig = MOCK_CONFIG;
    for (var i = 0; i < mockConfig.length; i++) {
        var serviceCfg = mockConfig[i];
        $provide.factory(serviceCfg.src,['$injector',(function(serviceCfg){
            return function($injector) {
                var $delegate = $injector.invoke(argMap[serviceCfg.src]);
                var targetService = $injector.get(serviceCfg.target);
                if (serviceCfg.api && serviceCfg.api.length) {
                    for (var k = 0; k < serviceCfg.api.length; k++) {
                       $delegate[serviceCfg.api[k]] = targetService[serviceCfg.api[k]];
                    }
                    return $delegate;
                }
                return targetService;
            }
        })(serviceCfg)]);
    }
    }]);


})();