/* ============================================================
 * File: config.js
 * Configure routing
 * ============================================================ */


(function(){
    angular.module('app')
        .config(['$breadcrumbProvider',function($breadcrumbProvider) {
            $breadcrumbProvider.setOptions({
                prefixStateName: 'app',
                templateUrl: LAZY_LOAD_BASE+'app/assets/config/breadcrumbs.html',
                includeAbstract: true
            });
        }])
        .config(['$stateProvider', '$urlRouterProvider','$httpProvider', StateMachine]);

    function StateMachine($stateProvider, $urlRouterProvider,$httpProvider) {

        $httpProvider.interceptors.push('EPHttpInterceptors');

        $urlRouterProvider
            .otherwise('/auth/login');

        /* Main Application States */
        $stateProvider
            .state('app', {
                abstract: true,
                url: "/app",
                templateUrl: LAZY_LOAD_BASE+"app/assets/config/app.html",
                controller:function(){},
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            'dataTables',
                            LAZY_LOAD_BASE+'app/plugins/currencyutils/currencies.min.js',
                            LAZY_LOAD_BASE+"app/assets/alerts/alerts.factory.js",
                            LAZY_LOAD_BASE+"app/assets/alerts/alerts-sidebar.controller.js",
                            LAZY_LOAD_BASE+'app/assets/common/controllers/verification.controller.js',
                            LAZY_LOAD_BASE+'app/assets/common/directives/verification-modal.directive.js',
                            LAZY_LOAD_BASE + 'app/assets/common/controllers/warning.controller.js',
                            LAZY_LOAD_BASE+'app/assets/common/filters/filters.js',
                            LAZY_LOAD_BASE+'app/assets/common/controllers/sub-accounts.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            removeHover.comb();
                        });
                    }]
                },
                ncyBreadcrumb:{
                    skip: true
                }
            })
            .state('app.home', {
                url: "/home",
                templateUrl: LAZY_LOAD_BASE+"app/assets/dashboard/home.html",
                controller: "HomeCtrl",
                controllerAs: "dashboard",
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                                'dataTables',
                                'nvd3',
                                'moment',
                                'datepicker',
                                'select',
                                'switchery'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/common/factories/common-utility.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/sims/sims.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/common/factories/color-shader.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/users/users.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/dashboard/home.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/data-usage/zones-donut.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/data-usage/sub-accounts-donut.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/dashboard/dashboard-graph.directive.js'
                                ]);
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Dashboard'
                }
            })
            .state('app.users', {
                url: '/users',
                templateUrl: LAZY_LOAD_BASE+'app/assets/users/users.html',
                controller: 'UsersCtrl',
                controllerAs: 'users',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                                'dataTables',
                                'select',
                                'dropzone',
                                'nvd3'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/users/users.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/auth/authentication.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/sims/sims.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/sims/sims.constants.js',
                                    LAZY_LOAD_BASE+'app/assets/users/sim-list-modal.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/users/users.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/users/add-user.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/users/bulk-upload-user.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/controllers/update-group.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/data-usage/modal-bar-chart.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/directives/bulk-upload.directive.js',
                                    LAZY_LOAD_BASE+'app/assets/common/directives/update-group.directive.js',
                                    LAZY_LOAD_BASE+'app/assets/data-usage/modal-bar-chart.directive.js',
                                    LAZY_LOAD_BASE+'app/assets/common/controllers/export.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/directives/export-modal.directive.js'
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'User Management'
                }
            })
            .state('app.profile', {
                abstract: true,
                url: '/profile',
                templateUrl: LAZY_LOAD_BASE+'app/assets/account-settings/profile.html',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                                'select'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/sims/sims.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/users/users.factory.js'
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Profile Settings',
                    linkAppend: '/account-information'
                }
            })
            .state('app.profile.accountInformation', {
                url: '/account-information',
                templateUrl: LAZY_LOAD_BASE+'app/assets/account-settings/account-information.html',
                controller: 'AccountInfoCtrl',
                controllerAs: 'accountInfo',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/account-settings/account-information.controller.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }
                        );
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Edit Account'
                }
            })
            .state('app.profile.changePassword', {
                url: '/change-password',
                templateUrl: LAZY_LOAD_BASE+'app/assets/account-settings/change-password.html',
                controller: 'ChangePasswordCtrl',
                controllerAs: 'changePassword',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/account-settings/change-password.controller.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }
                        );
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Change Password'
                }
            })
            .state('app.profile.generalInformation', {
                url: '/general-information',
                templateUrl: LAZY_LOAD_BASE+'app/assets/account-settings/general-information.html',
                controller: 'GeneralInfoCtrl',
                controllerAs: 'generalInfo',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/auth/authentication.factory.js',
                                LAZY_LOAD_BASE+'app/assets/sims/sims.constants.js',
                                LAZY_LOAD_BASE+'app/assets/users/sim-list-modal.controller.js',
                                LAZY_LOAD_BASE+'app/assets/account-settings/general-information.controller.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }
                        );
                    }]
                },
                ncyBreadcrumb:{
                    label: 'General information'
                }

            })
            .state('app.reports', {
                "abstract": true,
                url: '/reports',
                templateUrl: LAZY_LOAD_BASE+'app/assets/invoices/reports.html',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                                'dataTables'
                        ], {
                                insertBefore: '#lazyload_placeholder'
                        })
                        .then(function() {
                            return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/invoices/invoices.factory.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            });
                        }).then(function(){
                                removeHover.comb();
                        });

                    }]
                },
                ncyBreadcrumb:{
                    label: 'Reports',
                    linkAppend: '/invoices'
                }
            })
            .state('app.reports.invoices', {
                url: '/invoices',
                templateUrl: LAZY_LOAD_BASE+'app/assets/invoices/invoices.html',
                controller: 'InvoicesCtrl',
                controllerAs: 'invoices',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                                'dataTables',
                                'dropzone',
                                'moment',
                                'datepicker',
                                'select'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/invoices/invoices.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/invoices/invoice-upload.controller.js'
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Invoices'
                }
            })
            .state('app.reports.monthlyReports', {
                url: '/monthlyReports',
                templateUrl: LAZY_LOAD_BASE+'app/assets/invoices/monthlyReports.html',
                controller: 'MonthlyReportsCtrl',
                controllerAs: 'monthlyReports',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                                'dataTables',
                                'dropzone',
                                'moment',
                                'datepicker',
                                'select'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/invoices/monthlyReports.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/invoices/invoice-upload.controller.js'
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Monthly Reports'
                }
            })
            .state('app.reports.auditLog', {
                url: '/auditLog',
                templateUrl: LAZY_LOAD_BASE+'app/assets/invoices/auditLog.html',
                controller: 'AuditLogCtrl',
                controllerAs: 'auditLog',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            'dataTables',
                            'dropzone',
                            'moment',
                            'datepicker',
                            'select',
                            'daterangepicker'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/invoices/auditLog.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/invoices/audit-details.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/controllers/export.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/directives/export-modal.directive.js',
                                    LAZY_LOAD_BASE+'app/assets/invoices/invoice-upload.controller.js'
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Audit Log'
                }
            })
            .state('app.settings', {
                abstract: true,
                url: '/settings',
                templateUrl: LAZY_LOAD_BASE+'app/assets/enterprise-settings/settings.html',
                controller: 'SettingsCtrl',
                controllerAs: 'enterpriseSettings',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                                'select',
                                'switchery'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/common/directives/edit-tool.directive.js',
                                    LAZY_LOAD_BASE+'app/assets/enterprise-settings/settings.controller.js'
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Enterprise Settings',
                    linkAppend: '/enterprise-details'
                }
            })
            .state('app.settings.enterpriseDetails', {
                url: '/enterprise-details',
                templateUrl: LAZY_LOAD_BASE+'app/assets/enterprise-settings/enterprise-details.html',
                controller: 'EnterpriseDetailsCtrl',
                controllerAs: 'enterpriseDetails',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/enterprise-settings/enterprise-details.controller.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }
                        )
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Enterprise Details'
                }
            })
            .state('app.settings.pricingDetails', {
                url: '/pricing-details',
                templateUrl: LAZY_LOAD_BASE+'app/assets/enterprise-settings/pricing-details.html',
                controller: 'PricingDetailsCtrl',
                controllerAs: 'pricingDetails',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/enterprise-settings/pricing-details.controller.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }
                        )
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Pricing Details'
                }
            })
            .state('app.settings.alertDetails', {
                url: '/alert-details',
                templateUrl: LAZY_LOAD_BASE+'app/assets/enterprise-settings/alert-details.html',
                controller: 'AlertDetailsCtrl',
                controllerAs: 'alertDetails',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/enterprise-settings/alert-details.controller.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }
                        )
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Action Settings'
                }
            })
            .state('app.profiles', {
                url: '/profiles',
                templateUrl: LAZY_LOAD_BASE+'app/assets/profiles/profiles.html',
                controller: 'ProfilesCtrl',
                controllerAs: 'profiles',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            'dataTables',
                            'select'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/profiles/profiles.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/profiles/profiles.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/controllers/export.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/directives/export-modal.directive.js',
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Profile Management'
                }
            })
            .state('app.sims', {
                abstract: true,
                url: '/sims',
                templateUrl: LAZY_LOAD_BASE+'app/assets/sims/sims-tab.html',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            'dataTables',
                            'select',
                            'nvd3',
                            'datepicker'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/sims/sims.constants.js',
                                    LAZY_LOAD_BASE+'app/assets/sims/sims.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/sims/sims-common.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/users/users.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/sims/sims.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/controllers/update-group.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/sims/update-assignee.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/data-usage/modal-bar-chart.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/directives/edit-tool.directive.js',
                                    LAZY_LOAD_BASE+'app/assets/common/directives/update-group.directive.js',
                                    LAZY_LOAD_BASE+'app/assets/sims/update-assignee.directive.js',
                                    LAZY_LOAD_BASE+'app/assets/data-usage/modal-bar-chart.directive.js',
                                    LAZY_LOAD_BASE+'app/assets/common/controllers/export.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/directives/export-modal.directive.js',
                                    LAZY_LOAD_BASE+'app/assets/sims/zone-status.controller.js'
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'SIM Management',
                    linkAppend: '/physical-sims'
                }
            })
            .state('app.sims.physicalSims', {
                url: '/physical-sims',
                data: {deviceType: 'pSIM'},
                templateUrl: LAZY_LOAD_BASE+'app/assets/sims/sims.html',
                controller: 'SIMsCtrl',
                controllerAs: 'sims',
                ncyBreadcrumb:{
                    label: 'Physical SIMs'
                }
            })
            .state('app.sims.eSims', {
                url: '/e-sims',
                data: {deviceType: 'ESIM_CONSUMER'},
                templateUrl: LAZY_LOAD_BASE+'app/assets/sims/sims.html',
                controller: 'SIMsCtrl',
                controllerAs: 'sims',
                ncyBreadcrumb:{
                    label: 'eSIMs'
                }
            })
            .state('app.sims.m2mESims', {
                url: '/m2m-esims',
                data: {deviceType: 'IOT_M2M'},
                templateUrl: LAZY_LOAD_BASE+'app/assets/sims/sims.html',
                controller: 'SIMsCtrl',
                controllerAs: 'sims',
                ncyBreadcrumb:{
                    label: 'M2M eSIMs'
                }
            })
            .state('app.simDetail', {
            url: "/sim-detail/:iccId",
            templateUrl: LAZY_LOAD_BASE+"app/assets/sims/sim-details/sim-detail.html",
            controller: "SIMCtrl",
            controllerAs: "SIMVm",
            resolve: {
                deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                    return $ocLazyLoad.load([
                        'dataTables',
                        'select',
                        'qrcode'
                    ], {
                        insertBefore: '#lazyload_placeholder'
                    })
                        .then(function() {
                            return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/common/directives/scroll-link.directive.js',
                                LAZY_LOAD_BASE+'app/assets/sims/sims.factory.js',
                                LAZY_LOAD_BASE+'app/assets/sims/sims.constants.js',
                                LAZY_LOAD_BASE+'app/assets/profiles/profiles.factory.js',
                                LAZY_LOAD_BASE+'app/assets/common/factories/tooltips.factory.js',
                                LAZY_LOAD_BASE+'app/assets/common/directives/collapse.directive.js',
                                LAZY_LOAD_BASE+'app/assets/sims/sim-details/sim-detail.controller.js',
                                LAZY_LOAD_BASE+'app/assets/sims/location-history/location-history.controller.js',
                                LAZY_LOAD_BASE+'app/assets/sims/cdr/sim-cdr.controller.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/profile-list/profile-list.controller.js',
                                LAZY_LOAD_BASE+'app/assets/sims/sim-details/sim.controller.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            });
                        }).then(function(){
                            removeHover.comb();
                        });
                }]
            },
            ncyBreadcrumb:{
                label: ' sim details',
                parent:function($scope){
                    if($scope.$qparam.name && $scope.$qparam.name.indexOf('app.sims') != -1)
                        return $scope.$qparam.name;
                    else
                        return 'app.sims';
                }
            }
        })

            .state('app.groups', {
                url: '/groups',
                templateUrl: LAZY_LOAD_BASE+'app/assets/groups/groups.html',
                controller: 'GroupsCtrl',
                controllerAs: 'groups',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                                'dataTables',
                                'select'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/groups/groups.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/groups/add-group.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/controllers/export.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/directives/export-modal.directive.js'
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Group Management'
                }
            })
            .state('app.sub-accounts', {
                url: "/sub-accounts",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/companies.html",
                controller: "CompaniesCtrl",
                controllerAs: "companies",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            'dataTables',
                            'select'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function() {
                            return $ocLazyLoad.load([
                                LAZY_LOAD_BASE + 'app/assets/account-settings/account.factory.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/companies.controller.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/add-enterprise.controller.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            });
                        })
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Account Management'
                }
            })
            .state('app.account', {
                abstract: true,
                url: "/:accountId",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/enterprise.html",
                controller: "EnterpriseCtrl",
                controllerAs: "enterpriseVm",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/enterprise.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        });
                    }]
                },
                ncyBreadcrumb:{
                    label: '{{currentAccountName}}',
                    linkAppend: '/info'
                }
            })
            .state('app.account.info', {
                url: "/info",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/info/info.html",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/info/info.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Info'
                }
            })
            .state('app.dataUsage', {
                abstract: true,
                url: '/data-usage',
                templateUrl: LAZY_LOAD_BASE+"app/assets/data-usage/data-usage.html",
                controller: 'DataUsageCtrl',
                controllerAs: 'dataUsage',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                                'nvd3',
                                'moment',
                                'datepicker'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/common/factories/color-shader.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/data-usage/data-usage.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/data-usage/tab-graphs.directive.js'
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Data Usage',
                    linkAppend: '/enterprise-day'
                }
            })
            .state('app.dataUsageHistorical', {
            url: '/data-usage-historical',
            templateUrl: LAZY_LOAD_BASE+"app/assets/data-usage/data-usage-historical.html",
            controller: 'DataUsageHistoricalCtrl',
            controllerAs: 'dataUsagehistorical',
            resolve: {
                deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                    return $ocLazyLoad.load([

                            'nvd3',
                            'datepicker',
                            'daterangepicker'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                        .then(function() {
                            return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/data-usage/data-usage-historical.controller.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            });
                        }).then(function(){
                            removeHover.comb();
                        });
                }]
            },
            ncyBreadcrumb:{
                label: 'Data Usage',
                linkAppend: '/enterprise-day'
            }
        }).state('app.dataUsage.enterpriseDay', {
                url: '/enterprise-day',
                templateUrl: LAZY_LOAD_BASE+'app/assets/data-usage/bar-graph-wrapper.html',
                controller: 'DuEntAccCtrl',
                controllerAs: 'enterpriseAccount',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/data-usage/enterprise-account-bar.controller.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }
                        );
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Enterprise Usage By Day'
                }
            })
            .state('app.dataUsage.zonesTotal', {
                url: '/zones-total',
                templateUrl: LAZY_LOAD_BASE+'app/assets/data-usage/pie-graph-wrapper.html',
                controller: 'DuZonesDonutCtrl',
                controllerAs: 'zonesDonut',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/data-usage/zones-donut.controller.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }
                        );
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Zone Data Usage'
                }
            })
            .state('app.dataUsage.groupsTotal', {
                url: '/groups-total',
                templateUrl: LAZY_LOAD_BASE+'app/assets/data-usage/pie-graph-wrapper.html',
                controller: 'DuGroupsPieCtrl',
                controllerAs: 'groupsPie',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/data-usage/groups-pie.controller.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }
                        );
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Group Data Usage'
                }
            })
            .state('app.dataUsage.groupsWeek', {
                url: '/groups-week',
                templateUrl: LAZY_LOAD_BASE+'app/assets/data-usage/bar-graph-wrapper.html',
                controller: 'DuGroupsMultiBarCtrl',
                controllerAs: 'groupMultiBar',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/data-usage/groups-multi-bar.controller.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }
                        );
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Group Usage By Week'
                }
            })
            .state('app.alerts', {
                "abstract": true,
                url: "/alerts",
                template: "<ui-view/>",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                'dataTables',
                                'select',
                                'autonumeric'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+"app/assets/alerts/alerts.factory.js",
                                    LAZY_LOAD_BASE+"app/assets/alerts/alerts-common.factory.js",
                                    LAZY_LOAD_BASE+'app/assets/alerts/alert-modal.controller.js',
                                    LAZY_LOAD_BASE+'app/plugins/currencyutils/currencies.min.js'
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                });
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Actions',
                    linkAppend: '/transpired?alertType&alertId'
                }
            })
            .state('app.alerts.transpired', {
                url: '/transpired?alertType&alertId&alertTypeIndex',
                templateUrl: LAZY_LOAD_BASE+'app/assets/alerts/transpired/transpired-alerts.html',
                controller: 'TranspiredAlertsCtrl',
                controllerAs: 'transpiredAlerts',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+"app/assets/alerts/transpired/transpired-alerts.controller.js"
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            removeHover.comb();
                            // TODO - Remove this functionality during integration
                            //setTimeout(function(){Pace.stop();},1000);
                        });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Alerts'
                }
            })
            .state('app.alerts.account', {
                "abstract": true,
                url: '/account',
                templateUrl: LAZY_LOAD_BASE+'app/assets/alerts/account-alerts.html',
                controller: 'AccountAlertsCtrl',
                controllerAs: 'accountAlerts',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+"app/assets/alerts/account-alerts.controller.js"
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            removeHover.comb();
                            // TODO - Remove this functionality during integration
                            //setTimeout(function(){Pace.stop();},1000);
                        });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Account Actions',
                    linkAppend: '/enterprise-spend'
                }
            })
            .state('app.alerts.account.enterpriseSpend', {
                url: '/enterprise-spend',
                templateUrl: LAZY_LOAD_BASE+'app/assets/alerts/enterprise/enterprise-spend-alerts.html',
                controller: 'EnterpriseSpendAlertsCtrl',
                controllerAs: 'enterpriseSpendAlerts',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+"app/assets/alerts/enterprise/enterprise-alerts.controller.js"
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            // TODO - Remove this functionality during integration
                            //setTimeout(function(){Pace.stop();},1000);
                        });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Enterprise Spend'
                }
            })
            .state('app.alerts.account.zoneSpend', {
                url: '/zone-spend',
                templateUrl: LAZY_LOAD_BASE+'app/assets/alerts/zones/zone-spend-alerts.html',
                controller: 'ZoneSpendAlertsCtrl',
                controllerAs: 'zoneSpendAlerts',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+"app/assets/alerts/zones/zone-spend.controller.js"
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            // TODO - Remove this functionality during integration
                            //setTimeout(function(){Pace.stop();},1000);
                        });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Zone Spend'
                }
            })
            .state('app.alerts.account.perSIMDefaults', {
                url: '/per-sim-defaults',
                templateUrl: LAZY_LOAD_BASE+'app/assets/alerts/defaults/per-sim-default-alerts.html',
                controller: 'DefaultPerSIMAlertsCtrl',
                controllerAs: 'defaultPerSIMAlerts',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+"app/assets/alerts/defaults/per-sim-default.controller.js"
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            // TODO - Remove this functionality during integration
                            //setTimeout(function(){Pace.stop();},1000);
                        });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Per Sim Defaults'
                }
            })
            .state('app.alerts.account.zonePerSIMDefaults', {
                url: '/per-sim-by-zone-defaults',
                templateUrl: LAZY_LOAD_BASE+'app/assets/alerts/zones/zone-per-sim-default-alerts.html',
                controller: 'ZonePerSIMAlertsCtrl',
                controllerAs: 'zonePerSIMAlerts',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+"app/assets/alerts/zones/zone-per-sim.controller.js"
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            // TODO - Remove this functionality during integration
                            //setTimeout(function(){Pace.stop();},1000);
                        });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Per Sim By Zone Defaults'
                }
            })
            .state('app.alerts.group', {
                "abstract": true,
                url: '/group/:groupId',
                templateUrl: LAZY_LOAD_BASE+'app/assets/alerts/group-alerts.html',
                controller: 'GroupAlertsCtrl',
                controllerAs: 'groupAlerts',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {

                        return  $ocLazyLoad.load([
                                LAZY_LOAD_BASE+"app/assets/alerts/group-alerts.controller.js"
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function(){
                                //setTimeout(function(){Pace.stop();},1000);
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Group Actions',
                    linkAppend: '/per-sim-defaults'
                }
            })
            .state('app.alerts.group.perSIMDefaults', {
                url: '/per-sim-defaults',
                templateUrl: LAZY_LOAD_BASE+'app/assets/alerts/defaults/group-per-sim-default-alerts.html',
                controller: 'GroupDefaultPerSIMAlertsCtrl',
                controllerAs: 'groupDefaultPerSIMAlerts',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+"app/assets/alerts/defaults/group-per-sim-default.controller.js"
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            // TODO - Remove this functionality during integration
                            //setTimeout(function(){Pace.stop();},1000);
                        });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Per Sim Defaults',
                    parent: 'app.groups'
                }
            })
            .state('app.alerts.group.zonePerSIMDefaults', {
                url: '/per-sim-by-zone-defaults',
                templateUrl: LAZY_LOAD_BASE+'app/assets/alerts/zones/group-zone-per-sim-default-alerts.html',
                controller: 'GroupZonePerSIMAlertsCtrl',
                controllerAs: 'groupZonePerSIMAlerts',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+"app/assets/alerts/zones/group-zone-per-sim.controller.js"
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            // TODO - Remove this functionality during integration
                            //setTimeout(function(){Pace.stop();},1000);
                        });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Per Sim By Zone Defaults',
                    parent: 'app.groups'
                }
            })
            .state('app.alerts.sim', {
                url: '/sim/:simId',
                templateUrl: LAZY_LOAD_BASE+'app/assets/alerts/defaults/sim-alerts.html',
                controller: 'SIMDefaultAlertsCtrl',
                controllerAs: 'SIMDefaultAlerts',
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/sims/sims.factory.js',
                            LAZY_LOAD_BASE+"app/assets/alerts/defaults/sim-default.controller.js"
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            // TODO - Remove this functionality during integration
                            //setTimeout(function(){Pace.stop();},1000);
                        });
                    }]

                },
                ncyBreadcrumb:{
                    label: 'SIM Actions',
                    parent:function($scope){
                        if($scope.$qparam.name && $scope.$qparam.name.indexOf('app.sims') != -1)
                            return $scope.$qparam.name;
                        else
                            return 'app.sims';
                    }
                }
            });

        /* Application Authentication States */
        $stateProvider.state('auth', {
                abstract: true,
                url: "/auth",
                templateUrl: LAZY_LOAD_BASE+"app/assets/config/auth.html"
            })
            .state('auth.login', {
                url: "/login",
                templateUrl: LAZY_LOAD_BASE+"app/assets/auth/login.html",
                controller: "LoginCtrl",
                controllerAs: "login",
                resolve:{
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/auth/login.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            removeHover.comb();
                        });
                    }]
                }
            })
            .state('auth.loginInternal', {
                url: "/loginInternal",
                templateUrl: LAZY_LOAD_BASE+"app/assets/auth/login.html",
                controller: "LoginCtrl",
                controllerAs: "login",
                resolve:{
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/auth/login.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            removeHover.comb();
                        });
                    }]
                }
            })
            .state('auth.forgotPassword', {
                url: "/forgot-password",
                templateUrl: LAZY_LOAD_BASE+"app/assets/auth/forgot-password.html",
                controller: "ForgotPasswordCtrl",
                controllerAs: "forgotPassword",
                resolve:{
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/auth/forgot-password.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            removeHover.comb();
                        });
                    }]
                }
            })
            .state('auth.forceChangePassword', {
                url: "/force-change-password",
                templateUrl: LAZY_LOAD_BASE+"app/assets/auth/change-password.html",
                controller: "ChangePasswordCtrl",
                controllerAs: "changePassword",
                resolve:{
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/account-settings/change-password.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            removeHover.comb();
                        });
                    }]
                }
            })
            .state('auth.resetPassword', {
                url: "/reset-password?data",
                templateUrl: LAZY_LOAD_BASE+"app/assets/auth/reset-password.html",
                controller: "resetPasswordCtrl",
                controllerAs: "resetPwd",
                resolve:{
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/auth/reset-password.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            removeHover.comb();
                        });
                    }]
                }
            })
            .state('auth.unblockAccount', {
                url: "/unblock?data",
                templateUrl: LAZY_LOAD_BASE+"app/assets/auth/reset-password.html",
                controller: "unblockAccountCtrl",
                controllerAs: "unblockAccountObj",
                resolve:{
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/auth/unblock-account.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            removeHover.comb();
                        });
                    }]
                }
            })
            .state('auth.select-account', {
                url: "/select-account",
                templateUrl: LAZY_LOAD_BASE+"app/assets/auth/select-account.html",
                controller: "SelectAccountCtrl",
                controllerAs: "selectAccountObj",
                resolve:{
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/auth/select-account.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function(){
                            removeHover.comb();
                        });
                    }]
                }
            });

        /* Application Support States */
        $stateProvider.state('support', {
                abstract: true,
                url: "/support",
                templateUrl: LAZY_LOAD_BASE+"app/assets/config/support.html",
                controller:function(){},
                resolve: {
                    deps: ['$ocLazyLoad','removeHover', function($ocLazyLoad,removeHover) {
                        return $ocLazyLoad.load([
                            'dataTables',
                            'select'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function() {
                            return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/common/controllers/verification.controller.js',
                                LAZY_LOAD_BASE+'app/assets/common/directives/verification-modal.directive.js',
                                LAZY_LOAD_BASE + 'app/assets/common/controllers/warning.controller.js',
                                LAZY_LOAD_BASE + 'app/assets/common/controllers/country-list-modal.controller.js',
                                LAZY_LOAD_BASE+'app/assets/common/filters/filters.js'
                            ]);
                        }).then(function(){
                            removeHover.comb();
                        });
                    }]
                },
                ncyBreadcrumb:{
                    skip: true
                }
            })
            .state('support.search', {
                url: "/search",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/search/search.html",
                controller: "SearchCtrl",
                controllerAs: "searchVm",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE + 'app/assets/support/search/search.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Search'
                }
            })
            .state('support.enterprise', {
                abstract: true,
                url: "/:accountId",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/enterprise.html",
                controller: "EnterpriseCtrl",
                controllerAs: "enterpriseVm",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE + 'app/assets/common/factories/common-utility.factory.js',
                            LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/enterprise.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        });
                    }]
                },
                ncyBreadcrumb:{
                    label: '{{currentAccountName}}',
                    linkAppend: '/info'
                }
            })
            .state('support.enterprise.info', {
                url: "/info",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/info/info.html",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/info/info.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Info'
                }
            })
            .state('support.enterprise.roamingProfiles', {
                url: '/roaming-profiles',
                templateUrl: LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/roaming-profiles/enterprise.roaming-profiles.html',
                controller:function(){
                    clearDataTableStatus(['DataTables_entRPAssocDataTable_','DataTables_entRPSimMapDataTable_','DataTables_entRPAssocHistDataTable_']);
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                                'dataTables',
                                'select'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE + 'app/assets/common/directives/scroll-link.directive.js',
                                    LAZY_LOAD_BASE+'app/assets/support/roaming-profile/roaming-profile.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/common/factories/datatable-config.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/common/directives/data-table.directive.js',
                                    LAZY_LOAD_BASE+'app/assets/common/directives/datatable-controls.directive.js',
                                    LAZY_LOAD_BASE+'app/assets/sims/sims.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/sims/sims.constants.js',
                                    LAZY_LOAD_BASE+'app/assets/sims/sims-common.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/roaming-profiles/profiles/enterprise.roaming-profile.association.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/roaming-profiles/profiles/associate-roaming-profile.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/roaming-profiles/sims/roaming-profile.sim-mapping.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/roaming-profiles/sims/assign-roaming-profile/assign-roaming-profile.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/roaming-profiles/association-history/enterprise.roaming-profile.association-history.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/sims/zone-status.controller.js'
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Roaming Profiles'
                }
            })
            .state('support.enterprise.association', {
                abstract: true,
                url: "/roaming-profiles/association",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/roaming-profiles/association-history/enterprise.roaming-profile.associationStatus.base.html",
                ncyBreadcrumb:{
                    skip: true,
                    parent: 'support.enterprise.roamingProfiles'
                }
            })
            .state('support.enterprise.association.status', {
                url: '/:statusId',
                templateUrl: LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/roaming-profiles/association-history/enterprise.roaming-profile.association.status.html',
                controller: 'EntRPAssociationStatusCtrl',
                controllerAs: 'entRPAssociationStatus',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function ($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                                'dataTables',
                                'select'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function () {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/common/factories/tooltips.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/support/roaming-profile/roaming-profile.factory.js',
                                    LAZY_LOAD_BASE+"app/assets/common/directives/data-table.directive.js",
                                    LAZY_LOAD_BASE+"app/assets/common/directives/datatable-controls.directive.js",
                                    LAZY_LOAD_BASE+"app/assets/common/factories/datatable-config.factory.js",
                                    LAZY_LOAD_BASE + 'app/assets/support/enterprises/enterprise/roaming-profiles/association-history/enterprise.roaming-profile.association.status.controller.js'
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function () {
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb: {
                    label: 'Association Status'
                }
            })
            .state('support.enterprise.profiles', {
                url: '/profiles',
                templateUrl: LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/profiles/profiles.html',
                controller: 'GAPProfilesCtrl',
                controllerAs: 'profiles',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            'dataTables',
                            'select'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/profiles/profiles.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/profiles/profiles.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/controllers/export.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/directives/export-modal.directive.js',
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Profiles'
                }
            })
            .state('support.enterprise.simCards', {
                abstract: true,
                url: "/sim-cards",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/sims/sim-cards-tab.html",
                resolve: {
                    deps: ['$ocLazyLoad','removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            'dataTables',
                            'select',
                            'dropzone',
                            'nvd3'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        }).then(function() {

                            return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/sims/sims.constants.js',
                                LAZY_LOAD_BASE+'app/assets/sims/sims.factory.js',
                                LAZY_LOAD_BASE+'app/assets/common/factories/datatable-config.factory.js',
                                LAZY_LOAD_BASE+'app/assets/common/directives/data-table.directive.js',
                                LAZY_LOAD_BASE+'app/assets/common/directives/datatable-controls.directive.js',
                                LAZY_LOAD_BASE+'app/assets/sims/sims-common.factory.js',
                                LAZY_LOAD_BASE+'app/assets/common/controllers/export.controller.js',
                                LAZY_LOAD_BASE+'app/assets/common/directives/export-modal.directive.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/sim-cards.controller.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/association-history/enterprise-sim.associatiion-history.controller.js',
                                LAZY_LOAD_BASE+'app/assets/sims/zone-status.controller.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/bulk-upload-sim.controller.js',
                                LAZY_LOAD_BASE+'app/assets/common/directives/bulk-upload.directive.js',
                                LAZY_LOAD_BASE+'app/assets/common/factories/tooltips.factory.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/association-history/enterprise-sim.association.factory.js',
                                LAZY_LOAD_BASE+'app/assets/support/roaming-profile/roaming-profile.factory.js',
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                        }).then(function(){
                            removeHover.comb();
                        });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Sim Cards',
                    linkAppend: '/physical-sims'
                }
            })
            .state('support.enterprise.simCards.physicalSims', {
                url: '/physical-sims',
                data: {deviceType: 'pSIM'},
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/sims/sim-cards.html",
                controller: "SIMCardsCtrl",
                controllerAs: "SIMCardsVm",
                ncyBreadcrumb:{
                    label: 'Physical SIMs'
                }
            })
            .state('support.enterprise.simCards.eSims', {
                url: '/e-sims',
                data: {deviceType: 'ESIM_CONSUMER'},
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/sims/sim-cards.html",
                controller: "SIMCardsCtrl",
                controllerAs: "SIMCardsVm",
                ncyBreadcrumb:{
                    label: 'eSIMs'
                }
            })
            .state('support.enterprise.simCards.m2mESims', {
                url: '/m2m-esims',
                data: {deviceType: 'IOT_M2M'},
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/sims/sim-cards.html",
                controller: "SIMCardsCtrl",
                controllerAs: "SIMCardsVm",
                ncyBreadcrumb:{
                    label: 'M2M eSIMs'
                }
            })
            .state('support.enterprise.sim', {
                abstract: true,
                url: "/sim-cards/status",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/sims/association-history/enterprise-simSessionStatus.base.html",
                ncyBreadcrumb:{
                    skip: true,
                    parent:function($scope){
                        if($scope.$qparam.name && $scope.$qparam.name.indexOf('support.enterprise.simCards') != -1)
                            return $scope.$qparam.name;
                        else
                            return 'support.enterprise.simCards';
                    }
                }
            })
            .state('support.enterprise.sim.status', {
                url: '/:statusId',
                templateUrl: LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/association-history/enterprise.sim.status.html',
                controller: 'EntSIMAssociationStatusCtrl',
                controllerAs: 'entSIMssociationStatus',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function ($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            'dataTables',
                            'select'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                            .then(function () {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/common/factories/tooltips.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/support/roaming-profile/roaming-profile.factory.js',
                                    LAZY_LOAD_BASE+"app/assets/common/directives/data-table.directive.js",
                                    LAZY_LOAD_BASE+"app/assets/common/directives/datatable-controls.directive.js",
                                    LAZY_LOAD_BASE+"app/assets/common/factories/datatable-config.factory.js",
                                    LAZY_LOAD_BASE +'app/assets/support/enterprises/enterprise/sims/association-history/enterprise.sim.status.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/association-history/enterprise-sim.association.factory.js'

                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function () {
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb: {
                    label: 'Association Status'
                }
            })
            .state('support.enterprise.pricing', {
                url: "/pricing",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/pricing/pricing.html",
                controller:function(){
                    clearDataTableStatus(['DataTables_entZoneProvHistDataTable_']);
                    clearDataTableStatus(['DataTables_entZonesDataTable_'], true);
                },
                controllerAs: "pricingDetails",
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            'dropzone'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/common/directives/edit-tool.directive.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/pricing/zone-provisioning-history/zone-provisioning-history.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/pricing/zone-pricing.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/pricing/zones/zones.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/support/roaming-profile/roaming-profile.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/pricing/zones/import-zones/import-zones.js',
                                    LAZY_LOAD_BASE+'app/assets/common/directives/bulk-upload.directive.js',
                                    LAZY_LOAD_BASE + 'app/assets/common/directives/scroll-link.directive.js'
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Pricing'
                }
            })
            .state('support.enterprise.zone', {
                abstract: true,
                url: "/pricing/zones",
                controller:function(){
                    clearDataTableStatus(['DataTables_entZonePreviewDataTable_']);
                },
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/pricing/zones/import-zones/zones-preview.base.html",
                ncyBreadcrumb:{
                    skip: true,
                    parent: 'support.enterprise.pricing'
                }
            })
            .state('support.enterprise.zone.preview', {
                url: '/preview',
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/pricing/zones/import-zones/zones-preview.html",
                controller: "ZonesPreviewCtrl",
                controllerAs: "zonesPreview",
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            'dataTables',
                            'dropzone'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/pricing/zones/import-zones/zones-preview.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/support/roaming-profile/roaming-profile.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/pricing/zone-pricing.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/common/factories/modal-details.factory.js'
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb: {
                    label: 'Zone Provisioning Preview'
                }
            })
            .state('support.enterprise.zone.detail', {
                url: '/:zoneId',
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/pricing/zones/zone/zone-details.html",
                controller: "ZoneDetailsCtrl",
                controllerAs: "zoneDetails",
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            'select'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/pricing/zones/zone/zone-details.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/pricing/zones/zone/edit-pricing.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/support/roaming-profile/roaming-profile.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/pricing/zone-pricing.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/common/factories/modal-details.factory.js'
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb: {
                    label: 'Zone Details'
                }
            })
            .state('support.enterprise.zoneProvisioning', {
                abstract: true,
                controller:function(){
                    clearDataTableStatus(['DataTables_entZoneProvStatusDataTable_']);
                },
                url: "/pricing/zone-provisioning",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/pricing/zone-provisioning-history/zone-provisioning.status.base.html",
                ncyBreadcrumb:{
                    skip: true,
                    parent: 'support.enterprise.pricing'
                }
            })
            .state('support.enterprise.zoneProvisioning.status', {
                url: '/:statusId',
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/pricing/zone-provisioning-history/zone-provisioning.status.html",
                controller: "ZoneProvisioningStatusCtrl",
                controllerAs: "zoneProvisioningStatus",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/pricing/zone-provisioning-history/zone-provisioning.status.controller.js',
                            LAZY_LOAD_BASE+'app/assets/support/roaming-profile/roaming-profile.factory.js',
                            LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/pricing/zone-pricing.factory.js',
                            LAZY_LOAD_BASE+'app/assets/common/factories/modal-details.factory.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                    }]
                },
                ncyBreadcrumb: {
                    label: 'Zone Provisioning Status'
                }
            })
            .state('support.enterprise.users', {
                url: "/users",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/users/users.html",
                controller: "GapUserManagementCtrl",
                controllerAs: "users",
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                                'dataTables',
                                'select',
                                'dropzone',
                                'nvd3'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/users/users.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/auth/authentication.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/sims/sims.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/sims/sims.constants.js',
                                    LAZY_LOAD_BASE+'app/assets/users/sim-list-modal.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/users/users.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/users/add-user.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/controllers/export.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/common/directives/export-modal.directive.js'
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'User Management'
                }
            })
            .state('support.enterprise.reports', {
                abstract: true,
                url: '/reports',
                templateUrl: LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/reports/reports.html',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            'dataTables'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/invoices/invoices.factory.js'
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });

                    }]
                },
                ncyBreadcrumb:{
                    label: 'Reports',
                    linkAppend: '/invoices'
                }
            })
            .state('support.enterprise.reports.invoices', {
                url: '/invoices',
                templateUrl: LAZY_LOAD_BASE+'app/assets/invoices/invoices.html',
                controller: 'InvoicesCtrl',
                controllerAs: 'invoices',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            'dataTables',
                            'dropzone',
                            'moment',
                            'datepicker',
                            'select'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/invoices/invoices.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/invoices/invoice-upload.controller.js'
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Invoices'
                }
            })
            .state('support.enterprise.reports.monthlyReports', {
                url: '/monthlyReports',
                templateUrl: LAZY_LOAD_BASE+'app/assets/invoices/monthlyReports.html',
                controller: 'MonthlyReportsCtrl',
                controllerAs: 'monthlyReports',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            'dataTables',
                            'dropzone',
                            'moment',
                            'datepicker',
                            'select'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/invoices/monthlyReports.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/invoices/invoice-upload.controller.js'
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Monthly Reports'
                }
            })
            .state('support.enterprise.documents', {
                url: "/documents",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/documents/reports.html",
                controller: "ReportsCtrl",
                controllerAs: "reportsVm",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                'dropzone'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                                .then(function() {
                                    return $ocLazyLoad.load([
                                        LAZY_LOAD_BASE+'app/assets/common/directives/bulk-upload.directive.js',
                                        LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/documents/bulk-upload-document.controller.js',
                                        LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/documents/reports.controller.js'
                                    ],{
                                        insertBefore: '#lazyload_placeholder'
                                    });
                                });
                        }]
                    },
                    ncyBreadcrumb:{
                        label: 'Documents'
                    }
                })
            .state('support.enterprise.customerContacts', {
                url: "/customer-contacts",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/contacts/company-approved-contacts.html",
                controller: "ContactsCtrl",
                controllerAs: "contactsVm",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                'dropzone'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/contacts/add-contact.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/contacts/company-approved-contacts.controller.js'
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Customer Contacts'
                }
            })
            .state('support.sim-activity', {
                abstract: true,
                url: "/sim-activity",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/customer-activity/customer-activity.html",
                controller: "CustomerActivityCtrl",
                controllerAs: "filters",
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                            'moment',
                            'datepicker',
                            'daterangepicker',
                            'd3map'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/common/factories/country-code.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/support/roaming-profile/roaming-profile.factory.js',
                                    LAZY_LOAD_BASE + 'app/assets/support/customer-activity/customer-activity.factory.js',
                                    LAZY_LOAD_BASE + 'app/assets/support/customer-activity/customer-activity.controller.js',
                                ],{
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function(){
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    skip: true
                }
            })
            .state('support.sim-activity.cdr', {
                url: "/cdr",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/customer-activity/cdr/call-data-records.html",
                controller: "CdrCtrl",
                controllerAs: "cdrVm",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE + 'app/assets/support/customer-activity/cdr/call-data-records.controller.js',
                            LAZY_LOAD_BASE + 'app/assets/support/customer-activity/cdr/cdr-data/cdr-data-table.controller.js',
                            LAZY_LOAD_BASE + 'app/assets/support/customer-activity/cdr/cdr-events/cdr-events-table.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                    }]
                },
                ncyBreadcrumb:{
                    skip: true
                }
            })
            .state('support.sim-activity.location-updates', {
                url: "/location-updates",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/customer-activity/location/location-updates.html",
                controller: "SimLocationCtrl",
                controllerAs: "locationVm",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE + 'app/assets/support/customer-activity/location/location-updates.controller.js',
                            LAZY_LOAD_BASE + 'app/assets/support/customer-activity/location/location-data/location-data-table.controller.js',
                            LAZY_LOAD_BASE + 'app/assets/support/customer-activity/location/location-events/location-events-table.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                    }]
                },
                ncyBreadcrumb:{
                    skip: true
                }
            })
            .state('support.sim', {
                abstract: true,
                url: "/account/:accountId/sim",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/sims/sim-details/sim.html",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE + 'app/assets/common/directives/scroll-link.directive.js',
                            LAZY_LOAD_BASE+'app/assets/sims/sims.constants.js',
                            LAZY_LOAD_BASE+'app/assets/sims/sims-common.factory.js',
                            LAZY_LOAD_BASE+'app/assets/sims/sims.factory.js',
                            LAZY_LOAD_BASE+'app/assets/users/users.factory.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                    }]
                },
                ncyBreadcrumb:{
                    skip: true,
                    parent:function($scope){
                        if($scope.$qparam.name && $scope.$qparam.name.indexOf('support.enterprise.simCards') != -1)
                            return $scope.$qparam.name;
                        else
                            return 'support.enterprise.simCards';
                    }
                }
            })
            .state('support.sim.simDetail', {
                url: "/:iccId",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/sims/sim-details/sim-detail.html",
                controller: "GapSIMCtrl",
                controllerAs: "SIMVm",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            'qrcode'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                        .then(function() {
                            return $ocLazyLoad.load([
                                LAZY_LOAD_BASE+'app/assets/common/factories/tooltips.factory.js',
                                LAZY_LOAD_BASE+'app/assets/common/directives/collapse.directive.js',
                                LAZY_LOAD_BASE+'app/assets/profiles/profiles.factory.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/profile-list/profile-list.controller.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/imsi-profiles/switch-imsi.controller.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/imsi-profiles/switch-imsi.directive.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/sim-details/sim-detail.controller.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/location-history/location-history.controller.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/sim-operations-history/sim-operations-history.controller.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/ota-history/ota-history.controller.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/cdr/sim-cdr.controller.js',
                                LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/sim-details/sim.controller.js',
                                LAZY_LOAD_BASE+'app/assets/common/controllers/export.controller.js',
                                LAZY_LOAD_BASE+'app/assets/common/directives/export-modal.directive.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            });
                        })
                    }]
                },
                ncyBreadcrumb:{
                    label: ' sim details',
                    parent: 'support.sim'
                }
            })
            .state('support.sim.simOperationsHistory', {
                abstract: true,
                url: "/account/:accountId/sim/:iccId/status",
                template: '<ui-view></ui-view>',
                ncyBreadcrumb:{
                    skip: true,
                    parent: 'support.sim.simDetail'
                }
            })
            .state('support.sim.simOperationsHistory.status', {
                url: "/:operationId",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/sims/sim-operations-history/sim-operations-history-detail.html",
                controller: "SIMOperationsHistoryDetailCtrl",
                controllerAs: "SIMOperationsHistoryDetailVm",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/sims/sim-operations-history/sim-operations-history-detail.controller.js',
                            LAZY_LOAD_BASE+'app/assets/profiles/profiles.factory.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                    }]
                },
                ncyBreadcrumb:{
                    label: '{{operationType}} status details',
                    parent: 'support.sim.simOperationsHistory'
                }
            })
            .state('support.euiccProfile', {
                abstract: true,
                url: "/account/:accountId/profile",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/profiles/profile-details/profile-detail-base.html",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/profiles/profiles.factory.js',
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                    }]
                },
                ncyBreadcrumb:{
                    skip: true,
                    parent: 'support.enterprise.profiles'
                }
            })
            .state('support.euiccProfile.profile-details', {
                url: "/:iccid",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/enterprise/profiles/profile-details/profile-detail.html",
                controller: "ProfileDetailBaseCtrl",
                controllerAs: "ProfileDetailBaseVm",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/profiles/profiles.factory.js',
                            LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/profiles/profile-details/profile-detail-base.controller.js',
                            LAZY_LOAD_BASE+'app/assets/support/enterprises/enterprise/profiles/profile-details/profile-detail.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Profile Details',
                    parent: 'support.euiccProfile'
                }
            })
            .state('support.accounts', {
                abstract: true,
                url: "/accounts",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/accounts.html",
                ncyBreadcrumb:{
                    skip: true
                }
            })
            .state('support.accounts.companies', {
                url: "/companies",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/enterprises/companies.html",
                controller: "CompaniesCtrl",
                controllerAs: "companies",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE + 'app/assets/account-settings/account.factory.js',
                            LAZY_LOAD_BASE+'app/assets/support/enterprises/companies.controller.js',
                            LAZY_LOAD_BASE+'app/assets/support/enterprises/add-enterprise.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Enterprises'
                }
            })
            .state('support.profile', {
                abstract: true,
                url: '/profile',
                templateUrl: LAZY_LOAD_BASE+'app/assets/support/profile-settings/profile.html',
                resolve: {
                    deps: ['$ocLazyLoad', 'removeHover', function ($ocLazyLoad, removeHover) {
                        return $ocLazyLoad.load([
                                'select'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function () {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE + 'app/assets/common/factories/common-utility.factory.js',
                                    LAZY_LOAD_BASE + 'app/assets/account-settings/account.factory.js',
                                    LAZY_LOAD_BASE + 'app/assets/users/users.factory.js'
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function () {
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb: {
                    label: 'Profile Settings',
                    linkAppend: '/account-information'
                }
            })
            .state('support.profile.accountInformation', {
                url: '/account-information',
                templateUrl: LAZY_LOAD_BASE+'app/assets/support/profile-settings/account-information.html',
                controller: 'AccountInfoCtrl',
                controllerAs: 'accountInfo',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                LAZY_LOAD_BASE + 'app/assets/support/profile-settings/account-information.controller.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }
                        );
                    }]
                },
                ncyBreadcrumb: {
                    label: 'Account information'
                }
            })
            .state('support.profile.changePassword', {
                url: '/change-password',
                templateUrl: LAZY_LOAD_BASE+'app/assets/support/profile-settings/change-password.html',
                controller: 'ChangePasswordCtrl',
                controllerAs: 'changePassword',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                LAZY_LOAD_BASE + 'app/assets/support/profile-settings/change-password.controller.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }
                        );
                    }]
                },
                ncyBreadcrumb: {
                    label: 'Change Password'
                }
            })
            .state('support.roamingProfile', {
                abstract: true,
                url: "/roaming-profile",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/roaming-profile/roaming-profile.html",
                controller: "RoamingProfileCtrl",
                controllerAs: "roamingProfileVm",
                resolve: {
                    deps: ['$ocLazyLoad','removeHover', function($ocLazyLoad,removeHover) {
                        return $ocLazyLoad.load([
                            'dataTables'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                            .then(function () {
                                return $ocLazyLoad.load([
                                    LAZY_LOAD_BASE+'app/assets/support/roaming-profile/roaming-profile.controller.js',
                                    LAZY_LOAD_BASE+'app/assets/support/roaming-profile/roaming-profile.factory.js',
                                    LAZY_LOAD_BASE+'app/assets/common/factories/tooltips.factory.js'
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                });
                            }).then(function () {
                                removeHover.comb();
                            });
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Roaming Profiles'
                }
            })
            .state('support.roamingProfile.sets', {
                url: "/sets",
                templateUrl: LAZY_LOAD_BASE+"app/assets/support/roaming-profile/sets/roaming-profile-sets.html",
                controller: "RoamingProfileSetsCtrl",
                controllerAs: "roamingProfileSetsVm",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE+'app/assets/support/roaming-profile/sets/roaming-profile-sets.controller.js',
                            LAZY_LOAD_BASE+'app/assets/support/roaming-profile/sets/create-roaming-profile-set.controller.js',
                            LAZY_LOAD_BASE+'app/assets/support/roaming-profile/enterprises-modal.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                    }]
                },
                ncyBreadcrumb:{
                    label: 'Sets'
                }
            }).state('support.roamingProfile.set', {
            url: "/sets/:RPSetId",
            templateUrl: LAZY_LOAD_BASE+"app/assets/support/roaming-profile/sets/detail/roaming-profile-set-detail.html",
            controller: "RoamingProfileSetCtrl",
            controllerAs: "roamingProfileSetVm",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        LAZY_LOAD_BASE+'app/assets/support/roaming-profile/sets/detail/roaming-profile-set-detail.controller.js',
                        LAZY_LOAD_BASE+'app/assets/support/roaming-profile/enterprises-modal.controller.js',
                        LAZY_LOAD_BASE+'app/assets/support/roaming-profile/version/create-roaming-profile-version.controller.js'
                    ], {
                        insertBefore: '#lazyload_placeholder'
                    })
                }]
            },
            ncyBreadcrumb:{
                label: '{{RPSetName}}',
                parent: 'support.roamingProfile.sets'
            }
        }).state('support.roamingProfile.version', {
            url: "/sets/:RPSetId/version/:versionId",
            templateUrl: LAZY_LOAD_BASE+"app/assets/support/roaming-profile/version/roaming-profile-version.html",
            controller: "RoamingProfileVersionCtrl",
            controllerAs: "roamingProfileVersionVm",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'dropzone'
                    ], {
                        insertBefore: '#lazyload_placeholder'
                    }).then(function () {
                        return $ocLazyLoad.load([
                            LAZY_LOAD_BASE + 'app/assets/common/directives/scroll-link.directive.js',
                            LAZY_LOAD_BASE + 'app/assets/support/roaming-profile/version/roaming-profile-version.controller.js',
                            LAZY_LOAD_BASE + 'app/assets/support/roaming-profile/version/roaming-profile-version-countries.controller.js',
                            LAZY_LOAD_BASE + 'app/assets/support/roaming-profile/version/roaming-profile-version-networks.controller.js',
                            LAZY_LOAD_BASE + 'app/assets/support/roaming-profile/upload-roaming-profile.controller.js',
                            LAZY_LOAD_BASE + 'app/assets/common/controllers/warning.controller.js'
                        ], {
                            insertBefore: '#lazyload_placeholder'
                        })
                    })
                }]
            },
            ncyBreadcrumb:{
                label: '{{RPName}}',
                parent: 'support.roamingProfile.set'
            }
        });


        /* Application Error States */
        $stateProvider.state('error', {
                url: '/error/:url',
                templateUrl: LAZY_LOAD_BASE+'error.html',
                controller: "ErrCtrl"
            })
            .state('app.error', {
                url: '/error/:url',
                templateUrl: LAZY_LOAD_BASE+'error.html',
                controller: "ErrCtrl"
            });

    }

})();
