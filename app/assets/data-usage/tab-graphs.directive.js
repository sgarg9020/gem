'use strict';

/* Directive */
(function(){
	angular.module('app')
		.directive('gsTabGraphs', GsTabGraphs);

	GsTabGraphs.$inject = ['debounce','loadingState'];
	
	/**
		@function gsTabGraphs()
		@param {object} debounce - Factory service
		@param {object} loadingState - Factory service
	*/	
	function GsTabGraphs(debounce, loadingState) {

		return {
			link: link
		};

		/**
		 @function link()
		 @param {object} $scope - directive scope
		 @param {object} element - directive HTML element
		 @param {object} attrs - HTML element attributes
		 */
		function link($scope, element, attrs) {

            // Directive variables
			var resizeDebounce;
			var isCallbackDuringInit = true;
            var winCache = {};

            // Window Cache
            winCache.height = window.innerHeight;
            winCache.width = window.innerWidth;

            // Set graph options
            $scope.$parent.graphObj.updateGraphOptions();


            // Add graph callback
            $scope.$parent.graphObj.options.chart.callback = function(){
                if(isCallbackDuringInit){
                    loadingState.hide();
                    isCallbackDuringInit = false;}
            };

            // Add graph resize
            resizeDebounce = debounce.set(function(){
                if(winCache.height != window.innerHeight &&
                    winCache.width == window.innerWidth){
                    // Viewport changed only in the height dimension.
                    // Mobile browsers adding to viewport size when user scrolls, address bar shrinks
                    var mobileBrowserAddressBar = 80;

                    if(window.innerHeight > winCache.height+mobileBrowserAddressBar){
                        // Update window cache
                        winCache.height = window.innerHeight;
                        winCache.width = window.innerWidth;
                        $scope.$parent.graphObj.updateGraphOptions();}}
                else {
                    if(window.innerHeight - winCache.height!=69 && (window.innerHeight - winCache.height)!=0) //Hack for ios6 fix issue:SB-5447
                        $scope.$parent.graphObj.updateGraphOptions();
                }

            }, 25);

            window.addEventListener('resize', resizeDebounce);

			$scope.showCalendar = function(event){
				$($(event.target.parentNode).siblings('input')[0]).datepicker('show');
			};
			$scope.enableDatePick = true && ENABLE_DATA_USAGE_DATE_SELECTION;
            // Get graph data
            $scope.graphObj.requestData(redrawGraph);
			var timezone  = sessionStorage.getItem("timezone");
			var timezoneArr = timezone.split(':');
			var sign = parseInt(timezoneArr[0])?parseInt(timezoneArr[0])<0?-1:1:0;
			var today = new Date();
			today.setTime(today.getTime()+sign*(Math.abs(parseInt(timezoneArr[0])*60*60*1000)+Math.abs(parseInt(timezoneArr[1])*60*1000)));
			var start = new Date(today.getUTCFullYear(), today.getUTCMonth(), 1);
			var end = new Date(today.getUTCFullYear(), today.getUTCMonth() + 1, 0);
			$scope.start = ("0" + (start.getMonth()+1)).slice(-2)+'/'+("0" + start.getDate()).slice(-2)+'/'+start.getFullYear();
		    $scope.end = ("0" + (end.getMonth()+1)).slice(-2)+'/'+("0" + end.getDate()).slice(-2)+'/'+end.getFullYear();
			$scope.graphObj.start = start;
			$scope.graphObj.end = end;
			//On select of date range
			$scope.dateChange = function () {
				if ($scope.start && $scope.end) {
					var firstDay = new Date($scope.start);
					var lastDay = new Date($scope.end);
					var fromDate = firstDay.getFullYear() + '-' + ("0" + (firstDay.getMonth() + 1)).slice(-2) + '-' + ("0" + firstDay.getDate()).slice(-2) + ' 00:00:00';
					var toDate = lastDay.getFullYear() + '-' + ("0" + (lastDay.getMonth() + 1)).slice(-2) + '-' + ("0" + lastDay.getDate()).slice(-2) + ' 23:59:59';
					$scope.graphObj.start = firstDay;
					$scope.graphObj.end = lastDay;
					$scope.graphObj.getdata(redrawGraph, fromDate, toDate)
				}
			};
			$scope.month = ("0" + (today.getMonth()+1)).slice(-2)+'-'+today.getFullYear();
			$scope.graphObj.month = today;
			$scope.monthChange = function () {
				if ($scope.month) {
					var month = new Date($scope.month.split('-')[1],parseInt($scope.month.split('-')[0])-1,1);
					var firstDay = new Date(month.getFullYear(), month.getMonth(), 1);
					var lastDay = new Date(month.getFullYear(), month.getMonth() + 1, 0);
					var fromDate = firstDay.getFullYear() + '-' + ("0" + (firstDay.getMonth() + 1)).slice(-2) + '-' + ("0" + firstDay.getDate()).slice(-2) + ' 00:00:00';
					var toDate = lastDay.getFullYear() + '-' + ("0" + (lastDay.getMonth() + 1)).slice(-2) + '-' + ("0" + lastDay.getDate()).slice(-2) + ' 23:59:59';
					$scope.graphObj.month = month;
					$scope.graphObj.getdata(redrawGraph, fromDate, toDate)
				}
			};

            // Menu Pin Event
            $('body').on("onMenuPin",function(){
                var path = window.location.href;
                if(path.indexOf('/app/data-usage') != -1 && (path.indexOf('/enterprise-day') != -1 || path.indexOf('/groups-week') != -1)){
                    setTimeout(function(){
                        $scope.$parent.graphObj.api.refresh();
                    }, 200);
                }
            });

            /**
             * @func redrawGraph()
             * @param {object} viewModel - controllerAs object from graph controller
             */
			function redrawGraph(viewModel) {
				if (!viewModel.api.getScope().chart) {
					viewModel.api.updateWithOptions(viewModel.options);
				}
				loadingState.hide();
			}


			$('body').on("onMenuPin",function(){
				var path = window.location.href;
				if(path.indexOf('/app/data-usage') != -1 && (path.indexOf('=tab-ent-day') != -1 || path.indexOf('=tab-group-week') != -1)){
					setTimeout(function(){
						$scope.$parent.graphObj.api.refresh();
					}, 200);
				}

			});


		}
	}
})();