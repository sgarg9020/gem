'use strict';

(function(){
	angular
		.module('app')
		.factory('pieGraphToTable', PieGraphToTable);
		
	PieGraphToTable.$inject = ['commonUtilityService'];
	
	/**
		@function PieGraphToTable()
		 - Factory method for taking graph data and emulating
		   the pie table tooltip data for display in a table
	*/ 	
	function PieGraphToTable(commonUtilityService){
	    var factory = {
	        make: makeCache
	    };
	    
	    return factory;
	   
		/*
			@func makeCache
			@param {object} tableData - the repsonse data from XHR for Pie Chart data
			@desc - makeCache() will return an object for a modal template to reference
			and show the data in table format.	
		*/
		function makeCache(tableData){
			
			var enabledTotal = tableData
					        .filter(function(item){return !item.disabled;})
					        .reduce(function(a, b){return a + b.dataUsage}, 0);
					        
		    var toolTipCache = [];
		        
	        for(var i=0;i<tableData.length;i++){
		        if(toolTipCache.length < tableData.length){ 
			        var obj = {};

					obj.percent = Math.round((tableData[i].dataUsage/enabledTotal) * 100);
					obj.tickUnit = commonUtilityService.getDataUsageUnit(tableData[i].dataUsage);
					obj.tickVal = commonUtilityService.getUsageInUnit(tableData[i].dataUsage,obj.tickUnit);
			        
			        toolTipCache.push(obj);
		        } 
	        }
	        
	        return toolTipCache;
		}
	   		
	}	
})();