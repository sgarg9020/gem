'use strict';

(function(){
	angular
		.module('app')
		.factory('requestGraphData',['commonUtilityService','$q',RequestGraphData]);
	
	/**
		@function requestGraphData()
	*/ 	
	function RequestGraphData(commonUtilityService,$q) {

		var factory = {
			getDataUsage: getDataUsage,
			getMonthlyDataUsageOfZones: getMonthlyDataUsageOfZones,
			getDailyDataUsageOfGroups: getDailyDataUsageOfGroups,
			getWeeklyDataUsageOfGroups: getWeeklyDataUsageOfGroups,
			getDailyDataUsageOfEnterprise: getDailyDataUsageOfEnterprise,
			getDailyDataUsageOfSIMs:getDailyDataUsageOfSIMs,
			getDailyDataUsageOfUsers:getDailyDataUsageOfUsers,
			getEstimatedDataCost:getEstimatedDataCost,
			getUsageData:getUsageData,
            getMonthlyDataUsageOfSubAccounts: getMonthlyDataUsageOfSubAccounts,
            getMonthlyHighestDataUsageBySIM: getMonthlyHighestDataUsageBySIM,
            getMonthlyHighestDataUsageByUser: getMonthlyHighestDataUsageByUser
		};

		return factory;

		function getDataUsage(url) {
			return commonUtilityService.prepareHttpRequest(url,'GET').error(function (response) {
				console.error('factory::requestGraphData - request failed', response);
			});
		}

		function getMonthlyDataUsageOfZones(startDate, endDate,groupBy, consolidated) {
			var headerAdd=['READ_DATAUSAGE'];
			if(!groupBy)
			{
				groupBy = 'MONTH';
			}
            consolidated = !!consolidated;

			return commonUtilityService.prepareHttpRequest(ENTERPRISE_ANALYTICS_BASE + sessionStorage.getItem('accountId') +'/zones/datausage?groupByPeriod='+groupBy+'&fromDate='+startDate+'&toDate='+endDate+'&startIndex=0&count=1000000&sortBy=datausage&sortDirection=ASC&includeSubAccounts='+consolidated, 'GET',headerAdd);

		}

		function getWeeklyDataUsageOfGroups(startDate, endDate) {
			var headerAdd=['READ_DATAUSAGE'];
			return commonUtilityService.prepareHttpRequest(ENTERPRISE_ANALYTICS_BASE + sessionStorage.getItem('accountId') +'/groups/datausage?groupByPeriod=WEEK&fromDate='+startDate+'&toDate='+endDate+'&startIndex=0&count=1000000&sortBy=datausage&sortDirection=ASC', 'GET',headerAdd);
		}

		function getDailyDataUsageOfGroups(startDate, endDate) {
			var headerAdd=['READ_DATAUSAGE'];
			return commonUtilityService.prepareHttpRequest(ENTERPRISE_ANALYTICS_BASE + sessionStorage.getItem('accountId') +'/groups/datausage?groupByPeriod=MONTH&fromDate='+startDate+'&toDate='+endDate+'&startIndex=0&count=1000000&sortBy=datausage&sortDirection=ASC', 'GET',headerAdd);
		}

		function getDailyDataUsageOfEnterprise(startDate, endDate) {
			var headerAdd=['READ_DATAUSAGE'];
			return commonUtilityService.prepareHttpRequest(ENTERPRISE_ANALYTICS_BASE + sessionStorage.getItem('accountId') +'/datausage?groupByPeriod=DAY&fromDate='+startDate+'&toDate='+endDate+'&startIndex=0&count=1000000&sortBy=datausage&sortDirection=ASC', 'GET',headerAdd);
		}

		function getDailyDataUsageOfSIMs(sim, startDate, endDate) {
			var headerAdd=['READ_DATAUSAGE'];
			return commonUtilityService.prepareHttpRequest(ENTERPRISE_ANALYTICS_BASE + sessionStorage.getItem('accountId') +'/sims/sim/'+sim+'/datausage?groupByPeriod=DAY&fromDate='+startDate+'&toDate='+endDate+'&startIndex=0&count=1000000&sortBy=datausage&sortDirection=ASC', 'GET',headerAdd);
		}

		function getDailyDataUsageOfUsers(user, startDate, endDate) {
			var headerAdd=['READ_DATAUSAGE'];
			return commonUtilityService.prepareHttpRequest(ENTERPRISE_ANALYTICS_BASE + sessionStorage.getItem('accountId') +'/users/user/'+user+'/datausage?groupByPeriod=DAY&fromDate='+startDate+'&toDate='+endDate+'&startIndex=0&count=1000000&sortBy=datausage&sortDirection=ASC', 'GET',headerAdd);
		}

		function  getEstimatedDataCost(consolidated){
			var headerAdd=['READ_DATAUSAGE'];
            consolidated = !!consolidated;
			return commonUtilityService.prepareHttpRequest(ENTERPRISE_ANALYTICS_BASE + sessionStorage.getItem('accountId') +'/zones/estimatedcost?startIndex=0&count=1000000&includeSubAccounts='+consolidated, 'GET',headerAdd);
		}

		function getUsageData(category,dateRange,groupPeriod){
			var headerAdd=['READ_DATAUSAGE'];
			var url = ENTERPRISE_ANALYTICS_BASE + sessionStorage.getItem('accountId')+'/' ;
			if(category === 'enterprise'){
				url += 'datausage';
				//return commonUtilityService.prepareHttpRequest(ENTERPRISE_ANALYTICS_BASE + sessionStorage.getItem('accountId') +'/datausage?groupByPeriod=DAY&fromDate='+startDate+'&toDate='+endDate+'&startIndex=0&count=1000000&sortBy=datausage&sortDirection=ASC', 'GET',headerAdd);
			}else{
				url += category+'/datausage';
			}
			url+='?groupByPeriod='+groupPeriod;
			url+='&fromDate='+dateObjToBackendDateString(dateRange.startDate)+' 00:00:00';
			url+='&toDate='+dateObjToBackendDateString(dateRange.endDate)+' 23:59:59';
			url+='&startIndex=0&count=1000000&sortBy=datausage&sortDirection=ASC';

			return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd);
		}

		function dateObjToBackendDateString(date){
			date = new Date(date);
			return date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
		}

        function getMonthlyDataUsageOfSubAccounts(startDate, endDate,groupBy) {

            var headerAdd=['READ_DATAUSAGE'];
            if(!groupBy)
            {
                groupBy = 'MONTH';
            }
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_ANALYTICS_BASE + sessionStorage.getItem('accountId') +'/groups/datausage?groupByPeriod='+groupBy+'&fromDate='+startDate+'&toDate='+endDate+'&startIndex=0&count=1000000&sortBy=datausage&sortDirection=ASC', 'GET',headerAdd);

        }

        /**
         * Returns top 10 highest usage by SIM
         * @param startDate
         * @param endDate
         * @param groupBy
         * @param consolidated
         * @return {*}
         */
        function getMonthlyHighestDataUsageBySIM(startDate, endDate, groupBy, consolidated) {
            var headerAdd=['READ_DATAUSAGE'];
            if(!groupBy)
            {
                groupBy = 'MONTH';
            }
            consolidated = !!consolidated;

            return commonUtilityService.prepareHttpRequest(ENTERPRISE_ANALYTICS_BASE + sessionStorage.getItem('accountId') +'/sims/datausage?groupByPeriod='+groupBy+'&fromDate='+startDate+'&toDate='+endDate+'&startIndex=0&count=10&sortBy=datausage&sortDirection=DESC&includeSubAccounts='+consolidated, 'GET',headerAdd);
        }

        /**
         * Returns top 10 highest usage by User
         * @param startDate
         * @param endDate
         * @param groupBy
         * @param consolidated
         * @return {*}
         */
        function getMonthlyHighestDataUsageByUser(startDate, endDate, groupBy, consolidated) {
            var headerAdd=['READ_DATAUSAGE'];
            if(!groupBy)
            {
                groupBy = 'MONTH';
            }
            consolidated = !!consolidated;

            return commonUtilityService.prepareHttpRequest(ENTERPRISE_ANALYTICS_BASE + sessionStorage.getItem('accountId') +'/users/datausage?groupByPeriod='+groupBy+'&fromDate='+startDate+'&toDate='+endDate+'&startIndex=0&count=10&sortBy=datausage&sortDirection=DESC&includeSubAccounts='+consolidated, 'GET',headerAdd);
        }
	}
})();