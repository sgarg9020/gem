'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('DuGroupsPieCtrl', GroupsPie);

	GroupsPie.$inject=['$scope', 'requestGraphData', 'colorShader','AccountService','dataUsageService','commonUtilityService','pieGraphToTable'];

    /**
     @constructor GroupsPie()
     @param {object} $scope - Angular.js $scope
     @param {object} requestGraphData - factory service
     @param {object} colorShader - factory service
	 @param {object} AccountService - factory service
	 @param {object} dataUsageService - factory service
	 @param {object} commonUtilityService - factory service
	 @param {object} pieGraphToTable - factory service
	 */
    function GroupsPie($scope, requestGraphData, colorShader,AccountService,dataUsageService,commonUtilityService,pieGraphToTable) {
        var groupsPie = this;
        var _colorShadeValue = {
	        count: 0,
	        bump: -15
        };

		$scope.graphObj = {};

		/**
		 * Start up logic for controller
		 */
		function activate(){
			$scope.graphObj.colName= "Group";
			$scope.graphObj.requestData = requestData;
			$scope.graphObj.updateGraphOptions = addOrUpdateOptions;
			$scope.graphObj.toolTipCache = [];
			$scope.graphObj.graphDataModalID = "groupsPieDataModal";
			$scope.graphObj.getdata = getdata;
		}

        function addOrUpdateOptions(){
	        if($scope.graphObj.options)
		        delete $scope.graphObj.options;
		        
		    calculateLegendColumns();

			$scope.graphObj.options = {
		    	title: {
					enable: true,
					text: "Group Data Usage",
					className: "nvd3-gs-title",
					css: {
		               textAlign: "center",
		               textTransform: "uppercase",
		               fontFamily: "Montserrat",
		               marginTop: 15,
		               marginBottom: 0
	                }
				},
				subtitle: {
					enable: true,
					text: function() {
						return 'From ' + $scope.graphObj.start.getDate()+'-'+ $scope.graphObj.start.getMonthName()+'-'+ $scope.graphObj.start.getFullYear()+' To '+ $scope.graphObj.end.getDate()+'-'+ $scope.graphObj.end.getMonthName()+'-'+ $scope.graphObj.end.getFullYear();
					},
		            className: "nvd3-gs-subtitle", //Doesn't work with current NVD3.js
		            css: {
			            textAlign: "center",
			            fontSize: 10,
			            lineHeight: 1,
			            marginBottom: 15
		            }
		        },
	            chart: {
	                type: 'pieChart',
	                height: groupsPie.calculatedHeight,
	                width: getWidth(),
	                donut: false,
	                growOnHover: true, //Doesn't work with current NVD3.js
	                pieLabelsOutside: true,
	                cornerRadius: 5,
	                color: function(d, i){
		                if(d.color){
		                	return d.color;}
		                else{
			                if(i%8 == 0){
			                	if(i == 0){
			                		_colorShadeValue.count = 0;}
			                	else{
			                		_colorShadeValue.count += 3;}
			                }
			                
			                var shadePer = parseFloat(_colorShadeValue.count*_colorShadeValue.bump/100);
			                
			                if(shadePer > .6 || shadePer < -.6){
			                	_colorShadeValue.count = 0;
			                	shadePer = parseFloat(_colorShadeValue.count*_colorShadeValue.bump/100);
							}
			                
			                return colorShader.shade(GRAPH_COLORS[i%8],shadePer);}
	                },
	                tooltips: true,
	                tooltip: {
						width: 200,
		                gravity: "n", //Doesn't work with current NVD3.js
		                distance: 25
	                },
	                tooltipContent: function(key, y, obj, graph) {

						var enabledTotal = $scope.graphObj.data
						var enabledTotal = $scope.graphObj.data
								.filter(function(item){return !item.disabled;})
								.reduce(function(a, b){return a + b.dataUsage}, 0);

						var per = Math.round((obj.value/enabledTotal) * 100);

						var tickUnit = commonUtilityService.getDataUsageUnit(obj.point.dataUsage);
						var tickVal = commonUtilityService.getUsageInUnit(obj.point.dataUsage,tickUnit);

			            return '<div class="nvd3-gs-title"><p>'+key+'</p><div class="nvd3-gs-usage">'+tickVal+'<span class="nvd3-gs-usage-unit">'+tickUnit+'</div><div class="nvd3-gs-date">'+per.toFixed(2)+'%</div>';
			        },
			        labelType: "percent",
	                x: function(d){return d.name;},
	                y: function(d){return d3.format('g')(d.dataUsage);},
					showLabels: true,
					labelThreshold:0.01,
	                transitionDuration: 500,
	                legend: {
						align: true,
						rightAlign: false,
	                    margin: {
	                        top: 5,
	                        right: 0,
	                        bottom: 25,
	                        left: 0
	                    }
	                },
	                noData: "There is no data to display."
	            }
	        };

	    }

		function getWidth(){
			var clientWidth = document.body.clientWidth;
			var bodyHasMenuPin = $('body').hasClass('menu-pin');
			var width = clientWidth-30;// -30 because of panel padding
			if(clientWidth > 1200 || (clientWidth > 700 && !bodyHasMenuPin)){
				width = 500;
			}else if(clientWidth > 700 && bodyHasMenuPin){
				width = 400;
			}

			return width;
		}

		function requestData(callback) {
			AccountService.getBillingCycle().success(function (data) {
				
				getdata(callback,data.billingStart,data.billingEnd);
			}).error(function (data) {
				commonUtilityService.showErrorNotification(data.errorStr);
			});
		}

		function getdata(callback,startDay,endDay){
			if(groupsPie.startDay != startDay || groupsPie.endDay != endDay || !$scope.graphObj.data || $scope.graphObj.data.length<1) {
				requestGraphData.getDailyDataUsageOfGroups(startDay,endDay).success(function(data, status, headers, config){
					groupsPie.startDay = startDay;
					groupsPie.endDay = endDay;
					data.list = sortByTotalDataUsedInBytes(data.list,-1);
					var dataList = dataUsageService.prepareGroupUsageData(data);
					$scope.graphObj.data = dataList;
					$scope.graphObj.completeData = dataList;
					// Set toolTip Cache data for the graph details table in modal
					if($scope.graphObj.data){
						$scope.graphObj.toolTipCache = pieGraphToTable.make($scope.graphObj.data);
					}
					$scope.graphObj.data = filterData($scope.graphObj.data,$scope.graphObj.toolTipCache);

					sumData();
					calculateLegendColumns();
					$scope.graphObj.updateGraphOptions();
					$scope.graphObj.api.updateWithOptions($scope.graphObj.options);
					prepareURL(config);
					callback($scope.graphObj);
				}).error(function(data){
					commonUtilityService.showErrorNotification(data.errorStr)
				});
			}else{

				callback( $scope.graphObj);
			}
		}

		/**
		 * It filters  15 zones with highest dat-usage, remaining zones will make new group as 'Others'
		 * and within those 15 , again zones with less then 2% data-usage will become part of 'Others'.
		 * @param tableData
		 * @param toolTipCache
		 * @returns {Array}
		 */
		function filterData(tableData, toolTipCache) {
			var filteredData = [];

			if (tableData.length > 15) {
				var otherDataUsage = 0;
				for (var i = 0; i < tableData.length; i++) {

					if (i >= 15) {
						otherDataUsage = otherDataUsage + tableData[i].dataUsage;
					} else if (toolTipCache[i].percent < 2) {
						otherDataUsage = otherDataUsage + tableData[i].dataUsage;
					} else {
						filteredData.push(tableData[i]);
					}
				}
			} else {
				var otherDataUsage = 0;
				for (var i = 0; i < tableData.length; i++) {
					if (toolTipCache[i].percent < 2) {
						otherDataUsage = otherDataUsage + tableData[i].dataUsage;
					} else {
						filteredData.push(tableData[i]);
					}
				}
			}
			if (otherDataUsage > 0) {
				var obj = {};
				obj.name = "All other groups";
				obj.dataUsage = otherDataUsage;

				filteredData.push(obj);
			}
			return filteredData;
		}

		//sortOder -1 is for DESC and 1 is for ASC
		//This function needs to be removed once sortBy dataUsage is available in backend api
		function sortByTotalDataUsedInBytes(values,sortOrder){
			values.sort(function(a,b){
				var x = a.totalDataUsedInBytes;
				var y = b.totalDataUsedInBytes;
				if (x === undefined) x = 0;
				if (y === undefined) y = 0;
				return ((x > y) ? sortOrder : ((x < y) ? -sortOrder : 0));
			});

			return values;
		}
		
				
		function sumData(){
			groupsPie.dataSum = 0;
			for(var i=0;i<$scope.graphObj.data.length;i++){
				groupsPie.dataSum += $scope.graphObj.data[i].dataUsage;
			}	        			
		}
		
		function calculateLegendColumns(){
			if($scope.graphObj.data){
	
				/*
					Estimation for how many legend columns nvd3 with produce.
					Not 100% accurate since we use an averaged legend character
					length. (legendCharLength)
					
					Todo - calculate each legend column width 100% accurately
					by allowing the graph to render first	
				*/
	
				var legendCharLength = 5.7;
				var spacing = 28;			
	
				var graphWidth = ((document.body.clientWidth > 700 ? 500 : document.body.clientWidth - 30) - 40);
				var legendWidth = 0;
				var seriesPerRow = 0;
				var columnWidths = [];
				
				while (legendWidth < graphWidth && seriesPerRow < $scope.graphObj.data.length) {
		          columnWidths[seriesPerRow] = (String($scope.graphObj.data[seriesPerRow].name).length * legendCharLength) + spacing;
		          legendWidth += (String($scope.graphObj.data[seriesPerRow++].name).length * legendCharLength) + spacing;
		        }
	
		        if (seriesPerRow === 0) seriesPerRow = 1;
		        
		        while (legendWidth >= graphWidth && seriesPerRow > 1 ) {
		          columnWidths = [];
		          seriesPerRow--;
		
		          for (var i=0; i<$scope.graphObj.data.length; i++) {
		            if ((String($scope.graphObj.data[i].name).length * legendCharLength) + spacing > (columnWidths[i%seriesPerRow] || 0) )
		              columnWidths[i%seriesPerRow] = (String($scope.graphObj.data[i].name).length * legendCharLength) + spacing;
		          }
		
		          legendWidth = columnWidths.reduce(function(prev, cur, index, array) {
		                          return prev + cur;
		                        });
		        }
		        
		        var columns = columnWidths.length;
		        var columnHeight = ($scope.graphObj.data.length/columns)*20;
		        
		        if(graphWidth > 700){
					groupsPie.calculatedHeight = (columnHeight * 3) + 85; // 85 for heading and legend margins
				} else {
					groupsPie.calculatedHeight = columnHeight + 300 + 85; // 300 for chart, 85 for heading
				}
			}
			
		}

		function prepareURL(config){
			$scope.url = config.url+'&fileType=CSV&token='+encodeURIComponent(config.headers.token);
		}
		activate();

    }
})();