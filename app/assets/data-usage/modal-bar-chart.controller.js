'use strict';

/* Controllers */
(function(){
	angular.module('app')
	    .controller('ModalBarChartCtrl', ModalBarChart);

	ModalBarChart.$inject=['$scope', 'modalDetails', 'requestGraphData','dataUsageService','commonUtilityService','AccountService'];
	 
	/**
		@constructor ModalBarChart()
		@param {object} $scope - Angular.js $scope
		@param {object} modalDetails - Factory object
		@param {object} requestGraphData - factory service
	*/   
	function ModalBarChart($scope, modalDetails, requestGraphData, dataUsageService,commonUtilityService, AccountService ){

		var modalBarChart = this;

		$scope.graphObj = {};

		/**
		 * Start up logic for controller
		 */
		function activate(){
			$scope.$parent.graphObj.setSIM = setSIM;
			$scope.$parent.graphObj.setUser = setUser;
			$scope.prevSIM;
			$scope.graphObj.requestData = requestData;
			$scope.graphObj.updateGraphOptions = addOrUpdateOptions;
			$scope.graphObj.clearData = clearData;
			$scope.graphObj.graphLabel = null;

			/* ModalDetails factory */
			modalDetails.scope(modalBarChart, true);
			modalDetails.attributes({
				id: 'barChartModal',
				formName: '',
				header: 'SIM Data Usage',
				cancel: '',
				submit: 'Close'
			});
		}

		function clearData(){
			if( $scope.graphObj.data ){
				delete $scope.graphObj.data;
			}
		}
		function addOrUpdateOptions(){

			if($scope.graphObj.options){
		        delete $scope.graphObj.options;}

			$scope.graphObj.options = {
		    	title: {
	                enable: true,
	                text: !modalBarChart.userFlag?'SIM Data Usage':'User Data Usage',
	                className: 'nvd3-gs-title', //Doesn't work with current NVD3.js
	                css: {
		               textAlign: "center",
		               textTransform: "uppercase",
		               fontFamily: "Montserrat",
		               marginTop: 25,
		               marginBottom: 0
	                }
	            },
	            subtitle: {
		            enable: true,
		            text: 'for current billing period',
		            className: 'nvd3-gs-subtitle', //Doesn't work with current NVD3.js
		            css: {
			            textAlign: "center",
			            fontSize: 10,
			            lineHeight: 1
		            }
	            },
	            chart: {
	                type: 'historicalBarChart',
	                transitionDuration: 500,
					height: (document.body.clientHeight <= 360 ? 220 : (document.body.clientHeight < 440 ? 260 : (document.body.clientWidth > 650 ? 450 : document.body.clientWidth * (450 / 600)))),

					margin : {
	                    top: 20,
						right: ($(document).outerWidth() > 991 ? 70 : (document.body.clientWidth > 768 ? 60 : 25)),

						bottom: 50,
	                    left: 70
	                },
	                color: function (d, i) {
		                if(d.color)
		                	return d.color
		                return GRAPH_COLORS[1];
			        },
	                showLegend: false,
	                useInteractiveGuideline: true,
	                tooltips: true,
	                tooltip: {
		                gravity: "n", //Doesn't work with current NVD3.js
		                distance: 25
	                },

	                tooltipContent: function(key, y, e, graph) {
		                var date = d3.time.format('%b %d')(new Date(graph.series.values[graph.pointIndex].x));
						var tickUnit = commonUtilityService.getDataUsageUnit(graph.point.dataUsage);
						var tickVal = commonUtilityService.getUsageInUnit(graph.point.dataUsage,tickUnit);
						return '<div class="nvd3-gs-usage">'+tickVal+'<span class="nvd3-gs-usage-unit">'+tickUnit+'</div><div class="nvd3-gs-date">'+date+'</div>';
			        },
			        clipEdge: true,
	                showValues: true,
	                x: function(d){return d.x},
	                xAxis: {
	                    axisLabel: modalBarChart.xaxisLabel, //This will need to be addressed post EFB Users Forum
	                    axisLabelDistance: 25,
	                    tickFormat: function(d) {	                    
	                        return d3.time.format('%d')(new Date(d));
	                    },
	                    tickValues: function (d) {
				            var dateSets = [];
				            for (var i = 0; i < d[0].values.length; i++) {
					            if(i%3 == 0)
				               		dateSets.push(d[0].values[i].x);
				               	else
				               		dateSets.push('');
				            }
				            return dateSets;
				        },
				        height: 100,
				        tickPadding: 7,
	                    rotateLabels: 0,
	                    showMaxMin: false
	                },
	                y: function(d){return d.y},
	                yAxis: {
	                    axisLabel: modalBarChart.yaxisLabel,
	                    axisLabelDistance: 35,
	                    tickFormat: function(d){
	                        return d3.format(',g')(d);
	                    }
	                },
		            noData: "There is no data to display."
                }
	        };

	        try{

				$scope.graphObj.api.update();}
	       catch(e){
		         console.info('Modal update not available yet.');}
		}

        /**
	    	@func setSIM
	    	@param {string} id - id used to get the SIM from the database
	    	@desc - responsible for adding data to the object which controls the bar chart.
	    */
        function setSIM(id){

			modalBarChart.SIM = id;
			modalBarChart.userFlag = false;
			$scope.graphObj.graphLabel = "sim data usage";
	        angular.element('#'+modalBarChart.modal.id).modal('show');
        }
        
        /**
	    	@func setUser
	    	@param {string} id - id used to get the user from the database
	    	@desc - responsible for adding data to the object which controls the bar chart.
	    */
        function setUser(id,userName){
			modalBarChart.user = id;
			modalBarChart.userName = userName;
			modalBarChart.userFlag = true;
			$scope.graphObj.graphLabel = "user data usage";
	        angular.element('#'+modalBarChart.modal.id).modal('show');
        }
        
        function requestData(callback) {
			//angular.element( "svg").style.opacity = "0";

            AccountService.getBillingCycle().success(function (data) {
                if( !modalBarChart.userFlag ) getSIMUsageData(callback,data.billingStart,data.billingEnd);
                else getUserUsageData( callback, data.billingStart, data.billingEnd );
            }).error(function (data) {
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

        function getSIMUsageData( callback, start, end ) {

			console.log( "modalBarChart data::");
			console.log( $scope.graphObj.data );
			console.log( "prevSIM::");
			console.log( $scope.prevSIM );

            if( ( !$scope.graphObj.data && typeof $scope.prevSIM == "undefined" )  || ( $scope.prevSIM && $scope.prevSIM != modalBarChart.SIM )) {

				modalBarChart.callback = callback;
				modalBarChart.start = start;
				modalBarChart.end = end;
                requestGraphData.getDailyDataUsageOfSIMs(modalBarChart.SIM, start,end).success( successCbk ).error(failureCbk );
                $scope.prevSIM = modalBarChart.SIM;


            }
            else{
				$scope.graphObj.data = JSON.parse( sessionStorage.getItem( "graphData") );
				modalBarChart.callback($scope.graphObj);
            }

        }

        function getUserUsageData( callback, start, end ) {

            if( ( !$scope.graphObj.data && typeof $scope.prevUser == "undefined" )  || ( $scope.prevUser && $scope.prevUser != modalBarChart.user )) {
				modalBarChart.callback = callback;
				modalBarChart.start = start;
				modalBarChart.end = end;
                requestGraphData.getDailyDataUsageOfUsers(modalBarChart.user, start,end).success( successCbk ).error(failureCbk );
                $scope.prevUser= modalBarChart.user;
			}
            else{
				$scope.graphObj.data = JSON.parse( sessionStorage.getItem( "graphData") );
				modalBarChart.callback($scope.graphObj);
			}

        }

        function successCbk(data, status, headers, config ){
			//angular.element( "svg").style.opacity = "1";


			$scope.graphObj.data = dataUsageService.prepareDayWiseUsageData(data, modalBarChart.start, modalBarChart.end );
			console.log( "data::");
			console.log( $scope.graphObj.data );

			prepareURL(config);
			modalBarChart.yaxisLabel = $scope.graphObj.data[0].usageUnit +' Used';
            if($scope.graphObj.data ) {
                var startM = getDateObj( modalBarChart.start).getMonthName();
                var endM = getDateObj( modalBarChart.end).getMonthName();
				modalBarChart.xaxisLabel = 'Days in ' + startM +  ( startM != endM  ? " - " +  endM  :"" );
				modalBarChart.callback($scope.graphObj);
				addOrUpdateOptions();
				sessionStorage.setItem( "graphData", JSON.stringify($scope.graphObj.data ));
            }
        }

		function prepareURL(config){
			$scope.url = config.url+'&fileType=CSV&token='+encodeURIComponent(config.headers.token);
		}

        function failureCbk(data ){
            commonUtilityService.showErrorNotification(data.errorStr);

        }

		activate();

    }
})();
