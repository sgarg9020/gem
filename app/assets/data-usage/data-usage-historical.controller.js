'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('DataUsageHistoricalCtrl', DataUsageHistorical);

    DataUsageHistorical.$inject = ['$scope', 'requestGraphData', 'AccountService', 'dataUsageService', 'commonUtilityService', 'loadingState','$location'];

    /**
     @constructor EnterpriseAccount()
     @param {object} $scope - Angular.js $scope
     @param {object} requestGraphData - factory service
     @param {object} AccountService - factory service
     @param {object} dataUsageService - factory service
     @param {object} commonUtilityService - factory service
     @param {object} loadingState - factory service
     */
    function DataUsageHistorical($scope, requestGraphData, AccountService, dataUsageService, commonUtilityService, loadingState, $location) {
        var dataUsageHistorical = this;
        // var curDate = sessionStorage.getItem("date");
        // if(!curDate)
        // {
        //     curDate = new Date();
        // }


        $scope.graphObj = {};

        $scope.graphObj.month = new Date();
        $scope.groupBy = 'DAY';
        $scope.category = $scope.isSubAccount ? 'enterprise' : 'groups';
        $scope.legendSelected = false;
        $scope.graphType = 'multiBarChart';
        $scope.enabledLegendItems = 10;
        $scope.groupbyDayEnabled = true;
        $scope.enableImport = false;
        $scope.previousKey = -1;
        $scope.groupTabLabel = $scope.isResellerAccount ? 'Consolidated Data' : 'Group';
        var accountName = sessionStorage.getItem("accountName");
        $scope.dataUsageCatagories = {
            groups: $scope.isResellerAccount ? 'Consolidated Accounts under '+accountName : accountName + ' Groups Data Usage',
            enterprise: accountName +' Data Usage',
            zones: accountName +' Zones Data Usage'
        };

        /**
         * Start up logic for controller
         */
        function activate() {
            setCategory();
            configureCalendar(new Date());
            requestData();
        }

        function setCategory(){
            var currentActiveTab = $location.search();
            if(currentActiveTab.ct === '2'){
                $scope.category = 'enterprise';
            }else if(currentActiveTab.ct === '3'){
                $scope.category = 'zones';
            }else{
                $location.search ({ct: 1});
            }
        }

        function configureCalendar(curDate){
            //setting minDate to last one year
            var today = new Date(curDate);
            today.setFullYear(today.getFullYear()-1);

            $scope.opts = {
                minDate: moment(today),
                maxDate: moment(curDate),
                startDate:moment(curDate).startOf('month'),
                endDate:moment(curDate).endOf('month'),
                linkedCalendars: true,
                opens:'left',

                locale: {
                    applyClass: 'btn-green',
                    applyLabel: "Apply",
                    fromLabel: "From",
                    format: "DD/MMM/YY",
                    toLabel: "To",
                    cancelLabel: 'Cancel',
                    customRangeLabel: 'Custom range'
                },
                ranges: {
                    'Last 7 Days': [moment(curDate).subtract(6, 'days'), moment(curDate)],
                    'Last 30 Days': [moment(curDate).subtract(29, 'days'), moment(curDate)],
                    'Last 90 Days': [moment(curDate).subtract(90, 'days'), moment(curDate)],
                    'Last 12 Months': [moment(curDate).subtract(11, 'months').startOf('month'), moment(curDate).endOf('month')]
                },
                eventHandlers : {
                    'apply.daterangepicker' : function() {
                        $scope.dateChange();
                    }
                }
            };
        }
        $scope.datePicker = {};
        $scope.graphObj.options = {
            title: {
                enable: true,
                text: $scope.dataUsageCatagories[$scope.category],
                className: 'nvd3-gs-title', //Doesn't work with current NVD3.js
                css: {
                    textAlign: "center",
                    textTransform: "uppercase",
                    fontFamily: "Montserrat",
                    marginTop: 15,
                    marginBottom: 0
                }
            },
            subtitle: {
                enable: true,
                text: function () {
                    return 'From : ' + $scope.datePicker.startDate.format('DD/MMM/YY') + ' To : ' + $scope.datePicker.endDate.format('DD/MMM/YY');
                },
                className: 'nvd3-gs-subtitle', //Doesn't work with current NVD3.js
                css: {
                    textAlign: "center",
                    fontSize: 10,
                    lineHeight: 1,
                    marginBottom: 15
                }
            },
            chart: {
                stacked: true,
                type: $scope.graphType,
                transitionDuration: 500,
                height: 500,
                margin: {
                    top: 25,
                    right: ($(document).outerWidth() > 991 ? 70 : (document.body.clientWidth > 768 ? 60 : 25)),
                    bottom: 100,
                    left: 70
                },
                clipEdge: false,
                showValues: true,
                showLegend: false,
                showControls: false,
                useInteractiveGuideline: true,
                tooltips: true,
                objectEquality: true,
                staggerLabels: false,
                padData: true,
                padDataOuter: 10,
                showTotalInTooltip: true,
                totalLabel: 'TOTAL',
                tooltip: {
                    width: 200,
                    gravity: "n", //Doesn't work with current NVD3.js
                    distance: 25
                },
                tooltipContent: function (key, y, e, graph) {

                    var tickUnit = commonUtilityService.getDataUsageUnit(graph.point.dataUsage);
                    var tickVal = commonUtilityService.getUsageInUnit(graph.point.dataUsage, tickUnit);

                    return '<div class="nvd3-gs-title"><p>' + key + '</p><div class="nvd3-gs-usage">' + tickVal + '<span class="nvd3-gs-usage-unit">' + tickUnit + '</div><div class="nvd3-gs-date">' + y + '</div>';
                },
                // xScale : d3.time.scale(),
                xAxis: {
                    // ticks : d3.time.days,
                    axisLabel: 'Days',
                    showMaxMin: false,
                    // tickPadding: 10,
                    tickFormat: function (d) {
                        if ($scope.groupBy == 'DAY')
                            return d3.time.format('%d-%b')(new Date(d));
                        return d3.time.format('%b-%y')(new Date(d));
                    }
                },
                yAxis: {
                    axisLabel: 'Bytes Used',
                    axisLabelDistance: 25,
                    tickFormat: function (d) {
                        return d3.format(',g')(parseFloat(d.toFixed(2)));
                    }
                },
                noData: "There is no data to display.",
                forceY: [0, 1],
                callback: function () {
                    ($scope.groupBy == 'MONTH') ? alignXAxisLabel() : '';
                    loadingState.hide();
                }
            }
        };

        function requestData() {

            AccountService.getBillingCycle().success(function (data) {
                dataUsageHistorical.startDay = data.billingStart;
                dataUsageHistorical.endDay = data.billingEnd;
                $scope.datePicker = {
                    startDate: moment(dataUsageHistorical.startDay).startOf('day'),
                    endDate: moment(dataUsageHistorical.endDay).endOf('day')
                };
                configureCalendar(data.currentDate);
                getDataHistorical($scope.category, $scope.datePicker, $scope.groupBy);
            }).error(function (data) {
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

        function disableAllLegends(state) {
            for (var i = 0; i < state.disabled.length; i++) {
                state.disabled[i] = true;
                console.log(state.disabled[i]);
            }
        }

        function enableLegends(state, legendsToEnable) {
            if (state.disabled.length < legendsToEnable)
                legendsToEnable = state.disabled.length;

            for (var i = 0; i < legendsToEnable; i++) {
                state.disabled[i] = false;
            }
        }

        /**
         * This function updates y axis tick values and y axis label based on usageUnit.
         * @param legend
         * @param usageUnit
         * @param key
         */
        function updateYAxisTicksForLegend(legend, usageUnit) {
            for(var i=0; i<legend.values.length ; i++){
                var val = legend.values[i];
                val.y = commonUtilityService.getUsageInUnit(val.dataUsedInBytes, usageUnit);
            }

            $scope.graphObj.options.chart.yAxis.axisLabel = usageUnit + ' Used';
        }

        $scope.legendElementClick = function (key) {

            var chart = $scope.graphObj.api.getScope().chart;
            var state = chart.state();

            //updating previously updated yaxis values.
            if($scope.previousKey != -1){
                var legend = $scope.graphObj.data[$scope.previousKey];
                updateYAxisTicksForLegend(legend,legend.usageUnit);
                $scope.previousKey = -1;
            }

            if ($scope.legendSelected && !state.disabled[key]) {
                //enabled and modified state
                //set default state
                state.disabled[key] = true;
                enableLegends(state, $scope.enabledLegendItems);
                $scope.legendSelected = false;

            } else {
                //enabled legend and default state
                //disable all enable this
                disableAllLegends(state);
                console.log($scope.graphObj.api.getScope().chart.state().disabled);
                state.disabled[key] = false;
                $scope.legendSelected = true;

                var legend = $scope.graphObj.data[key];
                var legendUsageUnit = commonUtilityService.getDataUsageUnit(legend.totalDataUsedInBytes);
                if(legend.usageUnit != legendUsageUnit){
                    updateYAxisTicksForLegend(legend, legendUsageUnit, key);
                    $scope.previousKey = key;
                }
            }
            console.log($scope.graphObj.api.getScope().chart.state().disabled);

            chart.dispatch.changeState(state);
            chart.update();
        };


        $scope.categoryChange = function (category, $event) {
            if (!angular.element($event.target.parentNode).hasClass('active')) {
                setCurrentActiveTab(category);
                $scope.category = category;
                $scope.legendSelected = false;
                $scope.previousKey = -1;
                getDataHistorical(category, $scope.datePicker, $scope.groupBy);
            }
        };

        function setCurrentActiveTab(category){
            if(category == 'groups'){
                $location.search ({ct: 1});
            }else if(category == 'enterprise'){
                $location.search ({ct: 2});
            }else{
                $location.search ({ct: 3});
            }
        }

        $scope.graphTypeChange = function () {
            $scope.graphObj.options.chart.type = $scope.graphType;
            //getDataHistorical($scope.category, $scope.datePicker, $scope.groupBy);
        };
        $scope.groupByChange = function () {
            // if($scope.groupBy == 'DAY')
            //     $scope.graphObj.options.chart.xAxis.ticks = d3.time.days;
            // else
            //     $scope.graphObj.options.chart.xAxis.ticks = d3.time.months;
            $scope.previousKey = -1;
            $scope.legendSelected = false;
            if ($scope.groupBy == 'MONTH' && (!$scope.datePicker.startDate.isSame($scope.datePicker.startDate.clone().startOf('month')) || !$scope.datePicker.endDate.isSame($scope.datePicker.endDate.clone().endOf('month')))) {
                $scope.datePicker = {
                    startDate: $scope.datePicker.startDate.clone().startOf('month'),
                    endDate: $scope.datePicker.endDate.clone().endOf('month')
                };
                commonUtilityService.showSuccessNotification('Date range adjusted to align the start and end of the months');
            }
            getDataHistorical($scope.category, $scope.datePicker, $scope.groupBy);
        };

        $scope.dateChange = function () {
            $scope.opts.startDate = $scope.datePicker.startDate;
            $scope.opts.endDate = $scope.datePicker.endDate;
            $scope.previousKey = -1;
            $scope.legendSelected = false;
            //if >90 set to months if not months and change date if not exact month

            if ($scope.datePicker.endDate.diff($scope.datePicker.startDate, 'days') > 90) {
                $scope.groupbyDayEnabled = false;
                if ($scope.groupBy != 'MONTH') {
                    $scope.groupBy = 'MONTH';
                }
                if (!$scope.datePicker.startDate.isSame($scope.datePicker.startDate.clone().startOf('month')) || !$scope.datePicker.endDate.isSame($scope.datePicker.endDate.clone().endOf('month'))) {
                    $scope.datePicker = {
                        startDate: $scope.datePicker.startDate.clone().startOf('month'),
                        endDate: $scope.datePicker.endDate.clone().endOf('month')
                    };
                    commonUtilityService.showSuccessNotification('Date range adjusted to align the start and end of the months');
                }
            }
            //if <90 , existing is not days and selected date is not exact month, set to days
            else if ($scope.groupBy != 'DAY') {
                if(!$scope.datePicker.startDate.isSame($scope.datePicker.startDate.clone().startOf('month')) || !$scope.datePicker.endDate.isSame($scope.datePicker.endDate.clone().endOf('Month'))){
                    $scope.groupBy = 'DAY';
                }
                $scope.groupbyDayEnabled = true;
            }
            var firstDay = new Date($scope.datePicker.startDate);
            var lastDay = new Date($scope.datePicker.endDate);

            getDataHistorical($scope.category, $scope.datePicker, $scope.groupBy);
        };

        function getDataHistorical(category, dateRange, groupPeriod) {

            function onGraphDataResponse(data, status, headers, config){

                $scope.enableImport = data && data.totalDataUsedInBytes;
                if (category == 'enterprise') {
                    dataUsageHistorical.preparedData = dataUsageService.prepareEnterpriseUsageData(data, dateRange, groupPeriod);
                    //$scope.graphObj.data = dataUsageHistorical.preparedData.data;
                    $scope.graphObj.data = dataUsageHistorical.preparedData.totalDataUsedInBytes?dataUsageHistorical.preparedData.data:[];
                    $scope.graphObj.options.chart.xAxis.axisLabel = dataUsageHistorical.preparedData.groupPeriod;
                    $scope.graphObj.options.chart.yAxis.axisLabel = dataUsageHistorical.preparedData.usageUnit + ' Used';
                }
                else {

                    dataUsageHistorical.preparedData = dataUsageService.prepareDataUsageHistorical(data, dateRange, groupPeriod, $scope.enabledLegendItems);
                    $scope.graphObj.data = dataUsageHistorical.preparedData.totalDataUsedInBytes?dataUsageHistorical.preparedData.data:[];
                    $scope.graphObj.options.chart.xAxis.axisLabel = dataUsageHistorical.preparedData.groupPeriod;
                    $scope.graphObj.options.chart.yAxis.axisLabel = dataUsageHistorical.preparedData.usageUnit + ' Used';
                }

                $scope.legendUsageLabel = $scope.graphObj.options.chart.yAxis.axisLabel;
                if ($scope.graphObj.data && $scope.graphObj.data.length && $scope.graphObj.data[0].values && $scope.graphObj.data[0].values.length && $scope.graphObj.data[0].values.length < 14) {
                    var tickValues = [];
                    for (var i = 0; i < $scope.graphObj.data[0].values.length; i++) {
                        tickValues[i] = $scope.graphObj.data[0].values[i].x;
                    }
                    $scope.graphObj.options.chart.xAxis.tickValues = tickValues;
                    $scope.graphObj.options.chart.reduceXTicks = false;
                }
                else {
                    $scope.graphObj.options.chart.xAxis.tickValues = null;
                    $scope.graphObj.options.chart.reduceXTicks = true;
                }
                $scope.graphObj.options.title.text = $scope.dataUsageCatagories[$scope.category];
                prepareURL(config);
                redrawGraph($scope.graphObj);
            }
             requestGraphData.getUsageData(category, dateRange, groupPeriod).success(onGraphDataResponse).error(function (data) {
                 commonUtilityService.showErrorNotification(data.errorStr)
             });
        }

        /**
         * This function aligns x-axis in the middle, as text-anchor:middle is not working in case of groupBy Months.
         * This will be taken care automatically after upgrading nvd3 version,So this can be removed in the future.
         */
        function alignXAxisLabel(){
            var texts = $('.nv-axislabel');
            for(var i=0 ; i<texts.length ; i++){
                var content = $('.nv-axislabel')[i].textContent;
                if(content == 'Months'){
                    var svgWidth = $('svg').width();
                    var text = $('.nv-axislabel')[i]
                    $(text).attr("x",(svgWidth/2)-70);
                    break;
                }
            };
        }

        function prepareURL(config) {
            $scope.url = config.url + '&fileType=CSV&token=' + encodeURIComponent(config.headers.token);
        }

        function redrawGraph(viewModel) {
            if (!viewModel.api.getScope().chart) {
                viewModel.api.updateWithOptions(viewModel.options);
            }

            settingCurrentTabActive();
            loadingState.hide();
        }

        function settingCurrentTabActive(){
            if($scope.category == 'enterprise'){
                $('#dataUsageTab a[id="enterpriseTab"]').tab('show');
            }else if($scope.category == 'zones'){
                $('#dataUsageTab a[id="zonesTab"]').tab('show');
            }
        }

        activate();
    }
})();
