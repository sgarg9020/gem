'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('DuZoneMultiBarCtrl', ZonesMultiBar);

    ZonesMultiBar.$inject=['$scope', 'requestGraphData','commonUtilityService'];

    /**
     @constructor ZonesMultiBar()
     @param {object} $scope - Angular.js $scope
     @param {object} requestGraphData - factory service
     */
    function ZonesMultiBar($scope, requestGraphData,commonUtilityService) {
        var zonesMultiBar = this;

        $scope.graphObj = {};
        /**
         * Start up logic for controller
         */
        function activate(){
            $scope.graphObj.requestData = requestData;

            $scope.graphObj.options = {
                chart: {
                    type: 'multiBarChart',
                    height: 450,
                    margin : {
                        top: 20,
                        right: 100,
                        bottom: 120,
                        left: 100
                    },
                    clipEdge: true,
                    staggerLabels: true,
                    transitionDuration: 500,
                    stacked: true,
                    xAxis: {
                        axisLabel: 'Weeks',
                        showMaxMin: false,
                        tickFormat: function(d){
                            return d3.time.format('%x')(new Date(d))
                        }
                    },
                    yAxis: {
                        axisLabel: 'MB Used',
                        axisLabelDistance: 40,
                        tickFormat: function(d){
                            return d3.format(',.2f')(d);
                        },
                        margin: {
                            left: 100
                        }
                    }
                }
            };
        }

        function requestData(callback){
            if(!$scope.graphObj.data) {
                requestGraphData.getDataUsage('app/data/data-usage/zones-multibar.json').success(function(data){
                    $scope.graphObj.data =data;
                    callback($scope.graphObj);
                }).error(function(data){
                    commonUtilityService.showErrorNotification(data.errorStr)
                });
            }else{
                callback($scope.graphObj);
            }
        }

        activate();
    }
})();