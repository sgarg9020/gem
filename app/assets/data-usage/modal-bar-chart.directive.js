'use strict';

/* Directive */
(function(){
	angular.module('app')
		.directive('gsModalBarChart', ModalBarChart);

	ModalBarChart.$inject = ['$timeout', 'debounce'];
	
	/**
		@function modalBarChart()	
		@param {object} $timeout - Angular.js $timeout service
		@param {object} debounce - factory service
	*/	
	function ModalBarChart($timeout, debounce) {
		
		var user = {};
		
		var link = {
			link: link
		};
		
		return link;
		
		/**
			@function link()
			@param {object} $scope - directive scope
			@param {object} element - directive HTML element
			@param {object} attrs - attributes on the directive HTML element
			
		*/
		function link($scope, element, attrs) {
			
			var resizeDebounce;
			
			/**
				@function addCallBack()
				@desc - addCallBack is responsible for adding a call back function
				to each charts plugin options.
			**/
			function addCallBack(){
				// this function can be invoked twice if options and data update
				$scope.$parent.graphObj.options.chart.callback = function(){
					toggleLoadingState(false);
				}
			};
			
			/**
				@function toggleLoadingState()
				@param {boolean} show - truthy value to show or hide the loading box
				@desc - toggleLoadingState is responsible for toggling the spinner
				icon in the bar chart modal. It will be called by the nvd3 callback
				for a graph being complete and when a new nvd3 graph is requested.
			*/
			function toggleLoadingState(show){
				if(show)
					angular.element('.loading-box').show();
				else
					angular.element('.loading-box').hide();
			}

            function redrawGraph(viewModel){
				viewModel.api.updateWithOptions(viewModel.options);
            };

			/*
				When the modal starts to show, add the loader
				When the modal is shown, we need to request data to add to the graph
				When the modal is hidden, we need to remove the directive	
			*/
			angular.element('#'+$scope.modalBarChart.modal.id).on('show.bs.modal', function (e) {
		
				toggleLoadingState(true);
				$scope.$parent.graphObj.updateGraphOptions();
				addCallBack();
		
				if(typeof $scope.$parent.graphObj.updateGraphOptions == "function"){ // Set debounce with function from parent and add as a resize event callback
					resizeDebounce = debounce.set($scope.$parent.graphObj.updateGraphOptions,75);
					window.addEventListener('resize', resizeDebounce);
				}
			}).on('shown.bs.modal', function (e) {
				toggleLoadingState(true);
				$scope.graphObj.requestData(redrawGraph);
			}).on('hidden.bs.modal', function (e) {
				if(typeof $scope.$parent.graphObj.updateGraphOptions == "function"){ // Remove the resize event
					window.removeEventListener('resize', resizeDebounce);
				}
				$scope.$parent.graphObj.clearData(); // clear the previous graph data
				$scope.graphObj.api.clearElement();// Clear nvd3 instance
			});
			
		}
	}
})();