'use strict';

/* Controller */
(function () {
	angular.module('app')
		.controller('DuGroupsMultiBarCtrl', GroupMultiBar);

	GroupMultiBar.$inject = ['$scope', 'requestGraphData', 'colorShader','AccountService','dataUsageService', 'commonUtilityService'];

	/**
	 @constructor GroupMultiBar()
	 @param {object} $scope - Angular.js $scope
	 @param {object} requestGraphData - factory service
	 @param {object} colorShader - factory service
	 */
	function GroupMultiBar($scope, requestGraphData, colorShader,AccountService,dataUsageService,commonUtilityService) {
		var groupMultiBar = this;
		var _colorShadeValue = {
			count: 0,
			bump: -15
		};
		var _stacked = false;
		$scope.graphObj = {};



		/**
		 * Start up logic for controller
		 */
		function activate(){
			groupMultiBar.currLegend={};
			$scope.graphObj.requestData = requestData;
			$scope.graphObj.updateGraphOptions = addOrUpdateOptions;
			groupMultiBar.disabledLegends = [];
			$scope.graphObj.getdata = getdata;
		}

		function addOrUpdateOptions(){
			if($scope.graphObj.options)
				delete $scope.graphObj.options;

			_stacked = ($(document).outerWidth() > 991) ? _stacked : true;

			calculateLegendColumns();
			$scope.graphObj.options = {
				title: {
					enable: true,
					text: 'Group Usage by Week',
					className: 'nvd3-gs-title', //Doesn't work with current NVD3.js
					css: {
						textAlign: "center",
						textTransform: "uppercase",
						fontFamily: "Montserrat",
						marginTop: 15,
						marginBottom: 0
					}
				},
				subtitle: {
					enable: true,
					text: function() {
						return 'for billing period : ' + $scope.graphObj.month.getMonthName()+'-'+$scope.graphObj.month.getFullYear();
					},
					className: 'nvd3-gs-subtitle', //Doesn't work with current NVD3.js
					css: {
						textAlign: "center",
						fontSize: 10,
						lineHeight: 1,
						marginBottom: 15
					}
				},
				chart: {
					reduceXTicks:false,
					type: 'multiBarChart',
					stacked: _stacked,
					transitionDuration: 500,
					height: groupMultiBar.calculatedHeight,
					margin : {
						top: 25,
						right: ($(document).outerWidth() > 991 ? 70 : (document.body.clientWidth > 768 ? 60 : 25)),
						bottom: 100,
						left: 70
					},
					color: function(d, i){

						if(d.color){
							return d.color;}
						else{
							if(i%5 == 0){
								if(i == 0){
									_colorShadeValue.count = 0;}
								else{
									_colorShadeValue.count += 1;}
							}

							var shadePer = parseFloat(_colorShadeValue.count*_colorShadeValue.bump/100);

							if(shadePer > .6 || shadePer < -.6){
								_colorShadeValue.count = 0;
								shadePer = parseFloat(_colorShadeValue.count*_colorShadeValue.bump/100);}

							return colorShader.shade(GRAPH_COLORS[i%5],shadePer);}
					},
					clipEdge: true,
					showValues: true,
					showLegend: true,
					legend: {
						align: true,
						rightAlign: ($(document).outerWidth() > 991 ? true : false),
						padding: 20,
						margin: {
							top: 5,
							right: 0,
							bottom: 25,
							left: 0
						},

						//	legendColor: changeUnselectedLegendColor,
						dispatch:{
							stateChange:function(e){

								delete $scope.graphObj.options;
								delete  $scope.graphObj.data;

								var disabledLegends = e.disabled;

								var originalData = JSON.parse( sessionStorage.getItem( "rawData"));


								groupMultiBar.currLegend = {};

								groupMultiBar.preparedData = dataUsageService.prepareWeekWiseGroupUsageData( originalData, groupMultiBar.startDay,groupMultiBar.endDay,disabledLegends);
								$scope.graphObj.data = groupMultiBar.preparedData.data;

								groupMultiBar.yaxisLabel = groupMultiBar.preparedData.usageUnit +' Used';

								$scope.graphObj.updateGraphOptions();

								$scope.graphObj.options.chart.callback = function(data, data1, data2){
								};
								//originalData = JSON.parse( sessionStorage.getItem( "rawData"));
								$scope.graphObj.api.updateWithOptions($scope.graphObj.options);
								$scope.graphObj.api.updateWithData( $scope.graphObj.data );
								groupMultiBar.callback( $scope.graphObj);
							}

						}


					},
					showControls: ($(document).outerWidth() > 991 ? true : false),
					controls: {
						align: true,
						rightAlign: false,
						padding: 20,
						margin: {
							top: 5,
							right: 0,
							bottom: 5,
							left: 0
						}
					},
					dispatch:{
						stateChange:function(e){
							if(e.stacked !=undefined) {
								_stacked = e.stacked;
								if(!$scope.stackedOnce) {
									$scope.graphObj.options.chart.stacked = _stacked;
									setTimeout(function () {
										$scope.$apply();
									}, 1);
									$scope.stackedOnce = true;
								}
							}


						}
					},
					useInteractiveGuideline: true,
					tooltips: true,
					objectEquality:true,
					tooltip: {
						width: 200,
						gravity: "n", //Doesn't work with current NVD3.js
						distance: 25
					},
					tooltipContent: function(key, y, e, graph) {

						//var datey = y.replace(/(Week ending )/,"");
						var tickUnit = commonUtilityService.getDataUsageUnit(graph.point.dataUsage);
						var tickVal = commonUtilityService.getUsageInUnit(graph.point.dataUsage,tickUnit);

						return '<div class="nvd3-gs-title"><p>'+key+'</p><div class="nvd3-gs-usage">'+tickVal+'<span class="nvd3-gs-usage-unit">'+tickUnit+'</div><div class="nvd3-gs-date">'+y+'</div>';
					},
					staggerLabels: false,
					xAxis: {
						axisLabel: 'Weeks',
						showMaxMin: false,
						tickPadding: 10,
						tickFormat: function(d) {
							var sixDaysAgo = 86400*6*1000; // in MS
							var begin = d-sixDaysAgo;
							if(groupMultiBar.preparedData.from > begin)
								begin = groupMultiBar.preparedData.from;
							var end = new Date(d);
							if(groupMultiBar.preparedData.to < end)
								end = groupMultiBar.preparedData.to;
							var bodyWidth = document.body.clientWidth;

							if(bodyWidth < 1200 &&
								bodyWidth > 991){
								var bodyHasMenuPin = $('body').hasClass('menu-pin');

								if(bodyHasMenuPin){
									if($scope.graphObj.api && $scope.graphObj.api.getScope().chart){
										$scope.graphObj.api.getScope().chart.xAxis.rotateLabels(-90);
									}
									$scope.graphObj.options.chart.xAxis.rotateLabels = -90;
									return d3.time.format('%b %e')(new Date(begin))+" - "+d3.time.format('%e')(end);
								}else{
									//EX: Oct 7 - 13
									if($scope.graphObj.api && $scope.graphObj.api.getScope().chart){
										$scope.graphObj.api.getScope().chart.xAxis.rotateLabels(0);
									}
									return d3.time.format('%b %e')(new Date(begin))+" - "+d3.time.format('%e')(end);
								}

							} else if(bodyWidth < 600){
								//EX: Oct 7 - 13
								if($scope.graphObj.api && $scope.graphObj.api.getScope().chart){
									$scope.graphObj.api.getScope().chart.xAxis.rotateLabels(-90);
								}
								$scope.graphObj.options.chart.xAxis.rotateLabels = -90;
								return d3.time.format('%b %e')(new Date(begin))+" - "+d3.time.format('%e')(end);
							} else if(bodyWidth < 769){
								//EX: Oct 7 - 13
								$scope.graphObj.options.chart.xAxis.rotateLabels = -90;
								$scope.graphObj.options.chart.xAxis.tickPadding = -5;
								return d3.time.format('%b %e')(new Date(begin))+" - "+d3.time.format('%e')(end);
							} else {
								//EX: Oct 7 - Oct 13
								if($scope.graphObj.api && $scope.graphObj.api.getScope().chart){
									$scope.graphObj.api.getScope().chart.xAxis.rotateLabels(0);

								}
								return d3.time.format('%b %e')(new Date(begin))+" - "+d3.time.format('%b %e')(end);
							}
						},
						tickValues: function (d) {
							var dateSets = [];
							for (var i = 0; i < d[0].values.length; i++) {
								dateSets.push( d[0].values[i].x )
							}
							return dateSets;
						}
					},
					yAxis: {
						axisLabel: groupMultiBar.yaxisLabel,
						axisLabelDistance: 25,
						tickFormat: function(d){
							//console.log( "d::");
							//console.log( d );
							return d3.format(',g')(parseFloat(d.toFixed(2)));
						}
					},



					noData: "There is no data to display.",
					forceY : [0, 1]

				}

			};

		}



		function requestData(callback) {

			AccountService.getBillingCycle().success(function (data) {
				groupMultiBar.startDay = data.billingStart;
				groupMultiBar.endDay = data.billingEnd;
				groupMultiBar.callback = callback;

				getdata(callback,data.billingStart,data.billingEnd);
			}).error(function (data) {
				commonUtilityService.showErrorNotification(data.errorStr);
			});

		}
		function getRawData(){
			return groupMultiBar.rawData;
		}
		function getdata(callback,startDay,endDay){
			if(groupMultiBar.startDay != startDay || groupMultiBar.endDay != endDay || !$scope.graphObj.data || $scope.graphObj.data.length<1) {
				requestGraphData.getWeeklyDataUsageOfGroups(startDay,endDay).success(function(data, status, headers, config){

					sessionStorage.setItem( "rawData", JSON.stringify( data ) );

					groupMultiBar.startDay = startDay;
					groupMultiBar.endDay = endDay;
					groupMultiBar.preparedData = dataUsageService.prepareWeekWiseGroupUsageData(data,startDay,endDay);
					$scope.graphObj.data = groupMultiBar.preparedData.data;
					groupMultiBar.yaxisLabel = groupMultiBar.preparedData.usageUnit +' Used';
					calculateLegendColumns();
					$scope.graphObj.updateGraphOptions();

					prepareURL(config);
					callback( $scope.graphObj);
				}).error(function(data){
					commonUtilityService.showErrorNotification(data.errorStr)
				});
			}else{

				callback( $scope.graphObj);
			}
		}
		function calculateLegendColumns(){
			if($scope.graphObj.data){

				/*
				 Estimation for how many legend columns nvd3 with produce.
				 Not 100% accurate since we use an averaged legend character
				 length. (legendCharLength)

				 Todo - calculate each legend column width 100% accurately
				 by allowing the graph to render first
				 */

				var legendCharLength = 5.7;
				var spacing = 28;

				var graphWidth = ((document.body.clientWidth > 700 ? 500 : document.body.clientWidth - 30) - 40);
				var legendWidth = 0;
				var seriesPerRow = 0;
				var columnWidths = [];

				while (legendWidth < graphWidth && seriesPerRow < $scope.graphObj.data.length) {
					columnWidths[seriesPerRow] = (String($scope.graphObj.data[seriesPerRow].name).length * legendCharLength) + spacing;
					legendWidth += (String($scope.graphObj.data[seriesPerRow++].name).length * legendCharLength) + spacing;
				}

				if (seriesPerRow === 0) seriesPerRow = 1;

				while (legendWidth >= graphWidth && seriesPerRow > 1 ) {
					columnWidths = [];
					seriesPerRow--;

					for (var i=0; i<$scope.graphObj.data.length; i++) {
						if ((String($scope.graphObj.data[i].name).length * legendCharLength) + spacing > (columnWidths[i%seriesPerRow] || 0) )
							columnWidths[i%seriesPerRow] = (String($scope.graphObj.data[i].name).length * legendCharLength) + spacing;

					}

					legendWidth = columnWidths.reduce(function(prev, cur, index, array) {
						return prev + cur;
					});
				}

				var columns = columnWidths.length;
				var columnHeight = ($scope.graphObj.data.length/columns)*20;

				if(graphWidth > 700){
					groupMultiBar.calculatedHeight = (columnHeight * 3) + 85; // 85 for heading and legend margins
				} else {
					groupMultiBar.calculatedHeight = columnHeight + 300 + 85; // 300 for chart, 85 for heading
				}
			}

		}

		function prepareURL(config){
			$scope.url = config.url+'&fileType=CSV&token='+encodeURIComponent(config.headers.token);
		}

		activate();

	}

})();