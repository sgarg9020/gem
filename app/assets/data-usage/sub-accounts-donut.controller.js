'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('DuSubAccountsDonutCtrl', SubAccountsDonut);

	SubAccountsDonut.$inject = ['$rootScope','$scope', 'requestGraphData', 'colorShader','AccountService','dataUsageService','commonUtilityService','pieGraphToTable','$state'];

	/**
	 * @func SubAccountsDonut()
	 * @param $rootScope
	 * @param $scope
	 * @param requestGraphData
	 * @param colorShader
	 * @param AccountService
	 * @param dataUsageService
	 * @param commonUtilityService
	 * @param pieGraphToTable
	 * @param $state
     * @constructor
     */
    function SubAccountsDonut($rootScope,$scope,requestGraphData, colorShader,AccountService,dataUsageService,commonUtilityService,pieGraphToTable,$state) {
        var subAccountsDonut = this;
        var _colorShadeValue = {
	        count: 0,
	        bump: -15
        };

		$scope.graphObj = {};
		/**
		 * Start up logic for controller
		 */
		function activate(){
			$scope.graphObj.colName = "Account";
			$scope.graphObj.requestData = requestData;
			$scope.graphObj.updateGraphOptions = addOrUpdateOptions;
			$scope.graphObj.toolTipCache = [];
			$scope.graphObj.graphDataModalID = "subAccountDonutDataModal";
			$scope.graphObj.getdata = getdata;
		}

        function addOrUpdateOptions(){
	        var isDashboard = false;
	        var pixels = 1400;
	        var newWidth = 500;
	        var newHeight = 450;
			calculateLegendColumns();
			$(".pricing-wrapper").height(subAccountsDonut.calculatedHeight-(35+80+20+25+50+10));
	        if($scope.graphObj.options)
		        delete $scope.graphObj.options;
		    	        
	        if(window.location.href.indexOf('#/app/home') > -1 &&
	        $(document).outerWidth() > 991 &&
	        $(document).outerWidth() < pixels){
		        isDashboard = true;
		        /*
			    	The panel which the donut chart on the homepage resides in changes in size
			    	from 1160px to 992px. The difference in the size is 500px - 416px = 84px.
			    	
			    	Therefore we need to adjust the width and height of the graph by a factor 
			    	of 84px / 1160px - 991px = 84px/169px or .4970 pixels.
			    	
			    	WIDTH: Every pixel in width from 1160px down to 992px takes off .4970 
			    	of a pixel from the width attribute.
			    	
			    	HEIGHT: Every pixel in width from 1160px down to 992px takes off .4970
			    	* (450/500 or .90) of a pixel from the height attribute. NOTE: 450/500
			    	is the ratio of height/width for the graph.	    
			    */
				
			    pixels -= $(document).outerWidth();
			    newWidth -= (pixels * .4970);
			    newHeight -= (pixels * .4970 * .90);
			    newWidth = newWidth.toFixed(2);
				
				if($('body').hasClass('menu-pin'))
				{
					var outerContainerWidth = $('.zones-panel').outerWidth();
					if(newWidth>outerContainerWidth)
					{
						newWidth = outerContainerWidth;
					}
				}
			    newHeight = newHeight.toFixed(2);
	        }

			$scope.graphObj.options = {
	            title: {
					enable: true,
					text: "Account Data Usage",
					className: "nvd3-gs-title",  //Doesn't work with current NVD3.js
					css: {
		               textAlign: "center",
		               textTransform: "uppercase",
		               fontFamily: "Montserrat",
		               marginTop: 15,
		               marginBottom: 0
	                }
				},
				subtitle: {
					enable: true,
					text: function() {
						if ($scope.graphObj.start && $scope.graphObj.end)
							return 'From ' + $scope.graphObj.start.getDate() + '-' + $scope.graphObj.start.getMonthName() + '-' + $scope.graphObj.start.getFullYear() + ' To ' + $scope.graphObj.end.getDate() + '-' + $scope.graphObj.end.getMonthName() + '-' + $scope.graphObj.end.getFullYear();
						else
							return 'for current billing period'
					},
		            className: "nvd3-gs-subtitle", //Doesn't work with current NVD3.js
		            css: {
			            textAlign: "center",
			            fontSize: 10,
			            lineHeight: 1,
			            marginBottom: 15
		            }
		        },
	            chart: {
	                type: 'pieChart',
					height: subAccountsDonut.calculatedHeight,
	                width: (!isDashboard ? getWidth() : newWidth),
	                donut: true,
	                growOnHover: true, //Doesn't work with current NVD3.js
	                donutLabelsOutside: true,
	                cornerRadius: 5,
	                color: function(d, i){
		                if(d.color){
		                	return d.color;}
		                else{
			                if(i%8 == 0){
			                	if(i == 0){
			                		_colorShadeValue.count = 0;}
			                	else{
			                		_colorShadeValue.count += 3;}
			                }
			                
			                var shadePer = parseFloat(_colorShadeValue.count*_colorShadeValue.bump/100);

			                if(shadePer > .6 || shadePer < -.6){
			                	_colorShadeValue.count = 0;
			                	shadePer = parseFloat(_colorShadeValue.count*_colorShadeValue.bump/100);}
			                
			                return colorShader.shade(GRAPH_COLORS[i%8],shadePer);}
	                },
	                tooltips: true,
	                tooltip: {
						width: 200,
		                gravity: "n", //Doesn't work with current NVD3.js
		                distance: 25
	                },
					tooltipContent: function(key, y, obj) {
		                var enabledTotal = $scope.graphObj.data
					        .filter(function(item){return !item.disabled;})
					        .reduce(function(a, b){return a + b.dataUsage}, 0);
					
						var percent = Math.round((obj.value/enabledTotal) * 100);
						var tickUnit = commonUtilityService.getDataUsageUnit(obj.point.dataUsage);
						var tickVal = commonUtilityService.getUsageInUnit(obj.point.dataUsage,tickUnit);

			            return '<div class="nvd3-gs-title"><p>'+key+'</p><div class="nvd3-gs-usage">'+tickVal+'<span class="nvd3-gs-usage-unit">'+tickUnit+'</div><div class="nvd3-gs-date">'+percent+'%</div>';
			        },
			        labelType: "percent",
	                x: function(d){return d.name;},
	                y: function(d){return d3.format('g')(d.dataUsage);},
	                showLabels: true,
					labelThreshold: 0.01,
	                transitionDuration: 500,
	                legend: {
						align: true,
						rightAlign: false,
	                    margin: {
	                        top: 5,
	                        right: 0,
	                        bottom: 25,
	                        left: 0
	                    }
	                },
	                noData: "There is no data to display."
	            }
	        };
	        
	    }

		function getWidth(){
			var clientWidth = document.body.clientWidth;
			var bodyHasMenuPin = $('body').hasClass('menu-pin');
			var width = clientWidth-30;// -30 because of panel padding
			if(clientWidth > 1200 || (clientWidth > 500 && !bodyHasMenuPin)){
				width = 500;
			}else if(clientWidth > 500 && bodyHasMenuPin){
				width = 400;
			}

			return width;
		}

		function requestData(callback) {
			AccountService.getBillingCycle().success(function (data) {
				getdata(callback,data.billingStart,data.billingEnd);
			}).error(function (data) {
				commonUtilityService.showErrorNotification(data.errorStr);
			});
		}
		function getdata(callback,startDay,endDay){
			if(subAccountsDonut.startDay != startDay || subAccountsDonut.endDay != endDay || !$scope.graphObj.data || $scope.graphObj.data.length<1) {
				requestGraphData.getMonthlyDataUsageOfSubAccounts(startDay,endDay).success(function(data, status, headers, config){
					subAccountsDonut.startDay = startDay;
					subAccountsDonut.endDay = endDay;
					data.list = sortByTotalDataUsedInBytes(data.list,-1);
					/*if($state.current.name == 'app.home'){
						var response ={
							data : data,
							startDate : startDay,
							endDate : endDay
						};
						$rootScope.$emit('onGetMonthlyDataUsagesOfZones',response);
					}*/
					var dataList = dataUsageService.prepareSubAccountUsageData(data);
					$scope.graphObj.data = dataList;
					$scope.graphObj.completeData = dataList;
					// Set toolTip Cache data for the graph details table in modal
					if($scope.graphObj.completeData){
						$scope.graphObj.toolTipCache = pieGraphToTable.make($scope.graphObj.completeData, "subAccountDonutDataModal");
					}

					$scope.graphObj.data = filterData($scope.graphObj.data,$scope.graphObj.toolTipCache);

					sumData();
					calculateLegendColumns();
					$scope.graphObj.updateGraphOptions();
					$scope.graphObj.api.updateWithOptions($scope.graphObj.options);
					prepareURL(config);
					if(callback)
						callback($scope.graphObj);
				}).error(function(data){
					commonUtilityService.showErrorNotification(data.errorStr)
				});
			}else{

				callback( $scope.graphObj);
			}
		}

		/**
		 * It filters  15 zones with highest dat-usage, remaining zones will make new group as 'Others'
		 * and within those 15 , again zones with less then 2% data-usage will become part of 'Others'.
		 * @param tableData
		 * @param toolTipCache
		 * @returns {Array}
		 */
		function filterData(tableData,toolTipCache){
			var filteredData = [];
			
			var otherDataUsage = 0;

			if (tableData.length > 15) {
				for (var i = 0; i < tableData.length; i++) {
					if (i >= 15) {
						otherDataUsage = otherDataUsage + tableData[i].dataUsage;
					} else if (toolTipCache[i].percent < 2) {
						otherDataUsage = otherDataUsage + tableData[i].dataUsage;
					} else {
						filteredData.push(tableData[i]);
					}
				}
			} else {
				for (var j = 0; j < tableData.length; j++) {
					if (toolTipCache[j].percent < 2) {
						otherDataUsage = otherDataUsage + tableData[j].dataUsage;
					} else {
						filteredData.push(tableData[j]);
					}
				}
			}
			if (otherDataUsage > 0) {
				var obj = {};

				obj.name = "All other accounts";
				obj.dataUsage = otherDataUsage;

				filteredData.push(obj);
			}
			return filteredData;
		}

		//sortOder -1 is for DESC and 1 is for ASC
		//This function needs to be removed once sortBy dataUsage is available in backend api
		function sortByTotalDataUsedInBytes(values,sortOrder){
			values.sort(function(a,b){
				var x = a.totalDataUsedInBytes;
				var y = b.totalDataUsedInBytes;
				if (x === undefined) x = 0;
				if (y === undefined) y = 0;
				return ((x > y) ? sortOrder : ((x < y) ? -sortOrder : 0));
			});

			return values;
		}
		
		function sumData(){
			subAccountsDonut.dataSum = 0;
			for(var i=0;i<$scope.graphObj.data.length;i++){
				subAccountsDonut.dataSum += $scope.graphObj.data[i].dataUsage;
			}
            //todo: check this wasn't working - srs
			// $scope.graphObj.api.updateWithOptions($scope.graphObj.options);
		}

		function calculateLegendColumns(){
			if($scope.graphObj.data){

				/*
				 Estimation for how many legend columns nvd3 with produce.
				 Not 100% accurate since we use an averaged legend character
				 length. (legendCharLength)

				 Todo - calculate each legend column width 100% accurately
				 by allowing the graph to render first
				 */

				var legendCharLength = 5.7;
				var spacing = 28;

				var graphWidth = ((document.body.clientWidth > 700 ? 500 : document.body.clientWidth - 30) - 40);
				var legendWidth = 0;
				var seriesPerRow = 0;
				var columnWidths = [];

				while (legendWidth < graphWidth && seriesPerRow < $scope.graphObj.data.length) {
					columnWidths[seriesPerRow] = (String($scope.graphObj.data[seriesPerRow].name).length * legendCharLength) + spacing;
					legendWidth += (String($scope.graphObj.data[seriesPerRow++].name).length * legendCharLength) + spacing;
				}

				if (seriesPerRow === 0) seriesPerRow = 1;

				while (legendWidth >= graphWidth && seriesPerRow > 1 ) {
					columnWidths = [];
					seriesPerRow--;

					for (var i=0; i<$scope.graphObj.data.length; i++) {
						if ((String($scope.graphObj.data[i].name).length * legendCharLength) + spacing > (columnWidths[i%seriesPerRow] || 0) )
							columnWidths[i%seriesPerRow] = (String($scope.graphObj.data[i].name).length * legendCharLength) + spacing;
					}

					legendWidth = columnWidths.reduce(function(prev, cur) {
						return prev + cur;
					});
				}

				var columns = columnWidths.length;
				var columnHeight = ($scope.graphObj.data.length/columns)*20;

				if(graphWidth > 700){
					subAccountsDonut.calculatedHeight = (columnHeight * 3) + 85; // 85 for heading and legend margins
				} else {
					subAccountsDonut.calculatedHeight = columnHeight + 300 + 85; // 300 for chart, 85 for heading
				}
			}

		}
		function prepareURL(config){
			$scope.url = config.url+'&fileType=CSV&token='+encodeURIComponent(config.headers.token);
		}

		activate();
		
    }
})();