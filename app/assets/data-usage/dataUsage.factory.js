'use strict';

(function(){
    angular
        .module('app')
        .factory('dataUsageService',['commonUtilityService',DataUsageService]);



    function DataUsageService(commonUtilityService){

        var colors = ["#023fa5", "#7d87b9", "#bec1d4", "#d6bcc0", "#bb7784", "#8e063b", "#4a6fe3", "#8595e1", "#b5bbe3", "#e6afb9", "#e07b91", "#d33f6a", "#11c638", "#8dd593", "#c6dec7", "#ead3c6", "#f0b98d", "#ef9708", "#0fcfc0", "#9cded6", "#d5eae7", "#f3e1eb", "#f6c4e1", "#f79cd4"];
        var fixedColor = "#e4e400";
        Date.prototype.getWeekNumber = function(){
            var onejan = new Date(this.getFullYear(), 0, 1);
            return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
        };
        var factory = {

            prepareDayWiseUsageData:prepareDayWiseUsageData,
            prepareZoneUsageData:prepareZoneUsageData,
            prepareGroupUsageData:prepareGroupUsageData,
            prepareWeekWiseGroupUsageData:prepareWeekWiseGroupUsageData,
            prepareZonePricing :prepareZonePricing,
            prepareDataUsageHistorical: prepareDataUsageHistorical,
            prepareEnterpriseUsageData: prepareEnterpriseUsageData,
            prepareEstimateZoneUsageData:prepareEstimateZoneUsageData,
            prepareZoneUsageForEstimations:prepareZoneUsageForEstimations,
            prepareValues:prepareValues,
            prepareSubAccountUsageData: prepareSubAccountUsageData

        };

        return factory;

        function prepareDayWiseUsageData(data, start, end ){
            if(data) {
                var preparedData = {};
                preparedData.bar = true;
                var preparedValues = [];
                var values = data.list;


                if (values && values.length > 0) {
                    var sortedValues = sortByKey(values,'dataUsedInBytes');
                    var maxUsage = sortedValues[sortedValues.length-1].dataUsedInBytes;
                    preparedData.usageUnit = commonUtilityService.getDataUsageUnit(maxUsage);
                    var len = values.length;

                    var adate = [];

                    for (var w = 0; w < len; w++) {
                        adate.push(values[w].fromDate);
                    }
                    var start = getDateObj(start);
                    var sm = parseInt(start.getMonth()) + 1;
                    var sd = start.getDate();
                    if (sd <= 9) {
                        sd = "0" + sd;
                    }
                    if(sm<=9)
                    {
                        sm="0" + sm;
                    }

                    var startD = start.getFullYear() + "-" + sm + "-" + sd + " 00:00:00";

                    var end = getDateObj(end);
                    var em = parseInt(end.getMonth()) + 1;
                    var ed = end.getDate();
                    if (ed <= 9) {
                        ed = "0" + ed;
                    }
                    if(em<=9)
                    {
                        em = "0" + em;
                    }

                    var endD = end.getFullYear() + "-" + em + "-" + ed + " 00:00:00";
                    startD = getDateObj(startD);
                    endD = getDateObj(endD);
                    var between = betweenDate(startD, endD);
                    var bn = [];


                    for (var l = 0; l < between.length; l++) {
                        var m = parseInt(between[l].getMonth()) + 1;
                        var d = between[l].getDate();
                        if (d <= 9) {
                            d = "0" + d;
                        }
                        if(m<=9){
                            m = "0" + m;
                        }
                        var newdformat = between[l].getFullYear() + "-" + m + "-" + d + " 00:00:00";
                        bn.push(newdformat);
                    }

                    var diff = bn.diff(adate);

                    for (var x = 0; x < diff.length; x++) {
                        values.push({"fromDate": diff[x], "dataUsedInBytes": 0});
                    }

                     values.sort( function( a,b ) {
                         var sortOrder = 1;
                         var x = a.fromDate; var y = b.fromDate;
                         if(x===undefined) x = 0;
                         if(y===undefined) y = 0;
                         return ((x > y) ? sortOrder : ((x < y) ? -sortOrder: 0));

                     });

                    for (var i = 0; i < values.length; i++) {
                        preparedValues[i] = {
                            x: getDateObj(values[i].fromDate).getTime(),
                            y: commonUtilityService.getUsageInUnit(values[i].dataUsedInBytes,preparedData.usageUnit),
                            dataUsage:parseFloat(values[i].dataUsedInBytes.toFixed(2))
                        };
                    }
                }
                preparedData.values = preparedValues;
                return [preparedData];
            }
        }

        function prepareZoneUsageForEstimations(data){

            if (data) {
                var preparedValues = [];
                var values = data.list;
                if (values && values.length > 0) {
                    for (var i = 0; i < values.length; i++) {
                            preparedValues[i] = {
                                name: values[i].zoneNickName || values[i].zoneName,
                                dataUsage: parseFloat(values[i].totalDataUsedInBytes.toFixed(6)),
                                zoneId:values[i].zoneId,
                                spend:values[i].totalDataUsageCost,
                                minimumCommitment:values[i].minimumCommitment

                            };

                    }
                }
                return preparedValues;
            }
        }


        function prepareZoneUsageData(data){
            if (data) {
                var preparedValues = [];
                var values = data.list;
                if (values && values.length > 0) {
                    for (var i = 0; i < values.length; i++) {
                        if (values[i].list && values[i].list.length && values[i].list[0].dataUsedInBytes >= 0) {
                            //Hack to show zero usage zones in pie chart.
                            if (values[i].totalDataUsedInBytes == 0) {
                                values[i].totalDataUsedInBytes = 0.000001;
                            }
                            preparedValues[i] = {
                                name: values[i].zoneNickName || values[i].zoneName,
                                dataUsage: parseFloat(values[i].totalDataUsedInBytes.toFixed(6)),
                                zoneId:values[i].zoneId,
                                spend:values[i].totalDataUsageCost,
                                minimumCommitment:values[i].minimumCommitment

                            };
                        }
                    }
                }
                return preparedValues;
            }
        }

        function prepareSubAccountUsageData(data){
            if (data) {
                var preparedValues = [];
                var values = data.list;
                if (values && values.length > 0) {
                    for (var i = 0; i < values.length; i++) {
                        if (values[i].list && values[i].list[0].dataUsedInBytes >= 0) {
                            //Hack to show zero usage zones in pie chart.
                            if (values[i].totalDataUsedInBytes == 0) {
                                values[i].totalDataUsedInBytes = 0.000001;
                            }
                            preparedValues[i] = {
                                name: values[i].accountName,
                                dataUsage: parseFloat(values[i].totalDataUsedInBytes.toFixed(6)),
                                accountId:values[i].accountId

                            };
                        }
                    }
                }
                return preparedValues;
            }
        }

        function prepareGroupUsageData(data){
            if (data) {
                var preparedValues = [];
                var values = data.list;
                if (values && values.length > 0) {
                    for (var i = 0; i < values.length; i++) {
                        if (values[i].totalDataUsedInBytes >= 0) {
                            //Hack to show zero usage groups in pie chart.
                            if (values[i].totalDataUsedInBytes == 0) {
                                values[i].totalDataUsedInBytes = 0.000001;
                            }

                            var accountName = values[i].accountType === "ENTERPRISE" ? 'No Association' : values[i].accountName;
                            preparedValues[i] = {
                                name: accountName,
                                dataUsage: parseFloat(values[i].totalDataUsedInBytes.toFixed(6))
                            };
                        }
                    }
                }
                return preparedValues;
            }
        }
        function getDateOfISOWeek(w, y) {

            var ISOweekEnd = new Date(y, 0, 1 + w  * 7);
            ISOweekEnd.setDate(ISOweekEnd.getDate() - (ISOweekEnd.getDay()+1));
            return ISOweekEnd;

        }
        function sortByKey(array, key) {
            return array.sort(function(a, b) {
                var x = a[key]; var y = b[key];
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
        }
        function getWeeks(start,end){
            var weeks = [];
            weeks.push(start.getWeekNumber());
            start.setTime(start.getTime() + (1000 * 60 * 60 * 24 * (7-start.getDay())));
            while(true){
                weeks.push(start.getWeekNumber());
                start.setTime(start.getTime() + (1000 * 60 * 60 * 24 * 7));
                if(start <= end){
                    continue;
                }
                    break;

            }
            return weeks
        }

        function prepareWeekWiseGroupUsageData(response,from,to, filerList ){
            if(response) {
                var origData;
                var data = response.list;
                var preparedData = [];
                var usageUnit = 'Bytes';

                if (data && data.length > 0) {
                    var weeks = getWeeks(getDateObj(from), getDateObj(to));
                    var maxUsage = 0;
                    for (var l = 0; l < data.length; l++) {
                        var values = ( data[l] !== undefined ? data[l].list : undefined );
                        if (values !== undefined && (!filerList || !filerList[l])) {
                            for (var m = 0; m < values.length; m++) {
                                if (values[m] !== undefined && values[m].dataUsedInBytes > maxUsage)
                                    maxUsage = values[m].dataUsedInBytes;
                            }
                        }
                    }

                    // console.log( "max Usage::" + maxUsage );
                    usageUnit = commonUtilityService.getDataUsageUnit(maxUsage);

                    origData = response.list;
                    for (var j = 0; j < origData.length; j++) {
                        var accountName = origData[j].accountType === "ENTERPRISE" ? 'No Association' : origData[j].accountName;
                        preparedData[j] = {
                            key: accountName,
                            bar: true,
                            disabled: (filerList) ? filerList[j] : false
                        };
                        var preparedValues = [];

                        var values = origData[j].list;
                        var k = 0;
                        for (var i = 0; i < weeks.length; i++) {
                            for (k = 0; k < values.length; k++) {
                                if (values[k] !== undefined && weeks[i] === values[k].week) {

                                    preparedValues[i] = {
                                        x: new Date(getDateOfISOWeek(values[k].week, getDateObj(from).getFullYear())).getTime(),
                                        y: commonUtilityService.getUsageInUnit(values[k].dataUsedInBytes, usageUnit),
                                        dataUsage: parseFloat(values[k].dataUsedInBytes.toFixed(2))
                                    };

                                    break;
                                } else {
                                    preparedValues[i] = {
                                        x: new Date(getDateOfISOWeek(weeks[i], getDateObj(from).getFullYear())).getTime(),
                                        y: parseFloat(0),
                                        dataUsage: parseFloat(0),
                                    };
                                }
                            }
                        }

                        preparedData[j].values = preparedValues;
                    }
                }

                var rep = {
                    from: getDateObj(from),
                    usageUnit: usageUnit,
                    to: getDateObj(to)
                };
                rep.data = preparedData;

                return rep;
            }
        }


        function isDate(dateArg) {
            var t = (dateArg instanceof Date) ? dateArg : (new Date(dateArg));
            return !isNaN(t.valueOf());
        }

        function isValidRange(minDate, maxDate) {
            return (new Date(minDate) <= new Date(maxDate));
        }

        function betweenDate(startDt, endDt) {
            var error = ((isDate(endDt)) && (isDate(startDt)) && isValidRange(startDt, endDt)) ? false : true;
            var between = [];
            if (error) return false;
            else {
                var currentDate = new Date(startDt),
                    end = new Date(endDt);
                while (currentDate <= end) {
                    between.push(new Date(currentDate));
                    currentDate.setDate(currentDate.getDate() + 1);
                }
            }
            return between;
        }
        function prepareDataUsageHistorical(data, dateRange, groupPeriod,enabledLegendItems) {
            var preparedData = [];
            var usageUnit = 'Bytes';
            if(!enabledLegendItems){
                enabledLegendItems = 20;
            }
            if (data) {
                var values = data.list;

                if (groupPeriod == 'DAY') {
                    groupPeriod = 'Days';
                } else {
                    groupPeriod = 'Months';
                }

                if (values && values.length > 0) {

                    var maxUsage = 0;
                    for (var l = 0; l < values.length; l++) {
                        var val = values[l];
                        if(val.totalDataUsedInBytes>maxUsage)
                            maxUsage = val.totalDataUsedInBytes;
                    }

                    // console.log( "max Usage::" + maxUsage );
                    usageUnit = commonUtilityService.getDataUsageUnit(maxUsage);

                    values.sort(function(x,y){
                            var diff = y.totalDataUsedInBytes - x.totalDataUsedInBytes;
                            var keyf = (x.zoneNickName || x.zoneName) || ((x.accountType == 'GROUP' || x.accountType == SUB_ACCOUNT_TYPE || x.accountType == RESELLER_ACCOUNT_TYPE) ? x.accountName : 'No Association');
                            var keyn = (y.zoneNickName || y.zoneName) || ((y.accountType == 'GROUP' || y.accountType == SUB_ACCOUNT_TYPE || y.accountType == RESELLER_ACCOUNT_TYPE) ? y.accountName : 'No Association');
                            if(diff==0)
                                return keyf.localeCompare(keyn);
                            else
                                return diff;

                    });


                    var sortedValues = values;

                    for (var i = 0; i < sortedValues.length; i++) {

                        preparedData[i] = {
                            key: (sortedValues[i].zoneNickName || sortedValues[i].zoneName) || ((sortedValues[i].accountType == 'GROUP' || sortedValues[i].accountType == SUB_ACCOUNT_TYPE || sortedValues[i].accountType == RESELLER_ACCOUNT_TYPE) ? sortedValues[i].accountName : 'No Association'),
                            bar: true,
                            totalDataUsageCost:sortedValues[i].totalDataUsageCost,
                            totalDataUsedInBytes:sortedValues[i].totalDataUsedInBytes,
                            totalDataUsage:commonUtilityService.getUsageInUnit(sortedValues[i].totalDataUsedInBytes, usageUnit),
                            usageUnit:usageUnit
                        };
                        if ( i < enabledLegendItems) {
                            preparedData[i].color = colors[i];
                            preparedData[i].disabled = false;
                        } else {
                            preparedData[i].color = fixedColor;
                            preparedData[i].disabled = true;
                        }

                        var dataUsageValues = sortedValues[i];
                        var startDate = moment(dateRange.startDate).clone().startOf('day');
                        var endDate = moment(dateRange.endDate).clone().startOf('day');

                        preparedData[i].values = prepareValues(dataUsageValues, groupPeriod, startDate, endDate, usageUnit);
                    }

                }
            }
            return {data: preparedData, groupPeriod: groupPeriod, dateRange: dateRange, usageUnit: usageUnit,totalDataUsedInBytes:data.totalDataUsedInBytes};
        }

        function prepareEnterpriseUsageData(data, dateRange, groupPeriod) {
            var preparedData = [];
            var usageUnit = 'Bytes';
            preparedData[0] = {
                key: 'Enterprise',
                bar: true
            };
            if (data) {
                var values = data.list;

                if (groupPeriod == 'DAY') {
                    groupPeriod = 'Days';
                } else {
                    groupPeriod = 'Months';
                }

                //if (values && values.length > 0) {
                var maxUsage = 0;
                if (values && values.length > 0) {
                    for (var m = 0; m < values.length; m++) {
                        if (values[m] !== undefined && values[m].dataUsedInBytes > maxUsage)
                            maxUsage = values[m].dataUsedInBytes;
                    }
                    usageUnit = commonUtilityService.getDataUsageUnit(maxUsage);
                }

                var startDate = moment(dateRange.startDate).clone().startOf('day');
                var endDate = moment(dateRange.endDate).clone().startOf('day');
                preparedData[0].values = prepareValues(data, groupPeriod, startDate, endDate, usageUnit);
                //}
            }
            return {data: preparedData, groupPeriod: groupPeriod, dateRange: dateRange, usageUnit: usageUnit,totalDataUsedInBytes:data.totalDataUsedInBytes};
        }

        function prepareValues(dataUsageValues, groupPeriod, startDate, endDate, usageUnit) {
            var preparedValues = [];
            var j = 0, k = 0;
            if(dataUsageValues.list){
                dataUsageValues.list.sort(function (left, right) {
                    return moment.utc(left.fromDate).diff(moment.utc(right.fromDate))
                });
            }
            var date = startDate;
            do {
                if (dataUsageValues.list && dataUsageValues.list.length > k && ((groupPeriod == 'Months' && moment(dataUsageValues.list[k].fromDate).isSame(date, 'month')) || (groupPeriod == 'Days' && moment(dataUsageValues.list[k].fromDate).isSame(date, 'day')))) {

                    preparedValues[j] = {
                        from: new Date(dataUsageValues.list[k].fromDate),
                        to: new Date(dataUsageValues.list[k].toDate),
                        x: new Date(date).getTime(),
                        y: commonUtilityService.getUsageInUnit(dataUsageValues.list[k].dataUsedInBytes, usageUnit),
                        cost: dataUsageValues.list[k].dataUsageCost,
                        dataUsedInBytes: dataUsageValues.list[k].dataUsedInBytes,
                        dataUsage: dataUsageValues.list[k].dataUsedInBytes
                    };
                    k++;
                }
                else {
                    preparedValues[j] = {
                        from: new Date(date),
                        to: new Date(date),
                        x: new Date(date).getTime(),
                        y: 0,
                        cost: 0,
                        dataUsedInBytes: 0,
                        dataUsage: 0
                    };
                }
                j++;
                date.add(1, groupPeriod)
            } while (!date.isAfter(endDate, groupPeriod));
            return preparedValues;
        }
    }

    Array.prototype.diff = function(a) {
        return this.filter(function(i) {
             var ind = a.indexOf(i);
             if( ind < 0)
                 return true;

            return false;
        });
    };

    function prepareZonePricing(zoneList,plansList){
        if (zoneList && plansList) {
            var plans = plansList.list;
            if (zoneList && zoneList.length > 0) {
                for (var i = 0; i < zoneList.length; i++) {
                    var zoneId = zoneList[i].zoneId;
                    if(plans && plans.length>0){
                        for(var j=0; j<plans.length;j++){
                            if(plans[j].zoneId == zoneId){
                                zoneList[i].pricePerMB = plans[j].pricePerMB;
                                break;
                            }
                        }
                    }

                }
            }
            return zoneList;
        }
    }

    function prepareEstimateZoneUsageData(zoneList,estimatedDataList){
        if(zoneList && estimatedDataList){
            for(var i=0; i<zoneList.length; i++){
                var zoneId = zoneList[i].zoneId;
                for(var j=0; j<estimatedDataList.length; j++){
                    var zoneEstimate = estimatedDataList[j];
                    if(zoneEstimate.zoneId == zoneId){
                        zoneList[i].estimatedCost = zoneEstimate.totalDataUsageCost;
                        zoneList[i].estimatedDataUsage = zoneEstimate.totalDataUsedInBytes;

                    }
                }
            }
        }

        return zoneList;

    }


})();
