/* Controllers */
(function(){
    "use strict";

    angular.module('app')
        .controller('SearchCtrl', Search);

    Search.$inject=['$scope', '$sce','$rootScope', '$compile', 'dataTableConfigService','AuthenticationService','commonUtilityService','loadingState','$location', '$state'];

    /**
     @constructor Search()
     @param {object} $scope - Angular.js $scope provider
     @param {object} $sce - Angular.js $sce provider (Strict Contextual Escaping)
     @param {function} $compile - Angular.js $sanitize provider
     @param {object} dataTableConfigService - Factory Object
     */
    function Search($scope, $sce, $rootScope,$compile, dataTableConfigService, authenticationService, commonUtilityService, loadingState, $location, $state){

        var searchVm = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            clearDataTableStatus(['DataTables_searchDataTable_']);
            $scope.emptyTableMessage = "No Enterprises available.";

            // Controller As
            searchVm.trustAsHtml = function (value){return $sce.trustAsHtml(value);};
            searchVm.initialiseDataTableObj = initialiseDataTableObj;
            searchVm.searchIt = searchIt;

            commonUtilityService.getCountries().success(function(data){
                searchVm.countries = data.countryList;
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });
            initialiseDataTableObj();
            loadingState.hide();

        }

        $scope.navigateToSimDetails =  function(accountSubscriptionType, accountId, simId){

            commonUtilityService.setSubscriptionTypeInSessionStorage(accountSubscriptionType);
            $state.go('support.sim.simDetail',{accountId: accountId, iccId: simId});
        }

        $scope.navigateToSimCards = function(accountId){
            $location.path("/support/"+accountId+"/sim-cards/physical-sims");
            //$location.search("simId",simId);
        };
        $scope.navigateToGem = function(accountId,enterpriseCurrency){

            $rootScope.isNavigatedThroughAdmin = true;
            sessionStorage.setItem('isNavigatedThroughAdmin', true);

            sessionStorage.setItem("accountId", accountId);
            sessionStorage.setItem( "gs-enterpriseCurrency",enterpriseCurrency);
            sessionStorage.setItem("exit-to-state","support.search");
            var queryParam = {};
            queryParam.details = 'alertVersionSupported|pricingModelVersionSupported';

            /* get enterprise details */
            // need to pass query param once the defect is fixed from backend
            authenticationService.getEnterpriseAccountDetails(accountId/*,queryParam*/).success(function (response) {
                $scope.setHierarchicalData(response.accountType == RESELLER_ACCOUNT_TYPE, $scope.isResellerRoot || response.accountType == RESELLER_ACCOUNT_TYPE, response.accountType == SUB_ACCOUNT_TYPE);
                if(response.accountType == RESELLER_ACCOUNT_TYPE){
                    sessionStorage.setItem('rootResellerAccountId', response.accountId);
                }
                commonUtilityService.setSubscriptionTypeInSessionStorage(response.accountSubscriptionType);
                authenticationService.setAlertsFeatureEnabled(response.alertVersionSupported);
                authenticationService.setPricingModelVersion(response.pricingModelVersionSupported);
                $location.path('/app/home');
            }).error(function (data) {
                console.log("error while fetching enterprise details");
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        };

        activate();

        function initialiseDataTableObj(){
            $scope.showDataTable = false;

            // DataTable
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = "searchDataTable";
            $scope.dataTableObj.controlID = "datatable-controls-search";

            $scope.dataTableObj.searchParams = [
                {value: "CID", DbValue: "cId"},
                {value: "ICCID", DbValue: "iccId"},
                {value: "EID", DbValue: "eid"},
                {value: "IMSI", DbValue: "imsi"},
                {value: "MSISDN", DbValue: "msisdn"}
            ];
            $scope.dataTableObj.tableHeaders = [
                {value: "Company", DbValue: "accountName"},
                {value: "Country", DbValue: "country"},
                {value: "Gem Product", DbValue: "accountSubscriptionType"},
                {value: "Currency", DbValue: "currency"},
                {value: "CID", DbValue: "cId"},
                {value: "View As", DbValue: ""}

            ];
            $scope.dataTableObj.searchOn = $scope.dataTableObj.searchParams[0];

            $scope.dataTableObj.dataTableOptions = {
                iDeferLoading: 10,         //prevent initial loading
                order: [[ 0, "asc" ]],
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'AID_'+data.accountId);
                    $compile(row)($scope);
                },
                columns: [
                    { orderable: true},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false}
                ],
                columnDefs: [{
                    targets: 0,
                    render: function (data, type, row) {
                        var accountName = row.accountName ?row.accountName : '';
                        return '<a ui-sref="support.enterprise.info({ accountId: '+row.accountId+'})" class="cursor">'+accountName+'</a>';
                    }
                }, {
                    targets: 1,
                    render: function (data, type, row) {
                        var country = (row.company && row.company.address && row.company.address.country) ? commonUtilityService.getCountryObject(searchVm.countries,row.company.address.country).name : 'Not Available';
                        return '<span>'+country+'</span>';
                    }
                }, {
                    targets: 2,
                    render:function (data, type, row){
                        var subscriptionType = (row.accountSubscriptionType) ? row.accountSubscriptionType.replace( "ENTERPRISE_", "GEM ") : 'Not Available';
                        subscriptionType = subscriptionType.replace("_"," ");
                        return '<span>'+subscriptionType+'</span>';
                    },
                }, {
                    targets: 3,
                    render:function (data, type, row){
                        var currency = (row.company && row.company.currency) ? row.company.currency : 'Not available';
                        return '<span>'+currency+'</span>';
                    }
                }, {
                    targets: 4,
                    render:function( data, type, row) {
                        var sims='';
                        if(row.simList){
                            for(var i=0;i<row.simList.length && i<3;i++){
                                var sim = row.simList[i];
                                var simId = sim.simId;
                                var cId = sim.cId;
                                sims+=' <a href="javascript:void(0)" ng-click=\'navigateToSimDetails(\"'+row.accountSubscriptionType+'\",'+row.accountId+',\"'+ simId + '\")\'><span>'+cId+'</span></a>'+"</br>";
                            }
                            if(row.simList.length>3){
                                sims+=' <a href="javascript:void(0)" ng-click=\'navigateToSimCards('+row.accountId+');\'><span>more</span></a>';
                            }

                        }
                        return sims;
                    }
                },{
                    targets: 5,
                    render: function (data, type, row) {
                        var accountName = row.accountName ?row.accountName : '';
                        return '<a href="javascript:void(0)" ng-click=\'navigateToGem('+row.accountId+','+row.company.currency+');\' class="cursor">View As</a>';
                    }
                }
                ],
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords:$scope.emptyTableMessage
                },
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {
                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    if($scope.dataTableObj.searchValue != undefined && $scope.dataTableObj.searchValue != ''){
                        queryParam['searchBy'] = $scope.dataTableObj.searchOn.DbValue;
                        queryParam['search'] = $scope.dataTableObj.searchValue.trim();
                    }
                    queryParam.ignoreGroups = true;

                    authenticationService.getSubAccounts(sessionStorage.getItem("rootAccountId"),queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
                        $scope.companiesList = datTabData.aaData;
                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }

                    }).error(function (data) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"enterprises");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });

                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.companiesList )
                        dataTableConfigService.disableTableHeaderOps( $scope.companiesList.length, oSettings );
                    var table$ = $( "#" + $scope.dataTableObj.tableID );
                    var enableViewAsColumn = authenticationService.isOperationAllowed(['GEM_ACCESS_FROM_GAP']);
                    table$.DataTable().column(5).visible(enableViewAsColumn);

                    var simReadAllowed = authenticationService.isOperationAllowed(['SIM_READ']);
                    table$.DataTable().column(4).visible(simReadAllowed);
                }
            };
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

            if(sessionStorage.getItem('sOn')){
                var result = $scope.dataTableObj.searchParams.filter(function( obj ) {
                    return obj.value == sessionStorage.getItem('sOn');
                });
                //GS-CR2 ,this issue can be resolved by defining callback function like 'dataTableAdded on $scope.dataTableObj and call this function on first time creation of table in data-table.js
                $scope.dataTableObj.searchValue = sessionStorage.getItem('sVal');
                $scope.dataTableObj.searchOn = result[0];
                searchIt(false);
            }
        }

        /*
         *@function searchIt
         * @param searchFrom
         * @description checks if the form is valid and loads the data-table with the search results
         */
        function searchIt(searchFrom) {
            if (!(searchFrom && !$scope.isFormValid(searchFrom)) && $scope.dataTableObj.searchValue) {
                $scope.showDataTable = true;
                sessionStorage.setItem('sOn', $scope.dataTableObj.searchOn.value);
                sessionStorage.setItem('sVal', $scope.dataTableObj.searchValue);
                //$location.search({sOn: $scope.dataTableObj.searchOn.value, sVal: $scope.dataTableObj.searchValue});
                //$timeout(function(){
                if ($scope.dataTableObj && $scope.dataTableObj.refreshDataTable) {
                    $scope.dataTableObj.refreshDataTable(searchFrom?true:false);
                }
                else {
                    $scope.dataTableObj.onDrawComplete = function () {
                        searchIt();
                    };
                }
                //});
                //$("#searchTableBox").show();
            }
            else {
                searchVm.searchErrorStr = 'Search value is required';
            }
        };

    }
})();
