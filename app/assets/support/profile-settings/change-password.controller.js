'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('ChangePasswordCtrl', ChangePassword);

    ChangePassword.$inject = ['$scope', '$location', 'commonUtilityService', 'loadingState', 'AccountService', 'AuthenticationService'];

    /**
     @constructor ChangePassword()
     */
    function ChangePassword($scope, $location, commonUtilityService, loadingState, AccountService, AuthenticationService){

        var changePassword = this;

        /**
         * @func activate()
         * @desc controller start up logic
         */
        function activate(){

            $scope.generalObj = {};
            
            changePassword.updatePW = {};
            changePassword.updatePW.errorMessage = '';
            changePassword.updatePW.updatePassword = updatePassword;
            changePassword.generalInfo = {};
            changePassword.isGSAdmin = sessionStorage.getItem( "isGSAdmin");

            _resetPasswordFields();

            $scope.generalObj.previousData = null;
            if (typeof $scope.resetInvalidFieldErrorStr == 'function') {
                $scope.resetInvalidFieldErrorStr();
            }

            loadingState.hide();
        }

        activate();

        /**
         * @func updatePassword()
         * @param {object} updatePWForm - password form object
         */
        function updatePassword(updatePWForm,event) {

            if(event && event.which != 13){
                return;
            }

            changePassword.updatePW.errorMessage = '';
            if ($scope.isFormValid(updatePWForm)) {
                var oldPassword = changePassword.updatePW.oldPassword;
                var newPassword = changePassword.updatePW.newPassword;
                AccountService.updatePassword(oldPassword,newPassword).success(function (response) {
                    sessionStorage. setItem("token",response.token);
                    commonUtilityService.storeUserTokenExpiryTime(response);
                    var userId = sessionStorage.getItem('userId');
                    var enterpriseId = sessionStorage.getItem("rootAccountId")?sessionStorage.getItem("rootAccountId"):sessionStorage.getItem("accountId");
                    AuthenticationService.enterpriseLogin(enterpriseId, userId).success(function (enterpriseLoginResp) {
                        commonUtilityService.storeEnterpriseTokenExpiryTime(enterpriseLoginResp);
                        commonUtilityService.showSuccessNotification("Password has been changed successfully.");
                    }).error(function (enterpriseLoginResp) {
                        $location.path("#/auth/login");
                        console.log('authentication failure' + enterpriseLoginResp);
                    });
                }).error(function (data) {
                    changePassword.updatePW.errorMessage = data.errorStr;
                });

                _resetPasswordFields();
            }
        }

        /**
         * @func _resetPasswordFields()
         * @desc resets password data binding objects
         * @private
         */
        function _resetPasswordFields() {
            changePassword.updatePW.newPassword = '';
            changePassword.updatePW.oldPassword = '';
            changePassword.updatePW.verifyPassword = '';
        }
    }
})();