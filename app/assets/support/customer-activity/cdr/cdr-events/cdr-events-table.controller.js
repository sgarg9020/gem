(function(){
    "use strict";

    angular.module('app')
        .controller('CdrEventsCtrl', CdrEvents);

    CdrEvents.$inject=['$scope','$sce','$compile','dataTableConfigService','AuthenticationService','commonUtilityService','loadingState','CustomerActivityFactory'];

    function CdrEvents($scope,$sce,$compile,dataTableConfigService,authenticationService,commonUtilityService,loadingState,CustomerActivityFactory){

        var cdrEventsVm = this;

        function activate(){
            initTables();
        }

        activate();

        function initTables() {
            clearDataTableStatus(['DataTables_cdrEventsTable_']);
            cdrEventsVm.trustAsHtml = function (value){return $sce.trustAsHtml(value);};
            loadingState.hide();
            cdrEventsVm.simCDRReadAllowed = authenticationService.isOperationAllowed(['SIM_CDR_READ']);

            $scope.emptyCdrEventsTableMessage = "No CDR events available.";
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableHeaders = [
                {value: "SimId", DbValue: "simId"},
                {value: "Time", DbValue: "date"},
                {value: "Data Usage in KB", DbValue: "dataInBytes"},
                {value: "IMSI Profile", DbValue: "imsiProfile"},
                {value: "IMSI", DbValue: "imsi"},
                {value: "Account", DbValue: "accountName"},
                {value: "Carrier", DbValue: "carrierName"},
                {value: "MCC-MNC", DbValue: ""},
                {value: "Countries", DbValue: "country"}
            ];

            $scope.dataTableObj.tableID = "cdrEventsTable";
            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);
        }

        function _setDataTableOptions(){
            var columnsConfig = [
                { orderable:false},
                { orderable:true},
                { orderable:false},
                { orderable:false},
                { orderable:false},
                { orderable:false},
                { orderable:false},
                { orderable:false},
                { orderable:false}
            ];
            var columnsDef = [{
                targets: 0,
                render: function (data, type, row) {
                    var simId = row.simId;
                    if(cdrEventsVm.simCDRReadAllowed) {
                        var accountId = row.accountId;
                        return '<a ui-sref="support.sim.simDetail({accountId:\''+accountId+'\',iccId:\''+simId+'\'})" class="cursor">' + simId + '</a>';
                    }
                    else{
                        return simId;
                    }
                }
            }, {
                targets: 1,
                render: function (data, type, row) {
                    var time = row.recordStartTime;
                    return '<span>'+time+'</span>';
                }
            }, {
                targets: 2,
                render: function (data, type, row) {
                    var dataUsed= commonUtilityService.calculateByteRange(row.dataInBytes, "KB");
                    var dataUsedFormatted = commonUtilityService.calculateByteRange(row.dataInBytes);
                    return '<span>'+dataUsed.data+'</span>';
                }
            }, {
                targets: 3,
                render: function (data, type, row) {
                    var imsiProfile = row.imsiProfile;
                    return '<span>'+imsiProfile+'</span>';
                }
            }, {
                targets: 4,
                render: function (data, type, row) {
                    var imsi = row.imsi;
                    return '<span>'+imsi+'</span>';
                }
            }, {
                targets: 5,
                render: function (data, type, row) {
                    var accountId = row.accountId;
                    var route = "support.enterprise.info({accountId:"+accountId+"})";
                    return '<a ui-sref='+route+' class="cursor">'+row.accountName+'</a>';
                }
            }, {
                targets: 6,
                render: function (data, type, row) {
                    var carrierName = row.carrierName;
                    return '<span>'+carrierName+'</span>';
                }
            }, {
                targets: 7,
                render: function (data, type, row) {
                    var mcc = row.mcc+' - '+row.mnc;
                    return '<span>'+mcc+'</span>';
                }
            }, {
                targets: 8,
                render: function (data, type, row) {
                    var countries = row.countryList;
                    var columnValue = '';
                    for(var i=0; i<countries.length; i++){
                        if(columnValue){
                            columnValue += ', ';
                        }
                        columnValue += '<span>'+countries[i].code+'</span>';
                    }
                    return columnValue;
                }
            }
            ];

            return {
                order: [[ 1, "desc" ]],
                createdRow: function ( row, data, index ) {
                    $compile(row)($scope);
                },
                columns: columnsConfig,
                columnDefs: columnsDef,
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> entries",
                    sEmptyTable: $scope.emptyCdrEventsTableMessage,
                    sZeroRecords:$scope.emptyCdrEventsTableMessage
                },
                processing: true,
                pagingType: "simple",
                fnServerData: function (sSource, aoData, fnCallback) {
                    var tableQueryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    var queryParams = $scope.$parent.cdrVm.constructQueryParam();
                    queryParams.sortBy = tableQueryParam.sortBy;
                    queryParams.sortDirection = tableQueryParam.sortDirection;
                    queryParams.noPace = $scope.noPace;
                    var accountId = $scope.$parent.cdrFilters.account ? $scope.$parent.cdrFilters.account.accountId : sessionStorage.getItem("accountId");
                    queryParams.count = tableQueryParam.count;
                    queryParams.startIndex  = tableQueryParam.startIndex;
                    CustomerActivityFactory.getCdrEvents(accountId, queryParams).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        if(response.list.length < queryParams.count){
                            response.totalCount = queryParams.startIndex + response.list.length;
                        }else{
                            response.totalCount = queryParams.startIndex + queryParams.count + 1;
                        }

                        if(response.list.length == 0 && queryParams.startIndex != 0){
                            commonUtilityService.showErrorNotification("There are no more records to navigate further.");
                            oSettings._iDisplayStart = queryParams.startIndex - queryParams.count;
                            response.list = $scope.eventList;
                        }

                        var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyCdrEventsTableMessage);
                        $scope.eventList = datTabData.aaData;
                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }


                    }).error(function (data) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"cdr event data");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });
                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.eventList )
                        dataTableConfigService.disableTableHeaderOps( $scope.eventList.length, oSettings );
                    var table$ = $( "#" + $scope.dataTableObj.tableID );
                }
            };
        }

        $scope.$on('filtersUpdated', function (event, data) {
            $scope.noPace = false;
            if($scope.$parent.cdrFilters.country){
                clearDataTableStatus(['DataTables_cdrEventsTable_']);
                $scope.dataTableObj.dataTableRef.fnClearTable();
            }
        });
    }
})();
