(function(){
    "use strict";

    angular.module('app')
        .controller('CdrCtrl', Cdr);

    Cdr.$inject=['$scope','$rootScope','$sce','dataTableConfigService','AuthenticationService','commonUtilityService','CustomerActivityFactory','FilterValueService', 'CountryCode'];

    function Cdr($scope,$rootScope,$sce,dataTableConfigService,authenticationService,commonUtilityService,CustomerActivityFactory,FilterValueService,CountryCode){

        var cdrVm = this;

        function activate(){
            var savedFilters = FilterValueService.getFilterValue();
            var defaultFilters = {
                country: '',
                imsi: '',
                account: '',
                toDate: moment.utc(),
                fromDate: moment.utc().subtract(24, 'hours')
            };
            $scope.cdrFilters = savedFilters || defaultFilters;
            $scope.$parent.updateFiltersWithSavedData();

            cdrVm.constructQueryParam = constructQueryParam;
            fetchLatestData();
        }

        activate();

        function updateCdrMap(data) {
            var latestFilters = data.filters;
            var isOnlyCountryUpdated = CustomerActivityFactory.isOnlyCountryFilterUpdated($scope.cdrFilters,latestFilters);
            $scope.cdrFilters = latestFilters;
            if(isOnlyCountryUpdated){
                var selectedCountryCode = $scope.cdrFilters.country.code.length == 2 ? CountryCode.twoDigitToThreeDigit($scope.cdrFilters.country.code) : $scope.cdrFilters.country.code;
                var selectedCountryActivity =  CustomerActivityFactory.getEventActivityOfTheCountry(cdrVm.activityList, selectedCountryCode);
                if(selectedCountryActivity){
                    var mapData = {};
                    mapData[selectedCountryActivity.country.code] = {
                        fillKey: selectedCountryActivity.sims <= 10 ? 'LOW' : (selectedCountryActivity.sims <= 100 ? 'MEDIUM' : 'HIGH'),
                        sims: selectedCountryActivity.sims
                    }
                    $scope.$parent.drawMap(mapData);
                    if(!data.countrySelectionFromTable){
                        var response ={
                            "list" : [selectedCountryActivity]
                        }
                        $rootScope.$emit('onCDRActivityInfo',response);
                    }
                }else{
                    fetchLatestData();
                }
            }else{
                fetchLatestData();
            }
        }

        function fetchLatestData() {
            var queryParams = cdrVm.constructQueryParam();
            var accountId = $scope.cdrFilters.account ? $scope.cdrFilters.account.accountId : sessionStorage.getItem("accountId");
            getCdrMapData(accountId, queryParams);
        }



        function constructQueryParam() {
            var filter = '';
            if ($scope.cdrFilters.country) {
                var code = $scope.cdrFilters.country.code;
                var twoDigitCode = (code.length == 3) ? CountryCode.threeDigitToTwoDigit($scope.cdrFilters.country.code) : code;
                filter = filter + 'country=='+twoDigitCode;
            }
            if ($scope.cdrFilters.imsi) {
                filter = (filter ? (filter + '&') : filter) + 'imsiType=='+$scope.cdrFilters.imsi;
            }
            var queryParams = {
                groupBy: 'COUNTRY',
                fromDate: moment.utc($scope.cdrFilters.fromDate).format('YYYY-MM-DD HH:mm:ss'),
                toDate: moment.utc($scope.cdrFilters.toDate).format('YYYY-MM-DD HH:mm:ss'),
            };
            if(filter){
                queryParams['filters'] = filter;
            }
            return queryParams;
        }

        function getCdrMapData(accountId, queryParams) {
            var mapData = {};
            CustomerActivityFactory.getCdrActivity(accountId, queryParams).success(function (response) {
                if(response.list){
                    cdrVm.activityList = response.list;
                    $rootScope.$emit('onCDRActivityInfo',response);
                    response.list.forEach(function (cdr) {
                        cdr.country.code = CountryCode.twoDigitToThreeDigit(cdr.country.code);
                        mapData[cdr.country.code] = {
                            fillKey: cdr.sims <= 10 ? 'LOW' : (cdr.sims <= 100 ? 'MEDIUM' : 'HIGH'),
                            sims: cdr.sims
                        }
                    });
                }
                $scope.$parent.drawMap(mapData);
            }).error(function (error) {
                var errStr = (error.data)?error.data.errorStr:error.errorStr;
                commonUtilityService.showErrorNotification(errStr);
                $scope.$parent.drawMap(undefined);
            });
        }

        $scope.$on('filtersUpdated', function (event, data) {
            updateCdrMap(data);
        });
    }
})();
