(function(){
    "use strict";

    angular.module('app')
        .controller('CdrDataCtrl', CdrData);

    CdrData.$inject=['$scope','$rootScope','$sce','$compile','dataTableConfigService','AuthenticationService','commonUtilityService','loadingState','CustomerActivityFactory'];

    function CdrData($scope,$rootScope,$sce,$compile,dataTableConfigService,authenticationService,commonUtilityService,loadingState,CustomerActivityFactory){

        var cdrDataVm = this;
        var updateCdrInfoListener;
        function activate(){
            initTables();
        }

        activate();

        function initTables() {
            clearDataTableStatus(['DataTables_cdrDataTable_']);
            cdrDataVm.trustAsHtml = function (value) {
                return $sce.trustAsHtml(value);
            };
            loadingState.hide();

            $scope.emptyCdrActivityTableMessage = "No CDR data available.";
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableHeaders = [
                {value: "Country", DbValue: "country"},
                {value: "Sim Count", DbValue: "sims"}
            ];

            $scope.dataTableObj.tableID = "cdrDataTable";
            $scope.dataTableObj.dataList = [];
            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();


            updateCdrInfoListener = $rootScope.$on('onCDRActivityInfo', function (event, data) {
                $scope.dataTableObj.dataList = data.list;
                if($scope.dataTableObj.dataTableRef){
                    $scope.dataTableObj.dataTableRef.fnClearTable();
                    if(data.list.length > 0){
                        $scope.dataTableObj.dataTableRef.fnAddData(data.list, true);
                    }
                }
            });
        }

        function _setDataTableOptions() {
            return {
                data:  $scope.dataTableObj.dataList,
                "searching": false,
                "lengthChange": false,
                pageLength: 10,
                createdRow: function ( row, data, index ) {
                    $compile(row)($scope);
                },
                columnDefs: [{
                    targets: 0,
                    render: function (data, type, row, meta) {
                        var country = row.country;
                        return '<a href="javascript:void(0)" ng-click=\'selectCountryFromTable($event,'+ meta.row +')\'>'+country.name+'</a>';
                    }
                }, {
                    targets: 1,
                    render: function (data, type, row) {
                        var sims = row.sims;
                        return '<span>'+sims+'</span>';
                    }
                }],
                "oLanguage": {
                    "sEmptyTable": $scope.emptyCdrActivityTableMessage,
                    "sZeroRecords":$scope.emptyCdrActivityTableMessage
                }
            }
        }

        $scope.selectCountryFromTable = function (event, index) {
            var cdrActivity = $scope.dataTableObj.dataList[index];
            var $prevTr = $('.row-selected')[0];
            if($prevTr){
                $($prevTr).removeClass('row-selected');
            }
            var $tr = $(event.currentTarget).closest('tr');
            $tr.addClass('row-selected');
            var countrySelectObj = {
                id: cdrActivity.country.code,
                properties: {
                    name: cdrActivity.country.name,
                    code: cdrActivity.country.code
                }
            };
            $scope.$parent.selectCountry(countrySelectObj, true);
        };

        $scope.$on('$destroy', function(){
            //unregistering listener
            if(updateCdrInfoListener)
                updateCdrInfoListener();
        });

    }
})();
