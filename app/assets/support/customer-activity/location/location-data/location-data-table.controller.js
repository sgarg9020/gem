(function(){
    "use strict";

    angular.module('app')
        .controller('LocationDataCtrl', LocationData);

    LocationData.$inject=['$scope','$rootScope', '$sce','$compile','dataTableConfigService','AuthenticationService','commonUtilityService','loadingState','CustomerActivityFactory'];

    function LocationData($scope,$rootScope, $sce,$compile,dataTableConfigService,authenticationService,commonUtilityService,loadingState,CustomerActivityFactory){

        var locationDataVm = this;
        var updateLocationInfoListener;

        function activate(){
            initTables();
        }

        activate();

        function initTables() {
            clearDataTableStatus(['DataTables_locationDataTable_']);
            locationDataVm.trustAsHtml = function (value){return $sce.trustAsHtml(value);};
            loadingState.hide();

            $scope.emptyLocationActivityTableMessage = "No location data available.";
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableHeaders = [
                {value: "Country", DbValue: "country"},
                {value: "Sim Count", DbValue: "sims"}
            ];

            $scope.dataTableObj.tableID = "locationDataTable";
            $scope.dataTableObj.dataList = [];
            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();


            updateLocationInfoListener = $rootScope.$on('onLocationActivityInfo', function (event, data) {
                $scope.dataTableObj.dataList = data.list;
                if($scope.dataTableObj.dataTableRef){
                    $scope.dataTableObj.dataTableRef.fnClearTable();
                    if(data.list.length > 0){
                        $scope.dataTableObj.dataTableRef.fnAddData(data.list, true);
                    }
                }
            });
        }

        function _setDataTableOptions() {
            return {
                data:  $scope.dataTableObj.dataList,
                "searching": false,
                "lengthChange": false,
                pageLength: 10,
                createdRow: function ( row, data, index ) {
                    $compile(row)($scope);
                },
                columnDefs: [{
                    targets: 0,
                    render: function (data, type, row, meta) {
                        var country = row.country;
                        return '<a href="javascript:void(0)" ng-click=\'selectCountryFromTable($event,'+ meta.row +')\'>'+country.name+'</a>';
                    }
                }, {
                    targets: 1,
                    render: function (data, type, row) {
                        var sims = row.sims;
                        return '<span>'+sims+'</span>';
                    }
                }],
                "oLanguage": {
                    "sEmptyTable": $scope.emptyLocationActivityTableMessage,
                    "sZeroRecords":$scope.emptyLocationActivityTableMessage
                }
            }
        }

        $scope.selectCountryFromTable = function (event, index) {
            var locationActivity = $scope.dataTableObj.dataList[index];
            var $prevTr = $('.row-selected')[0];
            if($prevTr){
                $($prevTr).removeClass('row-selected');
            }
            var $tr = $(event.currentTarget).closest('tr');
            $tr.addClass('row-selected');
            var countrySelectObj = {
                id: locationActivity.country.code,
                properties: {
                    name: locationActivity.country.name,
                    code: locationActivity.country.code
                }
            };
            $scope.$parent.selectCountry(countrySelectObj, true);
        };

        $scope.$on('$destroy', function(){
            //unregistering listener
            if(updateLocationInfoListener)
                updateLocationInfoListener();
        });
    }
})();
