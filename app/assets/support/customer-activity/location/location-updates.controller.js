(function(){
    "use strict";

    angular.module('app')
        .controller('SimLocationCtrl', SimLocationCtrl);

    SimLocationCtrl.$inject=['$scope', '$rootScope', '$sce', 'dataTableConfigService','AuthenticationService','commonUtilityService','CustomerActivityFactory','FilterValueService', 'CountryCode'];

    function SimLocationCtrl($scope, $rootScope, $sce, dataTableConfigService, authenticationService, commonUtilityService,CustomerActivityFactory,FilterValueService,CountryCode){

        var locationVm = this;

        function activate(){
            var savedFilters = FilterValueService.getFilterValue();
            var defaultFilters = {
                country: '',
                imsi: '',
                account: '',
                toDate: moment.utc(),
                fromDate: moment.utc().subtract(24, 'hours')
            };
            $scope.locationFilters = savedFilters || defaultFilters;
            $scope.$parent.updateFiltersWithSavedData();

            locationVm.constructQueryParam = constructQueryParam;
            fetchLatestData();
        }

        activate();

        function updateLocationMap(data) {
            var latestFilters = data.filters;
            var isOnlyCountryUpdated = CustomerActivityFactory.isOnlyCountryFilterUpdated($scope.locationFilters,latestFilters);
            $scope.locationFilters = latestFilters;
            if(isOnlyCountryUpdated){
                var selectedCountryCode = $scope.locationFilters.country.code.length == 2 ? CountryCode.twoDigitToThreeDigit($scope.locationFilters.country.code) : $scope.locationFilters.country.code;
                var selectedCountryActivity =  CustomerActivityFactory.getEventActivityOfTheCountry(locationVm.activityList, selectedCountryCode);
                if(selectedCountryActivity){
                    var mapData = {};
                    mapData[selectedCountryActivity.country.code] = {
                        fillKey: selectedCountryActivity.sims <= 10 ? 'LOW' : (selectedCountryActivity.sims <= 100 ? 'MEDIUM' : 'HIGH'),
                        sims: selectedCountryActivity.sims
                    }
                    $scope.$parent.drawMap(mapData);
                    if(!data.countrySelectionFromTable){
                        var response ={
                            "list" : [selectedCountryActivity]
                        }
                        $rootScope.$emit('onLocationActivityInfo',response);
                    }
                }else{
                    fetchLatestData();
                }
            }else{
                fetchLatestData();
            }
        }

        function fetchLatestData() {
            var queryParams = locationVm.constructQueryParam();
            var accountId = $scope.locationFilters.account ? $scope.locationFilters.account.accountId : sessionStorage.getItem("accountId");
            getLocationMapData(accountId, queryParams);
        }

        function constructQueryParam() {
            var filter = '';
            if ($scope.locationFilters.country) {
                var code = $scope.locationFilters.country.code;
                var twoDigitCode = (code.length == 3) ? CountryCode.threeDigitToTwoDigit($scope.locationFilters.country.code) : code;

                filter = filter + 'country=='+twoDigitCode;
            }
            if ($scope.locationFilters.imsi) {
                filter = (filter ? (filter + '&') : filter) + 'imsiType=='+$scope.locationFilters.imsi;
            }
            var queryParams = {
                groupBy: 'COUNTRY',
                fromDate: moment.utc($scope.locationFilters.fromDate).format('YYYY-MM-DD HH:mm:ss'),
                toDate: moment.utc($scope.locationFilters.toDate).format('YYYY-MM-DD HH:mm:ss')
            };
            if(filter){
                queryParams['filters'] = filter;
            }
            return queryParams;
        }

        function getLocationMapData(accountId, queryParams) {
            var mapData = {};
            CustomerActivityFactory.getLocationActivity(accountId, queryParams).success(function (response) {
                var mapData = {};
                if(response.list){
                    locationVm.activityList = response.list;
                    $rootScope.$emit('onLocationActivityInfo',response);
                    response.list.forEach(function (locUpdate) {
                        locUpdate.country.code = CountryCode.twoDigitToThreeDigit(locUpdate.country.code);
                        mapData[locUpdate.country.code] = {
                            fillKey: locUpdate.sims <= 10 ? 'LOW' : (locUpdate.sims <= 100 ? 'MEDIUM' : 'HIGH'),
                            sims: locUpdate.sims
                        }
                    });
                }
                $scope.$parent.drawMap(mapData);
            }).error(function (error) {
                var errStr = (error.data)?error.data.errorStr:error.errorStr;
                commonUtilityService.showErrorNotification(errStr);
                $scope.$parent.drawMap(undefined);
            });
        }

        $scope.$on('filtersUpdated', function (event, data) {
            updateLocationMap(data);
        });
    }
})();
