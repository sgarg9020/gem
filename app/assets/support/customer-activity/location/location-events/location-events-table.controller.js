(function(){
    "use strict";

    angular.module('app')
        .controller('LocationEventsCtrl', LocationEvents);

    LocationEvents.$inject=['$scope','$sce','$compile','dataTableConfigService','AuthenticationService','commonUtilityService','loadingState','CustomerActivityFactory'];

    function LocationEvents($scope,$sce,$compile,dataTableConfigService,authenticationService,commonUtilityService,loadingState,CustomerActivityFactory){

        var locationEventsVm = this;

        function activate(){
            initTables();
        }

        activate();

        function initTables() {
            clearDataTableStatus(['DataTables_locationEventsTable_']);
            locationEventsVm.trustAsHtml = function (value){return $sce.trustAsHtml(value);};
            loadingState.hide();
            locationEventsVm.simCDRReadAllowed = authenticationService.isOperationAllowed(['SIM_CDR_READ']);

            $scope.emptyLocationEventsTableMessage = "No location events available.";
            $scope.dataTableObj = {};

            $scope.dataTableObj.tableHeaders = [
                {value: "SimId", DbValue: "simId"},
                {value: "Time", DbValue: "date"},
                {value: "Connection Type", DbValue: "connectionTechnologyType"},
                {value: "IMSI Profile", DbValue: "imsiProfile"},
                {value: "IMSI", DbValue: "imsi"},
                {value: "Account", DbValue: "accountName"},
                {value: "Carrier", DbValue: "carrierName"},
                {value: "MCC-MNC", DbValue: ""},
                {value: "Countries", DbValue: "country"},
                {value: "Source", DbValue: "source"}
            ];

            $scope.dataTableObj.tableID = "locationEventsTable";
            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);
        }

        function _setDataTableOptions(){
            var columnsConfig = [
                { orderable:false},
                { orderable:true},
                { orderable:false},
                { orderable:false},
                { orderable:false},
                { orderable:false},
                { orderable:false},
                { orderable:false},
                { orderable:false},
                { orderable:false}
            ];
            var columnsDef = [{
                targets: 0,
                render: function (data, type, row) {
                    var simId = row.simId;
                    if(locationEventsVm.simCDRReadAllowed) {
                        var accountId = row.accountId;
                        return '<a ui-sref="support.sim.simDetail({accountId:\''+accountId+'\',iccId:\''+simId+'\'})"  class="cursor">' + simId + '</a>';
                    }
                    else{
                        return simId;
                    }
                }
            }, {
                targets: 1,
                render: function (data, type, row) {
                    var time = row.locationTime;
                    return '<span>'+time+'</span>';
                }
            }, {
                targets: 2,
                render: function (data, type, row) {
                    if (row.connectionType)
                        return '<span class="status-text nowrap">' + (row.connectionType === 'CA'?'Connection Accepted':row.connectionType === 'RJ'?'Rejected':row.connectionType) + '</span>';
                    return '<span class="status-text nowrap">Not Available</span>';
                }
            }, {
                targets: 3,
                render: function (data, type, row) {
                    var imsiProfile = row.imsiProfile;
                    return '<span>'+imsiProfile+'</span>';
                }
            }, {
                targets: 4,
                render: function (data, type, row) {
                    var imsi = row.imsi;
                    return '<span>'+imsi+'</span>';
                }
            }, {
                targets: 5,
                render: function (data, type, row) {
                    var accountId = row.accountId;
                    var route = "support.enterprise.info({accountId:"+accountId+"})";
                    return '<a ui-sref='+route+' class="cursor">'+row.accountName+'</a>';
                }
            }, {
                targets: 6,
                render: function (data, type, row) {
                    var carrierName = row.carrierName;
                    return '<span>'+carrierName+'</span>';
                }
            }, {
                targets: 7,
                render: function (data, type, row) {
                    var mcc = row.mcc +' - '+ row.mnc;
                    return '<span>'+mcc+'</span>';
                }
            }, {
                targets: 8,
                render: function (data, type, row) {
                    var countries = row.countryList;
                    var columnValue = '';
                    for(var i=0; i<countries.length; i++){
                        if(columnValue){
                            columnValue += ', ';
                        }
                        columnValue += '<span>'+countries[i].code+'</span>';
                    }
                    return columnValue;
                }
            }, {
                targets: 9,
                render: function (data, type, row) {
                    var source = row.source;
                    return '<span>'+source+'</span>';
                }
            }
            ];

            return {
                order: [[ 1, "desc" ]],

                createdRow: function ( row, data, index ) {
                    $compile(row)($scope);
                },
                columns: columnsConfig,
                columnDefs: columnsDef,
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> entries",
                    sEmptyTable: $scope.emptyLocationEventsTableMessage,
                    sZeroRecords:$scope.emptyLocationEventsTableMessage
                },
                processing: true,
                pagingType: "simple",
                fnServerData: function (sSource, aoData, fnCallback) {
                    var tableQueryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    var queryParams = $scope.$parent.locationVm.constructQueryParam();
                    queryParams.sortBy = tableQueryParam.sortBy;
                    queryParams.sortDirection = tableQueryParam.sortDirection;
                    queryParams.noPace = $scope.noPace;
                    var accountId = $scope.$parent.locationFilters.account ? $scope.$parent.locationFilters.account.accountId : sessionStorage.getItem("accountId");
                    queryParams.count = tableQueryParam.count;
                    queryParams.startIndex  = tableQueryParam.startIndex;
                    CustomerActivityFactory.getLocationEvents(accountId, queryParams).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        if(response.list.length < queryParams.count){
                            response.totalCount = queryParams.startIndex + response.list.length;
                        }else{
                            response.totalCount = queryParams.startIndex + queryParams.count + 1;
                        }
                        if(response.list.length == 0 && queryParams.startIndex != 0){
                            commonUtilityService.showErrorNotification("There are no more records to navigate further.");
                            oSettings._iDisplayStart = queryParams.startIndex - queryParams.count;
                            response.list = $scope.eventList;
                        }

                        var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyLocationEventsTableMessage);
                        $scope.eventList = datTabData.aaData;
                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }
                    }).error(function (data) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"location event data");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });
                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.eventList )
                        dataTableConfigService.disableTableHeaderOps( $scope.eventList.length, oSettings );
                    var table$ = $( "#" + $scope.dataTableObj.tableID );
                }
            };
        }

        $scope.$on('filtersUpdated', function (event, data) {
            $scope.noPace = false;
            if($scope.$parent.locationFilters.country){
                clearDataTableStatus(['DataTables_locationEventsTable_']);
                $scope.dataTableObj.dataTableRef.fnClearTable();
            }
        });
    }
})();
