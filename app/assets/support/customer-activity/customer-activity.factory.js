'use strict';

(function(){
    angular
        .module('app')
        .factory('CustomerActivityFactory', CustomerActivityFactory);

    CustomerActivityFactory.$inject = ['commonUtilityService'];

    function CustomerActivityFactory(commonUtilityService){

        return {
            getCdrActivity: getCdrActivity,
            getCdrEvents: getCdrEvents,
            getLocationActivity: getLocationActivity,
            getLocationEvents: getLocationEvents,
            getEventActivityOfTheCountry:getEventActivityOfTheCountry,
            isOnlyCountryFilterUpdated:isOnlyCountryFilterUpdated
        };


        function getCdrActivity(accountId,queryParam){
            var url = ENTERPRISE_GIGSKY_OABS_BASE+'account/'+accountId+'/analytics/cdractivity';
            var headerAdd=['SIM_CDR_READ_ADVANCED'];
            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParam);
        }

        function getCdrEvents(accountId,queryParam){
            var url = ENTERPRISE_GIGSKY_OABS_BASE+'account/'+accountId+'/analytics/cdrevents';
            var headerAdd=['SIM_CDR_READ_ADVANCED'];
            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParam);
        }

        function getLocationActivity(accountId,queryParam){
            var url = ENTERPRISE_GIGSKY_OABS_BASE+'account/'+accountId+'/analytics/locationactivity';
            var headerAdd = ['SIM_LOCATION_READ_ADVANCED'];
            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParam);
        }

        function getLocationEvents(accountId,queryParam){
            var url = ENTERPRISE_GIGSKY_OABS_BASE+'account/'+accountId+'/analytics/locationevents';
            var headerAdd = ['SIM_LOCATION_READ_ADVANCED'];
            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParam);
        }

        function getEventActivityOfTheCountry(activityList, countryCode) {
            var activity;
            for(var i=0; i<activityList.length; i++){
                var currentActivity = activityList[i];
                if(currentActivity.country.code == countryCode){
                    activity = currentActivity;
                    break;
                }
            }
            return activity;
        }

        function isOnlyCountryFilterUpdated(oldFilters, latestFilters) {
            var onlyCountryUpdated = true;
            if(oldFilters.imsi != latestFilters.imsi){
                onlyCountryUpdated = false;
            }else if(oldFilters.account.accountId != latestFilters.account.accountId){
                onlyCountryUpdated = false;
            }else if(moment.utc(oldFilters.fromDate).format('YYYY-MM-DD HH:mm:ss') != moment.utc(latestFilters.fromDate).format('YYYY-MM-DD HH:mm:ss')){
                onlyCountryUpdated = false;
            }else if(moment.utc(oldFilters.toDate).format('YYYY-MM-DD HH:mm:ss') != moment.utc(latestFilters.toDate).format('YYYY-MM-DD HH:mm:ss')){
                onlyCountryUpdated = false;
            }else if(!latestFilters.country){
                onlyCountryUpdated = false;
            }
            return onlyCountryUpdated;
        }
        
    }
})();

(function(){
    angular
        .module('app')
        .factory('FilterValueService', FilterValueService);

    function FilterValueService(){
        return {
            getFilterValue: getFilterValue,
            setFilterValue: setFilterValue,
            clearFilters: clearFilters
        };

        function getFilterValue(tab){
            var data = sessionStorage.getItem('simActivityFilters')
            return JSON.parse(data);
        }

        function setFilterValue(value){
            sessionStorage.setItem('simActivityFilters', JSON.stringify(value));
        }
        function clearFilters() {
            sessionStorage.removeItem('simActivityFilters');
        }
    }
})();
