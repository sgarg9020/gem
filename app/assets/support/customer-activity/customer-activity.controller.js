'use strict';

(function(){
    angular.module('app')
        .controller('CustomerActivityCtrl',  CustomerActivity);

    CustomerActivity.$inject = ['$scope', '$sce','$q','commonUtilityService','roamingProfileService','AuthenticationService','HttpPendingRequestsService','$timeout', 'FilterValueService'];

    function CustomerActivity($scope, $sce, $q, commonUtilityService, RoamingProfileService, authenticationService, httpPendingRequestsService, $timeout, FilterValueService){

        var filters = this;

        function activate(){
            $scope.accountId = sessionStorage.getItem("accountId");
            filters.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};
            initFilters();
        }

        function initFilters() {
            filters.values = {
                country: '',
                imsi: '',
                account: '',
                fromDate: '',
                toDate: ''
            };

            $q.all([RoamingProfileService.getAllowedIMSIProfiles(),commonUtilityService.getCountries()]).then(function (response) {
                filters.imsiList = response[0].data.list;
                filters.countryList = response[1].data.countryList;

            }, function (error) {
                var errStr = (error.data)?error.data.errorStr:error.errorStr;
                commonUtilityService.showErrorNotification(errStr);
            });

            $scope.accountSearchKey = {
                prevValue: '',
                newValue: ''
            };

            configDatepicker();

            $scope.updateFiltersWithSavedData();
        }

        function configDatepicker() {
            var curDate = moment.utc();
            var minDate = moment.utc(curDate).subtract(30, 'days');

            $scope.datePicker = {
                endDate: moment.utc(),
                startDate: moment.utc().subtract(24, 'hours')
            };

            $scope.opts = {
                minDate: moment.utc(minDate),
                maxDate: moment.utc(),
                startDate: $scope.datePicker.startDate,
                endDate: $scope.datePicker.endDate,
                linkedCalendars: true,
                opens:'left',
                timePicker: true,

                locale: {
                    applyClass: 'btn-green',
                    applyLabel: "Apply",
                    fromLabel: "From",
                    format: "DD/MMM/YY HH:mm",
                    toLabel: "To",
                    cancelLabel: 'Cancel',
                    customRangeLabel: 'Custom range'
                },
                ranges: {
                    '1 hour': [moment.utc().subtract(1, 'hours'), moment.utc()],
                    'Today': [moment.utc().startOf('day'), moment.utc()],
                    'Yesterday': [moment.utc().subtract(1, 'days').startOf('day'), moment.utc().subtract(1, 'days').endOf('day')],
                    'Last 7 Days': [moment.utc().subtract(7, 'days'), moment.utc()],
                    'Last 30 Days': [moment.utc().subtract(30, 'days'), moment.utc()]
                },
                eventHandlers : {
                    'apply.daterangepicker' : function() {
                        $scope.onDateChange();
                    }
                }
            };

            setFilterDateParams();
        }

        $scope.onDateChange = function () {
            $scope.opts.startDate = $scope.datePicker.startDate;
            $scope.opts.endDate = $scope.datePicker.endDate;
            setFilterDateParams();
        };

        function setFilterDateParams() {
            filters.values.fromDate = $scope.datePicker.startDate;
            filters.values.toDate = $scope.datePicker.endDate;
        }

        var timer;
        $scope.searchAccount = function() {
            filters.values.account = '';
            $scope.accountSearchKey.newValue.replace(/\s/g,'');

            if($scope.accountSearchKey.newValue != $scope.accountSearchKey.prevValue) {
                var timerVal = 500;

                if(timer){
                    $timeout.cancel(timer);
                }
                timer = $timeout(function() {
                    $scope.accountSearchKey.prevValue = $scope.accountSearchKey.newValue;
                    httpPendingRequestsService.cancelAll('searchApi');
                    if ($scope.accountSearchKey.newValue) {
                        getAccountList($scope.accountSearchKey.newValue);
                    } else {
                        filters.accountList = undefined;
                    }
                }, timerVal);
            }
        };

        function getAccountList(searchKeyword) {
            var queryParam = {
                count: 10,
                ignoreGroups: true,
                search: searchKeyword,
                sortBy: 'accountName',
                sortDirection: 'ASC',
                startIndex: 0,
                status: 'ACTIVE'
            };

            authenticationService.getSubAccounts($scope.accountId,queryParam).success(function (response) {
                filters.accountList = response.list;
            }).error(function (error) {
                var errStr = (error.data)?error.data.errorStr:error.errorStr;
                commonUtilityService.showErrorNotification(errStr);
            });
        }

        $scope.setAccountFilter = function (selectedAccount) {
            $scope.accountSearchKey.newValue = selectedAccount.accountName;
            $scope.accountSearchKey.prevValue = '';
            filters.values.account = selectedAccount;
            filters.accountList = undefined;
        };

        $scope.broadCastFilterChanges = function (fromTableRow) {
            clearAccountSearchData();
            clearNotAppliedDateModal();
            var broadCastData = {
                filters: JSON.parse(JSON.stringify(filters.values)),
                countrySelectionFromTable: fromTableRow
            }
            FilterValueService.setFilterValue(broadCastData.filters);
            $scope.$broadcast('filtersUpdated', broadCastData);
        };

        function clearAccountSearchData() {
            if (!filters.values.account) {
                $scope.accountSearchKey.newValue = '';
                $scope.accountSearchKey.prevValue = '';
            }
            filters.accountList = undefined;
        }

        function clearNotAppliedDateModal() {
            $scope.datePicker = {
                startDate: filters.values.fromDate,
                endDate: filters.values.toDate
            };
        }

       $scope.updateFiltersWithSavedData = function(){
           var savedFilters = FilterValueService.getFilterValue();

           if(savedFilters){
               filters.values.country = savedFilters.country;
               filters.values.imsi = savedFilters.imsi;
               filters.values.account = savedFilters.account;
               filters.accountList = undefined;

               $scope.accountSearchKey.newValue = savedFilters.account ? savedFilters.account.accountName : '';
               $scope.accountSearchKey.prevValue = savedFilters.account ? savedFilters.account.accountName : '';

               $scope.datePicker = {
                   startDate: moment.utc(savedFilters.fromDate).format('YYYY-MM-DD HH:mm:ss'),
                   endDate: moment.utc(savedFilters.toDate).format('YYYY-MM-DD HH:mm:ss')
               };
               setFilterDateParams();
           }else{
               filters.values.country = '';
               filters.values.imsi = '';
               filters.values.account = '';
               filters.values.fromDate = moment.utc().subtract(24, 'hours');
               filters.values.toDate = moment.utc();

               clearAccountSearchData();
           }
        }

        $scope.drawMap = function(data) {
            document.getElementById('container').innerHTML = '';
            var map = new Datamap({
                element: document.getElementById('container'),
                responsive: true,
                fills: {
                    'HIGH': '#008000',
                    'MEDIUM': '#f7b127',
                    'LOW': '#f04e42',
                    defaultFill: '#e8e1e1'
                },
                data: data,
                geographyConfig: {
                    popupTemplate: function(geo, data) {
                        var sims = data ? data.sims : 0;
                        return '<div class="hovertext">'+ geo.properties.name + ': ' + sims +' sims</div>';
                    },
                    highlightFillColor: '#48b0f7'
                },
                done: function(datamap, data) {
                    datamap.svg.selectAll('.datamaps-subunit').on('click', function(country) {
                        $scope.$apply(function () {
                            $scope.selectCountry(country);
                        })
                    });
                }
            });
            window.addEventListener('resize', function() {
                map.resize();
            });
        };

        $scope.selectCountry = function(country, fromTableRow) {
            filters.values.country = {
                code: country.id,
                name: country.properties.name,
                type: "country"
            };
            $scope.broadCastFilterChanges(fromTableRow);
        }

        $scope.$on('$destroy', function(){
            FilterValueService.clearFilters();
        });

        activate();
    }
})();
