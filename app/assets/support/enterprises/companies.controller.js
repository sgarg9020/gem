/* Controllers */
(function(){
    "use strict";

    angular.module('app')
        .controller('CompaniesCtrl', Companies);

    Companies.$inject=['$rootScope','$scope', '$sce', '$compile', 'dataTableConfigService','AuthenticationService','commonUtilityService', 'loadingState','$location','AccountService'];

    /**
     * @constructor Companies()
     * @param $rootScope
     * @param {object} $scope - Angular.js $scope provider
     * @param {object} $sce - Angular.js $sce provider (Strict Contextual Escaping)
     * @param {function} $compile - Angular.js $sanitize provider
     * @param {object} dataTableConfigService - Factory Object
     * @param authenticationService
     * @param commonUtilityService
     * @param loadingState
     * @param $location
     * @param AccountService
     * @constructor
     */
    function Companies($rootScope,$scope, $sce, $compile, dataTableConfigService, authenticationService, commonUtilityService, loadingState, $location,AccountService){

        var companies = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            clearDataTableStatus(['DataTables_companiesDataTable_']);

            var p = $location.absUrl();
            if (p.indexOf('support/accounts/companies') != -1) {
                $scope.emptyTableMessage = "No companies available.";
                $scope.accountId = sessionStorage.getItem("rootAccountId");
                $rootScope.isNavigatedThroughAdmin = false;
                sessionStorage.setItem('isNavigatedThroughAdmin', false);
                sessionStorage.removeItem('rootResellerAccountId');
                sessionStorage.setItem("accountId", $scope.accountId);
                $scope.setHierarchicalData(false, false, false);
            } else if(p.indexOf('app/sub-accounts') != -1){
                $scope.emptyTableMessage = "No sub accounts available.";
                $scope.setHierarchicalData(true, true, false);
                $scope.accountId = sessionStorage.getItem("rootResellerAccountId") || sessionStorage.getItem('viewed-acc-detail');
                sessionStorage.setItem("accountId", $scope.accountId);
            }

            companies.trustAsHtml = function (value){return $sce.trustAsHtml(value);};

            $scope.enterpriseModalObj =  {};

            companies.addEnterprise = addEnterpriseModal;
            companies.onAddNewEnterprise = onAddNewEnterprise;
            companies.navigateToGem = navigateToGem;
            companies.addAccountPermission = $scope.isResellerAccount ? "RESELLER_SUB_ACCOUNT_CREATE" : "ACCOUNT_CREATE";
            companies.addAccountButtonLabel = $scope.isResellerAccount ? "Add Sub Account" : "Add Enterprise";
            $scope.shortInfoScreen = false;
            if($scope.resellerOrSubAccount){
                if(sessionStorage.getItem("isSupport") != "true"){
                    $scope.shortInfoScreen = true;
                } else if(sessionStorage.getItem("isNavigatedThroughAdmin") == "true"){
                    $scope.shortInfoScreen = true;
                }
            }


            $scope.accStatusFilters = [ 'ALL', 'ACTIVE', 'INACTIVE', 'SUSPENDED'];

            commonUtilityService.getCountries().success(function(data){
                companies.countries = data.countryList;
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });

            $scope.statusMap = { ACTIVE:'active', INACTIVE:'inactive', SUSPENDED:'suspended'};

            // DataTable
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableHeaders = [
                {value: "Name", DbValue: "accountName"},
                {value: "Status", DbValue: "status"},
                {value: "Country", DbValue: "country"},
                {value: "Subscription", DbValue: "accountSubscriptionType"},
                {value: "Currency", DbValue: "currency"},
                {value: "Physical SIMs", DbValue: "totalPSimCount"},
                {value: "eSIMs", DbValue: "totalConsumerESimCount"},
                {value: "M2M eSIMs", DbValue: "totalIotM2MSimCount"},
                {value: "Parent Account", DbValue: "parentAccountName"},
                {value: "View As", DbValue: ""}
            ];

            $scope.dataTableObj.tableID = "companiesDataTable";

            var filtersConfig = { selectedDisplayLength: {type:'local', dbParam: 'count', defaultValue:100}, status: {type:'local', defaultValue: 'ACTIVE' } };
            dataTableConfigService.configureFilters($scope.dataTableObj, filtersConfig);

            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);
        }

        activate();

        /**
         * @function _setDataTableOptions()
         * @desc - function to set data table. This keeps the activate() function clean.
         */
        function _setDataTableOptions(){
            var columnsConfig = [
                { orderable:true},
                { orderable:false},
                { orderable:false},
                { orderable:false},
                { orderable:false},
                { orderable:false},
                { orderable:false},
                { orderable:false}
            ];
            var columnsDef = [{
                targets: 0,
                render: function (data, type, row) {
                    var accountId = row.accountId;
                    var route = $scope.isResellerAccount ? "app.account.info({accountId:"+accountId+"})" : "support.enterprise.info({accountId:"+accountId+"})";
                    return '<a ui-sref='+route+' class="cursor">'+row.accountName+'</a>';
                }
                }, {
                    targets: 1,
                    render: function (data, type, row) {
                        var entStatus = ($scope.statusMap[row.status])?$scope.statusMap[row.status]:((row.status)?row.status:'Not Available');
                        return '<span class="enterprise-status '+entStatus+'">'+entStatus+'</span>';
                    }
                },{
                    targets: 2,
                    render: function (data, type, row) {
                        var country = (row.company && row.company.address && row.company.address.country) ? commonUtilityService.getCountryObject(companies.countries,row.company.address.country).name : 'Not Available';
                        return '<span>'+country+'</span>';
                    }
                },{
                    targets: 3,
                    render:function (data, type, row){
                        var subscriptionType = (row.accountSubscriptionType) ? row.accountSubscriptionType.replace( "ENTERPRISE_", "GEM ").replace("_", " ") : 'GEM PRO';
                        return '<span>'+subscriptionType+'</span>';
                    }
                },{
                    targets: 4,
                    render:function (data, type, row){
                        return '<span>'+row.company.currency+'</span>';
                    }
                }, {
                    targets: 5,
                    render:function( data, type, row) {
                        var count = row.simCount ? row.simCount.totalPSimCount : '0';
                        return '<span>'+count+'</span>';
                    }
                },{
                    targets: 6,
                    render:function( data, type, row) {
                        var count = row.simCount ? row.simCount.totalConsumerESimCount : '0';
                        return '<span>'+count+'</span>';
                    }
                },{
                    targets: 7,
                    render:function( data, type, row) {
                        var count = row.simCount ? row.simCount.totalIotM2MSimCount : '0';
                        return '<span>'+count+'</span>';
                    }
                },{
                    targets: 8,
                    render:function( data, type, row) {
                        if(row.parentAccountId != $scope.accountId){
                            var parentAccountId = row.parentAccountId;
                            var route = $scope.isResellerAccount ? "app.account.info({accountId:"+parentAccountId+"})" : "support.enterprise.info({accountId:"+parentAccountId+"})";
                            return '<a ui-sref='+route+' class="cursor">'+row.parentAccountName+'</a>';
                        } else{
                            return '<span>'+row.parentAccountName+'</span>';
                        }
                    }
                }, {
                    targets: 9,
                    render:function( data, type, row, meta) {
                        return '<a ng-click=\"prepareNavigationToGem('+ meta.row +')\" class="cursor">View As</a>';
                    }
                }
            ];

            return {
                order: [[ 0, "asc" ]],

                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'AID_'+data.accountId);
                    $compile(row)($scope);
                },
                columns: columnsConfig,
                columnDefs: columnsDef,
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords:$scope.emptyTableMessage
                },
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {
                    if(aoData){
                        companies.searchValue = (aoData[5])?aoData[5].value.value:'';
                    }

                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    queryParam.ignoreGroups = true;
                    $scope.isPageSizeFilterDisabled = false;

                        authenticationService.getSubAccounts($scope.accountId,queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
                        $scope.companiesList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.companiesList.length == 0);
                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }

                    }).error(function (data) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.companiesList ||  $scope.companiesList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"companies");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });
                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.companiesList )
                        dataTableConfigService.disableTableHeaderOps( $scope.companiesList.length, oSettings );
                    var table$ = $( "#" + $scope.dataTableObj.tableID );
                    if($scope.isResellerAccount) {
                        table$.DataTable().column(3).visible(false);
                        table$.DataTable().column(8).visible(false);
                    }

                    $scope.enableViewAsColumn = authenticationService.isOperationAllowed(['GEM_ACCESS_FROM_GAP']);
                    table$.DataTable().column(9).visible($scope.enableViewAsColumn);
                }
            };
        }

        /**
         * @func prepareNavigationToGem
         * @desc setting properties to navigate to GEM from GAP
         * @param index - index of the company in the companiesList
         */
        $scope.prepareNavigationToGem = function(index){
            var company = $scope.companiesList[index];

            $rootScope.isNavigatedThroughAdmin = true;
            sessionStorage.setItem('isNavigatedThroughAdmin', true);

            sessionStorage.setItem("accountId", company.accountId);
            var enterpriseCurrency = company.company ? company.company.currency : '';
            sessionStorage.setItem( "gs-enterpriseCurrency",enterpriseCurrency);
            if (!$scope.isSupport && $scope.isResellerAccount) {
                sessionStorage.setItem("exit-to-state","app.sub-accounts");
            } else {
                sessionStorage.setItem("exit-to-state","support.accounts.companies");
            }
            companies.navigateToGem(company.accountId);
        };

        /**
         * @func navigateToGem
         * @desc navigates to the GEM account
         * @param accountId - accountId of the account to which it needs to navigate
         */
        function navigateToGem(accountId){
            var queryParam = {};
            queryParam.details = 'alertVersionSupported|pricingModelVersionSupported';

            /* get enterprise details */
            // need to pass query param once the defect is fixed from backend
            authenticationService.getEnterpriseAccountDetails(accountId/*,queryParam*/).success(function (response) {
                $scope.setHierarchicalData(response.accountType == RESELLER_ACCOUNT_TYPE, $scope.isResellerRoot || response.accountType == RESELLER_ACCOUNT_TYPE, response.accountType == SUB_ACCOUNT_TYPE);
                if(response.accountType == RESELLER_ACCOUNT_TYPE){
                    sessionStorage.setItem('rootResellerAccountId', response.accountId);
                }
                commonUtilityService.setSubscriptionTypeInSessionStorage(response.accountSubscriptionType);
                authenticationService.setAlertsFeatureEnabled(response.alertVersionSupported);
                authenticationService.setPricingModelVersion(response.pricingModelVersionSupported);
                sessionStorage.setItem('viewed-acc-detail',response.accountId);
                $location.path('/app/home');

            }).error(function (data) {
                console.log("error while fetching enterprise details");
                companies.errorMessage = data.errorStr;
            });
        }

        /**
         * @func addEnterpriseModal
         * @desc launches Add enterprise Modal
         */
        function addEnterpriseModal(){
            var header = $scope.isResellerAccount ? "Add Sub Account" : "Add Enterprise";
            var obj = {
                header: header,
                submit: "Create",
                cancel: "Cancel"
            };
            $scope.enterpriseModalObj.addEnterpriseModal(companies.countries, obj,companies.onAddNewEnterprise);

            $('#addEnterpriseModal').modal('show');
        }


        /**
         * @func onAddNewEnterprise
         * @desc adds new enterprise
         * @param drawFromPageOne {bool} - boolean value
         */
        function onAddNewEnterprise(drawFromPageOne){
            $scope.dataTableObj.refreshDataTable(drawFromPageOne);
        }


    }
})();
