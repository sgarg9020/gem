/* Controllers */
(function () {
    "use strict";

    angular.module('app')
        .controller('ProfileDetailBaseCtrl', ProfileDetailBase);

    ProfileDetailBase.$inject = ['$scope','loadingState','$stateParams','$rootScope', 'AuthenticationService'];
    
    function ProfileDetailBase($scope,loadingState,$stateParams,$rootScope,authenticationService) {

        var ProfileDetailBaseVm = this;

        /**
         * Start up logic for controller
         */
        function activate() {

            $scope.iccId = $stateParams.iccId;
            $scope.enterpriseId = $stateParams.accountId;
            getEnterpriseDetails();

            loadingState.hide();
        }

        //fetching accountName to show in breadcrumb
        function getEnterpriseDetails(){
            var queryParam = {};
            queryParam.details = 'accountName';
            /* get enterprise details */
            authenticationService.getEnterpriseAccountDetails($scope.enterpriseId,queryParam).success(function (response) {
                $rootScope.currentAccountName = response.accountName;
            }).error(function (data) {
                console.log("error while fetching enterprise details");
            });
        }

        activate();

    }
})();
