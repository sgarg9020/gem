'use strict';

(function () {
    angular.module('app')
        .controller('ProfileDetailCtrl', ProfileDetail);

    ProfileDetail.$inject = ['$scope', '$rootScope', '$stateParams', 'ProfileService', 'commonUtilityService'];

    function ProfileDetail($scope, $rootScope, $stateParams, ProfileService, commonUtilityService) {
        var profileDetailVM = this;

        profileDetailVM.getProfileDetails = getProfileDetails;

        /**
         * @function activate
         * @description All the activation code goes here.
         */
        function activate() {
            profileDetailVM.iccid = $stateParams.iccid;
            $scope.enterpriseId = $stateParams.accountId;

            getProfileDetails();
        }

        /**
         * @function getProfileDetails
         * @description fetches the Profile Details
         */
        function getProfileDetails() {

            ProfileService.getProfileDetails($scope.enterpriseId , profileDetailVM.iccid).success(function (response) {
                profileDetailVM.profile = response;
                profileDetailVM.profile.profileType = ProfileService.getDisplayableProfileType(profileDetailVM.profile.profileType);
                profileDetailVM.profile.profilePackage = ProfileService.getDisplayableProfilePackage(profileDetailVM.profile.profilePackage);
                profileDetailVM.profile.deploymentStatus = ProfileService.getDisplayableDeploymentStatus(profileDetailVM.profile.deploymentStatus);
                profileDetailVM.imsis = response.imsis;
                $scope.imsis = profileDetailVM.imsis;
            }).error(function (data) {
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

        activate();

    }
})();
