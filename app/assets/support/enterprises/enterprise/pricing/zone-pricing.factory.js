'use strict';

(function(){
    angular
        .module('app')
        .factory('zonePricingService', zonePricingService);

    zonePricingService.$inject = ["commonUtilityService", "SettingsService"];

    /**
     @function AlertsService()
     */
    function zonePricingService(commonUtilityService,SettingsService) {

        var decimalReqEx =/^\d+(\.\d*(e[-+])?\d*)$/;

        return {
            getZoneProvisionHistory: getZoneProvisionHistory,
            getZoneProvisioningStatus: getZoneProvisioningStatus,
            getZones: getZones,
            getZoneDetails: getZoneDetails,
            updateZoneNickName: updateZoneNickName,
            getZonesExportURI: getZonesExportURI,

            addBucket: addBucket,
            removeBucket: removeBucket,
            validatePricingInfo: validatePricingInfo,
            validateBucket: validateBucket,
            updatePricing: updatePricing,
            prepareZonePricingList: prepareZonePricingList,
            prepareZonePricingInfo: prepareZonePricingInfo

        };

        function getZoneProvisionHistory(enterpriseId, queryParams) {

            var url = ENTERPRISE_PLAN_API_BASE_V3 + "account/" + enterpriseId + '/zones/upload/session';
            var headerAdd = ['GET_ZONE_UPDATE_SESSIONS'];

            return commonUtilityService.prepareHttpRequest(url, 'GET', headerAdd, queryParams);
        }

        function getZoneProvisioningStatus(enterpriseId, statusId, queryParams) {
            var url = ENTERPRISE_PLAN_API_BASE_V3 + "account/" + enterpriseId + '/zones/upload/session/' +statusId ;
            var headerAdd = ['GET_ZONE_UPDATE_SESSIONS'];

            return commonUtilityService.prepareHttpRequest(url, 'GET', headerAdd, queryParams);
        }

        function getZones(accountId, queryParam){
            var enterpriseId = accountId ? accountId : sessionStorage.getItem("accountId");

            var headerAdd=['READ_ZONES'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_PLAN_API_BASE_V3+'account/'+enterpriseId+'/zones', 'GET',headerAdd,queryParam);
        }

        function getZoneDetails(accountId, zoneId, queryParam){
            var enterpriseId = accountId ? accountId : sessionStorage.getItem("accountId");

            var headerAdd=['READ_ZONES'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_PLAN_API_BASE_V3+'account/'+enterpriseId+'/zones/zone/'+zoneId, 'GET',headerAdd,queryParam);
        }

        function updateZoneNickName(enterpriseId, zoneId, nickname) {
            var url = ENTERPRISE_PLAN_API_BASE_V3 +  "account/"+ enterpriseId + "/zones/zone/"+ zoneId ;
            var headerAdd = ['ACCOUNT_EDIT'];

            var zone = {type: 'ZoneInfo', nickName: nickname};
            return commonUtilityService.prepareHttpRequest(url, 'PUT',headerAdd,false,zone);

        }

        function getZonesExportURI(enterpriseId){
            return ENTERPRISE_PLAN_API_BASE_V3 + "account/"+ enterpriseId + "/zones?fileType=csv&token=" + encodeURIComponent(sessionStorage.getItem("token"));
        }

        function updatePricing(enterpriseId, zoneId, zoneInfo){

            var url = ENTERPRISE_PLAN_API_BASE_V3 +  "account/"+ enterpriseId + "/zones/zone/"+ zoneId ;
            var headerAdd = ['ZONE_EDIT'];

            return commonUtilityService.prepareHttpRequest(url, 'PUT',headerAdd,false,zoneInfo);

        }


        function addBucket(bucketList,index){

            var newBucket = angular.copy(bucketList[index]);

            var currentBucket = bucketList[index];

            if( bucketList.length === index+1){
                currentBucket.endRange = currentBucket.startRange+currentBucket.unitValue;
                newBucket.startRange = currentBucket.endRange;
            }
            else{
                newBucket.startRange = currentBucket.endRange;
                newBucket.endRange = newBucket.startRange;
                newBucket.unitValue = 0;
            }


            bucketList.splice(index+1, 0, newBucket);

        }

        function removeBucket(bucketList,index){

            if(bucketList.length > 1){
                bucketList[index-1].endRange = null;
            }

            bucketList.splice(index, 1);

        }

        function validatePricingInfo(zone, ignorePricePerUnitValidation){

            var bucketList = zone.bucketPricingInfo.list;
            var errors = {};

            for(var j=0; j<bucketList.length; j++){
                validateBucket(bucketList[j], errors, j, bucketList.length, ignorePricePerUnitValidation);
            }

            errors.hasErrors = errors.hasErrors ? errors.hasErrors : false;

            return errors;

        }

        function validateBucket(bucket, errors, index, bucketLength, ignorePricePerUnitValidation) {
            errors[index+1] =  errors[index+1] ? errors[index+1] : {};

            if(!bucket.unitValue){
                errors.hasErrors = true;
                errors[index+1].unitValue = errors[index+1].unitValue ? errors[index+1].unitValue : 'Bucket size invalid';
            }

            if(!ignorePricePerUnitValidation && !bucket.pricePerUnit){
                errors.hasErrors = true;
                errors[index+1].pricePerUnit = errors[index+1].pricePerUnit ? errors[index+1].pricePerUnit : 'Bucket price invalid';
            }

            if(!errors[index+1].unitValue && decimalReqEx.test(bucket.unitValue)){
                errors.hasErrors = true;
                errors[index+1].unitValue = errors[index+1].unitValue ? errors[index+1].unitValue : 'Bucket size should not contain decimals';
            }

            if(bucket.startRange && decimalReqEx.test(bucket.startRange)){
                errors.hasErrors = true;
                errors[index+1].startRange = errors[index+1].startRange ? errors[index+1].startRange : 'Start range should not contain decimals';
            }

            if(index !== bucketLength-1){
                validateEndRange(bucket, errors, index);
            }

            return errors;
        }

        function validateEndRange(bucket, errors, index) {
            if(!bucket.endRange){
                errors.hasErrors = true;
                errors[index+1].endRange = 'End range invalid';
            }

            if(!errors[index+1].endRange && decimalReqEx.test(bucket.endRange)){
                errors.hasErrors = true;
                errors[index+1].endRange = 'End range should not contain decimals';
            }

            if(!errors[index+1].endRange &&  (bucket.endRange <= 0 || (bucket.startRange >= bucket.endRange))){
                errors.hasErrors = true;
                errors[index+1].endRange = 'End range should be greater than Start range';
            }

            if(!errors[index+1].endRange && ((bucket.endRange-bucket.startRange)%bucket.unitValue !==0)){
                errors.hasErrors = true;
                errors[index+1].endRange = 'Tier range should be in multiples of the bucket size';
            }
        }

        function prepareZonePricingList(zoneList) {
            for(var i=0; i < zoneList.length; i++){
                var zone = zoneList[i];
                prepareZonePricingInfo(zone);
            }
        }

        function prepareZonePricingInfo(zone){
            if(zone.pricingModelVersion === '1.1'){
                return;
            }

            zone.buckets = [];

            if(!zone.bucketPricingInfo)
                return;

            zone.minimumCommitmentData = 0;
            zone.minimumCommitmentCost = zone.bucketPricingInfo.minimumCommitment;

            var remainingCommitmentCost = zone.bucketPricingInfo.minimumCommitment;

            var buckets =(zone.bucketPricingInfo)? zone.bucketPricingInfo.list?zone.bucketPricingInfo.list:[] :[];

            for(var j=0; j< buckets.length; j++){

                var bucket = buckets[j];
                var startRange = isNaN(bucket.startRange)?0:bucket.startRange;
                var endRange = bucket.endRange;

                bucket.tier = 'Tier '+(j+1);
                bucket.range  = SettingsService.preparePricingRange(startRange,endRange,bucket.unitType);

                if(remainingCommitmentCost === undefined || remainingCommitmentCost === 0){
                    bucket.committedData = 'NA';
                    zone.buckets.push(bucket);
                    continue;
                }

                if(endRange == null){
                    var committedData = (remainingCommitmentCost * bucket.unitValue) / bucket.pricePerUnit;
                    bucket.committedData = isNaN(committedData) ?0:committedData;
                    remainingCommitmentCost = 0;
                }
                else{

                    var dataInTier = (endRange - startRange);
                    var dataInBucket = dataInTier / bucket.unitValue ;
                    var dataCostInBucket = dataInBucket *  bucket.pricePerUnit;

                    if(dataCostInBucket <= remainingCommitmentCost ){
                        remainingCommitmentCost -= dataCostInBucket;
                        bucket.committedData = isNaN(dataInTier)?0:dataInTier;
                    }
                    else{
                        var committedData = (remainingCommitmentCost * bucket.unitValue) / bucket.pricePerUnit;
                        bucket.committedData = isNaN(committedData)?0:committedData;
                        remainingCommitmentCost = 0;
                    }

                }

                zone.minimumCommitmentData += commonUtilityService.getUsageInBytes(bucket.committedData,bucket.unitType);

                bucket.committedData = (bucket.committedData !== 0)?SettingsService.formatDataSize(commonUtilityService.getUsageInBytes(bucket.committedData,bucket.unitType)):0;

                zone.buckets.push(bucket);
            }

            zone.minimumCommitmentData = (zone.minimumCommitmentData !== 0)?SettingsService.formatDataSize(zone.minimumCommitmentData):0;


        }

    }
})();