(function(){
    "use strict";

    angular.module('app')
        .controller('ZoneProvisioningStatusCtrl', ZoneProvisioningStatusCtrl);

    ZoneProvisioningStatusCtrl.$inject=['$scope','$rootScope','loadingState','$stateParams','$compile','dataTableConfigService','commonUtilityService','$timeout','zonePricingService','roamingProfileService'];

    /**
     @constructor ZoneProvisioningStatusCtrl()
     */
    function ZoneProvisioningStatusCtrl($scope,$rootScope,loadingState,$stateParams,$compile,dataTableConfigService,commonUtilityService,$timeout,zonePricingService,roamingProfileService){

        var zoneProvisioningStatus = this;
        var countries;

        /**
         * Start up logic for controller
         */
        function activate(){

            $scope.enterpriseId = $stateParams.accountId;
            $scope.statusId = $stateParams.statusId;

            $scope.emptyTableMessage = "No zones available.";
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = "entZoneProvStatusDataTable";
            $scope.dataTableObj.tableHeaders = [
                {value: "Zone", DbValue: "name"},
                {value: "Change", DbValue: "previewStatus"},
                {value: "Countries Added", DbValue: ""},
                {value: "Countries Deleted", DbValue: ""}
            ];
            
            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

            $scope.zoneStatusMap = { CREATE : 'Added', DELETE: 'Deleted', EDIT: 'Updated', NO_CHANGE: 'No Change', INVALID_CONFIGURATION: 'Invalid Configuration'};

        }
        activate();
        
        
        zoneProvisioningStatus.showCountries = function(index, type ) {
            var zone = $scope.zoneProvisioningStatusList[index];

            if(zone && zone.changeDescription){
                var countries = (type === 'added') ? zone.changeDescription.countriesToBeAdded : ((type === 'deleted') ? zone.changeDescription.countriesToBeDeleted : []);
            }

            $scope.countriesModalObj.showCountries(countries, 'countryList');

            $('#countryListModal').modal('show');
        };

        function _setDataTableOptions(){
            return {
                ordering: false,
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'ZID_'+(index+1));
                    $compile(row)($scope);
                },
                columns: [
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false}
                    ],
                columnDefs: [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return '<span>'+row.name+'</span>';
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            var status = $scope.zoneStatusMap[row.previewStatus] ? $scope.zoneStatusMap[row.previewStatus] : row.previewStatus;
                            return '<span>'+status+'</span>';
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row, meta) {
                            countries = row.changeDescription ?  row.changeDescription.countriesToBeAdded : [];
                            return countries && countries.length ? '<a ng-click=\'zoneProvisioningStatus.showCountries('+meta.row+',\"added\")\'>'+countries.length+'</a>': '<span>0</span>';
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row, meta) {
                            countries =  row.changeDescription ? row.changeDescription.countriesToBeDeleted: [];
                            return countries && countries.length ? '<a ng-click=\'zoneProvisioningStatus.showCountries('+meta.row+',\"deleted\")\'>'+countries.length+'</a>': '<span>0</span>';
                        }
                    }
                ],
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords:$scope.emptyTableMessage
                },
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {

                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    queryParam.count = 1000;
                    zonePricingService.getZoneProvisioningStatus($scope.enterpriseId,$scope.statusId, queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();

                        zoneProvisioningStatus.zonesAdded = response.zoneChangesDetailsCount? response.zoneChangesDetailsCount.zonesAddedCount : 0;
                        zoneProvisioningStatus.zonesDeleted =  response.zoneChangesDetailsCount? response.zoneChangesDetailsCount.zonesDeletedCount : 0;
                        zoneProvisioningStatus.zonesUpdated =  response.zoneChangesDetailsCount? response.zoneChangesDetailsCount.zonesEditedCount : 0;
                        var status = roamingProfileService.getDisplayStatusOfSIMAssociation(response.status);
                        zoneProvisioningStatus.sessionStatus = status.name;
                        zoneProvisioningStatus.changedBy = response.userDetails.emailId;
                        zoneProvisioningStatus.errorMessage = response.errorMessage;
                        response.zoneChangeList.totalCount = response.zoneChangeList.list ? response.zoneChangeList.list.length : 0;
                        var datTabData = dataTableConfigService.prepareResponseData(response.zoneChangeList) ;
                        $scope.zoneProvisioningStatusList = datTabData.aaData;
                        if(oSettings != null) {
                            oSettings._iDisplayLength = $scope.zoneProvisioningStatusList.length;
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }
                    }).error(function (data) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"zone provisioning status");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });

                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.zoneProvisioningStatusList )
                        dataTableConfigService.disableTableHeaderOps( $scope.zoneProvisioningStatusList.length, oSettings );

                }

            }
        }


    }
})();
