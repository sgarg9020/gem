(function(){
    "use strict";

    angular.module('app')
        .controller('ZoneProvisioningHistCtrl', ZoneProvisioningHistCtrl);

    ZoneProvisioningHistCtrl.$inject=['$scope','loadingState','$stateParams','$compile','dataTableConfigService','roamingProfileService','commonUtilityService','$timeout','zonePricingService'];

    /**
     @constructor ZoneProvisioningHistCtrl()
     */
    function ZoneProvisioningHistCtrl($scope,loadingState,$stateParams,$compile,dataTableConfigService,roamingProfileService,commonUtilityService,$timeout,zonePricingService){

        var ZoneProvisioningHist = this;

        /**
         * Start up logic for controller
         */
        function activate(){

            $scope.enterpriseId = $stateParams.accountId;

            ZoneProvisioningHist.refreshZoneProvHistTable = refreshZoneProvHistTable;
            $scope.emptyTableMessage = "No zone provisioning history available.";
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = "entZoneProvHistDataTable";
            $scope.dataTableObj.tableHeaders = [
                {value: "Date (UTC)", DbValue: "createdTime"},
                {value: "Session Id", DbValue: "sessionId"},
                {value: "Zones Added", DbValue: "zonesAddedCount"},
                {value: "Zones Deleted", DbValue: "zonesDeletedCount"},
                {value: "Zones Updated", DbValue: "zonesEditedCount"},
                {value: "Status", DbValue: "status"},
                {value: "Initiated By", DbValue: ""}
            ];
            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

        }

        activate();

        function _setDataTableOptions(){
            return {
                ordering: false,
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'SID_'+data.sessionId);
                    $compile(row)($scope);
                },
                columns: [
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false}
                ],
                columnDefs: [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return '<span>'+row.createdTime+'</span>';
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return '<span>'+row.sessionId+'</span>';
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            return '<span>'+row.zoneChangesDetailsCount ? row.zoneChangesDetailsCount.zonesAddedCount: 0+'</span>';
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return '<span>'+row.zoneChangesDetailsCount ? row.zoneChangesDetailsCount.zonesDeletedCount: 0+'</span>';
                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return '<span>'+row.zoneChangesDetailsCount ? row.zoneChangesDetailsCount.zonesEditedCount: 0+'</span>';
                        }
                    },
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            var status = roamingProfileService.getDisplayStatusOfSIMAssociation(row.status);
                            return '<span class="RP-association-status '+status.class+'"><a ui-sref="support.enterprise.zoneProvisioning.status({accountId:\''+$scope.enterpriseId+'\',statusId:\''+row.sessionId+'\'})" class="cursor">'+status.name+'</a></span>';
                        }
                    },
                    {
                        targets: 6,
                        render: function (data, type, row) {
                            return '<span>'+row.userDetails.emailId+'</span>';
                        },
                        "defaultContent":''
                    }
                ],
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords:$scope.emptyTableMessage
                },
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {

                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    $scope.isPageSizeFilterDisabled = false;
                    zonePricingService.getZoneProvisionHistory($scope.enterpriseId,queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response) ;
                        $scope.zoneProvHistList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.zoneProvHistList.length === 0);
                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }
                    }).error(function (data) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.zoneProvHistList ||  $scope.zoneProvHistList.length === 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"zone provisioning history");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });

                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.zoneProvHistList )
                        dataTableConfigService.disableTableHeaderOps( $scope.zoneProvHistList.length, oSettings );
                }

            }
        }


        function refreshZoneProvHistTable(notify){
            if(notify == 'showMsg')
                commonUtilityService.showSuccessNotification("Zone provisioning history refresh has been triggered.");
            $scope.dataTableObj.refreshDataTable(false);
        }

    }
})();
