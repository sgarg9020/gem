/* Controllers */
(function(){
    "use strict";

    angular.module('app')
        .controller('ZonesPreviewCtrl', ZonesPreview);

    ZonesPreview.$inject=['$scope','$rootScope','SettingsService','loadingState','$q','$stateParams','$compile','dataTableConfigService','commonUtilityService','$timeout','zonePricingService','roamingProfileService','$sce','$state','$location'];


    /**
     *
     * @param $scope
     * @param $rootScope
     * @param SettingsService
     * @param loadingState
     * @param $q
     * @param $stateParams
     * @param $compile
     * @param dataTableConfigService
     * @param commonUtilityService
     * @param $timeout
     * @param zonePricingService
     * @param roamingProfileService
     * @param $sce
     * @param $state
     * @constructor
     */
    function ZonesPreview($scope,$rootScope,SettingsService,loadingState,$q,$stateParams,$compile,dataTableConfigService,commonUtilityService,$timeout,zonePricingService,roamingProfileService,$sce,$state,$location){

        var zonesPreview = this;
        var countries;

        /**
         * Start up logic for controller
         */
        function activate(){

            $scope.enterpriseId = $stateParams.accountId;
            zonesPreview.errorMessage = $rootScope.zoneImportPreviewChanges ? '' : $sce.trustAsHtml('Zone provisioning session has been terminated due to page refresh. <a href="#/support/'+$stateParams.accountId+'/pricing">Continue</a> to zone provisioning page.');
            $scope.disableSubmit = zonesPreview.errorMessage !== '';
            zonesPreview.warningMessage = 'Please do not refresh the page to avoid termination of zone provisioning session.';
            zonesPreview.infoMessage = 'Please verify the zones import changes and click on \'Save\' to apply them.';
            $scope.emptyTableMessage = "No zones preview available.";
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = "entZonePreviewDataTable";
            $scope.dataTableObj.tableHeaders = [
                {value: "Zone", DbValue: "name"},
                {value: "Change", DbValue: "previewStatus"},
                {value: "Countries Added", DbValue: ""},
                {value: "Countries Deleted", DbValue: ""},
                {value: "Errors/Warnings", DbValue: ""}
            ];

            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

            $scope.zoneStatusMap = { CREATE : 'Added', DELETE: 'Deleted', EDIT: 'Updated',  NO_CHANGE: 'No Change', INVALID_CONFIGURATION: 'Invalid Configuration'};
        }
        activate();

        zonesPreview.navigateToPricing = function () {
            $rootScope.zoneImportPreviewChanges = null;
            $location.replace();
            $state.go('support.enterprise.pricing', {accountId: $stateParams.accountId});
        };

        zonesPreview.showCountries = function(index, type ) {
            var zone = $scope.zonesPreviewList[index];

            if(zone && zone.changeDescription){
                var countries = (type === 'added') ? zone.changeDescription.countriesToBeAdded : ((type === 'deleted') ? zone.changeDescription.countriesToBeDeleted : []);
            }

            $scope.countriesModalObj.showCountries(countries, 'countryList');

            $('#countryListModal').modal('show');
        };

        zonesPreview.displayErrorModal = function(index, type ) {
            var zone = $scope.zonesPreviewList[index];

            var errorObj = {
                type: 'ErrorsGroup',
                errors: zone.errors,
                warnings: zone.warnings
            }
            $scope.warningModalObj = $scope.warningModalObj ? $scope.warningModalObj : {};
            $scope.warningModalObj.disableButtons = true;
            commonUtilityService.showErrorNotification(errorObj,null ,$scope.warningModalObj);
        };

        function _setDataTableOptions(){
            return {
                ordering: false,
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'ZID_'+(index+1));
                    $compile(row)($scope);
                },
                columns: [
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false}
                ],
                columnDefs: [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return '<span>'+row.name+'</span>';
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            var status = $scope.zoneStatusMap[row.previewStatus] ? $scope.zoneStatusMap[row.previewStatus] : row.previewStatus;
                            return '<span>'+status+'</span>';
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row, meta) {
                            countries = row.changeDescription ? row.changeDescription.countriesToBeAdded : [];
                            return countries && countries.length ? '<a ng-click=\'zonesPreview.showCountries('+meta.row+',\"added\")\'>'+countries.length+'</a>': '<span>0</span>';
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row, meta) {
                            countries = row.changeDescription ? row.changeDescription.countriesToBeDeleted: [];
                            return countries && countries.length ? '<a ng-click=\'zonesPreview.showCountries('+meta.row+',\"deleted\")\'>'+countries.length+'</a>': '<span>0</span>';
                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row, meta) {
                            var errorType = null;

                            if(row.errors &&  row.errors.length){
                                zonesPreview.errorMessage = 'There are errors in certain zone(s). Please fix the errors & re-upload the file.';
                                $scope.disableSubmit = true;
                                errorType = 'ERROR_MODAL';
                            }else if(row.warnings &&  row.warnings.length){
                                errorType = 'WARNING_MODAL';
                            }

                             var status = roamingProfileService.getDisplayStatusOfSIMAssociation(errorType);


                            return (errorType)? '<span class="RP-association-status '+status.class+'"><a ng-click=\'zonesPreview.displayErrorModal('+meta.row+')\' class="cursor">'+status.name+'</a>': '<span>Not Available</span>';
                        }
                    }
                ],
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords:$scope.emptyTableMessage
                },
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {
                    loadingState.hide();
                    $scope.zoneChanges = ($rootScope.zoneImportPreviewChanges) ? $rootScope.zoneImportPreviewChanges : {list:[]};
                    $scope.zoneChanges.totalCount = $scope.zoneChanges.list.length;

                    $timeout(function () {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        if($rootScope.zoneImportPreviewChanges){
                            if(oSettings){
                                oSettings._iDisplayLength = $scope.zoneChanges.list.length;
                            }
                            var datTabData = dataTableConfigService.prepareResponseData($scope.zoneChanges) ;
                            $scope.zonesPreviewList = datTabData.aaData;
                            zonePricingService.prepareZonePricingList($scope.zonesPreviewList);
                            aoData[4].value = $scope.zonesPreviewList.length;
                            fnCallback(datTabData);
                        }else{
                            dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"zone provisioning status");
                            zonesPreview.errorMessage = $rootScope.zoneImportPreviewChanges ? '' : $sce.trustAsHtml('Zone provisioning session has been terminated due to page refresh. <a href="#/support/'+$stateParams.accountId+'/pricing">Continue</a> to zone provisioning page.');
                        }
                    });


                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.zonesPreviewList )
                        dataTableConfigService.disableTableHeaderOps( $scope.zonesPreviewList.length, oSettings );

                }

            }
        }

        zonesPreview.submitChanges = function () {
            loadingState.show();
            $rootScope.$broadcast('submitDropzoneFiles');
        };

        $scope.$on('dropZoneUploadSuccess',function (event) {
            loadingState.hide();
            $rootScope.showZonesImportSuccessMsg = true;
            zonesPreview.navigateToPricing();
        });

        $scope.$on('dropZoneUploadError',function (event, error) {
            $timeout(function () {
                loadingState.hide();
                $scope.disableSubmit = true;
                if(error && error.errorStr){
                    zonesPreview.errorMessage = error.errorStr;
                }
            });
        });

    }
})();
