'use strict';

/* Controllers */
(function(){
    angular.module('app')
        .controller('ImportZonesCtrl', ImportZones);

    ImportZones.$inject=['$scope','$rootScope', 'modalDetails', 'analytics','$stateParams','roamingProfileService','$sce', 'formMessage', '$state'];


    /**
     *
     * @param $scope
     * @param $rootScope
     * @param modalDetails
     * @param analytics
     * @param $stateParams
     * @param roamingProfileService
     * @param $sce
     * @param formMessage
     * @param $state
     * @constructor
     */
    function ImportZones($scope,$rootScope, modalDetails, analytics,$stateParams,roamingProfileService,$sce,formMessage, $state){
        var importZonesModal = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            Dropzone.autoDiscover = false;

            $rootScope.importZoneDropZoneConfig = {
                autoProcessQueue: false,
                maxFiles: 1,
                parallelUploads: 1,
                maxFilesize: 25,
                addRemoveLinks: false,
                clickable: true,
                acceptedFiles: ".csv",
                headers:{ "token" : sessionStorage.getItem( "token")},
                url: ENTERPRISE_PLAN_API_BASE_V3 + "account/" + $stateParams.accountId + '/zones/preview/',
                dictInvalidFileType:'File type not supported'

            };
            $scope.eventHandlers ={
                'addedfile': function(file){
                    //appending file name as title in the url
                    $rootScope.importZoneDropZoneConfig.url = $scope.url+ file.name;
                }
            }
            /* ModalDetails factory */
            modalDetails.scope(importZonesModal, true);
            modalDetails.attributes({
                id: 'importZonesModal',
                formName: 'importZonesForm',
                cancel: 'Clear',
                submit: 'Preview'
            });
            modalDetails.cancel(reset);
            modalDetails.submit(previewUpload);
            importZonesModal.cancelUpload = cancelUpload;
            importZonesModal.previewUploadObj = $scope.$parent.previewUploadObj ? $scope.$parent.previewUploadObj : {};
            importZonesModal.previewUploadObj.callback = previewCallback;
        }

        function reset(){
            $rootScope.$broadcast('clearDropzoneFiles');
        }


        function previewUpload() {
            importZonesModal.previewUploadObj.status = 'preview';
            $rootScope.$broadcast('submitDropzoneFiles');
        }

        function previewCallback(previewResponse) {
            $rootScope.zoneImportPreviewChanges = previewResponse ? JSON.parse(previewResponse): null;
            importZonesModal.previewUploadObj.status = 'upload';
            importZonesModal.previewUploadObj.disableErrorNotification = true;
            $rootScope.importZoneDropZoneConfig.url = ENTERPRISE_PLAN_API_BASE_V3 + "account/" + $stateParams.accountId + '/zones/upload/';
            $state.go('support.enterprise.zone.preview', {accountId: $stateParams.accountId});
        }


        function cancelUpload(){
            $rootScope.$broadcast('cancelDropzoneUpload');
        }

        activate();

    }
})();



