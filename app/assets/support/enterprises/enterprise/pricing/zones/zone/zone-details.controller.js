/* Controllers */
(function(){
    "use strict";

    angular.module('app')
        .controller('ZoneDetailsCtrl', ZoneDetails);

    ZoneDetails.$inject=['$scope','$rootScope','commonUtilityService','SettingsService','loadingState','$q','$stateParams','AuthenticationService','zonePricingService'];

    /**
     * @constructor ZoneDetails()
     * @param $scope
     * @param $rootScope
     * @param commonUtilityService
     * @param SettingsService
     * @param loadingState
     * @param $q
     * @param $stateParams
     * @param AuthenticationService
     * @param zonePricingService
     * @constructor
     */
    function ZoneDetails($scope,$rootScope,commonUtilityService,SettingsService,loadingState,$q,$stateParams,AuthenticationService,zonePricingService){

        var zoneDetails = this;

        /**
         * Start up logic for controller
         */
        function activate(){

            $scope.enterpriseId = $stateParams.accountId;
            $scope.zoneId = $stateParams.zoneId;
            $scope.editPricingObj = {editMode: false, onSuccessCallback: function () {
                    _pricingDetails();
                    commonUtilityService.showSuccessNotification('Pricing details have been updated successfully.');
                }};

            zoneDetails.account = {};

            _pricingDetails();
        }


        /**
         * @func _pricingDetails()
         * @desc controller request logic
         * @private
         */
        function _pricingDetails() {
            zoneDetails.errorMessage = '';

            var zoneQueryParam = {};
            zoneQueryParam.startIndex = 0;
            zoneQueryParam.count = 10000;
            zoneQueryParam.details = 'countryList';

            $q.all([zonePricingService.getZoneDetails($scope.enterpriseId, $scope.zoneId, zoneQueryParam), SettingsService.getEnterpriseDetails($scope.enterpriseId)]).then(function (response) {
                if(response[0]){
                    zoneDetails.zone = response[0].data;
                }
                var accountInfo = response[1].data;
                $rootScope.currentAccountName = accountInfo.accountName;
                zoneDetails.account.currency = commonUtilityService.getLocalCurrencySign(accountInfo.company.currency);

                zonePricingService.prepareZonePricingInfo(zoneDetails.zone);
                $scope.showPricingDetails = (zoneDetails.zone.pricingModelVersion === '1.2'  && (zoneDetails.zone.buckets && zoneDetails.zone.buckets.length));
            },function (data) {
                commonUtilityService.showErrorNotification((data&&data.data)?(data.data.errorStr):null);
            });
        }

        activate();

        zoneDetails.showCountries = function() {
            $scope.countriesModalObj.showCountries(zoneDetails.zone , 'zoneCountryList');
            $('#countryListModal').modal('show');
        };

        zoneDetails.editPricing =function(type){
            $scope.editPricingObj.editMode = true;
            $scope.editPricingObj.updatePricing((type === 'edit')? zoneDetails.zone : null, zoneDetails.account.currency);
        }

    }
})();
