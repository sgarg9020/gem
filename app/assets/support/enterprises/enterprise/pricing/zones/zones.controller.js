(function(){
    "use strict";

    angular.module('app')
        .controller('ZonesCtrl', Zones);

    Zones.$inject=['$scope','$rootScope','loadingState','$stateParams','$compile','dataTableConfigService','roamingProfileService','commonUtilityService','$timeout','zonePricingService','AuthenticationService','$q','SettingsService'];

    /**
     @constructor Zones()
     */
    function Zones($scope,$rootScope,loadingState,$stateParams,$compile,dataTableConfigService,roamingProfileService,commonUtilityService,$timeout,zonePricingService,AuthenticationService,$q,SettingsService){

        var zones = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            if($rootScope.showZonesImportSuccessMsg){
                $rootScope.showZonesImportSuccessMsg = false;
                commonUtilityService.showSuccessNotification('Zones have been imported successfully.');
            }

            $rootScope.importZoneDropZone = null;
            $scope.isBucketBasedPricing = true;
            $scope.enterpriseId = $stateParams.accountId;
            $scope.previewUploadObj = {};

            zones.refreshZonesTable = refreshZonesTable;
            zones.bulkImport = importZonesModal;
            zones.exportStartMessage = exportStartMessage;

            $scope.emptyTableMessage = "No zones available.";
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = "entZonesDataTable";
            $scope.dataTableObj.tableHeaders = [
                {value: "Name", DbValue: "name"},
                {value: "NickName", DbValue: "nickName"},
                {value: "Countries", DbValue: "zoneCountryList"},
                {value: "Pricing Version", DbValue: "pricingModelVersion"},
                {value: "Commitment Data", DbValue: "minimumCommitment"},
                {value: "Commitment Price", DbValue: ""},
                {value: "Price per MB", DbValue: "pricePerMB"},
                {value: "Last Updated (UTC)", DbValue: "lastUpdatedDate"}
            ];
            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

            $scope.isNickNameEditAllowed = AuthenticationService.isOperationAllowed(['ACCOUNT_EDIT']);
            $scope.coverageInfoExportUrl = zonePricingService.getZonesExportURI($scope.enterpriseId);
        }

        activate();

        function _setDataTableOptions(){
            return {
                ordering: false,
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'ZID_'+data.zoneId);
                    $compile(row)($scope);
                },
                columns: [
                    { orderable:false},
                    {
                        width:"210px",
                        orderable:false
                    },
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false}
                ],
                columnDefs: [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return (row.pricingModelVersion === '1.2')? '<a ui-sref="support.enterprise.zone.detail({accountId:\''+$scope.enterpriseId+'\',zoneId:\''+row.zoneId+'\'})" class="cursor">'+row.name+'</a>' : '<span>'+row.name+'</span>';
                        }
                    },
                    {
                        targets: 1,
                        "render": function (data, type, row) {
                            var nickname = row.nickName;
                            if ($scope.isSubAccount) {
                                return '<span>'+nickname+'</span>';
                            } else {
                                return $scope.isNickNameEditAllowed ? '<div class="nicknameContainer"><gs-edit-tool id=\'"nickname-' + row.zoneId + '"\' target-field="\'name\'" name="' + nickname + '" on-save="updateZoneNickname" on-error="showErrMsg" on-success="showSuccessMsg" /></div>' : nickname;
                            }
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row, meta) {
                            var countries = row.zoneCountryList;
                            return countries.length ? '<a ng-click=\'zones.showCountries('+meta.row+')\'>'+countries.length+'</a>': '<span>0</span>';
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return '<span>'+row.pricingModelVersion+'</span>';
                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return (row.minimumCommitmentData) ?  '<span>'+row.minimumCommitmentData+'</span>' : 'Not Available';
                        }
                    },
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return (row.minimumCommitmentCost) ? "<span class='nowrap'>{{'"+row.minimumCommitmentCost+"' | currency : zones.currency:2}}</span>": 'Not Available';
                        }
                    },
                    {
                        targets: 6,
                        render: function (data, type, row) {
                            return (row.pricePerMB) ? "<span class='nowrap'>{{'"+row.pricePerMB+"' | currency : zones.currency:2}}</span>": 'Not Available';
                        }
                    },
                    {
                        targets: 7,
                        render: function (data, type, row) {
                            return '<span>'+row.lastUpdatedDate+'</span>';
                        }
                    }
                ],
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords:$scope.emptyTableMessage
                },
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {

                    var zonesQueryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    zonesQueryParam.count = 1000;
                    var enterpriseQueryParam = {};
                    enterpriseQueryParam.details = 'company|pricingModelVersionSupported';


                    $q.all([zonePricingService.getZones($scope.enterpriseId,zonesQueryParam), SettingsService.getEnterpriseDetails($scope.enterpriseId,enterpriseQueryParam)]).then(function (response) {
                        var accountResponse = response[1].data;
                        zones.currency = commonUtilityService.getLocalCurrencySign(accountResponse.company.currency);
                        $scope.isBucketBasedPricing = (accountResponse.pricingModelVersionSupported === '1.2');


                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response[0].data) ;
                        $scope.zoneList = datTabData.aaData;
                        $scope.disableExport = ($scope.zoneList.length === 0);
                        zonePricingService.prepareZonePricingList($scope.zoneList);
                        if(oSettings != null) {
                            oSettings._iDisplayLength = $scope.zoneList.length;
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }

                    },function (data) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"zones");
                        commonUtilityService.showErrorNotification((data && data.data)?(data.data.errorStr):'Failed to load zones.');
                    });
                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.zoneList )
                        dataTableConfigService.disableTableHeaderOps( $scope.zoneList.length, oSettings );
                    hideTableColumns($scope.isBucketBasedPricing);
                }

            }
        }

        function hideTableColumns(isBucketBasedPricing) {
            var $table = $("#" + $scope.dataTableObj.tableID);

            $table.DataTable().column(4).visible(isBucketBasedPricing);
            $table.DataTable().column(5).visible(isBucketBasedPricing);
            $table.DataTable().column(6).visible(!isBucketBasedPricing);
        }

        function refreshZonesTable(notify){
            if(notify === 'showMsg')
                commonUtilityService.showSuccessNotification("Zone refresh has been triggered.");
            $scope.dataTableObj.refreshDataTable(false);
        }

        function importZonesModal() {
            if ($scope.isSubAccount) {
                commonUtilityService.showErrorNotification('Zones import is not allowed  to this enterprise. Please perform this operation in parent reseller account, which will be auto inherited to this enterprise.');
            } else {
                $('#importZonesModal').modal('show');
            }
        }

        function exportStartMessage() {
            commonUtilityService.showSuccessNotification("Export will start in few seconds.");
        }

        $scope.updateZoneNickname = function( zoneId, newNickname ){
            zoneId = zoneId.split( "-")[1];
            return zonePricingService.updateZoneNickName($scope.enterpriseId, zoneId, newNickname);
        };


        $scope.showErrMsg = function( errorMsg ) {
            commonUtilityService.showErrorNotification( errorMsg );
        };

        $scope.showSuccessMsg= function( ){
            commonUtilityService.showSuccessNotification( 'Zone Nickname updated successfully!' );

        };

        zones.showCountries = function(index) {
            var zone = $scope.zoneList[index];

            $scope.countriesModalObj.showCountries(zone, 'zoneCountryList');
            $('#countryListModal').modal('show');
        };

    }
})();
