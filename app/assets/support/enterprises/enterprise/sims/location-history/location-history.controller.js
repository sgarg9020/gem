'use strict';

(function () {
    angular.module('app')
        .controller('GapLocationHistoryCtrl', ['$scope', 'commonUtilityService', '$stateParams', 'dataTableConfigService', '$compile', 'SIMService','$filter', LocationHistory]);

    function LocationHistory($scope, commonUtilityService, $stateParams, dataTableConfigService, $compile, SIMService,$filter) {
        var locationHistory = this;
        var refreshRequired = false;

        function activate() {
            $scope.emptyTableMessage = "No Location history Available.";
            $scope.dataTableObj = {};
            $scope.dataTableObj.controlID = 'datatable-controls-location-history';
            $scope.dataTableObj.tableID = 'gapLocationHistoryDataTable';
            locationHistory.accountId = $stateParams.accountId;
            locationHistory.simId = $stateParams.iccId;
            var d = new Date();
            var year = d.getFullYear();
            var month = d.getMonth();
            var day = d.getDate();
            var startDate = encodeURIComponent($filter('date')(new Date(year - 10, month, day),'dd-MM-yyyy HH:mm:ss'));
            var endDate = encodeURIComponent($filter('date')(new Date(year, month, day),'dd-MM-yyyy HH:mm:ss'));

            locationHistory.exportData={
                url: SIMService.getExportLHdataUrl(locationHistory.accountId,locationHistory.simId,startDate,endDate)
            };

            $scope.dataTableObj.tableHeaders = [
                {"value": "Date (UTC)", "DbValue": "locationTime"},
                {"value": "IMSI", "DbValue": "imsi"},
                {"value": "IMSI Provider", "DbValue": "imsiProfile"},
                {"value": "MSISDN", "DbValue": "msisdn"},
                {"value": "Country", "DbValue": "countryList"},
                {"value": "Carrier", "DbValue": "carrierName"},
                {"value": "MCC-MNC", "DbValue": "mccmnc"},
                {"value": "Connection Type", "DbValue": "connectionType"},
                {"value": "Source", "DbValue": "source"}
            ];
            $scope.dataTableObj.dataTableOptions = {
                bSort: false,
                columnDefs: [{
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        return "<span class='nowrap'>" + row.locationTime + "</span>";
                    }
                },
                    {
                        "targets": 1,
                        "render": function (data, type, row, meta) {
                            return '<span>' + row.imsi + '</span>';
                        }
                    },
                    {
                        "targets": 2,
                        "render": function (data, type, row, meta) {
                            var provider = row.imsiProfile ? row.imsiProfile : 'Not Available';
                            return '<span>' + provider + '</span>';
                        }
                    },
                    {
                        "targets": 3,
                        "render": function (data, type, row, meta) {
                            return '<span>' + row.msisdn + '</span>';
                        }
                    },
                    {
                        "targets": 4,
                        "render": function (data, type, row, meta) {
                            return "<span class='nowrap'>" + commonUtilityService.getCountriesString(row.countryList) + "</span>";
                        }
                    },
                    {
                        "targets": 5,
                        "render": function (data, type, row, meta) {
                            var carrierName = row.carrierName ? row.carrierName : 'Not Available';
                            return "<span class='nowrap'>" + carrierName + "</span>";
                        }
                    },
                    {
                        "targets": 6,
                        "render": function (data, type, row, meta) {
                            if (row.mcc && row.mnc)
                                return '<span class="status-text nowrap">' + row.mcc + '-' + row.mnc + '</span>';
                            return '<span class="status-text">Not Available</span>';
                        }
                    },
                    {
                        "targets": 7,
                        "render": function (data, type, row, meta) {
                            if (row.connectionType)
                                return '<span class="status-text nowrap">' + (row.connectionType === 'CA'?'Connection Accepted':row.connectionType === 'RJ'?'Rejected':row.connectionType) + '</span>';
                            return '<span class="status-text nowrap">Not Available</span>';
                        }
                    },
                    {
                        "targets": 8,
                        "render": function (data, type, row, meta) {
                            var source = row.source ? row.source : 'Not Available';
                            return "<span class='nowrap'>" + source + "</span>";
                        }
                    }
                ],
                "createdRow": function (row, data, index) {
                    $compile(row)($scope);
                },
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $scope.isPageSizeFilterDisabled = false;

                    if(aoData){
                        locationHistory.searchValue = aoData[5].value.value;
                    }

                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);

                    locationHistory.exportData.startIndex = queryParam.startIndex;
                    locationHistory.exportData.count = queryParam.count;

                    SIMService.getSIMLocationHistory(locationHistory.accountId, locationHistory.simId, queryParam).success(function (response) {
                        locationHistory.simType = response.simType;
                        var tableForCol = $('#gapLocationHistoryDataTable').DataTable();
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
                        $scope.locationHistoryList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ($scope.locationHistoryList.length == 0);
                        fnCallback(datTabData);
                    }).error(function (data, status) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.locationHistoryList ||  $scope.locationHistoryList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"location history");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });

                },
                "fnDrawCallback": function (oSettings) {
                    if($scope.locationHistoryList )
                        dataTableConfigService.disableTableHeaderOps( $scope.locationHistoryList.length, oSettings );
                },
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ",
                    "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    "sEmptyTable": $scope.emptyTableMessage,
                    "sZeroRecords": $scope.emptyTableMessage
                }
            };


            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

        }

        activate();

    }
})();
