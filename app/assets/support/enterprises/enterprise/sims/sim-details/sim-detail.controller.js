'use strict';

(function () {
    angular.module('app')
        .controller('GapSIMDetailCtrl', SIMDetail);

    SIMDetail.$inject = ['$rootScope','$scope', '$state', '$stateParams','SIMService', 'commonUtilityService','AuthenticationService','loadingState', 'SIMConstants', 'ProfileService'];

    /**
     * @param $scope
     * @param SIMService
     * @param commonUtilityService
     * @param $stateParams
     * @constructor SIMDetail
     */
    function SIMDetail($rootScope,$scope, $state, $stateParams, SIMService, commonUtilityService,AuthenticationService, loadingState, SIMConstants, ProfileService) {
        var SIMDetailVM = this;

        SIMDetailVM.getSIMDetails = getSIMDetails;
        SIMDetailVM.hasOperations = hasOperations;
        SIMDetailVM.markESimAsDeleted = markESimAsDeleted;
        SIMDetailVM.hasAccess = {
            auditEis: function () {
                return SIMDetailVM.isM2mEsim && AuthenticationService.isOperationAllowed(['PROFILE_READ_ADVANCED']);
            },
            switchImsi: function () {
                return (SIMDetailVM.isPhysicalSim || SIMDetailVM.isConsumerEsim) && !SIMDetailVM.isSingleImsi && SIMDetailVM.SIM && SIMDetailVM.SIM.switchIMSIMode === 'MANUAL'
                    && AuthenticationService.isOperationAllowed(['SIM_EDIT_ADVANCED']);
            },
            changeMode: function () {
                return (SIMDetailVM.isPhysicalSim || SIMDetailVM.isConsumerEsim) && !SIMDetailVM.isSingleImsi
                    && AuthenticationService.isOperationAllowed(['SIM_EDIT_ADVANCED']);
            },
            markESimAsDeleted: function () {
                return SIMDetailVM.isConsumerEsim && AuthenticationService.isOperationAllowed(['PROFILE_STATUS_EDIT'])
                    && SIMDetailVM.consumerEsimInfo && SIMDetailVM.consumerEsimInfo.consumerEsimProfileStatus !== 'DELETED';
            }
        };

        /**
         * @function activate
         * @description All the activation code goes here.
         */
        function activate() {
            $scope.imsiModalObj = {};
            SIMDetailVM.simId = $stateParams.iccId;
            SIMDetailVM.accountId = $stateParams.accountId;
            SIMDetailVM.connectionStatus = null;
            SIMDetailVM.verifySwitchIMSIMOde = verifySwitchIMSIMOde;
            SIMDetailVM.switchIMSI = switchIMSI;
            SIMDetailVM.auditEIS = auditEIS;
            getSIMDetails();
        }


        function setPreferredIMSIPriority(preferredIMSIsList, imsi) {
            for (var i in  preferredIMSIsList) {
                var preferredIMSI = preferredIMSIsList[i];
                if (imsi.profile == preferredIMSI.profile) {
                    imsi.priority = preferredIMSI.priority;
                    imsi.profilePriority = imsi.profile + " (Preferred : " + preferredIMSI.priority + ")";
                    break;
                }
            }

            imsi.profilePriority = (imsi.profilePriority) ? imsi.profilePriority : imsi.profile;
        }

        /**
         * @function getSIMDetails
         * @description fetches the SIM Details
         */
        function getSIMDetails() {
            var queryParam = {
                details:"account|addedToAccountOn|nickName|user|roamingProfile|deviceType|eUICCDetails"
            };

            var permissionList = ['SIM_READ_ADVANCED','SIM_READ_EXTENDED'];
            var fetchSimDetails = AuthenticationService.isOperationAllowed(permissionList);
            queryParam.details = fetchSimDetails ? queryParam.details + "|simProfileDetails" : queryParam.details;
            SIMService.getSimDetail(SIMDetailVM.accountId,SIMDetailVM.simId,queryParam).success(function(response){
                SIMDetailVM.SIM = response;
                SIMDetailVM.user = response.user ? response.user.firstName + " " + response.user.lastName : 'Unassigned';
                SIMDetailVM.group = response.account ? response.account.accountName : '';
                if(response.imsis && response.imsis.length > 0){
                    SIMDetailVM.imsis = response.imsis;
                    SIMDetailVM.isSingleImsi = (SIMDetailVM.imsis.length <= 1) ? true : false;
                    var preferredIMSIsList = response.preferredIMSIProfilesList;
                    for(var i in SIMDetailVM.imsis){
                        var imsi = SIMDetailVM.imsis[i];
                        if(imsi.activationStatus == 'ACTIVE'){
                            SIMDetailVM.imsiProfile  = imsi.profile;
                            SIMDetailVM.msisdn = imsi.msisdn;
                        }
                        setPreferredIMSIPriority(preferredIMSIsList, imsi);
                    }
                    $scope.imsis = SIMDetailVM.imsis;
                }
                if(response.roamingProfile){
                    SIMDetailVM.roamingProfileName = response.roamingProfile.roamingProfileName;
                    SIMDetailVM.roamingProfileId = response.roamingProfile.roamingProfileId;
                    SIMDetailVM.roamingProfileSetId = response.roamingProfile.roamingProfileSetId;
                }
                if(response.eUICCDetails){
                    SIMDetailVM.eUICCDetails = response.eUICCDetails;
                }
                if(response.consumerEsimInfo){
                    SIMDetailVM.consumerEsimInfo = response.consumerEsimInfo;
                }
                configureSectionsAndLabels();
            }).error(function(response){
                if(response && (response.errorInt == 4801 || response.errorInt == 4800)){
                    commonUtilityService.showErrorNotification("SIM not found");
                    loadingState.hide();
                }
            });
        }

        function configureSectionsAndLabels() {
            SIMDetailVM.status = SIMConstants.status[SIMDetailVM.SIM.status] || {};
            SIMDetailVM.deviceType = SIMConstants.deviceType[SIMDetailVM.SIM.deviceType] || {};
            switch (SIMDetailVM.SIM.deviceType) {
                case 'pSIM':
                    SIMDetailVM.isPhysicalSim = true;
                    SIMDetailVM.canShowImsiProfiles = true;
                    SIMDetailVM.canShowOtaHistory = true;
                    SIMDetailVM.simIdLabel = 'Gigsky ICCID';
                    break;
                case 'ESIM_CONSUMER':
                    SIMDetailVM.isConsumerEsim = true;
                    SIMDetailVM.canShowImsiProfiles = true;
                    SIMDetailVM.canShowOtaHistory = true;
                    SIMDetailVM.simIdLabel = 'Gigsky ICCID';
                    SIMDetailVM.installationStatus = SIMConstants.installationStatus[SIMDetailVM.consumerEsimInfo.consumerEsimProfileStatus] || {};
                    break;
                case 'IOT_M2M':
                    SIMDetailVM.isM2mEsim = true;
                    SIMDetailVM.canShowEuiccProfiles = true;
                    SIMDetailVM.canShowOperationsHistory = true;
                    SIMDetailVM.simIdLabel = 'Gigsky EID';
                    break;
            }
        }

        function verifySwitchIMSIMOde(CurrentIMSIMode){
            var attr = {
                cancel: 'Cancel',
                submit: 'submit'
            };
            var msg = '';
            if (CurrentIMSIMode == 'MANUAL') {
                attr.header = 'Change Switch IMSI Mode to AUTOMATIC';
                msg = "Changing mode to AUTOMATIC is required for normal operation.";
            } else {
                attr.header = 'Change Switch IMSI Mode to MANUAL';
                msg = 'WARNING! Changing mode to MANUAL will prevent the GigSky system from changing IMSI automatically for this SIM card. In some cases, this may mean that the IMSI selected on the SIM is not active in that location, resulting in a loss of connectivity. For normal operation, change mode to AUTOMATIC.';
            }
            var IMSIMode = CurrentIMSIMode =='MANUAL'?'AUTOMATIC':'MANUAL';
            var func = function(){$scope.switchIMSIMode(SIMDetailVM.SIM.simId,IMSIMode)};

            $scope.verificationModalObj.updateVerificationModel(attr,func);
            $scope.verificationModalObj.addVerificationModelMessage(msg);

            $('#verificationModal').modal('show');
        }

        $scope.switchIMSIMode = function(simId,IMSIMode){

            var simsConf = {
                "type": "SimConfigurationList",
                "list": [
                    {
                        "type": "SimConfiguration",
                        "simId": simId,
                        "switchIMSIMode": IMSIMode
                    }
                ]
            };

            SIMService.switchIMSIMode(SIMDetailVM.accountId,simsConf).success(function(response){
                commonUtilityService.showSuccessNotification("Switch IMSI mode changed successfully to "+IMSIMode);
                SIMDetailVM.SIM.switchIMSIMode = IMSIMode;

            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });

        };

        function switchIMSI() {

            $scope.imsiModalObj.vfUpdateModal(SIMDetailVM.imsis, $scope.processSwitchIMSI);

            $('#switchImsiModal').modal('show');

        }

        $scope.processSwitchIMSI = function(targetImsi){

            var imsiConf = {
                "type": "SwitchIMSIRequest",
                "description": targetImsi.description,
                "imsiProfile": targetImsi.value.profile
            };

            SIMService.switchIMSI(SIMDetailVM.accountId,SIMDetailVM.simId,imsiConf).success(function(response){
                commonUtilityService.showSuccessNotification("Switch IMSI to " +targetImsi.value.profile+ " has been scheduled successfully");

                $rootScope.$emit('onSwitchImsi','');

            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        };

        function auditEIS() {
            var msg = "Are you sure you want to trigger Audit EIS?";
            var attr = {
                header: 'Audit EIS',
                cancel: 'Cancel',
                submit: 'Confirm'
            };
            var func = function(){
                SIMService.auditEIS(SIMDetailVM.accountId, SIMDetailVM.simId).success(function(response){
                    commonUtilityService.showSuccessNotification("Audit EIS operation has been triggered successfully.");
                    $rootScope.$broadcast('RefreshSIMOperationsHistory');
                }).error(function(data){
                    commonUtilityService.showErrorNotification(data.errorStr);
                });
            };

            $scope.verificationModalObj.updateVerificationModel(attr,func);
            $scope.verificationModalObj.addVerificationModelMessage(msg);

            $('#verificationModal').modal('show');
        }

        function markESimAsDeleted() {
            var msg = "Warning! Selected eSIMs will be deleted and CANNOT be used again.";
            var attr = {
                header: 'Mark eSIM as Deleted',
                cancel: 'Cancel',
                submit: 'Confirm'
            };
            var func = function(){
                ProfileService.changeProfileStatusOfEsim(SIMDetailVM.accountId, SIMDetailVM.simId).success(function(response){
                    commonUtilityService.showSuccessNotification("The eSIM has been marked as deleted successfully.");
                    getSIMDetails();
                }).error(function(data){
                    commonUtilityService.showErrorNotification(data.errorStr);
                });
            };

            $scope.verificationModalObj.updateVerificationModel(attr,func);
            $scope.verificationModalObj.addVerificationModelMessage(msg);

            $('#verificationModal').modal('show');
        }

        function hasOperations() {
            return SIMDetailVM.hasAccess.auditEis()
                || SIMDetailVM.hasAccess.switchImsi()
                || SIMDetailVM.hasAccess.changeMode()
                || SIMDetailVM.hasAccess.markESimAsDeleted();
        }

        activate();

    }
})();
