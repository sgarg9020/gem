/* Controllers */
(function () {
    "use strict";

    angular.module('app')
        .controller('GapSIMCtrl', SIM);

    SIM.$inject = ['$scope','loadingState','$stateParams','AuthenticationService','$rootScope'];

    /**
     @constructor SIM()
     */
    function SIM($scope,loadingState,$stateParams,authenticationService,$rootScope) {

        var SIMVm = this;

        /**
         * Start up logic for controller
         */
        function activate() {

            clearDataTableStatus(['DataTables_gapLocationHistoryDataTable_','DataTables_gapCdrListDataTable_','DataTables_OTAHistoryDataTable_']);

            $scope.iccId = $stateParams.iccId;
            $scope.enterpriseId = $stateParams.accountId;
            getEnterpriseDetails();

            loadingState.hide();
        }

        //fetching accountName to show in breadcrumb
        function getEnterpriseDetails(){
            var queryParam = {};
            queryParam.details = 'accountName';
            /* get enterprise details */
            authenticationService.getEnterpriseAccountDetails($scope.enterpriseId,queryParam).success(function (response) {
                $rootScope.currentAccountName = response.accountName;
            }).error(function (data) {
                console.log("error while fetching enterprise details");
            });
        }

        activate();

    }
})();
