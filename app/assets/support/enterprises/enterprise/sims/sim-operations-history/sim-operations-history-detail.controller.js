'use strict';

(function () {
    angular.module('app')
        .controller('SIMOperationsHistoryDetailCtrl', SIMOperationsHistoryDetail);

        SIMOperationsHistoryDetail.$inject = ['$scope', '$rootScope', '$stateParams', 'ProfileService'];

    function SIMOperationsHistoryDetail($scope, $rootScope, $stateParams, ProfileService) {
        var SIMOperationsHistoryDetailVm = this;

        /**
         * @function activate
         * @description All the activation code goes here.
         */
        function activate() {
            getProfileStatusDetails();
        }

        /**
         * @function getProfileDetails
         * @description fetches the Profile Details
         */
        function getProfileStatusDetails() {
            var data = sessionStorage.getItem('SIMOperationsHistoryDetail');
            SIMOperationsHistoryDetailVm.operation = JSON.parse(data);

            var processEndTime;
            for(var i in SIMOperationsHistoryDetailVm.operation.attempts){
                var attempt = SIMOperationsHistoryDetailVm.operation.attempts[i];
                if(attempt.status == 'COMPLETED'){
                    processEndTime = attempt.processEndTime;
                    break;
                }
            }
            SIMOperationsHistoryDetailVm.operation.processEndTime = processEndTime;
            $scope.operationType = ProfileService.getDisplayableOperationType(SIMOperationsHistoryDetailVm.operation.operationType);
        }

        activate();
    }
})();
