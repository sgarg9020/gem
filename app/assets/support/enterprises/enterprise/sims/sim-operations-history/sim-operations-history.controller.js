'use strict';

(function () {
    angular.module('app')
        .controller('SIMOperationsHistoryCtrl', ['$rootScope','$scope', 'commonUtilityService', '$stateParams', 'dataTableConfigService', '$compile', 'SIMService','$state', 'tooltips', 'ProfileService', SIMOperationsHistoryCtrl]);

    function SIMOperationsHistoryCtrl($rootScope,$scope, commonUtilityService, $stateParams, dataTableConfigService, $compile, SIMService,$state, tooltips, ProfileService) {
        var SIMOperationsHistoryVm = this;

        function activate() {
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = 'SIMOperationsHistoryDataTable';
            SIMOperationsHistoryVm.accountId = $stateParams.accountId;
            SIMOperationsHistoryVm.simId = $stateParams.iccId;
            SIMOperationsHistoryVm.navigateToStatus = navigateToStatus;

            $scope.dataTableObj.tableHeaders = [
                {"value": "Operation", "DbValue": "operationType"},
                {"value": "Initiator", "DbValue": "initiator"},
                {"value": "Target Profile", "DbValue": "targetProfileInfo"},
                {"value": "Active Profile", "DbValue": "causeProfileInfo"},
                {"value": "Country", "DbValue": "countryList"},
                {"value": "Status", "DbValue": "status"},
                {"value": "Request reason", "DbValue": "requestReason"},
                {"value": "Request time(UTC)", "DbValue": "requestTime"},
                {"value": "Attempts", "DbValue": "attempts"},
                {"value": "activation time(UTC)", "DbValue": "processEndTime"}
            ];
            $scope.dataTableObj.dataTableOptions = {
                bSort: false,
                columnDefs: [
                    {
                        "targets": 0,
                        "render": function (data, type, row) {
                            return '<span>' + ProfileService.getDisplayableOperationType(row.operationType) + '</span>';
                        }
                    },
                    {
                        "targets": 1,
                        "render": function (data, type, row) {
                            return '<span>' + row.initiator + '</span>';
                        }
                    },
                    {
                        "targets": 2,
                        "render": function (data, type, row) {
                            return row.targetProfileInfo.imsiProfile
                                ? '<span data-toggle="tooltip" data-placement="top" title="' + row.targetProfileInfo.imsi + '">' + row.targetProfileInfo.imsiProfile + '</span>'
                                : '<span></span>';
                        }
                    },
                    {
                        "targets": 3,
                        "render": function (data, type, row) {
                            return '<span data-toggle="tooltip" data-placement="top" title="'+row.causeProfileInfo.imsi+'">' + row.causeProfileInfo.imsiProfile  + '</span>';
                        }
                    },
                    {
                        "targets": 4,
                        "render": function (data, type, row) {
                            if(row.operationType === 'AUDIT_EIS') {
                                return "<span class='status-text nowrap'>Not Applicable</span>";
                            } else if (row.causeProfileInfo && row.causeProfileInfo.countryList) {
                                var location = '';
                                for (var i = 0; i < row.causeProfileInfo.countryList.length; i++) {
                                    if (i > 0)
                                        location = location + " , ";
                                    location = location + row.causeProfileInfo.countryList[i].countryName;
                                }

                                var zoneName = (row.causeProfileInfo.zoneInfo)? row.causeProfileInfo.zoneInfo.name : 'Zone Info Not Available';
                                return '<span class="nowrap" data-toggle="tooltip" data-placement="top" title="'+zoneName+'">' + location + '</span>';
                            }

                            return "<span class='status-text nowrap'>Not Available</span>";
                        }
                    },
                    {
                        "targets": 5,
                        "render": function (data, type, row) {
                            var statusColor = (row.status  == "IN_PROGRESS") ? "yellow" : (row.status  == "COMPLETED") ? "green" : "red";
                            return '<span class="status-text '+statusColor+'">' + row.status + '</span>';
                        }
                    },
                    {
                        "targets": 6,
                        "render": function (data, type, row) {
                            if(row.initiator === 'MANUAL'){
                                var title = "<b>" + row.userEmail + "</b>";
                                return '<span class="status-text nowrap" style="text-transform:none;" data-toggle="tooltip" data-html="true" data-placement="top" title="'+title+'">' + "USER " + '<span class="small hint-text">(hover for details)</span>'+'</span>';
                            }
                            else
                                return '<span></span>';
                        }
                    },
                    {
                        "targets": 7,
                        "render": function (data, type, row) {
                            return '<span class="nowrap">' + row.requestTime + '</span>';
                        }
                    },
                    {
                        "targets": 8,
                        "render": function (data, type, row, meta) {
                            if(row.attemptsCount > 0){
                                return '<a ng-click="SIMOperationsHistoryVm.navigateToStatus('+ meta.row +')" class="cursor">' + row.attemptsCount + '</a>';
                            }else{
                                return "<span class='nowrap'>" + row.attemptsCount + "</span>";
                            }
                        }
                    },
                    {
                        "targets": 9,
                        "render": function (data, type, row) {
                            var actuationTime;
                            for(var i in row.attempts){
                                var attempt = row.attempts[i];
                                if(attempt.status == 'COMPLETED'){
                                    actuationTime = attempt.processEndTime;
                                    break;
                                }
                            }

                            actuationTime = actuationTime ? actuationTime : 'Not Available';
                            return "<span class='nowrap'>" + actuationTime + "</span>";
                        }
                    }
                ],
                "createdRow": function (row, data, index) {
                    $compile(row)($scope);
                },
                "fnServerData": function (sSource, aoData, fnCallback) {
                    tooltips.destroyTooltips('[data-toggle="tooltip"]');
                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);

                    queryParam.includeLocations = false;
                    $scope.isPageSizeFilterDisabled = false;
                    SIMService.getSIMOperationsHistory(SIMOperationsHistoryVm.accountId, SIMOperationsHistoryVm.simId, queryParam).success(function (response) {
                        var datTabData = dataTableConfigService.prepareResponseData(response);
                        $scope.SIMOperationsHistoryList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.SIMOperationsHistoryList.length == 0);
                        fnCallback(datTabData);
                        tooltips.initTooltips('[data-toggle="tooltip"]');
                    }).error(function (data, status) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.SIMOperationsHistoryList ||  $scope.SIMOperationsHistoryList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"sim operations history");
                        commonUtilityService.showErrorNotification(data.errorStr,data.errorInt);
                    });

                },
                "fnDrawCallback": function (oSettings) {
                    if($scope.SIMOperationsHistoryList )
                        dataTableConfigService.disableTableHeaderOps( $scope.SIMOperationsHistoryList.length, oSettings );
                },
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ",
                    "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    "sEmptyTable": "No SIM operations history available.",
                    "sZeroRecords": "No SIM operations history available."
                }
            };


            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

        }

        function navigateToStatus(index){
            var operation = $scope.SIMOperationsHistoryList[index];
            sessionStorage.setItem('SIMOperationsHistoryDetail', JSON.stringify(operation));
            $state.go('support.sim.simOperationsHistory.status',{accountId: SIMOperationsHistoryVm.accountId, iccId: SIMOperationsHistoryVm.simId, operationId: operation.operationId});
        }

        $scope.$on('RefreshSIMOperationsHistory', function () {
            $scope.dataTableObj.refreshDataTable(true); 
        });

        $scope.$on('$destroy', function () {
            tooltips.destroyTooltips('[data-toggle="tooltip"]');

        });

        activate();

    }
})();
