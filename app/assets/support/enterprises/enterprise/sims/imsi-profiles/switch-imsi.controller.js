'use strict';

/* Controllers */
(function () {
    angular.module('app')
        .controller('SwitchImsiCtrl', SwitchImsi);

    SwitchImsi.$inject = ['$scope', '$sce', 'modalDetails', 'formMessage'];

    /**
     @constructor SwitchImsi()
     @param {object} $scope - Angular.js $scope
     @param {object} $sce - Angular.js strict contextual escaping service
     @param {object} modalDetails - Factory object
     */
    function SwitchImsi($scope, $sce, modalDetails, formMessage) {

        var imsiModalObj = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            /* Form elements object */
            imsiModalObj.imsi = {};

            /* modalDetails factory */
            modalDetails.scope(imsiModalObj, true);
            modalDetails.attributes({
                id: 'switchImsiModal',
                formName: 'switchImsi',
                header: 'Select required IMSI profile',
                cancel: 'Cancel',
                submit: 'Switch IMSI'
            });
            $scope.$parent.imsiModalObj.vfUpdateModal = updateModal;
            $scope.$parent.imsiModalObj.resetModal = removeData;

            
            modalDetails.cancel(removeData);

            imsiModalObj.trustAsHtml = function (value) {
                return $sce.trustAsHtml(value);
            };

            imsiModalObj.warningMessage = "<i class='fa fa-warning'></i> Switching IMSI to one that is not active in the SIM card's location will result in a loss of connectivity.";
        }

        function updateModal(imsis, submit,attrs) {
            imsiModalObj.imsis = [];

            //excluding active imsi
            for(var i=0; i<imsis.length ; i++){
                var imsi = imsis[i];
                if(imsi.activationStatus == 'INACTIVE'){
                    imsiModalObj.imsis.push(imsi);
                }
            }
            formMessage.scope({});
            formMessage.message( false, "");

            if (attrs)
                modalDetails.attributes(attrs);
            modalDetails.scope(imsiModalObj, false);



            modalDetails.submit(function (switchImsiForm) {
                $scope.imsiModalObj.switchImsiErrMsg = '';

                if ( typeof imsiModalObj.imsi == "undefined" || typeof imsiModalObj.imsi.value == "undefined" ) {

                    if( imsiModalObj.imsis.length > 0 ) {
                        $scope.imsiModalObj.switchImsiErrMsg = "Please select a valid IMSI!";
                    }
                    return false;
                }

                else {
                    if($scope.isFormValid(switchImsiForm,imsiModalObj.imsi,true)) {
                        submit(imsiModalObj.imsi);
                        imsiModalObj.imsi = {};
                        angular.element('#' + $scope.imsiModalObj.modal.id).modal('hide');
                    }
                }

            });
        }

        function removeData(){
            $scope.imsiModalObj.imsi = {};
            $scope.imsiModalObj.switchImsiErrMsg = '';
        }

       activate();

    }
})();
