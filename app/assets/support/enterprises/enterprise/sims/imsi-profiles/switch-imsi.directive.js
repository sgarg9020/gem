'use strict';

/* Directive */
(function(){
	angular.module('app')
		.directive('gsSwitchImsi',SwitchImsi);

	SwitchImsi.$inject = ['formMessage'];
	
	/**
		@function updateGroup
		@param {object} formMessage - Factory object
	*/		
	function SwitchImsi(formMessage) {
		
		var link = {
			link: link
		};
		
		return link;
		
		/**
			@function link()
			@param {object} $scope - directive $scope
			@param {object} element - directive HTML element
			@param {object} attrs - attributes on the directive HTML element	
		*/
		function link($scope, element, attrs) {
			$scope.$parent.imsiModalObj.switchImsiModalMessage = setMessage;
			
			/**
				@function setMessage()
				@param {string} msg - message to use for formMessage factory
				@param {string} className - the class name to use for the formMessage
			*/
			function setMessage(msg, className){
				/* Enable a formMessage factory */
				formMessage.scope($scope.imsiModalObj);
				formMessage.className(className);
				formMessage.message(true, msg);
			}
		}
	}
})();

/* Directive to reset the previously selected imsi when invalid imsi name is entered */
(function(){
	angular.module('app').directive('gsResetSelectedImsi', function() {
			return {
				restrict: 'AE',
				link:function ($scope, element, attrs) {

					console.log( element );
					//scope.selectObj.selected = true;
					//scope.selectObj.open = true;
					if (typeof $scope.$parent.imsiModalObj.imsi != "undefined") delete $scope.$parent.imsiModalObj.imsi;
				}

			};
		});
})();
