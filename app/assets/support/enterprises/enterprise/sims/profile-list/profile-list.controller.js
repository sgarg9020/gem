'use strict';

(function () {
    angular.module('app')
        .controller('ProfileListCtrl', ProfileList);

    ProfileList.$inject = ['$scope', '$rootScope', '$stateParams', 'ProfileService', 'commonUtilityService', '$state', 'AuthenticationService'];

    /**
     * @param $scope
     * @param ProfileService
     * @param commonUtilityService
     * @param $stateParams
     */
    function ProfileList($scope, $rootScope, $stateParams, ProfileService, commonUtilityService, $state, AuthenticationService) {
        var profileListVM = this;
        
        profileListVM.getInstalledProfilesOfAnEid = getInstalledProfilesOfAnEid;
        profileListVM.canUpdateStatus = canUpdateStatus;
        profileListVM.updateStatus = updateStatus;

        profileListVM.statusToNameEnum = {
            'INSTALLED_ENABLED':  'Disable',
            'INSTALLED_DISABLED': 'Enable'
        };

        var statusToServiceEnum = {
            'INSTALLED_ENABLED':  ProfileService.disableProfile,
            'INSTALLED_DISABLED': ProfileService.enableProfile
        };

        /**
         * @function activate
         * @description All the activation code goes here.
         */
        function activate() {
            $scope.simId = $stateParams.iccId;
            $scope.enterpriseId = $stateParams.accountId;
            getInstalledProfilesOfAnEid();
        }

        function getInstalledProfilesOfAnEid() {
            ProfileService.getEuiccProfilesOfAnEid($scope.enterpriseId, $scope.simId).success(function (response) {
                profileListVM.installedProfiles = response.list;

                for(var i=0; i<profileListVM.installedProfiles.length; i++){
                    var profile = profileListVM.installedProfiles[i];
                    profile.displayableProfileType = ProfileService.getDisplayableProfileType(profile.profileType);
                    profile.displayableDeploymentStatus = ProfileService.getDisplayableDeploymentStatus(profile.deploymentStatus);
                }
            }).error(function (data) {
                if(data.errorInt != 100000){
                    commonUtilityService.showErrorNotification(data.errorStr);
                }
            });
        }

        function canUpdateStatus() {
            return AuthenticationService.isOperationAllowed(['PROFILE_STATUS_EDIT']);
        }
        
        function updateStatus(profile) {
            var operationName = profileListVM.statusToNameEnum[profile.deploymentStatus];
            var msg = "Are you sure you want to " + operationName + " the profile?";
            var attr = {
                header: operationName + ' Profile',
                cancel: 'Cancel',
                submit: 'Confirm'
            };
            var func = function(){
                var request = statusToServiceEnum[profile.deploymentStatus];
                request($scope.enterpriseId, $scope.simId, profile.iccid).success(function (response) {
                    commonUtilityService.showSuccessNotification(operationName + ' Profile operation has been triggered successfully.');
                    $rootScope.$broadcast('RefreshSIMOperationsHistory');
                }).error(function (data) {
                    commonUtilityService.showErrorNotification(data.errorStr);
                });
            };

            $scope.verificationModalObj.updateVerificationModel(attr,func);
            $scope.verificationModalObj.addVerificationModelMessage(msg);

            $('#verificationModal').modal('show');
        }

        $scope.navigateToProfileDetails = function(iccid){
            $state.go('support.euiccProfile.profile-details',{accountId: $scope.enterpriseId, iccid: iccid});
        }

        activate();

    }
})();
