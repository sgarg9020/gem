"use strict";

(function(){

    angular.module('app')
        .controller('EntSIMAssociationStatusCtrl', EntSIMAssociationStatusCtrl);

    EntSIMAssociationStatusCtrl.$inject=['$scope','loadingState','dataTableConfigService','$compile','roamingProfileService','$stateParams','commonUtilityService','$location','$sce','$rootScope','$q','tooltips','simSessionService'];

    /**
     @constructor EntSIMAssociationStatusCtrl()
     */
    function EntSIMAssociationStatusCtrl($scope,loadingState,dataTableConfigService,$compile,roamingProfileService,$stateParams,commonUtilityService,$location,$sce,$rootScope,$q,tooltips,simSessionService){

        var entSIMssociationStatus = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            clearDataTableStatus(['DataTables_entSIMAssociationStatusDataTable_']);
            tooltips.initTooltips('[data-toggle="tooltip"]');

            $scope.enterpriseId = $stateParams.accountId;
            $scope.statusId = $stateParams.statusId;

            entSIMssociationStatus.refreshEntSIMAssocStatusDataTable  = refreshEntSIMAssocStatusDataTable;
            entSIMssociationStatus.trustAsHtml = function (value){return $sce.trustAsHtml(value);};


            $scope.changeStatusFilters = [
                {
                    name:'ALL',
                    dbRef:'',
                    skipQueryParam:true
                },
                {
                    name:'COMPLETE',
                    dbRef:'COMPLETED'
                },
                {
                    name:'PENDING',
                    dbRef:'IN_PROGRESS'
                },
                {
                    name:'FAILED',
                    dbRef:'ERROR'
                },
                {
                    name:'SKIPPED',
                    dbRef:'SKIPPED'
                }
            ];

            $scope.emptyTableMessage = "No sims available.";
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = "entSIMAssociationStatusDataTable";
            $scope.dataTableObj.tableHeaders = [
                {value: 'CID', DbValue: "cId",search:true,placeHolder:'CID'},
                {value: 'SIMID', DbValue: "simId",search:true,placeHolder:'simId'},
                {value: "Status", DbValue: "status"}
            ];

            var filtersConfig = {status: {type:'url', defaultValue: $scope.changeStatusFilters[0] }};
            dataTableConfigService.configureFilters($scope.dataTableObj, filtersConfig);

            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

        }

        activate();

        function _setDataTableOptions(){
            return {
                ordering: false,
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'SID_'+data.simId);
                    $compile(row)($scope);
                },
                columns: [
                    { orderable:false},
                    { orderable:false},
                    { orderable:false}
                ],
                columnDefs: [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return '<span>'+row.cId+'</span>';
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return '<span>'+row.simId+'</span>';
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {

                            var status = roamingProfileService.getStatusClassMap(row.status);
                            return (row.status == 'ERROR')?  '<span class="cursor-pointer ds-inline RP-association-status '+status.class+'" gs-tooltip data-toggle="tooltip" data-placement="top" title="'+row.errorSimInfo.errorMessage+'">'+status.name+'</span>'
                                :'<span class="RP-association-status '+status.class+'">'+status.name+'</span>';
                        }
                    }
                ],
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords:$scope.emptyTableMessage
                },
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {
                    if(aoData){
                        entSIMssociationStatus.searchValue = (aoData[5])?aoData[5].value.value:'';
                    }

                    var simQueryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    $scope.isPageSizeFilterDisabled = false;
                    simSessionService.getDetailsOfSimOperationSession($scope.enterpriseId,$scope.statusId,simQueryParam).success(function (response) {

                        //data preparations for association updated sims
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response);
                        datTabData.aaData = response.simOperationStatusInfoList;
                        $scope.assocSimStatusList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.assocSimStatusList.length == 0);

                        //data preparations for the association status details
                        entSIMssociationStatus.operationType = response.operationType;
                        var targetRoamingProfile = response.roamingProfile;
                        if(targetRoamingProfile){
                            entSIMssociationStatus.targetRoamingProfileName = targetRoamingProfile.roamingProfileName;
                        }
                        entSIMssociationStatus.errorMessage = response.errorMessage;

                         var status = roamingProfileService.getDisplayStatusOfSIMAssociation(response.status,response.simStatusCount );
                        entSIMssociationStatus.sessionStatus = status.name;

                        entSIMssociationStatus.totalCompletedSimCount = (response.simStatusCount)?response.simStatusCount.COMPLETED:0;
                        entSIMssociationStatus.totalSimCount = response.simsCount;

                        var userDetails = response.userDetails;
                        entSIMssociationStatus.changedBy = ( userDetails)? (userDetails.emailId)?userDetails.emailId:'':'';

                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }
                    }).error(function (data) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.simMapHistList ||  $scope.simMapHistList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"SIM Mapping history");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });
                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.assocSimStatusList )
                        dataTableConfigService.disableTableHeaderOps( $scope.assocSimStatusList.length, oSettings );

                }

            }
        }

        function refreshEntSIMAssocStatusDataTable(){
            commonUtilityService.showSuccessNotification("SIM status refresh has been triggered.");
            $scope.dataTableObj.refreshDataTable(true);
        }

        $scope.$on('$destroy', function () { tooltips.destroyTooltips('[data-toggle="tooltip"]'); });

    }
})();
