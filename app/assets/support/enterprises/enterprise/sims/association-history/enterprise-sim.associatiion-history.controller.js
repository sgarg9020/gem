"use strict";

(function(){

    angular.module('app')
        .controller('SimAssociationHistoryController', SimAssociationHistoryController);

    SimAssociationHistoryController.$inject=['$scope','loadingState','$stateParams','$compile','dataTableConfigService','roamingProfileService','commonUtilityService','$timeout','simSessionService'];

    /**
     @constructor SimAssociationHistoryController()
     */
    function SimAssociationHistoryController($scope,loadingState,$stateParams,$compile,dataTableConfigService,roamingProfileService,commonUtilityService,$timeout,simSessionService){

        var simAssociationHistory = this;

        /**
         * Start up logic for controller
         */
        function activate(){

            $scope.enterpriseId = $stateParams.accountId;
            simAssociationHistory.refreshRPSimAssocHistTable = refreshRPSimAssocHistTable;

            $scope.emptyTableMessage = "No SIM history available.";
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = "simAssociationHistoryTable";
            $scope.dataTableObj.tableHeaders = [
                {value: "Operation Type", DbValue: "operationType"},
                {value: "Date (UTC)", DbValue: "createdTime"},
                {value: "Target Roaming profile", DbValue: "roamingProfileName"},
                {value: "SIMs", DbValue: "simsCount"},
                {value: "Status", DbValue: "status"},
                {value: "Completed SIMs", DbValue: ""}
            ];
            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

        }

        activate();

        function _setDataTableOptions(){
            return {
                bSort: false,

                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'SID_'+data.sessionId);
                    $compile(row)($scope);
                },
                columns: [
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                ],
                columnDefs: [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return '<span>'+row.operationType+'</span>';
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return '<span>'+row.createdTime+'</span>';
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            var roamingProfileName = (row.roamingProfile)?row.roamingProfile.roamingProfileName:'NA';
                            return '<span>'+roamingProfileName+'</span>';
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return '<span>'+row.simsCount+'</span>';
                        }
                    },
                    {
                        targets: 4,
                        render:function (data, type, row){
                            var status = roamingProfileService.getDisplayStatusOfSIMAssociation(row.status,row.simStatusCount );
                            // if(status.name == 'Pending'){
                            //     return '<span class="RP-association-status '+status.class+'"><a ng-click=\"entRPAssociationHist.onPendingSessionCancelled()\" class="cursor">'+status.name+'</a></span>';
                            // }
                            return '<span class="RP-association-status '+status.class+'"><a ui-sref="support.enterprise.sim.status({accountId:\''+$scope.enterpriseId+'\',statusId:\''+row.sessionId+'\'})" class="cursor">'+status.name+'</a></span>';
                        }
                    },
                    {
                        targets: 5,
                        render:function (data, type, row){
                            var completedSimCount = (row.simStatusCount)?row.simStatusCount.COMPLETED:'Undefined';
                            return '<span>'+completedSimCount+'</span>';
                        }
                    }
                ],
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords:$scope.emptyTableMessage
                },
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {

                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    $scope.isPageSizeFilterDisabled = false;

                    simSessionService.getSimOperationSession($scope.enterpriseId,queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response);
                        $scope.simMapHistList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.simMapHistList.length == 0);
                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }
                    }).error(function (data) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.simMapHistList ||  $scope.simMapHistList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"SIM Mapping history");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });

                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.simMapHistList )
                        dataTableConfigService.disableTableHeaderOps( $scope.simMapHistList.length, oSettings );

                }

            }
        }


        function refreshRPSimAssocHistTable(notify){
            if(notify == 'showMsg')
                commonUtilityService.showSuccessNotification("SIM association history refresh has been triggered.");
            $scope.dataTableObj.refreshDataTable(false);
        }




    }
})();
