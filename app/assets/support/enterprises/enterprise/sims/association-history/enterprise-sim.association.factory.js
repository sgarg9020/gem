'use strict';

(function(){
    angular
        .module('app')
        .factory('simSessionService', SimSessionService);

    SimSessionService.$inject = ["commonUtilityService"];

    /**
     @function AlertsService()
     */
    function SimSessionService(commonUtilityService) {

        return {
            getSimOperationSession: getSimOperationSession,
            getDisplayStatusOfSIMOperation:getDisplayStatusOfSIMOperation,
            getDetailsOfSimOperationSession:getDetailsOfSimOperationSession
        };

        function getSimOperationSession(enterpriseId, queryParams) {

            var url = ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/" + enterpriseId + '/sims/operation/session';
            var headerAdd = ['PROVISIONING_SIMS_GET_OPERATION_SESSION'];

            return commonUtilityService.prepareHttpRequest(url, 'GET', headerAdd, queryParams);
        }
        function getDisplayStatusOfSIMOperation(status, simCountObj){

            status = (status == 'ERROR' && simCountObj && simCountObj.COMPLETED)?'PARTIAL':status;

            return getStatusClassMap(status);

        }
        function getDetailsOfSimOperationSession(enterpriseId,statusId, queryParams) {
            var url = ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/" + enterpriseId + '/sims/operation/session/' + statusId;
            var headerAdd = ['PROVISIONING_SIMS_GET_OPERATION_SESSION'];

            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd, queryParams);
        }

    }
 })();
