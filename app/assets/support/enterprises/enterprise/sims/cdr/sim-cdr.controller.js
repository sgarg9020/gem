'use strict';

(function () {
    angular.module('app')
        .controller('GapCallDateRecordsCtrl', CDR);

    CDR.$inject = ['$scope', '$stateParams', '$compile', 'SIMService', 'dataTableConfigService', 'commonUtilityService','tooltips','$filter'];

    /**
     * @param $scope
     * @param $stateParams
     * @param $compile
     * @param SIMService
     * @param dataTableConfigService
     * @param commonUtilityService
     * @param tooltips
     * @constructor CDR
     */
    function CDR($scope, $stateParams, $compile, SIMService, dataTableConfigService, commonUtilityService,tooltips,$filter) {
        var cdrVm = this;

        /**
         * @function activate
         * @description All the activation code goes here.
         */
        function activate() {
            $scope.emptyTableMessage = "No CDRs available.";
            $scope.dataTableObj = {};
            cdrVm.accountId = $stateParams.accountId;
            cdrVm.simId = $stateParams.iccId;

            $scope.dataTableObj.controlID = 'datatable-controls-cdr-list';
            $scope.dataTableObj.tableID = 'gapCdrListDataTable';
            $scope.dataTableObj.searchValue = '';
            var d = new Date();
            var year = d.getFullYear();
            var month = d.getMonth();
            var day = d.getDate();
            var startDate = encodeURIComponent($filter('date')(new Date(year - 10, month, day),'dd-MM-yyyy HH:mm:ss'));
            var endDate = encodeURIComponent($filter('date')(new Date(year+1, month, day),'dd-MM-yyyy HH:mm:ss'));
            cdrVm.exportData={
                url: SIMService.getExportCDRdataUrl(cdrVm.accountId,cdrVm.simId,startDate,endDate)
            };

            $scope.dataTableObj.tableHeaders = [
                {"value": "Date (UTC)", "DbValue": "recordStartTime"},
                {"value": "Usage", "DbValue": "dataInBytes"},
                {"value": "Carrier", "DbValue": "carrierName"},
                {"value": "MCC-MNC", "DbValue": "mcc-mnc"},
                {"value": "Country", "DbValue": "countryList"},
                {"value": "IMSI Provider", "DbValue": "imsiProfile"},
                {"value": "IMSI", "DbValue": "imsi"},
                {"value": "MSISDN", "DbValue": "msisdn"}
            ];
            $scope.dataTableObj.dataTableOptions = {
                "createdRow": function (row, data, index) {
                    $compile(row)($scope);
                },
                bSort : false,
                "columnDefs": [
                    {
                        "targets": 0,
                        "render": function (data, type, row, meta) {
                            return "<span class='nowrap'>" + row.recordStartTime + "</span>";
                        }
                    },
                    {
                        "targets": 1,
                        "render": function (data, type, row, meta) {
                            // Always show in MB
                            var dataUsed= commonUtilityService.calculateByteRange(row.dataInBytes, "MB");
                            var dataUsedFormatted = commonUtilityService.calculateByteRange(row.dataInBytes);
                            return '<span class="cursor-pointer font-inconsolata text-right w-60 ds-inline" data-toggle="tooltip" data-placement="top" title="'+dataUsedFormatted.data+' '+dataUsedFormatted.measure+'">'+dataUsed.data+'</span>';
                        }
                    },
                    {
                        "targets": 2,
                        "render": function (data, type, row, meta) {
                            var carrierName = row.carrierName ? row.carrierName : 'Not Available';
                            return "<span class='nowrap'>" + carrierName + "</span>";
                        }
                    }, {
                        "targets": 3,
                        "render": function (data, type, row, meta) {
                            if (row.mcc && row.mnc)
                                return '<span class="status-text nowrap">' + row.mcc + '-' + row.mnc + '</span>';
                            return '<span class="status-text nowrap">Not Available</span>';
                        }
                    },
                    {
                        "targets": 4,
                        "render": function (data, type, row, meta) {
                            return "<span class='nowrap'>" + commonUtilityService.getCountriesString(row.countryList) + "</span>";
                        }
                    },
                    {
                        "targets": 5,
                        "data": "imsiProfile",
                        "defaultContent": "Not Available"
                    },
                    {
                        "targets": 6,
                        "data": "imsi",
                        "defaultContent": "Not Available"
                    },
                    {
                        "targets": 7,
                        "data": "msisdn",
                        "defaultContent": "Not Available"
                    }
                ],
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ",
                    "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    "sEmptyTable": $scope.emptyTableMessage,
                    "sZeroRecords": $scope.emptyTableMessage
                },
                "processing": true,
                fnServerData: function (sSource, aoData, fnCallback) {
                    tooltips.destroyTooltips('[data-toggle="tooltip"]');

                    if(aoData){
                        cdrVm.searchValue = aoData[5].value.value;
                    }

                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    $scope.isPageSizeFilterDisabled = false;

                    cdrVm.exportData.startIndex = queryParam.startIndex;
                    cdrVm.exportData.count = queryParam.count;


                    SIMService.getSIMCdrs(cdrVm.accountId, cdrVm.simId, queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response, oSettings, $scope.emptyTableMessage);
                        $scope.cdrs = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ($scope.cdrs.length == 0);
                        fnCallback(datTabData);
                        tooltips.initTooltips('[data-toggle="tooltip"]');
                    }).error(function (data, status) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.cdrs ||  $scope.cdrs.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings, fnCallback, "CDRs");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });

                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.cdrs )
                        dataTableConfigService.disableTableHeaderOps( $scope.cdrs.length, oSettings );

                }
            };
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

            $scope.$on('$destroy', function () { tooltips.destroyTooltips('[data-toggle="tooltip"]'); });
        }

        activate();
    }
})();
