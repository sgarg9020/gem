'use strict';

/* Controllers */
(function(){
    angular.module('app')
        .controller('BulkUploadSimCtrl', BulkUploadSims);

    BulkUploadSims.$inject=['$scope','$rootScope', 'modalDetails', 'analytics','$stateParams','roamingProfileService','$sce', 'formMessage','commonUtilityService'];

    /**
     * @func BulkUploadSims()
     * @param $scope
     * @param $rootScope
     * @param modalDetails
     * @param analytics
     * @constructor
     */
    function BulkUploadSims($scope,$rootScope, modalDetails, analytics,$stateParams,roamingProfileService,$sce,formMessage,commonUtilityService){
        var bulkUploadSims = this;

        /**
         * Start up logic for controller
         */
        function activate(){

            Dropzone.autoDiscover = false;
            $scope.enterpriseId = $stateParams.accountId;
            bulkUploadSims.refreshDatatable = $scope.$parent.dataTableObj.refreshDataTable;
            $scope.url = ENTERPRISE_GIGSKY_BACKEND_BASE+"accounts/account/" + $scope.enterpriseId + "/sims/upload";
            $scope.$parent.entAssociateSIMModalObj.updateEntAssociationSIMModal = updateModal;
            $scope.dropzoneConfig = {
                autoProcessQueue: false,
                maxFiles: 1,
                parallelUploads: 1,
                maxFilesize: 25,
                addRemoveLinks: false,
                clickable: true,
                acceptedFiles: ".csv",
                headers:{ "token" : sessionStorage.getItem( "token")},
                dictInvalidFileType:'File type not supported'
            };
            /* ModalDetails factory */
            modalDetails.scope(bulkUploadSims, true);
            modalDetails.attributes({
                id: 'bulkUploadSimsModal',
                formName: 'file',
                cancel: 'Clear',
                submit: 'Upload'
            });
            modalDetails.cancel(resetFiles);
            modalDetails.submit(attemptToUpload);
            bulkUploadSims.cancelUpload = cancelUpload;
        }

        /**
         @function reset()
         - call a directive level function
         */

        function resetFiles() {
            $rootScope.$broadcast('clearDropzoneFiles');
        }

        function reset(){
            bulkUploadSims.roamingProfile = {};
            $scope.addSimForm.roamingProfileVersion.errorStr = '';
            $scope.addSimForm.roamingProfileSet.errorStr = '';
            $rootScope.$broadcast('clearDropzoneFiles');
        }

        function updateModal() {
            $scope.isRPVersionSelectDisabled = true;
            reset();
        }

        /**
         @function attemptToUpload()
         - call a directive level function
         */
        function attemptToUpload(){
            if($scope.isFormValid($scope.addSimForm,null,true)) {
                $scope.dropzoneConfig.url = $scope.url + '?' +  'roamingProfileId=' +  bulkUploadSims.roamingProfile.version.roamingProfileId;
                $rootScope.$broadcast('submitDropzoneFiles');
            }

        }
        var rpQueryParam = {startIndex: 0, count: 10000};
        roamingProfileService.getRPAssociatedToEnterprise($scope.enterpriseId,rpQueryParam).success(function (response) {

            if(response.list.length==0) {
                bulkUploadSims.errorMessage = "No roaming profiles associated to this account";
                $scope.isRPSetSelectDisabled = true;
            } else {
                prepareRPSelectDropDownList(response.list);
            }

        }).error(function (response) {
            console.log(response.errorStr);
            commonUtilityService.showErrorNotification(response.errorStr);
        });
        //enabling Dropdown
        bulkUploadSims.trustAsHtml = function (value){return $sce.trustAsHtml(value);};
        function prepareRPSelectDropDownList(associatedRPDetails){
            bulkUploadSims.rpDetails ={ sets: []};
            var rpSetIdPool = {};
            var poolIndex = 0;

            for(var i=0; i<associatedRPDetails.length; i++){

                var rpDetails = associatedRPDetails[i];

                if(rpSetIdPool[rpDetails.roamingProfileSetId] !== undefined){
                    bulkUploadSims.rpDetails.sets[rpSetIdPool[rpDetails.roamingProfileSetId]].localVersionList.push(rpDetails);
                }else{
                    bulkUploadSims.rpDetails.sets.push({setName:rpDetails.roamingProfileSetName, setId:rpDetails.roamingProfileSetId, localVersionList:[rpDetails]});
                    rpSetIdPool[rpDetails.roamingProfileSetId] = poolIndex++;
                }
            }

        }
        //enanling version select
        bulkUploadSims.enableRPVersionSelect = function(){

            if(bulkUploadSims.roamingProfile.set.localVersionList.length==0) {
                $scope.isRPVersionSelectDisabled = true;
                bulkUploadSims.errorMessage = "No roaming profiles associated to this account";
            } else {
                $scope.isRPVersionSelectDisabled = false;
                bulkUploadSims.rpVersionList = bulkUploadSims.roamingProfile.set.localVersionList;
                bulkUploadSims.roamingProfile.version = null;
            }

        };
        /**
         @function attemptToUpload()
         - call a directive level function
         */
        function cancelUpload(){
            $rootScope.$broadcast('cancelDropzoneUpload');
        }

        activate();

    }
})();
