'use strict';

(function () {
    angular.module('app')
        .controller('OTAHistoryCtrl', ['$rootScope','$scope', 'commonUtilityService', '$stateParams', 'dataTableConfigService', '$compile', 'SIMService', 'tooltips', OTAHistoryCtrl]);

    function OTAHistoryCtrl($rootScope,$scope, commonUtilityService, $stateParams, dataTableConfigService, $compile, SIMService, tooltips) {
        var OTAHistory = this;
        var refreshRequired = false;
        var switchImsiListener;

        function activate() {
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = 'OTAHistoryDataTable';
            OTAHistory.accountId = $stateParams.accountId;
            OTAHistory.simId = $stateParams.iccId;

            $scope.dataTableObj.tableHeaders = [
                {"value": "Initiator", "DbValue": "initiator"},
                {"value": "Target IMSI Profile", "DbValue": "targetImsiInfo"},
                {"value": "Active IMSI Profile", "DbValue": "causeImsiInfo"},
                {"value": "Country", "DbValue": "zoneInfo"},
                {"value": "Status", "DbValue": "carrierName"},
                {"value": "Request reason", "DbValue": "requestReason"},
                {"value": "Request time(UTC)", "DbValue": "requestTime"},
                {"value": "Schedule time(UTC)", "DbValue": "scheduledTime"},
                {"value": "activation time(UTC)", "DbValue": "actuationTime"}
            ];
            $scope.dataTableObj.dataTableOptions = {
                bSort: false,
                columnDefs: [
                    {
                        "targets": 0,
                        "render": function (data, type, row) {
                            return '<span>' + row.initiator + '</span>';
                        }
                    },
                    {
                        "targets": 1,
                        "render": function (data, type, row) {
                            return '<span data-toggle="tooltip" data-placement="top" title="'+row.targetImsiInfo.imsi+'">' + row.targetImsiInfo.imsiProfile + '</span>';
                        }
                    },
                    {
                        "targets": 2,
                        "render": function (data, type, row) {
                            return '<span data-toggle="tooltip" data-placement="top" title="'+row.causeImsiInfo.imsi+'">' + row.causeImsiInfo.imsiProfile  + '</span>';
                        }
                    },
                    {
                        "targets": 3,
                        "render": function (data, type, row) {
                            if (row.causeImsiInfo && row.causeImsiInfo.countryList) {
                                var location = '';
                                for (var i = 0; i < row.causeImsiInfo.countryList.length; i++) {
                                    if (i > 0)
                                        location = location + " , ";
                                    location = location + row.causeImsiInfo.countryList[i].countryName;
                                }

                                var zoneName = (row.causeImsiInfo.zoneInfo)? row.causeImsiInfo.zoneInfo.name : 'Zone Info Not Available';
                                return '<span class="nowrap" data-toggle="tooltip" data-placement="top" title="'+zoneName+'">' + location + '</span>';
                            }

                            return "<span class='status-text nowrap'>Not Available</span>";
                        }
                    },
                    {
                        "targets": 4,
                        "render": function (data, type, row) {
                            var statusColor = ((row.status  == "SCHEDULED") || (row.status  == "PENDING_DELIVERY_RECEIPT")) ? "yellow" : ((row.status  == "DELIVERED") || (row.status  == "ACTUATED")) ? "green" : "red";
                            var status = (row.status  == 'ACTUATED') ? 'ACTIVATED' : row.status;
                            return '<span class="status-text '+statusColor+'">' + status + '</span>';
                        }
                    },
                    {
                        "targets": 5,
                        "render": function (data, type, row) {
                            if(row.requestReason == 'EXTERNAL'){
                                var title = "<b>" + row.gapUserEmail + "</b><br/> Desc: " + row.gapUserDescription;
                                return '<span class="status-text nowrap" style="text-transform:none;" data-toggle="tooltip" data-html="true" data-placement="top" title="'+title+'">' + "USER " + '<span class="small hint-text">(hover for details)</span>'+'</span>';
                            }
                            else
                                return '<span class="status-text">' + row.requestReason + '</span>';
                        }
                    },
                    {
                        "targets": 6,
                        "render": function (data, type, row) {
                            return '<span class="nowrap">' + row.requestTime + '</span>';
                        }
                    },
                    {
                        "targets": 7,
                        "render": function (data, type, row) {
                            var scheduledTime = row.scheduledTime ? row.scheduledTime : 'Not Available';
                            return "<span class='nowrap'>" + scheduledTime + "</span>";
                        }
                    },
                    {
                        "targets": 8,
                        "render": function (data, type, row) {
                            var actuationTime;
                            for(var i in row.attempts){
                                var attempt = row.attempts[i];
                                if(attempt.actuationStatus == 'SUCCESSFUL'){
                                    actuationTime = attempt.actuationTime;
                                    break;
                                }
                            }

                            actuationTime = actuationTime ? actuationTime : 'Not Available';
                            return "<span class='nowrap'>" + actuationTime + "</span>";
                        }
                    }
                ],
                "createdRow": function (row, data, index) {
                    $compile(row)($scope);
                },
                "fnServerData": function (sSource, aoData, fnCallback) {
                    tooltips.destroyTooltips('[data-toggle="tooltip"]');
                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);

                    queryParam.includeLocations = false;
                    $scope.isPageSizeFilterDisabled = false;
                    SIMService.getSIMOTAHistory(OTAHistory.accountId, OTAHistory.simId, queryParam).success(function (response) {
                        OTAHistory.simType = response.simType;
                        var datTabData = dataTableConfigService.prepareResponseData(response);
                        $scope.OTAHistoryList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.OTAHistoryList.length == 0);
                        fnCallback(datTabData);
                        tooltips.initTooltips('[data-toggle="tooltip"]');
                    }).error(function (data, status) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.OTAHistoryList ||  $scope.OTAHistoryList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"ota history");
                        commonUtilityService.showErrorNotification(data.errorStr,data.errorInt);
                    });

                },
                "fnDrawCallback": function (oSettings) {
                    if($scope.OTAHistoryList )
                        dataTableConfigService.disableTableHeaderOps( $scope.OTAHistoryList.length, oSettings );
                },
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ",
                    "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    "sEmptyTable": "No Ota history available.",
                    "sZeroRecords": "No Ota history available."
                }
            };


            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

        }

        switchImsiListener = $rootScope.$on('onSwitchImsi', function(event,data) {
            $scope.dataTableObj.refreshDataTable(false);
        });

        $scope.$on('$destroy', function () {
            tooltips.destroyTooltips('[data-toggle="tooltip"]');

            //unregistering listener
            if(switchImsiListener)
                switchImsiListener();
        });

        activate();

    }
})();
