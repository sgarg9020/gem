/* Controllers */
(function(){
    "use strict";

    angular.module('app')
        .controller('EnterpriseCtrl', Enterprise);

    Enterprise.$inject=['$scope','$stateParams','SettingsService','$rootScope','commonUtilityService'];

    /**
     @constructor Enterprise()
     */
    function Enterprise($scope,$stateParams,SettingsService,$rootScope,commonUtilityService){

        var enterpriseVm = this;

        /**
         * Start up logic for controller
         */
        function activate(){

            $scope.enterpriseId = $stateParams.accountId;
            $scope.shortInfoScreen = false;
            if($scope.resellerOrSubAccount){
                if(sessionStorage.getItem("isSupport") != "true"){
                    $scope.shortInfoScreen = true;
                } else if(sessionStorage.getItem("isNavigatedThroughAdmin") == "true"){
                    $scope.shortInfoScreen = true;
                }
            }

            SettingsService.getEnterpriseDetails($scope.enterpriseId).success(function (response) {
                $rootScope.currentAccountName = response.accountName;
                commonUtilityService.setSubscriptionTypeInSessionStorage(response.accountSubscriptionType);
                $scope.setHierarchicalData(response.accountType == RESELLER_ACCOUNT_TYPE);
            }).error(function (data) {
                console.log(data.errorStr);
            });

        }

        activate();

    }
})();
