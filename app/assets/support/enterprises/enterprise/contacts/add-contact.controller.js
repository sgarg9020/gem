'use strict';

/* Controllers */
(function(){
	angular.module('app')
		.controller('AddContactCtrl', AddContact);

	AddContact.$inject=['$scope', '$http', '$sce', 'modalDetails', 'notifications','formMessage'];

	/**
	 @constructor AddContact()
	 @param {object} $scope - Angular.js $scope
	 @param {object} $http - Angular.js $http service
	 @param {object} $sce - Angular.js strict contextual escaping service
	 @param {object} modalDetails - Factory object
	 */
	function AddContact($scope, $http, $sce, modalDetails,notifications,formMessage){

		var addContact = this;

		/**
		 * Start up logic for controller
		 */
		function activate(){
			/* Form elements object */
			addContact.contact = {};
			$scope.generalObj = {};

			/* ModalDetails factory */
			modalDetails.scope(addContact, true);
			modalDetails.attributes({
				id: 'addOrEditContactModal',
				formName: 'addOrEditConatct',
				cancel: 'Cancel',
				submit: 'Add contact'
			});
			modalDetails.cancel(remove);

			addContact.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};
			$scope.$parent.contactModalObj.updateContactModal = updateModal; /* Attach to parent */
			$scope.$parent.contactModalObj.resetContactModal = removeData;
		}

		function hideModal() {
			angular.element('#' + $scope.addContact.modal.id).modal('hide');
		}

		/**
		 @function updateModal()
		 @param {object} contact - optional - contact's data to auto populate the form
		 @param {object} obj - object to change modal attributes in directive
		 @param {function} submit - function to execute on submit
		 - Allow a directive to pass data for the modal to update
		 */
		function updateModal(contact, obj, submit){
			//clear old form data
			addContact.contact = {};
			addContact.errorMessage = '';
			modalDetails.scope(addContact, false);
			modalDetails.attributes(obj);

			//Update the form data
			if(contact){
				updateData(contact);
				var expando = contact.$$hashKey;
				$scope.generalObj.previousData = angular.copy(addContact.contact);
				modalDetails.submit(function(addContactForm){
					addContact.errorMessage = '';
					if($scope.isFormValid(addContactForm,addContact.contact,true)) {
						submit(addContact.contact, expando);
						hideModal();
					}else if($scope.generalObj.isNoChangeError){
						addContact.errorMessage = "There is no change in the data to update.";
						$scope.generalObj.isNoChangeError = false;
					}
				});
			}
			else {
				$scope.generalObj.previousData = null;
				modalDetails.submit(function(addContactForm){
					addContact.errorMessage = '';
					if($scope.isFormValid(addContactForm,addContact.contact,true)) {
						submit(addContact.contact);
						hideModal();
					}

				});
			}

		}

		function removeData(){
			$scope.addContact.contact ={phoneNumber:'',emailId:''};
			formMessage.scope($scope.addContact);
			formMessage.message(false, "");
		}

		/**
		 @function updateData()
		 @param {object} contact - contacts data to add to the form
		 */
		function updateData(contact) {
			if (contact.contactId) {
				$scope.addContact.contact.contactId = contact.contactId;
			}

			if (contact.firstName) {
				$scope.addContact.contact.firstName = contact.firstName;
			}

			if (contact.lastName) {
				$scope.addContact.contact.lastName = contact.lastName;
			}

			if (contact.emailId) {
				$scope.addContact.contact.emailId = contact.emailId;
			}
			if(contact.jobTitle){
				$scope.addContact.contact.jobTitle = contact.jobTitle;
			}
			$scope.addContact.contact.phoneNumber = contact.phoneNumber;
		}




		/**
		 @function remove()
		 - call a directive level function
		 */
		function remove(){
			removeData();
		}

		activate();

	}
})();
