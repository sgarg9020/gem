/* Controllers */
(function(){
    "use strict";

    angular.module('app')
        .controller('ContactsCtrl', Contacts);

    Contacts.$inject=['$scope', '$sce', '$compile', 'dataTableConfigService','commonUtilityService', 'loadingState','$sanitize','SettingsService','$stateParams','AuthenticationService'];

    /**
     @constructor Companies()
     @param {object} $scope - Angular.js $scope provider
     @param {object} $sce - Angular.js $sce provider (Strict Contextual Escaping)
     @param {function} $compile - Angular.js $sanitize provider
     @param {object} dataTableConfigService - Factory Object
     */
    function Contacts($scope, $sce, $compile, dataTableConfigService, commonUtilityService, loadingState, $sanitize, SettingsService, $stateParams, AuthenticationService){

        var contactsVm = this;
        contactsVm.showDataTable = false;

        /**
         * Start up logic for controller
         */
        function activate(){

            $scope.emptyTableMessage = "No contacts available.";

            // Controller As
            contactsVm.trustAsHtml = function (value){return $sce.trustAsHtml(value);};

            // Angular $scope
            $scope.enterpriseId = $stateParams.accountId;
            // DataTable
            $scope.dataTableObj = {};
            $scope.contactModalObj = {};
            $scope.dataTableObj.tableHeaders = [
                {"value": "","DbValue":""},
                {value: "Name", DbValue: "fullName"},
                {"value": "Email","DbValue":"emailId"},
                {value: "Job Title", DbValue: "jobTitle"},
                {value: "Phone Number", DbValue: "phoneNumber"},
            ];
            $scope.dataTableObj.tableID = "contactsDataTable";
            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);
            contactsVm.showDataTable = true;

            contactsVm.addContact = addContactModal;
            contactsVm.verifyDeletion = verifyDeletion;
            setTimeout(function(){
                $scope.dataTableObj.refreshDataTable();
            }, 500);

        }

        activate();

        /**
         * @function _setDataTableOptions()
         * @desc - function to set data table. This keeps the activate() function clean.
         * @private
         */
        function _setDataTableOptions(){
            return {
                bPaginate: false,
                bInfo: false,
                iDeferLoading: 10,         //prevent initial loading
                ordering: false,
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'cust_contact_'+data.contactId);
                    $compile(row)($scope);
                },
                columnDefs: [{
                    targets: 0,
                    className: "checkbox-table-cell",
                    render: function ( data, type, row, meta) {
                        return '<div gs-has-permission="ACCOUNT_EDIT" class="checkbox check-success large no-label-text hideInReadOnlyMode"> <input type="checkbox" value="1" id="checkbox_'+row.contactId+'" gs-index="'+meta.row+'"><label ng-click="checkBoxClicked($event)" id="label_'+row.contactId+'"></label></div>';
                    }
                },{
                    targets: 1,
                    render: function (data, type, row, meta) {
                        var fName = $sanitize(row.firstName);
                        var lName = $sanitize(row.lastName);
                        var permissionList = ['ACCOUNT_EDIT'];
                        var isEditingAllowed = AuthenticationService.isOperationAllowed(permissionList);
                        return isEditingAllowed ? '<a id="edit_contact_'+row.contactId+'" class="nowrap" ng-click="editContact('+meta.row+');" href="javascript:void(0)" title="Click to edit contact '+fName + ' ' + lName+'"><i class="fa fa-pencil edit-ico"></i>'+ ' ' + fName + ' ' + lName+'</a>' : fName + ' ' + lName;;
                    }
                }, {
                    targets: 2,
                    render: function (data, type, row) {
                        return '<span>'+row.emailId+'</span>';
                    }
                }, {
                    targets: 3,
                    render:function (data, type, row){
                        return row.jobTitle ? '<span>'+row.jobTitle+'</span>' : '';
                    }
                }, {
                    targets: 4,
                    render:function (data, type, row){
                        return row.phoneNumber ? '<span>'+row.phoneNumber+'</span>' : '';
                    }
                }],
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords: $scope.emptyTableMessage
                },
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {

                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    SettingsService.getApprovedContacts($scope.enterpriseId,queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
                        $scope.contactsList = datTabData.aaData;
                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }

                    }).error(function (data) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"contacts");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });
                }
            };
        };

        /**
         @function addContactModal()
         - Open the addContactModal
         */
        function addContactModal() {
            /* Attributes for new contact */
            var obj = {
                header: "Add New Contact",
                submit: "Add"
            };
            $scope.contactModalObj.updateContactModal(null, obj, $scope.onAddNewContact);

            $('#addOrEditContactModal').modal('show');
        };

        /**
         @function editContactModal()
         @param {obj} contact - contact object to update
         - Open the editContactModal
         */
        function editContactModal(contact) {
            /* Attributes for existing contact */
            var obj = {
                header: "Update Contact",
                submit: "Update"
            };
            if(!contact.phoneNumber)
                contact.phoneNumber='';
            $scope.contactModalObj.updateContactModal(contact, obj, $scope.onEditExistingContact);

            $('#addOrEditContactModal').modal('show');
        };

        /**
         @function processTableRowAdd()
         @param {obj} contact - contact object
         */
        $scope.onAddNewContact = function(contact) {

            contact.type = "ApprovedContactInfo";
            var list = [ contact ];

            var contactList = { type:"AddOrUpdateApprovedContactRequest", list:list };

            SettingsService.addContact(contactList,$scope.enterpriseId).success(function(){
                $scope.dataTableObj.refreshDataTable(false);
                commonUtilityService.showSuccessNotification("An contact has been created for " + contact.firstName + " " + " " + contact.lastName );
                $scope.contactModalObj.resetContactModal();
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
                $scope.contactModalObj.resetContactModal();

            });

        };

        $scope.editContact = function(index){
            editContactModal($scope.contactsList[index]);
        };

        /**
         @function processTableRowUpdate()
         @param {obj} contact - contact object
         @param {string} expando - expando $$hashKey value
         */
        $scope.onEditExistingContact =function(contact, expando) {

            /* Add the expando back to the contact object */
            contact.$hashKey = expando;
            var contactList = { type:"AddOrUpdateApprovedContactRequest", list: [getContactForUpdate( contact ) ] };

            SettingsService.updateContacts(contactList,$scope.enterpriseId).success(function () {
                $scope.dataTableObj.refreshDataTable(false);
                commonUtilityService.showSuccessNotification("Contact " + contact.firstName + " " + contact.lastName + " details have been updated successfully.");
                $scope.contactModalObj.resetContactModal();
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
                $scope.contactModalObj.resetContactModal();

            });

        };

        function getContactForUpdate(contact) {

            var ref= {};
            ref.type =  ( contact.type !== undefined  ? contact.type : "ApprovedContactInfo" );
            ref.contactId = contact.contactId;
            ref.emailId = contact.emailId;
            ref.firstName = contact.firstName;
            ref.lastName = contact.lastName;
            ref.jobTitle = contact.jobTitle;
            ref.phoneNumber = contact.phoneNumber;
            return ref;
        };

        $scope.checkBoxClicked = function(event){
            $scope.dataTableObj.selectRow(event);
        };

        function verifyDeletion(rows){

            var msg = "Deleting contacts cannot be undone!";
            var attr = {
                header: 'Confirm Deletion',
                cancel: 'Cancel',
                submit: 'Delete'
            };
            var func = function(){$scope.processTableRowDeletion(rows);};

            $scope.verificationModalObj.updateVerificationModel(attr,func);
            $scope.verificationModalObj.addVerificationModelMessage(msg);

            $('#verificationModal').modal('show');
        };

        /**
         @function processTableRowDeletion()
         @return {boolean} - truthy value to determine if delete request was successful
         */
        $scope.processTableRowDeletion = function(rows){

            var len = rows.length;

            var contactList = { type:"DeleteApprovedContactRequest", list: getSelectedContactsListForDeletion(rows) };

            SettingsService.deleteContacts(contactList,$scope.enterpriseId).success(function() {

                $scope.dataTableObj.refreshDataTable(false,len);

                if( len == 1){
                    commonUtilityService.showSuccessNotification("Selected contact has been deleted.");
                }else{
                    commonUtilityService.showSuccessNotification("All selected contacts have been deleted.");
                }
                return true;
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });

        };

        function getSelectedContactsListForDeletion(rows) {
            var contactList = [];
            for (var i = 0; i <= rows.length-1; i++) {
                var contact = {};
                contact.type = "ApprovedContactInfo";
                contact.contactId = rows[i].contactId;
                contactList.push(contact);
            }
            return contactList;
        }

    }
})();
