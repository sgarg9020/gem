/* Controllers */
(function(){
    "use strict";

    angular.module('app')
        .controller('InfoCtrl', Info);

    Info.$inject=['$scope','loadingState','SettingsService','commonUtilityService','$stateParams','$q','$sce','$rootScope','AuthenticationService'];

    /**
     @constructor Info()
     */
    function Info($scope,loadingState,SettingsService,commonUtilityService,$stateParams,$q,$sce,$rootScope,authenticationService){

        var enterpriseInfo = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            // $scope
            $scope.generalObj = {};
            $scope.disableUISelect=false;

            // Enterprise account
            enterpriseInfo.account = {};
            enterpriseInfo.account.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};
            enterpriseInfo.allowTimeZoneEdit=false;

            enterpriseInfo.statusKeyMap = {ACTIVE : ['ACTIVE', 'SUSPENDED'], SUSPENDED: ['ACTIVE', 'SUSPENDED'], INACTIVE:['ACTIVE', 'SUSPENDED','INACTIVE'] };

            enterpriseInfo.updateEnterpriseDetails = updateEnterpriseDetails;

            enterpriseInfo.subsTypeLabel = $scope.$parent.shortInfoScreen ? 'Account Subscription' : 'Gem Product';

                // Initialize
            _enterpriseDetails();

            $scope.isSubscriptionEditAllowed = authenticationService.isOperationAllowed(['ACCOUNT_SUBSCRIPTION_TYPE_EDIT']);

            loadingState.hide();
        }

        /**
         * @func _onEnterpriseDetails()
         * @desc controller request logic
         * @private
         */
        function _enterpriseDetails() {

            enterpriseInfo.errorMessage = '';
            $scope.enterpriseId = $stateParams.accountId;

            var getCountriesPromise = commonUtilityService.getCountries();
            var getEnterpriseDetailsPromise = SettingsService.getEnterpriseDetails($scope.enterpriseId);
            var getSubscriptionTypes = SettingsService.getEnterpriseSubscriptionTypes();

            $q.all([getCountriesPromise, getEnterpriseDetailsPromise,commonUtilityService.getSupportedCurrencies(),commonUtilityService.getTimeZones(),getSubscriptionTypes]).then(function (response) {
                enterpriseInfo.countries = response[0].data.countryList;
                enterpriseInfo.subscriptionTypes = response[4].data.map(function(subscription){return subscription.name.replace( "ENTERPRISE_", "GEM ")}).filter(function(sub){return !sub.match(/RESELLER_MA/g)});

                enterpriseInfo.supportedCurrencies = response[2].data.list;

                enterpriseInfo.timeZones = response[3].data.list;

                var enterpriseResponse = response[1].data;

                if (enterpriseResponse != null) {
                    $rootScope.currentAccountName = enterpriseResponse.accountName;
                    enterpriseInfo.account.accountName = enterpriseResponse.accountName;
                    enterpriseInfo.account.companyName = enterpriseResponse.company.name;
                    enterpriseInfo.account.city = enterpriseResponse.company.address.city;
                    enterpriseInfo.account.state = enterpriseResponse.company.address.state;
                    enterpriseInfo.account.postalCode = enterpriseResponse.company.address.zipCode;
                    enterpriseInfo.account.currency = getCurrencyObject(enterpriseInfo.supportedCurrencies,enterpriseResponse.company.currency);
                    enterpriseInfo.account.enterpriseId = enterpriseResponse.accountId;
                    enterpriseInfo.account.accountSubscriptionType = enterpriseResponse.accountSubscriptionType ? enterpriseResponse.accountSubscriptionType : ENTERPRISE_DEFAULT_SUBSCRIPTION_TYPE;
                    enterpriseInfo.account.accountSubscriptionType = enterpriseInfo.account.accountSubscriptionType.replace( "ENTERPRISE_", "GEM ").replace("_"," ");
                    enterpriseInfo.account.timezone = enterpriseResponse.company.timeZone;
                    enterpriseInfo.account.timezoneDesc = {offset:enterpriseResponse.company.timeZone,description:enterpriseResponse.company.timeZoneInfo,info: enterpriseResponse.company.timeZone+' ('+ enterpriseResponse.company.timeZoneInfo +')'};
                    enterpriseInfo.account.alertVersionSupported = enterpriseResponse.alertVersionSupported;
                    enterpriseInfo.account.pricingModelVersion = enterpriseResponse.pricingModelVersionSupported;
                    enterpriseInfo.account.status = enterpriseResponse.status;

                    enterpriseInfo.statusList = (enterpriseInfo.statusKeyMap[enterpriseResponse.status])?enterpriseInfo.statusKeyMap[enterpriseResponse.status]:['ACTIVE', 'SUSPENDED','INACTIVE'];

                    if(enterpriseResponse.company.address){
                        enterpriseInfo.account.address1 = enterpriseResponse.company.address.address1;
                        enterpriseInfo.account.address2 = enterpriseResponse.company.address.address2;
                        enterpriseInfo.account.country = enterpriseResponse.company.address.country ? commonUtilityService.getCountryObject(enterpriseInfo.countries,enterpriseResponse.company.address.country): '';
                    }

                    $scope.setHierarchicalData(enterpriseResponse.accountType == RESELLER_ACCOUNT_TYPE, $scope.isResellerRoot, enterpriseResponse.accountType == SUB_ACCOUNT_TYPE);
                    enterpriseInfo.editAccountPermission = enterpriseResponse.accountType == SUB_ACCOUNT_TYPE ? "RESELLER_SUB_ACCOUNT_EDIT,ACCOUNT_READ" : "ACCOUNT_EDIT,ACCOUNT_READ";
                    enterpriseInfo.allowTimeZoneEdit = (enterpriseInfo.account.status == 'INACTIVE') && enterpriseResponse.accountType != SUB_ACCOUNT_TYPE;
                    $scope.generalObj.previousData = angular.copy(enterpriseInfo.account);
                }

            }, function (data) {
                commonUtilityService.showErrorNotification((data&&data.data)?(data.data.errorStr):null);
            });
        }

        /**
         * @func updateEnterpriseDetails()
         * @param {object} enterpriseDetailsForm - form data binding object
         * @desc updates enterprise details
         */
        function updateEnterpriseDetails(enterpriseDetailsForm){
            enterpriseInfo.errorMessage = '';
            var isAlertConfig = false;
            if($scope.isFormValid(enterpriseDetailsForm,enterpriseInfo.account,true)) {
                SettingsService.updateEnterpriseDetails(isAlertConfig,enterpriseInfo.account).success(function (response) {
                    $rootScope.currentAccountName = enterpriseInfo.account.accountName;
                    $scope.generalObj.previousData = angular.copy(enterpriseInfo.account);
                    commonUtilityService.showSuccessNotification("Enterprise " + enterpriseInfo.account.accountName + " details have been updated successfully.");
                    _enterpriseDetails();

                }).error(function (data) {
                    enterpriseInfo.errorMessage = data.errorStr;
                });
            }else if($scope.generalObj.isNoChangeError){
                enterpriseInfo.errorMessage = "There is no change in the data to update.";
                $scope.generalObj.isNoChangeError = false;
            }
        }

        /**
         * @func getCurrencyObject
         * @desc returns the currency object that matches the currency code
         * @param currencies
         * @param code
         * @returns {object}
         */
        function getCurrencyObject(currencies,code){
            var curObj = {};

            for(var i =0; i<currencies.length; i++){
                if(currencies[i].code == code){
                    curObj = currencies[i];
                    break;
                }
            }

            return curObj;
        }

        activate();

    }
})();
