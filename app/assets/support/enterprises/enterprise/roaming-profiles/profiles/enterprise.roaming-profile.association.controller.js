"use strict";

(function(){

    angular.module('app')
        .controller('EntRPAssociationCtrl', EntRPAssociationCtrl);

    EntRPAssociationCtrl.$inject=['$scope','loadingState','$stateParams','$compile','dataTableConfigService','roamingProfileService','commonUtilityService','$timeout','$rootScope'];

    /**
     @constructor EntRPAssociationCtrl()
     */
    function EntRPAssociationCtrl($scope,loadingState,$stateParams,$compile,dataTableConfigService,roamingProfileService,commonUtilityService,$timeout,$rootScope){

        var entRPAssociation= this;

        function activate(){

            $scope.enterpriseId = $stateParams.accountId;

            entRPAssociation.disassociateRP = disassociateRoamingProfileFromEnterprise;
            entRPAssociation.entAssociateRP = associateRoamingProfileToEnterprise;
            entRPAssociation.onEntAssociateRoamingProfile = onEntAssociateRoamingProfile;

            $scope.entAssociateRPModalObj = {};

            $scope.emptyTableMessage = "No roaming profiles available.";
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = "entRPAssocDataTable";
            $scope.dataTableObj.tableHeaders = [
                {value: "Roaming Profile", DbValue: "roamingProfileName",search:true,placeHolder:'Profile'},
                {value: "Roaming Profile Set", DbValue: "roamingProfileSetName",search:true,placeHolder:'Set'},
                {value: "SIMs", DbValue: "simsCount"},
                {value: "Actions", DbValue: ''}
            ];
            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

        }

        activate();

        function _setDataTableOptions(){
            return {
                ordering: false,
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'RPID_'+data.roamingProfileId);
                    $compile(row)($scope);
                },
                columns: [
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false}
                ],
                columnDefs: [
                    {
                        targets: 0,
                        render: function (data, type, row) {

                            return '<a ui-sref="support.roamingProfile.version({RPSetId:\''+row.roamingProfileSetId+'\',versionId:\''+row.roamingProfileId+'\'})">'+row.roamingProfileName+'</a>';
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {

                            return '<a ui-sref="support.roamingProfile.set({RPSetId:\''+row.roamingProfileSetId+'\'})" class="cursor">'+row.roamingProfileSetName+'</a>';
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            var simCount = (row.simsCount)?(row.simsCount):0;
                            return '<span>'+simCount+'</span>';
                        }
                    },
                    {
                        targets: 3,
                        render:function (data, type, row){
                            if(row.simsCount == 0 || !row.simsCount){
                                return '<a gs-has-permission="UPDATE_ROAMING_PROFILE_ACCOUNT_MAP" href="javascript:void(0)" ng-click=\'showDisassociateVersionModal(\"'+ row.roamingProfileId + '\");\'>Disassociate</a>';
                            }
                        },
                        "defaultContent":''
                    }
                ],
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords:$scope.emptyTableMessage
                },
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {
                    if(aoData){
                        entRPAssociation.searchValue = (aoData[5])?aoData[5].value.value:'';
                    }

                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    $scope.isPageSizeFilterDisabled = false;

                    roamingProfileService.getRPAssociatedToEnterprise($scope.enterpriseId,queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response);
                        $scope.profilesList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.profilesList.length == 0);
                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }
                    }).error(function (data) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.profilesList ||  $scope.profilesList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"associated roaming profiles.");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });
                }

            }
        }

        $scope.showDisassociateVersionModal = function(versionId){

            var msg = "Disassociating roaming profile cannot be undone!";
            var attr = {
                header: 'Confirm Disassociation',
                cancel: 'Cancel',
                submit: 'Disassociate'
            };
            var func = function(){entRPAssociation.disassociateRP($scope.enterpriseId,versionId);};

            $scope.verificationModalObj.updateVerificationModel(attr,func);
            $scope.verificationModalObj.addVerificationModelMessage(msg);
            $('#verificationModal').modal('show');

        };

        function disassociateRoamingProfileFromEnterprise(enterpriseId,versionId){
            roamingProfileService.disassociateRPFromEnterprise(enterpriseId,versionId).success(function (response) {
                $rootScope.$broadcast('onAssociatedEntRPChanged');
                $scope.dataTableObj.refreshDataTable(false);
                commonUtilityService.showSuccessNotification("Roaming profile has been disassociated successfully.");
            }).error(function (response) {
                if(response && response.errorInt == 6039){
                    $scope.dataTableObj.refreshDataTable(false);
                }
                commonUtilityService.showErrorNotification(response);
            });
        }

        function associateRoamingProfileToEnterprise(){
            if ($scope.isSubAccount) {
                commonUtilityService.showErrorNotification('Association of a roaming profile is not allowed  to this enterprise. Please do associate roaming profiles to parent reseller account, which will be auto inherited to this enterprise.');
            } else {
                var obj = {
                    header: "Associate Roaming Profile to an Enterprise",
                    submit: "Associate",
                    cancel: "Cancel"
                };

                $scope.entAssociateRPModalObj.updateEntAssociationRPModal(obj,entRPAssociation.onEntAssociateRoamingProfile);

                $('#entAssociateRPModal').modal('show');
            }
        }

        function onEntAssociateRoamingProfile(roamingProfile,queryParams){

            roamingProfileService.associateRPToEnterprise($scope.enterpriseId,roamingProfile.roamingProfileId,queryParams).success(function (response) {
                $rootScope.$broadcast('onAssociatedEntRPChanged');
                $scope.dataTableObj.refreshDataTable(false);
                commonUtilityService.showSuccessNotification("Roaming profile has been associated to this account.");

            }).error(function (data) {
                $scope.warningModalObj.disableButtons = false;
                $scope.warningModalObj.onContinue = function(){
                    onEntAssociateRoamingProfile(roamingProfile,{ignoreWarnings:true});
                };
                console.log('associate roaming profile to enterprise failure.' + data);
                commonUtilityService.showErrorNotification(data,null,$scope.warningModalObj);

            });


        }


    }
})();
