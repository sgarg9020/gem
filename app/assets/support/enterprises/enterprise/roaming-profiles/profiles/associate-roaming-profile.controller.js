"use strict";

(function(){

    angular.module('app')
        .controller('EntAssociateRPCtrl', EntAssociateRPCtrl);

    EntAssociateRPCtrl.$inject=['$scope','modalDetails','$sce','roamingProfileService','formMessage','commonUtilityService'];

    /**
     @constructor EntAssociateRPCtrl()
     */
    function EntAssociateRPCtrl($scope,modalDetails,$sce,roamingProfileService,formMessage,commonUtilityService){

        var entAssociateRP = this;

        /**
         * Start up logic for controller
         */
        function activate(){

            /* Form elements object */
            entAssociateRP.roamingProfile = {};

            /* ModalDetails factory */
            modalDetails.scope(entAssociateRP, true);

            entAssociateRP.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};
            $scope.$parent.entAssociateRPModalObj.updateEntAssociationRPModal = updateModal; /* Attach to parent */
            entAssociateRP.enableRPVersionSelect = enableRPVersionSelect;

            var queryParam = {
                count:10000,
                startIndex:0
            };
            roamingProfileService.getRoamingProfileSetsList(queryParam).success(function (response) {

                entAssociateRP.roamingProfileSets = response.list;
            }).error(function (data) {
                commonUtilityService.showErrorNotification(data.errorStr);
            });

        }

        activate();

        function updateModal(modalObj,submit){

            entAssociateRP.roamingProfile = {};

            entAssociateRP.errorMessage = (entAssociateRP.roamingProfileSets && entAssociateRP.roamingProfileSets.length)?'':'Unable to process the request. Please reload the page.';
            $scope.isRPVersionSelectDisabled = true;

            modalDetails.scope(entAssociateRP, false);
            var obj = modalObj?modalObj:{formName: 'associateEntRP', cancel: 'Cancel', submit: 'Submit'};
            modalDetails.attributes(obj);
            modalDetails.cancel(resetModal);

            modalDetails.submit(function(entAssociateRPForm){
                if($scope.isFormValid(entAssociateRPForm,null,true)) {
                    entAssociateRP.errorMessage = '';
                    submit(entAssociateRP.roamingProfile.version);
                    resetModal();
                }

            });

        }

        function resetModal(){
            $scope.entAssociateRP.roamingProfile = {};
            formMessage.scope($scope.entAssociateRP);
            formMessage.message(false, "");
            angular.element('#entAssociateRPModal').modal('hide');
        }

        function enableRPVersionSelect(){

            var rpSetId = (entAssociateRP.roamingProfile.set)?(entAssociateRP.roamingProfile.set.roamingProfileSetId):null;
            entAssociateRP.roamingProfile.version = null;
            entAssociateRP.errorMessage = '';

            var queryParam = {
                count:10000,
                startIndex:0,
                status:'PUBLISHED'
            };

            roamingProfileService.getRoamingProfileRevisionsList(rpSetId,queryParam).success(function (response) {

                entAssociateRP.errorMessage = (response.list.length ==0)?'No published roaming profiles available for the selected roaming profile set.' :'';
                entAssociateRP.roamingProfileVersions = response.list;
                $scope.isRPVersionSelectDisabled = (response.list.length ==0);

            }).error(function (data) {
                entAssociateRP.errorMessage = data.errorStr;
            });

        }

    }
})();
