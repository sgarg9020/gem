"use strict";

(function(){

    angular.module('app')
        .controller('EntRPAssociationHistCtrl', EntRPAssociationHistCtrl);

    EntRPAssociationHistCtrl.$inject=['$scope','loadingState','$stateParams','$compile','dataTableConfigService','roamingProfileService','commonUtilityService','$timeout'];

    /**
     @constructor EntRPAssociationHistCtrl()
     */
    function EntRPAssociationHistCtrl($scope,loadingState,$stateParams,$compile,dataTableConfigService,roamingProfileService,commonUtilityService,$timeout){

        var entRPAssociationHist = this;

        /**
         * Start up logic for controller
         */
        function activate(){

            $scope.enterpriseId = $stateParams.accountId;

            entRPAssociationHist.cancelAssociation = cancelAssociation;
            entRPAssociationHist.refreshRPSimAssocHistTable = refreshRPSimAssocHistTable;
            entRPAssociationHist.onPendingSessionCancelled = onPendingSessionCancelled;

            $scope.emptyTableMessage = "No association history available.";
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = "entRPAssocHistDataTable";
            $scope.dataTableObj.tableHeaders = [
                {value: "Date (UTC)", DbValue: "createdTime"},
                {value: "Target Roaming profile", DbValue: "roamingProfileName"},
                {value: "SIMs", DbValue: "simsCount"},
                {value: "Status", DbValue: "status"},
                {value: "Completed SIMs", DbValue: ""},
                {value: "Actions", DbValue: ""}
            ];
            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

        }

        activate();

        function _setDataTableOptions(){
            return {
                order: [[ 0, "desc" ]],

                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'SID_'+data.sessionId);
                    $compile(row)($scope);
                },
                columns: [
                    { orderable:true},
                    { orderable:true},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false}
                ],
                columnDefs: [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return '<span>'+row.createdTime+'</span>';
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            var roamingProfileName = (row.roamingProfile)?row.roamingProfile.roamingProfileName:'';
                            return '<span>'+roamingProfileName+'</span>';
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            return '<span>'+row.simsCount+'</span>';
                        }
                    },
                    {
                        targets: 3,
                        render:function (data, type, row){
                            var status = roamingProfileService.getDisplayStatusOfSIMAssociation(row.status,row.simStatusCount );
                            if(status.name == 'Pending'){
                                return '<span class="RP-association-status '+status.class+'"><a ng-click=\"entRPAssociationHist.onPendingSessionCancelled()\" class="cursor">'+status.name+'</a></span>';
                            }
                            return '<span class="RP-association-status '+status.class+'"><a ui-sref="support.enterprise.association.status({accountId:\''+$scope.enterpriseId+'\',statusId:\''+row.sessionId+'\'})" class="cursor">'+status.name+'</a></span>';
                        }
                    },
                    {
                        targets: 4,
                        render:function (data, type, row){
                            var completedSimCount = (row.simStatusCount)?row.simStatusCount.COMPLETED:'Undefined';
                            return '<span>'+completedSimCount+'</span>';
                        }
                    },
                    {
                        targets: 5,
                        render:function( data, type, row) {
                            if(row.status == 'IN_PROGRESS' || row.status == 'CREATING'){
                                return '<a gs-has-permission="DELETE_SIM_ROAMING_PROFILE_SESSION" href="javascript:void(0)" ng-click=\'showCancelAssociationModal(\"'+ row.sessionId + '\");\'>Cancel</a>';
                            }
                        },
                        "defaultContent":''
                    }
                ],
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords:$scope.emptyTableMessage
                },
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {

                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    $scope.isPageSizeFilterDisabled = false;

                    roamingProfileService.getEnterpriseRPUpdateSession($scope.enterpriseId,queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response);
                        $scope.simMapHistList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.simMapHistList.length == 0);
                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }
                    }).error(function (data) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.simMapHistList ||  $scope.simMapHistList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"SIM Mapping history");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });

                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.simMapHistList )
                        dataTableConfigService.disableTableHeaderOps( $scope.simMapHistList.length, oSettings );

                }

            }
        }

        $scope.showCancelAssociationModal = function(sessionId){

            var msg = "Cancelling association of roaming profile cannot be undone!";
            var attr = {
                header: 'Confirm roaming profile association cancellation',
                cancel: 'Close',
                submit: 'Confirm'
            };
            var func = function(){entRPAssociationHist.cancelAssociation(sessionId);};

            $scope.verificationModalObj.updateVerificationModel(attr,func);
            $scope.verificationModalObj.addVerificationModelMessage(msg);
            $('#verificationModal').modal('show');
        };

        function cancelAssociation(sessionId){
            roamingProfileService.cancelEnterpriseRPUpdateSession(sessionId).success(function (response) {
                commonUtilityService.showSuccessNotification("Roaming profile association has been cancelled successfully.");
                $scope.dataTableObj.refreshDataTable(false);
            }).error(function (response) {
                if(response && response.errorInt == 5003){
                    $scope.dataTableObj.refreshDataTable(false);
                }
                commonUtilityService.showErrorNotification(response);
            });
        }

        function refreshRPSimAssocHistTable(notify){
            if(notify == 'showMsg')
                commonUtilityService.showSuccessNotification("Association history refresh has been triggered.");
            $scope.dataTableObj.refreshDataTable(false);
        }


        $scope.$on('onRPAssignedToSims',entRPAssociationHist.refreshRPSimAssocHistTable);

        function onPendingSessionCancelled(){
            commonUtilityService.showWarningNotification("This operation is not allowed as the session is still pending.");
        }
    }
})();
