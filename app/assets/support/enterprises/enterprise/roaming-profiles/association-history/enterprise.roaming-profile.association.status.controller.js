"use strict";

(function(){

    angular.module('app')
        .controller('EntRPAssociationStatusCtrl', EntRPAssociationStatusCtrl);

    EntRPAssociationStatusCtrl.$inject=['$scope','loadingState','dataTableConfigService','$compile','roamingProfileService','$stateParams','commonUtilityService','$location','$sce','$rootScope','$q','tooltips'];

    /**
     @constructor EntRPAssociationStatusCtrl()
     */
    function EntRPAssociationStatusCtrl($scope,loadingState,dataTableConfigService,$compile,roamingProfileService,$stateParams,commonUtilityService,$location,$sce,$rootScope,$q,tooltips){

        var entRPAssociationStatus = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            clearDataTableStatus(['DataTables_entRPAssociationStatusDataTable_']);
            tooltips.initTooltips('[data-toggle="tooltip"]');

            $scope.enterpriseId = $stateParams.accountId;
            $scope.statusId = $stateParams.statusId;

            entRPAssociationStatus.refreshEntRPAssocStatusDataTable  = refreshEntRPAssocStatusDataTable;
            entRPAssociationStatus.trustAsHtml = function (value){return $sce.trustAsHtml(value);};


            $scope.changeStatusFilters = [
                {
                    name:'ALL',
                    dbRef:'',
                    skipQueryParam:true
                },
                {
                    name:'COMPLETE',
                    dbRef:'COMPLETED'
                },
                {
                    name:'PENDING',
                    dbRef:'IN_PROGRESS'
                },
                {
                    name:'CANCELLED',
                    dbRef:'CANCELLED'
                },
                {
                    name:'FAILED',
                    dbRef:'ERROR'
                }
            ];

            $scope.emptyTableMessage = "No sims available.";
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = "entRPAssociationStatusDataTable";
            $scope.dataTableObj.tableHeaders = [
                {value: "CID", DbValue: "cId"},
                {value: "Status", DbValue: "status"}
            ];

            var filtersConfig = {status: {type:'url', defaultValue: $scope.changeStatusFilters[0] }};
            dataTableConfigService.configureFilters($scope.dataTableObj, filtersConfig);

            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

        }

        activate();

        function _setDataTableOptions(){
            return {
                ordering: false,
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'SID_'+data.cId);
                    $compile(row)($scope);
                },
                columns: [
                    { orderable:false},
                    { orderable:false}
                ],
                columnDefs: [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return '<span>'+row.cId+'</span>';
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {

                            var status = roamingProfileService.getStatusClassMap(row.status);
                            return (row.status == 'ERROR')?  '<span class="cursor-pointer ds-inline RP-association-status '+status.class+'" gs-tooltip data-toggle="tooltip" data-placement="top" title="'+row.errorMessage+'">'+status.name+'</span>'
                                :'<span class="RP-association-status '+status.class+'">'+status.name+'</span>';
                        }
                    }
                ],
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords:$scope.emptyTableMessage
                },
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {
                    if(aoData){
                        entRPAssociationStatus.searchValue = (aoData[5])?aoData[5].value.value:'';
                    }

                    var simQueryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    $scope.isPageSizeFilterDisabled = false;

                    $q.all([roamingProfileService.getEnterpriseRPUpdateSessionDetails($scope.statusId),roamingProfileService.getEnterpriseRPUpdateSessionSimsBySessionId($scope.statusId,simQueryParam)]).then(function (response) {
                        var sessionDetailsResponse = response[0].data;
                        var sessionSimResponse = response[1].data;

                        //data preparations for association updated sims
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(sessionSimResponse);
                        $scope.assocSimStatusList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.assocSimStatusList.length == 0);

                        //data preparations for the association status details
                        var targetRoamingProfile = sessionDetailsResponse.roamingProfile;
                        entRPAssociationStatus.targetRoamingProfileName = (targetRoamingProfile )?targetRoamingProfile.roamingProfileName:'';

                        var status = roamingProfileService.getDisplayStatusOfSIMAssociation(sessionDetailsResponse.status,sessionDetailsResponse.simStatusCount );
                        entRPAssociationStatus.sessionStatus = status.name;

                        entRPAssociationStatus.totalCompletedSimCount = (sessionDetailsResponse.simStatusCount)?sessionDetailsResponse.simStatusCount.COMPLETED:0;
                        entRPAssociationStatus.totalSimCount = sessionDetailsResponse.simsCount;

                        var userDetails = sessionDetailsResponse.userDetails;
                        entRPAssociationStatus.changedBy = ( userDetails)? (userDetails.emailId)?userDetails.emailId:'':'';

                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }

                    }, function (error) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.assocSimStatusList ||  $scope.assocSimStatusList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"roaming profile change status");
                        commonUtilityService.showErrorNotification(error.data.errorStr);
                    });
                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.assocSimStatusList )
                        dataTableConfigService.disableTableHeaderOps( $scope.assocSimStatusList.length, oSettings );

                }

            }
        }

        function refreshEntRPAssocStatusDataTable(){
            commonUtilityService.showSuccessNotification("Roaming profile change status refresh has been triggered.");
            $scope.dataTableObj.refreshDataTable(true);
        }

        $scope.$on('$destroy', function () { tooltips.destroyTooltips('[data-toggle="tooltip"]'); });

    }
})();
