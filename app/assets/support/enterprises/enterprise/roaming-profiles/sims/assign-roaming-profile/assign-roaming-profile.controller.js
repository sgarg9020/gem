"use strict";

(function(){

    angular.module('app')
        .controller('SimAssignRoamingProfileCtrl', SimAssignRoamingProfileCtrl);

    SimAssignRoamingProfileCtrl.$inject=['$scope','$rootScope' , 'loadingState','modalDetails','roamingProfileService','commonUtilityService','$sce','dataTableConfigService','SIMService','$compile','formMessage','$timeout'];

    /**
     @constructor SimAssignRoamingProfileCtrl()
     */
    function SimAssignRoamingProfileCtrl($scope,$rootScope ,loadingState,modalDetails,roamingProfileService,commonUtilityService,$sce,dataTableConfigService,SIMService,$compile,formMessage,$timeout){

        var simAssignRoamingProfile = this;

        /**
         * Start up logic for controller
         */
        function activate(){

            $scope.$parent.simAssignRoamingProfileObj = $scope.$parent.simAssignRoamingProfileObj || {};
            $scope.$parent.simAssignRoamingProfileObj.updateSimAssignRoamingProfileModal = updateModal;
            simAssignRoamingProfile.enableRPVersionSelect = enableRPVersionSelect;
            simAssignRoamingProfile.nextPage = nextPage;
            simAssignRoamingProfile.previousPage = previousPage;

            simAssignRoamingProfile.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};

            //initialise the dataTable

            $scope.emptyTableMessage = 'No SIMs available.';
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = "confirmRPToSimDataTable";
            $scope.dataTableObj.tableHeaders = [
                {value: "CId", DbValue:"cId",search:true,placeHolder:"CId"},
                {value: "SIM Version", DbValue:"simVersion"},
                {value: "Roaming Profile", DbValue:"roamingProfile"}
            ];
            var filtersConfig = { selectedDisplayLength: {type:'url', dbParam: 'count', defaultValue:10, handleFilter: $scope.dataTableObj.updateDisplayLength} };
            dataTableConfigService.configureFilters($scope.dataTableObj, filtersConfig);
            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions,true);


            /* ModalDetails factory */
            modalDetails.scope(simAssignRoamingProfile, true);

        }

        activate();

        function updateModal(sims, selectedSims, simsQueryParam ,modalObj, submit,associatedRPDetails){
            dataTableConfigService.removeFilter('selectedDisplayLength','url',$scope.dataTableObj.tableID);
            var filtersConfig = { selectedDisplayLength: {type:'url', dbParam: 'count', defaultValue:10, handleFilter: $scope.dataTableObj.updateDisplayLength} };
            dataTableConfigService.configureFilters($scope.dataTableObj, filtersConfig);
            $scope.dataTableObj.clearCurrentDataTableObj();
            $scope.disableSelectedSimsOpt = false;

            // Add new modal data
            modalDetails.scope(simAssignRoamingProfile, false);
            var obj = modalObj?modalObj: { header: 'Assign Roaming Profile to SIMs', submit: 'Assign', cancel:'Cancel'};
            modalDetails.attributes(obj);
            modalDetails.cancel(resetModal);

            simAssignRoamingProfile.roamingProfile = {};
            simAssignRoamingProfile.searchValue = '';

            simAssignRoamingProfile.errorMessage = (associatedRPDetails && associatedRPDetails.length)?'':'Unable to process the request. Please reload the page.';

            if(associatedRPDetails && associatedRPDetails.length){
                prepareRPSelectDropDownList(associatedRPDetails);
            }

            simAssignRoamingProfile.sims = sims;
            simAssignRoamingProfile.simsQueryParam = simsQueryParam;
            simAssignRoamingProfile.selectedSims = selectedSims;

            $scope.allSimsText = prepareAllSimsText(simAssignRoamingProfile.simsQueryParam);


            var isCurrentPageSelected = (simAssignRoamingProfile.sims && simAssignRoamingProfile.selectedSims && simAssignRoamingProfile.sims.length ==  simAssignRoamingProfile.selectedSims.length);
            simAssignRoamingProfile.selectedSimOption = (isCurrentPageSelected)?'curPageSims':'selectedSims';
            $scope.disableSelectedSimsOpt = simAssignRoamingProfile.selectedSimOption == 'curPageSims';

            $scope.isRPVersionSelectDisabled = true;
            $scope.isConfirmTab = false;

            modalDetails.submit(function(){
                var simList = (simAssignRoamingProfile.selectedSimOption == 'selectedSims')?prepareRequestIccIdList(simAssignRoamingProfile.selectedSims):null;
                var queryParam= simAssignRoamingProfile.simsQueryParam;
                if(simAssignRoamingProfile.selectedSimOption == 'allSims'){
                    queryParam.startIndex = 0;
                    queryParam.count = simAssignRoamingProfile.simCount;
                }
                submit(simAssignRoamingProfile.roamingProfile.version,simList,queryParam);
                resetModal();
            });
        }

        function enableRPVersionSelect(){

            $scope.isRPVersionSelectDisabled = false;
            simAssignRoamingProfile.rpVersionList = simAssignRoamingProfile.roamingProfile.set.localVersionList;
            simAssignRoamingProfile.roamingProfile.version = null;

        }

        function nextPage(entAssociateRPForm){

            if(!$scope.isFormValid(entAssociateRPForm,null,true)) {
                return;
            }
            $scope.dataTableObj.clearCurrentDataTableObj();
            $scope.isConfirmTab = true;
            simAssignRoamingProfile.modal.header = 'Confirm Assigning Roaming profile';

            $scope.dataTableObj.refreshDataTable(true,undefined,true);
        }

        function previousPage(){
            $scope.isConfirmTab = false;
            simAssignRoamingProfile.modal.header = 'Assign Roaming Profile to SIMs';
        }

        function resetModal(){
            simAssignRoamingProfile.roamingProfile = {};
            formMessage.scope(simAssignRoamingProfile);
            formMessage.message(false, "");
            angular.element('#simAssignRoamingProfileModal').modal('hide');
        }

        function _setDataTableOptions(){
            return {
                order: [[ 0, "asc" ]],
                "scrollY": "350px",
                "scrollCollapse": true,
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'SID_'+data.cId);
                    $compile(row)($scope);
                },
                columns: [
                    { orderable:false},
                    { orderable:false},
                    { orderable:false}
                ],
                columnDefs: [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return '<span>'+row.cId+'</span>';
                        }
                    },
                    {
                        targets: 1,
                        render:function (data, type, row){
                            return '<span>'+row.version+'</span>';
                        }
                    },
                    {
                        targets: 2,
                        render:function( data, type, row) {
                            if(row.roamingProfile){
                                return '<span>'+row.roamingProfile.roamingProfileName+'</span>';
                            }
                        },
                        "defaultContent":'Legacy'
                    }
                ],
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords:$scope.emptyTableMessage
                },
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {

                    if(simAssignRoamingProfile.selectedSimOption  == 'selectedSims'){

                        getTableData(simAssignRoamingProfile.selectedSims, aoData, fnCallback);

                    }else if(simAssignRoamingProfile.selectedSimOption  == 'curPageSims'){

                        getTableData(simAssignRoamingProfile.sims, aoData, fnCallback);
                    }
                    else if(simAssignRoamingProfile.selectedSimOption  == 'allSims'){

                        var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                        $scope.isPageSizeFilterDisabled = false;


                        queryParam =  prepareSimsRequestParams(queryParam,simAssignRoamingProfile.simsQueryParam)  ;

                        SIMService.getSIMs(queryParam,$scope.enterpriseId).success(function (response) {

                            var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                            var datTabData = dataTableConfigService.prepareResponseData(response);
                            $scope.simList = datTabData.aaData;
                            simAssignRoamingProfile.simCount = response.totalCount;
                            $scope.isPageSizeFilterDisabled = ( $scope.simList.length == 0);
                            if(oSettings != null) {
                                fnCallback(datTabData);
                            }else{
                                loadingState.hide();
                            }
                        }).error(function(data){
                            $scope.isPageSizeFilterDisabled = ( !$scope.simList ||  $scope.simList.length == 0);
                            var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                            dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"sims");
                            console.log('get sims failure' + data);
                            commonUtilityService.showErrorNotification(data.errorStr);
                        });
                    }
                    else {
                        loadingState.hide();
                    }
                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.simList )
                        dataTableConfigService.disableTableHeaderOps( $scope.simList.length, oSettings );

                }

            }
        }

        function getTableData(sims, aoData, fnCallback){

            var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
            $scope.isPageSizeFilterDisabled = false;

            var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
            var datTabData = prepareTableData(sims,queryParam,oSettings,$scope.emptyTableMessage);

            simAssignRoamingProfile.simCount = datTabData.aaData.length;

            $timeout(function(){
                fnCallback(datTabData);
            });

        }

        function prepareTableData(sims,queryParam,oSettings,message) {
            if(oSettings)
                oSettings.oLanguage.sEmptyTable = message;

            var datTabData = {};
            datTabData.aaData = getPaginatedData(sims,queryParam);
            datTabData.recordsTotal = sims.length;
            datTabData.recordsFiltered = sims.length;
            return datTabData;
        }


        function getPaginatedData(simData,queryParam){
            var startIndex = (queryParam.startIndex)?queryParam.startIndex:0;
            var endIndex = (queryParam.count)?(startIndex + queryParam.count):10;

            return simData.slice(startIndex,endIndex);

        }

        function prepareRequestIccIdList(simData){
            var iccIdList = [];

            for(var i=0;i<simData.length;i++){
                iccIdList.push(simData[i].simId);
            }

            return iccIdList;

        }

        function prepareRPSelectDropDownList(associatedRPDetails){

            simAssignRoamingProfile.rpDetails ={ sets: []};
            var rpSetIdPool = {};
            var poolIndex = 0;

            for(var i=0; i<associatedRPDetails.length; i++){

                var rpDetails = associatedRPDetails[i];

                if(rpSetIdPool[rpDetails.roamingProfileSetId] !== undefined){
                    simAssignRoamingProfile.rpDetails.sets[rpSetIdPool[rpDetails.roamingProfileSetId]].localVersionList.push(rpDetails);
                }else{
                    simAssignRoamingProfile.rpDetails.sets.push({setName:rpDetails.roamingProfileSetName, setId:rpDetails.roamingProfileSetId, localVersionList:[rpDetails]});
                    rpSetIdPool[rpDetails.roamingProfileSetId] = poolIndex++;
                }


            }

        }

        function prepareSimsRequestParams(queryParams, simsQueryParams){

            for(var param in simsQueryParams){
                if(simsQueryParams.hasOwnProperty(param) && param != 'startIndex' && param != 'count'){
                    queryParams[param] = simsQueryParams[param];
                }
            }

            return queryParams;

        }

        function prepareAllSimsText(queryParam){

            var simText = 'All SIMs';

            var rpField = (queryParam.roamingProfileName || queryParam.includeOnlyLegacyRpSims)?' profile '+(queryParam.roamingProfileName || 'Legacy'):'';
            var simVersionField = (rpField && queryParam.simVersion)?' having SIM version '+queryParam.simVersion:(queryParam.simVersion)?' SIM version '+queryParam.simVersion:'';
            var searchField = (queryParam.searchValue)?' matching search criteria '+queryParam.searchValue:'';

            simText += (rpField || simVersionField || searchField)?' filtered by' + (rpField + simVersionField + searchField):'';

            return simText;

        }


    }
})();
