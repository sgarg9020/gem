"use strict";

(function(){

    angular.module('app')
        .controller('EntRPSimMappingCtrl', EntRPSimMappingCtrl);

    EntRPSimMappingCtrl.$inject=['$scope','loadingState','$stateParams','$compile','dataTableConfigService','simsCommonFactory','SIMService','AuthenticationService','SettingsService','$q','$rootScope','commonUtilityService','roamingProfileService','$location','$sce'];

    /**
     @constructor EntRPSimMappingCtrl()
     */
    function EntRPSimMappingCtrl($scope,loadingState,$stateParams,$compile,dataTableConfigService,SimsCommonFactory,SIMService,authenticationService,SettingsService,$q,$rootScope,commonUtilityService,roamingProfileService,$location,$sce){

        var entRPSimMapping = this;
        var statusMap = {INACTIVE:'inactive',ACTIVE:'active',BLOCKED:'blocked',UNBLOCKED:'unblocked'};
        /**
         * Start up logic for controller
         */
        function activate(){

            $scope.enterpriseId = $stateParams.accountId;

            entRPSimMapping.hideTableColumns = hideTableColumns;
            entRPSimMapping.getSimCreditLimit = getSimCreditLimit;
            entRPSimMapping.prepareSimVersionFilters = prepareSimVersionFilters;
            entRPSimMapping.prepareRoamingProfileFilters = prepareRoamingProfileFilters;
            entRPSimMapping.changeRoamingProfileForSims = changeRoamingProfileForSims;
            entRPSimMapping.onSimAssignRoamingProfile = onSimAssignRoamingProfile;
            entRPSimMapping.refreshSimsTable = refreshSimsTable;

            entRPSimMapping.trustAsHtml = function (value){return $sce.trustAsHtml(value);};

            // Prepare simsCommonFactory with controller scope
            SimsCommonFactory.setScope($scope);
            $scope.zoneStatusModalObj = {};
            $scope.simAssignRoamingProfileObj = {};

            $scope.initialDataAvailable = false;

            $scope.emptyTableMessage = 'No SIMs available.';
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = "entRPSimMapDataTable";
            $scope.dataTableObj.tableHeaders = [
                {"value": "", "DbValue":""},
                {"value": "CID", "DbValue":"cId", search:true, placeHolder:"cId"},
                {"value": "simId", "DbValue":"simId", search:true, placeHolder:"simId"},
                {"value": "SIM Version", "DbValue":"simVersion"},
                {"value": "Status", "DbValue":"simStatus"},
                {"value": "Zone(s) Access", "DbValue":""},
                {"value": "Allocation", "DbValue":"limitKB"},
                {"value": "Roaming Profile", "DbValue":"roamingProfileName"},
                {value:'Procera',DbValue:''}
            ];

            var filtersConfig = { simVersion: {type:'url', defaultValue: { name:'ALL', dbRef:'',skipQueryParam:true} },  roamingProfile: {type:'url', dbParam: 'roamingProfileName', defaultValue: { name:'ALL', dbRef:'',skipQueryParam:true} }};
            dataTableConfigService.configureFilters($scope.dataTableObj, filtersConfig);

            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

        }
        activate();


        function _setDataTableOptions(){
            return {
                order: [[ 1, "asc" ]],
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'SID_'+data.simId);
                    $compile(row)($scope);
                },
                columns: [
                    { orderable:false},
                    { orderable:true},
                    { orderable:true},
                    { orderable:true},
                    { orderable:true},
                    { orderable:false},
                    { orderable:true},
                    { orderable:true},
                    { orderable:false}
                ],
                columnDefs: [
                    {
                        targets: 0,
                        className: "checkbox-table-cell",
                        render: function ( data, type, row, meta) {
                            return '<div gs-has-permission="UPDATE_SIM_ROAMING_PROFILE" class="checkbox check-success large no-label-text hideInReadOnlyMode"><input type="checkbox" value="1" id="checkbox_'+row.simId+'" gs-index="'+meta.row+'"><label ng-click="checkBoxClicked($event)" id="label_'+row.simId+'"></label></div>';
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return '<a ui-sref="support.sim.simDetail({accountId:\''+$scope.enterpriseId+'\',iccId:\''+row.simId+'\'})" class="cursor">'+row.cId+'</a>';

                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            return '<span>'+row.simId+'</span>';
                        }
                    },
                    {
                        targets: 3,
                        render:function (data, type, row){
                            return '<span>'+row.version+'</span>';
                        }
                    },
                    {
                        targets: 4,
                        render:function (data, type, row){
                            return '<div class="status-wrapper"><span class="status '+( row.status  == "ACTIVE" ? "active" : "inactive" )+'"></span><span class="status-text">'+( statusMap[row.status] ) +'</span></div>';
                        }

                    },
                    {
                        targets: 5,
                        render:function( data, type, row, meta) {
                            var zoneStatus = SimsCommonFactory.getZoneStatusForSim(row.zoneConnectivityStatus,entRPSimMapping.zonesCount);
                            return ($scope.alertEnabled) ? '<div class="status-wrapper status-as-link cursor" ng-click="entRPSimMapping.zoneStatus('+ meta.row +')"><span class="status '+( zoneStatus  == "Full Access" ? "full-access" : (zoneStatus  == "Partial Access" ? "partial-access" : "no-access") )+'"></span><span class="status-text">'+zoneStatus+'</span></div>' : '';
                        }
                    },
                    {
                        "targets": 6,
                        "render": function (data, type, row) {
                            $scope.simList[ row.simId ] =  row;
                            return  ($scope.alertEnabled) ? '' :  entRPSimMapping.getSimCreditLimit(row);

                        },
                        "defaultContent": '350'

                    },
                    {
                        targets: 7,
                        render:function( data, type, row) {
                            if(row.roamingProfile){
                                return '<a ui-sref="support.roamingProfile.version({RPSetId:\''+row.roamingProfile.roamingProfileSetId+'\',versionId:\''+row.roamingProfile.roamingProfileId+'\'})">'+row.roamingProfile.roamingProfileName+'</a>';
                            }
                        },
                        "defaultContent":'Legacy'
                    },
                    {
                        targets: 8,
                        render:function( data, type, row) {
                            if(row.roamingProfile && row.roamingProfile.roamingProfilePropagationPending){
                                return '<span class="RP-association-status Pending">Incomplete</span>';
                            }
                        },
                        "defaultContent":'<span class="RP-association-status Complete">Complete</span>'

                    }
                ],
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords:$scope.emptyTableMessage
                },
                processing: true,
                "rowCallback": function( row, data ) {
                    $scope.dataTableObj.showPreviousSelectedRows(data.simId, row);
                },
                fnServerData: function (sSource, aoData, fnCallback) {
                    if(aoData){
                        entRPSimMapping.searchValue = (aoData[5])?aoData[5].value.value:'';
                    }

                    var simsQueryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    $scope.isPageSizeFilterDisabled = false;

                    if(simsQueryParam.roamingProfileName == 'includeOnlyLegacyRpSims'){
                        simsQueryParam.includeOnlyLegacyRpSims = true;
                        delete simsQueryParam.roamingProfileName;
                    }

                    $scope.simsQueryParam = simsQueryParam;

                    var promiseArray = [SIMService.getSIMs(simsQueryParam,$scope.enterpriseId)];

                    if(!$scope.initialDataAvailable){
                        var entQueryParam = {};
                        entQueryParam.details = 'accountName|alertVersionSupported';
                        var planQueryDetails = 'name';
                        var rpQueryParam = {startIndex: 0, count: 10000};

                        promiseArray.push(authenticationService.getEnterpriseAccountDetails($scope.enterpriseId,entQueryParam),SettingsService.getPlanDetails(planQueryDetails,$scope.enterpriseId),SIMService.getSIMVersions($scope.enterpriseId),roamingProfileService.getRPAssociatedToEnterprise($scope.enterpriseId,rpQueryParam));
                    }

                    $q.all(promiseArray).then(function (response) {
                        angular.element('#checkbox_action').prop('checked', false);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response[0].data);
                        $scope.simList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.simList.length == 0);

                        if(!$scope.initialDataAvailable){
                            $scope.initialDataAvailable = true;

                            $rootScope.currentAccountName = response[1].data.accountName;
                            var alertVersion = response[1].data.alertVersionSupported;
                            $scope.alertEnabled = (alertVersion == "1.1");

                            entRPSimMapping.zonesCount = response[2].data.count;

                            var simVersions = response[3].data.list;
                            $scope.simVersionFilters = entRPSimMapping.prepareSimVersionFilters(simVersions);

                            entRPSimMapping.roamingProfiles = response[4].data.list;
                            $scope.roamingProfileFilters = entRPSimMapping.prepareRoamingProfileFilters(entRPSimMapping.roamingProfiles);

                        }

                        (oSettings != null)?fnCallback(datTabData):loadingState.hide();

                    }, function (error) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.simList ||  $scope.simList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"SIMs.");
                        commonUtilityService.showErrorNotification(error.data.errorStr);
                    });
                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.simList )
                        dataTableConfigService.disableTableHeaderOps( $scope.simList.length, oSettings );
                    entRPSimMapping.hideTableColumns();

                }

            }
        }

        entRPSimMapping.zoneStatus = function(index){
            var sim = $scope.simList[index];
            SimsCommonFactory.showZoneStatusModal(sim,entRPSimMapping.zonesCount,$scope.enterpriseId);
        };

        function hideTableColumns(){
            var $table = $("#" + $scope.dataTableObj.tableID);


            $scope.enableCheckbox = authenticationService.isOperationAllowed(['UPDATE_SIM_ROAMING_PROFILE']) && (entRPSimMapping.roamingProfiles && entRPSimMapping.roamingProfiles.length !=0);

            $table.DataTable().column(0).visible($scope.enableCheckbox);

            $table.DataTable().column(5).visible($scope.alertEnabled);
            $table.DataTable().column(6).visible(!$scope.alertEnabled);
        }

        function getSimCreditLimit( sim ){

            if(sim.creditLimitList && sim.creditLimitList.list  && sim.creditLimitList.list[0].limitKB !== undefined ) {
                return commonUtilityService.getUsageInMB()( sim.creditLimitList.list[0].limitKB * 1024 );
            }
            return commonUtilityService.getUsageInMB()(sessionStorage.getItem( "defaultCreditLimitForSims") * 1024 );
        }

        $scope.checkBoxClicked = function(event){
            $scope.dataTableObj.selectRow(event);
        };

        function prepareSimVersionFilters(sims){
            var simVersionFilters = [{ name:'ALL', dbRef:'',skipQueryParam:true} ];

            for(var i=0; i<sims.length; i++){
                var simVersionObj = {};
                simVersionObj.name = sims[i].version;
                simVersionObj.dbRef = sims[i].version;

                simVersionFilters.push(simVersionObj);
            }

            return simVersionFilters;
        }

        function prepareRoamingProfileFilters(roamingProfiles){
            var roamingProfileFilters =[{ name:'ALL', dbRef:'',skipQueryParam:true}, { name:'Legacy', dbRef:'includeOnlyLegacyRpSims'}];

            for(var i=0; i<roamingProfiles.length;i++){
                var roamingProfileObj = {};
                roamingProfileObj.name = roamingProfiles[i].roamingProfileName;
                roamingProfileObj.dbRef = roamingProfiles[i].roamingProfileName;

                roamingProfileFilters.push(roamingProfileObj);
            }

            return roamingProfileFilters;

        }


        function changeRoamingProfileForSims(rows){

            var obj = {
                header: "Assign Roaming Profile to SIMs",
                submit: "Assign",
                cancel: "Cancel"
            };

            var selectedSimList = processSimData(rows);

            $scope.simAssignRoamingProfileObj.updateSimAssignRoamingProfileModal($scope.simList,selectedSimList,$scope.simsQueryParam ,obj,entRPSimMapping.onSimAssignRoamingProfile,entRPSimMapping.roamingProfiles);

            $('#simAssignRoamingProfileModal').modal('show');
        }

        function onSimAssignRoamingProfile(roamingProfile,sims,queryParams){

            roamingProfileService.assignRoamingProfileToSims($scope.enterpriseId,roamingProfile.roamingProfileId,sims,queryParams).success(function (response) {
                $rootScope.$broadcast('onRPAssignedToSims');
                $scope.dataTableObj.refreshDataTable(false);
                commonUtilityService.showSuccessNotification("Roaming profile has been assigned to the SIMs.");

            }).error(function (data) {

                console.log('assign roaming profile to sims failure.' + data);
                commonUtilityService.showErrorNotification(data.errorStr);

            });


        }

        function processSimData(rows){
            var simList = [];

            for(var i=0; i<rows.length;i++){
                simList.push(rows[i]);
            }

            return simList;
        }

        function refreshSimsTable(){
            commonUtilityService.showSuccessNotification("SIM Mapping refresh has been triggered.");
            $scope.dataTableObj.refreshDataTable(false);
        }

        $scope.$on('onAssociatedEntRPChanged', function () {
            var rpQueryParam = {startIndex: 0, count: 10000};
            roamingProfileService.getRPAssociatedToEnterprise($scope.enterpriseId,rpQueryParam).success(function (response) {

                entRPSimMapping.roamingProfiles = response.list;
                $scope.roamingProfileFilters = entRPSimMapping.prepareRoamingProfileFilters(entRPSimMapping.roamingProfiles);

                var localEnableCheckbox = (entRPSimMapping.roamingProfiles && entRPSimMapping.roamingProfiles.length !=0);

                if(localEnableCheckbox != $scope.enableCheckbox){
                    $scope.enableCheckbox = authenticationService.isOperationAllowed(['UPDATE_SIM_ROAMING_PROFILE']) && localEnableCheckbox;
                    var $table = $("#" + $scope.dataTableObj.tableID);
                    $table.DataTable().column(0).visible($scope.enableCheckbox);
                }

            }).error(function (response) {
                console.log(response.errorStr);
                commonUtilityService.showErrorNotification(response.errorStr);
            });
        });
    }
})();
