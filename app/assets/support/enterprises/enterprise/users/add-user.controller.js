'use strict';

/* Controllers */
(function(){
	angular.module('app')
	    .controller('GapAddUserCtrl', AddUser);

	AddUser.$inject=['$scope', '$sce', 'modalDetails', 'notifications','formMessage','AccountService','commonUtilityService'];
	 
	/**
		@constructor AddUser()
		@param {object} $scope - Angular.js $scope
		@param {object} $sce - Angular.js strict contextual escaping service
		@param {object} modalDetails - Factory object
		@param {object} notifications - Factory object
		@param {object} formMessage - Factory object
		@param {object} AccountService - Factory object
		@param {object} commonUtilityService - Factory object
	*/
	function AddUser($scope, $sce, modalDetails,notifications,formMessage,AccountService,commonUtilityService){
		
		var addUser = this;

		/**
		 * Start up logic for controller
		 */
		function activate(){
			/* Form elements object */
			addUser.user = {};
			$scope.generalObj = {};

			/* ModalDetails factory */
			modalDetails.scope(addUser, true);
			modalDetails.attributes({
				id: 'addOrEditUserModal',
				formName: 'addOrEditUser',
				cancel: 'Cancel',
				submit: 'Add User'
			});
			modalDetails.cancel(resetModal);

			addUser.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};
			addUser.updateData = updateData;
			$scope.$parent.gapUserModalObj.updateUserModal = updateModal; /* Attach to parent */
		}

		/**
			@function updateModal()
			@param {object} user - optional - user's data to auto populate the form
			@param {object} obj - object to change modal attributes in directive
			@param {function} submit - function to execute on submit
			 - Allow a directive to pass data for the modal to update
		*/ 
		function updateModal(user ,enterpriseId, rolesList,obj, submit){
			//clear old form data
			addUser.user = {};
			modalDetails.scope(addUser, false);
			modalDetails.attributes(obj);

			$scope.rolesList = (rolesList && rolesList.length)?(rolesList).map(function(role){return {name:role.replace( "ENTERPRISE_", ""),dbRef:role}}):rolesList;

			addUser.errorMessage = ($scope.rolesList && $scope.rolesList.length)? '':'Unable to process the request. Please reload the page.';

			//Update the form data
			if(user){
				addUser.updateData(user,enterpriseId);
				var expando = user.$$hashKey;
				$scope.generalObj.previousData = angular.copy(addUser.user);
				modalDetails.submit(function(addUserForm){
					if($scope.isFormValid(addUserForm,addUser.user,true)) {
						submit(addUser.user, expando);
						resetModal()
					}else if($scope.generalObj.isNoChangeError){
						addUser.errorMessage = "There is no change in the data to update.";
						$scope.generalObj.isNoChangeError = false;
					}
				});
			}

			else {
				$scope.generalObj.previousData = null;
				modalDetails.submit(function(addUserForm){
					if($scope.isFormValid(addUserForm,addUser.user,true)) {
						submit(addUser.user);
						resetModal();
					}

				});
			}

		}

		function resetModal(){
			addUser.user ={};
			formMessage.scope(addUser);
			formMessage.message(false, "");
			angular.element('#' + $scope.addUser.modal.id).modal('hide');
		}

		/**
		 @function updateData()
		 @param {object} user - users data to add to the form
		 */
		function updateData(user, enterpriseId) {
			if (user.userId) {
				addUser.user.userId = user.userId;
			}

			if (user.firstName) {
				addUser.user.firstName = user.firstName;
			}

			if (user.lastName) {
				addUser.user.lastName = user.lastName;
			}

			if (user.emailId) {
				addUser.user.emailId = user.emailId;
			}
			if (user.accountDetails ) {

				addUser.user.accountDetails = user.accountDetails;

				for( var n = 0; n < user.accountDetails.enterpriseAccountDetails.length; n++ ) {

					var parentAcc = user.accountDetails.enterpriseAccountDetails[n].parentAccountDetails;
					for (var i = 0; parentAcc !== undefined, i < parentAcc.length; i++) {
						var accObj = parentAcc[i];
						if (accObj.accountId == enterpriseId && (accObj.accountType == 'ENTERPRISE' || accObj.accountType == RESELLER_ACCOUNT_TYPE || accObj.accountType == SUB_ACCOUNT_TYPE)) {
							addUser.user.role = {dbRef:accObj.role};
						}
					}

				}

				if(!addUser.user.role)
				{
                    addUser.user.role = {dbRef:'ENTERPRISE_SIM_USER'};
				}
				addUser.user.role.name = (addUser.user.role.dbRef)?(addUser.user.role.dbRef).replace( "ENTERPRISE_", ""):'SIM_USER';


			}
			if(user.location){
				addUser.user.location = user.location;
			}
		}

		activate();

	}
})();
