'use strict';

/* Controller */
(function(){
	angular.module('app')
	    .controller('GapUserManagementCtrl', Users);

	Users.$inject = ['$scope','$rootScope','$compile', 'UserService', 'dataTableConfigService', 'commonUtilityService','loadingState','$sanitize','AuthenticationService','$stateParams','$location','$sce','AccountService','$q','SettingsService'];

	/**
	 * @func Users()
 	 * @param $scope
	 * @param $rootScope
	 * @param $compile
	 * @param UserService
	 * @param dataTableConfigService
	 * @param commonUtilityService
	 * @param loadingState
     * @param $sanitize
	 * @param AuthenticationService
	 * @param $stateParams
	 * @param $location
	 * @param $sce
	 * @param AccountService
	 * @param $q
	 * @param SettingsService
     */
	function Users($scope,$rootScope, $compile,UserService, dataTableConfigService, commonUtilityService,loadingState,$sanitize,AuthenticationService,$stateParams,$location,$sce,AccountService,$q,SettingsService){
		
		var users = this;

		/**
		 * Start up logic for controller
		 */
		function activate(){
			clearDataTableStatus(['DataTables_gapUsersDataTable_']);

			$scope.emptyTableMessage = "No Users available.";

			users.addUser = addUserModal;
			users.verifyDeletion = verifyDeletion;
			users.hideTableColumns = hideTableColumns;
			users.resetPassword = resetPassword;

			$scope.dataTableObj = {};
			$scope.gapUserModalObj = {};

			$scope.enterpriseId = $stateParams.accountId;

			SettingsService.getEnterpriseDetails($scope.enterpriseId).success(function (response) {
				$scope.accountStatus = response.status;

				AccountService.getRoles(response.accountType,response.accountSubscriptionType).success(function (response) {

					$scope.rolesList = response.list;
					$scope.userRoleFilters = prepareUserRoleFilters(response.list);

				}).error(function (data) {
					commonUtilityService.showErrorNotification(data.errorStr);
					console.log(data.errorStr);
				});

			}).error(function (data) {
				commonUtilityService.showErrorNotification(data.errorStr);
				console.log(data.errorStr);
			});

			users.trustAsHtml = function (value){return $sce.trustAsHtml(value);};


			$scope.dataTableObj.tableID = "gapUsersDataTable";

			$scope.dataTableObj.tableHeaders = [
				{"value": "","DbValue":""},
				{"value": "Name","DbValue":"fullName"},
				{"value": "Email","DbValue":"emailId"},
				{"value": "Role","DbValue":'role'},
				{"value": "Location","DbValue":"location"},
				{"value": "SIM(s)","DbValue":"totalSimsAssociated"},
				{"value": "Group","DbValue":"accountName"},
				{"value": "Usage","DbValue":"dataUsage"},
				{"value": "Password","DbValue":""}
			];

			var filtersConfig = {roleName: {type:'url', defaultValue: { name:'ALL', dbRef:'',skipQueryParam:true} } };
			dataTableConfigService.configureFilters($scope.dataTableObj, filtersConfig);
			users.exportData={
				url: UserService.getExportDataUrl($scope.enterpriseId)
			};
			
			$scope.dataTableObj.dataTableOptions = _setDataTableOptions();
			$scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

		}


		function _setDataTableOptions(){
			return {
				order: [[ 1, "asc" ]],
				createdRow: function (row,data) {
					$(row).attr("id",'UID_'+data.userId);
					$compile(row)($scope);
				},
				columns: [
					{
						width:"60px",
						"orderable":false
					},
					{
						"orderable":true
					},
					{
						"orderable":true
					},
					{
						"orderable":false
					},
					{
						"orderable":true
					},
					{
						width:"75px",
						"orderable":true
					},
					{
						"orderable":true
					},
					{
						width:"75px",
						"orderable":true
					},
					{
						"orderable":false
					}
				],
				columnDefs: [{
					targets: 0,
					className: "checkbox-table-cell",
					render: function ( data, type, row, meta) {
						return '<div gs-has-permission="USER_DELETE" class="checkbox check-success large no-label-text hideInReadOnlyMode"> <input type="checkbox" value="1" id="checkbox_'+row.userId+'" gs-index="'+meta.row+'"><label ng-click="checkBoxClicked($event)" id="label_'+row.userId+'"></label></div>';
					}
				},
					{
						targets: 1,
						render: function (data, type, row, meta) {
							var fName = $sanitize(row.firstName);
							var lName = $sanitize(row.lastName);
							var permissionList = ['EDIT_USER','USER_PROFILE_EDIT'];
							var isEditingAllowed = AuthenticationService.isOperationAllowed(permissionList);

							return isEditingAllowed ? '<a id="edit_user_'+row.userId+'" class="nowrap" ng-click="editUser('+meta.row+');" href="javascript:void(0)" title="Click to edit user '+fName + ' ' + lName+'"><i class="fa fa-pencil edit-ico"></i>'+ ' ' + fName + ' ' + lName+'</a>' : fName + ' ' + lName;
						}
					},
					{
						targets: 2,
						render: function (data, type, row) {
							var emailId = $sanitize(row.emailId);
							return 	'<a href="mailto:'+emailId+'" title="Send an email to '+emailId+'" class="nowrap">'+emailId+'</a>'
						}
					}, {
						targets: 3,
						render: function( data, type, row){
							var role = getUserRole(row.accountDetails,$scope.enterpriseId);

							var userRole = (role)?role.replace("ENTERPRISE_", ""):'SIM_USER';

							return "<span class='nowrap'>{{'"+userRole+"' | underscoreless}}</span>";
						}
					}, {
						targets: 4,
						render: function( data, type, row){
							var location = $sanitize(row.location);
							return "<span class='nowrap'>"+location+"</span>";
						},
						defaultContent: ''
					},

					{
						targets: 5,
						render:function( data, type, row, meta ) {
							return (row.totalSimsAssociated ==0)?'0':'<a ng-click=\'showAssociatedSims('+row.userId+');\'><span>' + row.totalSimsAssociated + '</span></a>';
						},
						defaultContent: '0'

					},
					{
						targets: 6,
						render: function( data, type, row){


							var acc = getGroupDetails( row.accountDetails );

							if( acc ) {
								var accountName = $sanitize(acc.accountName);
								return "<span class='nowrap'>"+ ( (accountName) ? accountName  : 'Default' ) +"</span>";
							}
							else return "<span class='nowrap'>Default</span>";

						},
						defaultContent: 'Default'
					},
					{
						targets: 7,
						render: function (data, type, row) {
							return  '<span>'+((row.dataUsedInBytes!==undefined)?row.dataUsedInBytes === +row.dataUsedInBytes && row.dataUsedInBytes !== (row.dataUsedInBytes|0)? commonUtilityService.getUsageInMB()(row.dataUsedInBytes) : commonUtilityService.getUsageInMB()(row.dataUsedInBytes): 0)+'</span>';
						},
						defaultContent: '0'

					},
					{
						targets: 8,
						render: function (data, type, row,meta) {
							var userRole = getUserRole(row.accountDetails,$scope.enterpriseId);
							return (userRole == 'ENTERPRISE_SIM_USER')?'<span class="opacity-2">Reset</span>':'<a id="reset_user_pwd_'+row.userId+'" class="nowrap"  ng-click="users.resetPassword('+meta.row+')" href="javascript:void(0)">Reset</a>';
						}
					}


				],
				oLanguage: {
					sLengthMenu: "_MENU_ ",
					sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
					sEmptyTable: $scope.emptyTableMessage,
					sZeroRecords:$scope.emptyTableMessage
				},
				processing: true,
				rowCallback: function( row, data ) {
					$scope.dataTableObj.showPreviousSelectedRows(data.userId, row);
				},
				fnServerData: function (sSource, aoData, fnCallback) {
					if(aoData){
						users.searchValue = aoData[5].value.value;
					}
					$scope.isDisable = false;

					var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);

					users.exportData.startIndex = queryParam.startIndex;
					users.exportData.count = queryParam.count;
					users.exportData.sortBy = queryParam.sortBy;
					users.exportData.sortDirection = queryParam.sortDirection;
					users.exportData.search = queryParam.search;
					if(queryParam.roleName){
						users.exportData.roleName = queryParam.roleName;
					}

					$scope.isDisable = false;
					UserService.getUsers(queryParam,$scope.enterpriseId).success(function (response) {
						angular.element('#checkbox_action').prop('checked', false);
						var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
						var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
						$scope.usersList = datTabData.aaData;

						$scope.isDisable = ($scope.usersList.length == 0);

						if(oSettings != null) {
							fnCallback(datTabData);
						}else{
							loadingState.hide();
						}

					}).error(function(data){
						$scope.isDisable = !$scope.usersList ||  $scope.usersList.length == 0;
						var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
						dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"users");
						console.log('get users failure' + data);
						commonUtilityService.showErrorNotification(data.errorStr);
					});

				},

				fnDrawCallback: function( oSettings ) {

					if($scope.usersList)
						dataTableConfigService.disableTableHeaderOps( $scope.usersList.length, oSettings );

					users.hideTableColumns();
				}
			};
		}

		$scope.isDisableComponent = function(){
			return $scope.isDisable;
		};

		$scope.checkBoxClicked = function(event){
			$scope.dataTableObj.selectRow(event);
		};

		$scope.editUser = function(index){
			editUserModal($scope.usersList[index]);
		};


		$scope.showAssociatedSims = function(userId) {
			$scope.simsModalObj.showSIMs(userId);
		};

		function addUserModal() {
			/* Attributes for new user */
			var obj = {
				header: "Add New User",
				submit: "Add"
			};
			$scope.gapUserModalObj.updateUserModal(null, $scope.enterpriseId,$scope.rolesList, obj, $scope.onAddNewUser);

			$('#addOrEditUserModal').modal('show');
		}


		$scope.onAddNewUser = function(user) {

			user.type = "User";
			user.accountRoleAssociationList = [{
				type: "accountRoleAssociation",
				accountId: $scope.enterpriseId,
				role: user.role.dbRef
			}];
			delete user.role;
			var list = [ user ];

			var userList = { type:"UserList", list:list };

			UserService.addUser(userList, $scope.enterpriseId).success(function(){

				$scope.dataTableObj.refreshDataTable(false);
				commonUtilityService.showSuccessNotification("An account has been created for " + user.firstName + " " + " " + user.lastName );

			}).error(function(data){
				console.log('add user failure' + data);
				commonUtilityService.showErrorNotification(data.errorStr);
			});

		};



		/**
		 @function editUserModal()
		 @param {obj} user - user object to update
		 - Open the addUserModal
		 */
		function editUserModal(user) {
			/* Attributes for existing user */
			var obj = {
				header: "Update User",
				submit: "Update"
			};
			$scope.gapUserModalObj.updateUserModal(user, $scope.enterpriseId, $scope.rolesList, obj, $scope.onEditExistingUser);

			$('#addOrEditUserModal').modal('show');

		}

		$scope.onEditExistingUser =function(user, expando) {

			/* Add the expando back to the user object */
			user.$hashKey = expando;
			var userList = { type:"UserList", list: [getUserForUpdate( user ) ] };

			UserService.updateUsers(userList,$scope.enterpriseId).success(function () {

				$scope.dataTableObj.refreshDataTable(false);
				if(user.userId == sessionStorage.getItem('userId')){
					var data = {firstName: user.firstName,lastName : user.lastName};
					$rootScope.$emit('onUpdateAccountInfo',data);
				}

				commonUtilityService.showSuccessNotification("User " + user.firstName + " " + user.lastName + " details have been updated successfully.");

			}).error(function(data){
				console.log('update user failure' + data);
				commonUtilityService.showErrorNotification(data.errorStr);

			});

		};


		function verifyDeletion(rows){

			var msg = "Deleting users cannot be undone! All SIMs associated to the users will be unassigned.";
			var attr = {
				header: 'Confirm Deletion',
				cancel: 'Cancel',
				submit: 'Delete'
			};
			var func = function(){$scope.processTableRowDeletion(rows);};

			$scope.verificationModalObj.updateVerificationModel(attr,func);
			$scope.verificationModalObj.addVerificationModelMessage(msg);

			$('#verificationModal').modal('show');
		}


		function getGroupDetails(accDetails) {
			for( var n = 0; n < accDetails.enterpriseAccountDetails.length; n++ ) {

				var parentAcc = accDetails.enterpriseAccountDetails[n].parentAccountDetails;
				if (parentAcc !== undefined) {

					// case: only one parent Account, return that irrespective of the role
					if( parentAcc.length == 1 ) {
						return parentAcc[0];
					}

					for (var i = 0; i < parentAcc.length; i++) {
						var accObj = parentAcc[i];
						if (accObj.role == "ENTERPRISE_SIM_USER") {
							return accObj;
						}
					}
					return accDetails.enterpriseAccountDetails[n].rootAccountDetails;
				}
			}
		}

		/**
		 @function processTableRowDeletion()
		 @return {boolean} - truthy value to determine if delete request was successful
		 */
		$scope.processTableRowDeletion = function(rows){

			var len = rows.length;

			var userList = getSelectedUsersListForDeletion(rows);
			UserService.deleteUsers(userList,$scope.enterpriseId).success(function() {

			    $scope.dataTableObj.refreshDataTable(false,len);
				var action = 'Delete';
				if( len == 1){
					action = action + ' User';
					commonUtilityService.showSuccessNotification("Selected user has been deleted.");
				}else{
					action = action + ' Users';
					commonUtilityService.showSuccessNotification("All selected users have been deleted.");
				}

				return true;
			}).error(function(data){
				commonUtilityService.showErrorNotification(data.errorStr);
			});

		};




		function getUserForUpdate( user ) {

			var ref= {};
			ref.type =  ( user.type !== undefined  ? user.type : "User" );
			ref.userId = user.userId;
			ref.emailId = user.emailId;
			ref.firstName = user.firstName;
			ref.lastName = user.lastName;

			if(user.role){
				ref.accountRoleAssociationList = [{type:'accountRoleAssociation', accountId:$scope.enterpriseId, role:user.role.dbRef}];
			}

			return ref;
		}

		function getSelectedUsersListForDeletion(rows) {
			var userList = "";
			for (var i = 0; i < rows.length-1; i++) {

				userList += rows[i].userId + ",";
			}

			if(rows.length)
				userList += rows[i].userId;

			return userList;
		}

		function getUserRole(accDetails,enterpriseId){
			var userRole = 'ENTERPRISE_SIM_USER';

			for( var n = 0; n < accDetails.enterpriseAccountDetails.length; n++ ) {

				var parentAcc = accDetails.enterpriseAccountDetails[n].parentAccountDetails;
				for (var i = 0; parentAcc !== undefined, i < parentAcc.length; i++) {
					var accObj = parentAcc[i];
					if (accObj.accountId == enterpriseId && accObj.role != 'ENTERPRISE_SIM_USER') {
						return accObj.role;
					}else if(accObj.role == 'ENTERPRISE_ADMIN' && accObj.accountType == 'GROUP'){
						userRole = accObj.role + ' on '+ accObj.accountName;
					}
				}

			}

			return userRole;
		}

		function prepareUserRoleFilters(userRoles){

			var userRoleFilters = [ { name:'ALL', dbRef:'',skipQueryParam:true} ];

			for(var i=0; i< userRoles.length; i++){

				var userRoleFilterObj =  {};
				userRoleFilterObj.dbRef = userRoles[i];
				userRoleFilterObj.name = userRoles[i].replace( "ENTERPRISE_", "").replace(/_/g, ' ');

				userRoleFilters.push(userRoleFilterObj);
			}

			return userRoleFilters;

		}

		function hideTableColumns(){
			var $table = $("#" + $scope.dataTableObj.tableID);

			$scope.enableCheckbox = AuthenticationService.isOperationAllowed(['USER_DELETE']);

			$table.DataTable().column(0).visible($scope.enableCheckbox);
			$table.DataTable().column(8).visible(AuthenticationService.isOperationAllowed(['ACCOUNT_CREATE'])); //Since we don't have appropriate reset related permission we are using ACCOUNT_CREATE permission

			$table.DataTable().columns(6).visible(!$scope.resellerOrSubAccount);
		}

		function resetPassword(index){
			if($scope.accountStatus == 'INACTIVE'){
				commonUtilityService.showErrorNotification('The password cannot be reset as the enterprise is '+$scope.accountStatus+'.');
				return;
			}

			var user = $scope.usersList[index];
			var userRole = getUserRole(user.accountDetails,$scope.enterpriseId);
			if(userRole == 'ENTERPRISE_SIM_USER'){
				commonUtilityService.showErrorNotification('The password can only be reset for ADMIN role users.');
				return;
			}

			var msg = "A reset password link will be sent to "+user.emailId;
			var attr = {
				header: 'Reset Password',
				cancel: 'Cancel',
				submit: 'Reset'
			};
			var func = function(){
				AuthenticationService.forgotPassword(user.emailId).success(function () {
						commonUtilityService.showSuccessNotification("Instructions to reset the password have been sent to " + user.emailId);
					})
					.error(function (data, status) {
						commonUtilityService.showErrorNotification(data.errorStr);

					});
			};

			$scope.verificationModalObj.updateVerificationModel(attr,func);
			$scope.verificationModalObj.addVerificationModelMessage(msg);

			$('#verificationModal').modal('show');
		}

		activate();

	}
})();
