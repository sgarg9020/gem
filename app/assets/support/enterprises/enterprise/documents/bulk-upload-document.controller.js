'use strict';

/* Controllers */
(function(){
	angular.module('app')
	    .controller('BulkUploadDocumentCtrl', BulkUploadDocuments);

	BulkUploadDocuments.$inject=['$scope','$rootScope', 'modalDetails', '$stateParams'];

	/**
	 * @func BulkUploadDocuments()
	 * @param $scope
	 * @param $rootScope
	 * @param modalDetails
	 * @param analytics
     * @constructor
     */
	function BulkUploadDocuments($scope,$rootScope, modalDetails, $stateParams){

		var bulkUploadDocuments = this;

		/**
		 * Start up logic for controller
		 */
		function activate(){
			Dropzone.autoDiscover = false;
			$scope.enterpriseId = $stateParams.accountId;
			$scope.url = ENTERPRISE_GIGSKY_BACKEND_BASE+"accounts/account/" + $scope.enterpriseId + "/docs?title=";

			bulkUploadDocuments.refreshDatatable = $scope.$parent.dataTableObj.refreshDataTable;
			$scope.dropzoneConfig = {
				autoProcessQueue: false,
				maxFiles: 1,
				parallelUploads: 1,
				maxFilesize: 25,
				addRemoveLinks: false,
				clickable: true,
				acceptedFiles: ".csv,.pdf,.png,.txt,.doc,.docx,.xls,.xlsx",
				headers:{ "token" : sessionStorage.getItem( "token")}
				//url:ENTERPRISE_GIGSKY_BACKEND_BASE+"accounts/account/" + $scope.enterpriseId + "/docs?title=",
				//dictInvalidFileType:'File type not supported',

			};
			$scope.eventHandlers ={
				'addedfile': function(file){
					//appending file name as title in the url
					$scope.dropzoneConfig.url = $scope.url+ file.name;
				}
			}
			/* ModalDetails factory */
			modalDetails.scope(bulkUploadDocuments, true);
			modalDetails.attributes({
				id: 'bulkUploadDocumentsModal',
				formName: 'file',
				cancel: 'Clear',
				submit: 'Upload'
			});
			modalDetails.cancel(reset);
			modalDetails.submit(attemptToUpload);
			bulkUploadDocuments.cancelUpload = cancelUpload;
		}

		/**
			@function reset()
			 - call a directive level function
		*/
		function reset(){
			$rootScope.$broadcast('clearDropzoneFiles');
		}

		/**
			@function attemptToUpload()
			 - call a directive level function
		*/
		function attemptToUpload(){
			console.log($scope.dropzoneConfig.url);
			$rootScope.$broadcast('submitDropzoneFiles');
		}

		/**
			  @function attemptToUpload()
			 - call a directive level function
		 */
		function cancelUpload(){
			$rootScope.$broadcast('cancelDropzoneUpload');
		}




		activate();

	}
})();
