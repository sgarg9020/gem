/* Controllers */
(function () {
    "use strict";

    angular.module('app')
        .controller('ReportsCtrl', Reports);

    Reports.$inject = ['$scope','$stateParams','$compile','loadingState', 'dataTableConfigService', 'commonUtilityService', 'SettingsService'];

    /**
     @constructor Reports()
     */
    function Reports($scope,$stateParams,$compile,loadingState, dataTableConfigService, commonUtilityService, SettingsService) {

        var reportsVm = this;

        /**
         * Start up logic for controller
         */
        function activate() {
            clearDataTableStatus(['DataTables_reportsDataTable_']);
            $scope.emptyTableMessage = "No Documents Found.";
            $scope.dataTableObj = {};

            $scope.enterpriseId = $stateParams.accountId;
            $scope.dataTableObj.tableID = "reportsDataTable";
            $scope.dataTableObj.tableHeaders = [
                {"value": "Document", "DbValue": "title"},
                {"value": "","DbValue":""}
            ];
            $scope.dataTableObj.dataTableOptions = {
                ordering: false,
                "createdRow": function (row, data, index) {
                    $(row).attr("id", 'IID_' + data.docId);
                    $(row).attr("test-arg",data.title);
                    $compile(row)($scope);
                },
                "columnDefs": [{
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        var token = sessionStorage.getItem("token");
                        var url = ENTERPRISE_GIGSKY_BACKEND_BASE+'accounts/account/'+$scope.enterpriseId+'/docs/doc/'+row.docId+'?token='+encodeURIComponent(token);
                        return '<a href="' + url + '"> <i class="fa fa-file-o"></i> ' + row.title + '</a>';
                    }
                },{
                    targets: 1,
                    render: function (data, type, row, meta) {
                        return '<a  gs-has-permission="ACCOUNT_DOCUMENT_EDIT" id="delete_doc_'+row.docId+'" ng-click=\'confirmDeletion(\"'+row.docId+'\",\"'+row.title+'\");\' href="javascript:void(0)" title="Click to delete the document" class="nowrap"><i class="fa fa-trash-o"></i> Delete</a>';
                    }
                }
                ],
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ",
                    "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    "sEmptyTable": $scope.emptyTableMessage,
                    "sZeroRecords": $scope.emptyTableMessage
                },
                fnServerData: function (sSource, aoData, fnCallback) {
                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    $scope.isPageSizeFilterDisabled = false;

                    SettingsService.getDocuments($scope.enterpriseId,queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response, oSettings, $scope.emptyTableMessage);
                        $scope.reportsList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ($scope.reportsList.length == 0);
                        if (oSettings != null) {
                            fnCallback(datTabData);
                        } else {
                            loadingState.hide();
                        }
                    }).error(function (data, status) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.reportsList ||  $scope.reportsList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings, fnCallback, "invoices");
                        console.log('get reports failure' + data);
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });
                },

                "fnDrawCallback": function (oSettings) {
                    if($scope.reportsList)
                        dataTableConfigService.disableTableHeaderOps($scope.reportsList.length, oSettings);
                    commonUtilityService.preventDataTableActivePageNoClick($scope.dataTableObj.tableID);

                }
            };
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

            $scope.dataTableObj.dataTableOptions.columns = [
                {"width": "", "orderable": false},
                {"width": "", "orderable": false}
            ];
        }

        activate();

        $scope.confirmDeletion = function(docId,title){
            var msg = "Deleting document cannot be undone!";
            var attr = {
                header: 'Confirm Deletion',
                cancel: 'Cancel',
                submit: 'Delete'
            };
            var func = function(){$scope.deleteDocument(docId,title);};

            $scope.verificationModalObj.updateVerificationModel(attr,func);
            $scope.verificationModalObj.addVerificationModelMessage(msg);

            $('#verificationModal').modal('show');
        }

        /**
         * Deleting specific document
         * @param docId
         * @param title
         */
        $scope.deleteDocument = function(docId,title){
            SettingsService.deleteDocument($scope.enterpriseId,docId).success(function() {
                $scope.dataTableObj.refreshDataTable(false);
                commonUtilityService.showSuccessNotification("Document "+ title +" has been deleted.");

            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

    }
})();
