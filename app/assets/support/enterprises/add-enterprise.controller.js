'use strict';

/* Controllers */
(function(){
    angular.module('app')
        .controller('AddEnterpriseCtrl',  AddEnterprise);

    AddEnterprise.$inject = ['$scope', '$sce', 'modalDetails','formMessage','$q','commonUtilityService','AccountService','SettingsService','$state'];

    /**
     * @constructor AddEnterprise()
     * @param $scope
     * @param $sce
     * @param modalDetails
     * @param formMessage
     * @param $q
     * @param commonUtilityService
     * @param AccountService
     */
    function AddEnterprise($scope, $sce, modalDetails,formMessage,$q,commonUtilityService,AccountService,SettingsService,$state){

        var addEnterprise = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            /* Form elements object */
            addEnterprise.enterprise = {};

            /* ModalDetails factory */
            modalDetails.scope(addEnterprise, true);
            modalDetails.attributes({
                id: 'addEnterpriseModal',
                formName: 'addEnterpriseForm',
                cancel: 'Cancel',
                submit: 'Create'
            });
            modalDetails.cancel(resetModal);
            modalDetails.submit();

            addEnterprise.subsTypeLabel = $scope.$parent.shortInfoScreen ? 'Account Subscription' : 'Gem Product';
            addEnterprise.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};
            addEnterprise.formSubmit = formSubmit;

            $scope.$parent.enterpriseModalObj.addEnterpriseModal = updateModal; /* Attach to parent */

            $q.all([commonUtilityService.getSupportedCurrencies(),commonUtilityService.getTimeZones(),SettingsService.getEnterpriseSubscriptionTypes()]).then(function (response) {
                addEnterprise.supportedCurrencies = response[0].data.list;
                
                addEnterprise.subscriptionTypes = response[2].data.map(function(subscription){return subscription.name.replace( "ENTERPRISE_", "GEM ").replace("_"," ");});

                addEnterprise.timeZones = response[1].data.list;

            }, function (error) {
                var errStr = (error.data)?error.data.errorStr:error.errorStr;
                commonUtilityService.showErrorNotification(errStr);
            });

        }

        activate();


        /**
         * @func updateModal
         * @desc updates the modal
         * @param countries - list of countries
         * @param obj {obj} - modal attributes
         * @param onAddEnterprise {func} - parent controller callback
         */
        function updateModal(countries, obj, onAddEnterprise ){

            //clear old form data
            addEnterprise.enterprise = {};
            if($scope.isResellerAccount){
                getDefaultSubAccontFields();
            }
            addEnterprise.countries = countries;

            addEnterprise.errorMessage = ((addEnterprise.supportedCurrencies && addEnterprise.supportedCurrencies.length) && (addEnterprise.timeZones && addEnterprise.timeZones.length) && (addEnterprise.countries && addEnterprise.countries.length))? '':'Unable to process the request. Please reload the page.';

            modalDetails.scope(addEnterprise, false);
            modalDetails.attributes(obj);

            modalDetails.submit(function(addEnterpriseForm){
                addEnterprise.errorMessage = '';
                if($scope.isFormValid(addEnterpriseForm,null,true)) {
                    addEnterprise.formSubmit(addEnterprise.enterprise,onAddEnterprise);
                }

            });
        }

        function getDefaultSubAccontFields() {
            SettingsService.getEnterpriseDetails(sessionStorage.getItem("accountId")).success(function (enterpriseResponse) {
                addEnterprise.enterprise.accountSubscriptionType = SUB_ACCOUNT_SUBSCRIPTION_TYPE.replace( "ENTERPRISE_", "GEM ");;
                addEnterprise.enterprise.timezone = enterpriseResponse.company.timeZone;
                addEnterprise.enterprise.timezoneDesc = {offset:enterpriseResponse.company.timeZone,description:enterpriseResponse.company.timeZoneInfo,info: enterpriseResponse.company.timeZone+' ('+ enterpriseResponse.company.timeZoneInfo +')'};
                addEnterprise.enterprise.alertVersionSupported = enterpriseResponse.alertVersionSupported;
                addEnterprise.enterprise.currency = getCurrencyObject(addEnterprise.supportedCurrencies,enterpriseResponse.company.currency);
            }).error(function (data) {
                commonUtilityService.showErrorNotification(data.errorStr);
                console.log(data.errorStr);
            });
        }

        function getCurrencyObject(currencies,code){
            var curObj = {};

            for(var i =0; i<currencies.length; i++){
                if(currencies[i].code == code){
                    curObj = currencies[i];
                    break;
                }
            }

            return curObj;
        }

        /**
         *  @func resetModal
         *  @desc resets modal data
         */
        function resetModal(){
            addEnterprise.enterprise = {};
            if($scope.isResellerAccount){
                getDefaultSubAccontFields();
            }
            formMessage.scope(addEnterprise);
            formMessage.message(false, "");
            angular.element('#' + $scope.addEnterprise.modal.id).modal('hide');
        }


        /**
         * @func formSubmit
         * @param enterprise {obj} - enterprise object
         * @param onAddEnterprise - parent controller callback
         * @desc submits enterprise form
         */
        function formSubmit(enterprise,onAddEnterprise){

            AccountService.addEnterprise(enterprise).success(function (response) {
                resetModal();
                var accountId = (response.list && response.list.length)?response.list[0]:0;
                showProvisioningConfirmationModal(onAddEnterprise,accountId);
            }).error(function (data) {
                addEnterprise.errorMessage = data.errorStr;
            });
        }

        function showProvisioningConfirmationModal(onAddEnterprise,enterpriseId){

            if($scope.isResellerAccount){
                commonUtilityService.showSuccessNotification("Sub account has been created successfully.");
                onAddEnterprise(false);
                return;
            }

            if(!enterpriseId){
                commonUtilityService.showSuccessNotification("Enterprise account has been created successfully.");
                onAddEnterprise(false);
                return;
            }


            var msg = "Do you want to continue provisioning this account?";
            var attr = {
                header: 'Enterprise account has been created successfully.',
                cancel: 'Cancel',
                submit: 'Continue'
            };
            var onSubmit = function(){
                $state.go('support.enterprise.roamingProfiles',{accountId:enterpriseId});
            };
            var onCancel = function () {
                onAddEnterprise(false);
            };

            $scope.verificationModalObj.updateVerificationModel(attr,onSubmit,onCancel);
            $scope.verificationModalObj.addVerificationModelMessage(msg);

            $('#verificationModal').modal('show');
        }

    }
})();
