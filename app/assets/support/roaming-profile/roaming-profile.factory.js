'use strict';

(function(){
    angular
        .module('app')
        .factory('roamingProfileService', RoamingProfileService);

    RoamingProfileService.$inject = ["commonUtilityService",'$q'];

    /**
     @function AlertsService()
     */
    function RoamingProfileService(commonUtilityService,$q){

        var statusToClassMap = { COMPLETED:'Complete', IN_PROGRESS:'Pending', ERROR:'Failed', CANCELLED:'Cancelled',PARTIAL:'Partial', CREATING:'Pending', SKIPPED:'Skipped', ERROR_MODAL:'Errors', WARNING_MODAL:'Warnings'};

        return {
            getAllowedIMSIProfiles:getAllowedIMSIProfiles,

            createRoamingProfileSet:createRoamingProfileSet,
            getRoamingProfileSetDetails:getRoamingProfileSetDetails,
            getRoamingProfileSetsList:getRoamingProfileSetsList,
            getRoamingProfileRevisionsList:getRoamingProfileRevisionsList,
            deleteRoamingProfileSet:deleteRoamingProfileSet,
            getEnterprisesOfRoamingProfileSet:getEnterprisesOfRoamingProfileSet,

            createRoamingProfile:createRoamingProfile,
            editRoamingProfile:editRoamingProfile,
            getRoamingProfileDetails:getRoamingProfileDetails,
            getRoamingProfileCoverageInfo:getRoamingProfileCoverageInfo,
            deleteRoamingProfile:deleteRoamingProfile,
            getEnterprisesOfRoamingProfile:getEnterprisesOfRoamingProfile,

            getRPAssociatedToEnterprise : getRPAssociatedToEnterprise,
            getRPSetAssociatedToEnterprise : getRPSetAssociatedToEnterprise,
            associateRPToEnterprise : associateRPToEnterprise,
            disassociateRPFromEnterprise : disassociateRPFromEnterprise,
            assignRoamingProfileToSims : assignRoamingProfileToSims,
            getEnterpriseRPUpdateSession : getEnterpriseRPUpdateSession,
            getEnterpriseRPUpdateSessionDetails : getEnterpriseRPUpdateSessionDetails,
            cancelEnterpriseRPUpdateSession : cancelEnterpriseRPUpdateSession,
            getEnterpriseRPUpdateSessionSimsBySessionId : getEnterpriseRPUpdateSessionSimsBySessionId,
            getRoamingProfileExportURI:getRoamingProfileExportURI,

            getDisplayStatusOfSIMAssociation:getDisplayStatusOfSIMAssociation,
            getStatusClassMap: getStatusClassMap,

            prepareCountriesFilters:prepareCountriesFilters,
            prepareIMSIProfilesFilters:prepareIMSIProfilesFilters

        };

        function getAllowedIMSIProfiles(){
            var url = ENTERPRISE_PLAN_API_BASE_V3 + "ip";
            return commonUtilityService.prepareHttpRequest(url, 'GET');
        }

        // Roaming profile set API's Start

        function createRoamingProfileSet(roamingProfileSet){
            var headerAdd=['CREATE_ROAMING_PROFILE_SET'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_PLAN_API_BASE_V3  + "rpset", 'POST',headerAdd,false,roamingProfileSet);
        }

        function getRoamingProfileSetDetails(rpSetId){
            var url = ENTERPRISE_PLAN_API_BASE_V3 + "rpset/"+ rpSetId;

            var headerAdd=['GET_ROAMING_PROFILE_SET'];

            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd);
        }

        function getRoamingProfileSetsList(queryParam){
            var url = ENTERPRISE_PLAN_API_BASE_V3 + "rpset";

            var headerAdd=['GET_ROAMING_PROFILE_SET'];

            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParam);
        }

        function getRoamingProfileRevisionsList(rpSetId,queryParam){
            var url = ENTERPRISE_PLAN_API_BASE_V3 + "rpset/"+ rpSetId+"/rprofiles";

            var headerAdd=['GET_ROAMING_PROFILE_SET'];

            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParam);
        }
        function deleteRoamingProfileSet(roamingSetId){
            var url = ENTERPRISE_PLAN_API_BASE_V3 + "rpset/"+ roamingSetId;

            var headerAdd=['DELETE_ROAMING_PROFILE_SET'];
            return commonUtilityService.prepareHttpRequest(url, 'DELETE',headerAdd);
        }

        function getEnterprisesOfRoamingProfileSet(rpSetId,queryParams){

            var url = ENTERPRISE_PLAN_API_BASE_V3 + "rpset/"+ rpSetId+"/accounts";

            var headerAdd=['GET_ROAMING_PROFILE_SET'];

            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParams);

        }

        // Roaming profile set API's End


        // Roaming profile API's Start

        function createRoamingProfile(RPSetId,roamingProfile){

            var headerAdd=['CREATE_ROAMING_PROFILE'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_PLAN_API_BASE_V3  + "rpset/" + RPSetId + "/rp", 'POST',headerAdd,false,roamingProfile);

        }

        function editRoamingProfile(RPSetId,roamingProfileId,roamingProfile){
            var headerAdd=['EDIT_ROAMING_PROFILE'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_PLAN_API_BASE_V3  +'rpset/'+RPSetId+ "/rp/" + roamingProfileId, 'PUT',headerAdd,false,roamingProfile);
        }

        function getRoamingProfileDetails(RPSetId,roamingProfileId){
            var url = ENTERPRISE_PLAN_API_BASE_V3 +'rpset/'+RPSetId+ "/rp/"+ roamingProfileId;

            var headerAdd=['GET_ROAMING_PROFILE'];
            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd);
        }

        function getRoamingProfileCoverageInfo(RPSetId,versionId,queryParam){
            var url = ENTERPRISE_PLAN_API_BASE_V3 +"rpset/" + RPSetId + "/rp/"+versionId+"/coverage";
            var headerAdd=['GET_ROAMING_PROFILE'];

            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParam);
        }

        function deleteRoamingProfile(roamingProfileId){
            var url = ENTERPRISE_PLAN_API_BASE_V3 + "rp/"+ roamingProfileId;

            var headerAdd=['DELETE_ROAMING_PROFILE'];
            return commonUtilityService.prepareHttpRequest(url, 'DELETE',headerAdd);
        }

        function getEnterprisesOfRoamingProfile(versionId,queryParams){
            var url = ENTERPRISE_PLAN_API_BASE_V3 + "rp/"+versionId+"/accounts";
            var headerAdd=['GET_ROAMING_PROFILE'];
            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParams);
        }
        // Roaming profile API's End

        //Roaming profile management API's for an enterprise account  Start

        function getRPAssociatedToEnterprise(enterpriseId,queryParams){

            var url = ENTERPRISE_PLAN_API_BASE_V3 +"account/"+enterpriseId+'/rp';
            var headerAdd=['GET_ROAMING_PROFILE'];

            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParams);

        }

        function getRPSetAssociatedToEnterprise(enterpriseId,queryParams){

            var url = ENTERPRISE_PLAN_API_BASE_V3 + 'account/'+enterpriseId+'/rpset';
            var headerAdd=['GET_ROAMING_PROFILE'];

            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParams);

        }

        function associateRPToEnterprise(enterpriseId,roamingProfileId,queryParams){

            var url = ENTERPRISE_PLAN_API_BASE_V3 + 'account/'+enterpriseId+'/rp';
            var headerAdd=['UPDATE_ROAMING_PROFILE_ACCOUNT_MAP'];
            var postData = { 'type' : 'RoamingProfile', 'roamingProfileId': roamingProfileId };
            queryParams = queryParams?queryParams:null;

            return commonUtilityService.prepareHttpRequest(url, 'POST',headerAdd,queryParams,postData);

        }

        function disassociateRPFromEnterprise(enterpriseId,roamingProfileId){

            var url = ENTERPRISE_PLAN_API_BASE_V3 + 'account/'+enterpriseId+'/rp/'+ roamingProfileId;
            var headerAdd=['UPDATE_ROAMING_PROFILE_ACCOUNT_MAP'];

            return commonUtilityService.prepareHttpRequest(url, 'DELETE',headerAdd);
        }

        function assignRoamingProfileToSims(enterpriseId,roamingProfileId,sims,queryParams){

            var url = ENTERPRISE_GIGSKY_BACKEND_BASE + 'accounts/'+enterpriseId+'/sims/rp';
            var headerAdd=['UPDATE_SIM_ROAMING_PROFILE'];
            var putData = { type: 'SimRoamingProfileUpdate', roamingProfileId : roamingProfileId };


            if(sims && sims.length){
                putData.simIdsList=sims;
                return commonUtilityService.prepareHttpRequest(url, 'PUT',headerAdd,null,putData);
            }

            return commonUtilityService.prepareHttpRequest(url, 'PUT',headerAdd,queryParams,putData);

        }

        function getEnterpriseRPUpdateSession(enterpriseId,queryParams){

            var url = ENTERPRISE_GIGSKY_BACKEND_BASE +"accounts/"+enterpriseId+'/sims/rp/session';
            var headerAdd=['GET_SIM_ROAMING_PROFILE_SESSION'];

            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParams);
        }


        function getEnterpriseRPUpdateSessionDetails(sessionId,queryParams){

            var url = ENTERPRISE_GIGSKY_BACKEND_BASE +'sims/rp/session/'+sessionId;
            var headerAdd=['GET_SIM_ROAMING_PROFILE_SESSION'];

            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParams);
        }

        function getEnterpriseRPUpdateSessionSimsBySessionId(sessionId, queryParams){

            var url = ENTERPRISE_GIGSKY_BACKEND_BASE +'sims/rp/session/'+sessionId+'/sims';
            var headerAdd=['GET_SIM_ROAMING_PROFILE_SESSION'];

            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParams);
        }

        function cancelEnterpriseRPUpdateSession(sessionId){

            var url = ENTERPRISE_GIGSKY_BACKEND_BASE +'sims/rp/session/'+sessionId;
            var headerAdd=['DELETE_SIM_ROAMING_PROFILE_SESSION'];

            return commonUtilityService.prepareHttpRequest(url, 'DELETE',headerAdd);

        }

        function getRoamingProfileExportURI(RPSetId,versionId){
            return ENTERPRISE_PLAN_API_BASE_V3 +'rpset/'+RPSetId+ "/rp/"+versionId+"/coverage?responseType=csv&token=" + encodeURIComponent(sessionStorage.getItem("token"));
        }
        //Roaming profile management API's for an enterprise account End

        //Roaming profile management helper functions start


        function getDisplayStatusOfSIMAssociation(status, simCountObj){

            status = (status == 'ERROR' && simCountObj && simCountObj.COMPLETED)?'PARTIAL':status;

            return getStatusClassMap(status);

        }

        function getStatusClassMap(status){

            var statusObj = {};
            var statusStr = statusToClassMap[status];

            statusObj.class = statusStr?statusStr:'';
            statusObj.name = statusStr?statusStr:status;

            return statusObj;

        }

        //Roaming profile management helper functions end

        function prepareCountriesFilters(countriesArr){

            var countriesObj = [ { name:'ALL', dbRef:'',skipQueryParam:true} ];

            for(var i=0; i< countriesArr.length; i++){

                var countryObj =  {};
                countryObj.dbRef = countriesArr[i].code;
                countryObj.name = countriesArr[i].name;

                countriesObj.push(countryObj);
            }

            return countriesObj;

        }

        function prepareIMSIProfilesFilters(IMSIProfilesArr){

            var IMSIProfilesObj = [ { name:'ALL', dbRef:'',skipQueryParam:true} ];

            for(var i=0; i< IMSIProfilesArr.length; i++){

                var ImsiProfileObj =  {};
                ImsiProfileObj.dbRef = IMSIProfilesArr[i];
                ImsiProfileObj.name = IMSIProfilesArr[i];

                IMSIProfilesObj.push(ImsiProfileObj);
            }

            return IMSIProfilesObj;

        }

    }
})();
