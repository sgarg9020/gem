'use strict';

/* Controllers */
(function(){
    angular.module('app')
        .controller('EnterpriseModalCtrl', EnterpriseModal);

    EnterpriseModal.$inject=['$scope', 'modalDetails','commonUtilityService','roamingProfileService'];

    /**
     @constructor ModalBarChart()
     @param {object} $scope - Angular.js $scope
     @param {object} modalDetails - Factory object
     @param {object} requestGraphData - factory service
     */
    function EnterpriseModal($scope, modalDetails,commonUtilityService,RoamingProfileService ){

        var enterpriseModal = this;

        $scope.enterpriseObj = {};


        /**
         * Start up logic for controller
         */
        function activate(){
            enterpriseModal.enterprises = [];
            $scope.$parent.enterpriseObj = $scope.$parent.enterpriseObj || {};
            $scope.$parent.enterpriseObj.updateEnterpriseModal = updateModal;

            /* ModalDetails factory */
            modalDetails.scope(enterpriseModal, true);
            modalDetails.attributes({
                id: 'enterpriseModal',
                header: 'Enterprises',
                submit: 'Close'
            });
        }

        function updateModal(obj, submit,isRoamingProfileSet,id){

            // Add new modal data
            modalDetails.scope(enterpriseModal, false);
            modalDetails.attributes(obj);
            var accountListQueryParams = {
                count:10000,
                startIndex:0
            };
            
            (isRoamingProfileSet?RoamingProfileService.getEnterprisesOfRoamingProfileSet(id,accountListQueryParams):RoamingProfileService.getEnterprisesOfRoamingProfile(id,accountListQueryParams)).success(function(response){
                enterpriseModal.enterprises = response.list;
            }) .error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }
        activate();
    }
})();
