/* Controllers */
(function(){
    "use strict";

    angular.module('app')
        .controller('RoamingProfileCtrl', Accounts);

    Accounts.$inject=[];

    /**
     @constructor Accounts()
     */
    function Accounts(){

        var accountsVm = this;

        /**
         * Start up logic for controller
         */
        function activate(){

        }

        activate();

    }
})();
