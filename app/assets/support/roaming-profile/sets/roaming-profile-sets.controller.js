/* Controllers */
(function(){
    "use strict";

    angular.module('app')
        .controller('RoamingProfileSetsCtrl', RoamingProfileSets);

    RoamingProfileSets.$inject=['$scope', '$sce', '$compile', 'dataTableConfigService','commonUtilityService', 'loadingState','roamingProfileService','tooltips'];

    /**
     @constructor RoamingProfileSets()
     @param {object} $scope - Angular.js $scope provider
     @param {object} $sce - Angular.js $sce provider (Strict Contextual Escaping)
     @param {function} $compile - Angular.js $sanitize provider
     @param {object} dataTableConfigService - Factory Object
     */
    function RoamingProfileSets($scope, $sce, $compile, dataTableConfigService, commonUtilityService, loadingState,roamingProfileService,tooltips){

        var roamingProfileSetVm = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            clearDataTableStatus(['DataTables_roamingProfileSetsDataTable_']);

            $scope.emptyTableMessage = "No Roaming Profile sets are available.";

            // Controller As
            $scope.modalObj = {};
            $scope.enterpriseObj = {};
            // DataTable
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableHeaders = [
                {value: "Set Name", DbValue: "roamingProfileSetName"},
                {value: "Enterprises", DbValue: "accountsCount"},
                {value: "SIMs", DbValue: "simsCount"},
                {value: "IMSI Profiles", DbValue: "imsiProfileList"},
                {value: "Description", DbValue: "description"},
                {value: "Action", DbValue: ""}
            ];
            $scope.dataTableObj.tableID = "roamingProfileSetsDataTable";

            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.setOLanguage($scope.dataTableObj.dataTableOptions,$scope.dataTableObj.tableID,$scope.emptyTableMessage);
            tooltips.initTooltips('[data-toggle="tooltip"]');
            $scope.$on('$destroy', function () { tooltips.destroyTooltips('[data-toggle="tooltip"]'); });
        }

        activate();

        /**
         * @function _setDataTableOptions()
         * @desc - function to set data table. This keeps the activate() function clean.
         * @private
         */
        function _setDataTableOptions(){
            return {
                ordering: false,
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'RPSID_'+data.roamingProfileSetId);
                    $compile(row)($scope);
                },
                //Empty array must be declared for Columns display
                columns: [],
                columnDefs: [{
                    targets: 0,
                    render: function (data, type, row) {
                        return '<a ui-sref="support.roamingProfile.set({RPSetId:\''+row.roamingProfileSetId+'\'})" class="cursor">'+row.roamingProfileSetName+'</a>';
                    }
                }, {
                    targets: 1,
                    render: function (data, type, row) {
                        if(row.accountsCount>0)
                        return '<a href="javascript:void(0)" ng-click=\'showEnterprises(\"'+ row.roamingProfileSetId + '\");\'>'+row.accountsCount+'</a>';
                        return '<span>'+row.accountsCount+'</span>';
                    }
                }, {
                    targets: 2,
                    render:function (data, type, row){
                        return '<span>'+row.simsCount+'</span>';
                    }
                },{
                    targets: 3,
                    render:function (data, type, row){
                        return '<span>'+row.imsiProfileList+'</span>';
                    }
                },{
                    targets: 4,
                    render:function (data, type, row){
                        if(row.description.length >100)
                            return '<span class="cursor-pointer" data-toggle="tooltip" gs-tooltip data-placement="top" title="'+row.description+'">'+row.description.substring(0,100)+'...</span>';
                        else
                            return '<span>'+row.description+'</span>';
                    }
                },{
                    targets: 5,
                    render:function (data, type, row){
                        return '<a href="javascript:void(0)" ng-show="{{'+row.revisionsCount+' == 0}}" ng-click=\'showDeleteSetModal(\"'+ row.roamingProfileSetId + '\");\' gs-has-permission="DELETE_ROAMING_PROFILE_SET"> Delete</a>';
                    }
                }
                ],
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {
                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);

                    roamingProfileService.getRoamingProfileSetsList(queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
                        $scope.roamingProfileSetsList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.roamingProfileSetsList.length == 0);
                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }

                    }).error(function (data) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.roamingProfileSetsList ||  $scope.roamingProfileSetsList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"Roaming profile sets");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });
                }
            };
        }

        $scope.showCreateRoamingProfileSetModal = function(){

            $scope.modalObj.updateAlertModal({
                header: 'Create roaming profile set',
                cancel: 'Close',
                submit: 'Create'
            },submitCreateRoamingProfileSet);
            $("#createRoamingProfileSetModal").modal("show");

        };

        $scope.showEnterprises = function(RPSetId){

            $scope.enterpriseObj.updateEnterpriseModal({
                header: 'Enterprises',
                submit: 'Close'
            },undefined,true,RPSetId);
            $("#enterpriseModal").modal("show");

        };

        function submitCreateRoamingProfileSet(roamingProfileSet){
            roamingProfileService.createRoamingProfileSet(roamingProfileSet).success(function (response) {
                commonUtilityService.showSuccessNotification("Roaming profile set has been created successfully.");
                $scope.dataTableObj.refreshDataTable(true);
            }).error(function (data) {
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

        $scope.showDeleteSetModal = function(setId){

            var msg = "Deleting roaming profile set cannot be undone.";
            var attr = {
                header: 'Confirm Deletion',
                cancel: 'Cancel',
                submit: 'Delete'
            };
            var func = function(){deleteSet(setId);};

            $scope.verificationModalObj.updateVerificationModel(attr,func);
            $scope.verificationModalObj.addVerificationModelMessage(msg);

            $('#verificationModal').modal('show');

        };
        function deleteSet(setId){
            roamingProfileService.deleteRoamingProfileSet(setId).success(function (response) {
                commonUtilityService.showSuccessNotification("Roaming profile set has been deleted successfully.");
                $scope.dataTableObj.refreshDataTable(true);
            }).error(function (data) {
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

    }
})();
