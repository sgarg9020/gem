'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('CreateRoamingProfileSetModal', CreateRoamingProfileSetModal);
    CreateRoamingProfileSetModal.$inject = ['$scope', '$sce', 'modalDetails','commonUtilityService','roamingProfileService'];

    /**
     @constructor CreateRoamingProfileSetModal()
     @param {object} $scope - Angular.js $scope
     @param {object} $sce - Angular.js strict contextual escaping service
     @param {object} modalDetails - Factory object
     */
    function CreateRoamingProfileSetModal($scope, $sce, modalDetails,commonUtilityService,roamingProfileService) {

        var createRoamingProfileSetModal = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            createRoamingProfileSetModal.roamingProfileSet = {};
            createRoamingProfileSetModal.profileTypes = [
                {
                    id:"SINGLE_IMSI_PER_COUNTRY",
                    name:"Single Data IMSI per Country"
                },
                {
                    id:"MULTI_IMSI_PER_COUNTRY",
                    name:"Multi Data IMSI per Country"
                }
            ];
            createRoamingProfileSetModal.profileType = createRoamingProfileSetModal.profileTypes[0];

            if(!createRoamingProfileSetModal.IMSIProfiles) {
                roamingProfileService.getAllowedIMSIProfiles().success(function (data) {
                    createRoamingProfileSetModal.IMSIProfiles = [];
                    for (var i=0; i<data.list.length;i++) {
                        createRoamingProfileSetModal.IMSIProfiles.push({name: data.list[i]});
                    }
                }).error(function (data) {
                    commonUtilityService.showErrorNotification(data.errorStr);
                });
            }

            // ModalDetails factory
            modalDetails.scope(createRoamingProfileSetModal, true);
            modalDetails.attributes({
                id: 'createRoamingProfileSetModal',
                formName: 'createRoamingProfileSetModalForm'
            });

            // Strict contextual escaping
            createRoamingProfileSetModal.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};

            // Hooks for the parent
            $scope.$parent.modalObj = $scope.$parent.modalObj || {};
            $scope.$parent.modalObj.updateAlertModal = updateModal;

        }

        function hideModal() {
            angular.element('#' + createRoamingProfileSetModal.modal.id).modal('hide');
        }

        function reSetModal(){
            createRoamingProfileSetModal.roamingProfileSet={};
            if(createRoamingProfileSetModal.IMSIProfiles) {
                for (var i = 0; i < createRoamingProfileSetModal.IMSIProfiles.length; i++) {
                    createRoamingProfileSetModal.IMSIProfiles[i].selected = false;
                }
            }
            createRoamingProfileSetModal.profileType = createRoamingProfileSetModal.profileTypes[0];
        }
        /**
         @function updateModal()
         @param {object} obj - object to change modal attributes in directive
         @param {function} submit - function to invoke on form submit
         */
        function updateModal(obj, submit){

            // Add new modal data
            modalDetails.scope(createRoamingProfileSetModal, false);
            modalDetails.attributes(obj);

            reSetModal();
            modalDetails.submit(function(createRoamingProfileSetModalForm){
                createRoamingProfileSetModal.errorMessage = '';
                $scope.IMSIProfilesErrorMessage = '';

                createRoamingProfileSetModal.roamingProfileSet.profileType = createRoamingProfileSetModal.profileType.id;

                var IMSIProfileList = [];

                    for( var i=0;i<createRoamingProfileSetModal.IMSIProfiles.length;i++){
                        if(createRoamingProfileSetModal.IMSIProfiles[i].selected)
                            IMSIProfileList.push(createRoamingProfileSetModal.IMSIProfiles[i].name)

                }
                createRoamingProfileSetModal.roamingProfileSet.imsiProfileList = IMSIProfileList;

                var formValid = $scope.isFormValid(createRoamingProfileSetModalForm,createRoamingProfileSetModal.roamingProfileSet,true);
                if(formValid &&  IMSIProfileList.length > 0) {
                    submit(createRoamingProfileSetModal.roamingProfileSet);
                    hideModal();
                }else if (formValid && IMSIProfileList.length == 0){
                    $scope.IMSIProfilesErrorMessage = 'At least one IMSI profile must be selected';
                }
            });

        }

        activate();
    }
})();