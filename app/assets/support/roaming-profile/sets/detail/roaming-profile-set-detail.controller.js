/* Controllers */
(function(){
    "use strict";

    angular.module('app')
        .controller('RoamingProfileSetCtrl', RoamingProfileSet);

    RoamingProfileSet.$inject=['$scope', '$sce', '$compile', 'dataTableConfigService','commonUtilityService', 'loadingState','roamingProfileService','$stateParams','tooltips'];

    /**
     @constructor RoamingProfileSets()
     @param {object} $scope - Angular.js $scope provider
     @param {object} $sce - Angular.js $sce provider (Strict Contextual Escaping)
     @param {function} $compile - Angular.js $sanitize provider
     @param {object} dataTableConfigService - Factory Object
     */
    function RoamingProfileSet($scope, $sce, $compile, dataTableConfigService, commonUtilityService, loadingState,roamingProfileService,$stateParams,tooltips){

        var roamingProfileSetsVm = this;
        $scope.RPSetId = $stateParams.RPSetId;

        /**
         * Start up logic for controller
         */
        function activate(){
            clearDataTableStatus(['DataTables_roamingProfileSetsDataTable_']);

            roamingProfileService.getRoamingProfileSetDetails($scope.RPSetId).success(function(data){
                $scope.roamingProfileSetDetail = data;
                $scope.RPSetName = $scope.roamingProfileSetDetail.roamingProfileSetName;
                $scope.roamingProfileSetDetail.imsiProfiles = $scope.roamingProfileSetDetail.imsiProfileList.join(", ");
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });

            $scope.emptyTableMessage = "No roaming profile revisions available.";

            // Controller As
            $scope.modalObj = {};
            $scope.enterpriseObj = {};
            // DataTable
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableHeaders = [
                {value: "Revision", DbValue: "roamingProfileRevision"},
                {value: "Name", DbValue: "roamingProfileName"},
                {value: "Status", DbValue: "status"},
                {value: "Last Modified(UTC)", DbValue: "updatedTime"},
                {value: "Enterprises", DbValue: "accountsCount"},
                {value: "SIMs", DbValue: "simsCount"},
                {value: "Description", DbValue: "description"},
                {value: "Action", DbValue: ""}
            ];
            $scope.dataTableObj.tableID = "roamingProfileSetsDataTable";

            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.setOLanguage($scope.dataTableObj.dataTableOptions,$scope.dataTableObj.tableID,$scope.emptyTableMessage);

            $scope.$on('$destroy', function () { tooltips.destroyTooltips('[data-toggle="tooltip"]'); });
        }

        activate();

        /**
         * @function _setDataTableOptions()
         * @desc - function to set data table. This keeps the activate() function clean.
         * @private
         */
        function _setDataTableOptions(){
            return {
                ordering: false,
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'RPID_'+data.roamingProfileId);
                    $compile(row)($scope);
                },
                //Empty array must be declared for Columns display
                columns: [],
                columnDefs: [{
                    targets: 0,
                    render: function (data, type, row) {
                        return '<span>'+row.roamingProfileRevision+'</span>';
                    }
                },{
                    targets: 1,
                    render: function (data, type, row) {
                        return '<a ui-sref="support.roamingProfile.version({RPSetId:\''+$scope.RPSetId+'\',versionId:\''+row.roamingProfileId+'\'})">'+row.roamingProfileName+'</a>';
                    }
                }, {
                    targets: 2,
                    render:function (data, type, row){
                        return '<span class="roaming-profile-status '+row.status+'">{{"'+row.status+'" | underscoreless}}</span>';
                    }
                },{
                    targets: 3,
                    render:function (data, type, row){
                        return '<span>'+row.updatedTime+'</span>';
                    }
                },{
                    targets: 4,
                    render:function (data, type, row){
                        if(row.accountsCount>0)
                        return '<a href="javascript:void(0)" ng-click=\'showEnterprises(\"'+ row.roamingProfileId + '\");\'>'+row.accountsCount+'</a>';
                        return '<span>'+row.accountsCount+'</span>';
                    }
                },{
                    targets: 5,
                    render:function (data, type, row){
                        return '<span>'+row.simsCount+'</span>';
                    }
                }, {
                    targets: 6,
                    render: function (data, type, row) {
                        if (row.description.length > 30)
                            return '<span class="cursor-pointer" data-toggle="tooltip" gs-tooltip data-placement="top" title="' + row.description + '">' + row.description.substring(0, 30) + '...</span>';
                        else
                            return '<span>' + row.description + '</span>';
                    }
                },{
                    targets: 7,
                    render:function (data, type, row){
                        return '<a href="javascript:void(0)" ng-show="{{'+row.accountsCount+' == 0}}" ng-click=\'showDeleteVersionModal(\"'+ row.roamingProfileId + '\");\' gs-has-permission="DELETE_ROAMING_PROFILE"> Delete</a>';
                    }
                }
                ],
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {
                    tooltips.destroyTooltips('[data-toggle="tooltip"]');
                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);

                    roamingProfileService.getRoamingProfileRevisionsList($scope.RPSetId,queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
                        $scope.roamingProfileRevisionsList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.roamingProfileRevisionsList.length == 0);
                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }

                    }).error(function (data) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.roamingProfileRevisionsList ||  $scope.roamingProfileRevisionsList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"Roaming profile revision");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });
                    tooltips.initTooltips('[data-toggle="tooltip"]');
                }
            };
        }

        $scope.showCreateRoamingProfileSetModal = function(){

            $scope.modalObj.updateVersionCreateModal({
                header: 'Create roaming profile revision',
                cancel: 'Close',
                submit: 'Create'
            },submitCreateRoamingProfile);
            $("#versionCreateModal").modal("show");

        };

        $scope.showEnterprises = function(versionId){

            $scope.enterpriseObj.updateEnterpriseModal({
                header: 'Enterprises',
                submit: 'Close'
            },undefined,false,versionId);
            $("#enterpriseModal").modal("show");

        };

        $scope.showDeleteVersionModal = function(versionId){

            var msg = "Deleting roaming profile cannot be undone.";
            var attr = {
                header: 'Confirm roaming profile revision deletion',
                cancel: 'Cancel',
                submit: 'Delete'
            };
            var func = function(){deleteVersion(versionId);};

            $scope.verificationModalObj.updateVerificationModel(attr,func);
            $scope.verificationModalObj.addVerificationModelMessage(msg);

            $('#verificationModal').modal('show');

        };
        function deleteVersion(versionId){
            roamingProfileService.deleteRoamingProfile(versionId).success(function (response) {
                commonUtilityService.showSuccessNotification("Roaming profile has been deleted successfully.");
                $scope.dataTableObj.refreshDataTable(true);
            }).error(function (data) {
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

        function submitCreateRoamingProfile(roamingProfile){
                roamingProfileService.createRoamingProfile($scope.RPSetId,roamingProfile).success(function (response) {
                    commonUtilityService.showSuccessNotification("Roaming profile has been created successfully.");
                    $scope.dataTableObj.refreshDataTable(true);
                }).error(function (data) {
                    commonUtilityService.showErrorNotification(data.errorStr);
                });
        }
    }
})();
