/* Controllers */
(function(){
    "use strict";

    angular.module('app')
        .controller('RoamingProfileVersionCountriesCtrl', RoamingProfileVersionCountriesCtrl);

    RoamingProfileVersionCountriesCtrl.$inject=['$scope', '$sce', '$compile', 'dataTableConfigService','commonUtilityService', 'loadingState','roamingProfileService','$stateParams'];

    /**
     @constructor RoamingProfileVersionCountriesCtrl()
     @param {object} $scope - Angular.js $scope provider
     @param {object} $sce - Angular.js $sce provider (Strict Contextual Escaping)
     @param {function} $compile - Angular.js $sanitize provider
     @param {object} dataTableConfigService - Factory Object
     */
    function RoamingProfileVersionCountriesCtrl($scope, $sce, $compile, dataTableConfigService, commonUtilityService, loadingState,roamingProfileService,$stateParams){

        var roamingProfileVersionCountriesVm = this;

        var RPSetId = $stateParams.RPSetId;
        var versionId = $stateParams.versionId;

        /**
         * Start up logic for controller
         */
        function activate(){
            clearDataTableStatus(['DataTables_roamingProfileRevisionCountriesDataTable_']);

            $scope.emptyTableMessage = "No countries available.";

            // Controller As

            // DataTable
            $scope.dataTableObj = {};

            roamingProfileService.getAllowedIMSIProfiles().success(function(data){
                $scope.IMSIProfilesList = roamingProfileService.prepareIMSIProfilesFilters(data.list);
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });

            $scope.dataTableObj.tableHeaders = [
                {value: "Country", DbValue: "countryName",search:true,placeHolder:'Country'},
                {value: "ISO Code", DbValue: "countryCode",search:true},
                {value: "Primary IMSI", DbValue: "imsiProfileName",search:false},
                {value: "Priority", DbValue: "priority",search:false}
            ];
            $scope.dataTableObj.tableID = "roamingProfileRevisionCountriesDataTable";

            var filtersConfig = { imsiProfileName: {type:'url', defaultValue: { name:'ALL', dbRef:'',skipQueryParam:true}, urlUniquePattern:'ct' }};
            dataTableConfigService.configureFilters($scope.dataTableObj, filtersConfig);

            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.setOLanguage($scope.dataTableObj.dataTableOptions,$scope.dataTableObj.tableID,$scope.emptyTableMessage);
        }

        activate();

        /**
         * @function _setDataTableOptions()
         * @desc - function to set data table. This keeps the activate() function clean.
         * @private
         */
        function _setDataTableOptions(){
            return {
                ordering: false,
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'RPVCID_'+data.accountId);
                    $compile(row)($scope);
                },
                //Empty array must be declared for Columns display
                columns: [],
                columnDefs: [{
                    targets: 0,
                    render: function (data, type, row) {
                        return '<span>'+row.countryName+'</span>';
                    }
                }, {
                    targets: 1,
                    render: function (data, type, row) {
                        return '<span>'+row.countryCode+'</span>';
                    }
                }, {
                    targets: 2,
                    render:function (data, type, row){
                        return '<span>'+row.imsiProfileName+'</span>';
                    }
                },{
                    targets: 3,
                    render:function (data, type, row){
                        return '<span>'+row.priority+'</span>';
                    }
                }
                ],
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {

                    if(aoData){
                        roamingProfileVersionCountriesVm.searchValue = (aoData[5])?aoData[5].value.value:'';
                    }

                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);

                    queryParam.details = 'COUNTRY_IMSI_PROFILES';
                    queryParam.excludeNonPrimary = true;

                    roamingProfileService.getRoamingProfileCoverageInfo(RPSetId,versionId,queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
                        $scope.roamingProfileVersionCountriesList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.roamingProfileVersionCountriesList.length == 0);
                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }

                    }).error(function (data) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.roamingProfileVersionCountriesList ||  $scope.roamingProfileVersionCountriesList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"Roaming profile version countries");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });
                }
            };
        }

        $scope.$parent.refreshCountries = function(){
            $scope.dataTableObj.refreshDataTable();
        };
    }
})();
