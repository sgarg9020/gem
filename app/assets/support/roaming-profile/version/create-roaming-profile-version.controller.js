'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('CreateRoamingProfileVersionModal', CreateRoamingProfileVersionModal);
    CreateRoamingProfileVersionModal.$inject = ['$scope', '$sce', 'modalDetails','commonUtilityService'];

    /**
     @constructor VersionCreateModal()
     @param {object} $scope - Angular.js $scope
     @param {object} $sce - Angular.js strict contextual escaping service
     @param {object} modalDetails - Factory object
     @param {object} formMessage - Factory object
     @param {object} groupsService - Factory object
     */
    function CreateRoamingProfileVersionModal($scope, $sce, modalDetails,commonUtilityService) {

        var versionCreateModal = this;

        /**
         * Start up logic for controller
         */
        function activate(){

            versionCreateModal.roamingProfile = {};
            // ModalDetails factory
            modalDetails.scope(versionCreateModal, true);
            modalDetails.attributes({
                id: 'versionCreateModal',
                formName: 'versionCreateModalForm'
            });

            // Strict contextual escaping
            versionCreateModal.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};

            // Hooks for the parent
            $scope.$parent.modalObj = $scope.$parent.modalObj || {};
            $scope.$parent.modalObj.updateVersionCreateModal = updateModal;

        }

        function hideModal() {
            angular.element('#' + $scope.versionCreateModal.modal.id).modal('hide');
        }

        function resetModal(){
            versionCreateModal.roamingProfile = {};
        }
        /**
         @function updateModal()
         @param {object} obj - object to change modal attributes in directive
         @param {function} submit - function to invoke on form submit
         */
        function updateModal(obj, submit){

            // Add new modal data
            modalDetails.scope(versionCreateModal, false);
            modalDetails.attributes(obj);
            resetModal();
            modalDetails.submit(function(versionCreateModalForm){
                versionCreateModal.errorMessage = '';
                if($scope.isFormValid(versionCreateModalForm)) {
                    submit(versionCreateModal.roamingProfile);
                    hideModal();
                }
            });

        }

        activate();
    }
})();