/* Controllers */
(function(){
    "use strict";

    angular.module('app')
        .controller('RoamingProfileVersionCtrl', RoamingProfileVersionCtrl);

    RoamingProfileVersionCtrl.$inject=['$scope','commonUtilityService','roamingProfileService','$stateParams','$q'];

    /**
     @constructor RoamingProfileSets()
     @param {object} $scope - Angular.js $scope provider
     @param {object} commonUtilityService - Factory Object
     @param {function} roamingProfileService - Factory Object
     @param {object} $stateParams - Angular.js $stateParams provider
     */
    function RoamingProfileVersionCtrl($scope, commonUtilityService,roamingProfileService,$stateParams,$q){

        var roamingProfileVersionVm = this;
        $scope.RPSetId = $stateParams.RPSetId;
        $scope.versionId = $stateParams.versionId;
        $scope.$parent.activateRoamingProfileVersionCtrl = activate;

        /**
         * Start up logic for controller
         */
        function activate(){

            $scope.warningModalObj = {disableButtons: false};

            $scope.coverageInfoExportUrl = roamingProfileService.getRoamingProfileExportURI($scope.RPSetId,$scope.versionId);
            roamingProfileVersionVm.statuses =[];
            var coverageQueryParams = {
                count:10000,
                startIndex:0
            };
            coverageQueryParams.details = 'ONLY_IMSI_PROFILES';
            coverageQueryParams.excludeNonPrimary = true;


            $q.all([roamingProfileService.getRoamingProfileDetails($scope.RPSetId,$scope.versionId),roamingProfileService.getRoamingProfileCoverageInfo($scope.RPSetId,$scope.versionId,coverageQueryParams)]).then(function (response) {

                roamingProfileVersionVm.roamingProfileInfo = response[0].data;
                roamingProfileVersionVm.roamingProfileInfoEdit = angular.copy(response[0].data);

                $scope.RPSetName = roamingProfileVersionVm.roamingProfileInfo.roamingProfileSetName;
                $scope.RPName = roamingProfileVersionVm.roamingProfileInfo.roamingProfileName;
                if(roamingProfileVersionVm.roamingProfileInfo.status == "PENDING"){
                    roamingProfileVersionVm.statuses =["PENDING","PENDING_HLR_UPDATE"];
                }else if(roamingProfileVersionVm.roamingProfileInfo.status == "PENDING_HLR_UPDATE"){
                    roamingProfileVersionVm.statuses =["PENDING_HLR_UPDATE","PUBLISHED"];
                }else{
                    roamingProfileVersionVm.statuses =["PUBLISHED"];
                }

                roamingProfileVersionVm.roamingProfileInfoEdit.IMSIProfileList = angular.copy(response[1].data.list);
                roamingProfileVersionVm.IMSIProfiles = roamingProfileVersionVm.roamingProfileInfoEdit.IMSIProfileList;

            }, function (error) {
                commonUtilityService.showErrorNotification((error && error.data)?(error.data.errorStr):null);
            });

            var accountListQueryParams = {
                count:10000,
                startIndex:0
            };
            roamingProfileVersionVm.enterprises =[];
            roamingProfileService.getEnterprisesOfRoamingProfile($scope.versionId,accountListQueryParams).success(function(data){
                roamingProfileVersionVm.enterprises = data.list;
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

        $scope.showUploadRoamingProfileModal = function(){
            $("#uploadRoamingProfileModal").modal("show");
        };
        
        $scope.saveIMSIProfile = function(IMSIProfileForm){

            roamingProfileVersionVm.IMSIPRofileErrorMessage = '';
            if ($scope.isFormValid(IMSIProfileForm)) {
                var request = {};
                request.type = roamingProfileVersionVm.roamingProfileInfoEdit.type;
                request.status = roamingProfileVersionVm.roamingProfileInfoEdit.status;
                request.description = roamingProfileVersionVm.roamingProfileInfoEdit.description;
                if(roamingProfileVersionVm.roamingProfileInfo.status == 'PENDING_HLR_UPDATE'){
                    request.imsiProfileHlrIdList = [];
                    for(var i=0;i<roamingProfileVersionVm.roamingProfileInfoEdit.IMSIProfileList.length;i++){
                        request.imsiProfileHlrIdList.push({
                            type : "ImsiProfileHlrId",
                            name : roamingProfileVersionVm.roamingProfileInfoEdit.IMSIProfileList[i].imsiProfileName,
                            "HLR-2G3G-ID" : roamingProfileVersionVm.roamingProfileInfoEdit.IMSIProfileList[i]['HLR-2G3G-ID'],
                            "HLR-4G-ID" : roamingProfileVersionVm.roamingProfileInfoEdit.IMSIProfileList[i]['HLR-4G-ID']
                        })
                    }

                }
                roamingProfileService.editRoamingProfile($scope.RPSetId,$scope.versionId,request).success(function (response) {
                    commonUtilityService.showSuccessNotification("Roaming Profile details have been updated successfully.");
                    activate();
                }).error(function (data) {
                    commonUtilityService.showErrorNotification(data.errorStr);
                });
            }
        };

        roamingProfileVersionVm.exportStartMessage = function () {
            commonUtilityService.showSuccessNotification("Export will start in few seconds.");
        };
        activate();
    }
})();
