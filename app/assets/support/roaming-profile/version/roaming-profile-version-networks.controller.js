/* Controllers */
(function(){
    "use strict";

    angular.module('app')
        .controller('RoamingProfileVersionNetworksCtrl', RoamingProfileVersionNetworksCtrl);

    RoamingProfileVersionNetworksCtrl.$inject=['$scope', '$sce', '$compile', 'dataTableConfigService','commonUtilityService', 'loadingState','roamingProfileService','$stateParams'];

    /**
     @constructor RoamingProfileVersionNetworksCtrl()
     @param {object} $scope - Angular.js $scope provider
     @param {object} $sce - Angular.js $sce provider (Strict Contextual Escaping)
     @param {function} $compile - Angular.js $sanitize provider
     @param {object} dataTableConfigService - Factory Object
     */
    function RoamingProfileVersionNetworksCtrl($scope, $sce, $compile, dataTableConfigService, commonUtilityService, loadingState,roamingProfileService,$stateParams){

        var roamingProfileVersionNetworksVm = this;
        var RPSetId = $stateParams.RPSetId;
        var versionId = $stateParams.versionId;

        /**
         * Start up logic for controller
         */
        function activate(){
            clearDataTableStatus(['DataTables_roamingProfileRevisionNetworksDataTable_']);

            $scope.emptyTableMessage = "No networks available.";

            // Controller As
            $scope.dataFilterList = [{ name:'ALL', dbRef:'',skipQueryParam:true},{ name:'Enabled', dbRef:'true'}];

            roamingProfileService.getAllowedIMSIProfiles().success(function(data){
                $scope.IMSIProfilesList = roamingProfileService.prepareIMSIProfilesFilters(data.list);
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });
            commonUtilityService.getCountries().success(function(data){
                $scope.countriesList = roamingProfileService.prepareCountriesFilters(data.countryList);
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });

            // DataTable
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableHeaders = [
                {value: "Coverage Key", DbValue: "coverageKey",search:true,placeHolder:'Key'},
                {value: "MCC-MNC", DbValue: "mccMnc",search:true,placeHolder:'MCC-MNC'},
                {value: "TADIG", DbValue: "tadigCode"},
                {value: "Carrier", DbValue: "name"},
                {value: "Country", DbValue: "countryName"},
                {value: "ISO Code", DbValue: "countryCode"},
                {value: "2G", DbValue: "2GEnabled"},
                {value: "3G", DbValue: "3GEnabled"},
                {value: "4G", DbValue: "4GEnabled"},
                {value: "IMSI Profile", DbValue: "imsiProfileName"},
                {value: "Data", DbValue: "dataEnabled"},
                {value: "Signalling", DbValue: "signallingEnabled"}
            ];
            $scope.dataTableObj.tableID = "roamingProfileRevisionNetworksDataTable";

            var filtersConfig = { imsiProfileName: {type:'url', defaultValue: { name:'ALL', dbRef:'',skipQueryParam:true}, urlUniquePattern:'nw'  },countryCode:{type:'url', defaultValue: { name:'ALL', dbRef:'',skipQueryParam:true}},data:{type:'url', defaultValue: { name:'ALL', dbRef:'',skipQueryParam:true}}};
            dataTableConfigService.configureFilters($scope.dataTableObj, filtersConfig);

            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.setOLanguage($scope.dataTableObj.dataTableOptions,$scope.dataTableObj.tableID,$scope.emptyTableMessage);
            
        }

        activate();

        /**
         * @function _setDataTableOptions()
         * @desc - function to set data table. This keeps the activate() function clean.
         * @private
         */
        function _setDataTableOptions(){
            return {
                ordering: false,
                order: [[ 0, "asc" ]],
                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'RPVCID_'+data.accountId);
                    $compile(row)($scope);
                },
                //Empty array must be declared for Columns display
                columns: [],
                columnDefs: [{
                    targets: 0,
                    render: function (data, type, row) {
                        return '<span>'+row.coverageKey+'</span>';
                    }
                },{
                    targets: 1,
                    render: function (data, type, row) {
                        return '<span>'+row.mcc+'-'+row.mnc+'</span>';
                    }
                }, {
                    targets: 2,
                    render: function (data, type, row) {
                        return '<span>'+row.tadigCode+'</span>';
                    }
                }, {
                    targets: 3,
                    render:function (data, type, row){
                        return '<span>'+row.name+'</span>';
                    }
                },{
                    targets: 4,
                    render:function (data, type, row){
                        return '<span>'+row.countryName+'</span>';
                    }
                },{
                    targets: 5,
                    render:function (data, type, row){
                        return '<span>'+row.countryCode+'</span>';
                    }
                },{
                    targets: 6,
                    render:function (data, type, row){
                        var dat = 'N';
                        if(row["2GEnabled"]){
                            dat = 'Y';
                        }
                        return '<span>'+dat+'</span>';
                    }
                },{
                    targets: 7,
                    render:function (data, type, row){
                        var dat = 'N';
                        if(row["3GEnabled"]){
                            dat = 'Y';
                        }
                        return '<span>'+dat+'</span>';
                    }
                },{
                    targets: 8,
                    render:function (data, type, row){
                        var dat = 'N';
                        if(row["4GEnabled"]){
                            dat = 'Y';
                        }
                        return '<span>'+dat+'</span>';
                    }
                },{
                    targets: 9,
                    render:function (data, type, row){
                        return '<span>'+row.imsiProfileName+'</span>';
                    }
                },{
                    targets: 10,
                    render:function (data, type, row){
                        var dat = 'N';
                        if(row.dataEnabled){
                            dat = 'Y';
                        }
                        return '<span>'+dat+'</span>';
                    }
                },{
                    targets: 11,
                    render:function (data, type, row){
                        var dat = 'N';
                        if(row.signallingEnabled){
                            dat = 'Y';
                        }
                        return '<span>'+dat+'</span>';
                    }
                }
                ],
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {

                    if(aoData){
                        roamingProfileVersionNetworksVm.searchValue = (aoData[5])?aoData[5].value.value:'';
                    }

                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);

                    queryParam.details = 'COUNTRY_IMSI_PROFILE_CARRIERS';
                    if(queryParam.data){
                        queryParam.excludeNonPrimary = true;
                    }
                    delete queryParam.data;

                    roamingProfileService.getRoamingProfileCoverageInfo(RPSetId,versionId,queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
                        $scope.roamingProfileVersionNetworksList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.roamingProfileVersionNetworksList.length == 0);
                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }

                    }).error(function (data) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.roamingProfileVersionNetworksList ||  $scope.roamingProfileVersionNetworksList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"Roaming profile networks");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });
                }
            };
        }
        $scope.$parent.refreshNetworks = function(){
            $scope.dataTableObj.refreshDataTable();
        };

    }
})();
