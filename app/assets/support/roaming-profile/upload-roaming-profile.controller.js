'use strict';

/* Controllers */
(function(){
    angular.module('app')
        .controller('UploadRoamingProfileCtrl', UploadRoamingProfile);

    UploadRoamingProfile.$inject=['$scope','$rootScope', 'modalDetails', 'analytics','$state'];

    /**
     * @func BulkUploadUsers()
     * @param $scope
     * @param $rootScope
     * @param modalDetails
     * @param analytics
     * @constructor
     */
    function UploadRoamingProfile($scope,$rootScope, modalDetails, analytics,$state){

        var uploadRoamingProfile = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            Dropzone.autoDiscover = false;

            uploadRoamingProfile.RPSetId = $scope.$parent.RPSetId;
            uploadRoamingProfile.versionId = $scope.$parent.versionId;
            uploadRoamingProfile.warningModalObj = $scope.$parent.warningModalObj;
            uploadRoamingProfile.warningModalObj.attemptToUpload = attemptToUpload;
            
            $scope.dropzoneConfig = {
                autoProcessQueue: false,
                maxFiles: 1,
                method: 'put',
                parallelUploads: 1,
                maxFilesize: 25,
                addRemoveLinks: false,
                clickable: true,
                acceptedFiles: "text/csv",
                headers:{ "token" : sessionStorage.getItem( "token") },
                url:ENTERPRISE_PLAN_API_BASE_V3+"rpset/" + uploadRoamingProfile.RPSetId + "/rp/"+ uploadRoamingProfile.versionId + "/upload",
                dictInvalidFileType:'File type not supported'
            };
            /* ModalDetails factory */
            modalDetails.scope(uploadRoamingProfile, true);
            modalDetails.attributes({
                id: 'uploadRoamingProfileModal',
                formName: 'uploadFile',
                cancel: 'Clear',
                submit: 'Upload'
            });
            modalDetails.cancel(reset);
            modalDetails.submit(attemptToUpload);
            uploadRoamingProfile.successCallback = function(){
                $scope.activateRoamingProfileVersionCtrl();
                $scope.refreshCountries();
                $scope.refreshNetworks();
            };
            uploadRoamingProfile.cancelUpload = cancelUpload;
        }

        /**
         @function reset()
         - call a directive level function
         */
        function reset(){
            $rootScope.$broadcast('clearDropzoneFiles');
        }

        /**
         @function attemptToUpload()
         - call a directive level function
         */
        function attemptToUpload(ignoreWarnings){
            analytics.sendEvent({
                category: "Operations",
                action: "upload",
                label: "Roaming Profile"
            });
            $rootScope.$broadcast('submitDropzoneFiles',{ignoreWarnings:ignoreWarnings});
        }

        /**
         @function attemptToUpload()
         - call a directive level function
         */
        function cancelUpload(){
            $rootScope.$broadcast('cancelDropzoneUpload');
        }

        activate();

    }
})();
