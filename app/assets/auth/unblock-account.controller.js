'use strict';

/* Controllers */
(function(){
    angular.module('app')
        .controller('unblockAccountCtrl', UnblockAccount);

    UnblockAccount.$inject = ['$scope','$stateParams','$sce','AuthenticationService', 'loadingState'];

    /**
     @constructor Login()
     @param {object} $scope - Angular.js $scope
     @param {object} $location - Angular.js $location service
     @param {object} formMessage - Factory object
     */
    function UnblockAccount($scope,$stateParams,$sce,authenticationService,loadingState){

        var unblockAccountObj = this;

        $scope.resetPwd = {};

        /**
         * Start up logic for controller
         */
        function activate(){
            $scope.resetPwd.resetPassword = unblockAccount;
            $scope.resetPwd.title = "Unblock Account";
            $scope.resetPwd.isTokenValid = true;
            $scope.resetPwd.errorMessage ='';
            $scope.resetPwd.successMessage = '';
        }

        function unblockAccount(rsertForm,event) {

            if(event && event.which != 13){
                return;
            }

            if ($scope.isFormValid(rsertForm)) {
                authenticationService.unblockAccount(rsertForm.newPassword.$modelValue, $stateParams.data).success(function (response) {
                    $scope.resetPwd.isTokenValid = false;
                    $scope.resetPwd.message = $sce.trustAsHtml('Your account is unblocked. Please <a href="#/auth/login">log in</a> with new credentials.');

                }).error(function (response) {
                    if(response.errorStr == 'Invalid token'){
                        $scope.resetPwd.isTokenValid = false;
                        $scope.resetPwd.message = $sce.trustAsHtml('Unblock account link has expired. Please <a href="#/auth/forgot-password">Reset Password</a> and submit unblock account request.');
                    }else{
                        $scope.resetPwd.errorMessage = response.errorStr;
                    }

                });
            }
        }

        activate();
        loadingState.hide();
    }
})();