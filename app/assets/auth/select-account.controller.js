'use strict';

/* Controllers */
(function () {
    angular.module('app')
        .controller('SelectAccountCtrl', SelectAccount);

    SelectAccount.$inject = ['$scope','$location', 'AuthenticationService','$rootScope','loadingState'];

    /**
     @constructor SelectAccount()
     @param {object} $scope - Angular.js $scope
     */

    function SelectAccount($scope,$location, authenticationService,$rootScope,loadingState) {

        var selectAccountObj = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            // Quirk to remove .sidebar-visible from body
            angular.element('body').removeClass('sidebar-visible');

            if($rootScope.userAccount)
            {
                $scope.accountList = $rootScope.userAccount;
                delete $rootScope['userAccount'];
            }else if($rootScope.isGSAdmin){
                getUserSubAccounts();
            }
            else{
                getUserAccount();
            }
        }

        function getUserAccount() {
            authenticationService.getUserAccount(sessionStorage.getItem("userId")).success(function (data) {
                var enterpriseAccountDetails = data.accountDetails.enterpriseAccountDetails;
                var validLoginAccounts = [];
                if (enterpriseAccountDetails.length > 1) {
                    var i = 0;
                    for (i; i < enterpriseAccountDetails.length; i++) {
                        var parentAccountDetails = enterpriseAccountDetails[i].parentAccountDetails;
                        var j = 0;
                        for (j; j < parentAccountDetails.length; j++) {
                            var account = parentAccountDetails[j];
                            if (account.role === 'ENTERPRISE_ADMIN' || account.role === 'ENTERPRISE_SUPPORT_ADMIN') {
                                validLoginAccounts.push(account);
                            }
                        }

                    }

                }
                if (validLoginAccounts.length > 1) {
                    $scope.accountList = validLoginAccounts;
                }
            }).error(function (data) {
                console.log('failed to user account' + data);
                selectAccountObj.errorMessage = data.errorStr;
            });
        }

        function getUserSubAccounts(){
            var queryParam = {};
            queryParam.startIndex = 0;
            queryParam.count = 10000;
            queryParam.sortBy = 'accountName';
            queryParam.sortDirection = 'ASC';
            queryParam.depth = 'IMMEDIATE';
            authenticationService.getSubAccounts(sessionStorage.getItem("rootAccountId"),queryParam).success(function (data) {
                $scope.accountList = data.list;
            }).error(function (data) {
                //$location.path('/auth/login');
                console.log('failed to get sub accounts' + data);
                selectAccountObj.errorMessage = data.errorStr;
            });
        }

        $scope.selectAccount = function (account) {
            var isGSAdmin = sessionStorage.getItem("isGSAdmin") === "true" ? true : false;
            if (isGSAdmin) {
                sessionStorage.setItem("accountId", account.accountId);
                isAlertsFeatureEnabled(account.accountId);
            } else {
                authenticationService.storePermissionsInSession(account.permissionList);
                authenticationService.enterpriseLogin(account.accountId, sessionStorage.getItem("userId")).success(function (data) {
                    $location.path('/app/home');
                }).error(function (data) {
                    //$location.path('/auth/login');
                    console.log('enterprise account authentication failure' + data);
                    selectAccountObj.errorMessage = data.errorStr;
                });
            }
        };

        function isAlertsFeatureEnabled(accountId) {
            var queryParam = {};
            queryParam.details = 'alertVersionSupported|pricingModelVersionSupported';

            /* get enterprise details */
            // need to pass query param once the defect is fixed from backend
            authenticationService.getEnterpriseAccountDetails(accountId/*,queryParam*/).success(function (response) {
                authenticationService.setAlertsFeatureEnabled(response.alertVersionSupported);
                authenticationService.setPricingModelVersion(response.pricingModelVersionSupported);
                $location.path('/app/home');
            }).error(function (data) {
                console.log("error while fetching enterprise details");
                selectAccountObj.errorMessage = data.errorStr;
            });
        }

        activate();
        loadingState.hide();

    }
})();