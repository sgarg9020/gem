'use strict';

/* Controllers */
(function(){
	angular.module('app')
	    .controller('ForgotPasswordCtrl', ForgotPassword);

	ForgotPassword.$inject = ['$scope','AuthenticationService', 'loadingState'];

	/**
		@constructor ForgotPassword()
		@param {object} $scope - Angular.js $scope
		@param {object} formMessage - Factory object
	*/   
	function ForgotPassword($scope,authenticationService,loadingState){
		
		var forgotPassword = this;

		/**
		 * Start up logic for controller
		 */
		function activate(){
			forgotPassword.sendEmail = sendEmail;

			forgotPassword.widget = null;

			window.captchaLoadCallback = function()
			{
				var event = document.createEvent('Event');
				event.initEvent('CaptchaLoad', true, true);
				document.dispatchEvent(event);
			};

			document.addEventListener('CaptchaLoad', function (e) {
				forgotPassword.widgetId= grecaptcha.render('captcha-div', {
					'sitekey' : '6LdL3AgTAAAAAEznjTYkvV03gVKFzZQ_YSE0PXdH'
				});
			}, false);

			if(!DISABLE_CAPTCHA){
				if(!window["grecaptcha"])
				{
					var scriptElement =document.createElement('script');
					var firstScriptElement=document.getElementsByTagName('script')[0];

					scriptElement.async=1;
					scriptElement.src="//www.google.com/recaptcha/api.js?onload=captchaLoadCallback&render=explicit";

					firstScriptElement.parentNode.insertBefore(scriptElement,firstScriptElement);
				}
				else
				{
					forgotPassword.widgetId= grecaptcha.render('captcha-div', {
						'sitekey' : '6LdL3AgTAAAAAEznjTYkvV03gVKFzZQ_YSE0PXdH'
					});
				}
			}

		}

		/**
			@function attemptToSendEmail
			@param {object} user - The user object
			
			-Attempt to authenticate a user  
		 */
		function sendEmail(forgotPasswordForm){
			/*
				Any failure to find an email shall trigger an error
				Use the factory formMessage to update the message
			*/
			forgotPassword.successMessage = '';
			forgotPassword.errorMessage = "";
			if(!DISABLE_CAPTCHA){
				if($scope.isFormValid(forgotPasswordForm)){
					var emailId = forgotPasswordForm.email.$modelValue;
					if(grecaptcha.getResponse(forgotPassword.widgetId) == ""){
						forgotPasswordForm.captchaErrorStr = "Please complete CAPTCHA verification.";
						return;
					}
					authenticationService.forgotPasswordWithCaptcha(emailId,grecaptcha.getResponse(forgotPassword.widgetId))
						.success(function (data) {
								grecaptcha.reset(forgotPassword.widgetId);
								forgotPassword.successMessage = "Instructions to reset your password have been sent to " + emailId;
						})
						.error(function (data, status) {
							grecaptcha.reset(forgotPassword.widgetId);
							if(status==400)
							{
									forgotPassword.successMessage = "Instructions to reset your password have been sent to " + emailId;
							}
							else{
									forgotPassword.errorMessage = data.errorStr;
							}
							$(document).scrollTop(0);
						});
				}
			}
			else{
				if($scope.isFormValid(forgotPasswordForm)) {
					var emailId = forgotPasswordForm.email.$modelValue;
					authenticationService.forgotPassword(emailId).success(function () {
							forgotPassword.successMessage = "Instructions to reset your password have been sent to " + emailId;
						})
						.error(function (data, status) {
							if(status==400)
							{
								forgotPassword.successMessage = "Instructions to reset your password have been sent to " + emailId;
							}
							else{
								forgotPassword.errorMessage = data.errorStr;
							}
						});
				}
			}

		}

		activate();

		$scope.$on('$destroy',function() {
			if (window["grecaptcha"])
				grecaptcha.reset();
		});

		loadingState.hide();
	}
})();