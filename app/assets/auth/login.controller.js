'use strict';

/* Controllers */
(function () {
    angular.module('app')
        .controller('LoginCtrl', Login);

    Login.$inject = ['$scope', '$location', 'AuthenticationService', '$rootScope', 'loadingState','$state','$http','analytics','commonUtilityService'];

    /**
     @constructor Login()
     @param {object} $scope - Angular.js $scope
     @param {object} $location - Angular.js $location service
     @param {object} formMessage - Factory object
     */
    function Login($scope, $location, authenticationService, $rootScope,loadingState,$state,$http,analytics,commonUtilityService) {

        var login = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            login.authenticate = authenticate;
            login.loginInternal = loginInternal;

            if(!$rootScope.isStorageNotSupported)
              setDefaultConfig();

           var loginInternalPath = $location.path().indexOf('/auth/loginInternal')!= -1;
            if(loginInternalPath && authenticationService.isUserSessionValid()){
                commonUtilityService.showSuccessNotification("Password has been changed successfully."); //this notification was not shown from force change password screen, so added here.
                var userId = sessionStorage.getItem('userId');
                loginInternal(userId);
            }
            // Quirk to remove .sidebar-visible from body
            angular.element('body').removeClass('sidebar-visible');
        }

        /**
         @function attemptToAuthenticate()
         @param {object} loginForm
         - attempt to authenticate a user
         */
        function authenticate(loginForm) {
            $rootScope.formErrorMsg = "";
            login.errorMessage = "";
            if ($scope.isFormValid(loginForm)) {
                var label = (loginForm.emailId.$modelValue.indexOf("gigsky.com") != -1) ? 'internal' : 'external';
                sessionStorage.setItem('usageDimensionAnalytics', label);
                authenticationService.authenticate(loginForm.emailId.$modelValue, loginForm.password.$modelValue).success(function (response) {
                    analytics.setDimension(1,label);
                    analytics.sendEvent({
                        category: "Behaviour",
                        action: "Login",
                        label: label
                    });

                    if(response && response.forcePasswordChange){
                        var token = sessionStorage.getItem("token");

                        //removing token and userToken and saving it in tempToken for Force change password as the existing token is invalid once the password is changed.
                        //But we are saving the existing token in tempToken as it needed for querying the change password API.

                        sessionStorage.removeItem("token");
                        sessionStorage.removeItem("userToken");
                        sessionStorage.setItem("tempToken",token);
                        $location.path('/auth/force-change-password');
                    }else {
                        loginInternal(response.userId);
                    }
                }).error(function (response) {

                    analytics.sendEvent({
                        category: "Behaviour",
                        action: "Login Failure",
                        label: label
                    });

                    console.log('authentication failure' + response);
                    login.errorMessage = response.errorStr;
                });
            }

            // Quirk to remove .sidebar-visible from body
            angular.element('body').removeClass('sidebar-visible');
        }

        function loginInternal(userId){
            authenticationService.getUserAccount(userId).success(function (data) {
                var enterpriseAccountDetails = data.accountDetails.enterpriseAccountDetails;
                var validEnterpriseAccounts = [];
                var validSupportAccounts = [];

                if (enterpriseAccountDetails.length > 0) {
                    var i = 0;
                    for (i; i < enterpriseAccountDetails.length; i++) {
                        var parentAccountDetails = enterpriseAccountDetails[i].parentAccountDetails;
                        var j = 0;
                        for (j; j < parentAccountDetails.length; j++) {
                            var account = parentAccountDetails[j];
                            if (isGAPLoginAllowed(account.permissionList)) {
                                validSupportAccounts.push(account);
                            } else if(account.role !== 'ENTERPRISE_SIM_USER'){
                                $scope.setHierarchicalData(account.accountType == RESELLER_ACCOUNT_TYPE, account.accountType == RESELLER_ACCOUNT_TYPE, account.accountType == SUB_ACCOUNT_TYPE);
                                if(account.accountType == RESELLER_ACCOUNT_TYPE)
                                    sessionStorage.setItem('rootResellerAccountId', account.accountId);
                                validEnterpriseAccounts.push(account);
                            }
                        }

                    }
                }

                if($rootScope.isSupport){
                    processSupportUsers(validSupportAccounts,userId);
                }else{
                    enterpriseUsersLogin(validEnterpriseAccounts,userId,validSupportAccounts);
                }

            }).error(function (data) {
                console.log('authentication failure' + data);
                login.errorMessage = data.errorStr;
            });
        }


        /**
         * Checks for support users
         * @param permissions
         * @returns {boolean}
         */
        function isGAPLoginAllowed(permissions) {
            var gapLoginPermission = 'GAP_LOGIN_ALLOWED';
            return (permissions.indexOf(gapLoginPermission) != -1)
        }

        function processSupportUsers(validSupportAccounts,userId){
            if(validSupportAccounts.length > 0){
                //Assuming that user with support role will not be present at multiple enterprises level.
                var account = validSupportAccounts[0];
                if (account.role === 'GIGSKY_ADMIN') {
                    setGsAdminConfigDetails();
                }
                supportUsersLogin(account, userId);
            }else{
                login.errorMessage = "User doesn't have privileges to access Enterprise admin portal.";
                authenticationService.clearSession();
            }
        }

        function enterpriseUsersLogin(validEnterpriseAccounts, userId, validSupportAccounts) {

            if (validEnterpriseAccounts.length > 1) {
                $rootScope.userAccount = validEnterpriseAccounts;
                $rootScope.hasMultiAccount = true;
                sessionStorage.setItem('hasMultiAccount', true);
                $location.path('/auth/select-account');
            } else if (validEnterpriseAccounts.length === 1) {
                $rootScope.hasMultiAccount = false;
                sessionStorage.setItem('hasMultiAccount', false);
                authenticationService.storePermissionsInSession(validEnterpriseAccounts[0].permissionList);
                authenticationService.enterpriseLogin(validEnterpriseAccounts[0].accountId, userId).success(function (enterpriseLoginResp) {
                    $location.path('/app/home');
                }).error(function (enterpriseLoginResp) {
                    console.log('authentication failure' + enterpriseLoginResp);
                    login.errorMessage = enterpriseLoginResp.errorStr;
                });
            } else if (validSupportAccounts.length > 0) {
                login.errorMessage = 'Admin/Support user is not allowed. For any enterprise support please use admin.gigsky.com';
                authenticationService.clearSession();
            } else {
                console.log('authentication failure');
                login.errorMessage = "User doesn't have privileges to access Enterprise portal.";
                authenticationService.clearSession();
            }
        }

        function supportUsersLogin(account, userId) {
            authenticationService.storePermissionsInSession(account.permissionList);
            var accountId = account.accountId;

            authenticationService.enterpriseLogin(accountId, userId).success(function (enterpriseLoginResp) {
                sessionStorage.setItem("rootAccountId", accountId);
                sessionStorage.setItem("accountId", accountId);
                sessionStorage.setItem("token", enterpriseLoginResp.token);
                $rootScope.isSupport = true;
                sessionStorage.setItem('isSupport', true);

                var  globalSearchAllowed = authenticationService.isOperationAllowed(['GLOBAL_SEARCH_ALLOWED']);

                if(globalSearchAllowed){
                    $state.go("support.search");
                }else{
                    $state.go("support.accounts.companies");
                }

            }).error(function (enterpriseLoginResp) {
                console.log('authentication failure' + enterpriseLoginResp);
                login.errorMessage = enterpriseLoginResp.errorStr;
            });
        }

        /**
         * Setting default values for gsAdmin
         */
        function setGsAdminConfigDetails() {
            $rootScope.isGSAdmin = true;
            sessionStorage.setItem('isGSAdmin', true);
            $rootScope.hasMultiAccount = true;
            sessionStorage.setItem('hasMultiAccount', true);
        }

        /**
         * Setting default values to rootScope variables,which describes app state
         */
        function setDefaultConfig() {
            var p = $location.absUrl();
            if ((p.indexOf('admin.gigsky.com') != -1) || (p.indexOf('admin-stage.gigsky.com') != -1)  || (p.indexOf('admin-test.gigsky.com') != -1) || (p.indexOf('admin-beta.gigsky.com') != -1) || (p.indexOf('localhost') != -1 && p.indexOf('/gem') != -1)) {
                $rootScope.isSupport = true;
                sessionStorage.setItem('isSupport', true);
                $rootScope.pageTitle = ADMIN_PORTAL_TITLE;
            } else {
                $rootScope.pageTitle = ENTERPRISE_PORTAL_TITLE;
                $rootScope.isSupport = false;
                sessionStorage.setItem('isSupport', false);
            }
            $rootScope.isGSAdmin = false;
            sessionStorage.setItem('isGSAdmin', false);

            $rootScope.isNavigatedThroughAdmin = false;
            sessionStorage.setItem('isNavigatedThroughAdmin', false);
        }

        $scope.showMaintenanceMessage = function(){
            var displayMessage = false;

            var currentDate = new Date();
            var currentUTCDate=new Date(currentDate.getUTCFullYear(),currentDate.getUTCMonth(),currentDate.getUTCDate());

            var maintenanceDate = new Date(2019, 8,26);
            if(currentUTCDate.getTime() <= maintenanceDate.getTime()){
                displayMessage = true;
            }
            return displayMessage;
        }


        activate();
        loadingState.hide();
    }
})();
