'use strict';

(function(){
    angular
        .module('app')
        .factory('AuthenticationService',['$http', '$rootScope','$q','commonUtilityService','analytics',AuthenticationService]);

    /**
     @function debounce()
     @param {object} $timeout - angular timeout service
     - Factory method for functions to be executed after a delay
     */
    function AuthenticationService($http, $rootScope,$q,commonUtilityService,analytics){


        var factory = {
            authenticate: authenticate,
            clearSession:clearSession,
            clearHttpHeaders:clearHttpHeaders,
            isSessionValid:isSessionValid,
            isUserSessionValid:isUserSessionValid,
            getUserEmailId:getUserEmailId,
            getUserId:getUserId,
            isTokenValid:isTokenValid,
            isLoggedIn:isLoggedIn,
            userAccountLogout:userAccountLogout,
            enterpriseAccountLogout:enterpriseAccountLogout,
            forgotPassword:forgotPassword,
            forgotPasswordWithCaptcha:forgotPasswordWithCaptcha,
            resetPassword:resetPassword,
            unblockAccount:unblockAccount,
            getUserAccount:getUserAccount,
            enterpriseLogin:enterpriseLogin,
            getSubAccounts:getSubAccounts,
            getAccessLevel : getAccessLevel,
            getEnterpriseAccountDetails:getEnterpriseAccountDetails,
            setAlertsFeatureEnabled:setAlertsFeatureEnabled,
            setPricingModelVersion:setPricingModelVersion,
            isOperationAllowed: isOperationAllowed,
            storePermissionsInSession: storePermissionsInSession
        };
        var actualPermissions = [];
        var subscriptionType;
        return factory;

        function clearSession()
        {
            sessionStorage.clear();
            actualPermissions = [];
        }
        function clearHttpHeaders(){
            delete $http.defaults.headers.common["Authorization"];
            delete $http.defaults.headers.common["token"];
        }
        function isSessionValid()
        {
            var userToken =  sessionStorage.getItem("userToken");
            var token =  sessionStorage.getItem("token");
            var userId =  sessionStorage.getItem("userId");
            var accountId =  sessionStorage.getItem("accountId");


            return (token && token.length>9 && userToken && userToken.length>9 && userId  && accountId);
        }

        function isUserSessionValid()
        {
            var userToken =  sessionStorage.getItem("userToken");
            var userId =  sessionStorage.getItem("userId");
            return ( userToken && userId && userToken.length>9);
        }

        function getUserEmailId (){
            return sessionStorage.getItem('userEmail');
        }
        function getUserId()
        {
            return sessionStorage.getItem('userId');
        }
        function isTokenValid(token,tokenType){
            return $http.get(ENTERPRISE_API_BASE+"account/confirmToken?token="+token+"&tokenType=CONFIRM_PASSWORD_RESET");
        }

        /**
         @function setDebounce()
         @param {function} func - new function to execute
         @param {int} wait - time to wait before function execution
         @param {boolean|int[0-1]} immediate - truthy value to immediately execute function
         */
        function authenticate(emailId,password){
            var loginData = {
                "type" : "UserLogin",
                "emailId":emailId,
                "password":password
            };
            clearSession();
            return $http.post(ENTERPRISE_USER_API_BASE+'users/user/login', loginData).success(function(loginResponse){
                    //$http.defaults.headers.common.token = loginResponse.token;
                    sessionStorage. setItem("token",loginResponse.token);
                    sessionStorage.setItem("userId",loginResponse.userId);
                    commonUtilityService.storeUserTokenExpiryTime(loginResponse);
            });

        }


        function isLoggedIn(){
            return sessionStorage.getItem("userToken") ? true : false;
        };


        function userAccountLogout(){
            return $http.post(ENTERPRISE_USER_API_BASE+'users/user/'+ sessionStorage.getItem("userId") +'/logout');
        }

        function enterpriseAccountLogout(){
            var userId = sessionStorage.getItem("userId");
            var rootAccountId = sessionStorage.getItem("rootAccountId");
            var enterpriseId = rootAccountId ? rootAccountId : sessionStorage.getItem("accountId");
            return $http.post(ENTERPRISE_USER_API_BASE+'users/user/'+userId+'/account/'+ enterpriseId +'/logout');
        }

        function forgotPassword(emailId)
        {
            var forgotPasswordData={type:"ResetPassword", emailId: emailId};

            var req = $http.post(ENTERPRISE_USER_API_BASE+'users/user/password',forgotPasswordData);
            return req;
        }

        function forgotPasswordWithCaptcha(emailId,code){
            var forgotPasswordData={type:"ResetPassword", emailId: emailId,code:code};
            var orig = window.location.origin;
            var path = window.location.pathname;
            var index  = path.indexOf("app/");
            if(index==-1)
            {
                index  = path.indexOf("gem/");
            }
            var path = path.substring(0,index+4);
            var destination_url = orig+path+"forgotpwd.php?des=" + encodeURI(ENTERPRISE_USER_API_BASE);
            var req = $http.post(destination_url,forgotPasswordData);
            return req;
        }

        function resetPassword(password,token){
            var resetPasswordData={newPassword: password};
            return $http.post(ENTERPRISE_USER_API_BASE+'users/user/password/reset?token='+token,resetPasswordData);
        }

        function unblockAccount(password,token){
            var unblockAccountData={newPassword: password};
            return $http.post(ENTERPRISE_USER_API_BASE+'users/user/password/unblock?token='+token,unblockAccountData);
        }


        function getUserAccount(userId) {
            //This api is not going through common utility permission check as this api itself will respond with permission List.
            var queryParam = {};
            queryParam.details = 'permissionList';
            var url = ENTERPRISE_GIGSKY_BACKEND_BASE + 'accounts/account/user/' + userId;
            return $http({
                method: 'GET',
                url: url,
                params: queryParam
            });
        }
        function getSubAccounts(accountId,queryParam){
            //This api is not going through common utility permission check as permission list will be taken based on the enterprise selected.
            var url = ENTERPRISE_GIGSKY_BACKEND_BASE+'accounts/account/'+accountId+'/accounts';
            return $http({
                method: 'GET',
                url: url,
                params: queryParam
            });
        }

        function getEnterpriseAccountDetails(accountId,queryParam){
            var url = ENTERPRISE_GIGSKY_BACKEND_BASE+'accounts/account/'+accountId;
            var headerAdd=['ACCOUNT_READ'];
            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd,queryParam);
        }

        function enterpriseLogin(accountId, userId) {
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);
            $http.post(ENTERPRISE_USER_API_BASE + 'users/user/' + userId + '/account/' + accountId + '/login').success(function (loginResponse) {

                    if (sessionStorage.getItem("isGSAdmin") === "true") {

                        //$http.defaults.headers.common.token = loginResponse.token;
                        sessionStorage.setItem("rootAccountId", accountId);
                    } else {
                        //$http.defaults.headers.common.token = loginResponse.token;
                        sessionStorage.setItem("accountId", accountId);
                    }

                commonUtilityService.storeEnterpriseTokenExpiryTime(loginResponse);

                // get the subscription type
                 getEnterpriseAccountDetails( accountId ).success( function(response) {

                    console.log( "response");
                    console.log( response  );

                   if (sessionStorage.getItem("isGSAdmin") === "true" || $rootScope.isSupport) {
                       subscriptionType =  ENTERPRISE_DEFAULT_SUBSCRIPTION_TYPE;
                    }
                    else {
                        subscriptionType  = RBAC_ENABLED && response.accountSubscriptionType ? response.accountSubscriptionType : ENTERPRISE_DEFAULT_SUBSCRIPTION_TYPE;
                    }

                     sessionStorage.setItem("accountSubscriptionType", subscriptionType );
                     $rootScope.isGemLite = false;
                     $rootScope.isIotReseller = false;
                     if(subscriptionType == 'ENTERPRISE_LITE'){
                         $rootScope.isGemLite = true;
                     }else if(subscriptionType == 'IOT'){
                         $rootScope.isIotReseller = true;
                     }

                     if(response.alertVersionSupported){
                         setAlertsFeatureEnabled(response.alertVersionSupported);
                     }
                     setPricingModelVersion(response.pricingModelVersionSupported);

                     analytics.sendEvent({
                         category: "Behaviour",
                         action: "Enterprise Login",
                         label: response.accountName
                     });

                     defer.resolve({data:loginResponse});
                }).error( function(data,status){
                    console.log( "Error while fetching Enterprise account details ");
                     defer.reject({data:data,status:0});
                } );
            }).error(function(data,status){
                defer.reject({data:data,status:0});
            });

            return promise;
        }

        function setAlertsFeatureEnabled(alertVersionSupported) {
            if (alertVersionSupported == '1.1') {
                $rootScope.isAlertEnabled = true;
            } else{
                $rootScope.isAlertEnabled = false;
            }
            sessionStorage.setItem('alertSupported',$rootScope.isAlertEnabled);
        }

        function setPricingModelVersion(pricingModelVersion) {

            if (pricingModelVersion) {
                $rootScope.pricingModelVersion = pricingModelVersion;
            } else{
                $rootScope.pricingModelVersion = '1.1';
            }
            sessionStorage.setItem('pricingModelVersion',$rootScope.pricingModelVersion);
        }

        function getAccessLevel(module){
            if(module.constructor === Array){

                if( isLoggedIn() && ( actualPermissions === null || actualPermissions.length == 0 ) ) {
                    actualPermissions = JSON.parse(sessionStorage.getItem('userPermissions'));
                }
                return compareArrays(module, actualPermissions);
            }
            return {};

        }

        /**
         * It checks whether the permission required for that operation is present in current account permissions list.
         * @param permissionList
         * @returns {boolean}
         */
        function isOperationAllowed(permissionList){
            var accessLevel = getAccessLevel(permissionList);
            for(var i=0;i<permissionList.length;i++)
            {
                if(accessLevel[permissionList[i]] == true)
                {
                    return true;
                }
            }
            return false;
        }

        function compareArrays(fromArray, toArray){

            var res={};

            if( toArray == null || toArray.length == 0  ) return false;

            for(var val =0;val< fromArray.length;val++){
                if(toArray.indexOf(fromArray[val]) > -1){
                    res[fromArray[val]]=true;
                }
                else
                    res[fromArray[val]]=false;
            }
            return res;
        }

        function storePermissionsInSession(actualPermissionsArr){
            actualPermissions = actualPermissionsArr;
            sessionStorage.setItem("userPermissions", JSON.stringify(actualPermissionsArr));
        }
    }
})();
