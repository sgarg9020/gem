'use strict';

/* Controllers */
(function () {
    angular.module('app')
        .controller('AuditLogCtrl', AuditLog);

    AuditLog.$inject = ['$scope', 'dataTableConfigService', '$compile', 'InvoiceService', 'commonUtilityService', 'loadingState'];

    /**
     @constructor auditLog()
     @param {object} $scope - Angular.js $scope
     */
    function AuditLog($scope, dataTableConfigService, $compile, InvoiceService, commonUtilityService, loadingState) {

        var auditLog = this;

        /**
         * Start up logic for controller
         */
        function activate() {
            clearDataTableStatus(['DataTables_auditLogDataTable_']);
            $scope.emptyTableMessage = "No Audit Logs Found.";
            $scope.dataTableObj = {};
            $scope.auditDetailsModalObj = {};
            $scope.datePicker = {};

            var endDate = sessionStorage.getItem("date");
            var endDateObj = endDate ?  moment(new Date(endDate)) : moment(new Date());

            $scope.datePicker = {
                startDate: moment(endDateObj).add(-30, 'days').startOf('day'),
                endDate: moment(endDateObj).endOf('day')
            };

            auditLog.exportData={};
            auditLog.showAuditDetails = function(details){
                $scope.auditDetailsModalObj.updateAuditDetails(details);
                $('#auditDetailsModal').modal("show");
            };

            $scope.dataTableObj.tableID = "auditLogDataTable";
            $scope.dataTableObj.tableHeaders = [
                {"value": "Date (UTC)", "DbValue": "timeStamp"},
                {"value": "Operation", "DbValue": "displayableOperation"},
                {"value": "Details", "DbValue": "displayableString"},
                {"value": "Initiator Email", "DbValue": "userEmail"},
                {"value": "Initiator Name", "DbValue": "userName"},
                {"value": "Status", "DbValue": "operationStatus"}
            ];

            var statusToClassMap = { SUCCESS:'Complete', IN_PROGRESS:'Inprogress', FAILED:'Failed', PARTIAL: 'Partial'};
            $scope.dataTableObj.dataTableOptions = {
                "order": [[ 0, "desc" ]],
                "createdRow": function (row, data, index) {
                    $(row).attr("test-arg",data.fileName);
                    $compile(row)($scope);
                },
                "columnDefs": [{
                    "targets": 0,
                    "orderable": true,
                    "width": "140px",
                    "render": function (data, type, row, meta) {
                        return row.timeStamp;
                    }
                }, {
                    "targets": 1,
                    "orderable": false,
                    "width": "200px",
                    "render": function (data, type, row, meta) {
                        return row.displayableOperation;
                    }
                }, {
                    "targets": 2,
                    "orderable": false,
                    "width": "400px",
                    "render": function (data, type, row, meta) {
                        var data = Array.isArray(row.displayableString) ? row.displayableString.join(', ') : JSON.parse(row.displayableString).join(', ');
                        return data.length > 100 ?
                            data.substr(0, 100).replace(/</g, '&lt') +'<a ng-click="auditLog.showAuditDetails(\'' + data.toString() + '\')" class="cursor">  ...more</a>' : data;
                    }
                }, {
                    "targets": 3,
                    "orderable": false,
                    "render": function (data, type, row, meta) {
                        return row.userEmail;
                    }
                }, {
                    "targets": 4,
                    "orderable": false,
                    "render": function (data, type, row, meta) {
                        return row.userName;
                    }
                }, {
                    "targets": 5,
                    "orderable": false,
                    "render": function (data, type, row, meta) {
                        return '<span class="RP-association-status '+statusToClassMap[row.operationStatus]+'">'+statusToClassMap[row.operationStatus]+'</span>';
                    }
                }
                ],
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ",
                    "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    "sEmptyTable": $scope.emptyTableMessage,
                    "sZeroRecords": $scope.emptyTableMessage
                },
                fnServerData: function (sSource, aoData, fnCallback) {
                    auditLog.apiCallBack = fnCallback;
                    /* queryParam - api params to get table data to display */
                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    queryParam.startTime = moment($scope.datePicker.startDate).format('YYYY-MM-DD HH:mm:ss');
                    queryParam.endTime = moment($scope.datePicker.endDate).format('YYYY-MM-DD HH:mm:ss');

                    /* auditLog.exportData - api parameters to export table data */
                    auditLog.exportData.url = ENTERPRISE_GIGSKY_BACKEND_BASE + "account/"+sessionStorage.getItem('accountId')+"/auditReport?";
                    auditLog.exportData.startIndex = queryParam.startIndex;
                    auditLog.exportData.count = queryParam.count;
                    auditLog.exportData.sortBy = queryParam.sortBy;
                    auditLog.exportData.sortDirection = queryParam.sortDirection;
                    auditLog.exportData.search = queryParam.search;
                    auditLog.exportData.startTime = queryParam.startTime;
                    auditLog.exportData.endTime = queryParam.endTime;

                    $scope.isPageSizeFilterDisabled = false;

                    handleApiCallAndResponse(queryParam);

                },
                "fnDrawCallback": function (oSettings) {
                    if($scope.auditLogList)
                        dataTableConfigService.disableTableHeaderOps($scope.auditLogList.length, oSettings);
                }
            };
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

            $scope.dataTableObj.dataTableOptions.columns = [
                null,
                null,
                null
            ];

            configureCalendar($scope.datePicker.endDate, $scope.datePicker.startDate);
        }

        $scope.dateChange = function () {

            $scope.opts.startDate = $scope.datePicker.startDate;
            $scope.opts.endDate = $scope.datePicker.endDate;
            $scope.previousKey = -1;
            $scope.legendSelected = false;
            //if >90 set to months if not months and change date if not exact month

            if ($scope.datePicker.endDate.diff($scope.datePicker.startDate, 'days') > 90) {
                if (!$scope.datePicker.startDate.isSame($scope.datePicker.startDate.clone().startOf('month')) || !$scope.datePicker.endDate.isSame($scope.datePicker.endDate.clone().endOf('month'))) {
                    $scope.datePicker = {
                        startDate: $scope.datePicker.startDate.clone().startOf('month'),
                        endDate: $scope.datePicker.endDate.clone().endOf('month')
                    };
                    commonUtilityService.showSuccessNotification('Date range adjusted to align the start and end of the months');
                }
            }
            var queryParam = {};

            auditLog.exportData.startTime = moment($scope.datePicker.startDate).format('YYYY-MM-DD HH:mm:ss');
            auditLog.exportData.endTime = moment($scope.datePicker.endDate).format('YYYY-MM-DD HH:mm:ss');

            queryParam.startIndex = auditLog.exportData.startIndex;
            queryParam.count = auditLog.exportData.count;
            queryParam.sortBy = auditLog.exportData.sortBy;
            queryParam.sortDirection = auditLog.exportData.sortDirection;
            queryParam.search = auditLog.exportData.search;
            queryParam.startTime = moment($scope.datePicker.startDate).format('YYYY-MM-DD HH:mm:ss');
            queryParam.endTime = moment($scope.datePicker.endDate).format('YYYY-MM-DD HH:mm:ss');
            handleApiCallAndResponse(queryParam);
        };

        function handleApiCallAndResponse(queryParam) {
            InvoiceService.getAuditLogReports(queryParam).success(function (response) {
                // response = mockData;
                var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
                $scope.auditLogList = datTabData.aaData;
                $scope.isPageSizeFilterDisabled = ($scope.auditLogList.length == 0);
                if (oSettings != null) {
                    auditLog.apiCallBack(datTabData);
                } else {
                    loadingState.hide();
                }
            }).error(function (data, status) {
                $scope.isPageSizeFilterDisabled = ( !$scope.auditLogList ||  $scope.auditLogList.length == 0);
                var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                dataTableConfigService.hidePaceOnFailure(oSettings,auditLog.apiCallBack,"audit logs");
                console.log('get auditLog failure' + data);
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        }

        function configureCalendar(curDate, preDate){
            //setting minDate to last one year
            var today = new Date(curDate);
            today.setFullYear(today.getFullYear()-1);

            $scope.opts = {
                maxDate: moment(curDate),
                startDate:moment(preDate),
                endDate:moment(curDate),
                linkedCalendars: true,
                opens:'right',

                locale: {
                    applyClass: 'btn-green',
                    applyLabel: "Apply",
                    fromLabel: "From",
                    format: "DD/MMM/YY",
                    toLabel: "To",
                    cancelLabel: 'Cancel',
                    customRangeLabel: 'Custom range'
                },
                ranges: {
                    'Last 7 Days': [moment(curDate).subtract(6, 'days'), moment(curDate)],
                    'Last 30 Days': [moment(curDate).subtract(29, 'days'), moment(curDate)],
                    'Last 90 Days': [moment(curDate).subtract(90, 'days'), moment(curDate)],
                    'Last 12 Months': [moment(curDate).subtract(11, 'months').startOf('month'), moment(curDate).endOf('month')]
                },
                eventHandlers : {
                    'apply.daterangepicker' : function() {
                        $scope.dateChange();
                    }
                }
            };
        }

        activate();
    }
})();