'use strict';

/* Controller */
(function () {
    angular.module('app')
        .controller('AuditDetailsCtrl', AuditDetails);

    AuditDetails.$inject = ['modalDetails','$scope','$timeout'];

    /**
     @constructor AuditDetails()
     @param {object} modalDetails - Factory
     */
    function AuditDetails(modalDetails,$scope,$timeout) {

        var auditDetails = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            auditDetails.details = '';


            modalDetails.scope(auditDetails, true);
            modalDetails.attributes({
                id: 'auditDetailsModal',
                header: 'Audit Log Details',
                cancel: 'Close'
            });
            modalDetails.cancel(function(){console.log("This should do nothing");});

            $scope.$parent.auditDetailsModalObj.updateAuditDetails = updateModal; /* Attach to parent */
        }

        function updateModal(details){
            $timeout(function(){
                $('div.auditDetailsModal').scrollTop(0);
            },300);
            auditDetails.details = details;
        }

        activate();
    }
})();





