'use strict';

/* Controllers */
(function(){
    angular.module('app')
        .controller('InvoicesCtrl', Invoices);

	Invoices.$inject = ['$scope','$rootScope','$stateParams','dataTableConfigService','$compile','InvoiceService','commonUtilityService','loadingState'];

    /**
    	@constructor Invoices()
	 	@param {object} $scope - Angular.js $scope
     */
    function Invoices($scope,$rootScope,$stateParams,dataTableConfigService,$compile,InvoiceService,commonUtilityService,loadingState){

        var invoices = this;


		/**
		 * Start up logic for controller
		 */
		function activate(){
			clearDataTableStatus(['DataTables_invoicesDataTable_']);
			$scope.modalTitle = 'Invoice';
			$scope.emptyTableMessage = "No Invoices Found.";
			$scope.dataTableObj = {};

			if($rootScope.isSupport && !$rootScope.isNavigatedThroughAdmin){
				$scope.enterpriseId = $stateParams.accountId;
			}

			$scope.dataTableObj.tableID = "invoicesDataTable";
			$scope.dataTableObj.tableHeaders = [
				{"value": "Invoice","DbValue":"fileName"},
				{"value": "Created","DbValue":"generatedDate"},
				{"value": "","DbValue":""}
			];
			$scope.dataTableObj.dataTableOptions = {
				"order": [[ 1, "desc" ]],
				"createdRow": function ( row, data, index ) {
					$(row).attr("id",'IID_'+data.invoiceId);
					$(row).attr("test-arg",data.fileName);
					$compile(row)($scope);
				},
				"columnDefs": [{
					"targets": 0,
					"orderable": false,
					"render": function ( data, type, row, meta) {
						var url = ENTERPRISE_SERVER_BASE + row.url + "?token=" + encodeURIComponent(sessionStorage.getItem("token"));
						return '<a href="'+ url +'" gs-has-permission="READ_INVOICE"> <i class="fa fa-file-pdf-o"></i> '+ row.fileName +'</a>';
					}
				},{
					"targets": 1,
					"orderable": true,
					"render": function ( data, type, row, meta) {
						var val = getDateObj(row.generatedDate);
						return (val.getMonth()+1)+"/"+val.getDate()+"/"+val.getFullYear();
					}
				},{
					"targets": 2,
					"orderable": false,
					"render": function ( data, type, row, meta) {
						return '<a href gs-has-permission="ACCOUNT_INVOICE_UPLOAD" ng-click="invoices.showUploadInvoiceModal('+row.invoiceId+')"> Replace</a>';
					}
				}
				],
				"oLanguage": {
					"sLengthMenu": "_MENU_ ",
					"sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
					"sEmptyTable": $scope.emptyTableMessage,
					"sZeroRecords":$scope.emptyTableMessage
				},
				fnServerData: function (sSource, aoData, fnCallback) {
					var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
					$scope.isPageSizeFilterDisabled = false;
					InvoiceService.getInvoices(queryParam,$scope.enterpriseId).success(function (response) {
						var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
						var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
						$scope.invoicesList = datTabData.aaData;
						$scope.isPageSizeFilterDisabled = ($scope.invoicesList.length == 0);
						if (oSettings != null) {
							fnCallback(datTabData);
						}else{
							loadingState.hide();
						}
					}).error(function(data,status){
						$scope.isPageSizeFilterDisabled = ( !$scope.invoicesList || $scope.invoicesList.length == 0);
						var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
						dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"invoices");
						console.log('get invoices failure' + data);
						commonUtilityService.showErrorNotification(data.errorStr);
					});

				},

				"fnDrawCallback": function( oSettings ) {
					if($scope.invoicesList)
						dataTableConfigService.disableTableHeaderOps( $scope.invoicesList.length, oSettings );

					commonUtilityService.preventDataTableActivePageNoClick( $scope.dataTableObj.tableID );

				}
			};
			$scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

			$scope.dataTableObj.dataTableOptions.columns = [
				null,
				null,
				null
			];
		}

		invoices.showUploadInvoiceModal = function(invoiceId){

			$scope.invoiceId=invoiceId;
			$('#invoiceUploadModal').modal('show');
		};
		activate();

    }
})();
