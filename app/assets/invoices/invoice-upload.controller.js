'use strict';

/* Controllers */
(function () {
    angular.module('app')
        .controller('InvoiceUploadCtrl', InvoiceUpload);

    InvoiceUpload.$inject = ['$scope', '$rootScope', 'modalDetails','$timeout'];

    /**
     @constructor InvoiceUpload()
     @param {object} $scope - Angular.js $scope
     @param {object} formMessage - Factory object
     */
    function InvoiceUpload($scope, $rootScope, modalDetails,$timeout) {

        var invoiceUpload = this;

        /**
         * Start up logic for controller
         */
        function activate() {

            //For automation testing we need to consider date available in the sessionStorage as current Date.
            $scope.endDate = sessionStorage.getItem("date"); //"01-2017";
            if(!$scope.endDate)
                $scope.endDate = 'now';
            else
            {
                var dateSplit = $scope.endDate.split("-");
                $scope.endDate = dateSplit[1] +  '-' +dateSplit[0];
            }
            invoiceUpload.refreshDatatable = $scope.$parent.dataTableObj.refreshDataTable;
            $scope.title = $scope.$parent.modalTitle + ' Upload' || 'Upload';
            $scope.showCalendar = function (event) {
                $($(event.target.parentNode).siblings('input')[0]).datepicker('show');
            };

            var accountId = $scope.$parent.enterpriseId ? $scope.$parent.enterpriseId : sessionStorage.getItem("accountId");
            $scope.url = ENTERPRISE_GIGSKY_BACKEND_BASE + 'account/' + accountId;
            $scope.dropzoneConfig = {
                autoProcessQueue: false,
                maxFiles: 1,
                parallelUploads: 1,
                maxFilesize: 25,
                addRemoveLinks: false,
                clickable: true,
                acceptedFiles: ".csv,.xls,.xlsx,.pdf",
                headers:{ "token" : sessionStorage.getItem( "token") },
                //url: ENTERPRISE_GIGSKY_BACKEND_BASE + 'account/' + sessionStorage.getItem("accountId"),
                dictInvalidFileType:'File type not supported'
                //?title='+sample.pdf+'&billingPeriodStart='+2017-03-01%2000:00:00+'&billingPeriodEnd='+2017-03-31%2023:59:59+'&billGeneratedTime='+2017-03-31%2023:59:59
            };
            /* ModalDetails factory */
            modalDetails.scope(invoiceUpload, true);
            modalDetails.attributes({
                id: 'invoiceUploadModal',
                formName: 'invoiceUpload',
                cancel: 'Clear',
                submit: 'Upload'
            });
            modalDetails.cancel(reset);
            modalDetails.submit(attemptToUpload);
            $timeout(function(){
                $('#invoiceUploadModal').on('hidden.bs.modal', function () {
                    $scope.month='';
                    reset();
                });
            },1000);
        }

        /**
         @function reset()
         - call a directive level function
         */
        function reset() {
            console.log('Resetting Dropzone...');
            $rootScope.$broadcast('clearDropzoneFiles');
        }

        /**
         @function attemptToUpload()
         - call a directive level function
         */
        function attemptToUpload() {
            if ($scope.month) {
                var myDropzone = Dropzone.forElement("#invoiceUpload");
                if(myDropzone.files.length>0) {
                    var month = new Date($scope.month.split('-')[1], parseInt($scope.month.split('-')[0]) - 1, 1);
                    var firstDay = new Date(month.getFullYear(), month.getMonth(), 1);
                    var lastDay = new Date(month.getFullYear(), month.getMonth() + 1, 0);
                    var invoiceDay = new Date(firstDay.getFullYear(),firstDay.getMonth()+1,firstDay.getDate());
                    var fromDate = firstDay.getFullYear() + '-' + ("0" + (firstDay.getMonth() + 1)).slice(-2) + '-' + ("0" + firstDay.getDate()).slice(-2) + ' 00:00:00';
                    var toDate = lastDay.getFullYear() + '-' + ("0" + (lastDay.getMonth() + 1)).slice(-2) + '-' + ("0" + lastDay.getDate()).slice(-2) + ' 23:59:59';
                    var today_str= invoiceDay.getFullYear() + '-' + ("0" + (invoiceDay.getMonth() + 1)).slice(-2) + '-' + ("0" + invoiceDay.getDate()).slice(-2) + ' 00:00:00';

                    if($scope.$parent.invoiceId || $scope.$parent.reportId){
                        myDropzone.options.method = 'PUT';
                        if($scope.$parent.modalTitle == 'Invoice')
                            $scope.dropzoneConfig.url = $scope.url + '/invoice?invoiceId=' + $scope.$parent.invoiceId + '&billingPeriodStart=' + fromDate + '&billingPeriodEnd=' + toDate + '&billGeneratedTime=' + today_str;
                        else
                            $scope.dropzoneConfig.url = $scope.url + '/report?reportId='+ $scope.$parent.reportId + '&usagePeriodStart=' + fromDate + '&usagePeriodEnd=' + toDate + '&reportGeneratedTime=' + today_str;
                    }else{
                        myDropzone.options.method = 'POST';
                        if($scope.$parent.modalTitle == 'Invoice')
                            $scope.dropzoneConfig.url = $scope.url + '/invoice?title=' + myDropzone.files[0].name + '&billingPeriodStart=' + fromDate + '&billingPeriodEnd=' + toDate + '&billGeneratedTime=' + today_str;
                        else
                            $scope.dropzoneConfig.url = $scope.url + '/report?title='+ myDropzone.files[0].name + '&usagePeriodStart=' + fromDate + '&usagePeriodEnd=' + toDate + '&reportGeneratedTime=' + today_str;
                    }
                    console.log('Processing Dropzone...');
                    $rootScope.$broadcast('submitDropzoneFiles');
                }
                else{
                    invoiceUpload.fileErrorStr="No file selected";
                }
            } else {
                invoiceUpload.dateErrorStr = "Billing Month invalid";
            }
        }

        activate();

    }
})();
