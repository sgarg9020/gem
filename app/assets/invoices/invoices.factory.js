'use strict';

(function(){
    angular
        .module('app')
        .factory('InvoiceService',['commonUtilityService',InvoiceService]);

    /**
     @function
     - Factory method for functions to be executed after a delay
     */
    function InvoiceService(commonUtilityService){

        var factory = {
            getInvoices:getInvoices,
            getMonthlyUsageReports: getMonthlyUsageReports,
            getAuditLogReports: getAuditLogReports
        };

        return factory;

        function getInvoices(queryParam, accountId) {
            var enterpriseId = accountId;
            if(!accountId){
                enterpriseId = sessionStorage.getItem("accountId");
            }
            var headerAdd=['READ_INVOICE'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE+'account/'+enterpriseId+'/invoice', 'GET',headerAdd,queryParam);
        }

        function getMonthlyUsageReports(queryParam, accountId){
            var enterpriseId = accountId;
            if(!accountId){
                enterpriseId = sessionStorage.getItem("accountId");
            }
            var headerAdd=['READ_REPORT'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE+'account/'+enterpriseId+'/report', 'GET',headerAdd,queryParam);
        }

        function getAuditLogReports(queryParam){
            var enterpriseId = sessionStorage.getItem("accountId");
            var headerAdd=['READ_AUDIT_REPORT'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE+'account/'+enterpriseId+'/auditReport', 'GET',headerAdd,queryParam);
        }

    }
})();
