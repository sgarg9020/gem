'use strict';

/* Controllers */
(function () {
    angular.module('app')
        .controller('MonthlyReportsCtrl', MonthlyReports);

    MonthlyReports.$inject = ['$scope','$rootScope','$stateParams', 'dataTableConfigService', '$compile', 'InvoiceService', 'commonUtilityService', 'loadingState'];

    /**
     @constructor monthlyReports()
     @param {object} $scope - Angular.js $scope
     */
    function MonthlyReports($scope, $rootScope ,$stateParams , dataTableConfigService, $compile, InvoiceService, commonUtilityService, loadingState) {

        var monthlyReports = this;

        /**
         * Start up logic for controller
         */
        function activate() {
            clearDataTableStatus(['DataTables_monthlyReportsDataTable_']);
            $scope.modalTitle = 'Monthly Reports';
            $scope.emptyTableMessage = "No Monthly Reports Found.";
            $scope.dataTableObj = {};

            if($rootScope.isSupport && !$rootScope.isNavigatedThroughAdmin){
                $scope.enterpriseId = $stateParams.accountId;
            }

            $scope.dataTableObj.tableID = "monthlyReportsDataTable";
            $scope.dataTableObj.tableHeaders = [
                {"value": "Report", "DbValue": "fileName"},
                {"value": "Created", "DbValue": "generatedDate"},
                {"value": "", "DbValue": ""}
            ];
            $scope.dataTableObj.dataTableOptions = {
                "order": [[ 1, "desc" ]],
                "createdRow": function (row, data, index) {
                    $(row).attr("test-arg",data.fileName);
                    $compile(row)($scope);
                },
                "columnDefs": [{
                    "targets": 0,
                    "orderable": false,
                    "render": function (data, type, row, meta) {
                        var url = ENTERPRISE_SERVER_BASE + row.url + "?token=" + encodeURIComponent(sessionStorage.getItem("token"));
                        return '<a href="' + url + '"> <i class="fa fa-file-pdf-o"></i> ' + row.fileName + '</a>';
                    }
                }, {
                    "targets": 1,
                    "orderable": true,
                    "render": function (data, type, row, meta) {
                        var val = getDateObj(row.generatedDate);
                        return (val.getMonth()+1)+"/"+val.getDate()+"/"+val.getFullYear();
                    }
                },{
                    "targets": 2,
                    "orderable": false,
                    "render": function ( data, type, row, meta) {
                        return '<a href gs-has-permission="ACCOUNT_USAGE_REPORT_UPLOAD" ng-click="monthlyReports.showUploadMonthlyReportsModal('+row.reportId+')"> Replace</a>';
                    }
                }
                ],
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ",
                    "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    "sEmptyTable": $scope.emptyTableMessage,
                    "sZeroRecords": $scope.emptyTableMessage
                },
                fnServerData: function (sSource, aoData, fnCallback) {
                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    $scope.isPageSizeFilterDisabled = false;

                    InvoiceService.getMonthlyUsageReports(queryParam,$scope.enterpriseId).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
                        $scope.monthlyReportsList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ($scope.monthlyReportsList.length == 0);
                        if (oSettings != null) {
                            fnCallback(datTabData);
                        } else {
                            loadingState.hide();
                        }
                    }).error(function (data, status) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.monthlyReportsList ||  $scope.monthlyReportsList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"monthly reports");
                        console.log('get monthlyReports failure' + data);
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });

                },

                "fnDrawCallback": function (oSettings) {
                    if($scope.monthlyReportsList)
                        dataTableConfigService.disableTableHeaderOps($scope.monthlyReportsList.length, oSettings);


                }
            };
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

            $scope.dataTableObj.dataTableOptions.columns = [
                null,
                null,
                null
            ];
        }

        monthlyReports.showUploadMonthlyReportsModal = function(reportId){
            $scope.reportId=reportId;
            $('#invoiceUploadModal').modal('show');
        };
        activate();

    }
})();
