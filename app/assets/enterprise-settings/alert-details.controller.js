/* Controller */
(function () {

    'use strict';

    angular.module('app')
        .controller('AlertDetailsCtrl', AlertDetails);

    AlertDetails.$inject = ['$rootScope', '$scope', '$compile', '$q', 'SettingsService', 'commonUtilityService', 'loadingState','$sce','analytics'];

    /**
     @constructor AlertDetails()
     */
    function AlertDetails($rootScope, $scope, $compile, $q, SettingsService, commonUtilityService, loadingState,$sce,analytics){

        var alertDetails = this;
        var enterpriseId = sessionStorage.getItem("accountId");

        /**
         * @func activate()
         * @desc controller start up logic
         */
        function activate() {

            // $scope
            $scope.isRBACEnabled = RBAC_ENABLED;
            $scope.alertNotEnabled = false;
            alertDetails.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};
            alertDetails.submitAlertConfig = submitAlertConfig;

            $scope.generalObj = {};
            // Initialize
            _alertConfiguration();

            // Loading Factory
            loadingState.hide();
        }

        activate();

        /**
         * @func _alertConfiguration
         * @desc controller request logic
         * @private
         */
        function _alertConfiguration(){
            alertDetails.errorMessage = '';
            var getPossibleAlertConfigsPromise = SettingsService.getPossibleAlertConfigs();
            var getEnterpriseDetailsPromise = SettingsService.getEnterpriseDetails(enterpriseId);
            $q.all([getPossibleAlertConfigsPromise, getEnterpriseDetailsPromise]).then(function (response) {
                console.log(response);

                alertDetails.alertIntervals = response[0].data.alertIntervals;
                alertDetails.simAlertUsage = response[0].data.simAlertUsage;

                var alertSettings = response[1].data.alertSetting;

                // TODO - Issues with object organization start here

                alertDetails.alertConfig = {};
                alertDetails.alertConfig.alertAccount = alertSettings.alertEnabled;
                $scope.alertNotEnabled = alertDetails.alertConfig.alertAccount;

                if (alertSettings.defaultCreditLimitForSims && alertSettings.defaultCreditLimitForSims.length > 0)
                    alertDetails.alertConfig.qualifier =  SettingsService.getAlertPercentageObj(alertDetails.simAlertUsage,alertSettings.defaultCreditLimitForSims[0].alertPercentages[0]);
                else
                    alertDetails.alertConfig.qualifier = alertDetails.simAlertUsage[0];
                if (alertSettings.alertIntervalInSecs)
                    alertDetails.alertConfig.interval = SettingsService.getAlertIntervalObj(alertDetails.alertIntervals,alertSettings.alertIntervalInSecs);
                else
                    alertDetails.alertConfig.interval = alertDetails.alertIntervals[0];

                angular.element('#switchAccountAlerts').next().remove();
                $compile(angular.element('#switchAccountAlerts'))($scope);

                if(!$rootScope.isAlertEnabled){
                    alertDetails.alertConfig.alertUserCheck = (alertSettings.defaultCreditLimitForSims && alertSettings.defaultCreditLimitForSims.length > 0)?alertSettings.defaultCreditLimitForSims[0].alertUserByMail:false;
                    if (!alertDetails.alertConfig.alertUserCheck && alertDetails.alertConfig.alertAccount) {
                        angular.element('#switchEmailUsers').next().attr('style', 'border-color: rgb(223, 223, 223); box-shadow: rgb(223, 223, 223) 0px 0px 0px 0px inset; transition: border 0.4s, box-shadow 0.4s; background-color: rgb(255, 255, 255);');
                        angular.element('#switchEmailUsers').next().children().attr('style', 'left: 0px; transition: left 0.2s; background-color: rgb(255, 255, 255);');

                    } else if (alertDetails.alertConfig.alertUserCheck && !alertDetails.alertConfig.alertAccount) {
                        angular.element('#switchEmailUsers').next().attr('style', 'border-color: rgb(109, 92, 174); box-shadow: rgb(109, 92, 174) 0px 0px 0px 13px inset; transition: border 0.4s, box-shadow 0.4s, background-color 1.2s; opacity: 0.5; background-color: rgb(109, 92, 174);');
                        angular.element('#switchEmailUsers').next().children().attr('style', 'left: 16px; transition: left 0.2s; background-color: rgb(255, 255, 255);');
                    }
                    else if (!alertDetails.alertConfig.alertUserCheck && !alertDetails.alertConfig.alertAccount) {
                        angular.element('#switchEmailUsers').next().attr('style', 'box-shadow: rgb(223, 223, 223) 0px 0px 0px 0px inset; border-color: rgb(223, 223, 223); transition: border 0.4s, box-shadow 0.4s; opacity: 0.5; background-color: rgb(255, 255, 255);');
                        angular.element('#switchEmailUsers').next().children().attr('style', 'left: 0px; transition: left 0.2s; background-color: rgb(255, 255, 255);');
                    }
                    else {
                        angular.element('#switchEmailUsers').next().attr('style', 'border-color: rgb(109, 92, 174); box-shadow: rgb(109, 92, 174) 0px 0px 0px 13px inset; transition: border 0.4s, box-shadow 0.4s, background-color 1.2s; background-color: rgb(109, 92, 174);');
                        angular.element('#switchEmailUsers').next().children().attr('style', 'left: 16px; transition: left 0.2s; background-color: rgb(255, 255, 255);');
                    }
                }else{
                    alertDetails.alertConfig.alertUserCheck = alertSettings.alertEmailForUserEnabled;
                    angular.element('#switchEmailUsersId').next().remove();
                    $compile(angular.element('#switchEmailUsersId'))($scope);
                }
                $scope.generalObj.previousData = angular.copy(alertDetails.alertConfig);
            },function (data) {
                commonUtilityService.showErrorNotification((data&&data.data)?(data.data.errorStr):null);
            });
        }

        function submitAlertConfig(alertConfigForm){
            alertDetails.errorMessage = '';
            var isAlertConfig = true;
            if($scope.isFormValid(alertConfigForm,alertDetails.alertConfig)){
                SettingsService.updateEnterpriseDetails(isAlertConfig,alertDetails.alertConfig).success(function(){
                    $scope.generalObj.previousData = angular.copy(alertDetails.alertConfig);
                    var msg = $rootScope.isAlertEnabled ? 'Enterprise action settings have been updated successfully.' : 'Enterprise alert settings have been updated successfully.';
                    commonUtilityService.showSuccessNotification(msg);

                    analytics.sendEvent({
                        category: "Operations",
                        action: "Update Action Settings",
                        label: "Enterprise Settings"
                    });

                }).error(function (data) {
                    alertDetails.errorMessage = data.errorStr;
                });
            } else if($scope.generalObj.isNoChangeError){
                alertDetails.errorMessage = "There is no change in the data to update.";
                $scope.generalObj.isNoChangeError = false;
            }
        }
    }
})();