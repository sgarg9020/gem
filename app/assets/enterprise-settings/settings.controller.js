'use strict';

/* Controller */
(function(){
	angular.module('app')
	    .controller('SettingsCtrl', Settings);

	Settings.$inject = [];

	/**
 	 * @constructor Settings()
     */
	function Settings(){
		
		/*var enterpriseSettings = this;

		/**
		 * Start up logic for controller
		 */
		function activate(){
		}

		activate();
	}
})();
