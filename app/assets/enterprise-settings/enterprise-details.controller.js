/* Controller */
(function () {

    'use strict';

    angular.module('app')
        .controller('EnterpriseDetailsCtrl', EnterpriseDetails);

    EnterpriseDetails.$inject = ['$rootScope', '$scope', '$q', '$sce', 'SettingsService', 'commonUtilityService', 'loadingState','analytics'];

    /**
     @constructor EnterpriseDetails()
     */
    function EnterpriseDetails($rootScope, $scope, $q, $sce, SettingsService, commonUtilityService, loadingState,analytics){

        var enterpriseDetails = this;
        var enterpriseId = sessionStorage.getItem("accountId");
        var previousCompanyName;

        /**
         * @func activate()
         * @desc controller start up logic
         */
        function activate() {

            // $scope
            $scope.generalObj = {};
            $scope.disableUISelect=false;
            $scope.isNonGSAdmin = !(sessionStorage.getItem("isGSAdmin") === "true");
            $scope.isRBACEnabled = RBAC_ENABLED;
           // $scope.showErrMsg = function( errorMsg ) { commonUtilityService.showErrorNotification( errorMsg ); };
           // $scope.showSuccessMsg = function( msg ){ commonUtilityService.showSuccessNotification( msg ); };

            // Enterprise account
            enterpriseDetails.account = {};
            enterpriseDetails.account.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};

            // Enterprise details
            enterpriseDetails.updateEnterpriseDetails = updateEnterpriseDetails;

            // Initialize
            _enterpriseDetails();

            // Loading Factory
            loadingState.hide();
        }

        activate();

        /**
         * @func _onEnterpriseDetails()
         * @desc controller request logic
         * @private
         */
        function _enterpriseDetails() {

            enterpriseDetails.errorMessage = '';

            var getCountriesPromise = commonUtilityService.getCountries();
            var getEnterpriseDetailsPromise = SettingsService.getEnterpriseDetails(enterpriseId);
            var getSubscriptionTypes = SettingsService.getEnterpriseSubscriptionTypes();

            $q.all([getCountriesPromise, getEnterpriseDetailsPromise,getSubscriptionTypes]).then(function (response) {
                enterpriseDetails.countries = response[0].data.countryList;

                enterpriseDetails.subscriptionTypes = response[2].data.map(function(subscription){return subscription.name.replace( "ENTERPRISE_", "GEM ")}).filter(function(sub){return !sub.match(/RESELLER_MA/g)});

                var enterpriseResponse = response[1].data;
                enterpriseDetails.alertSettings = enterpriseResponse.alertSetting;

                if (enterpriseResponse != null) {
                    enterpriseDetails.account.companyName = enterpriseResponse.company.name;
                    enterpriseDetails.account.city = enterpriseResponse.company.address.city;
                    enterpriseDetails.account.state = enterpriseResponse.company.address.state;
                    enterpriseDetails.account.postalCode = enterpriseResponse.company.address.zipCode;
                    enterpriseDetails.account.currency = enterpriseResponse.company.currency;
                    enterpriseDetails.account.enterpriseId = enterpriseResponse.accountId;
                    enterpriseDetails.account.accountSubscriptionType = enterpriseResponse.accountSubscriptionType ? enterpriseResponse.accountSubscriptionType : ENTERPRISE_DEFAULT_SUBSCRIPTION_TYPE;
                    enterpriseDetails.account.accountSubscriptionType = (enterpriseDetails.account.accountSubscriptionType == 'ENTERPRISE_PRO+'?'ENTERPRISE_PRO': enterpriseDetails.account.accountSubscriptionType).replace( "ENTERPRISE_", "GEM ");
                    enterpriseDetails.account.timezone = enterpriseResponse.company.timeZoneInfo ? enterpriseResponse.company.timeZoneInfo : 'Hawaiian Standard Time';
                    if(enterpriseResponse.company.address){
                        enterpriseDetails.account.address1 = enterpriseResponse.company.address.address1;
                        enterpriseDetails.account.address2 = enterpriseResponse.company.address.address2;
                        enterpriseDetails.account.country = enterpriseResponse.company.address.country ? commonUtilityService.getCountryObject(enterpriseDetails.countries,enterpriseResponse.company.address.country): '';
                    }

                    $scope.generalObj.previousData = angular.copy(enterpriseDetails.account);
                }

            }, function (data) {
                commonUtilityService.showErrorNotification((data&&data.data)?(data.data.errorStr):null);
            });
        }

        /**
         * @func updateEnterpriseDetails()
         * @param {object} enterpriseDetailsForm - form data binding object
         * @desc updates enterprise details
         */
        function updateEnterpriseDetails(enterpriseDetailsForm){
            enterpriseDetails.errorMessage = '';
            var isAlertConfig = false;
            if($scope.isFormValid(enterpriseDetailsForm,enterpriseDetails.account,true)) {
                SettingsService.updateEnterpriseDetails(isAlertConfig,enterpriseDetails.account).success(function (response) {
                    $rootScope.$emit('onUpdateAccountInfo',enterpriseDetails.account);
                    $scope.generalObj.previousData = angular.copy(enterpriseDetails.account);
                    commonUtilityService.showSuccessNotification("Enterprise " + enterpriseDetails.account.companyName + " details have been updated successfully.");

                    analytics.sendEvent({
                        category: "Operations",
                        action: "Update Enterprise Details",
                        label: "Enterprise Settings"
                    });

                }).error(function (data) {
                    enterpriseDetails.errorMessage = data.errorStr;
                });
            }else if($scope.generalObj.isNoChangeError){
                enterpriseDetails.errorMessage = "There is no change in the data to update.";
                $scope.generalObj.isNoChangeError = false;
            }
        }
    }
})();
