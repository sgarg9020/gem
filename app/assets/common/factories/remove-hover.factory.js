'use strict';

(function(){
	angular
		.module('app')
		.factory('removeHover', RemoveHover);
		
	/**
		@function removeHover()
		 - Factory method to remove hover states from CSS
	*/
	function RemoveHover(){
		
	    var factory = {
	    	comb: hoverTouchUnstick
	    };
	    
	    return factory;
	    
	    /**
		 * Converts :hover CSS to :active CSS on mobile devices.
		 * Otherwise, when tapping a button on a mobile device, the button stays in
		 * the :hover state until the button is pressed. 
		 *
		 * Inspired by: https://gist.github.com/rcmachado/7303143
		 * @author  Michael Vartan <michael@mvartan.com>
		 * @version 1.0 
		 * @date    2014-12-20
		 *
		 * NOTE: This solution is used because of the dependency of 3rd party libraries
		 * which may or may not use :hover
		 */
		 
		function hoverTouchUnstick(start) {
		  if('ontouchstart' in document.documentElement &&
		  	 $('body').hasClass('mobile')) {

		    for(var i = document.styleSheets.length - 1; i >= 0; i--) {
		      var sheet = document.styleSheets[i];
		      // Verify if cssRules exists in sheet
		      if(sheet.cssRules && sheet.isCheckedForFocus !== true) {
			    sheet.isCheckedForFocus = true; 
		        // Loop through each rule in sheet
		        for(var j = sheet.cssRules.length - 1; j >= 0; j--) {
		          var rule = sheet.cssRules[j];
		          // Verify rule has selector text
		          if(rule.selectorText) {
		            // Replace hover psuedo-class with active psuedo-class
		            rule.selectorText = rule.selectorText.replace(":hover", ":active");
		          }
		        }
		      }
		    }
		  }
		}
	}
})();