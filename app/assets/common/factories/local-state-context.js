'use strict';

(function(){
    angular
        .module('app')
        .factory('localStateContext',['$location',LocalStateContext]);

    function LocalStateContext($location){

        var storageTypes = { 'local': localStorage, session : sessionStorage};

        return {

            getFilterFromUrl:getFilterFromUrl,
            getFilterFromSessionStorage: getFilterFromSessionStorage,
            getFilterFromLocalStorage : getFilterFromLocalStorage,

            saveFilterInUrl : saveFilterInUrl,
            saveFilterInSessionStorage : saveFilterInSessionStorage,
            saveFilterInLocalStorage : saveFilterInLocalStorage,

            removeFilterFromUrl : removeFilterFromUrl,
            removeFilterFromSessionStorage : removeFilterFromSessionStorage,
            removeFilterFromLocalStorage : removeFilterFromLocalStorage
        };

        function getFilterFromUrl(path, id, filterName,urlPattern){

            var urlFilters = $location.search().filters;

            if(urlFilters){

                var sesFilterPath = 'urlFilters_'+ path;
                var filterPattern = urlPattern?(filterName+'_'+urlPattern):filterName;
                filterPattern = new RegExp('(,|^)' + filterPattern + '(,|$)');
                return (filterPattern.test(urlFilters))? getFilter('session',sesFilterPath, id, filterName) : null;

            }
            return null;

        }

        function getFilterFromSessionStorage(path, id, filterName){

            return getFilter('session',path, id, filterName);

        }

        function getFilterFromLocalStorage(path, id, filterName){

            return getFilter('local',path, id, filterName);

        }

        function getFilter(storage, path, id, filterName ){

            var storageType = storageTypes[storage]?storageTypes[storage]:sessionStorage;
            var filters = storageType.getItem(path);
            filters = (filters)?JSON.parse(filters):{};
            var filterObj = (filters[id])?filters[id]:{};

            return (filterObj[filterName] != null && filterObj[filterName] != undefined)?filterObj[filterName]:null;

        }

        function saveFilterInUrl(path, id, filterName, filterValue,urlPattern){

            var sesFilterPath = 'urlFilters_'+ path;
            saveFilter('session',sesFilterPath, id, filterName, filterValue);
            var urlFilterPattern = urlPattern?(filterName+'_'+urlPattern):filterName;
            var filterPattern = new RegExp('(,|^)' + urlFilterPattern + '(,|$)');
            var urlFilters = $location.search().filters;

            urlFilters = (filterPattern.test(urlFilters))?urlFilters: (urlFilters)?(urlFilters+','+urlFilterPattern):urlFilterPattern;

            $location.search('filters',urlFilters);

        }

        function saveFilterInSessionStorage(path, id, filterName, filterValue){

            return saveFilter('session',path, id, filterName, filterValue);

        }

        function saveFilterInLocalStorage(path, id, filterName, filterValue){

            return saveFilter('local',path, id, filterName, filterValue);

        }

        function saveFilter(storage, path, id, filterName, filterValue){

            var storageType = storageTypes[storage]?storageTypes[storage]:sessionStorage;

            var sessionFilter = storageType.getItem(path);
            sessionFilter = (sessionFilter)?JSON.parse(sessionFilter):{};
            sessionFilter[id] = (sessionFilter[id])?sessionFilter[id] :{};
            sessionFilter[id][filterName] = filterValue;

            storageType.setItem(path, JSON.stringify(sessionFilter));

        }

        function removeFilterFromUrl(path, id, filterName){

            var urlFilters = $location.search().filters;

            if(urlFilters){

                var sesFilterPath = 'urlFilters_'+ path;
                var filterPattern = new RegExp('(,|^)' + filterName + '(,|$)');

                if(filterPattern.test(urlFilters)){
                    removeFilter('session',sesFilterPath, id, filterName);
                    urlFilters = urlFilters.replace(filterPattern,',');
                    var replacePattern =  new RegExp('(^,)|(,$)');
                    var b = urlFilters.replace(replacePattern,',');
                    urlFilters = urlFilters.replace(replacePattern,'');
                }
            }
            $location.search('filters',urlFilters);
        }

        function removeFilterFromSessionStorage(path, id, filterName){

            return removeFilter('session',path, id, filterName);

        }

        function removeFilterFromLocalStorage(path, id, filterName){

            return removeFilter('local',path, id, filterName);

        }

        function removeFilter(storage, path, id, filterName ){

            var storageType = storageTypes[storage]?storageTypes[storage]:sessionStorage;
            var filters = storageType.getItem(path);
            filters = (filters)?JSON.parse(filters):{};

            if(filters[id]){
                delete filters[id][filterName];
                storageType.setItem(path, JSON.stringify(filters));
            }

        }

    }

})();