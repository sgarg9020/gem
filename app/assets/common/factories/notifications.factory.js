'use strict';

(function(){
	angular
		.module('app')
		.factory('notifications', Notifications);
		
	/**
		@function notifications()
		 - Factory method to add notifications to a view
	*/
	function Notifications(){
		
	    var factory = {
	    	init: initNotification,
			closeNotifications:closeNotifications
	    };
	    var notificationIdArr = [];
	    return factory;
	    
		/**
			@function initNotification()
			@param {object} attrs - notification plugin options
			
			pgNotification takes an object with the following key pairs:
			    - attrs.message - notification message
			    - attrs.position - notification position
			    - attrs.type - notification class to use (.alert-[attrs.type])
			    - attrs.id - notification id
			    - attrs.showClose - give the user the ability to close the notification
			    - attrs.timeout - notification timeout in ms
			    - attrs.onShown - function to execute when notification is shown
			    - attrs.onClosed - function to execute when notification is closed
			    
			You can find the pgNotification declaration here:
				- /app/pages/js/pages.js
			
		*/
		function initNotification(attrs){
			if(!angular.element('#'+attrs.id).length){

				angular.element('body').pgNotification(attrs).show();
			} else {
				$('#'+attrs.id).hide();

				angular.element('body').pgNotification(attrs).show();
			}
			notificationIdArr.push(attrs.id);
		}
		function closeNotifications(){

			var notificationId;
			while (notificationId = notificationIdArr.pop())
			{
				$('#'+notificationId).hide();
			}
		}
		
	}	
})();