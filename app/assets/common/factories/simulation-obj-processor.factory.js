'use strict';

(function(){
    angular
        .module('app')
        .factory('SimulationLayerProcessor',[ SimulationLayerProcessor]);

    function SimulationLayerProcessor() {


        var factory = {

            postprocessor: postprocessor,
            preprocessor: preprocessor

        };

        return factory;


// post-process simulation layer response
        function postprocessor(listObj) {
            if (!listObj) return false;
            switch (listObj.type) {
                case "SIM":
                case "SIMs":
                    simObjPostprocessor(listObj);
                    break;
                case "User":
                case "Users":
                    userObjPostprocessor(listObj);
                    break;
                case "Group":
                case "Groups":
                    groupObjPostprocessor(listObj);
                    break;
                case "Invoice":
                case "Invoices":
                    invoiceObjPostprocessor(listObj);
                    break;
                case "Enterprise":
                    enterpriseObjPostprocessor(listObj);
                    break;
                case "Zone":
                case "Zones":
                    zoneListObjPostprocessor(listObj);
                    break;
                case "Alert":
                    alertObjPostprocessor(listObj);
                    break;
            }
        }


        function preprocessor(reqObj) {

        }



    }

})();
