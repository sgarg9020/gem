'use strict';

(function(){
	angular
		.module('app')
		.factory('loadingState',['$timeout','$location', LoadingState]);
	
	/**
		@function loadingState()
		@param - $timeout - angular.js $timeout service
		@desc - loadingState is responsbile for adding the class
		to the body which pace preloader is watching for to remove the
		preloading state.
	*/ 	
	function LoadingState($timeout,$location){

		var pendingRequest=0;
	    var factory = {
	        show: showPace,
	        hide: hidePace
	    };



	    return factory;


		/**	
			@function showPace()
			@desc - show Pace preloader
		*/
		function showPace(isTransparent){

			if(pendingRequest<0){
				pendingRequest = 0;
			}
			pendingRequest++;

			if(!isTransparent){
				$('body').addClass('initial-view-loading');
				console.log("initial view loading added");
			}
			$('body').removeClass('body-initial-load-complete');

			if (window.Pace && !window.Pace.bindings) {
				window.Pace.on('hide', function () {

					if(pendingRequest)
					{
						Pace.restart();
						return;
					}

					$('body').removeClass('initial-view-loading');
					console.log("initial view loading removed");
					//$("#paceTableBlockDiv").hide();
					//$("#paceTableCheckBoxDiv").hide();
					$("#paceFullBlockDiv").hide();
					//$(".paceDisable").hide();

				});

				window.Pace.on("start", function(){

					if(!pendingRequest)
					{
						hidePace();
						return;
					}

					$("#paceFullBlockDiv").show();

					//var p = $location.path();
					//$(".paceDisable").show();
					//if ((p.indexOf('/app/users') != -1) || (p.indexOf('/app/sims') != -1) || (p.indexOf('/app/groups') != -1)) {
					//	$("#paceTableBlockDiv").show();
					//	$("#paceTableCheckBoxDiv").show();
					//}else{
					//	$("#paceFullBlockDiv").show();
					//}
				});
			}

			Pace.restart();

		}

		/**	
			@function hidePace()
			@desc - hide Pace preloader by adding a class to the body which pace is watching for. 
			Check the Pace object and bind an event handler to listen for it's done broadcast
			so we can remove the class from the body for reuse of Pace.
		*/
		function hidePace(reset){

			pendingRequest--;

			if(pendingRequest<0) {
				pendingRequest = 0;

				if($('body').hasClass("body-initial-load-complete"))
				{
					return;
				}
			}

			if(reset) {
				pendingRequest = 0;
			}

			//console.log("HIDE PROG "+pendingRequest);




			if(pendingRequest==0) {
				$('body').addClass('body-initial-load-complete');
				Pace.stop();
			}

		}

	}	
})();