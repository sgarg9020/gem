'use strict';

(function(){
	angular
		.module('app')
		.factory('updateNGRptObj', UpdateNGRptObj);
		
	/**
		@function updateNGRptObj()
		 - Factory method to update ngrepeat objects without triggering
		   angular to create new DOM nodes. 
	*/
	function UpdateNGRptObj(){
		
	    var factory = {
	    	update: update
	    };
	    
	    return factory;

	    /**	
			@function update()
			@param {object} destination - one of the ng-repeat objects
			@param {object} source - source is a reference to one of the positions in destination
			 - Responsible for handling how many times to update the destination with the source
		*/
		function update(destination, source) {	        
            if(Array.isArray(source)){
	            for(var i=0;i<source.length;i++){
		            iterate(destination, source[i]);
	            }
			}
			
			else if(typeof source == "object"){
	            iterate(destination, source);
            }
        }
        
        /**
	        @function iterate()
			@param {object} destination - the array of objects that ng-repeat used to iterator over
			@param {object} source - source is a reference to one of the positions in destination
			 - Responsible for updating the destination with the source	
		*/
        function iterate(destination, source){
	        /* Expando property pattern */
	        var angularJSKeyPattern = /^\$\$/i;
	        	            
            for (var name in source) {
	            /* 
		            Update the destination if the expando properties match
		            and the source keys match.
	            */
                if (source.hasOwnProperty(name) && destination.hasOwnProperty(name) &&
                    !angularJSKeyPattern.test(name)){
                    destination[ name ] = source[ name ];
                }
            }
        }
	}	
})();