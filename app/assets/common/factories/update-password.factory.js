'use strict';

(function(){
	angular
		.module('app')
		.factory('updatePassword',['formMessage', UpdatePassword]);
	
	/**
		@function updatePassword()
		@param {object} formMessage - Factory object
  		 - Comparing new password and verification password
 		 - Making sure the new password and the old password
 		   arent the same
	*/ 	
	function UpdatePassword(formMessage){
		
		var viewModel;
		
	    var factory = {
	    	compare: comparePasswords,
	    	change: changePassword
	    };
	    
	    return factory;
	    	
		/**
			@function comparePasswords()
			@param {string} newPW - (required) the new password
			@param {string} verPW - (required) the verification password
			@param {string} oldPW - (optional) the old password
			 - Compare the two input passwords to make sure they are the same
		*/
		function comparePasswords(newPW, verPW, oldPW){
			
			/*
				TODO - We need to talk about some extended validation for 
				passwords. An enterprise admins account has the ability
				to do destructive things to the enterprise's connectivity.
				
				Talk about a minimum size of 12 characters, 1 capital 
				and 1 letter.				
			*/
			
			var result = {};
			var dirty = false;
			
			if(typeof newPW != "undefined" && 
			   typeof oldPW != "undefined"){
				if(newPW == oldPW){
					result.isError = true;
					result.msg = "The new password must be different than the old password";
					dirty = !dirty;
				}
			}
			
			if(!dirty){
				if(typeof newPW != "undefined" && 
				   typeof verPW != "undefined"){
					if(newPW == verPW){
						result.isError = false;
						result.msg = "";
					} else {
						result.isError = true;
						result.msg = "Passwords don't match.";
						dirty = !dirty;
					}	
				}
			}
			
			if(dirty){
				formMessage.className('alert-danger');
			} else {
				formMessage.className('alert-success');
			}
			formMessage.message(result.isError, result.msg);
		}		
		
		/**
			@function changePassword()
			@param {string} emailId - the email associated with the account to update
			@param {string} newPW - the new password to store
			@param {function} func - (optional) call back function to execute when a password update is complete
		 */
		function changePassword(emailId, newPW, func){
			
			/*
				TODO - The action to change password should be done here.
				
				NOTE: Plain text password logging in only for development
				purposes only and should be removed
			*/
			
			console.log('Update Password for '+emailId+' to '+newPW);
			
			/* Callback function */
			if(func !== undefined){
				func();
			}
	
		}
	}	
})();