'use strict';

(function () {
    angular
        .module('app')
        .factory('pollingAPIService', ['$q', '$interval', '$rootScope',pollingAPIService]);


    function pollingAPIService( $q, $interval, $rootScope) {

        var pollingAPIService = {
            monitorInterval : 10000,
            maxTries : 60,
            pendingQ : [],
            monitor: function (sessionId,request,condition,callback) {
                var pendingSessionItem = {sessionId: sessionId, request: null, nooftries: 0, startTime: new Date()};
                var sessionIdExists = false;
                for (var i = 0; i < this.pendingQ.length; i++) {
                    var pt = this.pendingQ[i];
                    if (pt.sessionId == sessionId) {
                        sessionIdExists = true;
                        break;
                    }
                }
                if (!sessionIdExists)
                    this.pendingQ.push(pendingSessionItem);
                this.start(request,condition,callback);
            },
            quit: function () {
                this.stop();
                this.pendingQ = [];
            },
            remove: function (sessionId) {

                for (var i = 0, c = this.pendingQ.length; i < c; i++) {
                    var session = this.pendingQ[i];
                    if (sessionId == session.sessionId) {
                        this.pendingQ.splice(i, 1);
                        if (!this.pendingQ.length) {
                            this.stop();
                        }
                        break;
                    }
                }
            },
            start: function (request,condition,callback) {
                if (this.timer && angular.isDefined(this.timer)) {
                    return;
                }
                var myMonitor = this;
                onTimerCbk();
                function onTimerCbk() {

                    if (myMonitor.pendingQ.length) {
                        if (!myMonitor.httpReqPromise) {
                            myMonitor.httpReqPromise = $q.defer();
                        }

                        for (var i = 0; i < myMonitor.pendingQ.length; i++) {
                            var pt = myMonitor.pendingQ[i];
                            //Request is not in progress..
                            if (pt.nooftries < myMonitor.maxTries) {
                                pt.nooftries++;
                                pt.request = request(pt.sessionId,true, myMonitor.httpReqPromise.promise).success((function (session) {

                                    return function (response) {
                                        callback(response);
                                        if (condition(response)) {
                                            myMonitor.remove(session.sessionId);
                                        }
                                    }
                                })(pt)).error((function (session) {
                                    return function () {
                                        cancelReqOnTimeOut(session);
                                    }
                                })(pt));
                            }
                            else if (pt.nooftries >= myMonitor.maxTries) {
                                console.log("exceeding maximum number of requests to monitor payment status");
                                myMonitor.remove(pt.sessionId);
                                $rootScope.$broadcast('onRefreshDataUsageTimeout', null);
                            }
                        }
                    }

                    function cancelReqOnTimeOut(session, response) {
                        session.request = null;
                        var curTime = new Date() - session.startTime;
                        if (curTime >= myMonitor.monitorInterval * myMonitor.maxTries) {
                            myMonitor.remove(session.sessionId);
                            console.log("Took more than 2 Minutes to verify status of refresh data usage .Stopping monitor of Refresh data usage status.");

                            if (response && response.status.search('PENDING') != -1) {
                                $rootScope.$broadcast('onRefreshDataUsagePendingTimeout', null);
                            }
                            else
                                $rootScope.$broadcast('onRefreshDataUsageTimeout', null);
                        }
                    }
                }

                this.timer = $interval(onTimerCbk, this.monitorInterval, 0, false);
            },

            stop: function () {
                if (this.timer && angular.isDefined(this.timer)) {
                    $interval.cancel(this.timer);
                    this.timer = undefined;
                }
                if (this.httpReqPromise) {
                    this.httpReqPromise.resolve();
                    this.httpReqPromise = $q.defer();
                }
                for (var i = 0, c = this.pendingQ.length; i < c; i++) {
                    var session = this.pendingQ[i];
                    session.request = null;
                }
            }
        };
        return pollingAPIService;
    }
})();