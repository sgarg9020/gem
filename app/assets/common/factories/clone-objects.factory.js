'use strict';

(function(){
	angular
		.module('app')
		.factory('cloneObjects', Clone);
	
	/**
		@function Clone()
		 - Factory method for creating object clones
	*/ 	
	function Clone(){
	    var factory = {
	        user: cloneUserObject,
	        group: cloneGroupObject,
	        sim: cloneSIMObject,
	        simUser: cloneSIMUserObject
	    };
	    
	    return factory;
		
		/**
			@function cloneUserObject()
			@param {object} obj - an object from the usersData array of objects that is to be cloned
			@return {object} - the cloned object from the obj parameter
			@desc - cloneUserObject will take the obj parameter and clone it to a new object.			
		 */
		function cloneUserObject(obj){
			var clonedObject = {
				type: "User",
				firstName: (typeof obj.firstName === "undefined" ? null : obj.firstName),
				lastName: (typeof obj.lastName === "undefined" ? null : obj.lastName),
				userId: (typeof obj.userId === "undefined" ? null : obj.userId),
				emailId: (typeof obj.emailId === "undefined" ? null : obj.emailId),
				location: (typeof obj.location === "undefined" ? null : obj.location),
				group: (obj.group === null ? null : {
					name: (typeof obj.group === "undefined" ? null : (typeof obj.group.name === "undefined" ? null : obj.group.name)),
					groupId: (typeof obj.group === "undefined" ? null : (typeof obj.group.groupId === "undefined" ? null : obj.group.groupId)),
					parentGroupId: (typeof obj.group === "undefined" ? null : (typeof obj.group.parentGroupId === "undefined" ? null : obj.group.parentGroupId))
				})
			};
			return clonedObject;
		}
		
		/**
			@function cloneGroupObject()
			@param {object} obj - an object from the groups array of objects that is to be cloned
			@return {object} - the cloned object from the obj parameter
			@desc - cloneGroupObject will take the obj parameter and clone it to a new object.
		 */
		function cloneGroupObject(obj){
			var clonedObject = {
			    type: "Group",
			    name: (typeof obj.name === "undefined" ? null : obj.name),
			    groupId: (typeof obj.groupId === "undefined" ? null : obj.groupId),
			    parentGroupId: (typeof obj.parentGroupId === "undefined" ? null : obj.parentGroupId)
			};
			
			return clonedObject;
		}
		
		/**
			@function cloneSIMObject()
			@param {object} obj - an object from the simsData array of objects that is to be cloned
			@return {object} - the cloned object from the obj parameter
			@desc - cloneSIMObject will take the obj parameter and clone it to a new object.	
		 */
		function cloneSIMObject(obj){
			var clonedObject = {
				type: "SIM",
				name: (typeof obj.name === "undefined" ? null : obj.name), 
				simId: (typeof obj.simId === "undefined" ? null : obj.simId),
				iccId: (typeof obj.iccId === "undefined" ? null : obj.iccId), 
				assigned: (typeof obj.assigned === "undefined" ? null : obj.assigned),
				status: (typeof obj.status === "undefined" ? null : obj.status), 
				user: (obj.user === null ? null : {
					userId: obj.user.userId,
					firstName: obj.user.firstName, 
					lastName: obj.user.lastName
				}),
				group: (obj.group === null ? null : {
					name: obj.group.name,
					groupId: obj.group.groupId,
					parentGroupId: obj.group.parentGroupId
				})
			};
			
			return clonedObject;
		}
		
		/**
			@function cloneSIMUserObject()
			@param {object} obj - an object from the usersData array of objects that is to be cloned
			@return {object} - the cloned object from the obj parameter
			@desc - cloneSIMUserObject will take the obj parameter and clone it to a new object with the
			structure for the value of key user in a SIM object.		
		 */
		function cloneSIMUserObject(obj){
			var clonedObject = {
				user: (obj.userId === null ? null : {
					userId: obj.userId,
					firstName: (obj.firstName === null ? null : obj.firstName), 
					lastName: (obj.lastName === null ? null : obj.lastName),
				})
			};
			
			return clonedObject;
		}
		
	}	
})();