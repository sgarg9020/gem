'use strict';

(function(){
	angular
		.module('app')
		.factory('modalDetails', ModalDetails);
	
	/**
		@function modalDetails()
		 - Factory for modals to attach attributes and functions.
		 - Factory stores an instance of a scope and uses mutator
		   functions to updated variables on the scope	
	*/ 	
	function ModalDetails(){
		var viewModel;

	    var factory = {
	        scope: setScope,
	        attributes: setAttributes,
	        submit: setSubmit,
	        cancel: setCancel
	    };
	    
	    return factory;
		
		/**
			@function setScope()
			@param {object} vm - instance of a scope to associate the modal details with
			@param {boolean} bool - truthy value to set default options
		*/
		function setScope(vm, bool){
			viewModel = vm;
			
			if(bool){
				viewModel.modal = {
					id: '',
					formName: '',
					header: '',
					cancel: 'Cancel',
					submit: 'Submit'
				};
				viewModel.modalSubmit = function(){};
				viewModel.modalCancel = function(){};
			}
		}
		
		/**
			@function setAttributes()
			@param {object} obj - object to describe modal
			@param {object} obj.id - id attribute for the modal
			@param {object} obj.formName - name attribute for the form
			@param {object} obj.cancel - text for the cancel button
			@param {object} obj.submit - text for the submit button
		*/
		function setAttributes(obj){
			
			if(obj.id){
				viewModel.modal.id = obj.id;
			}
			
			if(obj.formName){
				viewModel.modal.formName = obj.formName;
			}
			
			if(obj.header){
				viewModel.modal.header = obj.header;
			}
			
			if(obj.cancel){
				viewModel.modal.cancel = obj.cancel;
			}
			
			if(obj.submit){
				viewModel.modal.submit = obj.submit;
			}

		}
		
		
		/**
			@function setSubmit()
			@param {function} func - function to call on submit
		*/
		function setSubmit(func){
			viewModel.modalSubmit = function(form){func(form);};
		}
		
		/**
			setCancel()
			@param {function} func - function to call on cancel	
		*/
		function setCancel(func){
			viewModel.modalCancel = function(){func();};
		}
		
	}	
})();