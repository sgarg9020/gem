'use strict';

(function(){
	angular
		.module('app')
		.factory('tooltips', Tooltips);
	
	/**
		@function Tooltips()
	 	@desc a service to initialize tooltips
	 	@return {object} object literal consisting of accessor(s) to factory functions
	*/ 	
	function Tooltips(){

	    return {
			initTooltips: initTooltips,
			destroyTooltips: destroyTooltips
	    };

		/**
		 @function initTooltips()
		 @param {string} selector - selector to initialize tooltips on
		 @param {string} triggerType - optional value to add a specific trigger type to the tooltip
		 @desc - initToolTips will gather all nodes based on the selector
		 '[data-toggle="tooltip"]' and will initialize them. The content
		 for the tooltip will be taken from the title attribute on the selected
		 node and the position will be taken from the data-placement
		 attributre on the selected node.
		 */
		function initTooltips(selector, triggerType){
			var is_touch_device = 'ontouchstart' in document.documentElement;

			$(selector).tooltip({
				animation: true,
				container: 'body',
				trigger: (is_touch_device ? 'click' : (triggerType ? triggerType : 'hover')),
				template: '<div class="tooltip gs-tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
			});

			if(is_touch_device)
				$(selector).on("touchstart", _invokeTooltip);
		}

		/**
		 @function destroyTooltips()
		 @param {string} selector - selector to initialize tooltips on
		 @desc - destroy instances of the tooltip from the selector
		 */
		function destroyTooltips(selector){
			$(selector).tooltip("destroy");
		}

		/**
		 @function _invokeTooltip()
		 @param event - the touch event object
		 @desc - _invokeTooltip will show the tooltip for the element
		 that the touch event was triggered on. It will also remove
		 the tooltip after 5 seconds
		 */
		function _invokeTooltip(event) {
			$(this).tooltip('show');

			// Remove tooltip after 5 seconds
			setTimeout(function(){
				$(this).tooltip('hide');
			}, 5000);
		};


	}	
})();