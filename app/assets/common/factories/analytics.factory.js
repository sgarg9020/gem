(function(){
    "use strict";
    angular
        .module('app')
        .factory('analytics',analytics);

    /**
     @function analytics()
     - Factory method for accessing the API to send data.
     */
    function analytics(){

        return {
            sendEvent: sendEvent,
            trackPageView: trackPageView,
            setDimension: setDimension
        };

        /**
         * @func sendEvent
         * @param {object} gaObj - object containing .category, .action, and .label for ga.js event tracking
         * @param {number} gaObj.category - category number for Google Analytics event.
         * @param {string} gaObj.action - action for Google Analytics event.
         * @param {string|number} [gaObj.label] - label for Google Analytics event or number to invoke logic to set the label.
         * @desc - sends an event to Google Analytics for recording.
         */
        function sendEvent(gaObj){
            if(_isInstalled() && typeof gaObj === "object"){
                var fieldsObject = {
                    hitType: 'event'
                };

                if(!gaObj.category || !gaObj.action) {
                    console.error("Analytics: sendEvent requires a category and action key");
                    return false;
                }

                fieldsObject.eventCategory = gaObj.category;

                // Action
                if(typeof gaObj.action === "string"){
                    fieldsObject.eventAction = (gaObj.action).toLowerCase();}

                // Label
                if(typeof gaObj.label === "string"){
                    fieldsObject.eventLabel = (gaObj.label).toLowerCase();}
                else{
                    console.error("Analytics: sendEvent requires a valid label type");
                    return false;
                }

                ga('send', fieldsObject);
            }
        }

        /**
         * @func trackPageView
         * @desc tracks the current page based on angular.js hashbang
         */
        function trackPageView(){
            if(_isInstalled()){
                var hash = (window.location.hash).replace("#","").split('?')[0];

                ga('set', 'page', hash);
                ga('send', 'pageview');
            }
        }

        function setDimension(index,value){
            var dimension = 'dimension' + index;
            if(_isInstalled()){
                ga('set',dimension, value);
            }
        }


        /**
         * @func _isInstalled()
         * @desc - determines if Google Analytics is installed.
         * @returns {boolean}
         * @private
         */
        function _isInstalled(){
            if(window.ga)
                return true;
            console.error("Analytics: Google Analytics isn't included");
            return false;
        }

    }
})();