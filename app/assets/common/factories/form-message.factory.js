'use strict';

(function(){
	angular
		.module('app')
		.factory('formMessage', FormMessage);
	
	/**
		@function formMessage()
		 - Factory method for forms to place alert messages in a form
		 - formMessage factory stores an instance of a scope
		   and uses mutator functions to updated variables on the scope
	*/ 	
	function FormMessage(){
		var viewModel;

	    var factory = {
	        scope: setScope,
	        className: setClass,
	        message: setMessage
	    };
	    
	    return factory;
		
		/**	
			@function setScope()
			@param {object} vm - instance of a scope to associate the message with	
		*/
		function setScope(vm){
			viewModel = vm;
		}
		
		/**
			@function setClass()
			@param {string} c - css class to associate with the alert (no . before classname)	
		*/
		function setClass(c){
			viewModel.alertClass = c;
		}
	    
	    /**
			@function setMessage()
			@param {bool} bool - ng-show boolean value
			@param {string} str - message string to add to the scope errorMessage key	
		*/
	    function setMessage(bool, str){
		    if(typeof viewModel != "object"){
		    	return console.error('formMessage(): scope is not defined');
		    }
		    else{
		   		viewModel.isError = bool;
		   		viewModel.errorMessage = str;
		   	}
	    }
	}	
})();