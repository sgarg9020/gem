'use strict';

(function(){
    angular
        .module('app')
        .factory('commonUtilityService',['$http','notifications','$q','$interval','$rootScope','$sanitize','$timeout',CommonUtilityService]);



    function CommonUtilityService($http,notifications,$q,$interval,$rootScope,$sanitize,$timeout){

        var factory = {

            showErrorNotification: showErrorNotification,
            showSuccessNotification: showSuccessNotification,
            showWarningNotification: showWarningNotification,
            extendHttpPromise:extendHttpPromise,
            getCountries: getCountries,
            filterSelectedRows:filterSelectedRows,
            getCountryObject:getCountryObject,
            getDataUsageUnit:getDataUsageUnit,
            getUsageInUnit:getUsageInUnit,
            getUsageInMB:getUsageInMB,
            getAccountName:getAccountName,
            getLocalCurrencySign:getLocalCurrencySign,
            prepareHttpRequest:prepareHttpRequest,
            preventDataTableActivePageNoClick:preventDataTableActivePageNoClick,
            checkMobile: checkMobile,
            storeEnterpriseTokenExpiryTime: storeEnterpriseTokenExpiryTime,
            storeUserTokenExpiryTime: storeUserTokenExpiryTime,
            calculateByteRange:calculateByteRange,
            getUsageInBytes:getUsageInBytes,
            getTimeZones : getTimeZoneList,
            getSupportedCurrencies:getSupportedCurrencies,
            showBulkErrorNotification:showBulkErrorNotification,
            getCountriesString:getCountriesString,
            setSubscriptionTypeInSessionStorage:setSubscriptionTypeInSessionStorage
        };

        var userTokenRenewalTriggered = false;
        var enterpriseTokenRenewalTriggered = false;
        var userTokenNearExpiry = false;
        var enterpriseTokenNearExpiry = false;
        var tokenRenewalFailed = false;
        var pendingRequests = [];
        var KB = 1024;
        var MB = 1024 * 1024;
        var GB = 1024*1024*1024;
        var TB = 1024*1024*1024*1024;

        /**
         @function showErrorNotification()
         @return {boolean} false, indicating that something went wrong.
         - When an update to data fails, call this function to add an error message
         */
        function showErrorNotification(message,errorInt,warningModalObj, timeInMillisecond){

            if(message.type ==='ErrorsGroup') {
                showBulkErrorNotification(message,warningModalObj);
            }else if(message.type ==="error"){
                initNotification(message.errorStr,"danger");
            }else {
                if (!message) {
                    message = "Something went wrong. Try again?";
                }

                if (message == 'Do not show this message as session is already expired and we are in login page.' || message == 'Cancelled on state change')
                    return;
                if (errorInt == 100000)
                    return;
                /* Notifications factory */
                initNotification(message,"danger", timeInMillisecond);
            }
        }

        function initNotification(message,type, timeInMillisecond){
            message = $sanitize(message);
            notifications.init({
                style: "bar",
                message: message,
                type: type,
                id: "enterpriseMsgAlert",
                position: "top",
                timeout: timeInMillisecond ? timeInMillisecond : 10000
            });
        }
        function showSuccessNotification(message, timeInMillisecond){
            /* Notifications factory */
            initNotification(message,"success", timeInMillisecond);
        }

        function showWarningNotification(message, timeInMillisecond){
            /* Notifications factory */
            initNotification(message,"warning", timeInMillisecond);
        }

        function showBulkErrorNotification(messages,warningModalObj){
           /* if(messages.errors) {
                var errorMessages = prepareErrorMessages(messages.errors,"error","errorStr");
                var warningMessages = prepareErrorMessages(messages.warnings,"warning","warningStr");
                var notificationMessage = formatErrorMessages(errorMessages)+formatErrorMessages(warningMessages);
                /!* Notifications factory *!/
                notifications.init({
                    style: "simple",
                    message: notificationMessage,
                    id: "enterpriseBulkMsgAlert",
                    position: "top-right",
                    timeout: 0
                });
                if(warningModalObj.onCancel){
                    warningModalObj.onCancel();
                }
            }else{*/
                $('.modal').modal('hide');
                notifications.closeNotifications();
                var errorMessages = prepareErrorMessages(messages.errors,"error","errorStr");
                var warningMessages = prepareErrorMessages(messages.warnings,"warning","warningStr");
                warningModalObj.updateWarningModal({errors:errorMessages,warnings:warningMessages},warningModalObj.onContinue,warningModalObj.onCancel,warningModalObj.disableButtons);
                $timeout(function () {
                    $('#warningModal').modal('show');
                },700);
            //}
        }

        function prepareErrorMessages(messages,type,messageAttr){
            if(!type && messages[0]){
                type = messages[0].type
            }
            if(!messageAttr && messages[0]){
                messageAttr = messages[0].type + 'Str';
            }
            var processedMessages = [];
            if(messages){
                for (var i = 0; i < messages.length; i++) {
                    var messageObj = {};
                    var message =  messages[i][messageAttr];
                    message=message.replace(/\[/g,"").replace(/\]/g,"");
                    if(message.indexOf(':') > 0){
                        messageObj.isAccordian = true;
                        messageObj.message = $sanitize(message.substring(message.indexOf(':')+2));
                        messageObj.description = $sanitize(message.substring(0,message.indexOf(':')));
                    }else{
                        messageObj.isAccordian = false;
                        messageObj.message = message;
                    }

                    messageObj.type = type;
                    processedMessages.push(messageObj);
                }
            }

            return processedMessages;
        }

        function formatErrorMessages(messages) {
            var formattedMessage ='';
            if (messages.length>0) {
                formattedMessage = '<div class="panel-group" id="' + messages[0].type + 'accordion">';

                for (var i = 0; i < messages.length; i++) {
                    if(messages[i].isAccordian){
                    formattedMessage += '<div class="panel panel-default">' +
                        '<div class="panel-heading"';

                    if (messages[i].type == 'warning') {
                        formattedMessage += ' style="background-color: #fef6dd">';
                    } else {
                        formattedMessage += ' style="background-color: #fddddd">';
                    }
                    formattedMessage += '<h4 class="panel-title">' +
                        '<a class="accordion-toggle p-r-10" data-toggle="collapse" data-parent="#' + messages[0].type + 'accordion" href="#' + messages[i].type + i + '">' +
                        messages[i].message +
                        '</a>' +
                        '</h4>' +
                        '</div>' +
                        '<div id="' + messages[i].type + i + '" class="panel-collapse collapse ';
                    formattedMessage += '">' +
                        '<div class="panel-body">' +
                        messages[i].description +
                        '</div>' +
                        '</div>' +
                        '</div>';
                    }else{
                        formattedMessage += '<div class="panel panel-default">' +
                            '<div class="panel-heading"';

                        if (messages[i].type == 'warning') {
                            formattedMessage += ' style="background-color: #fef6dd">';
                        } else {
                            formattedMessage += ' style="background-color: #fddddd">';
                        }
                        formattedMessage +=  messages[i].message +
                            '</div>' +
                            '</div>';
                    }
                }
                formattedMessage += "</div>";
            }
            return formattedMessage;
        }

        function filterSelectedRows(rows,filter){
            var len = rows.length;
            var i  = 0;
            while(i<len)
            {
                var row = rows[i];
                if (filter(row)) {
                    rows.splice(i, 1);
                    len = rows.length;
                }
                else i++;
            }

        }



        function getLocalCurrencySign(currencyCode){
            var CurrencyInfo = {
                'AED': [2, 'dh', '\u062f.\u0625.', 'DH'],
                'AUD': [2, '$', 'AU$'],
                'BDT': [2, '\u09F3', 'Tk'],
                'BRL': [2, 'R$', 'R$'],
                'CAD': [2, '$', 'C$'],
                'CHF': [2, 'CHF', 'CHF'],
                'CLP': [0, '$', 'CL$'],
                'CNY': [2, '\u00A5', 'RMB\u00A5'],
                'COP': [0, '$', 'COL$'],
                'CRC': [0, '\u20a1', 'CR\u20a1'],
                'CZK': [2, 'K\u010d', 'K\u010d'],
                'DKK': [18, 'kr', 'kr'],
                'DOP': [2, '$', 'RD$'],
                'EGP': [2,'\u00A3','LE'],
                'EUR': [18, '\u20AC', '\u20AC'],
                'GBP': [2,'\u00A3','GB\u00A3'],
                'HKD': [2, '$', 'HK$'],
                'ILS': [2, '\u20AA', 'IL\u20AA'],
                'INR': [2, '\u20B9', 'Rs'],
                'ISK': [0, 'kr', 'kr'],
                'JMD': [2, '$', 'JA$'],
                'JPY': [0, '\u00A5', 'JP\u00A5'],
                'KRW': [0, '\u20A9', 'KR\u20A9'],
                'LKR': [2, 'Rs', 'SLRs'],
                'MNT': [0, '\u20AE', 'MN\u20AE'],
                'MXN': [2, '$', 'Mex$'],
                'MYR': [2, 'RM', 'RM'],
                'NOK': [18, 'kr', 'NOkr'],
                'PAB': [2, 'B/.', 'B/.'],
                'PEN': [2, 'S/.', 'S/.'],
                'PHP': [2, '\u20B1', 'Php'],
                'PKR': [0, 'Rs', 'PKRs.'],
                'RUB': [42, 'руб.', 'руб.'],
                'SAR': [2, 'Rial', 'Rial'],
                'SEK': [2, 'kr', 'kr'],
                'SGD': [2, '$', 'S$'],
                'THB': [2, '\u0e3f', 'THB'],
                'TRY': [2, 'TL', 'YTL'],
                'TWD': [2, 'NT$', 'NT$'],
                'USD': [2, '$', 'US$'],
                'UYU': [2, '$', 'UY$'],
                'VND': [0, '\u20AB', 'VN\u20AB'],
                'YER': [0, 'Rial', 'Rial'],
                'ZAR': [2, 'R', 'ZAR'],
                'AFN': [16, 'Af.', 'AFN'],
                'ALL': [0, 'Lek', 'Lek'],
                'AMD': [0, 'Dram', 'dram'],
                'AOA': [2, 'Kz', 'Kz'],
                'ARS': [2, '$', 'AR$'],
                'AWG': [2, 'Afl.', 'Afl.'],
                'AZN': [2, 'man.', 'man.'],
                'BAM': [18, 'KM', 'KM'],
                'BBD': [2, '$', 'Bds$'],
                'BGN': [2, 'lev', 'lev'],
                'BHD': [3, 'din', 'din'],
                'BIF': [0, 'FBu', 'FBu'],
                'BMD': [2, '$', 'BD$'],
                'BND': [2, '$', 'B$'],
                'BOB': [2, 'Bs', 'Bs'],
                'BSD': [2, '$', 'BS$'],
                'BTN': [2, 'Nu.', 'Nu.'],
                'BWP': [2, 'P', 'pula'],
                'BYR': [0, 'BYR', 'BYR'],
                'BZD': [2, '$', 'BZ$'],
                'CDF': [2, 'FrCD', 'CDF'],
                'CUC': [1, '$', 'CUC$'],
                'CUP': [2, '$', 'CU$'],
                'CVE': [2, 'CVE', 'Esc'],
                'DJF': [0, 'Fdj', 'Fdj'],
                'DZD': [2, 'din', 'din'],
                'ERN': [2, 'Nfk', 'Nfk'],
                'ETB': [2, 'Birr', 'Birr'],
                'FJD': [2, '$', 'FJ$'],
                'FKP': [2, '£', 'FK£'],
                'GEL': [2, 'GEL', 'GEL'],
                'GHS': [2, 'GHS', 'GHS'],
                'GIP': [2, '£', 'GI£'],
                'GMD': [2, 'GMD', 'GMD'],
                'GNF': [0, 'FG', 'FG'],
                'GTQ': [2, 'Q', 'GTQ'],
                'GYD': [0, '$', 'GY$'],
                'HNL': [2, 'L', 'HNL'],
                'HRK': [2, 'kn', 'kn'],
                'HTG': [2, 'HTG', 'HTG'],
                'HUF': [0, 'Ft', 'Ft'],
                'IDR': [0, 'Rp', 'Rp'],
                'IQD': [0, 'din', 'IQD'],
                'IRR': [0, 'Rial', 'IRR'],
                'JOD': [3, 'din', 'JOD'],
                'KES': [2, 'Ksh', 'Ksh'],
                'KGS': [2, 'KGS', 'KGS'],
                'KHR': [2, 'Riel', 'KHR'],
                'KMF': [0, 'CF', 'KMF'],
                'KPW': [0, '\u20A9KP', 'KPW'],
                'KWD': [3, 'din', 'KWD'],
                'KYD': [2, '$', 'KY$'],
                'KZT': [2, '\u20B8', 'KZT'],
                'LAK': [0, '\u20AD', '\u20AD'],
                'LBP': [0, 'L£', 'LBP'],
                'LRD': [2, '$', 'L$'],
                'LSL': [2, 'LSL', 'LSL'],
                'LTL': [2, 'Lt', 'Lt'],
                'LVL': [2, 'Ls', 'Ls'],
                'LYD': [3, 'din', 'LD'],
                'MAD': [2, 'dh', 'MAD'],
                'MDL': [2, 'MDL', 'MDL'],
                'MGA': [0, 'Ar', 'MGA'],
                'MKD': [2, 'din', 'MKD'],
                'MMK': [0, 'K', 'MMK'],
                'MOP': [2, 'MOP', 'MOP$'],
                'MRO': [0, 'MRO', 'MRO'],
                'MUR': [0, 'MURs', 'MURs'],
                'MWK': [2, 'MWK', 'MWK'],
                'MZN': [2, 'MTn', 'MTn'],
                'NAD': [2, '$', 'N$'],
                'NGN': [2, '\u20A6', 'NG\u20A6'],
                'NIO': [2, 'C$', 'C$'],
                'NPR': [2, 'Rs', 'NPRs'],
                'NZD': [2, '$', 'NZ$'],
                'OMR': [3, 'Rial', 'OMR'],
                'PGK': [2, 'PGK', 'PGK'],
                'PLN': [2, 'z\u0142', 'z\u0142'],
                'PYG': [0, 'Gs', 'PYG'],
                'QAR': [2, 'Rial', 'QR'],
                'RON': [2, 'RON', 'RON'],
                'RSD': [0, 'din', 'RSD'],
                'RWF': [0, 'RF', 'RF'],
                'SBD': [2, '$', 'SI$'],
                'SCR': [2, 'SCR', 'SCR'],
                'SDG': [2, 'SDG', 'SDG'],
                'SHP': [2, '£', 'SH£'],
                'SLL': [0, 'SLL', 'SLL'],
                'SOS': [0, 'SOS', 'SOS'],
                'SRD': [2, '$', 'SR$'],
                'STD': [0, 'Db', 'Db'],
                'SYP': [16, '£', 'SY£'],
                'SZL': [2, 'SZL', 'SZL'],
                'TJS': [2, 'Som', 'TJS'],
                'TND': [3, 'din', 'DT'],
                'TOP': [2, 'T$', 'T$'],
                'TTD': [2, '$', 'TT$'],
                'TZS': [0, 'TSh', 'TSh'],
                'UAH': [2, '\u20B4', 'UAH'],
                'UGX': [0, 'UGX', 'UGX'],
                'UZS': [0, 'so\u02bcm', 'UZS'],
                'VEF': [2, 'Bs', 'Bs'],
                'VUV': [0, 'VUV', 'VUV'],
                'WST': [2, 'WST', 'WST'],
                'XAF': [0, 'FCFA', 'FCFA'],
                'XCD': [2, '$', 'EC$'],
                'XOF': [0, 'CFA', 'CFA'],
                'XPF': [0, 'FCFP', 'FCFP'],
                'ZMK': [0, 'ZMK', 'ZMK']
            };

            if(CurrencyInfo[currencyCode])
                return CurrencyInfo[currencyCode][1];
            return currencyCode;
        }


        function getCountryObject(countries,code){
            var country = {};
            for(var i in countries){
                if(countries[i].code === code){
                    country = countries[i];
                    break;
                }
            }
            return country;
        }

        function extendHttpPromise(val,reject,defer)
        {
            var def = (!defer)?$q.defer():defer;

            if (reject && val)
                def.reject({data:val,status:0});
            else if(val)
                def.resolve({data:val});

            def.promise.success = function(fn) {
                def.promise.then(function(v){
                    fn(v.data, v.status,v.headers,v.config);
                }, null);
                return def.promise;
            };
            def.promise.error = function(fn) {
                def.promise.then(null, function(v){
                    fn(v.data, v.status,v.headers,v.config);
                });
                return def.promise;
            };
            return def.promise;
        }

        /**
         @function request()
         - Check local storage before making an http request for countries data
         */
        function getCountries(){

            var localCountries = sessionStorage.getItem('gs-countries') || 0;

            if(!localCountries){
                var queryParam = {};

                queryParam.startIndex = 0;
                queryParam.count = 10000;

                return $http({
                    method: 'GET',
                    url: ENTERPRISE_PLAN_API_BASE_V3+'account/countries',
                    params: queryParam
                }).success(function(response){
                    console.log('Requested countries', response);
                    sessionStorage.setItem('gs-countries', JSON.stringify(response));
                });
            }
            /*
             TODO - Perform a check on the database to see if the countries data
             should be considered "dirty". If the countries data is dirty, we will
             have to get a fresh copy of the countries and overwrite the stored local
             storage value gs-countries
             */
            return extendHttpPromise(JSON.parse(localCountries));
        }

        function getDataUsageUnit(usageInBytes) {
            var usageUnit = 'Bytes';
            if (usageInBytes >= KB && usageInBytes < MB) {
                usageUnit = 'KB';
            }
            if (usageInBytes >= MB && usageInBytes < GB) {
                usageUnit = 'MB';
            }
            if (usageInBytes >= GB && usageInBytes < TB) {
                usageUnit = 'GB';
            } else if (usageInBytes >= TB) {
                usageUnit = 'TB';
            }
            return usageUnit;
        }

        function getUsageInUnit(usageInBytes, usageUnit,decimals) {
            decimals = decimals ||2;
            var usageInUnit = parseFloat(usageInBytes.toFixed(decimals));
            if (usageUnit === 'KB') {
                usageInUnit = parseFloat((usageInBytes / KB).toFixed(decimals));
            }
            if (usageUnit === 'MB') {
                usageInUnit = parseFloat((usageInBytes / MB).toFixed(decimals));
            }
            if (usageUnit === 'GB') {
                usageInUnit = parseFloat((usageInBytes / GB).toFixed(decimals));
            }
            if (usageUnit === 'TB') {
                usageInUnit = parseFloat((usageInBytes / TB).toFixed(decimals));
            }
            return usageInUnit;
        }

        /**
         * @func getUsageInBytes - to convert given usage in bytes
         * @param usageInUnit - usage in terms of usageUnit
         * @param usageUnit
         * @return usage in bytes
         */
        function getUsageInBytes(usageInUnit,usageUnit){

            var dataUsedInBytes = usageInUnit;

            if (usageUnit === 'KB') {
                dataUsedInBytes = usageInUnit * KB;
            }
            else if (usageUnit === 'MB') {
                dataUsedInBytes = usageInUnit * MB;
            }
            else if (usageUnit === 'GB') {
                dataUsedInBytes = usageInUnit * GB;
            }
            else if (usageUnit === 'TB') {
                dataUsedInBytes = usageInUnit * TB;
            }

            return dataUsedInBytes;
        }


        function getUsageInMB() {
            return function (usageInBytes) {
                if(usageInBytes < 1 ){
                    return 0;
                }
                var usageInMB = parseFloat((usageInBytes / MB).toFixed(2));
                return usageInMB > 0?usageInMB:'<0.01';
            }
        }
        function getAccountName() {
            return function (account) {
                var accountName = 'Default';
                var subAcc = false;
                var enterpriseAcc;
                var i=0;
                for(i;i<account.enterpriseAccountDetails.length;i++){
                    var j=0;
                    for(j;j<account.enterpriseAccountDetails[i].parentAccountDetails.length;j++){
                        if(account.enterpriseAccountDetails[i].parentAccountDetails[j].accountType === 'GROUP' &&
                        account.enterpriseAccountDetails[i].parentAccountDetails[j].role === 'ENTERPRISE_SIM_USER'){
                            accountName =  account.enterpriseAccountDetails[i].parentAccountDetails[j].accountName;
                            subAcc = true;
                            break;
                        }
                        else if( account.enterpriseAccountDetails[i].parentAccountDetails[j].accountType  === "ENTERPRISE"){
                            enterpriseAcc = account.enterpriseAccountDetails[i].parentAccountDetails[j].accountName;
                        }
                    }
                }

                if( !subAcc ) accountName = enterpriseAcc;
                return accountName;
            }
        }

        function getCurrentTimezoneDate(date) {
            date = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds(), date.getUTCMilliseconds());
            if (sessionStorage.getItem("timezone")) {
                date.setTime(date.getTime() + getEnterpriseTimezoneINMilliSec(sessionStorage.getItem("timezone")));
            }
            return date;
        }
        function getEnterpriseTimezoneINMilliSec(timezone){
            var timezoneArr = timezone.split(':');
            var sign = parseInt(timezoneArr[0]) ? parseInt(timezoneArr[0]) < 0 ? -1 : 1 : 0;
            return sign*(Math.abs(parseInt(timezoneArr[0]) * 60 * 60 * 1000) + Math.abs(parseInt(timezoneArr[1]) * 60 * 1000))
        }
        function prepareHttpRequest(url, method, access, params, data,config,isBackgroundCall) {

            if(!ENABLE_RENEW_TOKEN_SUPPORT){
                return $http({
                    url: url,
                    method: method,
                    data: data,
                    headers: {
                        accessCheck: access
                    },
                    params: params,
                    config: config
                });
            }


            //what token needs renewal
            var enterpriseToken = sessionStorage.getItem("enterpriseTokenExpiryTime");
            var enterpriseTokenExpiryTime = (enterpriseToken && enterpriseToken != "undefined") ? getDateObj(enterpriseToken+" GMT") : '';
            var userToken = sessionStorage.getItem("userTokenExpiryTime");
            var userTokenExpiryTime = (userToken && userToken != "undefined") ? getDateObj(userToken+" GMT") : '';
            var currentTime = new Date();
            var token = sessionStorage.getItem("token");

            var enterpriseTokenExpiryInterval = Number(sessionStorage.getItem("enterpriseTokenExpiryInterval"));
            var userTokenExpiryInterval = Number(sessionStorage.getItem("userTokenExpiryInterval"));

            if((userTokenExpiryTime instanceof Date) && (userTokenExpiryInterval > RENEW_TOKEN_THRESHOLD_TIME) && (currentTime < userTokenExpiryTime) && userTokenExpiryTime - currentTime < RENEW_TOKEN_THRESHOLD_TIME){
                userTokenNearExpiry = true;
            }
            if((enterpriseTokenExpiryTime instanceof Date) && (enterpriseTokenExpiryInterval > RENEW_TOKEN_THRESHOLD_TIME) && (currentTime < enterpriseTokenExpiryTime) && enterpriseTokenExpiryTime - currentTime  < RENEW_TOKEN_THRESHOLD_TIME){
                enterpriseTokenNearExpiry = true;
            }


            if ( !ENABLE_RENEW_TOKEN_SUPPORT || isBackgroundCall || !(userTokenNearExpiry || enterpriseTokenNearExpiry || userTokenRenewalTriggered || enterpriseTokenRenewalTriggered) || tokenRenewalFailed) {
                return $http({
                    url: url,
                    method: method,
                    data: data,
                    headers: {
                        accessCheck: access
                    },
                    params: params,
                    config: config
                });
            }
            else if(!(userTokenRenewalTriggered || enterpriseTokenRenewalTriggered)){
               // do call to renew token, and halt all other calls till token renews
                var defer = $q.defer();
                var promise = extendHttpPromise(null,null,defer);
                addPendingRequest(defer, function () {
                    return prepareHttpRequest(url, method, access, params, data, config);
                });
                var renewTokenRequests = [];
                if(userTokenNearExpiry) {
                    userTokenRenewalTriggered = true;
                    var userToken = sessionStorage.getItem("userToken");
                    renewTokenRequests.push($http({
                        url: ENTERPRISE_USER_API_BASE + 'users/renewToken',
                        method:'POST',
                        headers: {
                            token: userToken
                        }
                    }))
                }
                if(enterpriseTokenNearExpiry){
                    enterpriseTokenRenewalTriggered = true;
                    var entToken = sessionStorage.getItem("token");
                    renewTokenRequests.push($http({
                        url: ENTERPRISE_USER_API_BASE + 'users/renewToken',
                        method:'POST',
                        headers: {
                            token: entToken
                        }
                    }))
                }
                $q.all(renewTokenRequests).then(function(responses){
                    var i=0;
                    if(userTokenNearExpiry){
                        var response = responses[i].data;
                        i++;
                        storeUserTokenExpiryTime(response);
                        userTokenRenewalTriggered = false;
                        userTokenNearExpiry = false;
                    }
                    if(enterpriseTokenNearExpiry){
                        var response1 = responses[i].data;
                        storeEnterpriseTokenExpiryTime(response1);
                        enterpriseTokenRenewalTriggered = false;
                        enterpriseTokenNearExpiry = false;
                    }
                    tokenRenewalFailed = false;
                    processPendingRequests(pendingRequests);
                },function(data){
                    console.log(data);
                    userTokenRenewalTriggered = false;
                    userTokenNearExpiry = false;
                    enterpriseTokenRenewalTriggered = false;
                    enterpriseTokenNearExpiry = false;
                    tokenRenewalFailed = true;
                    rejectPendingRequests(pendingRequests,data);
                });
                return promise
            } else {
                var defer = $q.defer();
                var promise = extendHttpPromise(null,null,defer);
                addPendingRequest(defer, function () {
                    return prepareHttpRequest(url, method, access, params, data, config);
                });
                return promise;
            }
        }

        /**
         * calculates expiry time interval based on current time
         * @param response
         * @returns {number}
         */
        function getExpiryInterval(response) {
            var currentTime = new Date();
            var expiryTime = response.expiryDate ? getDateObj(response.expiryDate + " GMT") : 0;
            var expiryInterval = expiryTime - currentTime;
            return expiryInterval;
        }

        /**
         * Stores token,expirytime and expiryTime interval for enterprise token
         * @param response
         */
        function storeEnterpriseTokenExpiryTime(response){
            var expiryInterval = getExpiryInterval(response);
            sessionStorage.setItem("enterpriseTokenExpiryInterval", expiryInterval);
            sessionStorage.setItem("token", response.token);
            sessionStorage.setItem("enterpriseTokenExpiryTime",response.expiryDate);
        }

        /**
         * Stores token,expirytime and expiryTime interval for user token
         * @param response
         */
        function storeUserTokenExpiryTime(response){
            var expiryInterval = getExpiryInterval(response);
            sessionStorage.setItem("userTokenExpiryInterval", expiryInterval);
            sessionStorage.setItem("userToken", response.token);
            sessionStorage.setItem("userTokenExpiryTime", response.expiryDate);
        }

        function addPendingRequest(defer, func) {
            pendingRequests.push(
                {
                    defer: defer,
                    func: func
                }
            );
        }
        function processPendingRequests(pendingRequests) {
            while (pendingRequests.length>0) {
                var req = pendingRequests.pop();
                (function(req){
                req.func().success(function(data,status,headers,config){
                    req.defer.resolve({"data":data,"status":status,"headers":headers,"config":config});
                }).error(function(data){
                    req.defer.reject({"data":data});
                });
                })(req);
            }
        }

        /**
         * Rejects all pending requests
         * @param pendingRequests
         * @param data
         */
        function rejectPendingRequests(pendingRequests,data){
            while (pendingRequests.length>0) {
                var req = pendingRequests.pop();
                req.defer.reject({"data":data});
            }
        }

        function preventDataTableActivePageNoClick( tableId ){
            // prevent active page number clicking
            $("#"+ tableId + "_paginate li.active > a").click(function(e){
                e.preventDefault();
                e.stopPropagation();return false;
            });
        }

        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            webOS: function() {
                return navigator.userAgent.match(/webOS/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows() || isMobile.webOS());
            }
        };

        var isTablet = {
            iOS: function() {
                return navigator.userAgent.match(/iPad/i);
            },
            Android: function() {
                return navigator.userAgent.match(/Android/i) && !navigator.userAgent.match(/Mobile/i);
            },
            any: function() {
                return isTablet.iOS() || isTablet.Android();
            }
        };

        function checkMobile(){
            if ($(window).width() < 960 || isMobile.any() || isTablet.any()) {
                return true;
            }

            return false;
        }

        function getRefreshDataUsageStatus(sessionId, cancelPromise) {
            var headerAdd=['REFRESH_DATAUSAGE'];
            var accId = sessionStorage.getItem('accountId');
            if (cancelPromise) {
                return prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE  + "accounts/account/" + accId  + "/users/datausage/refresh/" + sessionId,'GET',headerAdd,false,null, {timeout: cancelPromise});
            }
            else
                return prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE  + "accounts/account/" + accId  + "/users/datausage/refresh/" + sessionId,'GET',headerAdd);
        }


        var refreshDataUsageMonitor = {
            monitorInterval:3000,
            maxTries:60,
            pendingQ:[],
            monitor:function(sessionId){
                var pendingSessionItem  = {sessionId:sessionId,request:null,nooftries:0,startTime:new Date()};
                var sessionIdExists = false;
                for(var i=0;i<this.pendingQ.length;i++)
                {
                    var pt = this.pendingQ[i];
                    if(pt.sessionId==sessionId)
                    {
                        sessionIdExists = true;
                        break;
                    }
                }
                if(!sessionIdExists)
                    this.pendingQ.push(pendingSessionItem);
                this.start();
            },
            quit:function(){
                this.stop();
                this.pendingQ = [];
            },
            remove:function(sessionId){

                for(var i= 0,c=this.pendingQ.length;i<c;i++)
                {
                    var session = this.pendingQ[i];
                    if(sessionId == session.sessionId)
                    {
                        this.pendingQ.splice(i,1);
                        if(!this.pendingQ.length)
                        {
                            this.stop();
                        }
                        break;
                    }
                }
            },
            start:function(){
                if(this.timer && angular.isDefined(this.timer))
                {
                    return;
                }
                var myMonitor = this;
                onTimerCbk();
                function onTimerCbk(){

                    if(myMonitor.pendingQ.length)
                    {
                        if(!myMonitor.httpReqPromise)
                        {
                            myMonitor.httpReqPromise = $q.defer();
                        }

                        for(var i=0;i<myMonitor.pendingQ.length;i++)
                        {
                            var pt = myMonitor.pendingQ[i];
                            //Request is not in progress..
                            if(!pt.request && pt.nooftries<myMonitor.maxTries){
                                pt.nooftries++;
                                pt.request = getRefreshDataUsageStatus(pt.sessionId,myMonitor.httpReqPromise.promise).success((function(session){

                                    return function(response){

                                        if(response.status == "COMPLETED" || response.status == "FAILED")
                                        {
                                            myMonitor.remove(session.sessionId);
                                            if(response.status == "COMPLETED")
                                                $rootScope.$broadcast('onRefreshDataUsageCompleted',response);
                                            if(response.status == "FAILED")
                                                $rootScope.$broadcast('onRefreshDataUsageStatusFailed',response);
                                        }
                                        else
                                        {
                                            cancelReqOnTimeOut(session,response);
                                        }
                                    }})(pt)).error((function(session){return function(){
                                    cancelReqOnTimeOut(session);
                                }})(pt));
                            }
                            else if(!pt.request && pt.nooftries>=myMonitor.maxTries)
                            {
                                console.log("exceeding maximum number of requests to monitor payment status");
                                myMonitor.remove(pt.sessionId);
                                $rootScope.$broadcast('onRefreshDataUsageTimeout',null);
                            }
                        }
                    }

                    function cancelReqOnTimeOut(session,response){
                        session.request = null;
                        var curTime = new Date() - session.startTime;
                        if(curTime>=myMonitor.monitorInterval*myMonitor.maxTries)
                        {
                            myMonitor.remove(session.sessionId);
                            console.log("Took more than 2 Minutes to verify status of refresh data usage .Stopping monitor of Refresh data usage status.");

                            if(response &&  response.status.search('PENDING')!=-1)
                            {
                                $rootScope.$broadcast('onRefreshDataUsagePendingTimeout',null);
                            }
                            else
                                $rootScope.$broadcast('onRefreshDataUsageTimeout',null);
                        }
                    }
                }
                this.timer = $interval(onTimerCbk,this.monitorInterval,0,false);
            },

            stop:function(){
                if(this.timer && angular.isDefined(this.timer))
                {
                    $interval.cancel(this.timer);
                    this.timer = undefined;
                }
                if(this.httpReqPromise)
                {
                    this.httpReqPromise.resolve();
                    this.httpReqPromise = $q.defer();
                }
                for(var i= 0,c=this.pendingQ.length;i<c;i++)
                {
                    var session = this.pendingQ[i];
                    session.request = null;
                }
            }
        };

        /**
         * @function caluculateByteRange
         * @param dataLimit
         * @param {string} specificUnit - specify B, KB, MB, or GB to get data in a specific unit
         * @returns {{data: *, measure: string}}
         * Takes data in BYTES and returns data in B, KB, MB or GB
         */
        function calculateByteRange(dataLimit, specificUnit){
            var measure='B';
            var data=dataLimit;

            if (specificUnit && typeof specificUnit == "string") {
                specificUnit = specificUnit.toUpperCase();

                if (specificUnit == "GB") {
                    measure = 'GB';
                    data = dataLimit / ((1024.0) * (1024.0) * (1024.0));
                }

                else if (specificUnit == "MB") {
                    measure = 'MB';
                    data = dataLimit / ((1024.0) * (1024.0));
                }

                else if (specificUnit == "KB") {
                    measure = 'KB';
                    data = dataLimit / 1024.0;
                }

                else{
                    console.error("calculateByteRange expects specificUnit to be B, KB, MB, or GB");
                }

            } else {
                if ((dataLimit / (1024 * 1024 * 1024)) >= 1.0 || (dataLimit / (1024 * 1024 * 1024)) <= -1.0) {
                    measure = 'GB';
                    data = dataLimit / ((1024.0) * (1024.0) * (1024.0));
                }
                else if ((dataLimit / (1024.0 * 1024.0)) >= 1.0 || (dataLimit / (1024.0 * 1024.0)) <= -1.0) {
                    measure = 'MB';
                    data = dataLimit / ((1024.0) * (1024.0));
                }
                else if ((dataLimit / (1024.0)) >= 1.0 || (dataLimit / (1024.0)) <= -1.0) {
                    measure = 'KB';
                    data = dataLimit / 1024.0;
                }
            }

            data=(Math.floor(data*100))/100;
            return {data:data,measure:measure};
        }

        function getTimeZoneList(){
            var localTimezones = sessionStorage.getItem('gs-timezones') || 0;
            if(!localTimezones){
                var queryParam = {};
                queryParam.startIndex = 0;
                queryParam.count = 10000;

                return $http({
                    method: 'GET',
                    url: ENTERPRISE_GIGSKY_BACKEND_BASE+'timezones',
                    params: queryParam
                }).success(function(response){
                    console.log('Requested timezones', response);
                    sessionStorage.setItem('gs-timezones', JSON.stringify(response));
                });
            }
            console.log('timezones from local storage');
            return extendHttpPromise(JSON.parse(localTimezones));
        }

        function getSupportedCurrencies(){
            var localSupportedCurrencies = sessionStorage.getItem('gs-currencies') || 0;
            if(!localSupportedCurrencies){
                var queryParam = {};

                queryParam.startIndex = 0;
                queryParam.count = 10000;

                return $http({
                    method: 'GET',
                    url: ENTERPRISE_GIGSKY_BACKEND_BASE+'currency',
                    params: queryParam
                }).success(function(response){
                    console.log('Requested supportedCurrencies', response);
                    sessionStorage.setItem('gs-currencies', JSON.stringify(response));
                });
            }
            console.log('supportedCurrencies from local storage');
            return extendHttpPromise(JSON.parse(localSupportedCurrencies));
        }

        function getCountriesString(countryList){
            if (countryList) {
                var location = '';
                for (var i = 0; i < countryList.length; i++) {
                    if (i > 0)
                        location = location + " , ";
                    location = location + countryList[i].countryName;
                }
                return location;
            }
            return 'Not Available';
        }

        function setSubscriptionTypeInSessionStorage(accountSubscriptionType){
            var subscriptionType =  ENTERPRISE_DEFAULT_SUBSCRIPTION_TYPE;
            $rootScope.isIotReseller = false;
            if(accountSubscriptionType == "IOT"){
                subscriptionType = "IOT";
                $rootScope.isIotReseller = true;
            }
            sessionStorage.setItem("accountSubscriptionType",subscriptionType);
        }

        factory.refreshDataUsageMonitor = refreshDataUsageMonitor;
        return factory;

    }

})();
