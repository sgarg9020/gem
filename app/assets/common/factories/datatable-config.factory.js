'use strict';

(function () {
    angular
        .module('app')
        .factory('dataTableConfigService', [ 'localStateContext','$state', DataTableConfigService]);

    /**
     @function DataTableConfigService()
     @param localStateContext
     @param $state
     @desc factory method for building datatables within GEM
     */
    function DataTableConfigService(localStateContext,$state) {

        var getFilterFuncMap = { URL: 'getFilterFromUrl', session: 'getFilterFromSessionStorage', local: 'getFilterFromLocalStorage'};
        var setFilterFuncMap = { URL: 'saveFilterInUrl', session: 'saveFilterInSessionStorage', local: 'saveFilterInLocalStorage'};
        var removeFilterFuncMap = { URL: 'removeFilterFromUrl', session: 'removeFilterFromSessionStorage', local: 'removeFilterFromLocalStorage'};

        return {
            prepareOptions: prepareOptions,
            prepareRequestParams: prepareRequestParams,
            prepareResponseData: prepareResponseData,
            disableTableHeaderOps:disableTableHeaderOps,
            hidePaceOnFailure: hidePaceOnFailure,
            configureFilters :configureFilters,
            getFilter : getFilter,
            setFilter: setFilter,
            removeFilter: removeFilter,
            setOLanguage:setOLanguage
        };

        function prepareOptions(dataTableOpts,disableStateSave) {

            dataTableOpts.stateSave= (disableStateSave)?!disableStateSave:true;
            dataTableOpts.stateDuration=-1; // to use session storage
            dataTableOpts.serverSide = true;

            if(!dataTableOpts.columns) {
                dataTableOpts.columns = [];
                dataTableOpts.columns[0] = {"width": "60px", "orderable": false};
            }

            dataTableOpts.sDom = "<'exportOptions'T><'table-responsive't><'row'<i p>>";
            dataTableOpts.destroy = true;
            dataTableOpts.scrollCollapse = true;
            dataTableOpts.oTableTools = {
                "sSwfPath": "app/plugins/jquery-datatable/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [{
                    "sExtends": "csv",
                    "sButtonText": "<i class='pg-grid'></i>"
                }, {
                    "sExtends": "xls",
                    "sButtonText": "<i class='fa fa-file-excel-o'></i>"
                }, {
                    "sExtends": "pdf",
                    "sButtonText": "<i class='fa fa-file-pdf-o'></i>"
                }]
            };

            return dataTableOpts;
        }

        function prepareRequestParams(aoData,dataTableObj) {
            var tableHeaders = dataTableObj.tableHeaders;
            var filters = dataTableObj.filters;
            var filtersConfig = dataTableObj.filtersConfig;

            var queryParam = {};
            var sortOrder = "DESC";
            if (aoData) {
                queryParam.startIndex = aoData[3].value;
                //This value will be -1,when pagination is disabled for datatables.
                if(aoData[4].value == -1){
                    queryParam.count = 10;
                }else{
                    queryParam.count = aoData[4].value;
                }

                var searchFields = '';
                for(var i=0;i<tableHeaders.length;i++){
                    if(tableHeaders[i].search){
                        if(searchFields){
                            searchFields+=',';
                        }
                        searchFields+=tableHeaders[i].DbValue;
                    }
                }
                if(searchFields.length>0){
                    if( aoData[5].value.value ){
                        queryParam.searchFields =  searchFields;
                        queryParam.searchValue =  aoData[5].value.value;
                    }

                }else{
                    if( aoData[5].value.value ){
                        queryParam.search =  aoData[5].value.value;
                    }
                }
                if(aoData[2].value.length > 0 && aoData[2].value[0].dir === 'asc')
                    sortOrder = "ASC";
                if(aoData[2].value.length > 0 && tableHeaders[aoData[2].value[0].column].DbValue && tableHeaders[aoData[2].value[0].column].DbValue.length > 0){
                    queryParam.sortBy = [tableHeaders[aoData[2].value[0].column].DbValue];
                    queryParam.sortDirection = sortOrder;
                }

                for (var filterName in filters){

                    if(filters.hasOwnProperty(filterName)){

                        var filterValue = filters[filterName];
                        filterName = (filtersConfig && filtersConfig[filterName])?(filtersConfig[filterName].dbParam)?filtersConfig[filterName].dbParam:filterName: filterName;

                        var skipQueryParam = (filterValue && filterValue instanceof Object)?filterValue.skipQueryParam:false;
                        if(!skipQueryParam){
                            queryParam[filterName] = (filterValue && filterValue instanceof Object)? filterValue.dbRef : filterValue;
                        }
                    }
                }
            }

            return queryParam;
        }

        function prepareResponseData(response,oSettings,message) {
            if(oSettings)
                oSettings.oLanguage.sEmptyTable = message;

            var datTabData = {};
            datTabData.aaData = ( response.list !== undefined ? response.list : [] );
            datTabData.recordsTotal = response.totalCount;
            datTabData.recordsFiltered = response.totalCount;
            return datTabData;
        }

        function disableTableHeaderOps( len, settings ){

            var sortClass;
            if( len == 0 ){

                sortClass = "sorting-disabled";
                $( 'table.dataTable thead .sorting').css( "cursor", "default");
            }
            else {
                sortClass = "sorting";
                $( 'table.dataTable thead .sorting').css( "cursor", "pointer");
            }

            for( var i =0; i < settings.aoColumns.length; i++ ) {
                var col = settings.aoColumns[i];
                if(col.bSortable || col.sortEnabled){
                    col.bSortable = !(len==0);
                    col.sortEnabled = true;
                    col.sSortingClass = sortClass;
                }
            }
        }

        function hidePaceOnFailure(oSettings,fnCallback,message){
            message = message ? message : 'data';
            oSettings.oLanguage.sEmptyTable = "Unable to load " + message;
            var emptyResponse = {aaData : [],recordsFiltered : 0, recordsTotal : 0};
            fnCallback(emptyResponse);
        }

        function getFilter(filterName, filterType, tableId, urlPattern){

            var getFilterFunc = (getFilterFuncMap)?getFilterFuncMap[filterType] ?getFilterFuncMap[filterType] : 'getFilterFromUrl':'getFilterFromUrl';

            var userId = sessionStorage.getItem('userId');

            var currentState = $state.current.name;

            var path = userId +'_' + currentState;

            return (filterType=='url')?localStateContext[getFilterFunc](path, tableId, filterName,urlPattern):localStateContext[getFilterFunc](path, tableId, filterName);

        }

        function setFilter(filterName, filterValue, filterType, tableId, urlPattern){

            var setFilterFunc = (setFilterFuncMap)?setFilterFuncMap[filterType] ?setFilterFuncMap[filterType] : 'saveFilterInUrl':'saveFilterInUrl';

            var userId = sessionStorage.getItem('userId');

            var currentState = $state.current.name;

            var path = userId+'_' + currentState;


            (filterType=='url')?localStateContext[setFilterFunc](path, tableId, filterName, filterValue,urlPattern):localStateContext[setFilterFunc](path, tableId, filterName, filterValue);

        }

        function removeFilter(filterName, filterType, tableId,urlPattern){

            var removeFilterFunc = (removeFilterFuncMap)?removeFilterFuncMap[filterType] ?removeFilterFuncMap[filterType] : 'removeFilterFromUrl':'removeFilterFromUrl';

            var userId = sessionStorage.getItem('userId');

            var currentState = $state.current.name;

            var path = userId +'_' + currentState;

            return (filterType=='url')?localStateContext[removeFilterFunc](path, tableId, filterName,urlPattern):localStateContext[removeFilterFunc](path, tableId, filterName);

        }

        function configureFilters(dataTableObj, configurations){

            dataTableObj.filtersConfig = configurations?configurations:{};

            dataTableObj.filters = {};

            for (var filterConfigName in configurations){

                if(configurations.hasOwnProperty(filterConfigName) ){

                    var filterType = configurations[filterConfigName].type ;

                    var storedFilter = getFilter(filterConfigName,filterType,dataTableObj.tableID,configurations[filterConfigName].urlUniquePattern);

                    dataTableObj.filters[filterConfigName] =  (storedFilter)?storedFilter : configurations[filterConfigName].defaultValue;

                    setFilter(filterConfigName, dataTableObj.filters[filterConfigName], filterType, dataTableObj.tableID,configurations[filterConfigName].urlUniquePattern);
                }
            }

        }

        function setOLanguage(dataTableObj,emptyTableMessage) {
            dataTableObj.oLanguage = {
                sLengthMenu: "_MENU_ ",
                sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                sEmptyTable: emptyTableMessage,
                sZeroRecords: emptyTableMessage
            };
            return dataTableObj;
        }

    }
})();