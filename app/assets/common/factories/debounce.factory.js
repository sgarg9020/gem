'use strict';

(function(){
	angular
		.module('app')
		.factory('debounce',['$timeout',Debounce]);
	
	/**
		@function debounce()
		@param {object} $timeout - angular timeout service	
		- Factory method for functions to be executed after a delay
	*/ 	
	function Debounce($timeout){
		
	    var factory = {
	    	set: setDebounce
	    };
	    
	    return factory;

	    /**
			@function setDebounce()
			@param {function} func - new function to execute
			@param {int} wait - time to wait before function execution
			@param {boolean|int[0-1]} immediate - truthy value to immediately execute function
		*/
	    function setDebounce(func, wait, immediate){
			var timeout;
			return function() {
				var context = this;
				var args = arguments;
				var later = function() {
					timeout = null;
					if (!immediate) func.apply(context, args);
				};
			
				var callNow = immediate && !timeout;
				$timeout.cancel(timeout);
				timeout = $timeout(later, wait);
				if (callNow) func.apply(context, args);
			};
	    }
	}	
})();