'use strict';

/* Controllers */
(function () {
    angular.module('app')
        .controller('ErrCtrl', ErrCtrl);

    ErrCtrl.$inject = ['$scope', '$rootScope', '$location', '$state'];

    /**
     @constructor Error
     @param {object} $scope - Angular.js $scope
     */
    function ErrCtrl($scope, $rootScope, $location, $state) {

        $scope.retryPageLoad = function () {
            $rootScope.pageLoadingFailed = false;
            $rootScope.servicesDown = false;
            $location.path($location.path());
            $state.reload();
        };



    }
})();