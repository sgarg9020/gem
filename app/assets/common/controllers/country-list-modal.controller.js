'use strict';

(function(){
    angular.module('app')
        .controller('CountryListModalCtrl', CountryListModal);

    CountryListModal.$inject=['$scope','$rootScope', 'modalDetails', 'analytics','$stateParams'];

    /**
     * @param $scope
     * @param $rootScope
     * @param modalDetails
     * @param analytics
     * @constructor
     */
    function CountryListModal($scope,$rootScope, modalDetails, analytics){
        var countryListModal = this;
        /**
         * Start up logic for controller
         */
        function activate(){
            modalDetails.scope(countryListModal, true);
            modalDetails.attributes({
                id: 'countryListModal'
            });

            $scope.$parent.countriesModalObj = $scope.$parent.countriesModalObj || {};
            $scope.$parent.countriesModalObj.showCountries = showCountries;
        }

        activate();

        function prepareCountryList(zone, type) {
            var countries = [];

            if(type === 'zoneCountryList' && zone.zoneCountryList) {
                countries =  zone.zoneCountryList.map(function (zoneCountry) {
                    return zoneCountry.countryDetails;
                });
            }

            countries = (type === 'countryList') ? zone : countries;

            return countries;
        }

        function showCountries(zone, type) {
            countryListModal.countries = prepareCountryList(zone, type);
        }

    }
})();



