'use strict';

/* Controllers */
(function () {
    angular.module('app')
        .controller('UpdateGroupCtrl', UpdateGroup);

    UpdateGroup.$inject = ['$scope', '$sce', 'modalDetails', 'formMessage','commonUtilityService', 'GroupsService'];

    /**
     @constructor UpdateGroup()
     @param {object} $scope - Angular.js $scope
     @param {object} $sce - Angular.js strict contextual escaping service
     @param {object} modalDetails - Factory object
     @param {object} GroupsService - Factory object
     */
    function UpdateGroup($scope, $sce, modalDetails, formMessage, commonUtilityService, GroupsService ) {

        var groupModalObj = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            /* Form elements object */
            groupModalObj.group = {};

            /* modalDetails factory */
            modalDetails.scope(groupModalObj, true);
            modalDetails.attributes({
                id: 'updateGroupsModal',
                formName: 'updateGroups',
                header: 'Add to Group',
                cancel: 'Cancel',
                submit: 'Add to Group'
            });
            $scope.$parent.groupModalObj.vfUpdateModal = updateModal;
            $scope.$parent.groupModalObj.resetAddToGroupModal = removeData;

            GroupsService.getGroups({
                count: 100000,
                startIndex: 0,
                sortBy: "accountName",
                sortDirection: "ASC"
            }).success(function (response) {
                groupModalObj.groups  = ( response.list !== undefined ? response.list : [] );
            }).error(function (data) {
                console.log("error while fetching groups");
                commonUtilityService.showErrorNotification(data.errorStr);
            });

            modalDetails.cancel(removeData);

            groupModalObj.trustAsHtml = function (value) {
                return $sce.trustAsHtml(value);
            };
        }

        function updateModal(rows,groups, submit,attrs) {
            //groupModalObj.groups = groups;
            formMessage.scope({});
            formMessage.message( false, "");

            if (attrs)
                modalDetails.attributes(attrs);
            modalDetails.scope(groupModalObj, false);



            modalDetails.submit(function () {

                if ( typeof groupModalObj.group == "undefined" || Object.getOwnPropertyNames(groupModalObj.group).length == 0 ) {

                    if( groupModalObj.groups.length > 0 ) {
                        $scope.groupModalObj.updateGrpErrMsg = "Please select a valid Group!";
                    }


                    return false;
                }

                else {
                    submit(groupModalObj.group, rows);
                    groupModalObj.group = {};
                    angular.element('#' + $scope.groupModalObj.modal.id).modal('hide');
                    delete $scope.groupModalObj.updateGrpErrMsg;


                }

            });
        }



        function removeData(){
            $scope.groupModalObj.group = {};
            delete $scope.groupModalObj.updateGrpErrMsg;
        }

        groupModalObj.createAndSelectGroup = function(groupName){

            var group = { type:"AccountList", list: [ { "type" : "Account",  "accountType":"GROUP", "accountName": groupName, "parentAccountId":  sessionStorage.getItem( "accountId" ) }] };
            GroupsService.addGroup(group).success(function() {
                commonUtilityService.showSuccessNotification("Group "+group.list[0].accountName+ " has been created.");

                GroupsService.getGroups({
                    count: 100000,
                    startIndex: 0,
                    sortBy: "accountName",
                    sortDirection: "ASC"
                }).success(function (response) {
                     groupModalObj.groups  = ( response.list !== undefined ? response.list : [] );
                     $scope.$parent.groupModalObj.updateGroups(groupModalObj.groups);
                }).error(function (data) {
                    console.log("error while fetching groups");
                    commonUtilityService.showErrorNotification(data.errorStr);
                });



            }).error(function(data){
                console.log('add group failure' + data);
                commonUtilityService.showErrorNotification(data.errorStr);
            });
        };
       activate();

    }
})();
