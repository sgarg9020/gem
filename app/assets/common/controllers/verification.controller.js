'use strict';

/* Controllers */
(function(){
	angular.module('app')
		.controller('VerificationCtrl', VerificationConf);

	VerificationConf.$inject = ['$scope', 'modalDetails'];

	/**
	 @constructor VerificationConf()
	 @param {object} $scope - Angular.js $scope
	 @param {object} modalDetails - Factory object
	 */
	function VerificationConf($scope, modalDetails){

		var verificationModal = this;

		/**
		 * Start up logic for controller
		 */
		function activate(){
			/* modalDetails factory */
			modalDetails.scope(verificationModal, true);
			modalDetails.attributes({
				id: 'verificationModal',
				header: 'Confirm',
				cancel: 'Cancel',
				submit: 'Submit'
			});

			$scope.$parent.verificationModalObj = $scope.$parent.verificationModalObj || {};
			$scope.$parent.verificationModalObj.addVerificationModelMessage = addModalMessage;
			$scope.$parent.verificationModalObj.updateVerificationModel = updateModal;
		}

		/**
		 @function addModalMessage()
		 @param {string} msg - message to use for formMessage factory
		 - Expose access to change the message for directive gs-verification-modal
		 */
		function addModalMessage(msg){
			verificationModal.addModalMessage(msg);
		}


		/**
		 @function updateModal()
		 @param {object} obj - object to change modal attributes in directive
		 @param {function} submit - function to execute on submit
		 @param {function} cancel - function to execute on cancel
		 */
		function updateModal(obj, submit, cancel){
			modalDetails.scope(verificationModal, false);
			modalDetails.attributes(obj);
			//modalDetails.submit(function(){submit();});

			modalDetails.submit(submit);
			if(cancel){
				modalDetails.cancel(cancel);
			}

		}

		activate();

	}
})();
