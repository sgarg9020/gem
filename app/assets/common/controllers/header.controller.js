'use strict';

/* Controllers */
(function(){
    angular.module('app')
        .controller('HeaderCtrl', Header);

    Header.$inject = ['$scope','$rootScope','$location', 'AccountService','SettingsService','analytics','$sce'];

    function Header($scope,$rootScope, $location, AccountService,SettingsService,analytics,$sce){

        var updateAccountInfoListener;

        /**
         * Start up logic for controller
         */
        function activate(){
            var enterpriseId = sessionStorage.getItem("accountId");
            if($("#account-selection-panel").hasClass('open'))
                $scope.toggleAccountTypesPanel();
            if($scope.accountId != enterpriseId){
                $scope.isRBACEnabled = RBAC_ENABLED;
                $scope.changeAccountType = changeAccountType;
                var accountDetails;
                $scope.accountId = enterpriseId;
                var queryParam = {};
                queryParam.details = 'firstName|lastName|emailId';



                /* Get the account details */
                AccountService.getAccountDetails(queryParam,true).success(function(response){
                    if(response != null){
                        accountDetails = response;
                        if(accountDetails.firstName || accountDetails.lastName){
                            $scope.userNameOrEmail = accountDetails.firstName + " " + accountDetails.lastName;
                        }else{
                            $scope.userNameOrEmail = accountDetails.emailId;
                        }

                    }
                }).error(function(data,status){
                    console.log("error while fetching account details");
                });

                $scope.getAccountDetails();
            }
        }

        $scope.getAccountDetails = function () {
            var enterpriseId = sessionStorage.getItem("accountId");
            var enterpriseQueryParam = {};
            var enterpriseCurrency =  sessionStorage.getItem( "gs-enterpriseCurrency");
            /* get enterprise details */
            SettingsService.getEnterpriseDetails(enterpriseId,enterpriseQueryParam,true).success(function(response){

                if (response != null) {
                    analytics.setDimension(2,response.accountName);
                    $scope.accountName = response.accountName;
                    sessionStorage.setItem("accountName", response.accountName);
                    sessionStorage.setItem("defaultCreditLimitForSims", ( response.alertSetting.defaultCreditLimitForSims ? response.alertSetting.defaultCreditLimitForSims[0].limitKB : 10240 ));
                    var subscriptionType  = response.accountSubscriptionType ? response.accountSubscriptionType : ENTERPRISE_DEFAULT_SUBSCRIPTION_TYPE;

                    subscriptionType = (subscriptionType == 'ENTERPRISE_PRO+'?'ENTERPRISE_PRO':subscriptionType);
                    /*if($rootScope.isGSAdmin || $rootScope.isSupport){
                        subscriptionType =  ENTERPRISE_DEFAULT_SUBSCRIPTION_TYPE;
                    }else{
                        subscriptionType  = response.accountSubscriptionType ? response.accountSubscriptionType : ENTERPRISE_DEFAULT_SUBSCRIPTION_TYPE;
                    }*/
                    if(subscriptionType == RESELLER_SUBSCRIPTION_TYPE){
                        $scope.accountSubscriptionType = "reseller MA"
                    } else{
                        subscriptionType = (subscriptionType.indexOf("_") != -1 ) ? (subscriptionType.split("_")[1]).toLowerCase() : subscriptionType.toLowerCase();
                        $scope.accountSubscriptionType = subscriptionType;
                    }
                    enterpriseCurrency = response.company.currency;
                    sessionStorage.setItem("gs-enterpriseCurrency", enterpriseCurrency);
                    sessionStorage.setItem('timezone', response.company.timeZone);


                }
            }).error(function(data,status){
                console.log("error while fetching enterprise details");
            });
        }

        updateAccountInfoListener = $rootScope.$on('onUpdateAccountInfo', function(event,data) {
            if(data.firstName || data.lastName){
                $scope.userNameOrEmail = data.firstName + " " + data.lastName;
            }

            if( data.accountSubscriptionType ) $scope.accountSubscriptionType = data.accountSubscriptionType.split( " ")[1].toLowerCase();

            $scope.accountName = (data.accountName) ? data.accountName : $scope.accountName;
            sessionStorage.setItem("accountName", response.accountName);
        });

        $scope.showView = function(path,q){
            var prevPath = $location.path();

            if(prevPath != path){
                $location.path(path);
                $location.search({});
            }
        }

        $scope.updateAlertHistory = function(){
            $rootScope.$emit('onUpdateAlertHistory',{});
            $("#quickview").addClass( "open");
        }

        $scope.toggleAccountTypesPanel = function(){
            var $panel = $("#account-selection-panel");
            $panel.hasClass('open') ? $panel.removeClass( "open") : $panel.addClass( "open");
            var $trigger = $('#account-selector');
            $trigger.css('visibility') == 'hidden' ? $trigger.css('visibility','visible') : $trigger.css('visibility','hidden');
            $scope.accountPanelClose = !$scope.accountPanelClose;
        }

        /**
         * @func prepareNavigationToSubAccount
         * @desc setting properties to navigate to subaccount from reseller account
         * @param account - account info
         */
        $scope.onSubAccountSelection = function (account) {
            sessionStorage.setItem("accountId", account.accountId);
            var enterpriseCurrency = account.company ? account.company.currency : '';
            sessionStorage.setItem( "gs-enterpriseCurrency",enterpriseCurrency);
            if (sessionStorage.getItem('isSupport') === 'true' && sessionStorage.getItem('isNavigatedThroughAdmin') === 'true') {
                sessionStorage.setItem("exit-to-state","support.accounts.companies");
            }
            $rootScope.$emit('onChangeAccount',{accountId: account.accountId, updateDetail: $scope.getAccountDetails});
        };

        /**
         @func changeAccountType()
         @desc - opens a model with sub account list to select from when Reseller user clicks on drop down to select another account
         */
        function changeAccountType(selectedAccountType){
            if(selectedAccountType === 'SUB ACCOUNT') {
                var obj = {
                    header: "Select Sub Account",
                    submit: "Select",
                    cancel: "Cancel"
                };
                $scope.subAccountsModalObj.subAccountsModalAttributes(obj, $scope.onSubAccountSelection);
                $rootScope.$emit("$onOpenAccountsModal", {});
                $('#subAccountsModal').modal('show');
            } else {
                $scope.onSubAccountSelection({accountId: sessionStorage.getItem('rootResellerAccountId')});
            }
            $scope.toggleAccountTypesPanel();
        }

        $scope.$on('$destroy', function(){
            //unregistering listener
            if(updateAccountInfoListener)
                updateAccountInfoListener();
        });

        activate();
    }

})();
