'use strict';

/* Controllers */
(function(){
	angular.module('app')
	    .controller('SubAccountsCtrl', SubAccounts);

	SubAccounts.$inject = ['$scope', '$rootScope', '$sce', 'modalDetails','notifications', 'dataTableConfigService', 'commonUtilityService', 'AuthenticationService', '$compile'];

	function SubAccounts($scope, $rootScope, $sce, modalDetails, notifications, dataTableConfigService, commonUtilityService, authenticationService, $compile){

		var subAccountsModalObj = this;

		/**
		 * Start up logic for controller
		 */
		function activate(){

			/* modalDetails factory */
			modalDetails.scope(subAccountsModalObj, true);
			modalDetails.attributes({
				id: 'subAccountsModal',
				formName: 'subAccounts',
				header: 'Sub Accounts',
				cancel: 'Cancel',
				submit: 'Select Account'
			});
			modalDetails.cancel(function(){});
            modalDetails.submit();

            $scope.$parent.subAccountsModalObj = $scope.$parent.subAccountsModalObj || {};
			$scope.$parent.subAccountsModalObj.subAccountsModalAttributes = updateModalAttributes;

            $scope.dataTableObj = {};
            $scope.dataTableObj.tableID = "subAccountsDataTable";
            clearDataTableStatus(['DataTables_subAccountsDataTable_']);
            $scope.emptyTableMessage = "No accounts available.";
            subAccountsModalObj.trustAsHtml = function (value){return $sce.trustAsHtml(value);};

            $scope.enterpriseModalObj =  {};

            commonUtilityService.getCountries().success(function(data){
                subAccountsModalObj.countries = data.countryList;
            }).error(function(data){
                commonUtilityService.showErrorNotification(data.errorStr);
            });

            $scope.statusMap = { ACTIVE:'active', INACTIVE:'inactive', SUSPENDED:'suspended'};

            // DataTable
            $scope.dataTableObj = {};
            $scope.dataTableObj.tableHeaders = [
                {"value": "", "DbValue":""},
                {value: "Name", DbValue: "accountName"},
                {value: "Status", DbValue: "status"},
                {value: "Country", DbValue: "country"},
                {value: "Physical SIMs", DbValue: "totalPSimCount"},
                {value: "eSIMs", DbValue: "totalConsumerESimCount"},
                {value: "M2M eSIMs", DbValue: "totalIotM2MSimCount"}
            ];
            $scope.dataTableObj.tableID = "subAccountsDataTable";

            var filtersConfig = { selectedDisplayLength: {type:'local', dbParam: 'count', defaultValue:10}, status: {type:'local', defaultValue: 'ALL' } };
            dataTableConfigService.configureFilters($scope.dataTableObj, filtersConfig);

            $scope.dataTableObj.dataTableOptions = _setDataTableOptions();
            $scope.dataTableObj.dataTableOptions = dataTableConfigService.prepareOptions($scope.dataTableObj.dataTableOptions);

		}

        /**
         * @function _setDataTableOptions()
         * @desc - function to set data table. This keeps the activate() function clean.
         */
        function _setDataTableOptions(){
            return {
                order: [[ 1, "asc" ]],

                createdRow: function ( row, data, index ) {
                    $(row).attr("id",'AID_'+data.accountId);
                    $compile(row)($scope);
                },
                columns: [
                    {
                        width:"40px",
                        orderable:false
                    },
                    { orderable:true},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false},
                    { orderable:false}
                ],
                columnDefs: [{
                    "targets": 0,
                    className: "radio-table-cell",
                    "render": function ( data, type, row, meta) {
                        return '<div class="radio radio-success"><input type="radio" name="table-radio-row" value="'+row.accountId+'" id="radio_'+row.accountId+'" gs-index="'+meta.row+'"><label ng-click="radioClicked($event)" id="label_'+row.accountId+'"></label></div>';
                    }
                }, {
                    targets: 1,
                    render: function (data, type, row) {
                        return '<span>'+row.accountName+'</span>';
                    }
                }, {
                    targets: 2,
                    render: function (data, type, row) {
                        var entStatus = ($scope.statusMap[row.status])?$scope.statusMap[row.status]:((row.status)?row.status:'Not Available');
                        return '<span class="enterprise-status '+entStatus+'">'+entStatus+'</span>';
                    }
                },{
                    targets: 3,
                    render: function (data, type, row) {
                        var country = (row.company && row.company.address && row.company.address.country) ? commonUtilityService.getCountryObject(subAccountsModalObj.countries,row.company.address.country).name : 'Not Available';
                        return '<span>'+country+'</span>';
                    }
                }, {
                    targets: 4,
                    render:function( data, type, row) {
                        var count = row.simCount ? row.simCount.totalPSimCount : '0';
                        return '<span>'+count+'</span>';
                    }
                },{
                    targets: 5,
                    render:function( data, type, row) {
                        var count = row.simCount ? row.simCount.totalConsumerESimCount : '0';
                        return '<span>'+count+'</span>';
                    }
                },{
                    targets: 6,
                    render:function( data, type, row) {
                        var count = row.simCount ? row.simCount.totalIotM2MSimCount : '0';
                        return '<span>'+count+'</span>';
                    }
                }
                ],
                oLanguage: {
                    sLengthMenu: "_MENU_ ",
                    sInfo: "Showing <b>_START_ to _END_</b> of _TOTAL_ entries",
                    sEmptyTable: $scope.emptyTableMessage,
                    sZeroRecords:$scope.emptyTableMessage
                },
                processing: true,
                fnServerData: function (sSource, aoData, fnCallback) {
                    if(aoData){
                        subAccountsModalObj.searchValue = (aoData[5])?aoData[5].value.value:'';
                    }

                    var queryParam = dataTableConfigService.prepareRequestParams(aoData,$scope.dataTableObj);
                    queryParam.ignoreGroups = true;
                    $scope.isPageSizeFilterDisabled = false;

                    var accountId = sessionStorage.getItem("rootResellerAccountId") || sessionStorage.getItem("accountId");
                        authenticationService.getSubAccounts(accountId,queryParam).success(function (response) {
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        var datTabData = dataTableConfigService.prepareResponseData(response,oSettings,$scope.emptyTableMessage);
                        $scope.companiesList = datTabData.aaData;
                        $scope.isPageSizeFilterDisabled = ( $scope.companiesList.length == 0);
                        if(oSettings != null) {
                            fnCallback(datTabData);
                        }else{
                            loadingState.hide();
                        }

                    }).error(function (data) {
                        $scope.isPageSizeFilterDisabled = ( !$scope.companiesList ||  $scope.companiesList.length == 0);
                        var oSettings = $scope.dataTableObj.dataTableRef.fnSettings();
                        dataTableConfigService.hidePaceOnFailure(oSettings,fnCallback,"companies");
                        commonUtilityService.showErrorNotification(data.errorStr);
                    });
                },
                "fnDrawCallback": function( oSettings ) {
                    if($scope.companiesList )
                        dataTableConfigService.disableTableHeaderOps( $scope.companiesList.length, oSettings );

                }
            };
        }

		/**
			@func updateModalAttributes()
			@param {object} obj - the object to pass to modalDetails factory
			@desc - Expose access to change the attributes of the modal for directive gs-verification-modal
		*/
		function updateModalAttributes(obj, onSubAccountSelection){
			modalDetails.scope(subAccountsModalObj, false);
			modalDetails.attributes(obj);

            modalDetails.submit(function () {
                var table = $('#'+$scope.dataTableObj.tableID).DataTable();
                var row = table.$('tr.row-selected');
                $scope.dataTableObj.selected = row;
                onSubAccountSelection(table.rows(row).data()[0]);
            });
		}

        $scope.radioClicked = function(event){
            $scope.dataTableObj.selectRow(event);
        };

		activate();

        $rootScope.$on("$onOpenAccountsModal", function(){
            $scope.dataTableObj.refreshDataTable(false);
        });

	}
})();
