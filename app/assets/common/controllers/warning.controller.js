'use strict';

/* Controllers */
(function(){
    angular.module('app')
        .controller('WarningCtrl', Warning);

    Warning.$inject=['$scope', '$sce', 'modalDetails', 'notifications'];

    /**
     @constructor Warning()
     @param {object} $scope - Angular.js $scope
     @param {object} $sce - Angular.js strict contextual escaping service
     @param {object} modalDetails - Factory object
     */
    function Warning($scope, $sce, modalDetails){

        var warning = this;

        /**
         * Start up logic for controller
         */
        function activate(){
            /* Form elements object */
            warning.warnings = {};
            $scope.generalObj = {};
            warning.activeIndex = 0;

            /* ModalDetails factory */
            modalDetails.scope(warning, true);
            modalDetails.attributes({
                id: 'warningModal',
                formName: 'showWarning',
                cancel: 'Cancel',
                submit: 'Continue',
                header: 'Warnings'
            });
            modalDetails.cancel(remove);

            warning.trustAsHtml = function(value) {return $sce.trustAsHtml(value);};
            $scope.$parent.warningModalObj = $scope.$parent.warningModalObj || {};
            $scope.$parent.warningModalObj.updateWarningModal = updateWarningModal;
            $scope.$parent.warningModalObj.resetWarningModal = removeData;
        }

        function hideModal() {
            angular.element('#' + $scope.warning.modal.id).modal('hide');
        }

        function updateWarningModal(messages,onContinue,onCancel, disableButtons){
            warning.warnings = messages.warnings;
            warning.errors = messages.errors;
            if(warning.errors.length > 0){
                warning.message = "";
                warning.modal.header = 'Errors';
            }else{
                warning.message = "Warning! Do you want to continue?";
                if(warning.errors.length > 0 && warning.warnings.length > 0){
                    warning.modal.header += 'and Warnings';
                }else if(warning.warnings.length > 0){
                    warning.modal.header = 'Warnings';
                }
            }

            if(disableButtons){
                warning.message = '';
                warning.disableButtons = true;
            }else{
                warning.disableButtons = false;
            }
            warning.onContinue = function(){
                onContinue();
                $("#warningModal").modal('hide');
            };
            if(onCancel){
                warning.onCancel = onCancel;
            }
            if(!$scope.$$phase)
                $scope.$apply();
        }
        function removeData(){
            warning.warnings ={};
        }


        /**
         @function remove()
         - call a directive level function
         */
        function remove(){
            if(warning.onCancel)
                warning.onCancel();
        }

        activate();

    }
})();
