'use strict';

/* Directive */
(function() {
    angular.module('app')
        .directive('gsScrollLink', ScrollLink);

    ScrollLink.$inject = ['$timeout'];

        /**
     @function ScrollLink()
     @desc directive function definition
     */
    function ScrollLink() {

        var winScroll;

        return {
            restrict: 'E',
            link: link
        }

        /**
         @function link()
         @param {object} $scope - directive scope
         @param {object} element - directive HTML element
         @param {object} attrs - attributes on the directive HTML element
         */
        function link($scope, element, attrs) {
            element.on("click", function(){scrollToDiv(attrs.linkId, attrs.offset);});
            if(!winScroll)
                winScroll = $(window).scroll(evaluateScrollPosition);
        }

        /**
            @function scrollToDiv()
            @param {string} id - node ID attribute value to scroll to
            @param {number} offset - node ID attribute value to offset the scroll top position
         */
        function scrollToDiv(id, offset){
            var $header = $(".header");
            var headerHeight = 0;

            if($header.css("position") == "fixed")
                headerHeight = parseInt(($header.css("height").split("px"))[0]);

            $('html, body').animate({
                scrollTop: parseInt($("#"+id).offset().top) - headerHeight - offset + 'px'
            }, 1000);
        }

        /**
         * @function evaluateScrollPosition()
         * @desc evaluate the current scroll top position of the window. Apply or remove a class
         * to fix the scroll-link-wrapper
         */

        function evaluateScrollPosition(){
            if($(".header").outerHeight()) {
                var $body = $(".body");
                var $container = $(".scroll-links-container");
                var $wrapper = $(".scroll-links-wrapper");

                // Take into account breadcrumbs
                var breadcrumbs = 0;
                var $bc = $(".breadcrumb-wrapper");

                if ($bc.outerHeight())
                    breadcrumbs = $bc.outerHeight();

                if ($body.offset().top >= $(".header").outerHeight() + breadcrumbs) {
                    $container.css("height", $wrapper.css("height"));
                    $wrapper.css("top", $(".header").outerHeight());
                    $wrapper.addClass("scroll-links-fixed");
                }
                else {
                    $container.css("height", "");
                    $wrapper.css("top", "");
                    $wrapper.removeClass("scroll-links-fixed");
                }
            }
        }

    }

})();