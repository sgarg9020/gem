'use strict';

/* Directive */
(function () {
    angular.module('app')
        .directive('gsCollapse', ['$timeout', simpleCollapse]);

    /**
     @function simpleCollapse()
     @param {object} $timeout - Angular JS $timeout service
     */
    function simpleCollapse($timeout) {

        return {
            restrict: 'E',
            link: link,
            transclude: true,
            replace: true,
            template: '<div class="simple-collapse-wrapper"><div class="simple-collapse-content" ng-transclude></div></div>'
        }

        /**
         @function link()
         @param {object} $scope - directive scope
         @param {object} element - directive HTML element
         @param {object} attrs - attributes on the directive HTML element
         */
        function link($scope, element, attrs) {
            $scope.$watch(attrs.collapse, function (isExpanded) {
                if (isExpanded)
                    $scope.expOrCol = 'COLLAPSE';
                else
                    $scope.expOrCol = 'EXPAND';
                $timeout(function () {
                    element.css('height', isExpanded ? element.children()[0].offsetHeight + 'px' : '0');
                }, 10);
            });
        }

    }

})();