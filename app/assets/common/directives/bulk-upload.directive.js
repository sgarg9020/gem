'use strict';

/* Directive */
(function(){
	angular.module('app')
		.directive('gsBulkUpload', SetupDropzone);
		
	/**
		@function SetupDropzone()
		 - gs-bulk-upload directive to initalizate dropzone.js plugin
	*/	
	function SetupDropzone() {
		
		/*
			Dropzone.js attaches a window variable. We want to disable 
			auto initialization if a class had "dropzone".
		*/
		Dropzone.autoDiscover = false;
		
		var link = {
			link: link
		};
		
		return link;
		
		/**
			@function link()
			@param {object} $scope - directive $scope
			@param {object} element - directive HTML element
			@param {object} attrs - attributes on the directive HTML element
		*/
		function link($scope, element, attrs) {
	        
	       	$scope.vm.dropzone = new Dropzone(element[0], {
				url: 'path/to/api',
				maxFiles: 1,
				parallelUploads: 1,
				maxFilesize: 25,
				addRemoveLinks: true,
				clickable: true,
				acceptedFiles: "*.csv,",
		        autoProcessQueue: false,
		    });
		    
		    /* Need to include dropzone events to handle file processing */
	        $scope.vm.processDropzone = function() {
	            console.log('Processed...');
	        };
	
	        $scope.vm.resetDropzone = function() {
				console.log('Reset...');   
	        }
		}
	}
})();