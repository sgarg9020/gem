(function() {
    "use strict";

    angular.module('app').directive('gsEditTool',editTool);

    editTool.$inject = [];

    function editTool(){

        return {
            restrict:'AE',
            scope:{ id:'=', save:'&onSave', targetField:'=', error:'&onError', success:'&onSuccess', name:'@', alloc:'=' },
            replace:true,
            template:"<div class='editable-wrapper'><div class='editable-input' id='nickname_{{id}}'><div class='input-ico'  ng-click='onEditClick($event);'><i class='fa fa-pencil edit-ico'></i></div><input type='text' id='input_{{id}}' value=''  ng-focus='addEditableState($event);' ng-blur='onEditBlur($event);' required /></div><div class='btn-group updateInputValues' id='gs_edit_{{ id }}' style='display:none;'><button class='btn btn-default save' ng-mousedown='textEdit( $event );'><i class='fa fa-check'></i></button><button class='btn btn-default delete'><i class='fa fa-times' ng-click='hideEditTool( $event );'></i></button></div></div>",
            link: link
        };

        function link(scope, element, attrs) {

            var inputElement;
            var inputElementDiv;
            var updateBtnsDivElement;

            scope.updated = false;
            scope.prevEle = null;

            var textBox = element[0].childNodes[0].childNodes[1];
            if (scope.targetField == "allocation") {
                textBox.setAttribute("maxlength", "7");
                textBox.setAttribute("pattern", "[0-9]{1,7}");
                textBox.setAttribute("type", "number");
                scope.oldText = scope.alloc;
                scope.name = scope.oldText;
                textBox.setAttribute("value", scope.alloc);
            }
            else if (scope.targetField == "name") {
                scope.oldText = scope.name;
                textBox.setAttribute("value", scope.oldText);
            }

            function attachKeyPressEvents(element) {
                var textBox = element[0];
                if (scope.targetField == "allocation") {

                    if (typeof textBox.addEventListener == "function") textBox.addEventListener("keypress", isNumberPressed, false);
                    else textBox.attachEvent("keypress", isNumberPressed);

                    textBox.oninput = function (event) {
                        if (this.value.length > 7) {
                            this.value = this.value.substr(0, 7);
                        }
                    };
                }
                else if (scope.targetField == "name") {

                    if (typeof textBox.addEventListener == "function") textBox.addEventListener("keypress", isValid, false);
                    else textBox.attachEvent("keypress", isValid);

                    //to check the pattern in android mobile, as keypress event will not be triggered in android
                    textBox.oninput = function (evt) {
                        var value = this.value + String.fromCharCode(evt.keyCode);
                        //pattern has unicode for >|<|"|“|”
                        var pattern = /\u0022|\u201C|\u201D|\u003C|\u003E/;
                        if (pattern.test(value)) {
                            var position = evt.target.selectionStart;
                            this.value = this.value.replace(/\u0022|\u201C|\u201D|\u003C|\u003E/g, '');
                            evt.target.selectionEnd = position - 1;
                        }
                    };
                }
            }

            function isValid(evt) {
                var value = this.value + String.fromCharCode(evt.keyCode);
                //pattern has unicode for >|<|"|“|”
                var pattern = /\u0022|\u201C|\u201D|\u003C|\u003E/;
                if(pattern.test(value) || value.length>50){
                    evt.preventDefault();
                    return false;
                }
                return true;
            }


            function isNumberPressed(evt) {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 46 || ( charCode > 31 && charCode < 48 ) || charCode > 57) {
                    evt.preventDefault();
                    return false;
                }
                return true;
            }


            /**
             * It returns input element inside the main div and finds the element only when it is not available
             * @returns {*}
             */
            function getInputElememt() {
                if (!inputElement) {
                    inputElement = element.find("#input_" + scope.id);
                }

                return inputElement;
            }

            /**
             * It returns parent div of the input elemnt,which has class 'editable-input'
             */
            function getInputElementDiv() {
                if (!inputElementDiv) {
                    inputElementDiv = element.find('.editable-input');
                }

                return inputElementDiv;
            }

            /**
             * It returns the div element,which has 'update' and 'cancel' buttons
             * @returns {*}
             */
            function getUpdateBtnsDivElement() {
                if (!updateBtnsDivElement) {
                    updateBtnsDivElement = element.find("#gs_edit_" + scope.id);
                }

                return updateBtnsDivElement;
            }

            scope.onEditClick = function (e) {
                scope.currInput = "";

                var inputElem = getInputElememt();
                inputElem.focus();

            };


            scope.addEditableState = function (e) {
                scope.currInput = "";

                var inputElemDiv = getInputElementDiv();
                if (!inputElemDiv.hasClass('focused')) {
                    inputElemDiv.addClass('focused');
                }
                getUpdateBtnsDivElement().show();
                var inputElem = getInputElememt();
                attachKeyPressEvents(inputElem);
            };

            function detachEvents(element) {
                if (scope.targetField == "allocation") {
                    if (element.removeEventListener) {                   // For all major browsers, except IE 8 and earlier
                        element.removeEventListener("keypress", isNumberPressed);
                    } else if (element.detachEvent) {                    // For IE 8 and earlier versions
                        element.detachEvent("keypress", isNumberPressed);
                    }
                } else if (scope.targetField == "name") {
                    if (element.removeEventListener) {
                        element.removeEventListener("keypress", isValid);
                    } else if (element.detachEvent) {
                        element.detachEvent("keypress", isValid);
                    }
                }
            }

            scope.hideEditTool = function () {
                scope.currInput = "";
                var inputElem = getInputElememt();
                inputElem.val(scope.oldText);

                getUpdateBtnsDivElement().hide();

                var element = inputElem[0];
                detachEvents(element);
            };

            scope.onEditBlur = function (e) {
                var inputElem = getInputElememt();
                scope.prevEle = inputElem;

                var inputElemDiv = getInputElementDiv();
                if (inputElemDiv.hasClass('focused')) {
                    inputElemDiv.removeClass('focused');
                }

                if (element.hasClass('editing')) {
                    if (inputElem.val() != scope.oldText) {
                        scope.currInput = inputElem.val();
                        inputElem.val(scope.oldText);
                    }

                    element.removeClass('editing');
                    if (element.parent().hasClass('editable-with-label')) {
                        element.parent().removeClass('editing');
                    }
                }
                scope.hideEditTool();
            };

            function updateNickName(newText, currInputBox, oldText, simId) {
                if (newText == "") {
                    scope.error()("Please enter a valid name!");
                    currInputBox.val(oldText);
                    angular.element("#updTxt-" + scope.id).remove();
                }
                else if (oldText != newText) {
                    scope.oldText = oldText;
                    currInputBox.val(newText);

                    scope.save()(simId, newText).then(function (response) {
                        scope.success()("SIM Nickname updated successfully!");
                        scope.updated = true;
                        scope.oldText = newText;
                        currInputBox.val(newText);

                        angular.element("#updTxt-" + scope.id).remove();
                    }, function (response) {
                        scope.error()(response.data.errorStr);
                        angular.element("#updTxt-" + scope.id).remove();
                        scope.updated = false;
                    });

                }
                else {
                    currInputBox.val(newText);
                    angular.element("#updTxt-" + scope.id).remove();
                }
            }

            function updateAllocation(newText, currInputBox, oldText, simId) {
                var empty = newText === "";
                newText = Number(newText);
                if (newText < 0 || empty) {
                    //|| ( !Number.isInteger( newText ) ) ) {
                    scope.error()("Please enter an valid number!");
                    currInputBox.val(oldText);
                    angular.element("#updTxt-" + scope.id).remove();
                }
                else if (oldText != newText) {
                    scope.save()(simId, newText).then(function (response) {
                        scope.success()("SIM Allocation updated successfully!");
                        scope.updated = true;
                        currInputBox.val(newText);
                        scope.oldText = newText;
                        angular.element("#updTxt-" + scope.id).remove();
                    }, function (response) {

                        scope.error()(response.data.errorStr);
                        angular.element("#updTxt-" + scope.id).remove();
                        scope.updated = false;
                    });
                }
                else {
                    currInputBox.val(newText);
                    angular.element("#updTxt-" + scope.id).remove();
                }
                return newText;
            }

            /**
             *
             * @param e
             * @param btn
             */
            scope.textEdit = function (e, btn) {
                var currInputBox = getInputElememt();

                var newText = scope.currInput || currInputBox.val();
                newText = newText.trim();
                // var oldText = ( scope.updated == true ? scope.oldText : decodeURIComponent( scope.name));

                var oldText = scope.oldText;

                /* Add Spinner */
                var div = document.createElement('div');
                var divId = "updTxt-" + scope.id;

                div.className = "updating-content";
                div.id = divId;

                var spinner = document.createElement('i');
                spinner.className = "fa fa-spinner fa-pulse";

                div.appendChild(spinner);

                angular.element("#input_" + scope.id).after(div);
                getUpdateBtnsDivElement().hide();

                var simId = scope.id;

                switch (scope.targetField) {

                    case "name":
                        updateNickName(newText, currInputBox, oldText, simId);
                        break;
                    case "allocation":
                        updateAllocation(newText, currInputBox, oldText, simId);
                        break;
                }
            };
        }

    };
})();

