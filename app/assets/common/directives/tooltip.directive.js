(function(){
    "use strict";

    angular.module('app')
        .directive('gsTooltip', tooltip);

    tooltip.$inject = [];

    /**
     @function toolTip()
     */
    function tooltip() {

        return {
            restrict: 'A',
            link: link
        };

        /**
         @function link()
         @param {object} $scope - directive $scope
         @param {object} element - directive HTML element
         @param {object} attrs - attributes on the directive HTML element
         */
        function link($scope, element, attrs) {

            var is_touch_device = 'ontouchstart' in document.documentElement;

            // Plugin Init
            element.tooltip({
                animation: true,
                container: 'body',
                trigger: (is_touch_device ? 'manual' : 'hover'),
                template: '<div class="tooltip gs-tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
            });

            // Manage the trigger for touch enabled devices
            if(is_touch_device){
                element.on("touchstart", _touchStart);
                element.on("touchend", _touchEnd);
            }

            var timer, lockTimer;
            var touchDuration = 500; // Length of time to touch before something is done.

            /**
             * @function _touchStart()
             * @param e - touch event object
             * @private
             * @desc - _touchStart will store a timeout to be executed
             * after touchDuration has passed.
             */
            function _touchStart(e) {

                if(lockTimer){ // A timeout already exists
                    return;
                }

                var data = this;

                timer = setTimeout(function(){
                    _invokeTooltip(data,e);
                }, touchDuration);
                lockTimer = true;
            }

            /**
             * @function _touchEnd()
             * @private
             * @desc - _touchStart will clear the stored timeout when
             * a touch event has ended before the touchDuration value
             */
            function _touchEnd() {
                if (timer){ // Stops short touches from firing the event
                    clearTimeout(timer);
                    lockTimer = false;
                }
            }

            /**
             * @function _invokeTooltip()
             * @param data - context from touch event
             * @param event - the touch event object
             * @private
             * @desc - _invokeTooltip will show the tooltip for the element
             * that the touch event was triggered on. It will also remove
             * the tooltip after 5 seconds
             */
            function _invokeTooltip(data,event) {
                $(data).tooltip('show');

                // Remove tooltip after 5 seconds
                setTimeout(function(){
                    $(data).tooltip('hide');
                }, 5000);
            }

        }
    }
})();