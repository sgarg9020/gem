'use strict';

/* Directive */
(function(){
	angular.module('app')
		.directive('gsExportModal', gsExportModal);

    gsExportModal.$inject=["$window","commonUtilityService", "analytics"];

	/**
		@function gsExportModal()
	*/
	function gsExportModal($window,commonUtilityService, analytics) {

		return {
			link: link
		};

		/**
		 @function link()
		 @param {object} $scope - directive scope
		 @param {object} element - directive HTML element
		 @param {object} attrs - attributes on the directive HTML element

		 */
		function link($scope, element, attrs) {
			// Changing DOM manipulation to angular
			$scope.exportOption = 1;
            $scope.exportPageStart=null;
            $scope.exportPageEnd=null;
            $scope.errorStr=false;

            //make this scope accessible in parent scope in-order to access the above variables
            $scope.$parent.exportModal.childAccess=$scope;

            $scope.handleExportRange=handleExportRange;
            $scope.handleExportRowRange=handleExportRowRange;
            $scope.handleClickForValidation=handleClickForValidation;
            $scope.handleRadioState = handleRadioState;

            /**
             * @function handleClickForValidation
             * @param {object} e
             * @desc validates the page range start and end values and permits the download based on the outcome.
             */
            function handleClickForValidation(e){
                var val=$scope.exportOption;
                if(val == 2){
                    var maxNoOfPagesToExport = 5000/$scope.$parent.exportModal.actualExpData.count;
                    if($scope.exportPageStart == null || $scope.exportPageEnd == null || $scope.exportPageStart <= 0 || $scope.exportPageEnd <= 0 || $scope.exportPageStart > $scope.exportPageEnd || ($scope.exportPageEnd - $scope.exportPageStart)+1 > maxNoOfPagesToExport){
                        $scope.errorStr = 'Invalid pages';
                        e.preventDefault();
                        return false;
                    }else{
                        handleExportRowRange();
                    }
                }else{
                    handleExportRange();
                }
            }

			/**
			 * @function handleExportRange
			 * @desc appends necessary queryparams based on the radio button
			 */
			function handleExportRange(){
				var val=$scope.exportOption;
                var expDataObj=$scope.$parent.exportModal.exportData;
                var tableId = ($scope.$parent.dataTableObj==undefined) ? null : $scope.$parent.dataTableObj.tableID;
                var actionLabel = tableId + " export";
				if(val == 1){
                    actionLabel = actionLabel + " all data";
                    expDataObj.startIndex=0;
                    expDataObj.count=5000;
				}
                else if(val == 3){
                    actionLabel = actionLabel + " visible data";
                    expDataObj.startIndex=$scope.$parent.exportModal.actualExpData.startIndex;
                    expDataObj.count=$scope.$parent.exportModal.actualExpData.count;
                }
                $scope.exportPageStart=null;
                $scope.exportPageEnd=null;
                $scope.$parent.exportModal.url=expDataObj.url + joinURLParams(expDataObj);
                $scope.$parent.$parent.exportModal(false);
                commonUtilityService.showSuccessNotification("Export will start in few seconds.");


                analytics.sendEvent({
                    category: "Operations",
                    action: actionLabel,
                    label: "Management"
                });
			}

            /**
             * @function handleExportRowRange
             * @desc checks if input is valid and appends the necessary queryParams.
             */
            function handleExportRowRange(){
                var expDataObj=$scope.$parent.exportModal.exportData;
                var noOfRows = $scope.$parent.exportModal.actualExpData.count;

                expDataObj.startIndex = ($scope.exportPageStart-1) * noOfRows;
                expDataObj.count =(($scope.exportPageEnd - $scope.exportPageStart) + 1) * noOfRows;

                $scope.$parent.exportModal.url=expDataObj.url + joinURLParams(expDataObj);
                $scope.$parent.$parent.exportModal(false);
                commonUtilityService.showSuccessNotification("Export will start in few seconds.");

                var tableId = ($scope.$parent.dataTableObj)?$scope.$parent.dataTableObj.tableID:null;

                analytics.sendEvent({
                    category: "Operations",
                    action: tableId + " export page range",
                    label: "Management"
                });
            }

            /**
             * @func handleRadioState()
             * @desc enable or disable access to range inputs based on selection
             */
            function handleRadioState(){
                var element = element;
                $scope.errorStr = '';
                var val=$scope.exportOption;
                var $begin = $('#'+$scope.exportModal.modal.id).find("[name='rowRange']");

                if(val != 2){
                    $begin.prop("disabled", true)
                        .addClass("disabled");

                    $scope.exportPageStart = '';
                    $scope.exportPageEnd = '';
                }
                else if(val == 2){
                    $begin.prop("disabled", false)
                        .removeClass("disabled");
                }

            }

            }

        /**
         * @function joinURLParams
         * @param obj
         * @returns {string} url encoded string
         * @desc takes key value from object and converts it into url query parameters
         */
        function joinURLParams(obj){
            var str=[];
            for(var key in obj){
                if( key != "url" ){
                    var addParam = true;
                    if((key == 'search' && !obj[key])){
                        addParam = false;
                    }
                    if(addParam){
                        str.push(encodeURIComponent(key) + "=" + encodeURIComponent(obj[key]));
                    }
                }
            }
            return str.join("&");
        }


     }

})();