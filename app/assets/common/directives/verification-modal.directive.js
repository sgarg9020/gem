'use strict';

/* Directive */
(function(){
	angular.module('app')
		.directive('gsVerificationModal',VerificationModal);

	VerificationModal.$inject = ['formMessage'];
	
	/**	
		@function updateGroup()
		@param {object} formMessage - Factory object
	*/		
	function VerificationModal(formMessage) {
		
		return {
			link: link
		};
		
		/**	
			@function link()
			@param {object} $scope - directive $scope
			@param {object} element - directive HTML element
			@param {object} attrs - attributes on the directive HTML element
		*/
		function link($scope, element, attrs) {
			$scope.$parent.verificationModal.addModalMessage = setMessage;
			
			/**
				@function setMessage()
				@param {string} msg - message to use for formMessage factory
			*/
			function setMessage(msg){
				/* Enable a formMessage factory */
				formMessage.scope($scope.verificationModal);
				formMessage.className('alert-plain');
				formMessage.message(true, msg);
			}
		}
	}
})();