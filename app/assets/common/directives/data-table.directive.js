'use strict';

/* Directive */
(function () {
    angular.module('app')
		.directive('gsGenerateTableBody',GenerateTableBody);

	GenerateTableBody.$inject = ['$timeout','loadingState'];

	/**
	 @function generateTableBody
	 @param {object} $timeout - timeout service
	 @param {object} debounce - debounce factory
	 */
	function GenerateTableBody($timeout,loadingState) {
		var isLinked = false;
		var updateWait;

		var link = {
			link: link,
			restrict: "A"
		};

		return link;

		/**
		 @function link()
		 @param {object} $scope - directive scope
		 @param {object} element - directive HTML element
		 @param {object} attrs - attributes on the directive HTML element
		 */
		function link($scope, element, attrs) {

			var correctScope = ($scope.dataTableObj ? $scope.dataTableObj : $scope.$parent.dataTableObj);

			correctScope.refreshDataTable = refreshDataTable;
			correctScope.isLastPageWithAllRowsSelected = isLastPageWithAllRowsSelected;

			if (!correctScope.dataTableRef) {
				createDataTable();
			}

			function createDataTable(notifyTableCreation,recreate){
					correctScope.addDataTable = addDataTable;
					/*
					 Initialise jQuery dataTables plugin.
					 Use $timeout to execute at the end of the digest cycle
					 */
					$timeout(function () {
						correctScope.addDataTable(correctScope,recreate);
						if($scope.dataTableObj.onDrawComplete){
							$scope.dataTableObj.onDrawComplete();
						}
						isLinked = true;
						var oSettings = correctScope.dataTableRef.fnSettings();
						if(oSettings != null && correctScope.filters && correctScope.filters.selectedDisplayLength) {
							//this setting is to notify the dataTable to consider the stored selectedDisplayLength value
							oSettings._iDisplayLength = correctScope.filters.selectedDisplayLength;
						}

						if(notifyTableCreation)
						{
							notifyTableCreation();
						}
					});
			}




			function refreshDataTable(drawFromPageOne,len,recreate) {

				if(recreate)
				{
					createDataTable(null,recreate);
					return;
				}

				if(correctScope.isLastPageWithAllRowsSelected(len)){
					var oSettings = correctScope.dataTableRef.fnSettings();
					if(oSettings != null) {
						oSettings._iDisplayStart = oSettings._iDisplayStart - correctScope.filters.selectedDisplayLength;
					}
				}

				correctScope.dataTableRef.fnDraw(drawFromPageOne);
				
				if(typeof correctScope.unselectProcessedRows === "function")
					correctScope.unselectProcessedRows();

			}

			/**
			 * @function addDataTable()
			 * @param {object} scope - $scope passed from link function
			 * @desc Adds the dataTable to the page. Note: The selector and options object
			 *    come from the controller scope
			 */
			function addDataTable(scope,recreate) {

				var isCallback = false;
				var $table = $("#" + scope.tableID);

				// Create a call back if it doesn't exist
				if(scope && scope.dataTableOptions && !scope.dataTableOptions.initComplete){
					isCallback = true;
					scope.dataTableOptions.initComplete = function(settings, json){
						loadingState.hide();
					}
				}

				loadingState.show(recreate);
				//Save a reference of the table to call functions on
				scope.dataTableRef = $table.dataTable(scope.dataTableOptions);

				// Remove the loading state because a callback already exists
				if(!isCallback){
					loadingState.hide();}
			}

			function isLastPageWithAllRowsSelected(len){
				var isSelected = false;
				var table = $('#'+correctScope.tableID).DataTable();
				var pageInfo = table.page.info();

				//to check if it is last page
				if(pageInfo && (pageInfo.pages > 1) && (pageInfo.pages - 1) == pageInfo.page) {

					var tableRowsLength = table.rows().data().length;

					//to check whether all rows are selected to delete
					if (len == tableRowsLength) {
						isSelected = true;
					}
				}

				return isSelected;
			}

		}

	}
})();