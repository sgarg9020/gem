/* ============================================================
 * Directive: pgSidebar
 * AngularJS directive for Pages Sidebar jQuery plugin
 * ============================================================ */

(function(){
	angular.module('app')
		.directive('gsPgSidebar', function() {
			return {
				restrict: 'A',
				link: function(scope, element, attrs) {
					var $sidebar = $(element);
					$sidebar.sidebar($sidebar.data());

					scope.$on('$destroy',function(){
						$('body').off('click','.sidebar-menu a');
						$('.sidebar-slide-toggle').off('click touchend');
						$('.menu-items li .icon-thumbnail').off('touchstart');
						$('.menu-items li .icon-thumbnail').off('click');
						$('.toggle-sidebar-tablet').off('touchstart');
						$('body').removeClass('sidebar-open');
						$('body').removeClass('sidebar-visible');
					});

					if($('body').hasClass('mobile')){
						$('.menu-items li .icon-thumbnail').on('touchstart', openView);
					}

					else {
						$('.menu-items li .icon-thumbnail').on('click', openView);
					}

					/*
					 Any .icon-thumbnail click should be handled.
					 There are two cases:
					 1. Previous node is a dropdown
					 2. Previous node is a link

					 Either trigger the dropdown to open or open the href
					 attribute of the link.
					 */
					function openView(e){
						e.stopPropagation();
						var href = $(this).prev().prop('href');

						if(href == '' || href.indexOf('javascript:') > -1){
							// Open the dropdown
							$(this).prev().trigger('click');
						} else if(window.location.href != href){
							// Change the location to the sibling link
							window.location.href = href;

							if($(document).outerWidth() < 992 && $('body').hasClass('mobile')){
								// Force the menu to close on mobile
								$('.sm-action-bar [data-toggle=sidebar]').trigger('click');
							}
						}

						return false;
					}

					$('.toggle-sidebar-tablet').on('touchstart', function(){
						// Sidebar Toggle
						if($('body').hasClass('sidebar-visible')){
							$('.page-sidebar').css({
								"transform": "translate(0, 0)",
								"transform": "translate3d(0px, 0px, 0px)"
							});
							$('body').removeClass('sidebar-visible');}

						else{
							$('.page-sidebar').trigger('mouseenter');}

					});

				}
			}
		});
})();
