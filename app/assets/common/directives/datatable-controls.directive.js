'use strict';

(function(){
	angular.module('app')
		.directive('gsDatatableControls', SetupControls);

	SetupControls.$inject = ['notifications', '$timeout', 'updateNGRptObj','commonUtilityService','$rootScope', 'analytics','HttpPendingRequestsService','dataTableConfigService'];

	/**
	 * @func SetupControls
	 * @param notifications
	 * @param $timeout
	 * @param updateNGRptObj
	 * @param commonUtilityService
	 * @param $rootScope
	 * @param analytics
	 * @param httpPendingRequestsService
	 * @param dataTableConfigService
	 * @returns {{link: link}}
     * @constructor
     */
	function SetupControls(notifications, $timeout, updateNGRptObj,commonUtilityService,$rootScope, analytics,httpPendingRequestsService,dataTableConfigService) {
		
		return {
			link: link
		};
		
		/**
			@function link()
			@param {object} $scope - directive scope
			@param {object} element - directive HTML element
			@param {object} attrs - attributes on the directive HTML element		
		*/
		function link($scope, element, attrs) {

			$scope.dataTableObj.filterBySearch = filterDataTableBySearchValue;
			$scope.dataTableObj.filterBySearch = filterDataTableBySearchValue;
			$scope.dataTableObj.selectAll = selectAllTableRows;
			$scope.dataTableObj.selectRow = selectSingleTableRow;
			$scope.dataTableObj.bulkUpload = showBulkUploadModal;
			$scope.dataTableObj.getSelectedRows = getSelectedRows;
			$scope.dataTableObj.checkRowSelection = checkRowSelection;
			$scope.dataTableObj.showPreviousSelectedRows = showPreviousSelectedRows;
			$scope.dataTableObj.showPreviousSelectAll = showPreviousSelectAll;
			$scope.dataTableObj.unselectProcessedRows = unselectProcessedRows;
			$scope.dataTableObj.unselectAllProcessedRows = unselectAllProcessedRows;
			$scope.dataTableObj.updateDisplayLength = updateDisplayLength;
			$scope.dataTableObj.onSelectFilter = onSelectFilter;
			$scope.dataTableObj.clearCurrentDataTableObj = clearCurrentDataTableObj;

			var timer;

			var previousSearchString = getPreviousSearchString();

			var $chkAct = element.find('#checkbox_action');
			var currentDataTableObj;

			var currentDataTableElement;

			function getPreviousSearchString(){

				var tableId = 'DataTables_'+$scope.dataTableObj.tableID+'_'+location.pathname;
				if(sessionStorage.getItem(tableId) == null){
					return '';
				}
				var savedTableState = JSON.parse(sessionStorage.getItem(tableId));
				return (savedTableState && savedTableState.search)?savedTableState.search.search:'';
			}

			function getDataTableObj(){
				if(!currentDataTableObj){
					currentDataTableObj = $('#'+$scope.dataTableObj.tableID).DataTable();
				}
				return currentDataTableObj;
			}

			function clearCurrentDataTableObj(){
				currentDataTableObj = null;
			}

			function getDataTableElement(){
				if(!currentDataTableElement){
					currentDataTableElement = $('#'+$scope.dataTableObj.tableID).dataTable();
				}
				return currentDataTableElement;
			}

			if(commonUtilityService.checkMobile()){
				$scope.dataTableObj.displayLengthvalues = [10,25,50,100];
			}else{
				$scope.dataTableObj.displayLengthvalues = [10,25,50,100,250];
			}


			$scope.dataTableObj.filters = ($scope.dataTableObj.filters)?$scope.dataTableObj.filters : {};
			$scope.dataTableObj.filtersConfig = ($scope.dataTableObj.filtersConfig)?$scope.dataTableObj.filtersConfig:{};
			$scope.dataTableObj.filtersConfig.selectedDisplayLength = $scope.dataTableObj.filtersConfig.selectedDisplayLength? $scope.dataTableObj.filtersConfig.selectedDisplayLength : {type: 'local',  dbParam: 'count' };
			$scope.dataTableObj.filtersConfig.selectedDisplayLength.handleFilter = updateDisplayLength;
			var filterType = $scope.dataTableObj.filtersConfig.selectedDisplayLength.type;

			var selectedDisplayLength = dataTableConfigService.getFilter('selectedDisplayLength',filterType ,$scope.dataTableObj.tableID);

			$scope.dataTableObj.filters.selectedDisplayLength = ($scope.dataTableObj.filters.selectedDisplayLength)?  $scope.dataTableObj.filters.selectedDisplayLength : (selectedDisplayLength)?selectedDisplayLength : 10;
			dataTableConfigService.setFilter('selectedDisplayLength', $scope.dataTableObj.filters.selectedDisplayLength, filterType ,$scope.dataTableObj.tableID);

			// Debounce the analytics event so it only sends once per .5 seconds
			var sendSearchEvent = debounce(function(tableId){
				analytics.sendEvent({
					category: "Operations",
					action: tableId + " search",
					label: "Management"
				});
			},500);

			/**
				@function filterDataTableBySearchValue()
				@param {object} event - input event triggered
				 - Filter the dataTable based on the search input
			*/  
		    function filterDataTableBySearchValue(event,newValue) {
				var val = $(event.currentTarget).val().trim();

				if(event.type == 'click'){
					val = newValue;
				}

				if((val != previousSearchString) || sessionStorage.getItem("gs-showSimsAssignedToCurrentUser") || sessionStorage.getItem("gs-showSimsAssignedToCurrentGapUser") ){
					var table = getDataTableElement();

					if((event.which==13 || event.which==1)){
						if(timer){
							$timeout.cancel(timer);
						}
						previousSearchString = val;
						httpPendingRequestsService.cancelAll('searchApi');
						table.fnFilter(val);
						sendSearchEvent(table.attr('id'));
					}else{
						var timerVal = 1500;

						if(timer){
							$timeout.cancel(timer);
						}
						timer = $timeout(function() {
							previousSearchString = val;
							httpPendingRequestsService.cancelAll('searchApi');
							table.fnFilter(val);
							sendSearchEvent(table.attr('id'));
						}, timerVal);
					}
				}

		    }

			function updateDisplayLength(){

				var table = getDataTableObj();

				if(table.page.len() != $scope.dataTableObj.filters.selectedDisplayLength){
                    var pageInfo = table.page.info();
                    table.page.len($scope.dataTableObj.filters.selectedDisplayLength);
					var filterType = $scope.dataTableObj.filtersConfig.selectedDisplayLength ?$scope.dataTableObj.filtersConfig.selectedDisplayLength.type:'local';

					dataTableConfigService.setFilter('selectedDisplayLength', $scope.dataTableObj.filters.selectedDisplayLength, filterType ,$scope.dataTableObj.tableID);

                    if(pageInfo.pages != 1 || $scope.dataTableObj.filters.selectedDisplayLength<pageInfo.recordsTotal)
                    {
                    	$scope.dataTableObj.refreshDataTable(true);
                    }
				}
			}

			$scope.dataTableObj.selected = [];
		        
		    /**
				@function selectAllTableRows()
				@param {object} event - click event triggered
				- Select/Deselect all table rows
			*/  
		    function selectAllTableRows($event, isOnlyCurrentPage){
			    $event.stopPropagation();
			    $event.preventDefault();

			    var $input = angular.element($event.currentTarget).prev();
				var table = getDataTableObj();
				var $tableRows, $tableInputs;

				if(isOnlyCurrentPage){
					$tableRows = table.$('tr:visible');
					$tableInputs = table.$('tr:visible').find('input[type=checkbox]');
				}else{
					$tableRows = table.$('tr');
					$tableInputs = table.$('tr').find('input[type=checkbox]');
				}

				var uncheckAll = false;
				if($tableInputs.length > 0){
				if($input.is(':checked')){
					$input.prop('checked', false);
					$tableInputs.prop('checked', false);
					$tableRows.removeClass('row-selected');
					uncheckAll = true;
				} else {
					$input.prop('checked', true);
					$tableInputs.prop('checked', true);
					$tableRows.addClass('row-selected');
				}
				setSelectedRow($tableInputs,uncheckAll);
				}
				else{
					commonUtilityService.showWarningNotification("There are no rows available to select.");
				}
		    }

			function setSelectedRow($tableInputs,uncheckAll) {
				for(var i=0; i<$tableInputs.length ; i++){
					var $input = $tableInputs[i];
					var str = $input.getAttribute('id');
					var id = str.split('_')[1];
					var index = $.inArray(id, $scope.dataTableObj.selected);

					if (index === -1) {
						if(!uncheckAll){
							$scope.dataTableObj.selected.push(id);
						}
					} else {
						$scope.dataTableObj.selected.splice(index, 1);
					}
				}
			}

			function showPreviousSelectedRows(id, row) {
				if( id )  { id = id.toString(); }

				var indx =  $.inArray(id, $scope.dataTableObj.selected);

				if ( indx !== -1) {
					//delete $scope.dataTableObj.selected[indx];

					$(row).addClass('row-selected');
					$(row).find("td:first div input").prop('checked', true);

					var $chkAct = element.find('#checkbox_action');
					if (!$chkAct.is(':checked')) {
						$chkAct.prop('checked', true);
					}
				}
			}

            /**
             * This function selects bulk checkbox
             * Only when all rows are selected
             * @param ids
             */
			function showPreviousSelectAll(ids) {
		    	var allSelected = true;
		    	for(var i=0 ; i<ids.length; i++){
		    		var id = ids[i].toString();
					var indx =  $.inArray(id, $scope.dataTableObj.selected);
					if ( indx === -1) {
						allSelected = false;
						break;
					}
				}
				var $selectAllCheckBox = element.find('#checkbox_selectAll');
		    	if(allSelected){
					$selectAllCheckBox.prop('checked', true);
				}else{
					$selectAllCheckBox.prop('checked', false);
				}
			}


			function unselectProcessedRows( ){

				var table = getDataTableObj();
				var $tableSelectedRows = table.$('tr.row-selected');

				$scope.dataTableObj.selected = $tableSelectedRows;
				$tableSelectedRows.removeClass('row-selected');

				var $chkAct = element.find('#checkbox_action');

				if($chkAct.is(':checked')) {
					$chkAct.prop('checked', false);
				}

			}

			/**
			 * This function unselects all selected rows and
			 * selectAll checkbox(Without operations)
			 */
			function unselectAllProcessedRows(){

				var table = getDataTableObj();
				var $tableSelectedRows = table.$('tr.row-selected');
				var $tableInputs = table.$('tr').find('input[type=checkbox]');

				$tableSelectedRows.removeClass('row-selected');
				$tableInputs.prop('checked', false);

				var $selectAllCheckBox = element.find('#checkbox_selectAll');

				if($selectAllCheckBox.is(':checked')) {
					$selectAllCheckBox.prop('checked', false);
				}

				setSelectedRow($tableInputs, true);
			}

			/**
				@function selectSingleTableRow()
				@param {object} event - click event triggered
				- Select/Deselect single table row

                @param isSelectAllCheckBox - This represents table without bulk operations,
                in which 'Select All' should be selected only when all rows are selected
			*/
			// Problem - 2
		    function selectSingleTableRow($event,isSelectAllCheckBox){
				var table$ = $event.currentTarget.parentNode.parentNode.parentNode.parentNode;
			    var $input = $($event.currentTarget).prev();
			    var $tr = $($event.currentTarget).closest('tr');
			    
				/* Style table row */
				switch($input.is(':checked')){
					case true:
						$tr.removeClass('row-selected');
						$input.prop('checked', false);
						break;
						
					case false:
						var isRadioSelection = $($input).attr("name") == 'table-radio-row';
						if (isRadioSelection) {
                            var $lastSelectedTr = $(table$).find('.row-selected')[0];
                            if ($lastSelectedTr) {
                                $($lastSelectedTr).removeClass('row-selected');
                            }
						}
						$tr.addClass('row-selected');
						$input.prop('checked', true);
						break;
					
				}
				var visibleSelectedRows = $(table$).find('tr:visible.row-selected');

				if(isSelectAllCheckBox){
					var $selectAllCheckBox = element.find('#checkbox_selectAll');
					var visibleRows = $(table$).find('tr:visible');

					if(visibleRows.length === visibleSelectedRows.length){
						$selectAllCheckBox.prop('checked', true);
					}else{
						$selectAllCheckBox.prop('checked', false);
					}
				}else{
					/* Style action label */
					switch(visibleSelectedRows.length){
						case 0:
							$chkAct.prop('checked', false);
							break;

						default:
							$chkAct.prop('checked', true);
					}
				}
				setSelectedRow($input,false);
			}


			/**
				@function showBulkUploadModal()
				 - Open the bulkUploadModal
			*/  
			function showBulkUploadModal(modalId) {
				$('#'+modalId).modal('show');
			};


			/**
				@function checkRowSelection()
				@param {function} func - function to execute if necessary
			*/  
			function checkRowSelection(func,action){
				if($chkAct.is(':checked')){

					var r = func(getSelectedRows(),action);

					/* Deletion functions return a truthy value	*/
					if(r){
						$chkAct.prop('checked', false);
					}
				} else {
					commonUtilityService.showWarningNotification("Atleast one row must be selected to take action.");
				}
			}

			/**
				@function getSelectedRows()
				@return {object} obj - information about selected table rows
				@return {object} obj.id - array containing the user id's for the selected rows
				@return {object} obj.c - integer value for the number of selected rows
				 - Get each table row's input id number
			*/
			function getSelectedRows(){
				var table = getDataTableObj();
				var rows = table.$('tr.row-selected');

				return table.rows( rows ).data();
			}


			/**
				@function initToolTips()
				@desc - initToolTips will gather all nodes based on the selector
				'[data-toggle="tooltip"]' and will initialize them. The content
				for the tooltip will be taken from the title attribute on the selected
				node and the position will be taken from the data-placement
				attributre on the selected node.
			*/
			function initToolTips(){
				var is_touch_device = 'ontouchstart' in document.documentElement;

				$('[data-toggle="tooltip"]').tooltip({
					animation: true,
					container: 'body',
					trigger: (is_touch_device ? 'manual' : 'hover'),
					template: '<div class="tooltip gs-tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
				});

				if(is_touch_device){
					$('[data-toggle="tooltip"]').on("touchstart", _touchStart);
					$('[data-toggle="tooltip"]').on("touchend", _touchEnd);
				}
			}

			initToolTips();

			/*
				- The following functions help determine a longtouch event.

				- All tooltips in the datatable controls area use this
				  longtouch functionality for touch based devices which
				  determine if the tooltip should be shown.

			*/

			var timer, lockTimer;
			var touchDuration = 1500; // Length of time to touch before something is done.

			/**
				@function _touchStart()
				@param e - touch event object
				@desc - _touchStart will store a timeout to be executed
				after touchDuration has passed.
			*/
			function _touchStart(e) {

				if(lockTimer){ // A timeout already exists
					return;
				}

				var data = this;

			    timer = setTimeout(function(){
				    _invokeTooltip(data,e);
				}, touchDuration);
				lockTimer = true;
			}

			/**
				@function _touchEnd()
				@param e - touch event object
				@desc - _touchStart will clear the stored timeout when
				a touch event has ended before the touchDuration value
			*/
			function _touchEnd() {
			    if (timer){ // Stops short touches from firing the event
			        clearTimeout(timer);
					lockTimer = false;
				}
			}

			/**
				@function _invokeTooltip()
				@param data - this from touch event
				@param event - the touch event object
				@desc - _invokeTooltip will show the tooltip for the element
				that the touch event was triggered on. It will also remove
				the tooltip after 5 seconds
			*/
			function _invokeTooltip(data,event) {
				$(data).tooltip('show');

				// Remove tooltip after 5 seconds
				setTimeout(function(){
					$(data).tooltip('hide');
				}, 5000);
			};

			var refreshDataUsageCompletedListener = $rootScope.$on('onRefreshDataUsageCompleted', function (data, response) {
				$scope.dataTableObj.refreshDataTable(false);
				$scope.isDisableRefreshBtn = false;
				commonUtilityService.showSuccessNotification("Data has refreshed successfully");
			});

			var refreshDataUsageStatusFailedListener = $rootScope.$on('onRefreshDataUsageStatusFailed',function(data,response){
				//commonUtilityService.showSuccessNotification("Data is refreshed successfully");
				$scope.isDisableRefreshBtn = false;
			});

			var refreshDataUsageTimeoutListener = $rootScope.$on('onRefreshDataUsageTimeout',function(data,response){
				$scope.isDisableRefreshBtn = false;
			});

			var logOutListener = $rootScope.$on('onLogOut',function(){
				commonUtilityService.refreshDataUsageMonitor.quit();
			});

			//un registering listeners
			$scope.$on('$destroy', function(){
				if(refreshDataUsageCompletedListener)
					refreshDataUsageCompletedListener();
				if(refreshDataUsageStatusFailedListener)
					refreshDataUsageStatusFailedListener();
				if(refreshDataUsageTimeoutListener)
					refreshDataUsageTimeoutListener();
				if(logOutListener)
					logOutListener();
			});

			function onSelectFilter(filterName){

				var filterConfig =($scope.dataTableObj.filtersConfig)?($scope.dataTableObj.filtersConfig[filterName]?$scope.dataTableObj.filtersConfig[filterName]:null):null;

				if(filterConfig && typeof filterConfig.handleFilter == 'function'){
					return filterConfig.handleFilter();
				}

				var filterType = (filterConfig)?filterConfig.type:null;
				var urlPattern = (filterConfig)?filterConfig.urlUniquePattern:null;
				var selectedFilter = $scope.dataTableObj.filters[filterName];
				var storedFilter = dataTableConfigService.getFilter(filterName,filterType,$scope.dataTableObj.tableID,urlPattern);
				var isObjFilter = (storedFilter && storedFilter instanceof Object);

				if((isObjFilter &&  (selectedFilter &&  storedFilter.dbRef != selectedFilter.dbRef))  || (!isObjFilter && storedFilter != selectedFilter)){

					$scope.dataTableObj.refreshDataTable(true);

					dataTableConfigService.setFilter(filterName, selectedFilter, filterType,$scope.dataTableObj.tableID,urlPattern);

				}


			}

		}
	}
})();
