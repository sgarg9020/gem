'use strict';

/* Directive */
(function(){
    angular.module('app').directive('gsFormValidation',['$sanitize',function($sanitize){
        return{
            controller:['$scope','$filter','$element',function($scope,$filter,$element){

                $scope.isFieldNotValid = function(field,label,errorStr)
                {
                    field.errorStr = null;
                    if(field.$invalid)
                    {
                        var el = $($element).find('input[name="'+field.$name+'"]');
                        var minlengthErrorStr = null;
                        if(el && el.length )
                        {
                            minlengthErrorStr = $(el).attr('minlengtherror');
                        }
                        if(field.$error && field.$error.minlength && minlengthErrorStr)
                        {
                            field.errorStr = minlengthErrorStr;
                        }else
                        if(errorStr)
                            field.errorStr = errorStr;
                        else
                            field.errorStr = label+' invalid';
                        $scope.currentInvalidField = field;
                        return true;
                    }
                    return false;
                };

                $scope.isFieldNotValidInGroup = function(field,groupField,label,errorStr)
                {
                    field.errorStr = null;
                    groupField.errorStr = null;
                    if(field.$invalid)
                    {
                        if(errorStr)
                            groupField.errorStr = errorStr;
                        else
                            groupField.errorStr = label+' invalid';
                        $scope.currentInvalidField = groupField;
                        return true;
                    }
                    return false;
                };

                $scope.fieldsNotMatching = function(f1,label1,f2,label2,errorStr)
                {
                    f1.errorStr = null;
                    f2.errorStr = null;
                    if(f1.$viewValue != f2.$viewValue)
                    {
                        if(errorStr)
                            f2.errorStr = errorStr;
                        else
                            f2.errorStr = label1+' and '+label2 + ' do not match';
                        $scope.currentInvalidField = f2;
                        return true;
                    }
                    return false;
                };

                /*$scope.isCCNotValid = function(mf,label)
                 {
                 mf.$invalid = false;
                 //console.log("CC validator" + mf.$viewValue);
                 var fieldVal = mf.$viewValue;
                 var errorStr = null;
                 var validVISAOrMasterOrAmex = false;
                 if (fieldVal){
                 fieldVal = fieldVal.replace(/[- ]/g,'');
                 if(fieldVal.length > 16) {
                 fieldVal = fieldVal.substring(0, 16);
                 mf.$setViewValue(fieldVal);
                 }
                 if(fieldVal.length==16 || fieldVal.length==15)
                 {
                 validVISAOrMasterOrAmex = commonInfoFactory.validateCreditCard(fieldVal);
                 if(!validVISAOrMasterOrAmex)
                 errorStr = $filter('translate')('lt_only_visa_or_master');
                 }
                 }

                 if (!fieldVal || !validVISAOrMasterOrAmex) {
                 mf.$invalid = true;
                 var el = $($element);
                 var f = $(el).find('input[name="' + mf.$name + '"]');
                 if (f && f.length)
                 $scope.scrollToFieldIfNotInView(f);
                 return $scope.isFieldNotValid(mf, label,errorStr);
                 }
                 return false;
                 }
                 */



                $scope.isExpDateNotValid=function(monthField,yearField,maskField,label,yearValue){
                    maskField.$invalid = true;
                    var invalid = monthField.$invalid || yearField.$invalid;
                    var formatDate = $filter('date')(new Date(), 'yyyy-MM');
                    var expDate = $filter('date')(yearValue + "-" + monthField.$viewValue, 'yyyy-MM');
                    invalid = (invalid) ? invalid : (formatDate > expDate);
                    if (invalid) {
                        var el = $($element);
                        var f = $(el).find('input[name="' + yearField.$name + '"]');
                        if (f && f.length)
                            $scope.scrollToFieldIfNotInView(f);
                        return  $scope.isFieldNotValid(maskField, label);
                    }
                    return false;
                };
                $scope.scrollToFieldIfNotInView = function(f)
                {
                    var offset = $(f).offset().top - $(document).scrollTop();
                    if(offset>window.innerHeight || offset<0){
                        $(document).scrollTop($(f).offset().top-40);
                    }
                }

            }],
            link:function(scope,element,attrs)
            {
                /*$(element).find(":input").bind('focus',function(){
                    scope.resetInvalidFieldErrorStr();

                });*/

                $(element).on('keyup',":input",function(){
                    scope.resetInvalidFieldErrorStr();
                    scope.$apply();
                });

                scope.$on('$destroy',function(){
                   // $(element).find(":input").unbind('focus');
                    $(element).off("keyup",":input");
                });

                $(element).on('hidden.bs.modal',function(){
                    $('.form-group.form-group-default').removeClass('focused');
                    $('.form-group.form-group-default label').removeClass('fade');
                    scope.resetInvalidFieldErrorStr();

                    if(scope.addUser && scope.addUser.modal.submit == 'Add'){
                        $(element).find('input').val('');
                    }
                });

                function fieldLabel(f)
                {
                    var fname = $(f).attr("name");
                    var labelTitle = $(f).attr("gslabel");
                    if(!labelTitle)
                    {
                        var inGroup = $(f).attr('groupfield');
                        if(inGroup)
                        {
                            fname = inGroup;
                        }
                        var  label = $(element).find('label[for="'+fname+'"]');
                        labelTitle = $(label).html();
                    }
                    return labelTitle;
                }


                function isTextInput(node) {
                    return ['INPUT', 'TEXTAREA'].indexOf(node.nodeName) !== -1;
                }

                scope.resetInvalidFieldErrorStr = function(){
                    if(scope.currentInvalidField){
                        scope.currentInvalidField.errorStr = null;
                    }
                }

                scope.sanitizeData = function(val,inputField,labelTitle){
                    var sanitizedVal = $sanitize(val);
                    sanitizedVal = angular.element('<div>'+sanitizedVal+'</div>').text();

                    if(sanitizedVal != val){
                        inputField.errorStr = labelTitle+' invalid';
                        scope.currentInvalidField = inputField;
                        inputField.$invalid = true;
                        return false;
                    }

                    return true;
                }

                scope.passwordPolicyMsg = function(){

                    scope.isDisplay = true;
                    scope.passwordMsg = 'Your GigSky password should be at least 8 characters in length and contain at least one uppercase letter and one number.';
                    return false;
                };


                scope.isFormValid = function(form,modelObj,isSanitizeRequired){

                    var newData = angular.copy(modelObj);
                    scope.resetInvalidFieldErrorStr();

                    if(scope.generalObj && scope.generalObj.previousData && newData && angular.equals(scope.generalObj.previousData,newData)){
                        scope.generalObj.isNoChangeError = true;
                        return false;
                    }

                    element = $('form[name="'+form.$name +'"]');

                    //to dismiss the keyboard when we click on go button in ios
                    var activeElem = document.activeElement;
                    if ( activeElem && isTextInput(activeElem)) {
                        activeElem.blur();
                    }

                    //input elements and ui-select dropdowns
                    var allinputs = $(element).find(":input:visible,.ui-select-container");
                    //$(element).find(".btn-default").blur();
                    for(var i= 0,c=allinputs.length;i<c;i++)
                    {
                        var f = allinputs[i];

                        var fname = $(f).attr('name');
                        var labelTitle = fieldLabel(f);
                        var inputField = form[fname];
                        if(!labelTitle)
                            continue;

                        var val = f.value;
                        if($(f).attr('angular-currency')){
                            val = $(f).autoNumeric('get');
                        }
                        var isUiSelectElement = false;
                        if($(f).hasClass('ui-select-container')){
                            val = $(f).find("span.ng-binding.ng-scope").text().trim();
                            isUiSelectElement = true;
                        }

                        //to check if the input has angle brackets(><)
                        var pattern = />|<|"|“|”/;
                        if(!isUiSelectElement && pattern.test(val)){
                            inputField.errorStr = labelTitle+' invalid';
                            scope.currentInvalidField = inputField;
                            inputField.$invalid = true;
                            return false;
                        }

                        if(isSanitizeRequired){
                            var isDataValid = scope.sanitizeData(val,inputField,labelTitle);
                            if(!isDataValid){
                                return false;
                            }
                        }
                        var inGroup = $(f).attr('groupfield');

                        var requiredError = $(f).attr('requiredError');
                        if(inGroup)
                        {
                            var gfield = form[inGroup];
                            if(!gfield)
                                form[inGroup] = {};

                            if(scope.isFieldNotValidInGroup(inputField,form[inGroup] ,labelTitle))
                            {
                                scope.scrollToFieldIfNotInView(f);
                                return false;
                            }
                        }
                        else
                        if(fname)
                        {
                            if(scope.isFieldNotValid(inputField,fieldLabel(f),requiredError))
                            {
                                scope.scrollToFieldIfNotInView(f);
                                return false;
                            }
                        }
                        var confirm = $(f).attr("confirm");
                        if(confirm)
                        {
                            var label2Title = fieldLabel(f);
                            var confirmField = $(element).find('input[name="'+confirm+'"]');
                            var label1Title = fieldLabel(confirmField);
                            if(scope.fieldsNotMatching(form[confirm],label1Title,inputField,label2Title))
                            {
                                if($(f).attr('type')=='password')
                                {
                                    $(f).val('');
                                    inputField.$setViewValue('');
                                }
                                scope.scrollToFieldIfNotInView(f);
                                return false;
                            }
                        }

                        /**
                         * password policy
                         * this validation applies for only input fields with id containing 'validatePwd'
                         */
                        if( $(f).attr('type')=='password')
                        {
                            var fieldId = $(f).attr('validatePwd');

                            if(fieldId){

                                //pattern to match non-latin characters
                                var pattern = /[^\u0000-\u007F | \u0080-\u00FF | \u0100-\u017F | \u0180-\u024F | \u1E00-\u1EFF | \u2C60-\u2C7F | \uA720-\uA7FF]+/;
                                if(pattern.test(val)){
                                    scope.currentInvalidField = inputField;
                                    inputField.errorStr = 'Your password must have only english characters.';
                                    return false;
                                }
                                else{
                                    if(!(/[A-Z]/.test(val) && /\d/.test(val))){
                                        scope.currentInvalidField =inputField;
                                        inputField.errorStr = 'Your password must have at least one uppercase letter and one number.';
                                        return false;
                                    }
                                }
                            }

                        }
                    }

                    return true;
                }

                scope.getUnformattedDataForAutonumericField = function(form,fieldId){

                    var formElement  = $('form[name="'+form.$name +'"]');

                    return $(formElement).find('#'+fieldId).autoNumeric('get');
                }
            }
        }
    }]);

    angular.module('app').directive('gsEnterpriseApp',['$rootScope',function($rootScope){
        var link = {link:linkFn,restrict:'A'};
        return link;

        function linkFn(scope,element){
            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams)
            {
                $('body').removeClass('modal-open');
                $('div.modal-backdrop.fade.in').remove();
            });

        };

    }]);

    angular.module('app').directive('gsHasPermission', ['$rootScope', 'AuthenticationService','$location','$timeout', function ($rootScope, authenticationService,$location,$timeout) {
        return {
            link: linkFn,
            restrict: 'A'
        };

        function linkFn(scope, element, attrs) {

            var editPermissionPattern = /edit|update|write|delete/;
            var readPermissionPattern = /read|get/;

            function processPermissionList(){

                var modAuthArray = attrs['gsHasPermission'].split(",");

                if (modAuthArray.constructor === Array) {
                    var editSet = false;
                    var readSet = false;
                    var othersSet = false;

                    var accessLevel = authenticationService.getAccessLevel(modAuthArray);

                    if(attrs['gsHasPermission'] == "")
                        editSet = readSet = true;

                    for (var i = 0; i < modAuthArray.length; i++) {
                        var permission = modAuthArray[i].toLowerCase();
                        if (editPermissionPattern.test(permission) && accessLevel[modAuthArray[i]] == true) {
                            editSet = true;
                            readSet = true;
                            break;
                        }
                        else if (readPermissionPattern.test(permission)  && accessLevel[modAuthArray[i]] == true) {
                            readSet = true;
                        }
                        else if (accessLevel[modAuthArray[i]] == true) {
                            othersSet = othersSet || true;
                            readSet = true;
                        }
                    }
                    element.attr("gs-edit-allowed",editSet || othersSet);
                    element.attr("gs-read-allowed",readSet);
                    var isReadAllowed =  readSet && attrs['gsContextualReadPermission'] != 'false';
                    var isEditAllowed = (editSet || othersSet) && attrs['gsContextualWritePermission'] != 'false';
                    !isReadAllowed?hideElement(element):(!isEditAllowed?makeFormReadOnly(scope,element,isEditAllowed):'');
                }
            }

            processPermissionList();
            attrs.$observe('gsContextualWritePermission', function (n,o) {
                if(n){
                    handleContextualPermissions(scope,element,attrs);
                }
            });

            attrs.$observe('gsContextualReadPermission', function (n,o) {
                if(n){
                    handleContextualPermissions(scope,element,attrs);
                }
            });
        }

        function handleContextualPermissions(scope, element, attrs){

            var isReadAllowed =  element.attr("gs-read-allowed")!="false" && attrs['gsContextualReadPermission'] != 'false';
            var isEditAllowed = element.attr("gs-edit-allowed")!="false" && attrs['gsContextualWritePermission'] != 'false';
            !isReadAllowed?hideElement(element): makeFormReadOnly(scope,element,isEditAllowed);
        }
    }]);

    function showElement(element){
        if(element.attr("gs-hidden-explicit")=="true")
            element.attr("gs-hidden-explicit","false");
        element.show();
    }


    //todo handle the menu items enabling in showElement function
    function hideElement(element){

        if (element.is('li') && element.hasClass('active')) {
            $timeout(function() {
                angular.element('#' + element.next().children()[0].id).triggerHandler('click');
            });
        }
        element.attr("gs-hidden-explicit","true");
        element.hide();
    }

    function makeFormReadOnly(scope,formElem,editable) {

        if(formElem.hasClass('hideInReadOnlyMode') && !editable){
            hideElement(formElem);
        }
        else{

            if(formElem.hasClass('hideInReadOnlyMode') && editable)
                showElement(formElem);

            if(!editable && formElem.is('a') && formElem.attr('id') && formElem.attr('id').indexOf("edit") > -1) {
                formElem.off();
                $(formElem).find(".fa-pencil").hide();
                formElem.css("cursor", "default");
            }

            if(formElem.attr("gs-hidden-explicit")=="true" && editable)
                showElement(formElem);

            var inputs = formElem.find('input');
            inputs.prop("disabled",!editable);
            var selects = formElem.find('ui-select,.ui-select-container');
            for (var k = 0; k < selects.length; k++) {
                scope[selects[k].getAttribute("ng-disabled")] = !editable;
            }

            var buttons = formElem.find('button:not([data-dismiss])');
            editable?buttons.show():buttons.hide();
        }
    }


   // has no permission directive to show the message to the users while there is no permission to access the given module
    angular.module('app').directive('gsHasNoPermission', ['$rootScope', 'AuthenticationService','$location', function ($rootScope, authenticationService,$location) {
        return {link: linkFn, restrict: 'A'};
        var toShow = false;
        function linkFn(scope, element, attrs) {

            var noPermissionArr = JSON.parse( attrs.gsHasNoPermission);

            var availablePermissionList = authenticationService.getAccessLevel( noPermissionArr  );
            for( var permission in availablePermissionList ) {

                if( !availablePermissionList[ permission ] ){
                    toShow = true;
                }
                else {
                    toShow = false;
                    break;
                }
            }


            if( toShow ){
                element.show();
            }
            else element.hide();
        }
    }]);

    angular.module('app').directive('gsTypeNumber',function (){
        var link = {link:linkFn, restrict:'A'};
        return link;

        function linkFn(scope,element){
            element.on('focus', function () {
                angular.element(this).on('mousewheel', function (e) {
                    e.preventDefault();
                });
                angular.element(this).on('keyup keydown', function (e) {
                    scope.errorStr = '';
                    var charCode = (e.which) ? e.which : e.keyCode;
                    if(charCode == 46 || ( charCode > 31 && charCode < 48 ) || charCode > 57)
                        e.preventDefault();
                });
            });
            element.on('blur', function () {
                angular.element(this).off('mousewheel');
                angular.element(this).off('keyup');
                angular.element(this).off('keydown');
            });
        }
    });

    angular.module('app').directive('gsDataTableSearch',function () {
        return {
            restrict: 'E',
            scope: {
                dataTableObj: '=tableObj',
                searchValue: '=searchVal'
            },
            template: '<div id="search-table" class="form-group form-group-default sm-w-100">' +
            '<label text-id="searchText">' +
            'Search:  <!-- <gs-input-with-clear></gs-input-with-clear> -->' +
            '<input type="text" class="form-control" placeholder="{{dataTableObj.tableHeaders | searchPlaceholders}}" ng-keyup="dataTableObj.filterBySearch($event)" ng-model="searchValue">' +
            '</label>' +
            '<div class="search-icon">' +
            '<span class="icon-thumbnail" ng-click="dataTableObj.filterBySearch($event,searchValue)"><i class="pg-search"></i></span>' +
            '</div>' +
            '</div>',

            link: function(scope, element, attrs, ctrls) {

            }
        };
    });

    angular.module('app').directive('gsDataTableFilter', [ '$sce', function ($sce){
        return {
            restrict: 'E',
            transclude: true,
            scope:{
                dataTableObj : "=tableObj",
                filter : "@filter",
                label : "@label",
                list : "=list",
                disableSelect : "=disableSelect",
                enableSearch : "=enableSearch",
                noSearchResultText : "@noSearchResultText"
            },
            template:
            '<div class="filter-table fg-wrap m-b-5 m-r-10">' +
                '<div gs-pg-form-group test-arg="{{filter}}" class="form-group form-group-default form-group-default-select">' +
                    '<div class="paceDiv paceDisable"></div>' +
                    '<label class="p-r-5" ng-class="{disabledComponent : disableSelect}">{{(label || filter)}}:</label>' +
                    '<ui-select ng-disabled="disableSelect" ng-model="dataTableObj.filters[filter]" search-enabled="enableSearch"  on-select="dataTableObj.onSelectFilter(filter)" theme="bootstrap">' +
                        '<ui-select-match>{{(dataTableObj.filters[filter] && dataTableObj.filters[filter].name) || dataTableObj.filters[filter] }}</ui-select-match>' +
                        '<ui-select-choices repeat="item in list | filter: $select.search ">' +
                            '<span ng-bind-html="trustAsHtml(item) | underscoreless | highlight:$select.search"></span>' +
                        '</ui-select-choices>' +
                        '<ui-select-no-choice>{{noSearchResultText}}</ui-select-no-choice>' +
                    '</ui-select>' +
                '</div>' +
            '</div>',
            link : function (scope, element, attrs) {

                scope.trustAsHtml = function (value){
                    var displayValue = value.name || value;


                    return (typeof displayValue === 'string')?$sce.trustAsHtml(displayValue):displayValue;
                };
            }
        };

    }]);

})();
