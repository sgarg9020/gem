// Define module using Universal Module Definition pattern
// https://github.com/umdjs/umd/blob/master/returnExports.js

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // Support AMD. Register as an anonymous module.
    // EDIT: List all dependencies in AMD style
    define(['angular', 'dropzone'], factory);
  }
  else {
    // No AMD. Set module as a global variable
    // EDIT: Pass dependencies to factory function
    factory(root.angular, root.Dropzone);
  }
}(this,
//EDIT: The dependencies are passed to this function
function (angular, Dropzone) {
  //---------------------------------------------------
  // BEGIN code for this module
  //---------------------------------------------------

  'use strict';

  return angular.module('ngDropzone', [])
    .directive('ngDropzone',['commonUtilityService','$location','$rootScope', function (commonUtilityService,$location,$rootScope) {
      return {
        restrict: 'AE',
        template: '<div ng-transclude></div>',
        transclude: true,
        scope: {
          dropzone: '=',
          dropzoneConfig: '=',
          eventHandlers: '=',
          refreshDatatable:"=",
          modalId:"=",
          warningModalObj:"=",
          previewUploadObj:"="
        },
        link: function(scope, element, attrs, ctrls) {
            try { Dropzone }
            catch (error) {
                throw new Error('Dropzone.js not loaded.');
            }

            if(!scope.dropzone) {
                var dropzone = new Dropzone(element[0], scope.dropzoneConfig);
                var f = dropzone.addFile;
                dropzone.addFile = function (file) {
                    if (dropzone.getQueuedFiles().length == 0) {
                        f.call(dropzone, file);
                    }
                    else {
                        //dropzone._this._errorProcessing([file], "file limit exceeded");
                        commonUtilityService.showErrorNotification("Can not upload more than one file at a time.");

                    }
                };

                dropzone.on('complete', function (file, responseText) {

                    if (dropzone.getQueuedFiles().length == 0 && dropzone.getUploadingFiles().length == 0) {
                        // Remove all files

                        if (file.status == 'success') {
                            if (scope.previewUploadObj && scope.previewUploadObj.status === 'preview' && (typeof scope.previewUploadObj.callback === 'function')) {
                                file.status = Dropzone.QUEUED;
                                file.xhr && file.xhr.response ? scope.previewUploadObj.callback(file.xhr.response) : scope.previewUploadObj.callback();
                            } else {
                                if (scope.refreshDatatable) {
                                    scope.refreshDatatable();
                                }
                                commonUtilityService.showSuccessNotification("File uploaded successfully");
                                if (dropzone.files.length > 0) {
                                    dropzone.removeAllFiles();
                                }
                            }
                            $rootScope.$broadcast('dropZoneUploadSuccess');
                        }
                        else if (file.status == 'canceled') {
                            if (scope.refreshDatatable) {
                                scope.refreshDatatable();
                            }
                            commonUtilityService.showErrorNotification("Upload Cancelled");
                        }

                        $('#' + scope.modalId).modal('hide');

                    }
                    if (file.status != 'error') {
                        $('#cancelUpload').prop('disabled', true);
                        $('#uploadFiles').prop('disabled', true);
                        $('#clearDropzone').prop('disabled', true);
                    }
                });


                // on error
                dropzone.on('error', function (file, errorMessage) {
                    if (scope.previewUploadObj && scope.previewUploadObj.disableErrorNotification){
                        return $rootScope.$broadcast('dropZoneUploadError',errorMessage);
                    }else if (errorMessage.type !== 'ErrorsGroup') {
                        if (errorMessage.errorStr !== undefined) {
                            file.previewElement.querySelector("[data-dz-errormessage]").textContent = errorMessage.errorStr;
                            if (errorMessage.errorStr == "Invalid token") {
                                $location.path("#/auth/login");
                                $location.replace();
                                window.location.reload(true);
                            } else {
                                commonUtilityService.showErrorNotification(errorMessage.errorStr);
                            }
                        }
                        else if (errorMessage === 'Server responded with 0 code.') {
                            commonUtilityService.showErrorNotification("Unable to connect to GigSky system");
                        }
                        else {
                            commonUtilityService.showErrorNotification(errorMessage);
                        }
                    } else {
                        scope.warningModalObj.onContinue = function () {
                            file.status = Dropzone.QUEUED;
                            scope.warningModalObj.attemptToUpload(true);
                        };
                        scope.warningModalObj.onCancel = function () {
                            if (dropzone.getUploadingFiles().length == 0) {
                                dropzone.removeAllFiles();
                            }
                        };
                        commonUtilityService.showErrorNotification(errorMessage, errorMessage.errorInt, scope.warningModalObj);
                    }

                    $rootScope.$broadcast('dropZoneUploadError',errorMessage);


                    $('#cancelUpload').prop('disabled', true);
                    $('#uploadFiles').prop('disabled', true);
                    $('#clearDropzone').prop('disabled', false);
                });

                dropzone.on('sending', function (file) {
                    $('#cancelUpload').prop('disabled', false);
                    $('#uploadFiles').prop('disabled', true);
                    $('#clearDropzone').prop('disabled', true);
                });

                dropzone.on('addedfile', function (file) {
                    if (dropzone.files.length > 0) {
                        $('#clearDropzone').prop('disabled', false);
                        $('#uploadFiles').prop('disabled', false);
                    }

                });
                dropzone.on('removedfile', function (file) {
                    if (dropzone.files.length == 0) {
                        $('#clearDropzone').prop('disabled', true);
                        $('#uploadFiles').prop('disabled', true);
                    }
                });
                $(document).on('hide.bs.modal', '#' + scope.modalId, function (e) {
                    if (dropzone.getUploadingFiles().length > 0) {
                        commonUtilityService.showSuccessNotification("File upload is happening in background");
                    }

                });

                if (scope.eventHandlers) {
                    Object.keys(scope.eventHandlers).forEach(function (eventName) {
                        dropzone.on(eventName, scope.eventHandlers[eventName]);
                    });
                }

                scope.dropzone = dropzone;
            }
            if(scope.dropzone){
                dropzone = scope.dropzone;
                scope.$on('submitDropzoneFiles', function (event, data) {
                    if(dropzone.files.length>0) {
                        if(scope.dropzoneConfig.url != dropzone.options.url){
                            dropzone.options.url = scope.dropzoneConfig.url;
                        }
                        if(data && data.ignoreWarnings){
                            dropzone.options.url = dropzone.options.url + "?ignoreWarnings=true";
                        }
                        dropzone.processQueue();
                    }
                    else {
                        $('#'+scope.modalId).modal('hide');
                        commonUtilityService.showErrorNotification("No file selected");
                    }
                });
                scope.$on('clearDropzoneFiles', function (event, data) {
                    // Remove all files
                    if (dropzone.getUploadingFiles().length == 0) {
                        dropzone.removeAllFiles();
                    }
                });
                scope.$on('cancelDropzoneUpload', function (event, data) {
                    // Stop upload
                    dropzone.removeAllFiles(true);
                });

            }

        }
      };
    }]);

  //---------------------------------------------------
  // END code for this module
  //---------------------------------------------------
}));


