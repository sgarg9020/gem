gemcdn=$1
buildno=$2
#./buildscripts/versioncdnresources.sh ./buildscripts/cdnassets-other.conf $gemcdn
#./buildscripts/versioncdnresources.sh ./buildscripts/cdnassets-css.conf $gemcdn
#./buildscripts/mergethirdpartyjs.sh ./buildscripts/thirdparty-js.conf $gemcdn $buildno thirdparty
#./buildscripts/mergethirdpartyjs.sh ./buildscripts/application-js.conf $gemcdn $buildno application

./buildScripts/versionimages.sh ./buildScripts/cdnassets-other.conf $gemcdn
./buildScripts/versioncss.sh ./buildScripts/cdnassets-css.conf $gemcdn

cp -rf img cdnassets/
cp -rf css cdnassets/
cp -rf fonts cdnassets/
cp -rf external cdnassets/

./buildScripts/minifyjs.sh ./buildScripts/application-individual-js.conf $gemcdn $buildno application
./buildScripts/mergethirdpartyjs.sh ./buildScripts/thirdparty-js.conf $gemcdn $buildno thirdparty
./buildScripts/mergethirdpartyjs.sh ./buildScripts/application-js.conf $gemcdn $buildno application
