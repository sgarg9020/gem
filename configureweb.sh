#!/bin/bash

#development
#base = "https://localhost:8443/webclient/";
#appid = "495138660529683";
#QA
#base = "https://localhost:8443/gscanvas/";
#appid = "353068714791824";
if [ $# -lt 1 ]
then
echo "usage is : configureweb.sh <option>"
echo "Options available are: "
echo "p   - Configures production environment"
echo "q   - Configures QA environment"
echo "lb   - Configures Local environment"
echo "t   - Configures test automation environment"
exit 1
fi

envtype=$1
echo "configuring webclient to $envtype environment"


machine="mac"
if [ "$(uname)" == "Darwin" ]; then
machine="mac"
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
machine="linux"
fi

location=$2


if [[ $envtype == "lb" ]]; then

    buildno=`cat versiondetails.json | sed -n 's/.*buildno":"\([0-9]*\)\".*$/\1/p'`


    if [ $# -eq 1 ]; then
    location="";
    else
    argpath=$2;
    argescpath=${argpath//\//\\\/};
    location="\/"$argescpath;
    fi

     address='localhost';
     if [ $# -eq 3 ]; then
     address=$3;
     fi

    build="\/gemassets\/"$buildno"\/";

    sed -i.bkup 's/var[ \t]*LAZY_LOAD_BASE[ \t]*=[ \t]*.*;/var LAZY_LOAD_BASE = "'"\/\/$address$location$build"'";/g' gemassets/$buildno/app/assets/config/apiconfig.js
    sed -i.bkup 's/var[ \t]*ENTERPRISE_ENVIRONMENT[ \t]*=[ \t]*.*;/var ENTERPRISE_ENVIRONMENT = "'"$envtype"'";/g' gemassets/$buildno/app/assets/config/apiconfig.js

    if [[ $machine == "mac" ]]; then
            find . \( -name "*.css" -o -name "*.html" \) -maxdepth 10 -exec sed -i "" "s/cdn.gigsky.com\/gem/$address$location\/gemassets/g" {} + ;
            else
            find . \( -name "*.css" -o -name "*.html" \) -exec sed -i "s/cdn.gigsky.com\/gem/$address$location\/gemassets/g" {} +;
    fi
    sed -i.bkup 's/apiconfig.*.js/apiconfig.js/g' index.html
else
    sed -i.bkup 's/apiconfig.*.js/apiconfig-'"$envtype"'.js/g' index.html
fi
