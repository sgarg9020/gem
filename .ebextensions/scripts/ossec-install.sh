#!/usr/bin/env bash

appName="gem"

if ([ ! -f /root/.not-a-new-instance.txt ]) then
  newEC2Instance=true
fi

if ([ $newEC2Instance ]) then
# Allow sudo command to be used as part of beanstalk ebextensions scripts without a terminal
    grep -q 'Defaults:root !requiretty' /etc/sudoers.d/$appName || echo -e 'Defaults:root !requirettyn' > /etc/sudoers.d/$appName
    chmod 440 /etc/sudoers.d/$appName

# Add sudo command if not already present to .bashrc of ec2-user so that we are logged on as root when we use ssh
    grep -q "sudo -s" /home/ec2-user/.bashrc || echo -e "nsudo -sn" >> /home/ec2-user/.bashrc

# Install OSSEC agent

yum -y install mysql-devel postgresql-devel
wget http://www.ossec.net/files/ossec-hids-2.7.1.tar.gz -P /opt
tar xzvf /opt/ossec-hids-2.7.1.tar.gz -C /opt/
rm /opt/ossec-hids-2.7.1.tar.gz
cp /gem/.ebextensions/scripts/ossec-preloaded-vars.conf /opt/ossec-hids-2.7.1/etc/preloaded-vars.conf
/opt/ossec-hids-2.7.1/install.sh
/var/ossec/bin/agent-auth -m 10.3.0.72 -p 1515
/var/ossec/bin/ossec-control restart
fi

if ([ $newEC2Instance ]) then
    echo -n "" > /root/.not-a-new-instance.txt
fi
