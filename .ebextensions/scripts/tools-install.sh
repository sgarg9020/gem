#!/usr/bin/env bash

appName="gem"

if ([ ! -f /root/.not-a-new-instance.txt ]) then
  newEC2Instance=true
fi

if ([ $newEC2Instance ]) then
# Allow sudo command to be used as part of beanstalk ebextensions scripts without a terminal
    grep -q 'Defaults:root !requiretty' /etc/sudoers.d/$appName || echo -e 'Defaults:root !requirettyn' > /etc/sudoers.d/$appName
    chmod 440 /etc/sudoers.d/$appName

# Add sudo command if not already present to .bashrc of ec2-user so that we are logged on as root when we use ssh
    grep -q "sudo -s" /home/ec2-user/.bashrc || echo -e "sudo -sn" >> /home/ec2-user/.bashrc

# Install Metricly Linux Agent

sudo N_APIKEY=552edced2c5c1d3045c46554248792c4 bash -c "$(curl -Ls http://repos.app.netuitive.com/linux.sh)"

# Install OSSEC agent

yum -y install mysql-devel postgresql-devel
wget https://github.com/ossec/ossec-hids/archive/2.9.3.tar.gz -P /gem
tar xzvf /gem/2.9.3.tar.gz -C /gem/
rm -f /gem/2.9.3.tar.gz
cp /gem/.ebextensions/scripts/ossec-preloaded-vars.conf /gem/ossec-hids-2.9.3/etc/preloaded-vars.conf
/gem/ossec-hids-2.9.3/install.sh
/var/ossec/bin/agent-auth -m $OSSEC_SERVER -p 1515
/var/ossec/bin/ossec-control restart
fi

if ([ $newEC2Instance ]) then
    echo -n "" > /root/.not-a-new-instance.txt
fi
