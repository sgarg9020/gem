if [[ $# < 1 ]]; then
echo " Require Following arguments "
echo " -i<Path to PEM file>";
exit 1;
fi
user="ec2-user";
pem=""
sshbox="54.67.3.161";
webserver="10.2.1.153";

while getopts ":i:" option; do
case $option in
i)
pem=$OPTARG ;;
\?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
 :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
esac
done;

rm -fr gembuild
svn export ./ gembuild --force
tar -zcf gembuild.tgz gembuild
echo 'sudo mv gembuild.tgz /var/www/gigsky/' > b.sh
echo 'cd /var/www/gigsky/' >> b.sh
echo 'sudo rm -fr gembuild' >> b.sh
echo 'sudo tar -zxf gembuild.tgz' >> b.sh
echo 'sudo rm -fr gemstage/gem' >> b.sh
echo 'sudo mv gembuild gemstage/gem' >> b.sh
echo 'sudo rm -fr gembuild.tgz' >> b.sh
echo 'cd gemstage/' >> b.sh
echo 'sudo sed -i.bkup "s/var[ \t]*ENTERPRISE_ENVIRONMENT[ \t]*=[ \t]*.*;/var ENTERPRISE_ENVIRONMENT = \"cs\";/g" gem/app/assets/scripts/config.js' >>b.sh

chmod 0777 b.sh

scp -i $pem gembuild.tgz $user@$sshbox:/home/$user/
scp -i $pem b.sh $user@$sshbox:/home/$user/


ssh -i $pem $user@$sshbox 'scp -i GS-Dev-US-VPC-Ubuntu.pem gembuild.tgz ubuntu@10.2.1.153:/home/ubuntu/'
ssh -i $pem $user@$sshbox 'scp -i GS-Dev-US-VPC-Ubuntu.pem b.sh ubuntu@10.2.1.153:/home/ubuntu/'
ssh -i $pem $user@$sshbox 'ssh -i GS-Dev-US-VPC-Ubuntu.pem ubuntu@10.2.1.153 ./b.sh'
rm -fr gembuild.tgz
rm -fr gembuild
rm -fr b.sh
