// Karma configuration
// Generated on Tue Sep 01 2015 01:20:36 GMT+0530 (IST)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '../',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
        '../app/app/plugins/jquery/jquery-1.11.1.min.js',
        '../app/app/plugins/jquery-ui/jquery-ui.min.js',
        '../app/app/plugins/angular/angular.js',
        'test/bower-components/angular-mocks.js',
        '../app/app/plugins/angular-ui-router/angular-ui-router.min.js',
        '../app/app/plugins/angular-ui-util/ui-utils.min.js',
        '../app/app/plugins/angular-sanitize/angular-sanitize.min.js',
        '../app/app/plugins/angular-oc-lazyload/ocLazyLoad.min.js',
        '../app/app/plugins/angular-dropzone/angular-dropzone.js',
        '../app/app/plugins/boostrapv3/js/bootstrap.min.js',
        '../app/app/plugins/ng-csv/ng-csv.js',
        '../app/app/plugins/pace/pace.min.js',
        '../app/app/plugins/currencyutils/currencies.js',
        '../app/app/plugins/angular-currency/angular-currency.js',
        '../app/app/plugins/angular-breadcrumb/angular-breadcrumb.min.js',
        'app/pages/script/pages.js',
        'app/assets/config/app.js',
        'app/assets/config/apiconfig.js',
        'app/assets/account-settings/*.js',
        'app/assets/alerts/**/*.js',
        'app/assets/auth/*.js',
        'app/assets/common/**/*.js',
        'app/assets/dashboard/*.js',
        'app/assets/data-usage/*.js',
        'app/assets/enterprise-settings/*.js',
        'app/assets/groups/*.js',
        'app/assets/invoices/*.js',
        'app/assets/sims/*.js',
        'app/assets/support/**/*.js',
        'app/assets/users/**/*.js',

        'test/unit/assets/scripts/**/*Spec.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        'app/assets/**/*.js': 'coverage'
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress','junit','coverage'],

    // the default configuration
    junitReporter: {
      outputDir: './test/results', // results will be saved as $outputDir/$browserName.xml
      outputFile: undefined, // if included, results will be saved as $outputDir/$browserName/$outputFile
      suite: '' // suite will become the package name attribute in xml testsuite element
    },

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    // browsers: ['PhantomJS2','Chrome'],
    browsers: ['PhantomJS2'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    //coverage reporter configuration
    coverageReporter: {
          type : 'html',
          dir : './test/results/coverage/'
    }
  })
}
