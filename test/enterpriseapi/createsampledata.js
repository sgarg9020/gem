/**
 * Created with JetBrains WebStorm.
 * User: jagadish
 * Date: 06/09/15
 * Time: 3:22 AM
 * To change this template use File | Settings | File Templates.
 */
var mongoose= require('mongoose');
var db = mongoose.connection;
var exports = module.exports = {};
var docs=require("./data/accounts.json");
var database = require('./database.js');


db.on("error",console.error.bind(console, 'connection error:'));

db.once("open",function(){
    console.log("Successfully connected to enterprise db");
});

database.group.collection.insert({
    "type": "Group",
    "name": "US West"
});

database.group.collection.insert({
    "type": "Group",
    "name": "California",
    "parent": "US West"
});

database.group.collection.insert({
    "type": "Group",
    "name": "Oregon",
    "parent": "US West"
});

database.group.collection.insert({
    "type": "Group",
    "name": "Washington",
    "parent": "US West"
});