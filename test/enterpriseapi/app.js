var express = require('express');
var path = require('path');
//var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors= require('cors');

var routes = require('./routes/index');
var users = require('./routes/users');
var groups = require('./routes/groups');
var sims = require('./routes/sims');
var account = require('./routes/account');
var commonApis = require('./routes/common');
var enterprise = require('./routes/enterprise');
var planDetails = require('./routes/planDetails');
var dayWiseUsages = require('./routes/usages');
var invoices = require('./routes/invoices');
var alerts = require('./routes/alerts');
var cors = require('cors');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
var subapp = express.Router();
var analyticsapp = express.Router();
var baseEgbApp = express.Router();
var baseUmApp = express.Router();
var basePmbApp = express.Router();


app.use(function(req, res, next) {
    if(req.originalUrl.indexOf("/login")==-1 && req.originalUrl.indexOf("/password")==-1 && req.originalUrl.indexOf("/sims/assign")==-1 && req.originalUrl.indexOf("/sims/generateUsage")==-1)
    {
        return account.authenticateAccount(req,res,next);
    }
    next();
});

subapp.use('/account/',account);
subapp.use('/users', users);
subapp.use('/groups',groups);
subapp.use('/sims',sims);
subapp.use('/common',commonApis);
subapp.use('/enterprise',enterprise);
subapp.use('/planDetails',planDetails);

analyticsapp.use('/account',dayWiseUsages);


//Invoices
baseEgbApp.use('/account/*/invoice',invoices);

// Enterprise SIM Management
baseEgbApp.use('/account/*/sims', sims);
baseEgbApp.use('/account/*/user/*/sims',sims);
baseEgbApp.use('/accounts/account/:aid/sims', sims);
baseEgbApp.use('/accounts/:aid', sims);


// Enterprise User Management
baseEgbApp.use('/account/*/users', users );
//baseEgbApp.use( '/*/users',users);
baseEgbApp.use( '/accounts/account/*/user', users );
baseEgbApp.use( '/accounts/account/*/users/', users );
baseEgbApp.use( "/accounts/*/users", users );


// enterprise/group settings
baseEgbApp.use( '/accounts/account/', groups );

// get enterprise settings
baseEgbApp.use( '/accounts/account/', enterprise );

// get enterprise alert settings
baseEgbApp.use( '/accounts/account/', alerts );

//countries
basePmbApp.use('/account/countries',commonApis);
basePmbApp.use('/account/*/zones',planDetails);

//user updates
baseUmApp.use('/users/user',account);

app.use('/api/v1',subapp);
app.use('/abs/api/v1',analyticsapp);
app.use('/embs/api/v1',baseEgbApp);
app.use('/umbs/api/v1',baseUmApp);
app.use('/pmbs/api/v1',basePmbApp);

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({type:"Error",errorInt:err.appErrCode,errorStr:err.message});
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});



module.exports = app;
