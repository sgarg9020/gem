mongoimport --db demoDb --collection enterprises --file enterprises.json --jsonArray --upsert --upsertFields companyName
mongoimport --db demoDb --collection countries --file countries.json --jsonArray --upsert --upsertFields name
mongoimport --db demoDb --collection groups --file groups.json --jsonArray --upsert --upsertFields name
mongoimport --db demoDb --collection users --file  users-attendants.json --jsonArray --upsert --upsertFields emailId
mongoimport --db demoDb --collection users --file  users-maintenance.json --jsonArray --upsert --upsertFields emailId
mongoimport --db demoDb --collection users --file  users-pilots.json --jsonArray --upsert --upsertFields emailId
mongoimport --db demoDb --collection sims --file sims.json --jsonArray --upsert --upsertFields name
mongoimport --db demoDb --collection plandetails --file planDetails.json --jsonArray --upsert --upsertFields zones
mongoimport --db demoDb --collection daywiseusages --file dataUsage.json --jsonArray