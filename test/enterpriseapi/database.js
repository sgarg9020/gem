/**
 * Created with JetBrains WebStorm.
 * User: jagadish
 * Date: 06/09/15
 * Time: 7:43 PM
 * To change this template use File | Settings | File Templates.
 */
var exports = {};

var mongoose = require('mongoose');

var transform = require('./jsonpath-object-transform.js');

var ISDEMO = false;

if(!ISDEMO){
mongoose.connect('mongodb://localhost/enterprise');
}
else{
mongoose.connect('mongodb://localhost/demoDb');
}

var mongoosePaginate = require('mongoose-paginate');
var db = mongoose.connection;


db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
    console.log("successfully opened mongodb connection");
});

exports.db = db;

exports.loginToken = mongoose.model('loginToken',mongoose.Schema({token:String,customerId:String}));
var userSchema = mongoose.Schema({firstName:{type:String},lastName:{type:String},emailId:{type:String},password:{type:String},location:{type:String},country:{type:String},group:{type: mongoose.Schema.Types.ObjectId, ref: 'group' },enterprise:{type: mongoose.Schema.Types.ObjectId, ref: 'enterprise' },accounts:[{type: mongoose.Schema.Types.ObjectId, ref: 'enterprise' }]},{id:false,toJSON:{virtuals:true}}).plugin(mongoosePaginate);
userSchema.virtual('userId').get(function(){return this._id.toHexString();});
userSchema.virtual('type').get(function(){return "User";});

userSchema.index({firstName:1,lastName:1,location:1,emailId:1});

exports.user = mongoose.model('user',userSchema);
exports.user.collection.ensureIndex({firstName:"text",lastName:"text",location:"text",emailId:"text"});
exports.user.projection = {firstName:1,lastName:1,group:1,location:1,emailId:1,country:1,accounts:1,enterprise:1};
exports.user.pwdProjection = {password:1};
exports.user.setFields = function(user){
  var o = user;
  delete o.type;

  if(  o.toAccount ) {
      if (o.toAccount == -1) {
          o.group = null;
      }
      else if (o.toAccount !==  null) {
          o.group = exports.ObjectId(o.toAccount);
      }
      delete o.toAccount;
  }

    if(o.address){
        var country = o.address.country;
        delete o.address;
        o.country = country;
    }

    var enterprise = o.enterprise;
    if (enterprise) {
        delete o.enterprise;
        if (enterprise.accountId)
            o.enterprise = exports.ObjectId(enterprise.accountId);
    }

    return o;
};

exports.extractFilter = function(filter){

    var fields = filter.split(",");
    var f  = {};
    for(var i =0;i <fields.length;i++)
    {
        var o = fields[i];
        var kv = o.split(":");
        f[kv[0]]=kv[1];
    }
    return f;
};
exports.user.getFilters=function(filter, direction ){
    var f = {};
    if( direction == "ASC") direction = 1;
    else direction = -1;
    f[ filter ] = direction;
    if(filter == "fullName" ) {
        f = {"firstName": direction }; //, "lastName": direction};
    }

    if( filter == "accountName"){
        f = { "group.name" : direction };
    }

    if( filter == "totalSimsAssociated") {
        f = { sim : direction };
    }

    if( filter =="dataUsage") {
        f = { usage: direction };
    }
    return f;
};

//exports.user.countryPopulation = {path:'country',select:'name code countryId'};
exports.user.groupPopulation = {path:'group',select:'name groupId parentGroupId'};
exports.user.enterprisePopulation = {path:'enterprise',select:'accountName accountId'};
exports.user.accountsPopulation = {path:'accounts',select:'accountName accountId'};
var groupSchema =mongoose.Schema({name:{type:String,index: {unique: true}},parentGroupId:mongoose.Schema.Types.ObjectId},{id:false,toJSON:{virtuals:true}, toObject:{virtuals:true } }).plugin(mongoosePaginate);
groupSchema.virtual('groupId').get(function(){ return this._id.toHexString();});
groupSchema.virtual('type').get(function(){return "Group";});
//groupSchema.index({name:1});
exports.group = mongoose.model('group',groupSchema);
//exports.group.collection.ensureIndex({name:"text"});

exports.group.projection = {name:1,parentGroupId:1};//{type:{$literal:"Group"},groupId:"$_id",_id:0,name:1,parentGroupId:1};

exports.group.setFields = function(group){
    var o = {};
    delete group.type;
    delete group.accountType;

    for(var k in group){

        o[k] = group[k];
    }

    if(o.accountName ){
        o.name = o.accountName;
        delete o.accountName;
    }
    if(o.accountId ){
        o.groupId = o.accountId;
        delete o.accountId;
    }
    if(o.parentAccountId ) {
        o.parentGroupId = o.parentAccountId;
        delete o.parentAccountId;
    }


    return o;
};

exports.group.getFilters=function(filter, direction ){
    var f = {};
    if( direction == "ASC") direction = 1;
    else direction = -1;
    f[filter]= direction;

    if( filter == "accountName" ) {
        f = { "name" : direction };
    }
    return f;
};

var simSchema = mongoose.Schema({status:{type:String},assigned:{type:Boolean},iccId:{type:String,index:true},imsi:{type:String},msisdn:{type:String},name:{type:String,index:true},allocation:{type:Number},group:{type: mongoose.Schema.Types.ObjectId, ref: 'group' ,default:null},user:{type:mongoose.Schema.Types.ObjectId, ref: 'user',default:null}},{id:false,toJSON:{virtuals:true}}).plugin(mongoosePaginate);
//simSchema.index({name:1});
exports.sim = mongoose.model('sim',simSchema);
exports.sim.groupPopulation = {path:'group',select:'name groupId'};
exports.sim.userPopulation = {path:'user',select:'firstName lastName userId'};
exports.sim.collection.ensureIndex({name:"text"});
exports.sim.projection = {status:1,assigned:1,group:1,iccId:1,name:1,user:1, allocation:1,imsi:1, msisdn:1};
exports.sim.generalInfoProjection = {name:1,iccId:1};

simSchema.virtual('simId').get(function(){return this._id.toHexString();});
simSchema.virtual('type').get(function(){return "SIM";});
exports.mongoose = mongoose;
exports.schema = mongoose.Schema;
exports.ObjectId = function(id){return exports.mongoose.Types.ObjectId(id);}
module.exports = exports;

exports.sim.setFields = function(sim){
    var o = {};

    delete sim.type;


    for(var k in sim){
        o[k] = sim[k];
    }
    if(o.simId ){
        o.iccId = o.simId;
        delete o.simId;
    }
    if(o.status ){
        if( o.status == "ACTIVATE") o.status = "active";
        else if( o.status == "DEACTIVATE" ) o.status = "inactive";
    }
    var name = o.nickName;
    if( name ) {
        o.name = name;
        delete o.nickName;
    }

    if(o.creditiLimitList && o.creditLimitList.list ) {
        var allocation = o.creditLimitList.list[0].limitKB;
        o.allocation = allocation;
        delete o.creditLimitList;
    }


    var group = o.toAccount;
    if(group)
    {
        delete o.toAccount;
        if( group == -1 ){
            o.group = null;
            o.user = null;
            o.assigned = false;
        }
        else if( typeof group != "undefined" ){
            o.group =  exports.ObjectId(group);
            o.assigned = true;
            o.user = null;
        }

    }
    var user = o.toUserId;
    if(user) {

        delete o.toUserId;

        if (user == -1) {
            o.user = null;
        }
        else {
            o.user = exports.ObjectId(user);
        }
    }
    return o;
};

exports.sim.getFilters=function(filter, direction ){

    var f = {};
    if( direction == "ASC") direction = 1;
    else direction = -1;
    f[filter]= direction;

    if(filter == "simId" || filter == "ICCICD" ){
        f = {iccId:direction};
    }

    if(filter == "simStatus")
    {
        f = {status:direction};
    }
    if(filter == "accountName")
    {
        f = { "group.name": direction };
    }

    if( filter == "firstName"){
        f = { "user.firstName": direction };
    }

    if( filter == "fullName"){
        f = { "user.firstName": direction, "user.lastName": direction };
    }

    if( filter == "nickName"){
        f = { "name" : direction };
    }

    if( filter == 'dataUsage' ){
        f = { usage : direction };

    }
    if( filter == 'limitKB' ) {
        f = { allocation : direction };
    }
    return f;
};


exports.sortOnPopulatedFields= function( resultSet, populatedPath, sortField, sortOrder ) {

    resultSet = resultSet.sort(function (a, b) {

        if(a[populatedPath]==null && b[populatedPath] ==null)
        {
            return 0;
        }

        if(a[populatedPath]==null)
            return 1;
        if(b[populatedPath] == null)
            return -1;
        if (sortOrder == 1){
            return a[populatedPath][sortField].localeCompare(b[populatedPath][sortField]);
        }
        else{
            return b[populatedPath][sortField].localeCompare(a[populatedPath][sortField]);
        }
    });
    return resultSet;
};

function extractValuesFromObject(currentObj, columns, valList) {
    for (var key in currentObj) {
        if(typeof currentObj[key] == 'object' && currentObj[key] != null){
            extractValuesFromObject(currentObj[key]._doc,columns, valList);
        }else if(currentObj[key] != null && columns.indexOf(key) != -1) {
            valList.push(currentObj[key].toLowerCase());
        }
    }

    return valList
}

exports.transformJson = function(resp, template) {
    var result = transform(resp, template);
    return result;
}

exports.searchOnPopulatedFields= function( resultSet, searchKey, columns) {

    if( typeof searchKey == "undefined") return false;

    var searchKeywords = searchKey.split(' ');

    var responseList = [];
    for(var index in resultSet){
        var sim = resultSet[index];

        var valList = [];
        valList = extractValuesFromObject(sim, columns, valList);
        var simString = valList.toString();
        var len = searchKeywords.length;

        for(var j=0; j< len; j++) {
            var val = searchKeywords[j].toLowerCase();
            if(len > 1 && val != '' && simString.indexOf(val) != -1){
                responseList.push(sim);
                break;
            }else if(len == 1 && simString.indexOf(val) != -1){
                responseList.push(sim);
            }
        }
    }
    return responseList;
}

var countrySchema = mongoose.Schema({name:{type:String},code:{type:String}},{id:false,toJSON:{virtuals:true}});
exports.country = mongoose.model('country',countrySchema);
exports.country.projection = {name:1,'code':1};
countrySchema.virtual('countryId').get(function(){return this._id.toHexString();});

var currencySchema = mongoose.Schema({name:{type:String}},{id:false,toJSON:{virtuals:true}});
exports.currency = mongoose.model('currency',currencySchema);
exports.currency.projection = {name:1};
currencySchema.virtual('currencyId').get(function(){return this._id.toHexString();});

var enterpriseSchema = mongoose.Schema({accountName:{type:String},country:{type:String},currency:{type:String},accountSubscriptionType:{type:String},accountDescription:{type:String},simList:[simSchema]},{id:true,toJSON:{virtuals:true}});
exports.enterprise = mongoose.model('enterprise',enterpriseSchema);
exports.enterprise.projection = {accountName:1,country:1,currency:1,accountDescription:1,accountSubscriptionType:1,sims:1};

exports.enterprise.getFilters=function(filter, direction ){
    var f = {};
    if( direction == "ASC") direction = 1;
    else direction = -1;
    f[filter]= direction;

    if( filter == "accountName" ) {
        f = { "accountName" : direction };
    }
    return f;
};


var alertConfigSchema = mongoose.Schema({frequency:{interval: {type:String}, time: {type:Number}},trigger:{qualifier: {type:String},percentage: {type:Number}},alertuser:{type:Boolean},alertAccount:{type:Boolean},enterprise:{type: mongoose.Schema.Types.ObjectId, ref: 'enterprise' },possiblevalues:{alertIntervals:[{ "interval" : {type:String}, "time" : {type:Number} }],simAlertUsage:[{ "qualifier" : {type:String}, "percentage" : {type:Number} }]}});

exports.alertConfig = mongoose.model('alertconfig',alertConfigSchema);
userSchema.virtual('type').get(function(){return "AlertConfig";});
exports.alertConfig.projection = {frequency:1,trigger:1,alertuser:1,enterprise:1,possiblevalues:1,alertAccount:1};
exports.alertConfig.setFields = function(alertConfig){
    var o = alertConfig;
    var enterprise = o.enterprise;
    if(alertConfig.interval){
        delete alertConfig.interval._id;
    }
    if(alertConfig.qualifier){
        delete alertConfig.qualifier._id;
    }
    if (enterprise) {
        delete o.enterprise;
        if (enterprise.accountId)
            o.enterprise = exports.ObjectId(enterprise.accountId);
    }
    return o;
};

var zoneCountrySchema = mongoose.Schema({name:{type:String}});
var zoneSchema = mongoose.Schema({name:{type:String},price:{type:String},currency:{type:String},timestamp:{type:String},countries: [zoneCountrySchema]});
var planDetailsSchema = mongoose.Schema({enterprise:{type: mongoose.Schema.Types.ObjectId, ref: 'enterprise'},zones: [zoneSchema]},{id:false,toJSON:{virtuals:true}});
exports.planDetails = mongoose.model('planDetails',planDetailsSchema);
exports.planDetails.projection = {enterprise:1,zones:1};
planDetailsSchema.virtual('planDetailsId').get(function(){return this._id.toHexString();});


var dataUsageSchema = mongoose.Schema({date:{type:Date},dataUsageInMB:{type:Number},zone:{type:String},simId:{type: mongoose.Schema.Types.ObjectId, ref: 'sim' }},{id:false,toJSON:{virtuals:true}});
exports.dataUsage = mongoose.model('datausage',dataUsageSchema);
exports.dataUsage.projection = {date:1,dataUsageInMB:1,simId:1,zone:1,_id:0};

var invoiceSchema = mongoose.Schema({invoiceNumber:{type:String},created:{type:Date},url:{type:String}},{id:false,toJSON:{virtuals:true}}).plugin(mongoosePaginate);
exports.invoices = mongoose.model('invoices',invoiceSchema);
exports.invoices.projection = {invoiceNumber:1,url:1,created:1};
invoiceSchema.virtual('invoiceId').get(function(){return this._id.toHexString();});
exports.invoices.getFilters=function(filter,direction){
    var f = {};
    if( direction == "ASC") direction = 1;
    else direction = -1;
    f[ filter ] = direction;
    if(filter == "generatedDate" ) {
        f = {"created": direction };
    }

    return f;
};

var billingCycleSchema = mongoose.Schema({billingStart:{type:Date},billingEnd:{type:Date}},{id:false,toJSON:{virtuals:true}});
exports.billingCycle = mongoose.model('billingCycle',billingCycleSchema);
exports.billingCycle.projection = {billingStart:1,billingEnd:1};


var alertSchema = mongoose.Schema({occurredTime:{type: Date, default: Date.now},triggerCount: {type: Number, default: 1},accountId:{type:String},accountName:{ type:String},alertType:{type:String},zoneId:{type:String}, zoneName:{ type:String}, simId:{type:String},alertSpecification: {type:{type:String},enable:{type:Boolean},triggerCount:{type:String},limitValue:{type:String},limitType:{type:String},actions: [{type:String}]}},{id:false,toJSON:{virtuals:true}});
exports.alerts = mongoose.model('alerts',alertSchema);
exports.alerts.projection = {accountId:1,alertType:1,zoneId:1, zoneName:1, simId:1,alertSpecification:1};
alertSchema.virtual('alertId').get(function(){return this._id.toHexString();});
alertSchema.virtual('type').get(function(){return 'AlertConfiguration';});
exports.alerts.setFields = function(alert){
    var o = alert;
    return o;
};

var simLocationHistorySchema = mongoose.Schema({connectionType:{type:String},mcc:{type:String},mnc:{type:String},iccId:{type:String},imsi:{type:String},msisdn:{type:String},country:{type:String},networkName:{type:String},locationInfo:{type:String},timeAtGSBackend:{type:String},source:{type:String},connectionTechnologyType:{type:String}},{id:true,toJSON:{virtuals:true}});
exports.simLocationHistory = mongoose.model('simLocationHistory',simLocationHistorySchema);
exports.simLocationHistory.projection = {connectionType:1,mcc:1,mnc:1,iccId:1,imsi:1,msisdn:1,country:1,networkName:1,locationInfo:1,timeAtGSBackend:1,source:1,connectionTechnologyType:1};

var simCdrSchema = mongoose.Schema({cdrUsageType:{type:String},cdrTrafficType:{type:String},mcc:{type:String},mnc:{type:String},dataInBytes:{type:String},recordStartTime:{type:String},recordDurationInSec:{type:String},imsi:{type:String},msisdn:{type:String},uplinkVolume:{type:String},downlinkVolume:{type:String},iccId:{type:String}},{id:true,toJSON:{virtuals:true}});
exports.simCdr = mongoose.model('simCdr',simCdrSchema);
exports.simCdr.projection = {cdrUsageType:1,cdrTrafficType:1,mcc:1,mnc:1,dataInBytes:1,recordStartTime:1,recordDurationInSec:1,imsi:1,msisdn:1,uplinkVolume:1,downlinkVolume:1,iccId:1};

var companyApprovedContactSchema = mongoose.Schema({firstName:{type:String},lastName:{type:String},emailId:{type:String},jobTitle:{type:String},phoneNumber:{type:String}},{id:true,toJSON:{virtuals:true}});
exports.companyApprovedContact = mongoose.model('companyApprovedContact',companyApprovedContactSchema);
exports.companyApprovedContact.projection = {firstName:1,lastName:1,emailId:1,jobTitle:1,phoneNumber:1};
companyApprovedContactSchema.virtual('contactId').get(function(){return this._id.toHexString();});
exports.companyApprovedContact.setFields = function(contact){
    var o = contact;
    delete o.type;
    return o;
};