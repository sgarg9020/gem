mongoimport --db enterprise --collection enterprises --file enterprises.json --jsonArray --upsert --upsertFields accountName
mongoimport --db enterprise --collection countries --file countries.json --jsonArray --upsert --upsertFields name
mongoimport --db enterprise --collection groups --file groups.json --jsonArray --upsert --upsertFields name
mongoimport --db enterprise --collection users --file  users.json --jsonArray --upsert --upsertFields emailId
mongoimport --db enterprise --collection sims --file sims.json --jsonArray --upsert --upsertFields name
mongoimport --db enterprise --collection plandetails --file planDetails.json --jsonArray --upsert --upsertFields zones
mongoimport --db enterprise --collection daywiseusages --file dataUsage.json --jsonArray
mongoimport --db enterprise --collection invoices --file invoices.json --jsonArray --upsert --upsertFields created
mongoimport --db enterprise --collection billingcycles --file billingCycle.json --jsonArray --upsert --upsertFields billingStart
mongoimport --db enterprise --collection simlocationhistories --file simLocationHistory.json --jsonArray
mongoimport --db enterprise --collection simcdrs --file simCdr.json --jsonArray
mongoimport --db enterprise --collection alertconfigs --file alertConfig.json --jsonArray --upsert --upsertFields enterprise

