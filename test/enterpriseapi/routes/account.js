/**
 * Created with JetBrains WebStorm.
 * User: jagadish
 * Date: 05/09/15
 * Time: 11:46 PM
 * To change this template use File | Settings | File Templates.
 */
var express = require('express');
var router = express.Router();
var acc = {};
var db = require('../database.js');

acc.createAccount = function(req,res,next){

};

acc.logIn = function(req,res,next){

   var b = req.body;
   if(b.type!='UserLogin' || !b.emailId || !b.password )
   {
       var err = new Error("Invalid request type");
       next(err);
   }
   else
   {
       db.user.findOne({emailId:b.emailId,password: b.password}).select(db.user.projection).populate(db.user.groupPopulation).exec(function(err,ac){
           if(err)
           {
               next(err);
           }
           else if(!ac){
               err = new Error("Authentication failed");
               err.status = 401;
               next(err);
           }
           else{
               var uuid = require('node-uuid');
               var moment= require('moment');

               var uid = uuid.v1();
               var token = new db.loginToken({token:uid,customerId:ac._id});
               token.save(function(err,t){
                   if(err){
                       next(err);
                   }
                   else
                   res.json({ type:"UserLoginResponse",token:uid,userId:ac._id,expiryDate:moment().add(1,"days").format("YYYY-MM-DD HH:mm:ss"),accountId:ac.emailId});
               });

           }
       });
   }
};

acc.logOut = function(req,res,next){
    var token = req.header("Authorization");
    if(token){
        var p = token.match(/Basic (.*)/);
        token = p[1];
        db.loginToken.remove({token:token},function(err,r){
            if(r)
                res.sendStatus(200);
            else
                res.sendStatus(401);
        });
    }
    else
    {
        var err = new Error("Un authorized");
        err.status = 401;
        next(err);
    }

};

acc.resetPasswordReq = function(req,res,next){
    var b = req.body;
    if(b.type!='ResetPassword' || !b.emailId )
    {
        var err = new Error("Invalid request type");
        next(err);
        return;
    }

    res.sendStatus(200);
};

acc.resetPassword = function(req,res,next){
    var b = req.body;
    var confirmationCode = req.query.confirmationCode;
    if(!b.password || !confirmationCode )
    {
        var err = new Error("Invalid request type");
        next(err);
        return;
    }

    res.sendStatus(200);
};

acc.updatePassword = function (req, res, next) {
    var b = req.body;
    var url = req.originalUrl;
    var val = url.split('/');
    var id = val[ val.length - 2 ];

    db.user.findOne({_id:db.ObjectId(id)}).select(db.user.pwdProjection).exec(function (err, account) {
        if(err)
            return next(err);
        if(account.password != b.oldPassword){
            err = new Error("Invalid credentials");
            next(err);
        }else{
            acc.updateAccount(req,res,next);
        }
    });
}

acc.updateAccount = function (req, res, next) {
    var url = req.originalUrl;
    var val = url.split('/');

    var account = req.body;
    if(account.type == 'AccountPassword'){
       var request = {
           password : account.newPassword
       }
        account = request;
        var id = val[ val.length - 2 ];
    }else{
        var id = val[ val.length - 1 ];
        account = transformUserJsonForUpdate(account);
    }

    db.user.findOneAndUpdate({_id:db.ObjectId(id)},{$set:db.user.setFields(account)},function(err,acc){
        if(err){
            err = new Error("Unable to update account details");
            return next(err);
        }
        res.sendStatus(200);
    });
};

function transformUserJsonForUpdate(account){

    var template = {
        firstName: '$.firstName',
        lastName: '$.lastName',
        emailId: '$.emailId',
        country:'$.address.country',
    };

    return db.transformJson(account, template);
}

acc.confirmToken = function (req, res, next) {
    var b = req.query;
    if(b.tokenType!='CONFIRM_PASSWORD_RESET')
    {
        var err = new Error("Invalid request type");
        next(err);
        return;
    }
    if(b.token == "12345")
    res.sendStatus(200);
    else
    {
        var err = new Error("Invalid Token");
        next(err);
        return;
    }
};
acc.accountLogin = function( req, res, next ){

    var uid = req.params.uid;
    var aid = req.params.aid;

    var uuid = require('node-uuid');
    var moment= require('moment');

    var uid = uuid.v1();
    var token = new db.loginToken({token:uid,customerId:aid});
    token.save(function(err,t){
        if(err){
            next(err);
        }
        else
            res.json({ type:"AccountLoginResponse",token:uid,expiryDate:moment().add(1,"days").format("YYYY-MM-DD HH:mm:ss")});
        res.status(200);
        res.send();
    });

};
//Account Login
router.route('/:uid/account/:aid/login').post(acc.accountLogin);

router.route('/:id/password').put(acc.updatePassword);
router.route('/:id').put(acc.updateAccount);

router.post('/login',acc.logIn);

router.post('/:uid/logout',acc.logOut);

router.post('/password',acc.resetPasswordReq);

router.post('/password/confirm',acc.resetPassword);

router.get('/confirmToken',acc.confirmToken);

router.authenticateAccount = function(req,token,next){


    var token = req.header("Authorization");
    function sendError(){
        var err = new Error("Authentication failed");
        err.status = 401;
        next(err);
        return;
    }
    next();
   /* if(!token)
    {
        sendError();
    }
    token = token.match(/Basic (.*)/);
    if(!token || token.length<=1)
        sendError();

    token = token[1];
    db.loginToken.findOne({token:token},function(err,r){
        if(err || !r)
        {
            return  sendError();
        }
        next();

    });*/
};

module.exports = router;