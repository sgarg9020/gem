/**
 * Created with JetBrains WebStorm.
 * Group: jagadish
 * Date: 05/09/15
 * Time: 11:07 PM
 * To change this template use File | Settings | File Templates.
 */
var express = require('express');
var router = express.Router();

var groups = {};

var db = require('../database.js');


groups.isValidGroupListType = function(listReq,next){
    if( listReq.type != "AccountList"){
        var err = new Error("Type of input object doesn't match to Groups");
        err.status = 400;
        next(err);
        return false;
    }
    return true;
};

groups.isValidGroupType = function(group,next){
    if(group.type != "Account"){
        var err = new Error("Type of input object "+ group.type +" doesn't match to Group");
        err.status = 400;
        next(err);
        return false;
    }
    return true;

};

groups.createGroups = function(req,res,next){

    console.log( "@  createGRoups");
    console.log( req.body );

    if(req.body.list.length  == 1 )
    {
        groups.createGroup(req,res,next);
        return;
    }

    if(!groups.isValidGroupListType(req.body,next))
        return;

    var group = req.body.list;

    if( !groups.isValidGroupType( group, next ) )
        return;
    db.group.create( db.group.setFields( group ), function(err,group){
        console.log(group);
        if(!err)
            res.sendStatus(200);
        else
            next(err);
    });
};

groups.updateGroups = function(req,res,next){
    if(!groups.isValidGroupListType(req.body,next))
        return;

    var bulk = db.group.collection.initializeUnorderedBulkOp();
    var list = req.body.list;
    var count = list.length;
    for(var i=0;i<count;i++)
    {
        var group = list[i];
        if(!groups.isValidGroupType(group,next))
            return;
        var fields = db.group.setFields(group);
        console.log( "fields::");
        console.log( fields );

        bulk.find({"_id":db.ObjectId(group.accountId )}).updateOne({$set:fields });
    }

    bulk.execute(function(err){
        if(!err)
            res.sendStatus(200);
        else
            next(err);
    });
};


groups.deleteGroups = function(req,res,next){
    var list = req.query.aid.split( ",");
    var groupIds  = [];
    for( var i = 0; i < list.length; i++ ){
        groupIds[ i ] = db.ObjectId( list[ i ]);
    }
    db.group.find({}).where("_id").in(groupIds).exec(function (findErr, groupsList) {
        if (!findErr) {

            db.group.remove( { _id:{ $in: groupIds } }).exec( function( remErr,  remRes ) {
                if( !remErr ){

                    groups.updateGroupUsersAndSims(groupIds, res, next);
                }
                else {
                    return next(remErr );
                }
            });
        }
        else {
            next(findErr);

        }
    });

};


groups.getGroups = function(req,res,next){

    var startIndex = req.query.startIndex || 0;
    var count = req.query.count;
    var sortParam = { name:1 };
    var searchQ = req.query.search;

    if(req.query.sortBy !== undefined) {
        var sortParam = db.group.getFilters(req.query.sortBy, req.query.sortDirection);
    }

    db.group.find({}).sort(sortParam).exec(function(err,groupsList){
            if(!err){
                if(searchQ){
                    var columns = ['name'];
                    groupsList = db.searchOnPopulatedFields(groupsList,searchQ,columns);
                }

                var totalCount = groupsList.length;
                if(count){
                    groupsList = groupsList.splice(startIndex, count);
                }

                //added for time being as mongoose virtual fields are not populating
                for(var i in groupsList){
                    groupsList[i].type = 'Group';
                    groupsList[i].groupId =groupsList[i]._id.toHexString();
                }

                var resp = {type:"Groups",startIndex:startIndex,totalCount:totalCount,filteredCount:totalCount,count:groupsList.length,list:groupsList};

                // groups.getUserCount( resp, res, next );
                resp = groups.transformGroupsJson(resp);

                res.json(resp);


            }
            else{
                next(err);
            }
        });
};

groups.transformGroupsJson = function( groupList ){



var template = {
    type: "AccountList",
    totalCount: "$.totalCount",
    count: "$.count",
    list: ['$.list', {
        type: 'Account',
        accountId: '$._doc._id',
        accountName: '$._doc.name'
    }]
};

return  db.transformJson(groupList, template);
};

groups.getGroup = function(req,res,next){
    db.group.findOne({_id:db.ObjectId(req.params.id)},db.group.projection,function(err,group){
        if(err)
            return next(err);
        if(group)
            res.json(group);
        else
            res.sendStatus(404);
    });
};

groups.updateGroup = function(req,res,next){


    var request = req.body;

    if(request.accountType == "Group"){
        var group = request;

        if(!groups.isValidGroupType(group,next))
            return;
        var grpName = group.accountName;
        var fields = db.group.setFields(group);


        db.group.findOneAndUpdate({_id:db.ObjectId(group.accountId )},{$set:fields },function(err,group){
            if(err){
                if(err.code == 11000){
                    err.message = "Group "+ grpName + " already exists";
                }
                return next(err);
            }
            res.sendStatus(200);
        });
    }else if(request.accountType == "Enterprise"){
        var enterprise = request;
        if(enterprise.alertSetting){
            var alertConfig = transformEnterpriseJsonForAlertConfigUpdate(enterprise);

            db.alertConfig.findOneAndUpdate({enterprise:db.ObjectId(req.params.id)},{$set:db.alertConfig.setFields(alertConfig)},{upsert : true,new: true},function(err,response){
                if(err){
                    err = new Error("Unable to update enterprise details");
                    return next(err);
                }
                res.sendStatus(200);
            });
        }else{
            enterprise = transformEnterpriseJsonForUpdate(enterprise);
            db.enterprise.findOneAndUpdate({_id:db.ObjectId(req.params.id)},{$set:db.enterprise.setFields(enterprise)},function(err,response){
                if(err){
                    err = new Error("Unable to update enterprise details");
                    return next(err);
                }
                res.sendStatus(200);
            });
        }
    }
};

function transformEnterpriseJsonForAlertConfigUpdate(enterprise){
    var template = {
        alertAccount: enterprise.alertSetting.alertEnabled,
        alertuser: enterprise.alertSetting.defaultCreditLimitForSims[0].alertUserByMail,
        frequency: {
            time: enterprise.alertSetting.alertIntervalInSecs
        },
        trigger:{
            percentage: enterprise.alertSetting.defaultCreditLimitForSims[0].alertPercentages[0]
        }
    }

    return template;
}

function transformEnterpriseJsonForUpdate(enterprise) {
    var template = {
        address: '$.company.address.address1',
        city: '$.company.address.city',
        companyName: '$.accountName',
        country: '$.company.address.country',
        postalCode: '$.company.address.zipCode',
        state: '$.company.address.state'
    }

    return db.transformJson(enterprise, template);
}

groups.deleteGroup = function(req,res,next){
    db.group.findOneAndRemove({_id:db.ObjectId(req.params.id)},function(err,group){
        if(!err) {
            groups.updateGroupUsersAndSims([db.ObjectId(req.params.id)], res, next);
        }
        else{
            next(err);
        }
    });
};

groups.createGroup = function(req,res,next){

    //var group = req.body;
    var group = db.group.setFields(req.body.list[0]);

    //if(!groups.isValidGroupType(group,next))
      //  return;
    // delete group.type;
    var grpName = group.name;
    (new db.group(group)).save(function(err,group){
        if(err){
            if(err.code == 11000){
                err.message = "Group "+ grpName + " already exists";
            }
            return next(err);
        }

        res.status(200);
        res.json(group);
    });
};



// do update  uesrs & sims collection by replacing the group Id with Parent groupId during the group deletion
groups.updateGroupUsersAndSims = function( groupsList, res, next ) {

    if (groupsList === null) {
        res.sendStatus(200);
    }
    var len = groupsList.length;


    for (var i = 0; i < len; i++) {

        var groupId = groupsList[i];
        var parentGroupId = groupsList[i].parentGroupId;

        db.group.update({parentGroupId: db.ObjectId(groupId)}, {parentGroupId: db.ObjectId(parentGroupId)}, {multi: true}, function (grpErr, grpRes) {

            if (!grpErr) {

                db.user.update({"group": db.ObjectId(groupId)}, {"group": db.ObjectId(parentGroupId)}, {multi: true}, function (userErr, userRes) {

                    if (userErr) {
                        return next(userErr);
                    }
                    else {
                        db.sim.update({"group": db.ObjectId(groupId)}, {"group": db.ObjectId(parentGroupId)}, {multi: true}, function (simErr, simRes) {

                            if (simErr) {

                                return next(simErr);
                            }
                            else {
                                console.log(simRes);
                            }
                        });
                    }
                });
            }
            else {
                return next(grpErr);
            }
        });



    }
    res.sendStatus(200);
};


// get user count
groups.getUserCount = function( resp, res, next  ) {
    var grpList = resp.list;
    var len = grpList.length;
    var grpIds = [];
    // var userResult = [];
    // var simResult = [];

    //console.log( grpList );
    for( var i=0; i < len; i++ ){
        grpIds[ i ] = db.ObjectId( grpList[i]._id );
    }
    db.user.aggregate([
        { $match: {
            group : { $in: grpIds }
        }},
        { $group: {
            _id:"$group",
            count:{ $sum:1  }
        }}
    ], function (err, userResult) {
        if (err) {
            console.log(err);
            return next( err )
            // return;
        }
        var userResult = userResult;
        db.sim.aggregate( [ { $match:{ group:{ $in: grpIds }}}, { $group:{ _id:"$group", count:{ $sum:1 } } } ],
            function( err, simResult ) {
                if( err ) {
                    console.log( err );
                    return next( err );
                }
               // console.log( "sim Result ");
                // console.log( simResult);
                var userCountLen = userResult.length;
                for( var k=0; k < userCountLen;  k++ ){
                    checkPresence( userResult[k], "userCount" );
                }

                function checkPresence( userC, itemName){
                    for( var l =0; l < grpIds.length; l++ ){

                        if( grpIds[l].toHexString() == userC._id.toHexString() ){
                            grpList[l].set( itemName, userC.count, {strict: false} );

                           // var val = grpList[l].toObject();
                           // grpList[l].itemName = userC.count;
                           // grpList[l] = val;
                            break;
                        }
                        else {
                            // var v = grpList[l].toObject();
                            //v[itemName] = 10;
                            //grpList[l] = v;
                            //console.log( "No match found for ::" + grpList[l]);

                        }
                    }

                }

                var simCountLen = simResult.length;
                for( var m=0; m < simCountLen; m++ ) {
                    checkPresence( simResult[m], "simCount");
                }
                resp.list = grpList;
                res.json(resp);

            }
        );
    });



};

/* GET groups listing. */
//router.route('/:id/accounts').get(groups.getGroups);
router.route( "/").post( groups.createGroups).delete( groups.deleteGroups );
router.route( "/:id").put( groups.updateGroup );


module.exports = router;
