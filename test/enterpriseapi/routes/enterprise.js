var express = require('express');
var router = express.Router();
var db = require('../database.js');

var enterprises = {};

enterprises.getEnterprise = function (req, res, next) {
    var enterpriseId = req.params.id;
    db.enterprise.findOne({_id: db.ObjectId(enterpriseId)}).select(db.enterprise.projection).exec(function (err, enterpriseResponse) {
        if(err){
            err = new Error("Unable to get enterprise details");
            return next(err);
        }else{

            db.alertConfig.findOne({enterprise: db.ObjectId(enterpriseId)}).select(db.alertConfig.projection).populate(db.user.enterprisePopulation).exec(function (err, alertConfig){
                if(err){
                    err = new Error("Unable to get billing cycle details");
                    return next(err);
                }else{
                    delete alertConfig._doc.possiblevalues;

                    enterpriseResponse._doc.alertConfig = alertConfig;
                    enterpriseResponse = transformEnterpriseJson(enterpriseResponse);
                    var list = [enterpriseResponse.alertSetting.defaultCreditLimitForSims];
                    enterpriseResponse.alertSetting.defaultCreditLimitForSims = list;
                    res.json(enterpriseResponse);

                }
            });


        }

    });
};

enterprises.getEnterprises = function (req, res, next) {
    var enterpriseId = req.params.id;
    var startIndex = req.query.startIndex || 0;
    var count = req.query.count;
    var sortParam = { accountName:1 };
    var searchQ = req.query.search;
    var searchBy = req.query.searchBy;

    if(req.query.sortBy !== undefined) {
        var sortParam = db.enterprise.getFilters(req.query.sortBy, req.query.sortDirection);
    }

    db.enterprise.find({}).sort(sortParam).exec(function(err,enterpriseList){
        if(!err){
            /*if(searchQ){
                var columns = [searchBy];
                enterpriseList = db.searchOnPopulatedFields(enterpriseList,searchQ,columns);
            }
*/
            var totalCount = enterpriseList.length;
            if(count){
                enterpriseList = enterpriseList.splice(startIndex, count);
            }
            var resp = {startIndex:startIndex,totalCount:totalCount,count:enterpriseList.length,list:enterpriseList};
            resp = transformSubAccountsJson(resp);

            res.json(resp);


        }
        else{
            next(err);
        }
    });
}

function transformSubAccountsJson(resp){
    var template = {
        type: "AccountList",
        totalCount: "$.totalCount",
        count: "$.count",
        list: ['$.list', {
            type: 'Account',
            accountId: '$._doc._id',
            accountName: '$._doc.accountName',
            accountDescription: '$._doc.accountDescription',
            accountSubscriptionType: '$._doc.accountSubscriptionType',
            country: "$._doc.country",
            currency: "$._doc.currency",
            simList: ['$._doc.simList',{
                iccid : "$._doc.iccId",
                imsi : "$._doc.imsi",
                msisdn : "$._doc.msisdn",
            }]
        }]
    };

    var resp =  db.transformJson(resp, template);
    return resp;
}

function transformEnterpriseJson(res) {

    var template = {
        type: 'Account',
        accountType: 'Enterprise',
        accountId: '$._doc._id',
        accountName: '$._doc.companyName',
        accountDescription: "Desc",
        currency: "USD",
        parentAccountId: "",
        company: {
            type: "Company",
            name: '$._doc.companyName',
            details: "abcd",
            currency: "USD",
            timeZone: "+01:00",
            address: {
                type: "Address",
                city: "$._doc.city",
                address1: "$._doc.address",
                address2: "",
                zipCode: "$._doc.postalCode",
                phone: "",
                state: "$._doc.state",
                country: "$._doc.country"
            }
        },
        alertSetting : {
            type: "alertSetting",
            alertEnabled: '$._doc.alertConfig._doc.alertAccount',
            alertIntervalInSecs: '$._doc.alertConfig._doc.frequency.time',
            defaultCreditLimitForSims : ['',
                {
                    type                :   "creditLimit",
                    limitType           :    "SOFT",
                    limitKB             :   "1024",
                    limitCost           :   "50"  ,
                    limitDuration       :   "MONTHLY",
                    alertUserByMail     :  '$._doc.alertConfig._doc.alertuser',
                    alertEnabled        :   '$._doc.alertConfig._doc.alertAccount',
                    limitDescription    :   'Account Monthly Data Limit',
                    alertPercentages    :   ['$._doc.alertConfig._doc.trigger.percentage']

        }]

        }

};

    return db.transformJson(res, template);
}

enterprises.getBillingCycle = function (req, res, next) {

    db.billingCycle.find({}).select(db.billingCycle.projection).exec(function (err, billingCycle) {
        if(err){
            err = new Error("Unable to get billing cycle details");
            return next(err);
        }else{
            var response = {
                billingStart : new Date(billingCycle[0].billingStart.getUTCFullYear(), billingCycle[0].billingStart.getUTCMonth(), billingCycle[0].billingStart.getUTCDate(),  billingCycle[0].billingStart.getUTCHours(), billingCycle[0].billingStart.getUTCMinutes(), billingCycle[0].billingStart.getUTCSeconds()),
                billingEnd : new Date(billingCycle[0].billingEnd.getUTCFullYear(), billingCycle[0].billingEnd.getUTCMonth(), billingCycle[0].billingEnd.getUTCDate(),  billingCycle[0].billingEnd.getUTCHours(), billingCycle[0].billingEnd.getUTCMinutes(), billingCycle[0].billingEnd.getUTCSeconds()),
            }
            res.json(response);
        }
    });

};

enterprises.getUserAccount =function (req, res, next) {
    db.user.find({_id:db.ObjectId(req.params.id)}).select(db.user.projection).populate([{path: 'accounts', model: 'enterprise'}]).exec(function(err,users) {
        if (err) {
            err = new Error("Unable to get user details");
            return next(err);
        }
        var user = users[0]._doc;
        var response = {
            type: "User",
            userId: user._id,
            firstName: user.firstName,
            lastName: user.lastName,
            emailId: user.emailId,
            location: user.location,
            accountDetails: {
                type: "EnterpriseAccountList",
                "enterpriseAccountDetails": []
            }
        };
        var i=0;

        for(i=0;i<user.accounts.length;i++){
            var enterpriseAccountDetail ={type:"Accounts"};
            var account = user.accounts[i]._doc;
            enterpriseAccountDetail.parentAccountDetails =
                [{
                    type: "Account",
                    accountId: account._id,
                    accountName: account.companyName,
                    accountType: "ENTERPRISE",
                    role: "ENTERPRISE_ADMIN"
                }];
            response.accountDetails.enterpriseAccountDetails.push(enterpriseAccountDetail);
        }

        res.json(response);
    });
};
router.route('/user/:id').get(enterprises.getUserAccount);

enterprises.isValidApprovedConatctListType = function(listReq,next){
    if(listReq.type !="AddOrUpdateApprovedContactRequest"){
        var err = new Error("Type of input object doesn't match to account approved contact list");
        err.status = 400;
        next(err);
        return false;
    }
    return true;
};

enterprises.isValidContactType = function(contact,next){

    if(contact.type!="ApprovedContactInfo"){
        var err = new Error("Type of input object "+ contact.type +" doesn't match to ApprovedContactInfo");
        err.status = 400;
        next(err);
        return false;
    }
    return true;

};


enterprises.addApprovedContacts = function (req, res, next) {
    if(!enterprises.isValidApprovedConatctListType(req.body,next)) {
        return;
    }
    db.companyApprovedContact.create(req.body.list,function(err,contacts){
        if(err){
            err = new Error("Unable to create contacts");
            return next(err);
        }
        res.sendStatus(200);
    });
};

enterprises.getApprovedContacts = function (req, res, next) {
    var startIndex = req.query.startIndex || 0;
    var count = req.query.count;

    db.companyApprovedContact.find({}).select(db.companyApprovedContact.projection).exec(function (err, contacts) {
        if (err)
            return next(err);

        var totalCount = contacts.length;
        if(count){
            contacts = contacts.splice(startIndex, count);
        }

        var resp = {type:"AccountApprovedContactResponse",startIndex:startIndex,totalCount:totalCount,count:contacts.length,list:contacts};
        res.status(200);
        res.json(resp);
    });
}

enterprises.updateContacts = function (req, res, next) {

    if(req.body.type !="AddOrUpdateApprovedContactRequest"){
        var err = new Error("Type of input object doesn't match to account approved contact list");
        err.status = 400;
        next(err);
        return false;
    }

    var bulk = db.companyApprovedContact.collection.initializeUnorderedBulkOp();
    var list = req.body.list;
    var count = list.length;

    for(var i=0;i<count;i++){
        var contact = list[i];
        if(!enterprises.isValidContactType(contact,next))
            return;
        delete contact._id;
        var fields = db.companyApprovedContact.setFields(contact);


        bulk.find({_id:db.ObjectId(contact.contactId)}).updateOne({$set:fields });
    }

    bulk.execute(function(err){
        if(err){
            err = new Error("Unable to update contacts");
            return next(err);
        }else{
            res.sendStatus(200);
        }

    });
};

enterprises.deleteContacts = function (req, res, next) {
    if(req.body.type !="DeleteApprovedContactRequest"){
        var err = new Error("Type of input object doesn't match to account approved contact list");
        err.status = 400;
        next(err);
        return false;
    }

    var list = req.body.list;
    var emailIds = [];
    for( var i = 0; i < list.length; i++ ){
        emailIds[ i ] = list[i].emailId;
    }

    var contactId = req.params.contactId;
    contactId = db.ObjectId(contactId);

    db.companyApprovedContact.remove( { emailId:{ $in: emailIds } }).exec( function( remErr,  remRes ) {
        if( !remErr ) {
            res.sendStatus( 200 );
        }
        else {
            remErr = new Error("Unable to delete users");
            return next( remErr );
        }

    });
};

/*
enterprises.updateAccount = function (req, res, next) {
    var enterprise = req.body;
    if(enterprise.alertSetting){
        var alertConfig = transformEnterpriseJsonForAlertConfigUpdate(enterprise);

        db.alertConfig.findOneAndUpdate({enterprise:db.ObjectId(req.params.id)},{$set:db.alertConfig.setFields(alertConfig)},{upsert : true,new: true},function(err,response){
            if(err){
                err = new Error("Unable to update enterprise details");
                return next(err);
            }
            res.sendStatus(200);
        });
    }else{
        enterprise = transformEnterpriseJsonForUpdate(enterprise);
        db.enterprise.findOneAndUpdate({_id:db.ObjectId(req.params.id)},{$set:db.enterprise.setFields(enterprise)},function(err,response){
            if(err){
                err = new Error("Unable to update enterprise details");
                return next(err);
            }
            res.sendStatus(200);
        });
    }

};

function transformEnterpriseJsonForAlertConfigUpdate(enterprise){
    var template = {
        alertAccount: enterprise.alertSetting.alertEnabled,
        alertuser: enterprise.alertSetting.defaultCreditLimitForSims[0].alertUserByMail,
        frequency: {
            time: enterprise.alertSetting.alertIntervalInSecs
        },
        trigger:{
            percentage: enterprise.alertSetting.defaultCreditLimitForSims[0].alertPercentages[0]
        }
    }

    return template;
}

function transformEnterpriseJsonForUpdate(enterprise) {
    var template = {
        address: '$.company.address.address1',
        city: '$.company.address.city',
        companyName: '$.accountName',
        country: '$.company.address.country',
        postalCode: '$.company.address.zipCode',
        state: '$.company.address.state'
    }

    return db.transformJson(enterprise, template);
}



enterprises.updateAlertConfig = function(req, res, next){
    var alertConfig = req.body;
    if (!req.params.id) {
        req.params.id = new mongoose.mongo.ObjectID();
    }
    db.alertConfig.findOneAndUpdate({enterprise:db.ObjectId(req.params.id)},{$set:db.alertConfig.setFields(alertConfig)},{upsert : true,new: true},function(err,response){
        if(err){
            err = new Error("Unable to update enterprise details");
            return next(err);
        }
        res.sendStatus(200);
    });
};
enterprises.getAlertConfig = function (req, res, next) {
    db.alertConfig.findOne({enterprise: db.ObjectId(req.params.id)}).select(db.alertConfig.projection).populate(db.user.enterprisePopulation).exec(function (err, alertConfig){
        if(err){
            err = new Error("Unable to get billing cycle details");
            return next(err);
        }else{
            delete alertConfig._doc.possiblevalues;
            res.json(alertConfig);
        }
    });

};
enterprises.getPossibleAlertConfigs = function (req, res, next) {
    db.alertConfig.findOne({enterprise: db.ObjectId(req.params.id)}).select(db.alertConfig.projection).populate(db.user.enterprisePopulation).exec(function (err, alertConfig) {
        if(err){
            err = new Error("Unable to get billing cycle details");
            return next(err);
        }else{
            res.json(alertConfig.possiblevalues);
        }
    });
}

router.route('/possibleAlertConfig/:id').get(enterprises.getPossibleAlertConfigs);
router.route('/alertconfig/:id').put(enterprises.updateAlertConfig).get(enterprises.getAlertConfig);
router.route('/:id').put(enterprises.updateAccount);*/

router.route('/billingcycle').get(enterprises.getBillingCycle);
router.route('/:id').get(enterprises.getEnterprise);

router.route('/:accountId/accounts').get(enterprises.getEnterprises);
router.route('/:accountId/approvedContacts').get(enterprises.getApprovedContacts).post(enterprises.addApprovedContacts).put(enterprises.updateContacts).delete(enterprises.deleteContacts);

module.exports = router;
