/**
 * Created with JetBrains WebStorm.
 * User: jagadish
 * Date: 05/09/15
 * Time: 11:11 PM
 * To change this template use File | Settings | File Templates.
 */
/**
 * Created with JetBrains WebStorm.
 * sims: jagadish
 * Date: 05/09/15
 * Time: 11:07 PM
 * To change this template use File | Settings | File Templates.
 */
var express = require('express');
var router = express.Router();

var db = require('../database.js');

var sims = {};
sims.isValidSIMListType = function(listReq,next){
  if(listReq.type !="SimList"){
      var err = new Error("Type of input object doesn't match to SIMs");
      err.status = 400;
      return next(err);
  }
  return true;
};

sims.isValidSIMListStatusType = function( listReq, next ){
    if(listReq.type != "SimStatusList"){
        var err = new Error("Type of input object doesn't match to SimStatusList");
        err.status = 400;
        return next(err);
    }
    return true;

}

sims.isValidSIMStatusType = function(sim,next){

    if(sim.type!="SimStatus"){
        var err = new Error("Type of input object "+ sim.type +" doesn't match to SimStatus");
        err.status = 400;
        return next(err);

    }
    return true;

};

sims.isValidSIMType = function(sim,next){

    if(sim.type!="Sim"){
        var err = new Error("Type of input object "+sim.type +" doesn't match to SIM");
        err.status = 400;
        return next(err);

    }
    return true;

};

sims.createSIMs = function (req, res, next) {
    if (req.body.type == "SIM") {
        sims.createSIM(req, res, next);
        return;
    }

    if(!sims.isValidSIMListType(req.body,next))
    return;

    db.sim.create(req.body.list, function (err, sims) {
        if(err)
         return next("Unable to create the SIM!");
        res.status(200);
    });

};

sims.updateSIMs = function (req, res, next) {
    if (!sims.isValidSIMListType(req.body, next))
        return;

    var list = req.body.list;
    var count = list.length;
    var bulk = db.sim.collection.initializeUnorderedBulkOp();
    var group = null;
    var iccIds = [];

    for(var i = 0; i < count; i++) {
        var sim = list[i];
        if (!sims.isValidSIMType(sim, next))
            return;
        delete sim._id;
        sim = db.sim.setFields(sim);
        var iccId = sim.iccId;
        iccIds.push( iccId );

        delete sim.iccId;

        if( sim.group === null ) {
            sim.user = null;
        }
        bulk.find({iccId:iccId}).updateOne({$set:sim });
    }

    bulk.execute(function (err){
        if (err) {
            err = new Error("Unable to update sims");
            return next(err);
        } else {
            sims.updateGroupOfUsers( iccIds, sim.user, res, next );
        }
    });

};

sims.updateGroupOfUsers = function( iccIds, userId, res, next ){

    var group = null;
    if( userId !== null ) {
        db.user.findOne({"_id": userId}).exec(function (userErr, userRes) {
            if (userErr) {
                return next("Unable to update sims");
            }

            group = userRes ? userRes.group : null;
            db.sim.update({"iccId": {$in: iccIds}}, {$set: {group: group}}, {multi: true}, function (simErr, res) {

                if (simErr) {
                    simErr = new Error("Unable to update sims");
                    return next(simErr);
                }

            });
        });
    }
    else {
    }

    res.sendStatus(200);

};


sims.updateSIMsStatus = function( req, res, next ) {

    if(!sims.isValidSIMListStatusType(req.body,next))
        return;
    var bulk = db.sim.collection.initializeUnorderedBulkOp();
    var list = req.body.list;
    var count = list.length;

   for(var i=0;i<count;i++){
        var sim = list[i];
        if(!sims.isValidSIMStatusType(sim,next))
            return;

        bulk.find({ iccId:sim.simId }).updateOne({$set:db.sim.setFields(sim)});

    }
    bulk.execute(function (err) {
        if(err)
            return next("Unable to update the SIMs!");
        res.sendStatus(200);
    });


};

sims.deleteSIMs = function (req, res, next) {

     if(!sims.isValidSIMListType(req.body,next))
        return;

    var bulk = db.sim.collection.initializeUnorderedBulkOp();
    var list = req.body.list;
    var count = list.length;

    for(var i=0;i<count;i++){
        var sim = list[i];
        if(!sims.isValidSIMType(sim,next))
            return;
        bulk.find({_id:db.ObjectId(sim.simId)}).removeOne();
    }

    bulk.execute(function(err){
        if(err)
            return next( "Unable to delete the SIMs!!");
        res.sendStatus(200);
    });
};

sims.getSIMs = function (req, res, next) {
    var startIndex = req.query.startIndex || 0;
    var count = req.query.count;
    var searchQ = req.query.search;
    var sortParam = { simId:1 };
    var sortDirection = "DESC";

    var groupNameSortKey = userFNameSortKey = userLNameSortKey =1;

    if( req.query.sortBy !== undefined) {

        sortParam = db.sim.getFilters(req.query.sortBy, req.query.sortDirection );

          if( sortParam['group.name'] ) {
            if (sortParam['group.name'] == -1) {
                groupNameSortKey = -1;
            }
        }
        if( sortParam['user.firstName']) {
            if (sortParam['user.firstName'] == -1) {
                userFNameSortKey = -1;
            }
        }
        if( sortParam['user.lastName'] ) {
            if (sortParam['user.lastName'] == -1) {
                userLNameSortKey = -1;
            }
        }

    }

    db.sim.find({}).sort(sortParam).select(db.sim.projection).populate([db.sim.userPopulation,db.user.groupPopulation]).exec(function(err,simList){
        if(!err){
            if(searchQ){
                var columns = ['status','user','name','iccId','group','firstName','lastName'];
                simList = db.searchOnPopulatedFields(simList,searchQ,columns);
            }
            if( sortParam['group.name']  ){
                simList = db.sortOnPopulatedFields( simList, 'group', 'name', groupNameSortKey );
            }
            if( sortParam['user.firstName'] ){
                simList = db.sortOnPopulatedFields( simList, 'user', 'firstName', userFNameSortKey );

            }

            if( sortParam['user.lastName'] ){
                simList = db.sortOnPopulatedFields( simList, 'user', 'lastName', userLNameSortKey );
            }

            var totalCount = simList.length;
            if(!sortParam['usage'] && count){
               simList = simList.splice(startIndex, count);
            }

            var len= simList.length;
            var endDate, billingDate;
            var simIds = [];
            var iccIds=[]

            // get the usage data from simDataUsage collection
            for( var i=0; i < len; i++ ){
                simIds[ i ] =  db.ObjectId( simList[i]._id );
                iccIds[i] = { "simId": simList[i]._id, "iccId":simList[i].iccId};

                if( simList[i].group === null || simList[i].group === undefined ) {
                    simList[i].set( "accType", "ENTERPRISE", { strict:false});

                }
                else {

                    simList[i].set( "accType", "GROUP", { strict:false});
                }
                var status =  simList[i].status.toUpperCase();
                simList[i].set( "status", status, { strict:false } );


                simList[i].set( "usage",0, {strict: false} );
            }
            // get the current billing cycle
            db.billingCycle.find({}).select(db.billingCycle.projection).exec(function (err, billingCycle) {
                if (err) {
                    err = new Error("Unable to get billing cycle details");
                    return next(err);
                } else {
                    billingDate = billingCycle[0];

                    // check whether billing end date is greater than today's date
                    if (billingDate.billingEnd.getDate() > new Date().getDate()) {
                        endDate = new Date();

                    }
                    else {
                        endDate = new Date(billingDate.billingEnd);
                    }

                    var startDate = new Date( billingDate.billingStart );  //new Date("Wed Oct 21 2015 00:24:15 GMT+0530 (IST)"); // .toISOString();

                    // aggregate query to get the data usage
                     db.dataUsage.aggregate([
                     { $match: {
                     date: {'$gte': startDate,'$lte':endDate  },
                     simId : { $in: simIds }
                     }},
                     { $group: {
                     _id: "$simId",
                     dataUsage:{ $sum: '$dataUsageInMB'},
                     }
                     }
                     ], function (err, dataUsage ) {
                     if (err) {
                         return next( "Unable to get the data usage details!")
                     }


                     var resLen = dataUsage.length;

                     for( var k=0; k < resLen; k++ ){
                        checkPresence( dataUsage[k] );
                     }

                     function checkPresence( dataUsage ){
                         for( var l =0; l < simIds.length; l++ ){

                             if( simIds[l].toHexString() == dataUsage._id.toHexString() ){

                                 simList[l].set( "usage", dataUsage.dataUsage, {strict: false} );

                                 return true;
                             }
                     }
                     }

                     if(sortParam['usage'])
                     {
                         sortByKey(simList,"usage",sortParam['usage']);
                         simList = simList.splice(startIndex, count);
                     }
                     function sortByKey(array, key,sortOrder) {
                         return array.sort(function(a, b) {
                             var x = a._doc[key]; var y = b._doc[key];
                             if(x===undefined) x = 0;
                             if(y===undefined) y = 0;
                                return ((x > y) ? sortOrder : ((x < y) ? -sortOrder: 0));

                         });
                     }

                    var resp = {type:"SIMs",startIndex:startIndex,totalCount:totalCount,filteredCount:totalCount,count:simList.length,list:simList};

                    resp = sims.transformSIMsJson(resp);

                    res.json( resp );
                });
            }
        });
        }
        else{
            return next( "Unable to get the SIM details!");
        }

    });
};


sims.transformSIMJson = function( resp ){
    var template = {

            type: 'Sim',
            simId: '$._doc.iccId',
            status: '$._doc.status',
            dataUsedInBytes: '$._doc.usage',
            nickName: '$._doc.name',
            user: {
                userId: '$._doc.user._doc._id',
                firstName: '$._doc.user._doc.firstName',
                lastName: '$._doc.user._doc.lastName'
            },
            account: {
                type: "$._doc.accType",
                accountId: '$._doc.group._doc._id',
                accountName: '$._doc.group._doc.name'
            },

             creditLimitList: {
                type: 'creditLimitList',
                list:[ '', {
                    type: 'creditLimit',
                    limitKB: '$._doc.allocation'
                }]

            }

    };

    resp  =  db.transformJson(resp, template);
    var sim = new Object(resp );
    var creditList = [sim.creditLimitList.list];
    sim.creditLimitList.list = creditList;
    resp  = sim;

    return resp;

}

sims.transformSIMsJson=function(resp){

    var template = {
        type: "SimList",
        totalCount: "$.totalCount",
        count: "$.count",
        list: ['$.list', {
            type: 'Sim',
            simId: '$._doc.iccId',
            status: '$._doc.status',
            dataUsedInBytes: '$._doc.usage',
            nickName: '$._doc.name',
            user: {
                userId: '$._doc.user._doc._id',
                firstName: '$._doc.user._doc.firstName',
                lastName: '$._doc.user._doc.lastName'
            },
            account: {
                type: "$._doc.accType",
                accountId: '$._doc.group._doc._id',
                accountName: '$._doc.group._doc.name'
            }

            , creditLimitList: {
                type: 'creditLimitList',
                list:[ '', {
                    type: 'creditLimit',
                    limitKB: '$._doc.allocation'
                }]

            }
        }]
    };

    resp  =  db.transformJson(resp, template);
    for( var i=0; i < resp.list.length; i++ ) {
        var sim = new Object(resp.list[i]);
        var creditList = [sim.creditLimitList.list];
        sim.creditLimitList.list = creditList;
        resp.list[i] = sim;
    }
    return resp;
};

sims.getSIM = function (req, res, next) {

    db.sim.findOne({_id:db.ObjectId(req.params.id)}).select(db.sim.projection).populate([db.user.groupPopulation,db.sim.userPopulation]).lean().exec(function(err,sim){
        if(err)
            return next(err);
        //res.json(sim);
        // get the usage for given SIM
        var endDate, billingDate;

        // get the current billing cycle
        db.billingCycle.find({}).select(db.billingCycle.projection).exec(function (err, billingCycle) {
            if (err) {
                err = new Error("Unable to get billing cycle details");
                return next(err);
            }
            else {
                billingDate = billingCycle[0];

                // check whether billing end date is greater than today's date
                if (billingDate.billingEnd.getDate() > new Date().getDate()) {
                    endDate = new Date();
                }
                else {
                    endDate = new Date(billingDate.billingEnd);
                }

                var startDate = new Date( billingDate.billingStart );

                    // aggregate query to get the data usage
                    db.dataUsage.aggregate([
                        { $match: {
                            date: {'$gte': startDate,'$lte':endDate  },
                            simId : db.ObjectId( req.params.id)
                        }},
                        { $group: {
                            _id: "$simId",
                            dataUsage:{ $sum: '$dataUsageInMB'},
                        }}
                    ], function (err, dataUsage ) {
                        if (err) {
                            return next("Unable to get the SIM Data Usage");
                        }
                        sim.usage = ( dataUsage[0] && dataUsage[0].dataUsage ? dataUsage[0].dataUsage : 0 );
                        sim.type = 'SIM';
                        sim.simId = sim._id.toHexString();

                        sim.accType = ( ( sim.group === undefined || sim.group === null ) ? "ENTERPRISE" : "GROUP" );



                        sim = sims.transformSIMJson( sim );
                        res.send( sim );
                        });
                    }
                });
    });
};

sims.getSIMsByUserId = function (req, res, next) {

    db.sim.find({user:db.ObjectId(req.params.id)}).select(db.sim.generalInfoProjection).exec(function(err,sim){
        if(err){
            err = new Error("Error while fetching simList associated to logged in user");
            return next(err);
        };
        res.json(sim);
    });
};

sims.updateSIM = function (req, res, next) {
    console.log( "im @ updateSIM");

    var sim = req.body;
    if(!sims.isValidSIMType(sim,next))
        return;

    db.sim.findOneAndUpdate({_id:db.ObjectId(req.params.id)}, {$set:db.sim.setFields(sim)}, function (err, numAffected) {
        if(err){
            err = new Error("Unable to update SIM");
            return next(err);
        }
        res.sendStatus(200);
    });
};

sims.deleteSIM = function (req, res, next) {
    db.sim.findOneAndRemove({_id:db.ObjectId(req.params.id)}, function (err, sim) {
        if(err)
            return next(err);
        res.sendStatus(200);
    });
};

sims.createSIM = function (req, res, next) {
    var sim = req.body;
    if(!sims.isValidSIMType(sim,next))
        return;
    sim = db.sim.setFields(sim);
    delete sim.type;

    (new db.sim(sim)).save(function (err, sim) {
        if(err)
            return next(err);
        res.sendStatus(200);

    });
};

sims.assignSIMS = function(req,res,next){
    db.sim.find({}).exec(function(err,simList){
        db.user.find({}).exec(function(err,usersList){
           var i= 0;
           while(i<simList.length&&i<usersList.length){
               db.sim.update({_id: simList[i]._id},{user:usersList[i]._id,group:usersList[i].group},{},function(er,resp){
                   if (er) {
                       return next(er);
                   }
               });
               i++;
           }
           res.sendStatus(200);
        });
    });
};

sims.generateUsage = function(req,res,next){
    db.sim.find({}).exec(function(err,simList) {
        var i = 0;
        var start = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
        var end = new Date();
        while (i < simList.length) {
            var usage = {};
            usage.simId = db.ObjectId(simList[i]._id);
            usage.date = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
            usage.dataUsageInMB = Math.random() * 10000 ;
            usage.zone = 'zone ' + (Math.floor(Math.random() * (10 - 1 + 1)) + 1);

            (new db.dataUsage(usage)).save(function (err) {
                if (err)
                    return next(err);

            });
            i++;
    }
            res.sendStatus(200);

    });
};

sims.getSIMDataUsageDayWise = function( req, res, next ){

    var startDate = new Date( req.query.start );
    var endDate = new Date( req.query.end );

    db.dataUsage.aggregate([
        { $match: {
            date: {'$gte': startDate,'$lte':endDate  },
            simId : db.ObjectId(req.params.id)//[ db.ObjectId( "563c7e4823316c2ff401d538"), db.ObjectId( "563c7e4823316c2ff401d536")] }
        }},
        {
            $group: {
               // _id: "$simId",
                _id: { $dateToString: { format: "%Y-%m-%d", date: "$date" } },
                dataUsage: {$sum: '$dataUsageInMB'},
            }},
        {$sort:{

                _id:1



            }
        }
    ], function (err, result) {
        if (err) {
            return next("Unable to retrive the SIM Data Usgae!!");
        } else {
            var resp = {company: db.ObjectId(req.params.id),values: result};
            res.json(resp);
        }
    });
}

sims.generateUsagePost = function(req,res,next) {

    var list = req.body.list;
    var count = list.length;
    var i = 0;

    function insertUsage(err, dataUsage) {
        if (err) {
            res.status(500);
            res.send();
        }
        if (list.length) {
            var data = list.shift();
            db.sim.find({iccId: data.iccId}).exec(function (err, simList) {
                var usage = {};
                usage.simId = db.ObjectId(simList[0]._id);
                usage.date = new Date(data.date);
                usage.dataUsageInMB = data.usage;
                usage.zone = data.zone;
                (new db.dataUsage(usage)).save(insertUsage);
            });
        }
        else
        {
            res.status(200);
            res.send();
        }
    }
    insertUsage();
};

sims.getSimsLocationHistory = function(req,res,next) {

    var startIndex = req.query.startIndex || 0;
    var count = req.query.count;

    db.simLocationHistory.find({}).select(db.simLocationHistory.projection).exec(function (err, locationHistoryList) {
        if (err)
            return next(err);

        var totalCount = locationHistoryList.length;
        if(count){
            locationHistoryList = locationHistoryList.splice(startIndex, count);
        }

        var resp = {startIndex:startIndex,totalCount:totalCount,count:locationHistoryList.length,simType: "GIGSKY_SIM",list:locationHistoryList};
        res.status(200);
        res.json(resp);
    });
};

sims.getSimsCdrs = function(req,res,next) {

    var startIndex = req.query.startIndex || 0;
    var count = req.query.count;

    db.simCdr.find({}).select(db.simCdr.projection).exec(function (err, cdrList) {
        if (err)
            return next(err);

        var totalCount = cdrList.length;
        if(count){
            cdrList = cdrList.splice(startIndex, count);
        }

        var resp = {startIndex:startIndex,totalCount:totalCount,count:cdrList.length,list:cdrList};
        //resp = transformCountriesJson(resp);
        res.status(200);
        res.json(resp);
    });
};

/* GET sims listing. */
router.route('/').get(sims.getSIMs).put(sims.updateSIMs).post(sims.createSIMs).delete(sims.deleteSIMs);
router.route('/assign').get(sims.assignSIMS);
router.route('/generateUsage').get(sims.generateUsage);
router.route( '/dataUsage/:id').get( sims.getSIMDataUsageDayWise );
router.route('/generateUsage').post(sims.generateUsagePost);
//router.route('/[0-9]*').get(sims.getSIM).put(sims.updateSIM).delete(sims.deleteSIM);

router.route('/sim/:iccId/locations').get(sims.getSimsLocationHistory);
router.route('/sim/:iccId/cdrs').get(sims.getSimsCdrs);


// sims status update
router.route( "/status").put( sims.updateSIMsStatus);




module.exports = router;
