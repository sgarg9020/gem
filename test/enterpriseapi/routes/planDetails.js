var express = require('express');
var router = express.Router();
var db = require('../database.js');

var planDetails = {};

planDetails.getPlans = function (req, res, next) {

    var url = req.originalUrl;
    var val = url.split('/');
    var id = val[ val.length - 2 ];

    db.planDetails.findOne({enterprise: db.ObjectId(id)}).select(db.planDetails.projection).exec(function (err, details) {
        if(err){
            err = new Error("Error while fetching plan details");
            return next(err);
        };
        details = transformPlanDetailsJson(details);
        res.json(details);
    });
};

function transformPlanDetailsJson(res){

    var template = {
        type: 'AccountZoneList',
        list: ['$._doc.zones', {
            type : 'ZoneInfo',
            zoneId : '1234',
            name : '$._doc.name',
            mccmncList : ['',
                {
                    type : 'mccmnc',
                    mcc : '300',
                    mnc : '02'
                }
            ],
            countryList : ['$._doc.countries',{
                type:"country",
                name: '$._doc.name',
                code: '$._doc.name'
            }],
            pricePerMB : '$._doc.price',
            lastUpdatedDate : '$._doc.timestamp'
        }]

    };

    return db.transformJson(res, template);
}

router.route('/').get(planDetails.getPlans);

module.exports = router;
