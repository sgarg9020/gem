var express = require('express');
var router = express.Router();

var db = require('../database.js');

var commonApis = {};

commonApis.getCountryList = function (req, res, next) {

    db.country.find({}).select(db.country.projection).exec(function (err, countryList) {
        if (err)
            return next(err);

        var resp = {totalCount:countryList.length,count:countryList.length,list:countryList};
        resp = transformCountriesJson(resp);
        res.status(200);
        res.json(resp);
    });

};

function transformCountriesJson(res){
    var template = {
        type : 'countryList',
        totalCount:'$.totalCount',
        count: '$.count',
        countryList: ['$.list', {
            type: 'country',
            name: '$._doc.name',
            code: '$._doc.code'
        }]
    };

    return db.transformJson(res, template);
}

router.route('/').get(commonApis.getCountryList);

module.exports = router;
