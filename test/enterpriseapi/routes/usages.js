var express = require('express');
var router = express.Router();
var db = require('../database.js');

var dataUsage = {};

function sortByKey(array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
}

dataUsage.getUsages = function (req, res, next) {
    db.dataUsage.aggregate([
        {
            $match: {
                date: {'$gte': new Date(req.query.from), '$lte': new Date(req.query.to)}
            }
        },
        {
            $group: {
                _id: '$simId',
                dataUsage: {$sum: '$dataUsageInMB'},
            }
        }
    ], function (err, result) {
        if (err) {
            next(err);
        } else {
            db.dataUsage.populate(result, {path: '_id', model: 'sim'}, function (err, values) {
                db.dataUsage.populate(values, [{path: '_id.user', model: 'user'}, {
                    path: '_id.group',
                    model: 'group'
                }], function (err, values1) {
                    var i = 0;
                    var usageMap = {};
                    var usageArray = [];
                    for (i; i < values1.length; i++) {
                        if (values1[i]._id.user) {
                            usageMap[values1[i]._id.user.emailId] = {
                                dataUsage: values1[i].dataUsage + (usageMap[values1[i]._id.user.emailId] ? usageMap[values1[i]._id.user.emailId].dataUsage || 0 : 0),
                                location: values1[i]._id.user.location,
                                group: values1[i]._id.group ? values1[i]._id.group.name : 'Enterprise',
                                firstName: values1[i]._id.user.firstName,
                                lastName: values1[i]._id.user.lastName
                            };
                        }
                    }
                    for (var usageobj in usageMap) {
                        usageArray.push(usageMap[usageobj]);
                    }
                    res.json({
                        type: 'dataUsage',
                        from: new Date(req.query.from),
                        to: new Date(req.query.to),
                        values: sortByKey(usageArray, 'dataUsage').splice(0, 10)
                    });
                });
            })
        }
    });
};

dataUsage.getUsagesByDateRange = function (req, res, next) {
    var groupByPeriod = req.query.groupByPeriod;
    var fromDate = req.query.fromDate;
    var toDate = req.query.toDate;
    var startIndex = req.query.startIndex;
    var count = req.query.count;
    var sortBy = req.query.sortBy;
    var sortDirection = req.query.sortDirection;
    db.dataUsage.aggregate([
        {
            $match: {
                date: {'$gte': new Date(new Date(fromDate).getTime() + 330 * 60000), '$lte': new Date(toDate)}
            }
        },
        {
            $group: {
                _id: {$dateToString: {format: "%Y-%m-%d", date: "$date"}},
                dataUsage: {$sum: '$dataUsageInMB'},
            }
        }
    ], function (err, result) {
        if (err) {
            next(err);
        } else {
            var resp = {type: 'DataUsageList', values: result};

            res.json(transformUsageByDateRange(resp));
        }
    });
};

function transformUsageByDateRange(resp) {
    var template = {
        type: "$.type",
        groupByPeriod: "DAY",
        totalDataUsedInBytes: "12341234",
        totalCount: "100",
        count: "10",
        list: ['$.values', {
            type: 'DataUsage',
            dataUsedInBytes: '$.dataUsage',
            fromDate: '$._id',
            toDate: '$._id'
        }]
    };

    return db.transformJson(resp, template);
}

dataUsage.getUsagesByWeek = function (req, res, next) {
    var groupByPeriod = req.query.groupByPeriod;
    var fromDate = req.query.fromDate;
    var toDate = req.query.toDate;
    var startIndex = req.query.startIndex;
    var count = req.query.count;
    var sortBy = req.query.sortBy;
    var sortDirection = req.query.sortDirection;
    if (groupByPeriod === "WEEK"){
        db.dataUsage.aggregate([
            {
                $match: {
                    date: {
                        '$gte': new Date(new Date(fromDate).getTime() + 330 * 60000),
                        '$lte': new Date(toDate)
                    }
                }
            },
            {
                $group: {
                    _id: {simId: "$simId", week: {$week: "$date"}},
                    dataUsage: {$sum: '$dataUsageInMB'},
                    start_date: {$min: {$dateToString: {format: "%Y-%m-%d", date: "$date"}}},
                    end_date: {$max: {$dateToString: {format: "%Y-%m-%d", date: "$date"}}}
                }
            },
            {
                "$group": {
                    "_id": "$_id.simId",
                    "values": {
                        "$push": {
                            "week": "$_id.week",
                            "start": "$start_date",
                            "end": "$end_date",
                            "dataUsage": "$dataUsage"
                        }

                    }
                }
            }, {"$sort": {"_id": 1}}
        ], function (err, result) {
            if (err) {
                next(err);
            } else {
                db.dataUsage.populate(result, {path: '_id', model: 'sim'}, function (err, values) {
                    db.dataUsage.populate(values, [{path: '_id.group', model: 'group'}], function (err, values1) {
                        var i = 0;

                        var usageMap = {};
                        var usageArray = [];
                        for (i; i < values1.length; i++) {
                            var j = 0;
                            var group = values1[i]._id.group ? values1[i]._id.group.name : 'Default';
                            if (!usageMap[group]) {
                                usageMap[group] = {
                                    _id: values1[i]._id.group ? values1[i]._id.group.name : 'Default'

                                };
                            }
                            if (!usageMap[group].values) {
                                usageMap[group].values = {};
                            }
                            for (j; j < values1[i].values.length; j++) {
                                usageMap[group].values[values1[i].values[j].week] = {
                                    dataUsage: values1[i].values[j].dataUsage + (usageMap[group].values[values1[i].values[j].week] ? usageMap[group].values[values1[i].values[j].week].dataUsage || 0 : 0),
                                    week: values1[i].values[j].week,
                                    start: values1[i].values[j].start,
                                    end: values1[i].values[j].end
                                };
                            }
                        }
                        for (var usageobj in usageMap) {
                            var valObj = usageMap[usageobj].values;
                            usageMap[usageobj].values = [];
                            for (var weekValue in valObj) {
                                usageMap[usageobj].values.push(valObj[weekValue]);
                            }
                            usageArray.push(usageMap[usageobj]);
                        }

                        res.json(transformUsagesByWeek({
                            type: 'AccountDataUsageList',
                            from: new Date(fromDate),
                            to: new Date(toDate),
                            values: usageArray
                        }));
                    });
                });
            }
        });
    }
    else
    {
        db.dataUsage.aggregate([
            {
                $match: {
                    date: {'$gte': new Date(fromDate), '$lte': new Date(toDate)}
                }
            },
            {
                $group: {
                    _id: '$simId',
                    dataUsage: {$sum: '$dataUsageInMB'},
                }
            }
        ], function (err, result) {
            if (err) {
                next(err);
            } else {
                db.dataUsage.populate(result, {path: '_id', model: 'sim'}, function (err, values) {
                    db.dataUsage.populate(values, [{path: '_id.group', model: 'group'}], function (err, values1) {
                        var i = 0;
                        var usageMap = {};
                        var usageArray = [];
                        for (i; i < values1.length; i++) {
                            var group = values1[i]._id.group ? values1[i]._id.group.name : 'Default';
                            usageMap[group] = {
                                dataUsage: values1[i].dataUsage + (usageMap[group] ? usageMap[group].dataUsage || 0 : 0),
                                _id: values1[i]._id.group ? values1[i]._id.group.name : 'Default'

                            };
                        }
                        for (var usageobj in usageMap) {
                            usageArray.push(usageMap[usageobj]);
                        }
                        var resp = {
                            type: "AccountDataUsageList",
                            groupByPeriod: "MONTH",
                            totalDataUsedInBytes: 12341234,
                            totalCount: "100",
                            count: "10",
                        }
                        var i = 0;
                        resp.list =[];
                        for (i; i < usageArray.length; i++) {
                            var listData = {
                                type: "DataUsageList",
                                accountId: "1234",
                                accountName: usageArray[i]._id,
                                accountType: "GROUP",
                                totalDataUsedInBytes: "12341234"
                            }
                            listData.list =[{
                                type: 'DataUsage',
                                dataUsedInBytes: usageArray[i].dataUsage,
                                fromDate: fromDate,
                                toDate: toDate
                            }];
                            resp.list.push(listData)

                        }

                        res.json(resp);
                    });
                })
            }
        });
    }
};

function transformUsagesByWeek(resp) {
    var template = {
        type: "$.type",
        groupByPeriod: "WEEK",
        totalDataUsedInBytes: "100",
        totalCount: "100",
        count: "10",
        list: ['$.values', {
            list: ["$.values", {
                type: 'DataUsage',
                dataUsedInBytes: '$.dataUsage',
                fromDate: '$.start',
                week:'$.week',
                toDate: '$.end'
            }],
            type: "DataUsageList",
            accountId: "1234",
            accountName: "$._id",
            accountType: "GROUP",
            totalDataUsedInBytes: "12341234"

        }]
    };
    return db.transformJson(resp, template);
}
dataUsage.getUsagesByZone = function (req, res, next) {
    var groupByPeriod = req.query.groupByPeriod;
    var fromDate = req.query.fromDate;
    var toDate = req.query.toDate;
    var startIndex = req.query.startIndex;
    var count = req.query.count;
    var sortBy = req.query.sortBy;
    var sortDirection = req.query.sortDirection;

    db.dataUsage.aggregate([
        {
            $match: {
                date: {'$gte': new Date(fromDate), '$lte': new Date(toDate)}
            }
        },
        {
            $group: {
                _id: '$zone',
                dataUsage: {$sum: '$dataUsageInMB'},
            }
        }
    ], function (err, result) {
        if (err) {
            next(err);
        } else {

            var resp = {
                type: "AccountDataUsageList",
                groupByPeriod: "MONTH",
                totalCount: "100",
                count: "10",
            }
                var i = 0;
            resp.list =[];
            var totalDataUsedPerZone = 0;

            for (i; i < result.length; i++) {
                var listData = {
                    type: "DataUsageList",
                    zoneId: "1234",
                    zoneName: result[i]._id,
                    totalDataUsedInBytes: result[i].dataUsage
                }
                listData.list =[{
                    type: 'DataUsage',
                    dataUsedInBytes: result[i].dataUsage,
                    fromDate: fromDate,
                    toDate: toDate
                }];
                totalDataUsedPerZone += result[i].dataUsage;
                resp.list.push(listData);

            }
            resp.totalDataUsedInBytes = totalDataUsedPerZone;
            res.json(resp);

    }
});
};

function transformUsagesByZone(resp) {
    var template = {
        type: "$.type",
        groupByPeriod: "MONTH",
        totalDataUsedInBytes: "100",
        totalCount: "100",
        count: "10",
        list: ['$.values', {
            list:["$",{
                type: 'DataUsageList',
                dataUsedInBytes: '$.dataUsage',
                fromDate: '$._id',
                toDate: '$._id'
            }],
            type: "DataUsageList",
            zoneId: "1234",
            zoneName: "$._id",
            accountId: "1234",
            accountName: "Finance",
            accountType: "ENTERPRISE",
            totalDataUsedInBytes: "12341234"

        }]
    };

    return db.transformJson(resp, template);
}

dataUsage.getUsagesByCountry = function (req, res, next) {
    var companyName = req.query.company;
    db.dataUsage.aggregate([
        {
            $match: {
                date: {'$gte': new Date(req.query.from), '$lte': new Date(req.query.to)},
                company: companyName
            }
        },
        {
            $group: {
                _id: '$country',
                dataUsage: {$sum: '$dataUsageInMB'},
            }
        }, {"$sort": {"_id": 1}}
    ], function (err, result) {
        if (err) {
            next(err);
        } else {
            res.json(result);
        }
    });
};
dataUsage.getUsagesByGroup = function (req, res, next) {
    db.dataUsage.aggregate([
        {
            $match: {
                date: {'$gte': new Date(req.query.from), '$lte': new Date(req.query.to)}
            }
        },
        {
            $group: {
                _id: '$simId',
                dataUsage: {$sum: '$dataUsageInMB'},
            }
        }
    ], function (err, result) {
        if (err) {
            next(err);
        } else {
            db.dataUsage.populate(result, {path: '_id', model: 'sim'}, function (err, values) {
                db.dataUsage.populate(values, [{path: '_id.group', model: 'group'}], function (err, values1) {
                    var i = 0;
                    var usageMap = {};
                    var usageArray = [];
                    for (i; i < values1.length; i++) {
                        var group = values1[i]._id.group ? values1[i]._id.group.name : 'Default';
                        usageMap[group] = {
                            dataUsage: values1[i].dataUsage + (usageMap[group] ? usageMap[group].dataUsage || 0 : 0),
                            _id: values1[i]._id.group ? values1[i]._id.group.name : 'Default'

                        };
                    }
                    for (var usageobj in usageMap) {
                        usageArray.push(usageMap[usageobj]);
                    }
                    res.json({type: 'dataUsage', values: usageArray});
                });
            })
        }
    });
};

dataUsage.getTodaysUsage = function (req, res, next) {
    db.dataUsage.aggregate([
        {
            $match: {
                date: {
                    '$gte': new Date(new Date().setHours(0, 0, 0)),
                    '$lte': new Date(new Date().setHours(23, 59, 59))
                }
            }
        }, {
            $group: {
                dataUsage: {$sum: '$dataUsageInMB'},
            }
        }
    ], function (err, result) {
        if (err) {
            next(err);
        } else {
            res.json(result);
        }
    });
};

dataUsage.getSimUsageDaywise = function( req, res, next ){

    var startDate = new Date( req.query.fromDate );
    var endDate = new Date( req.query.toDate );
    var iccId = req.originalUrl.split( "/")[8];

    db.sim.find( { "iccId": iccId }).exec(function(err,simList) {
        if(err)
            return next(err);
        if( simList.length > 0 ) {
            var simId = db.ObjectId(simList[0]._id);
            db.dataUsage.aggregate([
                { $match: {
                    date: {'$gte': startDate,'$lte':endDate  },
                    simId:simId

                }},
                {
                    $group: {
                        // _id: "$simId",
                        _id: { $dateToString: { format: "%Y-%m-%d", date: "$date" } },
                        dataUsage: {$sum: '$dataUsageInMB'},
                    }},
                { $sort:{

                    "_id":1



                }
                }
            ], function (err, result) {
                if (err) {
                    return next("Unable to retrive the SIM Data Usgae!!");
                } else {
                    var resp = {type: 'DataUsageList', values: result};
                    resp= transformUsageByDateRange( resp );
                    res.json( resp );
                }
            });
        }

    });
};

dataUsage.getUserUsageDaywise = function( req, res, next ){

    var userId = [ db.ObjectId( req.originalUrl.split( "/")[8] ) ];
    var startDate = new Date( req.query.fromDate );
    var endDate = new Date( req.query.toDate );
    var len;
    var simIds = [];

    // get the associated SIMs
    db.sim.find({ "user":{ $in: userId } } ).exec( function( simErr, simList ) {
        if( simErr ){
            return next( simErr);
        }

        len = simList.length;

        // get the usage data from simDataUsage collection
        for( var j=0; j< len; j++ ) {

            simIds[j]= db.ObjectId(simList[j]._id);
        }

                // aggregate query to get the data usage
                db.dataUsage.aggregate([
                    { $match: {
                        date: {'$gte': startDate,'$lte':endDate  },
                        simId : { $in: simIds }
                    }},
                    { $group: {
                        _id: { $dateToString: { format: "%Y-%m-%d", date: "$date" } },
                        dataUsage:{ $sum: '$dataUsageInMB'},
                    }}

                ], function (dataUsageErr, dataUsage ) {
                    if (dataUsageErr) {
                        return next(dataUsageErr)
                    }

                    var resp = {type: 'DataUsageList', values: dataUsage};
                    resp= transformUsageByDateRange( resp );
                    res.json(resp);
                });
    });
}



router.route('/todaysusage').get(dataUsage.getTodaysUsage);
router.route('/topUserUsage').get(dataUsage.getUsages);
router.route('/:aid/datausage').get(dataUsage.getUsagesByDateRange);
router.route('/:aid/zones/datausage').get(dataUsage.getUsagesByZone);
router.route('/group').get(dataUsage.getUsagesByGroup);
/*router.route('/country').get(dataUsage.getUsagesByCountry);*/
router.route('/:aid/groups/datausage').get(dataUsage.getUsagesByWeek);

// SIM daywise data usage
router.route( "/:aid/sims/sim/:id/datausage").get( dataUsage.getSimUsageDaywise );

// User daywise data usage
router.route( "/:aid/users/user/:uid/datausage").get( dataUsage.getUserUsageDaywise );

module.exports = router;