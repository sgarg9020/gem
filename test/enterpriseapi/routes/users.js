var express = require('express');
var router = express.Router();

var db = require('../database.js');
var multer  = require('multer');
var upload = multer({ dest: './uploads/' ,rename : function(fieldname, filename, req, res) {
    return req.body.infoUser.id
}});
var fs = require('fs');

var users = {};

var schemaKeyList = ['firstName','lastName','emailId','location','group'];

users.isValidUsersListType = function(listReq,next){
    console.log( "listReq type::");
    console.log( listReq );

    if(listReq.type !="UserList"){
        var err = new Error("Type of input object doesn't match to Users");
        err.status = 400;
        next(err);
        return false;
    }
    return true;
};

users.isValidUserType = function(user,next){

    if(user.type!="User"){
        var err = new Error("Type of input object "+ user.type +" doesn't match to User");
        err.status = 400;
        next(err);
        return false;
    }
    return true;

};

users.createUsers = function(req,res,next){

       // single user creation
    if( req.body.list && req.body.list.length == 1 ){
        if(!users.isValidUsersListType(req.body,next)) {
            return;
        }
        users.createUser(req,res,next);
        return;
    }

  db.user.create(req.body.list,function(err,users){
      if(err){
          err = new Error("Unable to create users");
          return next(err);
      }
     res.sendStatus(200);
  });

};

users.updateUsers = function(req,res,next){
    if(!users.isValidUsersListType(req.body,next))
        return;
    var bulk = db.user.collection.initializeUnorderedBulkOp();
    var list = req.body.list;
    var count = list.length;
    var usersIds = [];
    var group = null;
    if( typeof list[0].toAccount != "undefined" && list[0].toAccount != -1 ) {
        var group = db.ObjectId(list[0].toAccount );
    }

    for(var i=0;i<count;i++){
        var user = list[i];
        usersIds[i] = db.ObjectId(user.userId);
        if(!users.isValidUserType(user,next))
        return;
        delete user._id;
        var fields = db.user.setFields(user);


        bulk.find({_id:db.ObjectId(user.userId)}).updateOne({$set:fields });
    }

    bulk.execute(function(err){
        if(err){
            err = new Error("Unable to update users");
            return next(err);
        }else{
            users.updateGroupOfSims(usersIds,group, res, next);
            res.sendStatus(200);
        }

    });
};

users.updateGroupOfSims = function (usersList,group, res, next) {
    var len = usersList.length;
    if (len == 0) res.sendStatus(200);

    for (var i = 0; i < len; i++) {
        var userId = usersList[i];
        db.sim.update({user: userId},{$set: {group: group}},{multi: true}, function (simErr, res) {

            if (simErr) {
                simErr = new Error("Unable to update sims");
                return next(simErr);
            }
        });
    }


}

users.deleteUsers = function(req,res,next){
    var list = req.query.uid.split( ",");
    var uids = [];
    for( var i = 0; i < list.length; i++ ){
        uids[ i ] = db.ObjectId( list[ i ]);
    }
    db.user.remove( { _id:{ $in: uids } }).exec( function( remErr,  remRes ) {
        if( !remErr ) {
            users.updateSims( uids, res, next);
        }
        else {
            remErr = new Error("Unable to delete users");
            return next( remErr );
        }

        res.sendStatus( 200 );

    });
};

users.getUsers = function(req,res,next) {
    var startIndex = req.query.startIndex || 0;
    var count = req.query.count;
    var sortParam = {firstName: 1};
    var sortKey = 1;
    var searchQ = req.query.search;

    if (req.query.sortBy !== undefined) {
        sortParam = db.user.getFilters(req.query.sortBy, req.query.sortDirection);
        if (sortParam['group.name'] == -1) {
            sortKey = -1;
        }
    }
            db.user.find({}).sort(sortParam).select(db.user.projection).populate([db.user.groupPopulation, db.user.accountsPopulation]).exec(function (err, userList) {
                if (!err) {
                    if (searchQ) {
                        var columns = ['firstName', 'lastName', 'name', 'emailId', 'group', 'location'];
                        userList = db.searchOnPopulatedFields(userList, searchQ, columns);
                    }

                    if (sortParam['group.name']) {
                        userList = db.sortOnPopulatedFields(userList, 'group', 'name', sortKey);
                    }

                    var totalCount = userList.length;

                    var len;
                    var endDate, billingDate;
                    var simIds = [];
                    var dataUsages = [];
                    var userId = [];
                    var associatedUsers = [];


                    for (var m = 0; m < userList.length; m++) {
                        userId.push(db.ObjectId(userList[m]._id));
                    }
                    // get the associated SIMs
                    db.sim.find({"user": {$in: userId}}).exec(function (simErr, simList) {
                        if (simErr) {
                            return next(simErr);
                        }

                        len = simList.length;

                        // get the usage data from simDataUsage collection
                        for (var j = 0; j < len; j++) {
                            var user = db.ObjectId(simList[j].user);

                            if (typeof associatedUsers[user] != "undefined") {
                                associatedUsers[user] = associatedUsers[user] + "," + db.ObjectId(simList[j]._id);
                            }
                            else {
                                associatedUsers[user] = db.ObjectId(simList[j]._id);
                            }

                            simIds[j] = db.ObjectId(simList[j]._id);
                        }


                        // get the current billing cycle
                        db.billingCycle.find({}).select(db.billingCycle.projection).exec(function (billErr, billingCycle) {
                            if (billErr) {
                                billErr = new Error("Unable to get billing cycle details");
                                return next(billErr);
                            } else {
                                billingDate = billingCycle[0];

                                // check whether billing end date is greater than today's date
                                if (billingDate.billingEnd.getDate() > new Date().getDate()) {
                                    endDate = new Date();

                                }
                                else {
                                    endDate = new Date(billingDate.billingEnd);
                                }

                                var startDate = new Date(billingDate.billingStart);


                                // aggregate query to get the data usage
                                db.dataUsage.aggregate([
                                    {
                                        $match: {
                                            date: {'$gte': startDate, '$lte': endDate},
                                            simId: {$in: simIds}
                                        }
                                    },
                                    {
                                        $group: {
                                            _id: "$simId",
                                            dataUsage: {$sum: '$dataUsageInMB'},
                                        }
                                    }
                                ], function (dataUsageErr, dataUsage) {
                                    if (dataUsageErr) {
                                        return next(dataUsageErr)
                                    }
                                    var resLen = dataUsage.length;
                                    var totalUsage = 0;
                                    var finalUsage = [];

                                    for (var k = 0; k < resLen; k++) {
                                        checkPresence(dataUsage[k]);
                                    }

                                    function checkPresence(dataUsage) {

                                        for (var l = 0; l < simIds.length; l++) {

                                            if (simIds[l].toHexString() == dataUsage._id.toHexString()) {
                                                dataUsages[dataUsage._id] = dataUsage.dataUsage;
                                                return true;
                                            }
                                        }
                                    }

                                    // loop through the users list and find the datausage for the associated SIMs
                                    for (var user in associatedUsers) {
                                        var simsArr = String(associatedUsers[user]).split(",");

                                        var totalUsage = 0;
                                        var simLen = simsArr.length;
                                        for (var x = 0; x < simLen; x++) {
                                            if (typeof dataUsages[simsArr[x]] != "undefined") {
                                                totalUsage = totalUsage + dataUsages[simsArr[x]];

                                            }
                                        }

                                        finalUsage[user] = {simCount: simLen, dataUsage: totalUsage};
                                    }
                                    for (var m = 0; m < userList.length; m++) {


                                        if (typeof finalUsage[userList[m]._id] != "undefined") {
                                            userList[m].set("sim", finalUsage[userList[m]._id].simCount, {strict: false});
                                            userList[m].set("usage", finalUsage[userList[m]._id].dataUsage, {strict: false});
                                        }
                                        else {
                                            userList[m].set("sim", 0, {strict: false});
                                            userList[m].set("usage", 0, {strict: false});
                                        }
                                     }


                                    if (sortParam['usage']) {
                                        sortByKey(userList, "usage", sortParam['usage']);

                                    }

                                    if (sortParam['sim']) {
                                        sortByKey(userList, "sim", sortParam['sim']);

                                    }
                                    if (count) {
                                        userList = userList.splice(startIndex, count);
                                    }

                                    function sortByKey(array, key, sortOrder) {
                                        return array.sort(function (a, b) {
                                            var x = a._doc[key];
                                            var y = b._doc[key];
                                            if (x === undefined) x = 0;
                                            if (y === undefined) y = 0;
                                            return ((x > y) ? sortOrder : ((x < y) ? -sortOrder : 0));

                                        });
                                    }

                                    var resp = {
                                        type: "Users",
                                        startIndex: startIndex,
                                        totalCount: totalCount,
                                        filteredCount: totalCount,
                                        count: userList.length,
                                        list: userList
                                    };
                                    resp = users.transformUsersJson(resp);

                                    res.json(resp);

                                });
                            }
                        });

                    });


                }
                else {
                    err = new Error("Unable to get users");
                    next(err);
                }
            });

};


users.transformUsersJson = function(resp){
    var template = {
        type: "UserList",
        totalCount: "$.totalCount",
        count: "$.count",
        list: ['$.list', {
            type: 'User',
            userId: '$._doc._id',
            firstName: '$._doc.firstName',
            lastName: '$._doc.lastName',
            emailId: '$._doc.emailId',
            location: '$._doc.location',
            dataUsedInBytes: '$._doc.usage',
            totalSimsAssociated: '$._doc.sim',
            accountDetails: {
                type: "EnterpriseAccountList",
                enterpriseAccountDetails: ['$._doc.accounts',{
                    type: "Account",
                    AccountDetails: {
                        type: "Account",
                        accountType: "ENTERPRISE",
                        accountId: '$._doc._id',
                        accountName: '$._doc.companyName'

                    }
                }],
                parentAccountDetails: {
                    type:"Account",
                    accountId: '$._doc.group._doc._id',
                    accountName: '$._doc.group._doc.name',
                    accountType:'GROUP',
                    role: 'ENTERPRISE_USER'

                }
            }

        }]
    };

    var resp =  db.transformJson(resp, template);

    for( var i=0; i < resp.list.length; i++ ){
        var user = new Object(resp.list[i]);
        var key = "parentAccountDetails";
        var enterprise=[];

        var acc = user.accountDetails;   //.enterpriseAccountDetails;
        var parentAcc = new Array(acc.parentAccountDetails);
        if( acc.enterpriseAccountDetails[0] ){
            enterprise = acc.enterpriseAccountDetails[0];
            enterprise[ key ] = parentAcc;
        }
        else {
            acc.enterpriseAccountDetails.push({ "parentAccountDetails": parentAcc });
        }
        delete user.accountDetails.parentAccountDetails;
        delete user.accountDetails;
        user.accountDetails = acc;
        resp.list[i] = user;
    }

    return resp;
};


users.transformUserJson = function( resp ){


    var template = {
      type: 'User',
      userId: '$._doc._id',
      firstName: '$._doc.firstName',
      lastName: '$._doc.lastName',
      emailId: '$._doc.emailId',
      location: '$._doc.location',
        address:{
            country:'$._doc.country'
        },
      dataUsedInBytes: '$._doc.usage',
      totalSimsAssociated: '$._doc.sim',
      accountDetails: {
        type: "EnterpriseAccountList",
        enterpriseAccountDetails: ['$._doc.accounts',{
        type: "Account",
        rootAccountDetails: {
            type: "Account",
            accountType: "ENTERPRISE",
            accountId: '$._doc._id',
            accountName: '$._doc.companyName'
        }
        }],

          parentAccountDetails: {
            type:"Account",
              accountId: '$._doc.group._doc._id',
              accountName: '$._doc.group._doc.name'
          }
      }
    };
    var user =  db.transformJson(resp, template);

    var key = "parentAccountDetails";

    var acc = user.accountDetails;
    var parentAcc = new Array(acc.parentAccountDetails);

    if( acc.enterpriseAccountDetails[0] ){
        enterprise = acc.enterpriseAccountDetails[0];
        enterprise[ key ] = parentAcc;
    }
    else {
        acc.enterpriseAccountDetails.push({ "parentAccountDetails": parentAcc });
    }


    delete user.accountDetails.parentAccountDetails;

    delete user.accountDetails;
    user.accountDetails = acc;
    return user;



};


users.getBillingCycle =function(){

    // get the current billing cycle
    db.billingCycle.find({}).select(db.billingCycle.projection).exec(function (err, billingCycle) {
        if (err) {
            err = new Error("Unable to get billing cycle details");
            return err;
        } else {
            return billingCycle[0];
        }
    });
};

users.getUser = function(req,res,next){

    db.user.findOne({_id:db.ObjectId(req.params.id)}).select(db.user.projection).populate([db.user.groupPopulation,db.user.enterprisePopulation]).exec(function(err,user){
        if(err){
            err = new Error("Unable to get user details");
            return next(err);
        }

        // get the associated SIMs
        db.sim.find({ "user":db.ObjectId( req.params.id ) } ).exec( function( simErr, simList ) {
            if( simErr ){
                simErr = new Error( "Unable to get the associated SIMs");
                return next( simErr);
            }

            len = simList.length;
            var simIds =[];

            // get the usage data from simDataUsage collection
            for( var j=0; j< len; j++ ) {

                simIds[j]= db.ObjectId(simList[j]._id);
            }


            // get the current billing cycle
            db.billingCycle.find({}).select(db.billingCycle.projection).exec(function (billErr, billingCycle) {
                if (billErr) {
                    billErr = new Error("Unable to get billing cycle details");
                    return next(billErr);
                } else {
                    billingDate = billingCycle[0];

                    // check whether billing end date is greater than today's date
                    if (billingDate.billingEnd.getDate() > new Date().getDate()) {
                        endDate = new Date();

                    }
                    else {
                        endDate = new Date(billingDate.billingEnd);
                    }

                    var startDate = new Date(billingDate.billingStart);


                    // aggregate query to get the data usage
                    db.dataUsage.aggregate([
                        { $match: {
                            date: {'$gte': startDate,'$lte':endDate  },
                            simId : { $in: simIds }
                        }},
                        { $group: {
                            _id: "$simId",
                            dataUsage:{ $sum: '$dataUsageInMB'},
                        }
                        }
                    ], function (dataUsageErr, dataUsage ) {
                        if (dataUsageErr) {
                            return next(dataUsageErr)
                        }
                        var resLen = dataUsage.length;
                        var totalUsage = 0;

                        for( var m =0; m < resLen; m++ ){
                            totalUsage = totalUsage + dataUsage[ m].dataUsage;
                        }
                        user.set( "usage", totalUsage, { strict:false });

                        user = users.transformUserJson( user );

                        res.json(user);
                    });
                }
            });

        });

    });
};

users.updateUser = function(req,res,next){
    var user = req.body;
    var userId = [];
    userId[0] = db.ObjectId(req.params.id);
    var group = null;
    if (user.group != null) {
        group = db.ObjectId(user.group.groupId);
    }

    if(!users.isValidUserType(user,next))
        return;

    db.user.findOneAndUpdate({_id:db.ObjectId(req.params.id)},{$set:db.user.setFields(user)},function(err,user){
        if(err){
            err = new Error("Unable to update user");
            return next(err);
        }else{
            users.updateGroupOfSims(userId,group, res, next);
            res.status(200);
            res.json(user);
        }

    });
};

users.deleteUser = function(req,res,next){

    db.user.findOneAndRemove({_id:db.ObjectId(req.params.id)},function(err,user){
        if( !err ){
            users.updateSims( [db.ObjectId(req.params.id)], res, next );
        }
        else {
            err = new Error("Unable to delete user");
            return next(err);
        }
        res.status(200);
    });
};

users.createUser = function(req,res,next){

    if( !users.isValidUserType( req.body.list[0], next ) ) {
        return;
    }

    var user = req.body.list[0];
    user.toAccount = req.originalUrl.split( "/")[6];

    // check whether the toAccount is an Enterprise / root account
    db.enterprise.find({"_id":db.ObjectId( user.toAccount ) }).exec( function( accErr, accRes ) {
       if( accErr ) {
           err = new Error("Unable to Create user");
           return next(err);
       }
        else {
           // given group is root account
           if( accRes && accRes.length > 0  ){
               user.toAccount=-1;
           }
           user = db.user.setFields(user );

           (new db.user(user)).save(function(err,user){
               if(err){
                   err = new Error("Unable to create user");
                   return next(err);
               }
               res.status(200);
               res.json(user);
           });

       }
    });


};


users.updateSims = function( usersList,  res, next ) {

    var len = usersList.length;

            db.sim.update({"user": {$in: usersList}}, {$set: {user:null }}, {multi: true}, function (simErr, res) {

           if (simErr) {
                simErr = new Error("Unable to update sims");
                 return next(simErr);
           }
           else {

           }
        });

};


users.getUserDataUsageDayWise = function( req, res, next ){

    var userId = [ db.ObjectId( req.params.id ) ];
    var len;
    var endDate, billingDate;
    var simIds = [];


    // get the associated SIMs
    db.sim.find({ "user":{ $in: userId } } ).exec( function( simErr, simList ) {
        if( simErr ){
            return next( simErr);
        }

        len = simList.length;

        // get the usage data from simDataUsage collection
        for( var j=0; j< len; j++ ) {

            simIds[j]= db.ObjectId(simList[j]._id);
        }

        // get the current billing cycle
        db.billingCycle.find({}).select(db.billingCycle.projection).exec(function (billErr, billingCycle) {
            if (billErr) {
                billErr = new Error("Unable to get billing cycle details");
                return next(billErr);
            } else {
                billingDate = billingCycle[0];

                // check whether billing end date is greater than today's date
                if (billingDate.billingEnd.getDate() > new Date().getDate()) {
                    endDate = new Date();

                }
                else {
                    endDate = new Date(billingDate.billingEnd);
                }

                var startDate = new Date(billingDate.billingStart);

                // aggregate query to get the data usage
                db.dataUsage.aggregate([
                    { $match: {
                        date: {'$gte': startDate,'$lte':endDate  },
                        simId : { $in: simIds }
                    }},
                    { $group: {
                        _id: { $dateToString: { format: "%Y-%m-%d", date: "$date" } },
                        dataUsage:{ $sum: '$dataUsageInMB'},
                    }}

                ], function (dataUsageErr, dataUsage ) {
                    if (dataUsageErr) {
                        return next(dataUsageErr)
                    }

                    var resp = {company: db.ObjectId(req.params.id),values: dataUsage};
                    res.json(resp);
                });
            }
        });

    });

};

users.getSIMsByUserId = function (req, res, next) {

    db.sim.find({user:db.ObjectId(req.params.id)}).select(db.sim.generalInfoProjection).exec(function(err,simsResponse){
        if(err){
            err = new Error("Error while fetching simList associated to logged in user");
            return next(err);
        };

        var resp = {type:"SIMs",list:simsResponse};
        simsResponse = transformSIMsJson(resp);
        res.json(simsResponse);
    });
};

function transformSIMsJson(resp){
    var template = {
        type: "SimList",
        totalCount: "$.totalCount",
        count: "$.count",
        list: ['$.list', {
            type: 'Sim',
            simId: '$._doc.iccId',
            status: '$._doc.status',
            dataUsedInBytes: '$._doc.usage',
            nickname: '$._doc.name',
            user: {
                userId: '$._doc.user._doc._id',
                firstName: '$._doc.user._doc.firstName',
                lastName: '$._doc.user._doc.lastName'
            },
            account: {
                type: "Enterprise",
                accountId: '$._doc.group._doc._id',
                accountName: '$._doc.group._doc.name'
            }

            , creditLimitList: {
                type: 'creditLimitList',
                list:[ '', {
                    type: 'creditLimit',
                    limitKB: '$._doc.allocation'
                }]

            }
        }]
    };

    resp  =  db.transformJson(resp, template);
    for( var i=0; i < resp.list.length; i++ ) {
        var sim = new Object(resp.list[i]);
        var creditList = [sim.creditLimitList.list];
        sim.creditLimitList.list = creditList;
        resp.list[i] = sim;
    }
    return resp;
};



router.route('/upload').post(upload.array('file'), function (req, res, next) {
//    console.log(req.files);
    var xls = req.files[0];
    var lineList = fs.readFileSync(xls.path).toString().split('\r');
    lineList.shift(); // Shift the headings off the list of records.
    var users = [];

    function createDocRecurse(err, user) {
        if (err) {
            console.log(err);
            res.status(500);
            res.send();
        }
        else if (lineList.length) {
            var line = lineList.shift();
            var doc = {};
            line.split(',').forEach(function (entry, i) {
                if (schemaKeyList[i] !== 'group') {
                    doc[schemaKeyList[i]] = entry;
                }
                else {
                    var groupref = new db.group();
                    db.group.findOne({'name': entry}, function (err, groupref) {
                        if (!err) {
                            doc.group = groupref;
                            var doc1 = db.user.setFields(doc);
                            var doc2 = new db.user(doc1);
                            doc2.save(createDocRecurse);

                        }
                    });
                }
            });

        } else if (!lineList.length) {
            res.status(200);
            res.send();
        }
    }

    createDocRecurse(null);
    fs.unlink(xls.path, function (err) {
        if (err) throw err;
        console.log('successfully deleted' + xls.path);
    });

    // req.body will hold the text fields, if there were any
});

/* GET users listing. */
router.route('/').get(users.getUsers).put(users.updateUsers).post(users.createUsers).delete(users.deleteUsers);
router.route('/:id').get(users.getUser).put(users.updateUser).delete(users.deleteUser);
router.route( '/dataUsage/:id').get( users.getUserDataUsageDayWise );
router.route('/:id/sims').get(users.getSIMsByUserId);

module.exports = router;
