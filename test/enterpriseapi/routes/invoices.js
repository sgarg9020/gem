var express = require('express');
var router = express.Router();
var db = require('../database.js');

var invoices = {};

invoices.getInvoices = function (req, res, next) {
    var startIndex = req.query.startIndex || 0;
    var count = req.query.count || 10;
    var pageNo = (startIndex/count)+1;
    var sortParam = {created: -1};
    var sortKey = 1;
    if(req.query.sortDirection!== undefined) {
        sortParam = db.invoices.getFilters(req.query.sortBy,req.query.sortDirection);
    }
    db.invoices.paginate({},{page:pageNo,limit:count,columns:db.invoices.projection,sortBy:sortParam},
        function(err,invoicesList,pageCount,itemCount){
            if(!err){
                var resp = {type:"AccountInvoiceList",startIndex:startIndex,totalCount:itemCount,count:invoicesList.length,list:invoicesList};
                resp = transformInvoicesJson(resp);
                res.json(resp);
            }
            else{
                err = new Error("Unable to get Invoices");
                next(err);
            }
        });
};

function transformInvoicesJson(res){
    var d = new Date();
    var dateStr = d.toString();
    var template = {
        type: '$.type',
        totalCount: '$.totalCount',
        count: '$.count',
        list: ['$.list', {
            type: 'AccountInvoice',
            invoiceId: '$._doc._id',
            fileName: '$._doc.invoiceNumber',
            url: '$._doc.url',
            generatedDate: '$._doc.created',
            periodStartDate:dateStr,
            periodEndDate: dateStr
        }]
    };

    return db.transformJson(res, template);
}

router.route('/').get(invoices.getInvoices);

module.exports = router;
