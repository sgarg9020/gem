/**
 * Created with JetBrains WebStorm.
 * Group: yogesh
 * Date: 06/05/16
 * Time: 10:07 PM
 * To change this template use File | Settings | File Templates.
 */
var express = require('express');
var router = express.Router();

var alerts = {};

var db = require('../database.js');

alerts.isValidAlertsListType = function (listReq, next) {
    console.log("listReq type::");
    console.log(listReq);

    if (listReq.type != "AlertConfigurationList") {
        var err = new Error("Type of input object doesn't match to Alerts");
        err.status = 400;
        next(err);
        return false;
    }
    return true;
};

alerts.isValidAlertsEditListType = function (listReq, next) {
    console.log("listReq type::");
    console.log(listReq);

    if (listReq.type != "EditAlertsRequest") {
        var err = new Error("Type of input object doesn't match to Alerts");
        err.status = 400;
        next(err);
        return false;
    }
    return true;
};

alerts.isValidAlertType = function (listReq, next) {
    console.log("listReq type::");
    console.log(listReq);

    if (listReq.type != "AlertConfiguration") {
        var err = new Error("Type of input object doesn't match to Alerts");
        err.status = 400;
        next(err);
        return false;
    }
    return true;
};

alerts.createAlerts = function (req, res, next) {
    if (!alerts.isValidAlertsListType(req.body, next))
        return;
    db.alerts.create(req.body.list, function (err, users) {
        if (err) {
            err = new Error("Unable to create alerts");
            return next(err);
        }
        res.sendStatus(200);
    });

};

alerts.updateAlerts = function (req, res, next) {
    if (!alerts.isValidAlertsEditListType(req.body, next))
        return;
    var bulk = db.alerts.collection.initializeUnorderedBulkOp();
    var list = req.body.list;
    var count = list.length;
    var alertIds = [];

    for (var i = 0; i < count; i++) {
        var alert = list[i];
        alertIds[i] = db.ObjectId(alert.alertId);
        if (!alerts.isValidAlertType(alert, next))
            return;
        delete alert._id;
        var fields = db.alerts.setFields(alert);


        bulk.find({_id: db.ObjectId(alertIds[i])}).updateOne({$set: fields});
    }

    bulk.execute(function (err) {
        if (err) {
            err = new Error("Unable to update users");
            return next(err);
        } else {
            //users.updateGroupOfSims(usersIds,group, res, next);
            res.sendStatus(200);
        }

    });
};

alerts.getAlerts = function (req, res, next) {

    var startIndex = req.query.startIndex || 0;
    var count = req.query.count || 10;
    var accountId = req.params.id;
    var simId = req.query.simId;
    var zoneId = req.query.zoneId;
    var alertType = req.query.alertType;
    var param;

    if(simId){
        param = {"alertType":alertType,"simId":simId}
    }
    else if(zoneId){
        param = {"alertType":alertType,"zoneId":zoneId,"accountId":accountId}
    }else if(alertType){
        param = {"alertType":alertType,"accountId":accountId}
    }else{
        param = {"accountId":accountId}
    }

    db.alerts.find(param).exec(function (err, alertList) {
        if (!err) {

            var totalCount = alertList.length;
            if (count) {
                alertList = alertList.splice(startIndex, count);
            }

            var resp = {
                type: "AlertConfigurationList",
                startIndex: startIndex,
                totalCount: totalCount,
                filteredCount: totalCount,
                count: alertList.length,
                list: alertList
            };
            res.json(resp);

        }
        else {
            next(err);
        }
    });
};

alerts.deleteAlerts = function (req, res, next) {
    var list = req.query.alertIDs.split(",");
    var alertIds = [];
    for (var i = 0; i < list.length; i++) {
        alertIds[i] = db.ObjectId(list[i]);
    }
    db.alerts.remove({_id: {$in: alertIds}}).exec(function (remErr, remRes) {
        if (remErr) {
            remErr = new Error("Unable to delete users");
            return next(remErr);
        }
        res.sendStatus(200);

    });
};

alerts.getAlert = function (req, res, next) {

    db.alerts.findOne({_id: db.ObjectId(req.params.alertId)}).exec(function (err, alert) {
        if (err) {
            err = new Error("Unable to get alert details");
            return next(err);
        }
        res.json(alert);
    });
};

alerts.getAllowedAlertActionType = function (req, res, next) {

    var allowedAlertActionType = {
        "type": "AlertActionType",
        "supportedAlertActionList": [
            "EMAIL",
            "DISABLE"
        ]
    }
        res.json(allowedAlertActionType);

};

alerts.getAlertHistory = function (req, res, next) {

    var startIndex = req.query.startIndex || 0;
    var count = req.query.count || 10;
    var accountId = req.params.id;
    var simId = req.query.simId;
    var filter = req.query.filter;
    //var zoneId = req.query.zoneId;
    var alertType;
    if(filter == "ACCOUNT"){
        alertType = "ACCOUNT_CUMULATIVE";
    }else if(filter == "ZONE"){
        alertType = "ZONE_CUMULATIVE";
    } else if(filter == "SIM"){
        alertType = "OVERRIDE_PER_SIM";
    }
    var param;

    if(simId){
        param = {"alertType":alertType,"accountId":accountId,"simId":simId}
    }
    /*else if(zoneId){
        param = {"alertType":alertType,"zoneId":zoneId,"acountId":acountId}
    }*/else if(alertType){
        param = {"alertType":alertType,"accountId":accountId}
    }else{
        param = {"accountId":accountId}
    }

    db.alerts.find(param).exec(function (err, alertList) {
        if (!err) {

            var totalCount = alertList.length;
            if (count) {
                alertList = alertList.splice(startIndex, count);
            }

            var resp = {
                type: "AlertConfigurationList",
                startIndex: startIndex,
                totalCount: totalCount,
                filteredCount: totalCount,
                count: alertList.length,
                list: alertList
            };
            res.json(resp);

        }
        else {
            next(err);
        }
    });
};

router.route('/:id/alerts').post(alerts.createAlerts).put(alerts.updateAlerts).get(alerts.getAlerts).delete(alerts.deleteAlerts);
router.route('/:id/alerts/:alertId').get(alerts.getAlert);
router.route('/:id/alertActions').get(alerts.getAllowedAlertActionType);
router.route('/:id/alertHistory').get(alerts.getAlertHistory);

module.exports = router;
