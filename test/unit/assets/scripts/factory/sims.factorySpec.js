describe('SIM service api', function () {

    var _SIMService,$httpBackend;
    var SIM =  {
        "type": "Sim",
        "simId": "1001100000000000000080",
        "simType": "GIGSKY_SIM",
        "status": "INACTIVE",
        "account": {
            "type": "ENTERPRISE",
            "accountId": 2,
            "accountName": "Uber"
        },
        "supplier": "BLUEFISH",
        "nickName": "SIM-0080",
        "creditLimitList": {
            "type": "CreditLimitList",
            "list": [{
                "type": "CreditLimit",
                "limitType": "SOFT",
                "limitKB": 46080,
                "limitDuration": "MONTH",
                "alertPercentages": [75],
                "alertUserByMail": true,
                "alertEnabled": false
            }]
        },
        "dataUsedInBytes": 12222995024
    };
    var SIMs = [SIM,
        {
            "type": "Sim",
            "simId": "1001100000000000000271",
            "simType": "GIGSKY_SIM",
            "status": "ACTIVE",
            "account": {
                "type": "ENTERPRISE",
                "accountId": 2,
                "accountName": "Uber"
            },
            "supplier": "BLUEFISH",
            "nickName": "SIM-0271",
            "creditLimitList": {
                "type": "CreditLimitList",
                "list": [{
                    "type": "CreditLimit",
                    "limitType": "SOFT",
                    "limitKB": 46080,
                    "limitDuration": "MONTH",
                    "alertPercentages": [75],
                    "alertUserByMail": true,
                    "alertEnabled": false
                }]
            },
            "dataUsedInBytes": 12224595694
        }];

    beforeEach(module('app'));
    beforeEach(inject(function ($injector,SIMService) {
        _SIMService = SIMService;
        $httpBackend = $injector.get('$httpBackend');

    }));

    it('verify get sims success', function() {

        var getSimsHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/2\/sims?.*/g )
            .respond(200,{"type":"SimList","totalCount":2,"count":2,"list":SIMs});
        var queryParam = {count:10,search:"",startIndex:0};
        sessionStorage.setItem('accountId','2');
        var response = _SIMService.getSIMs(queryParam);

        response.success(function(data){
            expect(data.type).toEqual("SimList");
            expect(data.list.length).toEqual(2);
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });
    it('verify get sims failure', function () {

        var getSimsHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/2\/sims?.*/g )
            .respond(500,{});
        var queryParam = {count:10,search:"",startIndex:0};

        var response = _SIMService.getSIMs(queryParam);

        response.success(function(data){
            expect(data.type).toEqual("SimList");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });
    it('verify get sim by user id success',function () {

        var userID = '12';
        sessionStorage.setItem('accountId','2');
        var getSimsHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/2\/user\/12\/sims?.*/g )
            .respond(200,{"type":"SimList","totalCount":2,"count":2,"list":[{"type":"Sim","simId":"1001100000000000000101","simType":"GIGSKY_SIM","status":"INACTIVE","user":{"type":"User","userId":12,"firstName":"uberIndAdmin","lastName":"uberIndAdmin"},"account":{"type":"GROUP","accountId":111287,"accountName":"Group1-new"},"supplier":"BLUEFISH","nickName":"SIM-0101","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":46080,"limitDuration":"MONTH","alertPercentages":[75],"alertUserByMail":true,"alertEnabled":false}]},"dataUsedInBytes":12367776748},{"type":"Sim","simId":"1001100000000000000020","simType":"GIGSKY_SIM","status":"INACTIVE","user":{"type":"User","userId":12,"firstName":"uberIndAdmin","lastName":"uberIndAdmin"},"account":{"type":"GROUP","accountId":111287,"accountName":"Group1-new"},"supplier":"BLUEFISH","nickName":"aaaa","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":20480,"limitDuration":"MONTH","alertPercentages":[75],"alertUserByMail":true,"alertEnabled":true}]},"dataUsedInBytes":0}]});
        var queryParam = {count:10,search:"",startIndex:0};
        var response = _SIMService.getSIMsByUserId(userID,queryParam);

        response.success(function(data){
            expect(data.type).toEqual("SimList");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });
    it('verify get sim by user id failure',function () {

        var userID = '12';

        var getSimsHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/2\/user\/12\/sims?.*/g )
            .respond(500,{});

        var response = _SIMService.getSIMsByUserId(userID);

        response.success(function(data){
            expect(data.type).toEqual("SimList");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });

    it('verify update sim success',function () {
        sessionStorage.setItem('accountId','2');
        var getSimsHandler = $httpBackend.expectPUT(/.*?api\/v1\/account\/2\/sims?.*/g )
            .respond(200,"OK");

        var response = _SIMService.updateSIM(SIM);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });

    it('verify update sim success', function () {
        var getSimsHandler = $httpBackend.expectPUT(/.*?api\/v1\/account\/2\/sims?.*/g )
            .respond(500,{});

        var response = _SIMService.updateSIM(SIM);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });

    it('verify update sims success',function () {

        var SIMData = {"type":"SimList","list":SIMs};

        var getSimsHandler = $httpBackend.expectPUT(/.*?api\/v1\/accounts\/account\/2\/sims?.*/g )
            .respond(200,"OK");

        var response = _SIMService.updateSIMs(SIMData);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });


    it('verify update sims failure',function () {

        var SIMData = {"type":"SimList","list":SIMs};

        var getSimsHandler = $httpBackend.expectPUT(/.*?api\/v1\/accounts\/account\/2\/sims?.*/g )
            .respond(500,{});

        var response = _SIMService.updateSIMs(SIMData);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });

    it('verify update sims status success',function () {
        sessionStorage.setItem('accountId','2');
        var SIMData = {"type":"SimList","list":SIMs};
        var getSimsHandler = $httpBackend.expectPUT(/.*?api\/v1\/accounts\/account\/2\/sims\/status?.*/g )
            .respond(200,"OK");

        var response = _SIMService.updateSIMsStatus(SIMData);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });

    it('verify refresh data usage success',function () {
        sessionStorage.setItem('accountId','2');

        var getSimsHandler = $httpBackend.expectPOST(/.*?api\/v1\/accounts\/account\/2\/sims\/datausage\/refresh?.*/g )
            .respond(200,"OK");

        var response = _SIMService.refreshDataUsage();

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });

    it('verify get sim details',function () {

        var simId = '1001100000000000000257';

        var getSimsHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/2\/sim\/1001100000000000000257?.*/g )
            .respond(200,{"type":"Sim","simId":"1001100000000000000257","simType":"GIGSKY_SIM","status":"ACTIVE","user":{"type":"User","userId":22,"firstName":"uberCalAdmin","lastName":"uberCalAdmin"},"account":{"type":"GROUP","accountId":4,"accountName":"UberCalifornia"},"nickName":"SIM-0257","dataUsedInBytes":99000000000});
        var queryParam = {
            details:"account|nickName|user"
        };
        var accountId = 2;
        var response = _SIMService.getSimDetail(accountId,simId,queryParam);

        response.success(function(data){
            expect(data.type).toEqual("Sim");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });

    it('verify get export data url',function () {
        var url = _SIMService.getExportDataUrl();
        expect(url).toContain('/accounts/account/2/sims');
    });

    it('verify get export data url (when userId is passed)',function () {
        var url = _SIMService.getExportDataUrl('12');
        expect(url).toContain('/accounts/account/2/user/12/sims');
    });
});