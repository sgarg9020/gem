describe('UsersFactory', function(){

    var $httpBackEnd;
    var _usersService;

    var user = {
        "type": "User",
        "userId": 29,
        "firstName": "uberCalUser7",
        "lastName": "uberCalUser7",
        "emailId": "uberCalUser7@uber.com",
        "address": {
            "type": "Address",
            "addressId": 35,
            "address1": "Kalmeshwar gudi oni",
            "address2": "Revan peth",
            "state": "Karnatak",
            "city": "Hebballi",
            "zipCode": "580112",
            "country": "IN"
        },
        "userStatus": "ACTIVE",
        "createdOn": "2015-12-02 13:58:16",
        "accountDetails": {
            "type": "EnterpriseAccountList",
            "enterpriseAccountDetails": [{
                "type": "Account",
                "rootAccountDetails": {
                    "type": "Account",
                    "accountId": 2,
                    "accountType": "ENTERPRISE",
                    "accountName": "Uber"
                },
                "parentAccountDetails": [{
                    "type": "Account",
                    "accountId": 4,
                    "accountType": "GROUP",
                    "accountName": "UberCalifornia",
                    "role": "ENTERPRISE_USER"
                }],
                "selfAccountDetails": {
                    "type": "Account",
                    "accountId": 429,
                    "accountType": "USER"
                }
            }]
        },
        "totalSimsAssociated": "0",
        "dataUsedInBytes": 0
    }
    var usersResponse = {
        "type": "UserList",
        "count": 1,
        "totalCount": 1,
        "list": [user]
    }
    beforeEach(module('app'));

    beforeEach(inject(function($injector,UserService){
        _usersService = UserService;
        $httpBackEnd = $injector.get('$httpBackend');
    }));

    it('verify get users success', function() {
        var getUsersHandler = $httpBackEnd.expectGET(/.*?api\/v1\/accounts\/account\/2\/users?.*/g )
            .respond(200,usersResponse);

        var queryParam = {count:10,search:"",startIndex:0};
        sessionStorage.setItem('accountId','2');
        var response = _usersService.getUsers(queryParam);

        response.success(function(data){
            expect(data.type).toEqual("UserList");
            expect(data.list.length).toEqual(1);
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackEnd.flush();
    });

    it('verify add user success(when group is not selected)', function() {
        var addUserHandler = $httpBackEnd.expectPOST(/.*?api\/v1\/accounts\/account\/2\/users?.*/g )
            .respond(200,"OK");

        var userRequest = {
            "type": "UserList",
            "list": [{
                "firstName": "test",
                "lastName": "test",
                "emailId": "testAdmin@gigsky.com",
                "type": "User"
            }]
        }
        var response = _usersService.addUser(userRequest);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackEnd.flush();
    });

    it('verify add user success (when group is selected)', function() {
        var addUserHandler = $httpBackEnd.expectPOST(/.*?api\/v1\/accounts\/account\/2\/users?.*/g )
            .respond(200,"OK");

        var userRequest = {
            "type": "UserList",
            "list": [{
                "firstName": "test",
                "lastName": "test",
                "emailId": "testAdmin@gigsky.com",
                "type": "User",
                "group": {
                    "type": "Account",
                    "accountId": 2,
                    "accountType": "GROUP",
                    "accountName": "UberIndia",
                    "parentAccountId": 1,
                    "accountSubscriptionType": "ENTERPRISE_PRO"
                }
            }]
        }
        var response = _usersService.addUser(userRequest);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackEnd.flush();
    });

    it('verify update user success',function () {

        var updateUserHandler = $httpBackEnd.expectPUT(/.*?api\/v1\/users\/29?.*/g )
            .respond(200,"OK");

        var user = {
            "type": "User",
            "userId": 29,
            "emailId": "testAdmin@gigsky.com",
            "firstName": "uberTest",
            "lastName": "test",
            "location": "Bangalore"
        }
        var response = _usersService.updateUser(user);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackEnd.flush();
    });

    it('verify update users success',function () {

        var updateUserHandler = $httpBackEnd.expectPUT(/.*?api\/v1\/accounts\/account\/2\/users?.*/g )
            .respond(200,"OK");

        var users = {
            "type": "UserList",
            "list": [{
                "type": "User",
                "userId": 29,
                "emailId": "testAdmin@gigsky.com",
                "firstName": "uberTest",
                "lastName": "test",
                "location": "Bangalore"
            }]
        }
        var response = _usersService.updateUsers(users);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackEnd.flush();
    });

    it('verify delete user success',function () {
        var deleteUserHandler = $httpBackEnd.expectDELETE(/.*?api\/v1\/accounts\/account\/2\/users?.*/g )
            .respond(200,"OK");
        var uidList = "12,13";
        var response = _usersService.deleteUsers(uidList);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackEnd.flush();
    });

    it('verify refresh data usage success',function () {

        var userHandler = $httpBackEnd.expectPOST(/.*?api\/v1\/accounts\/account\/2\/users\/datausage\/refresh?.*/g )
            .respond(200,"OK");

        var response = _usersService.refreshDataUsage();

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackEnd.flush();
    });

    it('verify get export data url',function () {
        var url = _usersService.getExportDataUrl();
        expect(url).toContain('/accounts/account/2/users');
    });


});