describe('data usage service', function () {

    var _dataUsageService;
    beforeEach(module('app'));
    beforeEach(inject(function ($injector, dataUsageService) {
        _dataUsageService = dataUsageService;
    }));

    it('should verify prepareDayWiseUsageData', inject(function () {

        var data = {"type":"DataUsageList","lastAvailableDate":"2016-06-10 00:00:00","groupByPeriod":"DAY","totalDataUsedInBytes":1212914,"totalDataUsageCost":0.0479,"list":[{"type":"DataUsage","dataUsedInBytes":4567,"fromDate":"2016-06-28 00:00:00","toDate":"2016-06-28 00:00:00","dataUsageCost":0.0044},{"type":"DataUsage","dataUsedInBytes":5000,"fromDate":"2016-06-18 00:00:00","toDate":"2016-06-18 00:00:00","dataUsageCost":0.0021},{"type":"DataUsage","dataUsedInBytes":5678,"fromDate":"2016-06-03 00:00:00","toDate":"2016-06-03 00:00:00","dataUsageCost":0.0002},{"type":"DataUsage","dataUsedInBytes":6010,"fromDate":"2016-06-24 00:00:00","toDate":"2016-06-24 00:00:00","dataUsageCost":0.0026},{"type":"DataUsage","dataUsedInBytes":7080,"fromDate":"2016-06-26 00:00:00","toDate":"2016-06-26 00:00:00","dataUsageCost":0.0068},{"type":"DataUsage","dataUsedInBytes":7600,"fromDate":"2016-06-20 00:00:00","toDate":"2016-06-20 00:00:00","dataUsageCost":0.0033},{"type":"DataUsage","dataUsedInBytes":12200,"fromDate":"2016-06-11 00:00:00","toDate":"2016-06-11 00:00:00","dataUsageCost":0.0012},{"type":"DataUsage","dataUsedInBytes":45000,"fromDate":"2016-06-01 00:00:00","toDate":"2016-06-01 00:00:00","dataUsageCost":0.0004},{"type":"DataUsage","dataUsedInBytes":60000,"fromDate":"2016-06-07 00:00:00","toDate":"2016-06-07 00:00:00","dataUsageCost":0.0057},{"type":"DataUsage","dataUsedInBytes":65678,"fromDate":"2016-06-04 00:00:00","toDate":"2016-06-04 00:00:00","dataUsageCost":0.0011},{"type":"DataUsage","dataUsedInBytes":123456,"fromDate":"2016-06-14 00:00:00","toDate":"2016-06-14 00:00:00","dataUsageCost":0.0118},{"type":"DataUsage","dataUsedInBytes":870645,"fromDate":"2016-06-10 00:00:00","toDate":"2016-06-10 00:00:00","dataUsageCost":0.0083}],"count":12,"totalCount":12};

        var expectedReturn = [{"bar":true,"usageUnit":"KB","values":[{"x":1464719400000,"y":43.95,"dataUsage":45000},{"x":1464805800000,"y":0,"dataUsage":0},{"x":1464892200000,"y":5.54,"dataUsage":5678},{"x":1464978600000,"y":64.14,"dataUsage":65678},{"x":1465065000000,"y":0,"dataUsage":0},{"x":1465151400000,"y":0,"dataUsage":0},{"x":1465237800000,"y":58.59,"dataUsage":60000},{"x":1465324200000,"y":0,"dataUsage":0},{"x":1465410600000,"y":0,"dataUsage":0},{"x":1465497000000,"y":850.24,"dataUsage":870645},{"x":1465583400000,"y":11.91,"dataUsage":12200},{"x":1465669800000,"y":0,"dataUsage":0},{"x":1465756200000,"y":0,"dataUsage":0},{"x":1465842600000,"y":120.56,"dataUsage":123456},{"x":1465929000000,"y":0,"dataUsage":0},{"x":1466015400000,"y":0,"dataUsage":0},{"x":1466101800000,"y":0,"dataUsage":0},{"x":1466188200000,"y":4.88,"dataUsage":5000},{"x":1466274600000,"y":0,"dataUsage":0},{"x":1466361000000,"y":7.42,"dataUsage":7600},{"x":1466447400000,"y":0,"dataUsage":0},{"x":1466533800000,"y":0,"dataUsage":0},{"x":1466620200000,"y":0,"dataUsage":0},{"x":1466706600000,"y":5.87,"dataUsage":6010},{"x":1466793000000,"y":0,"dataUsage":0},{"x":1466879400000,"y":6.91,"dataUsage":7080},{"x":1466965800000,"y":0,"dataUsage":0},{"x":1467052200000,"y":4.46,"dataUsage":4567},{"x":1467138600000,"y":0,"dataUsage":0},{"x":1467225000000,"y":0,"dataUsage":0}]}];

        var billingDate ={"billingStart":"2016-06-01 00:00:00","billingEnd":"2016-06-30 23:59:59"};
        expect(_dataUsageService.prepareDayWiseUsageData(data,billingDate.billingStart, billingDate.billingEnd )).toEqual(expectedReturn);
    }));


    it('should verify prepareDayWiseUsageData if no data', inject(function () {
        var billingDate ={"billingStart":"2015-10-20T18:54:15.000Z","billingEnd":"2015-11-20T18:54:15.000Z"};
        expect(_dataUsageService.prepareDayWiseUsageData( null, billingDate.billingStart, billingDate.billingEnd )).toEqual(undefined);
    }));


    it('should verify prepareZoneUsageData', inject(function () {
        var data = {"type":"AccountDataUsageList","count":8,"totalCount":8,"groupByPeriod":"MONTH","totalDataUsedInBytes":1212914,"totalDataUsageCost":0.0479,"list":[{"type":"DataUsageList","totalDataUsedInBytes":975645,"totalDataUsageCost":0.0093,"zoneId":1,"zoneName":"2-Zone1","zoneNickName":"2-Zone1123","list":[{"type":"DataUsage","dataUsedInBytes":975645,"fromDate":"2016-06-01 00:00:00","toDate":"2016-06-10 00:00:00","dataUsageCost":0.0093}]},{"type":"DataUsageList","totalDataUsedInBytes":195656,"totalDataUsageCost":0.0187,"zoneId":2,"zoneName":"2-Zone2","zoneNickName":"2-Zone2","list":[{"type":"DataUsage","dataUsedInBytes":195656,"fromDate":"2016-06-07 00:00:00","toDate":"2016-06-14 00:00:00","dataUsageCost":0.0187}]},{"type":"DataUsageList","totalDataUsedInBytes":18610,"totalDataUsageCost":0.008,"zoneId":3,"zoneName":"2-Zone3","zoneNickName":"2-Zone3","list":[{"type":"DataUsage","dataUsedInBytes":18610,"fromDate":"2016-06-18 00:00:00","toDate":"2016-06-24 00:00:00","dataUsageCost":0.008}]},{"type":"DataUsageList","totalDataUsedInBytes":11647,"totalDataUsageCost":0.0112,"zoneId":4,"zoneName":"2-Zone4","zoneNickName":"2-Zone4","list":[{"type":"DataUsage","dataUsedInBytes":11647,"fromDate":"2016-06-26 00:00:00","toDate":"2016-06-28 00:00:00","dataUsageCost":0.0112}]},{"type":"DataUsageList","totalDataUsedInBytes":5678,"totalDataUsageCost":0.0005,"zoneId":7,"zoneName":"2-Zone7","zoneNickName":"2-Zone7","list":[{"type":"DataUsage","dataUsedInBytes":5678,"fromDate":"2016-06-04 00:00:00","toDate":"2016-06-04 00:00:00","dataUsageCost":0.0005}]},{"type":"DataUsageList","totalDataUsedInBytes":5678,"totalDataUsageCost":0.0002,"zoneId":8,"zoneName":"2-Zone8","zoneNickName":"2-Zone8","list":[{"type":"DataUsage","dataUsedInBytes":5678,"fromDate":"2016-06-03 00:00:00","toDate":"2016-06-03 00:00:00","dataUsageCost":0.0002}]},{"type":"DataUsageList","totalDataUsedInBytes":0,"totalDataUsageCost":0,"zoneId":6,"zoneName":"2-Zone6","zoneNickName":"2-Zone6","list":[{"type":"DataUsage","dataUsedInBytes":0,"fromDate":"2016-06-01 00:00:00","toDate":"2016-06-27 00:00:00","dataUsageCost":0}]},{"type":"DataUsageList","totalDataUsedInBytes":0,"totalDataUsageCost":0,"zoneId":5,"zoneName":"2-Zone5","zoneNickName":"2-Zone5","list":[{"type":"DataUsage","dataUsedInBytes":0,"fromDate":"2016-06-01 00:00:00","toDate":"2016-06-27 00:00:00","dataUsageCost":0}]}]};

        var expectedReturn = [{"name":"2-Zone1123","dataUsage":975645},{"name":"2-Zone2","dataUsage":195656},{"name":"2-Zone3","dataUsage":18610},{"name":"2-Zone4","dataUsage":11647},{"name":"2-Zone7","dataUsage":5678},{"name":"2-Zone8","dataUsage":5678},{"name":"2-Zone6","dataUsage":0.000001},{"name":"2-Zone5","dataUsage":0.000001}];

        expect(_dataUsageService.prepareZoneUsageData(data)).toEqual(expectedReturn);
    }));


    it('should verify prepareZoneUsageData if no data', inject(function () {
        var expectedReturn = undefined;
        expect(_dataUsageService.prepareZoneUsageData()).toEqual(expectedReturn);
    }));


    it('should verify prepareGroupUsageData', inject(function () {
        var data = {"type":"AccountDataUsageList","count":3,"totalCount":3,"groupByPeriod":"MONTH","totalDataUsedInBytes":1212914,"totalDataUsageCost":0.0479,"list":[{"type":"DataUsageList","totalDataUsedInBytes":1212914,"totalDataUsageCost":0.0479,"accountId":2,"accountName":"Uber","accountType":"ENTERPRISE","list":[{"type":"DataUsage","dataUsedInBytes":1212914,"fromDate":"2016-06-01 00:00:00","toDate":"2016-06-28 00:00:00","dataUsageCost":0.0479}]},{"type":"DataUsageList","totalDataUsedInBytes":0,"totalDataUsageCost":0,"accountId":4,"accountName":"UberCalifornia","accountType":"GROUP","list":[{"type":"DataUsage","dataUsedInBytes":0,"fromDate":"2016-06-01 00:00:00","toDate":"2016-06-27 00:00:00","dataUsageCost":0}]},{"type":"DataUsageList","totalDataUsedInBytes":0,"totalDataUsageCost":0,"accountId":3,"accountName":"UberIndia","accountType":"GROUP","list":[{"type":"DataUsage","dataUsedInBytes":0,"fromDate":"2016-06-01 00:00:00","toDate":"2016-06-27 00:00:00","dataUsageCost":0}]}]};

        var expectedReturn = [{"name":"No Association","dataUsage":1212914},{"name":"UberCalifornia","dataUsage":0.000001},{"name":"UberIndia","dataUsage":0.000001}];

        expect(_dataUsageService.prepareGroupUsageData(data)).toEqual(expectedReturn);
    }));


    it('should verify prepareGroupUsageData if no data', inject(function () {
        var expectedReturn = undefined;
        expect(_dataUsageService.prepareGroupUsageData()).toEqual(expectedReturn);
    }));


    it('should verify prepareWeekWiseGroupUsageData', inject(function () {
        var data = {"type":"AccountDataUsageList","count":3,"totalCount":3,"groupByPeriod":"WEEK","totalDataUsedInBytes":1212914,"totalDataUsageCost":0.0478,"list":[{"type":"DataUsageList","totalDataUsedInBytes":0,"totalDataUsageCost":0,"accountId":4,"accountName":"UberCalifornia","accountType":"GROUP","list":[{"type":"DataUsage","dataUsedInBytes":0,"fromDate":"2016-06-01 00:00:00","toDate":"2016-06-27 00:00:00","dataUsageCost":0}]},{"type":"DataUsageList","totalDataUsedInBytes":0,"totalDataUsageCost":0,"accountId":3,"accountName":"UberIndia","accountType":"GROUP","list":[{"type":"DataUsage","dataUsedInBytes":0,"fromDate":"2016-06-01 00:00:00","toDate":"2016-06-27 00:00:00","dataUsageCost":0}]},{"type":"DataUsageList","totalDataUsedInBytes":1212914,"totalDataUsageCost":0.0478,"accountId":2,"accountName":"Uber","accountType":"ENTERPRISE","list":[{"type":"DataUsage","dataUsedInBytes":11647,"fromDate":"2016-06-26 00:00:00","toDate":"2016-06-28 00:00:00","week":26,"dataUsageCost":0.0112},{"type":"DataUsage","dataUsedInBytes":13610,"fromDate":"2016-06-20 00:00:00","toDate":"2016-06-24 00:00:00","week":25,"dataUsageCost":0.0058},{"type":"DataUsage","dataUsedInBytes":116356,"fromDate":"2016-06-01 00:00:00","toDate":"2016-06-04 00:00:00","week":22,"dataUsageCost":0.0017},{"type":"DataUsage","dataUsedInBytes":128456,"fromDate":"2016-06-14 00:00:00","toDate":"2016-06-18 00:00:00","week":24,"dataUsageCost":0.0139},{"type":"DataUsage","dataUsedInBytes":942845,"fromDate":"2016-06-07 00:00:00","toDate":"2016-06-11 00:00:00","week":23,"dataUsageCost":0.0152}]}]};

        var expectedReturn = {"from":"2016-05-31T18:30:00.000Z","usageUnit":"KB","to":"2016-06-30T18:29:59.000Z","data":[{"key":"UberCalifornia","bar":true,"disabled":false,"values":[{"x":1464978600000,"y":0,"dataUsage":0},{"x":1465583400000,"y":0,"dataUsage":0},{"x":1466188200000,"y":0,"dataUsage":0},{"x":1466793000000,"y":0,"dataUsage":0},{"x":1467397800000,"y":0,"dataUsage":0}]},{"key":"UberIndia","bar":true,"disabled":false,"values":[{"x":1464978600000,"y":0,"dataUsage":0},{"x":1465583400000,"y":0,"dataUsage":0},{"x":1466188200000,"y":0,"dataUsage":0},{"x":1466793000000,"y":0,"dataUsage":0},{"x":1467397800000,"y":0,"dataUsage":0}]},{"key":"No Association","bar":true,"disabled":false,"values":[{"x":1464978600000,"y":113.63,"dataUsage":116356},{"x":1465583400000,"y":920.75,"dataUsage":942845},{"x":1466188200000,"y":125.45,"dataUsage":128456},{"x":1466793000000,"y":13.29,"dataUsage":13610},{"x":1467397800000,"y":11.37,"dataUsage":11647}]}]};

        var billingDate ={"billingStart":"2016-06-01 00:00:00","billingEnd":"2016-06-30 23:59:59"};
        expect(JSON.stringify(_dataUsageService.prepareWeekWiseGroupUsageData(data,billingDate.billingStart, billingDate.billingEnd))).toEqual(JSON.stringify(expectedReturn));
    }));

    it('should verify prepareWeekWiseGroupUsageData if no data', inject(function () {
        var expectedReturn = undefined;
        var billingDate ={"billingStart":"2016-06-01 00:00:00","billingEnd":"2016-06-30 23:59:59"};
        expect(_dataUsageService.prepareWeekWiseGroupUsageData(null,billingDate.billingStart, billingDate.billingEnd)).toEqual(expectedReturn);
    }));

});