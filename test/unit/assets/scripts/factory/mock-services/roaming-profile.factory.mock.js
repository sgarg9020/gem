'use strict';

(function(){
    angular
        .module('app')
        .factory('roamingProfileServiceMock', RoamingProfileServiceMock);

    RoamingProfileServiceMock.$inject = ["commonUtilityService",'$q'];

    /**
     @function AlertsService()
     */
    function RoamingProfileServiceMock(commonUtilityService,$q){

        return {
            getAllowedIMSIProfiles:getAllowedIMSIProfiles,
            getCountries:getCountries,

            createRoamingProfileSet:createRoamingProfileSet,
            getRoamingProfileSetDetails:getRoamingProfileSetDetails,
            getRoamingProfileSetsList:getRoamingProfileSetsList,
            getRoamingProfileRevisionsList:getRoamingProfileRevisionsList,
            getRoamingProfileSetEnterpriseAccountsList:getRoamingProfileSetEnterpriseAccountsList,

            createRoamingProfile:createRoamingProfile,
            editRoamingProfile:editRoamingProfile,
            getRoamingProfileDetails:getRoamingProfileDetails,
            getRoamingProfileCoverageInfo:getRoamingProfileCoverageInfo,
            deleteRoamingProfile:deleteRoamingProfile,
            GetRoamingProfileEnterpriseAccountsList:GetRoamingProfileEnterpriseAccountsList,

            getRPAssociatedToEnterprise : getRPAssociatedToEnterprise,
            associateRPToEnterprise : associateRPToEnterprise,
            disassociateRPFromEnterprise : disassociateRPFromEnterprise,
            assignRoamingProfileToSims : assignRoamingProfileToSims,
            getEnterpriseRPUpdateSession : getEnterpriseRPUpdateSession,
            getEnterpriseRPUpdateSessionDetails : getEnterpriseRPUpdateSessionDetails,
            cancelEnterpriseRPUpdateSession : cancelEnterpriseRPUpdateSession,
            getEnterpriseRPUpdateSessionSimsBySessionId : getEnterpriseRPUpdateSessionSimsBySessionId

        };

        function getAllowedIMSIProfiles(){
           var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);

            var response = {
                "type" : "ImsiProfileSet",
                "list" : ["TDC", "Tele2","AT&T"]
            };

            defer.resolve({"data":response},false);
            return promise;

        }

        function getCountries(queryParam){
           var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);

            var response = {
                "type" : "CountriesList",
                "list" : [
                    {
                        "type" : "Country",
                        "id" : 12345,
                        "code" : "IN",
                        "name" : "India"
                    },{
                        "type" : "Country",
                        "id" : 12345,
                        "code" : "IN",
                        "name" : "India"
                    }
                ],
                "count": 20,
                "totalCount" : 300
            };

            defer.resolve({"data":response},false);
            return promise;
        }

        // Roaming profile set API's Start

        function createRoamingProfileSet(roamingProfileSet){
            var headerAdd=['CREATE_ROAMING_PROFILE_SET'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_PLAN_API_BASE_V3  + "rpset", 'POST',headerAdd,false,roamingProfileSet);
        }

        function getRoamingProfileSetDetails(rpSetId){
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);

            var response = {
                "type": "RoamingProfileSet",
                "id": 12345,
                "name": "GigSky 3.0 SIM Roaming Profile Set",
                "description": "Roaming profile applicable for Sim ver x.y with Imsis: x1, x2, x3",
                "profileType": "SINGLE_IMSI_PER_COUNTRY",
                "imsiProfileList": ["TDC", "Tele2"],
                "createdTime": "2016-12-31 23:59:59", /*in UTC*/
                "updatedTime": "2016-12-31 23:59:59", /*in UTC*/

                "revisionsCount": 8,
                "accountsCount": 5, /*accounts with which any of the revisions are associated with*/
                "simsCount": 123    /*sims applied with a roaming profile which is revision in the set*/
            };

            defer.resolve({"data":response},false);
            return promise;
        }

        function getRoamingProfileSetsList(queryParam){
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);

            var response = {
                "type": "RoamingProfileSetList",
                "count": 2,
                "totalCount": 20,
                "list": [
                    {
                        "type": "RoamingProfileSet",
                        "id": 12345,
                        "name": "GigSky 3.0 SIM Roaming Profile Set",
                        "description": "Roaming profile applicable for Sim ver x.y with Imsis: x1, x2, x3",
                        "profileType": "SINGLE_IMSI_PER_COUNTRY",
                        "imsiProfileList": ["TDC", "Tele2"],
                        "createdTime": "2016-12-31 23:59:59",
                        "updatedTime": "2016-12-31 23:59:59",
                        "revisionsCount": 8,
                        "accountsCount": 5,
                        "simsCount": 123
                    }, {
                        "type": "RoamingProfileSet",
                        "id": 12346,
                        "name": "GigSky 4.0 SIM Roaming Profile Set",
                        "description": "Roaming profile applicable for Sim ver x.y with Imsis: x1, x2, x3 Roaming profile applicable for Sim ver x.y with Imsis: x1, x2, x3",
                        "profileType": "SINGLE_IMSI_PER_COUNTRY",
                        "imsiProfileList": ["TDC", "Tele2"],
                        "createdTime": "2016-12-31 23:59:59",
                        "updatedTime": "2016-12-31 23:59:59",
                        "revisionsCount": 0,
                        "accountsCount": 0,
                        "simsCount": 0
                    }, {
                        "type": "RoamingProfileSet",
                        "id": 12347,
                        "name": "GigSky 5.0 SIM Roaming Profile Set",
                        "description": "Roaming profile applicable for Sim ver x.y with Imsis: x1, x2, x3",
                        "profileType": "MULTI_IMSI_PER_COUNTRY",
                        "imsiProfileList": ["TDC", "Tele2"],
                        "createdTime": "2016-12-31 23:59:59",
                        "updatedTime": "2016-12-31 23:59:59",
                        "revisionsCount": 8,
                        "accountsCount": 5,
                        "simsCount": 123
                    }
                ]
            };

            defer.resolve({"data":response},false);
            return promise;
        }

        function getRoamingProfileRevisionsList(rpSetId){
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);

            var response = {
                "type" : "RoamingProfileList",
                "totalCount" : "100",
                "count" : "10",
                "list" : [
                    {
                        "type" : "RoamingProfile",
                        "id" : 12345,
                        "revision" : "1.3",
                        "name" : "3IMSI-RC",
                        "description" : "Roaming profile description Roaming profile description",
                        "status" : "PENDING",
                        "accountsCount" : 5,
                        "simsCount" : 123,
                        "roamingProfileSet" : "3IMSI",
                        "roamingProfileSetId" : 1234,
                        "createdTime" : "2016-12-31 23:59:59",   /*in UTC*/
                        "updatedTime" : "2016-12-31 23:59:59",   /*in UTC*/
                    },{
                        "type" : "RoamingProfile",
                        "id" : 12346,
                        "revision" : "1.3",
                        "name" : "3IMSI-RC new",
                        "description" : "Roaming profile description",
                        "status" : "PUBLISHED",
                        "accountsCount" : 0,
                        "simsCount" : 0,
                        "roamingProfileSet" : "3IMSI",
                        "roamingProfileSetId" : 1234,
                        "createdTime" : "2016-12-31 23:59:59",   /*in UTC*/
                        "updatedTime" : "2016-12-31 23:59:59"   /*in UTC*/
                    }
                ]
            };

            defer.resolve({"data":response},false);
            return promise;

        }

        function getRoamingProfileSetEnterpriseAccountsList(rpSetId){

           var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);

            var response = {
                "type": "AccountList",
                "totalCount": "100",
                "count": "10",
                "list": [
                    {
                        "type": "AccountInfo",
                        "accountId": 12345,
                        "accountName": "Skyhigh Networks Pro Ltd.",
                        "simsCount": 123
                    }, {
                        "type": "AccountInfo",
                        "accountId": 12345,
                        "accountName": "Gigsky",
                        "simsCount": 123
                    }, {
                        "type": "AccountInfo",
                        "accountId": 12345,
                        "accountName": "some airlines",
                        "simsCount": 123
                    }
                ]
            };

            defer.resolve({"data":response},false);
            return promise;

        }

        // Roaming profile set API's End


        // Roaming profile API's Start

        function createRoamingProfile(RPSetId,roamingProfile){

            var headerAdd=['CREATE_ROAMING_PROFILE'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_PLAN_API_BASE_V3  + "rpset/" + RPSetId + "/rp", 'POST',headerAdd,false,roamingProfile);

        }

        function editRoamingProfile(roamingProfileId,roamingProfile){
            var headerAdd=['UPDATE_ROAMING_PROFILE'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_PLAN_API_BASE_V3  + "rp/" + roamingProfileId, 'PUT',headerAdd,false,roamingProfile);
        }

        function getRoamingProfileDetails(RPSetId,roamingProfileId){
            var url = ENTERPRISE_PLAN_API_BASE_V3 +'rpset/'+RPSetId+ "/rp/"+ roamingProfileId;

            var headerAdd=['GET_ROAMING_PROFILE'];
            return commonUtilityService.prepareHttpRequest(url, 'GET',headerAdd);
        }

        function getRoamingProfileCoverageInfo(RPSetId,versionId,queryParam){
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);
            var response = {};
            if (queryParam.details == "ONLY_IMSI_PROFILES") {
                response = {
                    "type": "RoamingProfile",
                    "id": 12345,
                    "revision": "1.3",
                    "name": "3IMSI-RC-1",
                    "description": "Roaming profile description",
                    "status": "PENDING", ///PUBLISHED/DELETED
                    "accountsCount": 5,
                    "simsCount": 123,
                    "roamingProfileSet": "3IMSI",
                    "roamingProfileSetId": 1234,
                    "createdTime": "2016-12-31 23:59:59", /*in UTC*/
                    "updatedTime": "2016-12-31 23:59:59", /*in UTC*/
                    "count": 10,
                    "totalCount": 100,
                    "list": [
                        {
                            "type": "RoamingProfileCoverage",

                            /*ONLY_IMSI_PROFILES*/
                            "imsiProfileName": "TDC",
                            "HLR-2G3G-ID": "HLR 2G 3G 1234",
                            "HLR-4G-ID": "HLR 4G 1234",
                            "dataEnabled": true,
                            "signallingEnabled": false,
                            "priority": 1
                        }, {
                            "type": "RoamingProfileCoverage",

                            /*ONLY_IMSI_PROFILES*/
                            "imsiProfileName": "Tele2",
                            "HLR-2G3G-ID": "HLR 2G 3G 1234",
                            "HLR-4G-ID": "HLR 4G 1234",
                            "dataEnabled": false,
                            "signallingEnabled": true,
                            "priority": 1
                        }
                    ]
                };
            }

            if(versionId == 12346){
                response.status= "PUBLISHED";
            }

            if (queryParam.details == "COUNTRY_IMSI_PROFILES") {
                response = {
                    "type": "RoamingProfile",
                    "id": 12346,
                    "revision": "1.3",
                    "name": "3IMSI-RC-2",
                    "description": "Roaming profile description",
                    "status": "PENDING",
                    "accountsCount": 5,
                    "simsCount": 123,
                    "roamingProfileSet": "3IMSI",
                    "roamingProfileSetId": 1234,
                    "createdTime": "2016-12-31 23:59:59", /*in UTC*/
                    "updatedTime": "2016-12-31 23:59:59", /*in UTC*/
                    "count": 10,
                    "totalCount": 100,
                    "list": [
                        {
                            "type": "RoamingProfileCoverage",

                            /*COUNTRY_IMSI_PROFILES*/
                            "countryCode": "IN",
                            "countryName": "India",
                            "imsiProfileName": "TDC",
                            "dataEnabled": true,
                            "signallingEnabled": false,
                            "priority": 1

                        }, {
                            "type": "RoamingProfileCoverage",

                            /*COUNTRY_IMSI_PROFILES*/
                            "countryCode": "IN",
                            "countryName": "India",
                            "imsiProfileName": "Tele2",
                            "dataEnabled": false,
                            "signallingEnabled": true,
                            "priority": 1
                        }
                    ]
                }
                ;
            }


            if (queryParam.details == "COUNTRY_IMSI_PROFILE_CARRIERS") {
                response = {
                    "type": "RoamingProfile",
                    "id": 12346,
                    "revision": "1.3",
                    "name": "3IMSI-RC-3",
                    "description": "Roaming profile description",
                    "status": "PENDING",
                    "accountsCount": 5,
                    "simsCount": 123,
                    "roamingProfileSet": "3IMSI",
                    "roamingProfileSetId": 1234,
                    "createdTime": "2016-12-31 23:59:59", /*in UTC*/
                    "updatedTime": "2016-12-31 23:59:59", /*in UTC*/
                    "count": 10,
                    "totalCount": 100,
                    "list": [
                        {
                            "type": "RoamingProfileCoverage",

                            /*COUNTRY_IMSI_PROFILE_CARRIERS*/
                            "countryCode": "IN",
                            "countryName": "India",
                            "imsiProfileName": "TDC",
                            "dataEnabled": true,
                            "signallingEnabled":false,
                            "priority": 1,
                            "mcc": "203",
                            "mnc": "001",
                            "tadigCode": "INDJB",
                            "name": "Aircel Karnataka",
                            "2G3GEnabled": true,
                            "4GEnabled": false
                        }, {
                            "type": "RoamingProfileCoverage",

                            /*COUNTRY_IMSI_PROFILE_CARRIERS*/
                            "countryCode": "IN",
                            "countryName": "India",
                            "imsiProfileName": "Tele2",
                            "dataEnabled": false,
                            "signallingEnabled": true,
                            "priority": 1,
                            "mcc": "203",
                            "mnc": "001",
                            "tadigCode": "INDJB",
                            "name": "Aircel Karnataka",
                            "2G3GEnabled": true,
                            "4GEnabled": true
                        }
                    ]
                }
                ;
            }

            defer.resolve({"data":response},false);
            return promise;
        }

        function deleteRoamingProfile(roamingProfileId){
            var url = ENTERPRISE_PLAN_API_BASE_V3 + "rp/"+ roamingProfileId;

            var headerAdd=['DELETE_ROAMING_PROFILE'];
            return commonUtilityService.prepareHttpRequest(url, 'DELETE',headerAdd);
        }

        function GetRoamingProfileEnterpriseAccountsList(versionId,queryParams){
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);

            var response = {
                "type": "AccountList",
                "totalCount": "100",
                "count": "10",
                "list": [
                    {
                        "type": "AccountInfo",
                        "accountId": 12345,
                        "accountName": "Skyhigh Networks Pro Ltd.",
                        "simsCount": 123
                    }, {
                        "type": "AccountInfo",
                        "accountId": 12345,
                        "accountName": "Gigsky",
                        "simsCount": 123
                    }, {
                        "type": "AccountInfo",
                        "accountId": 12345,
                        "accountName": "Some airlines",
                        "simsCount": 123
                    }
                ]
            };

            defer.resolve({"data":response},false);
            return promise;

        }
        // Roaming profile API's End

        //Roaming profile management API's for an enterprise account  Start

        function getRPAssociatedToEnterprise(enterpriseId,queryParams){
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);

            var response = {
                "type" : "RoamingProfileList",
                "totalCount" : "20",
                "count" : "10",
                "list" : [
                    {
                        "type" : "RoamingProfile",
                        "id" : 12345,
                        "revision" : "1.3",
                        "name" : "3IMSI-RC",
                        "description" : "Roaming profile description",
                        "status" : "PENDING",
                        "accountsCount" : 5,
                        "simsCount" : 123,
                        "roamingProfileSetName" : "3IMSI",
                        "roamingProfileSetId" : 1234,
                        "createdTime" : "2016-12-31 23:59:59",   /*in UTC*/
                        "updatedTime" : "2016-12-31 23:59:59"   /*in UTC*/
                    },{
                        "type" : "RoamingProfile",
                        "id" : 12346,
                        "revision" : "1.4",
                        "name" : "2IMSI-RC",
                        "description" : "Roaming profile description",
                        "status" : "PUBLISHED",
                        "accountsCount" : 0,
                        "simsCount" : 0,
                        "roamingProfileSetName" : "2IMSI",
                        "roamingProfileSetId" : 1235,
                        "createdTime" : "2016-12-31 23:59:59",   /*in UTC*/
                        "updatedTime" : "2016-12-31 23:59:59"  /*in UTC*/
                    }
                ]
            };

            defer.resolve({"data":response},false);
            return promise;

        }

        function associateRPToEnterprise(enterpriseId,roamingProfileId){

            var url = ENTERPRISE_PLAN_API_BASE_V3 + 'account/'+enterpriseId+'/rp';
            var headerAdd=['UPDATE_ROAMING_PROFILE_ACCOUNT_MAP'];
            var postData = { 'type' : 'RoamingProfile', 'id': roamingProfileId };

            return commonUtilityService.prepareHttpRequest(url, 'POST',headerAdd,null,postData);

        }

        function disassociateRPFromEnterprise(enterpriseId,roamingProfileId){

            var url = ENTERPRISE_PLAN_API_BASE_V3 + 'account/'+enterpriseId+'/rp/'+ roamingProfileId;
            var headerAdd=['UPDATE_ROAMING_PROFILE_ACCOUNT_MAP'];

            return commonUtilityService.prepareHttpRequest(url, 'DELETE',headerAdd);
        }

        function assignRoamingProfileToSims(enterpriseId,roamingProfileId,sims,queryParams){

            var url = ENTERPRISE_GIGSKY_BACKEND_BASE + 'accounts/'+enterpriseId+'/sims/rp';
            var headerAdd=['UPDATE_SIM_ROAMING_PROFILE'];
            var putData = { type: 'SimRoamingProfileUpdate', roamingProfileId : roamingProfileId };


            if(sims && sims.length){
                putData.iccIdsList=sims;
                return commonUtilityService.prepareHttpRequest(url, 'PUT',headerAdd,null,putData);
            }

            return commonUtilityService.prepareHttpRequest(url, 'PUT',headerAdd,queryParams,putData);

        }

        function getEnterpriseRPUpdateSession(enterpriseId,queryParams){
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);

            var response = {
                "type" : "SimRoamingProfileUpdateList",
                "count" : 10,
                "totalCount" : 20,
                "list" : [
                    {
                        "type" : "SimRoamingProfileUpdate",
                        "sessionId" : 1234,
                        "status" : "IN_PROGRESS",
                        "createdTime" : "2016-12-31 23:59:59",

                        "roamingProfile" :
                        {
                            "type" : "RoamingProfile",
                            "id" : 12345,
                            "revision" : "1.3",
                            "name" : "3IMSI-BETA",
                            "roamingProfileSetId" : 12345,
                            "roamingProfileSetName" : "GEM v.0 Roaming Profile"
                        },

                        "simsCount" : 103,
                        "errorMessage" : "Error in updating roaming profile for all sims",
                        "simStatusCount" : {
                            "COMPLETED" : 60,
                            "IN_PROGRESS" : 40,
                            "ERROR" : 3,
                            "CANCELLED" : 2
                        }
                    },
                    {
                        "type" : "SimRoamingProfileUpdate",
                        "sessionId" : 1234,
                        "status" : "CANCELLED",
                        "createdTime" : "2016-12-31 23:59:59",

                        "roamingProfile" :
                        {
                            "type" : "RoamingProfile",
                            "id" : 12345,
                            "revision" : "1.3",
                            "name" : "3IMSI-BETA",
                            "roamingProfileSetId" : 12345,
                            "roamingProfileSetName" : "GEM v.0 Roaming Profile"
                        },

                        "simsCount" : 500,
                        "errorMessage" : "Error in updating roaming profile for all sims",
                        "simStatusCount" : {
                            "COMPLETED" : 50,
                            "IN_PROGRESS" : 0,
                            "ERROR" : 3,
                            "CANCELLED" : 2
                        }
                    },
                ]
            };

            defer.resolve({"data":response},false);
            return promise;
        }


        function getEnterpriseRPUpdateSessionDetails(sessionId,queryParams){
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);

            var response = {
                "type" : "SimRoamingProfileUpdate",
                "sessionId" : 1234,
                "status" : "IN_PROGRESS",

                /*target roaming profile*/
                "roamingProfile" :
                {
                    "type" : "RoamingProfile",
                    "id" : 12345,
                    "revision" : "1.3",
                    "name" : "3IMSI-BETA",
                    "roamingProfileSetId" : 12345,
                    "roamingProfileSetName" : "GEM v.0 Roaming Profile"
                },

                "simsCount" : 103,
                "errorMessage" : "Error in updating roaming profile for all sims",
                "simStatusCount" : {
                    "COMPLETED" : 50,
                    "IN_PROGRESS" : 50,
                    "CANCELLED" : 2,
                    "ERROR" : 3
                },
                "errorSimList" : [
                    {
                        "type" : "ErrorSimInfo",
                        "iccId" : "1234567890123456789",
                        "errorMessage" : "Sim counter update failed as Subscriber is not found",
                    }
                ]

            };

            defer.resolve({"data":response},false);
            return promise;
        }

        function getEnterpriseRPUpdateSessionSimsBySessionId(sessionId, queryParams){
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);

            var response = {
                "type" : "SimRoamingProfileSimList",
                "count" : 10,
                "totalCount" : 100,


                "list" : [
                    {
                        "type" : "SimRoamingProfileSim",
                        "iccId" : "1234567890123456789",
                        "status" : "COMPLETED",
                        "errorMessage" : "Sim counter update failed as Subscriber is not found",

                        "roamingProfile" :
                        {
                            "type" : "RoamingProfile",
                            "id" : 12345,
                            "revision" : "1.3",
                            "name" : "3IMSI-BETA",
                            "roamingProfileSetId" : 12345,
                            "roamingProfileSetName" : "GEM v.0 Roaming Profile"
                        }
                    },
                    {
                        "type" : "SimRoamingProfileSim",
                        "iccId" : "1234567890123456789",
                        "status" : "IN_PROGRESS",
                        "errorMessage" : "Sim counter update failed as Subscriber is not found",

                        "roamingProfile" :
                        {
                            "type" : "RoamingProfile",
                            "id" : 12345,
                            "revision" : "1.3",
                            "name" : "2IMSI-BETA",
                            "roamingProfileSetId" : 12345,
                            "roamingProfileSetName" : "GEM v.0 Roaming Profile"
                        }
                    },{
                        "type" : "SimRoamingProfileSim",
                        "iccId" : "1234567890123456789",
                        "status" : "ERROR",
                        "errorMessage" : "Sim counter update failed as Subscriber is not found",

                        "roamingProfile" :
                        {
                            "type" : "RoamingProfile",
                            "id" : 12345,
                            "revision" : "1.3",
                            "name" : "2IMSI-BETA",
                            "roamingProfileSetId" : 12345,
                            "roamingProfileSetName" : "GEM v.0 Roaming Profile"
                        }
                    },
                    {
                        "type" : "SimRoamingProfileSim",
                        "iccId" : "1234567890123456789",
                        "status" : "IN_PROGRESS",
                        "errorMessage" : "Sim counter update failed as Subscriber is not found",

                        "roamingProfile" :
                        {
                            "type" : "RoamingProfile",
                            "id" : 12345,
                            "revision" : "1.3",
                            "name" : "2IMSI-BETA",
                            "roamingProfileSetId" : 12345,
                            "roamingProfileSetName" : "GEM v.0 Roaming Profile"
                        }
                    }
                ]
            };

            defer.resolve({"data":response},false);
            return promise;
        }

        function cancelEnterpriseRPUpdateSession(sessionId){

            var url = ENTERPRISE_GIGSKY_BACKEND_BASE +'sims/rp/session/'+sessionId;
            var headerAdd=['DELETE_SIM_ROAMING_PROFILE_SESSION'];

            return commonUtilityService.prepareHttpRequest(url, 'DELETE',headerAdd);

        }
        //Roaming profile management API's for an enterprise account End

    }
})();