'use strict';

(function(){
    angular
        .module('app')
        .factory('SIMServiceMock',['commonUtilityService','$q',SIMServiceMock]);

    /**
     @function debounce()
     @param {object} $timeout - angular timeout service
     - Factory method for functions to be executed after a delay
     */
    function SIMServiceMock(commonUtilityService,$q){

        var factory = {

            getSIMs:getSIMs,
            getSIMsByUserId:getSIMsByUserId,
            updateSIM:updateSIM,
            updateSIMs:updateSIMs,
            updateSIMsStatus:updateSIMsStatus,
            refreshDataUsage:refreshDataUsage,
            getExportDataUrl:getExportDataUrl,
            getSimDetail:getSimDetail,
            getSIMLocationHistory:getSIMLocationHistory,
            getSIMCdrs:getSIMCdrs,
            switchIMSIMode:switchIMSIMode,
            switchIMSI:switchIMSI,
            getSIMOTAHistory:getSIMOTAHistory,
            getSIMVersions:getSIMVersions
        };

        return factory;


        function getSIMs(queryParam,accountId) {
            var accountId  = ( accountId !== undefined ) ? accountId : sessionStorage.getItem('accountId');
            var headerAdd=['SIM_READ'];
            var config = {
                cancellable : 'searchApi'
            };
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+ accountId +"/sims", 'GET',headerAdd,queryParam,undefined,config);
        }

        function getSIMsByUserId(userId,queryParam) {
            var headerAdd=['SIM_READ'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+(sessionStorage.getItem('rootAccountId')?sessionStorage.getItem('rootAccountId'):sessionStorage.getItem('accountId'))+"/user/"+userId+'/sims/', 'GET',headerAdd,queryParam);
        }

        function updateSIM(SIM) {
            SIM.type = "Sim";
            var headerAdd=['SIM_UPDATE_ASSOCIATION'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_API_BASE + "account/"+ sessionStorage.getItem('accountId') + "/sims", 'PUT',headerAdd,false,SIM);
        }

        function updateSIMs(SIMs) {
            var headerAdd=['SIM_UPDATE_ASSOCIATION'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + 'accounts/account/'+sessionStorage.getItem('accountId')+'/sims', 'PUT',headerAdd,false,SIMs);
        }


        function updateSIMsStatus( SIMs ) {
            var headerAdd=['SIM_ACTIVATION'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+ sessionStorage.getItem('accountId') +"/sims/status", 'PUT',headerAdd,false,SIMs);
        }

        function refreshDataUsage(){
            var headerAdd=['REFRESH_DATAUSAGE'];
            var accId = sessionStorage.getItem('accountId');
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE  + "accounts/account/" + accId  + "/sims/datausage/refresh", 'POST',headerAdd);
        }

        function getExportDataUrl(uid){
            var exportDataUrl = ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+sessionStorage.getItem('accountId')+"/sims?";

            if(uid){
                exportDataUrl = ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+(sessionStorage.getItem('rootAccountId')?sessionStorage.getItem('rootAccountId'):sessionStorage.getItem('accountId'))+"/user/"+uid+'/sims?'
            }

            return exportDataUrl;
        }
        function getSimDetail(accountId,SIMId,queryParam){
            var headerAdd=['SIM_READ'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/sim/"+SIMId, 'GET',headerAdd,queryParam);
        }

        function getSIMLocationHistory(accountId,SIMId,queryParam) {
            var headerAdd=['SIM_LOCATION_READ'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/sim/"+SIMId+"/locations",'GET',headerAdd,queryParam);
        }

        function getSIMCdrs(accountId,SIMId,queryParam) {
            var headerAdd=['SIM_CDR_READ'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/sim/"+SIMId+"/callDataRecords",'GET',headerAdd,queryParam);
        }
        function switchIMSIMode(accountId,simsConf){
            var headerAdd=['SIM_EDIT_ADVANCED'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+ accountId +"/sims/conf", 'PUT',headerAdd,false,simsConf);
        }

        function switchIMSI(accountId,SIMId,imsiConf){
            var headerAdd=['SIM_EDIT_ADVANCED'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+ accountId +"/sim/"+SIMId+"/switchImsi", 'POST',headerAdd,false,imsiConf);
        }
        function getSIMOTAHistory(accountId,SIMId,queryParam){
            var headerAdd=['SIM_READ_ADVANCED'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/sim/"+SIMId+"/otaMessages",'GET',headerAdd,queryParam);
        }

        function getSIMVersions(){

            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);

            var response = {
                "type" : "SimVersions",
                "count" : 5,
                "totalCount" : 15,
                "list" : [
                    {
                        "type" : "SimVersion",
                        "version" : "3.0",
                        "imsiProfileList" : ["TDC", "Tele2"]
                    },
                    {
                        "type" : "SimVersion",
                        "version" : "4.0",
                        "imsiProfileList" : ["TDC", "Tele2",'AT&T']
                    }
                ]
            };

            defer.resolve({"data":response},false);
            return promise;

        }
    }
})();