'use strict';

(function(){
    angular
        .module('app')
        .factory('AccountServiceMock',['$q','commonUtilityService',AccountServiceMock]);

    function AccountServiceMock($q,commonUtilityService){

        var factory = {

            getAccountDetails:getAccountDetails,
            updateAccountDetails:updateAccountDetails,
            updatePassword:updatePassword,
            getBillingCycle: getBillingCycle,
            addEnterprise:addEnterprise,
            getRoles:getRolesForAccount
        };

        return factory;

        function getAccountDetails(queryParam,noCancelOnRouteChange){
            var enterpriseId = sessionStorage.getItem("accountId");
            var rootAccountId = sessionStorage.getItem("rootAccountId")
            var userId = sessionStorage.getItem('userId');

            var headerAdd=['USER_READ'];
            if(noCancelOnRouteChange) {
                var config = {
                    noCancelOnRouteChange: true
                };

                return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE+'accounts/account/'+(rootAccountId?rootAccountId:enterpriseId)+'/user/'+userId, 'GET',headerAdd,queryParam,undefined,config);
            }else{
                return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE+'accounts/account/'+(rootAccountId?rootAccountId:enterpriseId)+'/user/'+userId, 'GET',headerAdd,queryParam);
            }

        }

        function updateAccountDetails(account){
            var accountRequest = {
                type: "User",
                firstName: account.firstName,
                lastName: account.lastName,
                emailId: account.emailId,
                address:{
                    type:"Address",
                    addressId: account.addressId,
                    country:account.country.code
                }
            };
            var headerAdd=['USER_PROFILE_EDIT'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_USER_API_BASE + 'users/user/' + account.userId, 'PUT',headerAdd,false,accountRequest);
        }

        function updatePassword(oldPassword,newPassword){
            var data = {
                type : "AccountPassword",
                oldPassword : oldPassword,
                newPassword : newPassword
            }
            var userId = sessionStorage.getItem('userId');
            var headerAdd=['USER_PROFILE_EDIT'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_USER_API_BASE + 'users/user/' + userId + '/password', 'PUT',null,false,data);
        }

        function getBillingCycle() {
            var enterpriseId = sessionStorage.getItem("accountId");
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null, null, defer);
            var timezone = sessionStorage.getItem("timezone");
            if (!timezone) {
                var queryParam = {};
                queryParam.details = 'company';
                commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + 'accounts/account/' + enterpriseId, "GET",null,queryParam).success(function (response) {
                    sessionStorage.setItem('timezone', response.company.timeZone);
                    defer.resolve({"data": getBillingCycleForTimezone(response.company.timeZone)});
                }).error(function (data) {
                    defer.reject({"data": data});
                })
            } else {
                defer.resolve({"data": getBillingCycleForTimezone(timezone)}, false);
            }
            return promise;
        }
        function getBillingCycleForTimezone(timezone){
            var timezoneArr = timezone.split(':');
            var sign = parseInt(timezoneArr[0])?parseInt(timezoneArr[0])<0?-1:1:0;
            var curDate = window.sessionStorage.getItem("date");
            var date = new Date();
            if(curDate!=null)
                date = new Date(curDate);

            date.setTime(date.getTime()+sign*(Math.abs(parseInt(timezoneArr[0])*60*60*1000)+Math.abs(parseInt(timezoneArr[1])*60*1000)));

            var firstDay = new Date(date.getUTCFullYear(), date.getUTCMonth(), 1);
            var lastDay = new Date(date.getUTCFullYear(), date.getUTCMonth() + 1, 0);
            var fromDate = firstDay.getFullYear() + '-' + ("0" + (firstDay.getMonth() + 1)).slice(-2) + '-' + ("0" + firstDay.getDate()).slice(-2) + ' 00:00:00';
            var toDate = lastDay.getFullYear() + '-' + ("0" + (lastDay.getMonth() + 1)).slice(-2) + '-' + ("0" + lastDay.getDate()).slice(-2) + ' 23:59:59';
            date=new Date(date.getUTCFullYear(),date.getUTCMonth(),date.getUTCDate(),date.getUTCHours());
            return {billingStart: fromDate, billingEnd: toDate,currentDate:date};
        }

        function addEnterprise(enterprise){
            var enterpriseInfo = {
                type: 'Account',
                accountType: 'ENTERPRISE',
                accountName: enterprise.accountName,
                company: {
                    type: "Company",
                    name: enterprise.companyName,
                    currency: enterprise.currency.code,
                    timeZone: enterprise.timezoneDesc.offset,
                    timeZoneInfo: enterprise.timezoneDesc.description,
                    address: {
                        type: "Address",
                        city: enterprise.city,
                        address1: enterprise.address1,
                        address2: enterprise.address2,
                        zipCode: enterprise.postalCode,
                        state: enterprise.state,
                        country: enterprise.country.code
                    }
                },
                accountSubscriptionType:enterprise.accountSubscriptionType
            };

            enterpriseInfo.accountSubscriptionType = (enterprise.accountSubscriptionType)?enterprise.accountSubscriptionType.replace( "GEM ", "ENTERPRISE_" ):enterprise.accountSubscriptionType;

            var enterpriseRequest = { type: 'AccountList', list:[enterpriseInfo] };

            var headerAdd=['ACCOUNT_CREATE'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE  + 'accounts/account/', 'POST',headerAdd,null,enterpriseRequest);

        }

        function getRolesForAccount(accType){
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);

            var response = {
                "type" : "RoleList",
                "list" : [
                    "ENTERPRISE_ADMIN", "ENTERPRISE_USER"]
            };

            defer.resolve({"data":response},false);
            return promise;
        }




    }
})();