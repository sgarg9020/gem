'use strict';

(function(){
    angular
        .module('app')
        .factory('SettingsServiceMock',['$q','commonUtilityService','AuthenticationService',SettingsService]);



    function SettingsService($q,commonUtilityService,AuthenticationService){

        var factory = {
            getPlanDetails:getPlanDetails,
            getEnterpriseDetails:getEnterpriseDetails,
            updateEnterpriseDetails:updateEnterpriseDetails,
            submitAlertConfig:submitAlertConfig,
            getAlertConfig:getAlertConfig,
            getPossibleAlertConfigs:getPossibleAlertConfigs,
            getAlertPercentageObj:getAlertPercentageObj,
            getAlertIntervalObj:getAlertIntervalObj,
            updateZoneDetail:updateZoneDetail,
            getZonesCount:getZonesCount,
            addContact:addContact,
            getApprovedContacts:getApprovedContacts,
            updateContacts:updateContacts,
            deleteContacts:deleteContacts,
            preparePricingRange:preparePricingRange,
            formatDataSize:formatDataSize,
            getDocuments: getDocuments,
            deleteDocument: deleteDocument,
            getEnterpriseSubscriptionTypes:getEnterpriseSubscriptionTypes

        };

        return factory;

        function getPlanDetails(details,accountId){
            var enterpriseId = accountId ? accountId : sessionStorage.getItem("accountId");
            var queryParam = {};
            queryParam.startIndex = 0;
            queryParam.count = 10000;

            if(details){
                queryParam.details = details;
            }else{
                queryParam.details = 'countryList';
            }

            var headerAdd=['READ_ZONES'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_PLAN_API_BASE+'account/'+enterpriseId+'/zones', 'GET',headerAdd,queryParam);
        }

        function getZonesCount(){
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);
            var zonesCount = sessionStorage.getItem('gs-zonesCount') || 0;

            if(!zonesCount){
                var enterpriseId = sessionStorage.getItem("accountId");
                var details = 'name';
                getPlanDetails(details,enterpriseId).success(function(response){
                    sessionStorage.setItem('gs-zonesCount', response.count);
                    defer.resolve({"data":response.count});
                }).error(function(data,status){
                    defer.reject({"data":data});
                });
            } else {
                console.log('zones count from session storage');
                defer.resolve({"data":JSON.parse(zonesCount)},false);
            }
            return promise;
        }

        function getEnterpriseDetails(enterpriseId,queryParam,noCancelOnRouteChange){
            var headerAdd=['ACCOUNT_READ'];
            if(noCancelOnRouteChange){
                var config = {
                    noCancelOnRouteChange : true
                };
                return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE+'accounts/account/'+enterpriseId, 'GET',headerAdd,queryParam,undefined,config);
            }else{
                return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE+'accounts/account/'+enterpriseId, 'GET',headerAdd,queryParam);
            }
        }

        function updateEnterpriseDetails(isAlertConfig, enterprise) {
            var enterpriseRequest = {};
            var enterpriseId;
            if (isAlertConfig) {
                enterpriseId = sessionStorage.getItem("accountId");
                enterpriseRequest = {
                    type: 'Account',
                    alertSetting: {
                        type: "alertSetting",
                        alertEnabled: enterprise.alertAccount,
                        alertIntervalInSecs: enterprise.interval.time,
                        alertEmailForUserEnabled: enterprise.alertUserCheck,
                        defaultCreditLimitForSims: [
                            {
                                type: "creditLimit",
                                limitType: "SOFT",
                                alertUserByMail: enterprise.alertUserCheck,
                                alertEnabled: enterprise.alertAccount,
                                alertPercentages: [enterprise.qualifier.percentage]

                            }]

                    }
                }
            } else {
                enterpriseId = enterprise.enterpriseId;
                enterpriseRequest = {
                    type: 'Account',
                    company: {
                        type: "Company",
                        name: enterprise.companyName,
                        address: {
                            type: "Address",
                            city: enterprise.city,
                            address1: enterprise.address1,
                            address2: enterprise.address2,
                            zipCode: enterprise.postalCode,
                            state: enterprise.state,
                            country: enterprise.country.code
                        }
                    }
                };

                if(enterprise.accountName){
                    enterpriseRequest.accountName = enterprise.accountName;
                }

                if(enterprise.status == 'INACTIVE' && enterpriseRequest.company){
                    enterpriseRequest.company.timeZone = enterprise.timezoneDesc.offset;
                    enterpriseRequest.company.timeZoneInfo = enterprise.timezoneDesc.description;
                    enterpriseRequest.company.currency = enterprise.currency.code;
                }

                var permissionList = ['ACCOUNT_SUBSCRIPTION_TYPE_EDIT'];
                var isAllowed = AuthenticationService.isOperationAllowed(permissionList);
                if(isAllowed){
                    enterpriseRequest.accountSubscriptionType = enterprise.accountSubscriptionType.replace( "GEM ", "ENTERPRISE_" );
                }

                var statusUpdatePermission = ['ACCOUNT_STATUS_EDIT'];
                var isStatusUpdateAllowed = AuthenticationService.isOperationAllowed(statusUpdatePermission);
                if(isStatusUpdateAllowed){
                    enterpriseRequest.status = enterprise.status;
                }

            }
            var headerAdd=['ACCOUNT_EDIT'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + 'accounts/account/' + enterpriseId, 'PUT',headerAdd,false,enterpriseRequest);
        }

        function submitAlertConfig(alertConfig){
            var requestData ={};
            var enterpriseId = sessionStorage.getItem("accountId");
            requestData.enterprise = {
                enterpriseId:enterpriseId
            };
            requestData.frequency =alertConfig.interval;
            requestData.trigger =alertConfig.qualifier;
            requestData.alertuser =alertConfig.alertUserCheck;
            requestData.alertAccount =alertConfig.alertAccount;
            var headerAdd=['ENTERPRISE_SIM_USAGE_ALERT_WRITE'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_API_BASE + 'enterprise/alertConfig/' + enterpriseId, 'PUT',headerAdd,false,requestData);
        };
        function getAlertConfig(){
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);
            var enterpriseId = sessionStorage.getItem("accountId");

            var headerAdd=['ENTERPRISE_SIM_USAGE_ALERT_READ'];
            commonUtilityService.prepareHttpRequest(ENTERPRISE_API_BASE + 'enterprise/alertConfig/'+enterpriseId, 'GET',headerAdd).success(function(response){
                defer.resolve({"data":response});
            }).error(function(data,status){
                defer.reject({"data":data});
            });
            return promise;
        }
        function getPossibleAlertConfigs(){
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);
            var enterpriseId = sessionStorage.getItem("accountId");
            var possibleValues = {
                "simAlertUsage": [
                    {
                        "percentage": 75,
                        "qualifier": "Usage is >= 75% of SIM allocation"
                    },
                    {
                        "percentage": 90,
                        "qualifier": "Usage is >= 90% of SIM allocation"
                    },
                    {
                        "percentage": 100,
                        "qualifier": "Usage is >= 100% of SIM allocation"
                    }
                ],
                "alertIntervals": [
                    {
                        "time": 3600,
                        "interval": "Hourly"
                    },
                    {
                        "time": 7200,
                        "interval": "Every 2 Hours"
                    },
                    {
                        "time": 10800,
                        "interval": "Every 3 Hours"
                    },
                    {
                        "time": 43200,
                        "interval": "Every 12 Hours"
                    },
                    {
                        "time": 86400,
                        "interval": "Once daily"
                    },
                    {
                        "time": 604800,
                        "interval": "Once weekly"
                    }
                ]
            };
           /* $http.get(ENTERPRISE_API_BASE + 'enterprise/possibleAlertConfig/'+enterpriseId).success(function(response){
                defer.resolve({"data":response});
            }).error(function(data,status){
                defer.reject({"data":data});
            });*/
            defer.resolve({data:possibleValues});
            return promise;
        }
        function getAlertIntervalObj(alertIntervalList,alertIntervalInSecs){
            var i=0;
            for(i;i<alertIntervalList.length;i++){
                if(alertIntervalList[i].time === alertIntervalInSecs){
                    return alertIntervalList[i];
                }
            }
            return alertIntervalList[0];

        }
        function getAlertPercentageObj(alertPercentageList,alertPercentage){
            var i=0;
            for(i;i<alertPercentageList.length;i++){
                if(alertPercentageList[i].percentage === alertPercentage){
                    return alertPercentageList[i];
                }
            }
            return alertPercentageList[0];

        }
        function updateZoneDetail(zoneInfo,zoneId,accountId){
            var headerAdd=['ACCOUNT_EDIT'];
            var enterpriseId = accountId ? accountId : sessionStorage.getItem("accountId");
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_PLAN_API_BASE + 'account/' + enterpriseId+'/zones/zone/'+zoneId, 'PUT',headerAdd,false,zoneInfo);
        }

        function getApprovedContacts(accountId){
            var headerAdd=['ACCOUNT_READ'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/approvedContacts", 'GET',headerAdd);
        }

        function addContact(contact,accountId) {
            var headerAdd=['ACCOUNT_EDIT'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/approvedContacts", 'POST',headerAdd,false,contact);
        }

        function updateContacts(contact,accountId){
            var headerAdd=['ACCOUNT_EDIT'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/approvedContacts", 'PUT',headerAdd,false,contact);
        }

        function deleteContacts(contact,accountId){
            var headerAdd=['ACCOUNT_EDIT'];
            var config = {
                contentType : "application/json"
            }
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE + "accounts/account/"+accountId+"/approvedContacts", 'DELETE',headerAdd,false,contact,config);
        }

        function preparePricingRange(startRange, endRange, unitType){
            if(startRange==0 && endRange==null){
                return '0 - <span class="infinity v-align-middle">&infin;</span>';
            }
            if(endRange==null){
                startRange = formatDataSize(commonUtilityService.getUsageInBytes(startRange,unitType));
                return 'More than '+startRange;
            }
            startRange = (startRange!=0)?formatDataSize(commonUtilityService.getUsageInBytes(startRange,unitType)):0;
            endRange = formatDataSize(commonUtilityService.getUsageInBytes(endRange,unitType));

            return startRange +' - '+ endRange;
        }


        function formatDataSize(dataUsedInBytes) {
            var usageUnit = commonUtilityService.getDataUsageUnit(dataUsedInBytes);
            var usageInUnit = commonUtilityService.getUsageInUnit(dataUsedInBytes, usageUnit);
            return usageInUnit + ' ' + usageUnit;
        }

        function getDocuments(accountId,queryParam){
            var headerAdd=['ACCOUNT_DOCUMENT_READ'];
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE+'accounts/account/'+accountId+'/docs', 'GET',headerAdd,queryParam);
        }

        function deleteDocument(accountId,docId){
            var headerAdd=['ACCOUNT_DOCUMENT_EDIT'];
            var token = sessionStorage.getItem("token");
            return commonUtilityService.prepareHttpRequest(ENTERPRISE_GIGSKY_BACKEND_BASE+'accounts/account/'+accountId+'/docs?docId='+docId+'&token='+encodeURIComponent(token), 'DELETE',headerAdd);
        }

        function getEnterpriseSubscriptionTypes(){
            var defer = $q.defer();
            var promise = commonUtilityService.extendHttpPromise(null,null,defer);

            var response = {
                "type" : "SubscriptionType",
                "list" : [
                    "ENTERPRISE_LITE", "ENTERPRISE_PRO", "ENTERPRISE_PRO+", "ENTERPRISE_RESELLER"
                ]
            };

            var subscriptionTypes = [];
            for(var i=0;i<response.list.length;i++){
                subscriptionTypes.push({name:response.list[i]});
            }
            defer.resolve({"data":subscriptionTypes},false);
            return promise;
        }
    }
})();