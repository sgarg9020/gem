describe('Invoice service api', function () {

    var _InvoiceService, $httpBackend;
    var invoices = [
        {
            "invoiceNumber": "sample-invoice-1.pdf",
            "url": "/enterprise/app/data/sampleInvoice.pdf",
            "generatedDate": {"$date":"2015-10-20T00:00:00.000Z"}
        },
        {
            "invoiceNumber": "sample-invoice-2.pdf",
            "url": "/enterprise/app/data/sampleInvoice.pdf",
            "generatedDate": {"$date":"2015-09-20T00:00:00.000Z"}
        }];

    var monthlyUsageReports = [
        {
            "reportId" : "1234",
            "fileName": "sample-invoice-1.pdf",
            "url": "/enterprise/app/data/sampleInvoice.pdf",
            "generatedDate": {"$date":"2015-10-20T00:00:00.000Z"}
        },
        {
            "reportId" : "5678",
            "fileName": "sample-invoice-2.pdf",
            "url": "/enterprise/app/data/sampleInvoice.pdf",
            "generatedDate": {"$date":"2015-09-20T00:00:00.000Z"}
        }];


    beforeEach(module('app'));
    beforeEach(inject(function ($injector, InvoiceService) {
        _InvoiceService = InvoiceService;
        $httpBackend = $injector.get('$httpBackend');

    }));

    it('verify get invoices success', inject(function () {

        var getInvoicesHandler = $httpBackend.expectGET(/.*?api\/v1\/account\/1234\/invoice?.*/g )
            .respond(200,{"type":"Invoices","startIndex":"0","totalCount":2,"count":2,"list":invoices});
        var queryParam = {count:2,search:"",startIndex:0};
        sessionStorage.setItem('accountId','1234');
        var response = _InvoiceService.getInvoices(queryParam);

        response.success(function(data){
            expect(data.type).toEqual("Invoices");
            expect(data.list.length).toEqual(2);
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

    it('verify get invoices failure', inject(function () {

        var getInvoicesHandler = $httpBackend.expectGET(/.*?api\/v1\/account\/1234\/invoice?.*/g )
            .respond(500,{});
        var queryParam = {count:2,search:"",startIndex:0};
        sessionStorage.setItem('accountId','1234');
        var response = _InvoiceService.getInvoices(queryParam);

        response.success(function(data){
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

    it('verify get monthly reports success', inject(function () {
        var accountId = 1;

        var getInvoicesHandler = $httpBackend.expectGET(/.*?api\/v1\/account\/1234\/report?.*/g )
            .respond(200,{"type":"AccountUsageReportList","totalCount":2,"count":2,"list":monthlyUsageReports});
        var queryParam = {count:2,search:"",startIndex:0};
        sessionStorage.setItem('accountId','1234');
        var response = _InvoiceService.getMonthlyUsageReports(queryParam);

        response.success(function(data){
            expect(data.type).toEqual("AccountUsageReportList");
            expect(data.list.length).toEqual(2);
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

    it('verify get monthly reports failure', inject(function () {
        var accountId = 100;

        var getInvoicesHandler = $httpBackend.expectGET(/.*?api\/v1\/account\/1234\/report?.*/g )
            .respond(500,{});
        var queryParam = {count:2,search:"",startIndex:0};
        sessionStorage.setItem('accountId','1234');
        var response = _InvoiceService.getMonthlyUsageReports(queryParam);

        response.success(function(data){
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

});