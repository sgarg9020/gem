describe('Alerts service api', function () {

    var _AlertsService, $httpBackend;
    var alert = {
        "type": "AlertConfiguration",
        "accountId": 2,
        "accountName": "Uber",
        "alertType": "ACCOUNT_CUMULATIVE",
        "triggerCount": 311,
        "alertSpecification": {
            "type": "AlertSpecification",
            "enable": false,
            "limitValue": 20.00,
            "limitType": "COST",
            "actions": ["EMAIL"]
        }
    };
    var MockAlertsResponse = {
        "type": "AlertConfigurationList",
        "count": 2,
        "totalCount": 2,
        "list": [{
            "type": "AlertConfiguration",
            "alertId": 1,
            "accountId": 2,
            "accountName": "Uber",
            "alertType": "ACCOUNT_CUMULATIVE",
            "triggerCount": 311,
            "alertSpecification": {
                "type": "AlertSpecification",
                "enable": false,
                "limitValue": 20.00,
                "limitType": "COST",
                "actions": ["EMAIL"]
            },
            "updatedTime": "2016-06-21 05:04:27"
        }, {
            "type": "AlertConfiguration",
            "alertId": 4,
            "accountId": 2,
            "accountName": "Uber",
            "alertType": "ACCOUNT_CUMULATIVE",
            "triggerCount": 311,
            "alertSpecification": {
                "type": "AlertSpecification",
                "enable": false,
                "limitValue": 4.00,
                "limitType": "COST",
                "actions": ["EMAIL"]
            }
        }]
    }



    beforeEach(module('app'));
    beforeEach(inject(function ($injector, alertsService) {
        _AlertsService = alertsService;
        $httpBackend = $injector.get('$httpBackend');

    }));

    it('verify add alert success', function () {
        sessionStorage.setItem('accountId','2');
        var getAlertsHandler = $httpBackend.expectPOST(/.*?api\/v1\/accounts\/account\/2\/alerts?.*/g)
            .respond(200,"OK");

        var response = _AlertsService.addAlert(alert);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });

    it('verify add alerts failure',function () {

        var getAlertsHandler = $httpBackend.expectPOST(/.*?api\/v1\/accounts\/account\/2\/alerts?.*/g)
            .respond(500,{});

        var response = _AlertsService.addAlert(alert);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });

    it('verify get alerts success',function () {

        var getAlertsHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/2\/alerts?.*/g)
            .respond(200, MockAlertsResponse);
        var queryParam = {count: 10, search: "", startIndex: 0};

        var response = _AlertsService.getAlerts(queryParam);

        response.success(function (data) {
            expect(data.type).toEqual("AlertConfigurationList");
            expect(data.list.length).toEqual(2);
        }).error(function (data, status) {
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });

    it('verify get alerts failure',function () {

        var getAlertsHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/2\/alerts?.*/g)
            .respond(500, {});
        var queryParam = {count: 2, search: "", startIndex: 0};

        var response = _AlertsService.getAlerts(queryParam);

        response.success(function (data) {
        }).error(function (data, status) {
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });

    it('verify get alert success',function () {

        var getAlertHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/2\/alerts\/1?.*/g)
            .respond(200, alert);
        var alertId = '1';
        var response = _AlertsService.getAlert(alertId);

        response.success(function (data) {
            expect(data.type).toEqual("AlertConfiguration");
        }).error(function (data, status) {
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });

    it('verify update alert success', inject(function ($controller) {

        var getAlertsHandler = $httpBackend.expectPUT(/.*?api\/v1\/accounts\/account\/2\/alerts?.*/g)
            .respond(200,"OK");
        var alertConfig = {};
        alertConfig.type = 'EditAlertsRequest';
        alertConfig.list = [alert];
        var response = _AlertsService.editAlert(alertConfig);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

    it('verify update alerts failure', inject(function ($controller) {

        var getAlertsHandler = $httpBackend.expectPUT(/.*?api\/v1\/accounts\/account\/2\/alerts?.*/g)
            .respond(500,{});
        var alertConfig = {};
        alertConfig.type = 'EditAlertsRequest';
        alertConfig.list = [alert];
        var response = _AlertsService.editAlert(alertConfig);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

    it('verify delete alerts success', inject(function ($controller) {
        var alertIds = '1,2';

        var getAlertsHandler = $httpBackend.expectDELETE(/.*?api\/v1\/accounts\/account\/2\/alerts.*/g)
            .respond(200,"OK");

        var response = _AlertsService.deleteAlerts(alertIds);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

    it('verify delete alerts failure', inject(function ($controller) {
        var alertIds = '3,12';

        var getAlertsHandler = $httpBackend.expectDELETE(/.*?api\/v1\/accounts\/account\/2\/alerts.*/g)
            .respond(500,{});

        var response = _AlertsService.deleteAlerts(alertIds);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

    it('verify get alert history success',function () {
        var alertHistory = {
            "type": "AlertHistoryResponse",
            "list": [{
                "type": "AlertHistory",
                "accountId": 4,
                "accountName": "UberCalifornia",
                "alertId": 11,
                "alertHistoryId": 45,
                "alertType": "DEFAULT_PER_SIM_PER_ACCOUNT",
                "simId": "1001100000000000000257",
                "limitValue": 1234.00,
                "limitType": "DATA",
                "occurredTime": "2016-06-29 09:39:20",
                "action": "EMAIL",
                "actionStatus": "IN_PROGRESS"
            }],
            "count": 1,
            "totalCount": 1
        };
        var getAlertHistoryHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/2\/alertHistory?.*/g)
            .respond(200, alertHistory);
        var alertId = '1';
        var response = _AlertsService.getAlertHistory();

        response.success(function (data) {
            expect(data.type).toEqual("AlertHistoryResponse");
        }).error(function (data, status) {
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });

    it('verify get alert actions success',function () {

        var response = {"type":"AlertActionType","supportedAlertActionList":["EMAIL","DISABLE"]};
        var getAlertsHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/2\/alertActions?.*/g)
            .respond(200, response);

        var response = _AlertsService.getAllowedAlertActionType();

        response.success(function (data) {
            expect(data.type).toEqual("AlertActionType");
            expect(data.supportedAlertActionList.length).toEqual(2);
        }).error(function (data, status) {
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });


    it('verify customise alerts success', function () {
        var request = {
            "type": "AlertCopyRequest",
            "alertTypeList": ["PER_SIM_PER_ZONE"],
            "sourceDetails": {
                "type": "CopyDetails",
                "accountId": "2"
            },
            "destinationDetails": {
                "type": "CopyDetails",
                "accountId": "3"
            }
        }
        var alertsHandler = $httpBackend.expectPOST(/.*?api\/v1\/accounts\/account\/2\/alerts\/copy?.*/g)
            .respond(200,MockAlertsResponse);

        var response = _AlertsService.getAlertsForCustomization(request);

        response.success(function(data){
            expect(data.type).toEqual("AlertConfigurationList");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    });

});