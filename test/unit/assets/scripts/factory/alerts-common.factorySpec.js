describe('Alerts common factory', function () {

    var _AlertsCommonFactory, _AlertsService, _CommonUtilityService;
    var alert = {
        "type": "AlertConfiguration",
        "accountId": 2,
        "accountName": "Uber",
        "alertType": "ACCOUNT_CUMULATIVE",
        "triggerCount": 311,
        "alertSpecification": {
            "type": "AlertSpecification",
            "enable": false,
            "limitValue": 20.00,
            "limitType": "COST",
            "actions": ["EMAIL"]
        },
        "updatedTime": "2016-06-21 05:04:27"
    };
    var rows = [alert];
    var scope = {};
    scope.verificationModalObj = {};
    scope.dataTableObj = {};
    scope.alertModalObj = {};
    beforeEach(module('app'));
    beforeEach(inject(function ($injector, alertsCommonFactory, alertsService, commonUtilityService) {
        _AlertsCommonFactory = alertsCommonFactory;
        _AlertsService = alertsService;
        _CommonUtilityService = commonUtilityService;

        // scope.verificationModalObj.updateVerificationModel = jasmine.createSpy('updateVerificationModel spy method');
        scope.verificationModalObj.addVerificationModelMessage = jasmine.createSpy('addVerificationModelMessage spy method');

        scope.alertModalObj.setShowSpend = jasmine.createSpy('setShowSpend spy method');
        scope.alertModalObj.setShowZones = jasmine.createSpy('setShowZones spy method');

        spyOn(_CommonUtilityService, 'showErrorNotification');
        spyOn(_CommonUtilityService, 'showSuccessNotification');
        _AlertsCommonFactory.setScope(scope);
    }));

    it('Should test setScope to null', function () {

        expect(_AlertsCommonFactory.setScope(123)).toEqual(console.error("AlertsService: Please pass a valid scope for method setScope"));
    });
    it('Should test getScope', function () {

        expect(_AlertsCommonFactory.getScope()).toEqual(scope);
    });

    it('Should test confirmAlertsEnable', function () {
        spyOn(_CommonUtilityService, 'filterSelectedRows').and.callFake(function(rows,fn){
            rows.splice(0, 1);
        });
        scope.verificationModalObj.updateVerificationModel = function (obj, submit) {
            expect(obj.header).toBe('Enable Actions');
        };
        _AlertsCommonFactory.confirmAlertsEnable(rows);

        expect(_CommonUtilityService.filterSelectedRows).toHaveBeenCalled();
        //expect(scope.verificationModalObj.addVerificationModelMessage).toHaveBeenCalledWith('Are you sure you want to enable selected alert?');
    });

    it('Should test confirmAlertsEnable(when more then 1 rows are selected)', function () {
        spyOn(_CommonUtilityService, 'filterSelectedRows');
        scope.verificationModalObj.updateVerificationModel = function (obj, submit) {
            expect(obj.header).toBe('Enable Actions');
        };
        var rowsObj = [alert, alert];
        _AlertsCommonFactory.confirmAlertsEnable(rowsObj);

        expect(_CommonUtilityService.filterSelectedRows).toHaveBeenCalled();
        expect(scope.verificationModalObj.addVerificationModelMessage).toHaveBeenCalledWith('Enabling these actions can affect multiple SIMs, possibly deactivating them or triggering warning thresholds. If you are unsure, press cancel and review user\'s guide for more details.');
    });

    it('Should test confirmAlertsDisable', function () {
        spyOn(_CommonUtilityService, 'filterSelectedRows');
        scope.verificationModalObj.updateVerificationModel = function (obj, submit) {
            expect(obj.header).toBe('Disable Actions');
        };
        alert.alertSpecification.enable = true;
        _AlertsCommonFactory.confirmAlertsDisable(rows);

        expect(_CommonUtilityService.filterSelectedRows).toHaveBeenCalled();
        expect(scope.verificationModalObj.addVerificationModelMessage).toHaveBeenCalledWith('Disabling this action can affect multiple SIMs, ' +
            'possibly reactivating them if previously deactivated by the action(s) or eliminating warning thresholds.' +
            ' If you are unsure, press cancel and review user\'s guide for more details.');
    });

    it('Should test confirmAlertsDisable(when more then 1 rows are selected)', function () {
        spyOn(_CommonUtilityService, 'filterSelectedRows');
        scope.verificationModalObj.updateVerificationModel = function (obj, submit) {
            expect(obj.header).toBe('Disable Actions');
        };
        alert.alertSpecification.enable = true;
        var rowsObj = [alert, alert];
        _AlertsCommonFactory.confirmAlertsDisable(rowsObj);

        expect(_CommonUtilityService.filterSelectedRows).toHaveBeenCalled();
        expect(scope.verificationModalObj.addVerificationModelMessage).toHaveBeenCalledWith('Disabling these actions can affect multiple SIMs, possibly reactivating them if previously deactivated by the action(s) or eliminating warning thresholds. If you are unsure, press cancel and review user\'s guide for more details.');
    });

    it('Should test confirmAlertsDeletion', function () {
        scope.verificationModalObj.updateVerificationModel = function (obj, submit) {
            expect(obj.header).toBe('Delete Actions');
        };
        _AlertsCommonFactory.confirmAlertsDeletion(rows);
        expect(scope.verificationModalObj.addVerificationModelMessage).toHaveBeenCalledWith('Deleting this action can affect multiple SIMs, possibly reactivating them if previously deactivated by the action(s) or eliminating warning thresholds. If you are unsure, press cancel and review user\'s guide for more details.');
    });

    it('Should test confirmAlertsDeletion (when more then 1 rows are selected)', function () {
        scope.verificationModalObj.updateVerificationModel = function (obj, submit) {
            expect(obj.header).toBe('Delete Actions');
        };
        var rowsObj = [alert, alert];
        _AlertsCommonFactory.confirmAlertsDeletion(rowsObj);
        expect(scope.verificationModalObj.addVerificationModelMessage).toHaveBeenCalledWith('Deleting these actions can affect multiple SIMs, possibly reactivating them if previously deactivated by the action(s) or eliminating warning thresholds. If you are unsure, press cancel and review user\'s guide for more details.');
    });

    it('Should test confirmAlertCustomize', function () {
        scope.verificationModalObj.updateVerificationModel = function (obj, submit) {
            expect(obj.header).toBe('Customize Actions');
        };
        _AlertsCommonFactory.confirmAlertCustomize();
        expect(scope.verificationModalObj.addVerificationModelMessage).toHaveBeenCalledWith('This allows you to change actions for the chosen SIM only.');
    });

    it('Should test confirmAlertRevert', function () {
        scope.verificationModalObj.updateVerificationModel = function (obj, submit) {
            expect(obj.header).toBe('Revert Actions');
        };
        _AlertsCommonFactory.confirmAlertRevert(rows);
        expect(scope.verificationModalObj.addVerificationModelMessage).toHaveBeenCalledWith('This reverts chosen SIMs to default action configuration. (If defaults not set, all action settings will be removed.)');
    });

    it('Should test showAlertModal', function () {
        scope.alertModalObj.updateAlertModal = function (data, config, submit) {
            expect(config.header).toBe('Add Action');
        }
        _AlertsCommonFactory.showAlertModal({
            alertType: 'ZONE_CUMULATIVE',
            isNew: true,
            isSpendAlert: true,
            isZoneAlert: true
        });

        expect(scope.alertModalObj.setShowSpend).toHaveBeenCalledWith(true);
        expect(scope.alertModalObj.setShowZones).toHaveBeenCalledWith(true);
    });

    it('Should test showAlertModal (while updating data)', function () {
        scope.alertModalObj.updateAlertModal = function (data, config, submit) {
            expect(config.header).toBe('Edit Action');
        }
        _AlertsCommonFactory.showAlertModal({
            alertType: 'ZONE_CUMULATIVE',
            isNew: false,
            isSpendAlert: true,
            isZoneAlert: true
        }, alert);
        expect(scope.alertModalObj.setShowSpend).toHaveBeenCalledWith(true);
        expect(scope.alertModalObj.setShowZones).toHaveBeenCalledWith(true);
    });

    it('getAlertRequestObj', function () {
        var alert = {allocation: "1245", action: "DISABLE"};

        var alertObj = _AlertsCommonFactory.getAlertRequestObj(alert, 'ACCOUNT_CUMULATIVE', 'COST');
        expect(alertObj.type).toBe('AlertConfiguration');
        expect(alertObj.alertSpecification.type).toBe('AlertSpecification');
    });

    it('getAlertRequestObj for ZONE_CUMULATIVE', function () {
        var alert = {allocation: "1245", action: "DISABLE"};
        alert.zone = {type: "ZoneInfo", zoneId: 1, name: "2-Zone1", nickName: "2-Zone1", pricePerMB: 0.01};

        var alertObj = _AlertsCommonFactory.getAlertRequestObj(alert, 'ZONE_CUMULATIVE', 'COST');
        expect(alertObj.type).toBe('AlertConfiguration');
        expect(alertObj.alertSpecification.type).toBe('AlertSpecification');
    });

    it('getAlertRequestObj for OVERRIDE_PER_SIM', function () {
        var alert = {allocation: "1245", action: "DISABLE"};
        alert.simId = 1001100000000000000256;

        var alertObj = _AlertsCommonFactory.getAlertRequestObj(alert, 'OVERRIDE_PER_SIM', 'DATA');
        expect(alertObj.type).toBe('AlertConfiguration');
        expect(alertObj.alertSpecification.type).toBe('AlertSpecification');
    });

    it('should test setAlertCustomized', function () {
        var alerts = [alert];
        _AlertsCommonFactory.setAlertCustomized(alerts);

        expect(scope.alertCustomized).toBe(false);

        sessionStorage.setItem('accountId', '2');
        _AlertsCommonFactory.setAlertCustomized(alerts);

        expect(scope.alertCustomized).toBe(false);

        _AlertsCommonFactory.setAlertCustomized([]);

        expect(scope.alertCustomized).toBe(false);
    });

    it('should test showError (when errorINt is 4022)', function () {
        sessionStorage.setItem("gs-enterpriseCurrency", 'USD');
        var data = {
            "type": "error",
            "errorInt": 4022,
            "errorStr": "Conflicting alerts found",
            "failedAlertIndex": 1,
            "conflictingAlertList": [{
                "type": "AlertConfiguration",
                "alertId": 15,
                "accountId": 2,
                "accountName": "Uber",
                "alertType": "ACCOUNT_CUMULATIVE",
                "alertHistoryActionStatus": "COMPLETED",
                "triggerCount": 0,
                "alertSpecification": {
                    "type": "AlertSpecification",
                    "enable": false,
                    "limitValue": 1234.00,
                    "limitType": "COST",
                    "actions": ["EMAIL"]
                }
            }]
        };
        _AlertsCommonFactory.showError(data, 'ACCOUNT_CUMULATIVE');
        expect(_CommonUtilityService.showErrorNotification).toHaveBeenCalledWith('This is in conflict with action(s): <br/> - Warning : Limit is $1234');

        var data2 = {
            "type": "error",
            "errorInt": 4022,
            "errorStr": "Conflicting alerts found",
            "failedAlertIndex": 1,
            "conflictingAlertList": [{
                "type": "AlertConfiguration",
                "alertId": 18,
                "accountId": 2,
                "accountName": "Uber",
                "alertType": "ZONE_CUMULATIVE",
                "alertHistoryActionStatus": "COMPLETED",
                "triggerCount": 0,
                "alertSpecification": {
                    "type": "AlertSpecification",
                    "enable": true,
                    "limitValue": 234.00,
                    "limitType": "COST",
                    "actions": ["EMAIL"]
                },
                "updatedTime": "2016-07-02 14:28:52"
            }, {
                "type": "AlertConfiguration",
                "alertId": 15,
                "accountId": 2,
                "accountName": "Uber",
                "alertType": "ZONE_CUMULATIVE",
                "alertHistoryActionStatus": "COMPLETED",
                "triggerCount": 0,
                "alertSpecification": {
                    "type": "AlertSpecification",
                    "enable": false,
                    "limitValue": 1234.00,
                    "limitType": "COST",
                    "actions": ["EMAIL"]
                }
            }]
        };

        _AlertsCommonFactory.showError(data2, 'ZONE_CUMULATIVE');
        expect(_CommonUtilityService.showErrorNotification).toHaveBeenCalledWith("This is in conflict with action(s): <br/> - Warning : Limit is $234 <br/> - Warning : Limit is $1234");

        var data7 = {
            "type": "error",
            "errorInt": 4022,
            "errorStr": "Conflicting alerts found",
            "failedAlertIndex": 1,
            "conflictingAlertList": [{
                "type": "AlertConfiguration",
                "alertId": 18,
                "accountId": 2,
                "accountName": "Uber",
                "alertType": "ZONE_CUMULATIVE",
                "alertHistoryActionStatus": "COMPLETED",
                "triggerCount": 0,
                "alertSpecification": {
                    "type": "AlertSpecification",
                    "enable": true,
                    "limitValue": 234.00,
                    "limitType": "COST",
                    "actions": ["EMAIL"]
                },
                "updatedTime": "2016-07-02 14:28:52"
            }, {
                "type": "AlertConfiguration",
                "alertId": 15,
                "accountId": 2,
                "accountName": "Uber",
                "alertType": "ZONE_CUMULATIVE",
                "alertHistoryActionStatus": "COMPLETED",
                "triggerCount": 0,
                "alertSpecification": {
                    "type": "AlertSpecification",
                    "enable": false,
                    "limitValue": 1234.00,
                    "limitType": "COST",
                    "actions": ["EMAIL"]
                }
            }]
        };

        _AlertsCommonFactory.showError(data7, 'SIM_DEFAULT');
        expect(_CommonUtilityService.showErrorNotification).toHaveBeenCalledWith('This is in conflict with action(s): <br/> - Warning : Limit is $234 <br/> - Warning : Limit is $1234');

        var data4 = {
            "type": "error",
            "errorInt": 4022,
            "errorStr": "Conflicting alerts found",
            "failedAlertIndex": 1,
            "conflictingAlertList": [{
                "type": "AlertConfiguration",
                "alertId": 18,
                "accountId": 2,
                "accountName": "Uber",
                "alertType": "ZONE_CUMULATIVE",
                "alertHistoryActionStatus": "COMPLETED",
                "triggerCount": 0,
                "alertSpecification": {
                    "type": "AlertSpecification",
                    "enable": true,
                    "limitValue": 234.00,
                    "limitType": "COST",
                    "actions": ["EMAIL"]
                },
                "updatedTime": "2016-07-02 14:28:52"
            }]
        };

        _AlertsCommonFactory.showError(data4, 'ZONE_CUMULATIVE');
        expect(_CommonUtilityService.showErrorNotification).toHaveBeenCalledWith("This is in conflict with action(s): <br/> - Warning : Limit is $234");

        var data5 = {
            "type": "error",
            "errorInt": 4022,
            "errorStr": "Conflicting alerts found",
            "failedAlertIndex": 1,
            "conflictingAlertList": [{
                "type": "AlertConfiguration",
                "alertId": 18,
                "accountId": 2,
                "accountName": "Uber",
                "alertType": "ZONE_CUMULATIVE",
                "alertHistoryActionStatus": "COMPLETED",
                "triggerCount": 0,
                "alertSpecification": {
                    "type": "AlertSpecification",
                    "enable": true,
                    "limitValue": 234.00,
                    "limitType": "COST",
                    "actions": ["EMAIL"]
                },
                "updatedTime": "2016-07-02 14:28:52"
            }]
        };

        _AlertsCommonFactory.showError(data5, 'SIM_DEFAULT');
        expect(_CommonUtilityService.showErrorNotification).toHaveBeenCalledWith("This is in conflict with action(s): <br/> - Warning : Limit is $234");

        var data3 = {"type": "error", "errorInt": 4022, "errorStr": "Conflicting alerts found", "failedAlertIndex": 1};
        expect(_AlertsCommonFactory.showError(data3, 'ZONE_CUMULATIVE')).toEqual(undefined);

        var data6 = {
            "type": "error",
            "errorInt": 4022,
            "errorStr": "Conflicting alerts found",
            "failedAlertIndex": 1,
            "conflictingAlertList": [{
                "type": "AlertConfiguration",
                "alertId": 18,
                "accountId": 2,
                "accountName": "Uber",
                "alertType": "ZONE_CUMULATIVE",
                "alertHistoryActionStatus": "COMPLETED",
                "triggerCount": 0,
                "alertSpecification": {
                    "type": "AlertSpecification",
                    "enable": true,
                    "limitValue": 234.00,
                    "limitType": "COST",
                    "actions": ["EMAIL"]
                },
                "updatedTime": "2016-07-02 14:28:52"
            }, {
                "type": "AlertConfiguration",
                "alertId": 15,
                "accountId": 2,
                "accountName": "Uber",
                "alertType": "ZONE_CUMULATIVE",
                "alertHistoryActionStatus": "COMPLETED",
                "triggerCount": 0,
                "alertSpecification": {
                    "type": "AlertSpecification",
                    "enable": false,
                    "limitValue": 1234.00,
                    "limitType": "COST",
                    "actions": ["EMAIL"]
                }
            }, {
                "type": "AlertConfiguration",
                "alertId": 18,
                "accountId": 2,
                "accountName": "Uber",
                "alertType": "ZONE_CUMULATIVE",
                "alertHistoryActionStatus": "COMPLETED",
                "triggerCount": 0,
                "alertSpecification": {
                    "type": "AlertSpecification",
                    "enable": true,
                    "limitValue": 234.00,
                    "limitType": "COST",
                    "actions": ["EMAIL"]
                },
                "updatedTime": "2016-07-02 14:28:52"
            }]
        };

        _AlertsCommonFactory.showError(data6, 'ZONE_CUMULATIVE');
        expect(_CommonUtilityService.showErrorNotification).toHaveBeenCalledWith("This is in conflict with action(s): <br/> - Warning : Limit is $234 <br/> - Warning : Limit is $1234");

    });

    it('should test showError (when errorINt is 4024)', function () {

        var data = {
            "type": "error",
            "errorInt": 4024,
            "errorStr": "Maximum supported alert action reached",
            "failedAlertId": 18
        };

        _AlertsCommonFactory.showError(data, 'OVERRIDE_PER_SIM');
        expect(_CommonUtilityService.showErrorNotification).toHaveBeenCalledWith('You already have 3 actions created for this sim.You must delete an existing action in order to create a new one.');

        _AlertsCommonFactory.showError(data, 'ZONE_CUMULATIVE');
        expect(_CommonUtilityService.showErrorNotification).toHaveBeenCalledWith('You already have 3 actions created for this zone. You must delete an existing action in order to create a new one.');

        _AlertsCommonFactory.showError(data, 'PER_SIM_PER_ZONE');
        expect(_CommonUtilityService.showErrorNotification).toHaveBeenCalledWith('You can have only one action for a zone');

        expect(_AlertsCommonFactory.showError(data, '')).toEqual(undefined);
    });
    it('should test showError (when errorINt is not 4024 or 4022)', function () {

        var data = {
            "type": "error",
            "errorInt": 4014,
            "errorStr": "Custom error message from server",
            "failedAlertId": 18
        };

        _AlertsCommonFactory.showError(data, 'OVERRIDE_PER_SIM');
        expect(_CommonUtilityService.showErrorNotification).toHaveBeenCalledWith('Custom error message from server');
    });

});
