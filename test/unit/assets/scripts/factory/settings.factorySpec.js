describe('Settings service api', function () {

    var _SettingsService, $httpBackend;
    var details = {"_id":"111","enterprise":"124434432","zones":[{"name":"Zone 1","price":"0.17","currency":"$","timestamp":"07/07/2015","countries":[{"name":"India"},{"name":"Sri Lanka"}]}],"planDetailsId":"56276c2eca493dd9ab00738e"};
    var enterprise = {"_id":"123","postalCode":"560075","currency":"U.S.Dollar","companyName":"GigskyIndiaPvtLtd","address":"Aditya Plaza","city":"Bengaluru","state":"Karnataka","country":{"name":"Afghanistan","alpha-2":"AF"},"enterpriseId":"124434432"};
    beforeEach(module('app'));

    beforeEach(inject(function ($injector, SettingsService) {
        _SettingsService = SettingsService;
        $httpBackend = $injector.get('$httpBackend');

    }));

    it('verify get pricing details success', inject(function () {

        var enterpriseId = '124434432';

        var getSettingsHandler = $httpBackend.expectGET(/.*?api\/v1\/account\/1234\/zones?.*/g )
            .respond(200,details);
        sessionStorage.setItem("accountId",1234);
        var response = _SettingsService.getPlanDetails(enterpriseId);

        response.success(function(data){
            expect(data).toEqual(details);
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

    it('verify get pricing details failure', inject(function () {

        var enterpriseId = '124434432';

        var getSettingsHandler = $httpBackend.expectGET(/.*?api\/v1\/account\/1234\/zones?.*/g )
            .respond(500,{});
        sessionStorage.setItem("accountId","1234");
        var response = _SettingsService.getPlanDetails(enterpriseId);

        response.success(function(data){
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

    it('verify update enterprise details success', inject(function () {

        var getAccountsHandler = $httpBackend.expectPUT(/.*?api\/v1\/accounts\/account\/1234?.*/g )
            .respond(200,"OK");
        sessionStorage.setItem("accountId","1234");
        var enterrise = {"previousCompanyName":"Uber","companyName":"Uber1","city":"San Francisco","state":"California","postalCode":"94105","currency":"USD","enterpriseId":1234,"accountSubscriptionType":"GEM PRO","timezone":"American Samoa, Jarvis Island, Kingman Reef, Midway Atoll and Palmyra Atoll","address1":"c/o Uber Technologies, Inc","address2":"182 Howard Street # 8","country":{"type":"country","name":"United States","code":"US","$$hashKey":"object:847"}};
        var response = _SettingsService.updateEnterpriseDetails(false,enterrise,true);
        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

    it('verify update enterprise details failure', inject(function () {

        var getAccountsHandler = $httpBackend.expectPUT(/.*?api\/v1\/accounts\/account\/1234?.*/g )
            .respond(200,"OK");
        sessionStorage.setItem("accountId","1234");
        var enterrise = {"previousCompanyName":"Uber","companyName":"Uber1","city":"San Francisco","state":"California","postalCode":"94105","currency":"USD","enterpriseId":1234,"accountSubscriptionType":"GEM PRO","timezone":"American Samoa, Jarvis Island, Kingman Reef, Midway Atoll and Palmyra Atoll","address1":"c/o Uber Technologies, Inc","address2":"182 Howard Street # 8","country":{"type":"country","name":"United States","code":"US","$$hashKey":"object:847"}};
        var response = _SettingsService.updateEnterpriseDetails(false,enterrise,false);
        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));


    it('verify get enterprice details success', inject(function () {

        var enterpriseId = '124434432';

        var getSettingsHandler = $httpBackend.expectGET(/.*?api\/v1\/enterprise\/124434432?.*/g )
            .respond(200,enterprise);

        var response = _SettingsService.getEnterpriseDetails(enterpriseId);

        response.success(function(data){
            expect(data).toEqual(enterprise);
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
    }));

    it('verify get enterprice details failure', inject(function () {

        var enterpriseId = '124434432';

        var getSettingsHandler = $httpBackend.expectGET(/.*?api\/v1\/enterprise\/124434432?.*/g )
            .respond(500,{});

        var response = _SettingsService.getEnterpriseDetails(enterpriseId);

        response.success(function(data){
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
    }));

    describe('should test preparePricingRange function', function () {
        /**
         * @func preparePricingRange
         * @desc formatting to display the price range
         * @param startRange
         * @validInput - 0, number
         * @param endRange
         * @validInput - number, null
         * @param unitType - unit in which the bucket is defined
         * @validInput - MB, GB, TB
         */

        it('should test when startRange is 0 & endRange is a number', function () {
            var range = _SettingsService.preparePricingRange(0,204,'MB');
            expect(range).toBe('0 - 204 MB');
        });

        it('should test when startRange is number & endRange is a number', function () {
            var range = _SettingsService.preparePricingRange(500,700,'MB');
            expect(range).toBe('500 MB - 700 MB');
        });

        it('should test when startRange is 0 & endRange is a null', function () {
            var range = _SettingsService.preparePricingRange(0,null,'MB');
            expect(range).toBe('0 - <span class="infinity v-align-middle">&infin;</span>');
        });

        it('should test when startRange is number & endRange is a null', function () {
            var range = _SettingsService.preparePricingRange(500,null,'MB');
            expect(range).toBe('More than 500 MB');
        });

        it('should test when startRange and endRange is in the same unit', function () {
            var range1 = _SettingsService.preparePricingRange(500,750,'MB');
            expect(range1).toBe('500 MB - 750 MB');

            var range2 = _SettingsService.preparePricingRange(1024,2048,'MB');
            expect(range2).toBe('1 GB - 2 GB');
        });

        it('should test when startRange and endRange is in the different unit', function () {
            var range1 = _SettingsService.preparePricingRange(500,2048,'MB');
            expect(range1).toBe('500 MB - 2 GB');

            var range2 = _SettingsService.preparePricingRange(1024,2948000,'MB');
            expect(range2).toBe('1 GB - 2.81 TB');

            var range3 = _SettingsService.preparePricingRange(102,2948000,'MB');
            expect(range3).toBe('102 MB - 2.81 TB');

            var range4 = _SettingsService.preparePricingRange(222,1024,'GB');
            expect(range4).toBe('222 GB - 1 TB');
        });

    });

    describe('should test formatDataSize function', function () {
        /**
         * @func formatDataSize
         * @desc should return the usage with the nearest unit
         * @param dataUsedInBytes
         * @validInputs - 0, usage in bytes, KB, MB, GB, TB
         */

        it('should test when data is in bytes',function(){
            var usage = _SettingsService.formatDataSize(900);
            expect(usage).toBe('900 Bytes');
        });

        it('should test when data is in KB',function(){
            var usage = _SettingsService.formatDataSize(2234);
            expect(usage).toBe('2.18 KB');
        });

        it('should test when data is in MB',function(){
            var usage = _SettingsService.formatDataSize(6994620);
            expect(usage).toBe('6.67 MB');
        });

        it('should test when data is in GB',function(){
            var usage = _SettingsService.formatDataSize(7799993625);
            expect(usage).toBe('7.26 GB');
        });

        it('should test when data is in TB',function(){
            var usage = _SettingsService.formatDataSize(8987799993625);
            expect(usage).toBe('8.17 TB');
        });

        it('should text when the data is 0', function () {
            var usage = _SettingsService.formatDataSize(0);
            expect(usage).toBe('0 Bytes');
        });

    });

});