describe('Account service api', function () {

    var _AccountService,$httpBackend;
    var user = {
        userId:'124434432',
        firstName: 'firstName',
        lastName: 'lastName',
        emailId: 'emailId@emailId.emailId',
        addressId: 1,
        country:{
            code:1
        }
    };
    beforeEach(module('app'));
    beforeEach(inject(function ($injector,AccountService) {
        _AccountService = AccountService;
        $httpBackend = $injector.get('$httpBackend');

    }));

    it('verify get account details success with no root account Id', inject(function () {

        var enterpriseId = 2;
        sessionStorage.setItem("accountId",enterpriseId);
        var userID = '124434432';
        sessionStorage.setItem('userId',userID);

        var getAccountsHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/2\/user\/124434432.*/g).respond(200,user);

        var response = _AccountService.getAccountDetails();

        response.success(function(data){
            expect(data).toEqual(user);
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

    it('verify get account details success', inject(function () {

        var enterpriseId = 2;
        sessionStorage.setItem("accountId",enterpriseId);
        var rootAccountId = 1;
        sessionStorage.setItem("rootAccountId",rootAccountId);
        var userID = '124434432';
        sessionStorage.setItem('userId',userID);

        var getAccountsHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/1\/user\/124434432.*/g).respond(200,user);

        var response = _AccountService.getAccountDetails();

        response.success(function(data){
            expect(data).toEqual(user);
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));


    it('verify get account details failure', inject(function () {

        var enterpriseId = 2;
        sessionStorage.setItem("accountId",enterpriseId);
        var rootAccountId = 1;
        sessionStorage.setItem("rootAccountId",rootAccountId);
        var userID = '124434432';
        sessionStorage.setItem('userId',userID);

        var getAccountsHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/1\/user\/124434432.*/g).respond(500,{});

        var response =  _AccountService.getAccountDetails();

        response.success(function(data){
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

    it('verify update account details success', inject(function () {

        var getAccountsHandler = $httpBackend.expectPUT(/.*?api\/v1\/users\/user\/124434432?.*/g )
            .respond(200,"OK");

        var response = _AccountService.updateAccountDetails(user);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

    it('verify update account details failure', inject(function () {

        var getAccountsHandler = $httpBackend.expectPUT(/.*?api\/v1\/users\/user\/124434432?.*/g ).respond(500,{});

        var response = _AccountService.updateAccountDetails(user);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

    it('verify update password success', inject(function () {
        var userID = '124434432';
        sessionStorage.setItem('userId',userID);

        var oldPassword = '';
        var newPassword = '';

        var getAccountsHandler = $httpBackend.expectPUT(/.*?api\/v1\/users\/user\/124434432\/password?.*/g )
            .respond(200,"OK");

        var response = _AccountService.updatePassword(oldPassword,newPassword);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));

    it('verify update password failure', inject(function () {
        var userID = '124434432';
        sessionStorage.setItem('userId',userID);

        var oldPassword = '';
        var newPassword = '';

        var getAccountsHandler = $httpBackend.expectPUT(/.*?api\/v1\/users\/user\/124434432\/password?.*/g ).respond(500,{});

        var response = _AccountService.updatePassword(oldPassword,newPassword);

        response.success(function(data){
            expect(data).toEqual("OK");
        }).error(function(data,status){
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));
    it('verify get billing cycle is returning proper timezone specific time first time', inject(function () {

        var enterpriseId = 2;
        sessionStorage.setItem("accountId",enterpriseId);

        var account = {
            company:{
                timeZone : '+01:30'
            }
        };
        var getAccountsHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/2.*/g).respond(200,account);

        var response = _AccountService.getBillingCycle();

        $httpBackend.flush();

    }));

    it('verify get billing cycle is returning proper timezone specific time if timezone is saved in sessionStorage', inject(function () {

        sessionStorage.setItem("timezone",'-01:30');

        var response = _AccountService.getBillingCycle();

    }));

    it('verify get billing cycle is returning proper timezone specific time if timezone is saved in sessionStorage negative value', inject(function () {

        sessionStorage.setItem("timezone",'+01:30');

        var response = _AccountService.getBillingCycle();

    }));
    it('verify get billing cycle is returning proper timezone specific time if timezone is saved in sessionStorage negative value', inject(function () {

        sessionStorage.setItem("timezone",undefined);

        var response = _AccountService.getBillingCycle();

    }));

    it('verify get billing cycle error first time', inject(function () {

        sessionStorage.clear();
        var enterpriseId = 2;
        sessionStorage.setItem("accountId",enterpriseId);

        var account = {
            company:{
                timeZone : '+01:30'
            }
        };
        var getAccountsHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/2.*/g).respond(500,{});

        var response = _AccountService.getBillingCycle();

        $httpBackend.flush();

    }));


});