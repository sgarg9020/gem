describe('GroupsFactory', function () {

    var _commonUtilityService, $httpBackend,_GroupsService;


    beforeEach(module('app'));

    beforeEach(inject(function ($injector, commonUtilityService,GroupsService) {
        _commonUtilityService = commonUtilityService;
        _GroupsService= GroupsService;
        $httpBackend = $injector.get('$httpBackend');

    }));

    it('verify get Groups success', inject(function () {

        var groups = {"type":"AccountList","list":[{"type":"Account","accountId":29378,"accountType":"GROUP","accountName":"my group1","parentAccountId":2},{"type":"Account","accountId":4,"accountType":"GROUP","accountName":"UberCalifornia","parentAccountId":2,"accountSubscriptionType":"ENTERPRISE_PRO","company":{"type":"Company","name":"Uber","phone":"8888888888","currency":"USD","timeZone":"+00:00","timeZoneInfo":"American Samoa, Jarvis Island, Kingman Reef, Midway Atoll and Palmyra Atoll","address":{"type":"Address","address1":"c/o Uber Technologies, Inc","address2":"182 Howard Street # 8","state":"California","city":"San Francisco","zipCode":"94105","country":"US"}},"accountDescription":"Uber California Sub Account"},{"type":"Account","accountId":3,"accountType":"GROUP","accountName":"UberIndia","parentAccountId":2,"accountSubscriptionType":"ENTERPRISE_PRO","company":{"type":"Company","name":"Uber","phone":"8888888888","currency":"USD","timeZone":"+00:00","timeZoneInfo":"American Samoa, Jarvis Island, Kingman Reef, Midway Atoll and Palmyra Atoll","address":{"type":"Address","address1":"c/o Uber Technologies, Inc","address2":"182 Howard Street # 8","state":"California","city":"San Francisco","zipCode":"94105","country":"US"}},"accountDescription":"Uber India Sub Account"}],"accountType":"ENTERPRISE","accountName":"Uber","accountDescription":"Uber Enterprise Account","parentAccountId":1,"accountSubscriptionType":"ENTERPRISE_PRO","alertVersionSupported":"1.1","accountId":2,"count":3,"totalCount":3};
        var queryParam = {
            count: 100000,
            startIndex: 0,
            sortBy: "accountName",
            sortDirection: "ASC"
        };
        sessionStorage.setItem("accountId", '1234');
        var getSettingsHandler = $httpBackend.expectGET(/.*?api\/v1\/accounts\/account\/1234\/accounts?.*/g)
            .respond(200, groups);
        var response = _GroupsService.getGroups(queryParam);
        response.success(function (data) {
            expect(data).toEqual(groups);
        }).error(function (data, status) {
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));
    it('verify add Group success', inject(function () {

        var group = {"type":"AccountList","list":[{"type":"Account","accountType":"GROUP","accountName":"my group","parentAccountId":"2"}]};
        sessionStorage.setItem("accountId", '1234');
        var getSettingsHandler = $httpBackend.expectPOST(/.*?api\/v1\/accounts\/account?.*/g)
            .respond(200);
        var response = _GroupsService.addGroup(group);
        response.success(function (data,status) {
            expect(status).toEqual(200);
        }).error(function (data, status) {
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));
    it('verify update Group success', inject(function () {

        var group = {"accountId":29378,"accountName":"my group1","type":"Account"};
        sessionStorage.setItem("accountId", '1234');
        var getSettingsHandler = $httpBackend.expectPUT(/.*?api\/v1\/accounts\/account?.*/g)
            .respond(200);
        var response = _GroupsService.updateGroup(group);
        response.success(function (data,status) {
            expect(status).toEqual(200);
        }).error(function (data, status) {
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));
    it('verify delete Group success', inject(function () {

        var groupList = '29378';
        sessionStorage.setItem("accountId", '1234');
        var getSettingsHandler = $httpBackend.expectDELETE(/.*?api\/v1\/accounts\/account?.*/g)
            .respond(200);
        var response = _GroupsService.deleteGroups(groupList);
        response.success(function (data,status) {
            expect(status).toEqual(200);
        }).error(function (data, status) {
            expect(status).toEqual(500);
        });
        $httpBackend.flush();
    }));


});
