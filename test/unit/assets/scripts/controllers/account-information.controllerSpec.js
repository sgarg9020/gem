describe('Account information Controller', function() {

    var _commonUtilityService,AccountInfoCtrl,scope,_AccountService,_UserService,rootScope;


    beforeEach(module('app'));


    beforeEach(inject(function($injector,$rootScope,$sce,$controller,$location,AccountService,commonUtilityService,UserService,$q){
        scope ={};
        spyOn(AccountService,"getAccountDetails").and.callFake(function(){

            var expectedResponse = {"type":"User","userId":1,"firstName":"gsAdmin","lastName":"gsAdmin","emailId":"gsAdmin@gigsky.com","location":"Palo-Alto","address":{"type":"Address","addressId":1,"address1":"Beverly Hills","address2":"Inside Den","state":"","city":"SanHouse","zipCode":"560076","country":"Ca"},"userStatus":"ACTIVE","createdOn":"2015-12-02 13:58:16","homePhone":"080-58974536","mobile":"9886333515","accountDetails":{"type":"EnterpriseAccountList","enterpriseAccountDetails":[{"type":"Account","rootAccountDetails":{"type":"Account","accountId":1,"accountType":"ENTERPRISE","accountName":"Gigsky"},"parentAccountDetails":[{"type":"Account","accountId":1,"accountType":"GIGSKY_ROOT","accountName":"Gigsky","role":"GIGSKY_ADMIN"}],"selfAccountDetails":{"type":"Account","accountId":111111,"accountType":"USER"}}]},"totalSimsAssociated":"1","dataUsedInBytes":0};
            var d = $q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });

        spyOn(commonUtilityService,"getCountries").and.callFake(function(){
            var expectedResponse1 =[{"_id":"561771933b27e844933060b9","alpha-2":"AF","name":"Afghanistan","countryId":"561771933b27e844933060b9"},{"_id":"561771933b27e844933060ba","name":"Åland Islands","alpha-2":"AX","countryId":"561771933b27e844933060ba"},{"_id":"561771933b27e844933060bb","name":"Albania","alpha-2":"AL","countryId":"561771933b27e844933060bb"},{"_id":"561771933b27e844933060bc","name":"Algeria","alpha-2":"DZ","countryId":"561771933b27e844933060bc"},{"_id":"561771933b27e844933060bd","alpha-2":"AS","name":"American Samoa","countryId":"561771933b27e844933060bd"},{"_id":"561771933b27e844933060be","name":"Andorra","alpha-2":"AD","countryId":"561771933b27e844933060be"},{"_id":"561771933b27e844933060bf","name":"Angola","alpha-2":"AO","countryId":"561771933b27e844933060bf"},{"_id":"561771933b27e844933060c0","alpha-2":"AI","name":"Anguilla","countryId":"561771933b27e844933060c0"},{"_id":"561771933b27e844933060c1","name":"Antarctica","alpha-2":"AQ","countryId":"561771933b27e844933060c1"},{"_id":"561771933b27e844933060c2","name":"Antigua and Barbuda","alpha-2":"AG","countryId":"561771933b27e844933060c2"},{"_id":"561771933b27e844933060c3","name":"Argentina","alpha-2":"AR","countryId":"561771933b27e844933060c3"},{"_id":"561771933b27e844933060c4","alpha-2":"AM","name":"Armenia","countryId":"561771933b27e844933060c4"},{"_id":"561771933b27e844933060c5","name":"Aruba","alpha-2":"AW","countryId":"561771933b27e844933060c5"}];

            var d1 = $q.defer();
            d1.resolve({data:expectedResponse1});
            return d1.promise;
        });

        AccountInfoCtrl = $controller("AccountInfoCtrl",{$scope:scope,$rootScope:$rootScope,$sce:$sce,$location:$location,AccountService:AccountService,commonUtilityService:commonUtilityService,UserService:UserService});
        rootScope = $rootScope;
        rootScope.$apply();
        expect(AccountInfoCtrl.profile.user.firstName).toBe('gsAdmin');
        _commonUtilityService = commonUtilityService;
        _AccountService = AccountService;
        _UserService = UserService;
        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
    }));


    it('profile update accountDetails', function () {
        var expectedResponse = {};
        var _mockPromise = {
            success: function (fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function (fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        //AccountInfoCtrl.profile.user = expectedResponse;
        spyOn(_UserService, "updateUsers").and.returnValue(_mockPromise);
        scope.isFormValid = function () {
            return true;
        };
        var editAccountForm = {};
        AccountInfoCtrl.profile.updateAccountDetails(editAccountForm);
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith("Account gsAdmin gsAdmin details have been updated successfully.");
    });

    it('profile update accountDetails (when form validation fails)', function () {
        var expectedResponse = {
            "firstName": "Jagadish",
            "lastName": "Dande",
            "emailId": "jdande@gigsky.com",
            "country": null,
            "userId": "561771933b27e844933060c6"
        };
        var _mockPromise = {
            success: function (fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function (fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_UserService, "updateUsers").and.returnValue(_mockPromise);
        scope.isFormValid = function () {
            return false;
        };
        var editAccountForm = {};
        AccountInfoCtrl.profile.updateAccountDetails(editAccountForm);
        scope.generalObj.isNoChangeError = true;

        AccountInfoCtrl.profile.updateAccountDetails(editAccountForm);
        expect(AccountInfoCtrl.profile.errorMessage).toBe('There is no change in the data to update.');
    });
});