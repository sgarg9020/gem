describe('Profile Controller', function() {

    var _commonUtilityService,_SIMService,ProfileCtrl,scope,_AccountService,_UserService,_AuthenticationService;


    beforeEach(module('app'));


    beforeEach(inject(function($injector,$rootScope,$sce,$controller,$location,AccountService,SIMService,commonUtilityService,UserService,AuthenticationService){
        scope ={};
        ProfileCtrl = $controller("ProfileCtrl",{$scope:scope,$rootScope:$rootScope,$sce:$sce,$location:$location,AccountService:AccountService,SIMService:SIMService,commonUtilityService:commonUtilityService,UserService:UserService,AuthenticationService:AuthenticationService});

        _commonUtilityService = commonUtilityService;
        _SIMService = SIMService;
        _AccountService = AccountService;
        _UserService = UserService;
        _AuthenticationService = AuthenticationService;
        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
    }));

    it('Should test controller',function(){
        expect(true).toBe(true);
    });

});