describe('DataUsageHistoricalCtrl Controller', function() {

    var _commonUtilityService, DataUsageHistoricalCtrl, scope, _AccountService,_requestGraphData,_dataUsageService;
    var vm;


    beforeEach(module('app'));

    beforeEach(inject(function($injector,$rootScope,$controller,requestGraphData,dataUsageService,commonUtilityService,AccountService){
        scope =$rootScope.$new();

        DataUsageHistoricalCtrl = $controller("DataUsageHistoricalCtrl",{$scope:scope,requestGraphData:requestGraphData,dataUsageService:dataUsageService,commonUtilityService:commonUtilityService,AccountService:AccountService});

        _commonUtilityService = commonUtilityService;
        _AccountService = AccountService;
        _requestGraphData = requestGraphData;
        _dataUsageService = dataUsageService;
        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
        scope.graphObj.api = {};
        scope.graphObj.api.getScope = function(){
            return {chart: true};
        }

    }));


    it('should verify requestData', function () {
        var data ={"billingStart":"2015-11-01 00:00:00.00","billingEnd":"2015-11-03 23:59:59.00"};
        var error = {errorStr:"errorStr"}

        var _mockPromise = {
            success: function(fn) {
                fn(data, data.billingStart, data.billingEnd);
                return _mockPromise;
            },
            error: function(fn) {
                fn(error);
                return _mockPromise;
            }
        };
        spyOn(_AccountService,'getBillingCycle').and.returnValue(_mockPromise);

        DataUsageHistoricalCtrl.requestData();

        expect(_AccountService.getBillingCycle).toHaveBeenCalled();

        expect(DataUsageHistoricalCtrl.startDay).toEqual(data.billingStart);
        expect(DataUsageHistoricalCtrl.endDay).toEqual(data.billingEnd);

    });

    it('should verify getDataHistorical (when category is enterprise)', function () {
        var groupBy = 'DAY';
        var category = 'enterprise';
        var datePicker ={
            startDate : '2015-11-01 00:00:00.00',
            endDate : '2015-11-03 23:59:59.00'
        }

        var data1 = {"type":"DataUsageList","lastAvailableDate":"","groupByPeriod":"DAY","totalDataUsedInBytes":0,"count":0,"totalCount":0};
        var error = {errorStr:"errorStr"};
        var _mockPromise = {
            success: function(fn) {
                fn(data1);
                return _mockPromise;
            },
            error: function(fn) {
                fn(error);
                return _mockPromise;
            }
        };
        spyOn(_requestGraphData,'getUsageData').and.returnValue(_mockPromise);
        var modifiedData = {
            "usageUnit": "Bytes",
            "groupPeriod": "days",
            "dateRange": {
                "startDate": "2015-11-01 00:00:00.00",
                "endDate": "2015-11-03 23:59:59.00"
            },
            "data": [{
                "bar": true,
                "key": "Enterprise",
                "values": [{"cost": 0, "dataUsage": 0, "dataUsedInBytes": 0}]
            }]
        };
        spyOn(_dataUsageService,'prepareEnterpriseUsageData').and.returnValue(modifiedData);
        DataUsageHistoricalCtrl.getDataHistorical(category,datePicker,groupBy);
        expect(scope.graphObj.data).toBe(modifiedData.data);
        expect(scope.graphObj.options.chart.xAxis.axisLabel).toBe(modifiedData.groupPeriod);
        expect(scope.graphObj.options.chart.yAxis.axisLabel).toBe(modifiedData.usageUnit+' Used');

    });

    it('should verify getDataHistorical (when category is zones)', function () {

        scope.groupBy = 'DAY';
        scope.category = 'zones';
        scope.datePicker ={
            startDate : '2015-11-01 00:00:00.00',
            endDate : '2015-11-03 23:59:59.00'
        }
        var data1 = {
            "type": "AccountDataUsageList",
            "count": 1,
            "totalCount": 1,
            "groupByPeriod": "DAY",
            "totalDataUsedInBytes": 0,
            "totalDataUsageCost": 0.0000,
            "list": [{
                "type": "DataUsageList",
                "totalDataUsedInBytes": 0,
                "totalDataUsageCost": 0.0,
                "zoneId": 301,
                "zoneName": "Zone1",
                "zoneNickName": "Zone1-Nick11",
                "accountId": 52912,
                "accountName": "GigSky-GEMv1.1-Alpha-Load",
                "accountType": "ENTERPRISE"
            }]
        }
        var error = {errorStr:"errorStr"};
        var _mockPromise = {
            success: function(fn) {
                fn(data1);
                return _mockPromise;
            },
            error: function(fn) {
                fn(error);
                return _mockPromise;
            }
        };
        spyOn(_requestGraphData,'getUsageData').and.returnValue(_mockPromise);
        var modifiedData = {
            "usageUnit": "Bytes",
            "groupPeriod": "days",
            "dateRange": {
                "startDate": "2015-11-01 00:00:00.00",
                "endDate": "2015-11-03 23:59:59.00"
            },
            "data": [{
                "bar": true,
                "color": "#023fa5",
                "disabled": false,
                "enabled": true,
                "key": "zones7",
                "values": [{"cost": 0, "dataUsage": 0, "dataUsedInBytes": 0}]
            }]
        };
        spyOn(_dataUsageService,'prepareDataUsageHistorical').and.returnValue(modifiedData);
        DataUsageHistoricalCtrl.getDataHistorical(scope.category,scope.datePicker,scope.groupBy);
        expect(scope.graphObj.data).toBe(modifiedData.data);
        expect(scope.graphObj.options.chart.xAxis.axisLabel).toBe(modifiedData.groupPeriod);
        expect(scope.graphObj.options.chart.yAxis.axisLabel).toBe(modifiedData.usageUnit+' Used');

    });

    it('should verify categoryChange', function () {
        scope.categoryChange("enterprise");
        expect(scope.category).toBe("enterprise");

        scope.categoryChange("zones");
        expect(scope.category).toBe("zones");
    });

    it('should verify categoryChange', function () {
        scope.categoryChange("enterprise");
        expect(scope.category).toBe("enterprise");

        scope.categoryChange("zones");
        expect(scope.category).toBe("zones");
    });

    it('should verify graphTypeChange', function () {
        scope.graphType = 'multiBarChart';

        scope.graphTypeChange();
        expect(scope.graphObj.options.chart.type).toBe("multiBarChart");
    });

    it('should verify groupByChnage', function () {
        scope.groupBy = 'DAY';

        scope.groupByChnage();

        scope.groupBy = 'MONTH';

        scope.groupByChnage();
    });

});