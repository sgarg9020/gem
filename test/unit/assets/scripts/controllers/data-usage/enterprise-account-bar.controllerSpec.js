describe('DuEntAccCtrl Controller', function() {

    var _commonUtilityService, DuEntAccCtrl, scope, _AccountService,_requestGraphData,_dataUsageService;
    var vm;


    beforeEach(module('app'));

    beforeEach(inject(function($injector,$rootScope,$controller,requestGraphData,dataUsageService,commonUtilityService,AccountService){
        scope =$rootScope.$new();

        DuEntAccCtrl = $controller("DuEntAccCtrl",{$scope:scope,requestGraphData:requestGraphData,dataUsageService:dataUsageService,commonUtilityService:commonUtilityService,AccountService:AccountService});

        _commonUtilityService = commonUtilityService;
        _AccountService = AccountService;
        _requestGraphData = requestGraphData;
        _dataUsageService = dataUsageService;
        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
    }));
    it('should verify requestData', inject(function ($controller) {
        var data ={"billingStart":"2015-11-01 00:00:00.00","billingEnd":"2015-11-03 23:59:59.00"};
        var error = {errorStr:"errorStr"}

        var _mockPromise = {
            success: function(fn) {
                scope.graphObj.updateGraphOptions();
                fn(data, data.billingStart, data.billingEnd  );
                return _mockPromise;
            },
            error: function(fn) {
                fn(error);
                return _mockPromise;
            }
        };
        var data1 ={"company":"Default","values":[{"_id":"2015-11-10","dataUsage":16612},{"_id":"2015-11-12","dataUsage":5337},{"_id":"2015-11-03","dataUsage":22385},{"_id":"2015-11-09","dataUsage":22851},{"_id":"2015-11-11","dataUsage":20055},{"_id":"2015-11-05","dataUsage":31070},{"_id":"2015-11-01","dataUsage":22607},{"_id":"2015-11-08","dataUsage":21945},{"_id":"2015-11-04","dataUsage":29865},{"_id":"2015-11-07","dataUsage":19024},{"_id":"2015-11-02","dataUsage":34361},{"_id":"2015-11-06","dataUsage":26281}]};
        var error1 = {errorStr:"errorStr"}
        var _mockPromise1 = {
            success: function(fn) {
                scope.graphObj.updateGraphOptions();
                fn(data1, data.billingStart, data.billingEnd );
                return _mockPromise;
            },
            error: function(fn) {
                fn(error1);
                return _mockPromise;
            }
        };
        this.callback = function(){

        };
        spyOn(this,'callback');
        spyOn(_AccountService,'getBillingCycle').and.returnValue(_mockPromise);
        spyOn(_requestGraphData,'getDailyDataUsageOfEnterprise').and.returnValue(_mockPromise1);
        scope.graphObj.requestData(this.callback);
        expect(this.callback).toHaveBeenCalled();

    }));
    it('should verify requestData refresh', inject(function ($controller) {

        var data ={"billingStart":"2015-11-01T00:00:00.000Z","billingEnd":"2015-11-30T23:59:59.000Z"};
        var error = {errorStr:"errorStr"}
        var _mockPromise = {
            success: function(fn) {
                scope.graphObj.updateGraphOptions();
                fn(data, data.billingStart, data.billingEnd );
                return _mockPromise;
            },
            error: function(fn) {
                fn(error);
                return _mockPromise;
            }
        };

        this.callback = function(){

        };
        spyOn(this,'callback');
        spyOn(_AccountService,'getBillingCycle').and.returnValue(_mockPromise);
        scope.graphObj.data ={};

        scope.graphObj.requestData(this.callback);
        expect(this.callback).toHaveBeenCalled();
    }));
});