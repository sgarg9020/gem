describe('DuGroupsMultiBarCtrl Controller', function() {

    var _commonUtilityService, DuGroupsMultiBarCtrl, scope, _AccountService,_requestGraphData,_dataUsageService;


    beforeEach(module('app'));

    beforeEach(inject(function($injector,$rootScope,$controller,requestGraphData,dataUsageService,commonUtilityService,AccountService){
        scope =$rootScope.$new();

        DuGroupsMultiBarCtrl = $controller("DuGroupsMultiBarCtrl",{$scope:scope,requestGraphData:requestGraphData,dataUsageService:dataUsageService,commonUtilityService:commonUtilityService,AccountService:AccountService});

        _commonUtilityService = commonUtilityService;
        _AccountService = AccountService;
        _requestGraphData = requestGraphData;
        _dataUsageService = dataUsageService;
        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
    }));
    it('should verify requestData', inject(function ($controller) {
        var data ={"billingStart":"2015-11-01T00:00:00.000Z","billingEnd":"2015-11-30T23:59:59.000Z"};
        var error = {errorStr:"errorStr"}
        var _mockPromise = {
            success: function(fn) {
                fn(data);
                return _mockPromise;
            },
            error: function(fn) {
                fn(error);
                return _mockPromise;
            }
        };
        var data1 ={"type":"dataUsage","from":"2015-11-01T00:00:00.000Z","to":"2015-11-30T23:59:59.000Z","values":[{"_id":"California","values":[{"dataUsage":144943,"week":44,"start":"2015-11-02"},{"dataUsage":71841,"week":45,"start":"2015-11-08"}]},{"_id":"Australia","values":[{"dataUsage":15593,"week":44,"start":"2015-11-02"},{"dataUsage":7289,"week":45,"start":"2015-11-08"}]},{"_id":"Default","values":[{"dataUsage":21076,"week":44,"start":"2015-11-01"},{"dataUsage":6729,"week":45,"start":"2015-11-08"}]},{"_id":"US West","values":[{"dataUsage":3981,"week":44,"start":"2015-11-02"},{"dataUsage":941,"week":45,"start":"2015-11-10"}]}]};
        var error1 = {errorStr:"errorStr"}
        var _mockPromise1 = {
            success: function(fn) {
                fn(data1);
                return _mockPromise;
            },
            error: function(fn) {
                fn(error1);
                return _mockPromise;
            }
        };
        this.callback = function(){

        };
        spyOn(this,'callback');
        spyOn(_AccountService,'getBillingCycle').and.returnValue(_mockPromise);
        spyOn(_requestGraphData,'getWeeklyDataUsageOfGroups').and.returnValue(_mockPromise1);
        scope.graphObj.requestData(this.callback);
        expect(this.callback).toHaveBeenCalled();
    }));
    it('should verify requestData refresh', inject(function ($controller) {

        var data ={"billingStart":"2015-11-01T00:00:00.000Z","billingEnd":"2015-11-30T23:59:59.000Z"};
        var error = {errorStr:"errorStr"}
        var _mockPromise = {
            success: function(fn) {
                fn(data);
                return _mockPromise;
            },
            error: function(fn) {
                fn(error);
                return _mockPromise;
            }
        };

        this.callback = function(){

        };
        spyOn(this,'callback');
        spyOn(_AccountService,'getBillingCycle').and.returnValue(_mockPromise);
        scope.graphObj.data ={};
        scope.graphObj.requestData(this.callback);
        expect(this.callback).toHaveBeenCalled();
    }));
});