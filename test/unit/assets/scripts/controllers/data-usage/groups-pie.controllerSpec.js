describe('DuGroupsPieCtrl Controller', function() {

    var _commonUtilityService, DuGroupsPieCtrl, scope, _AccountService,_requestGraphData,_dataUsageService;


    beforeEach(module('app'));

    beforeEach(inject(function($injector,$rootScope,$controller,requestGraphData,dataUsageService,commonUtilityService,AccountService){
        scope =$rootScope.$new();

        DuGroupsPieCtrl = $controller("DuGroupsPieCtrl",{$scope:scope,requestGraphData:requestGraphData,dataUsageService:dataUsageService,commonUtilityService:commonUtilityService,AccountService:AccountService});

        _commonUtilityService = commonUtilityService;
        _AccountService = AccountService;
        _requestGraphData = requestGraphData;
        _dataUsageService = dataUsageService;
        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
    }));
    it('should verify requestData', inject(function ($controller) {
        var data ={"billingStart":"2015-11-01T00:00:00.000Z","billingEnd":"2015-11-30T23:59:59.000Z"};
        var error = {errorStr:"errorStr"}
        var _mockPromise = {
            success: function(fn) {
                fn(data);
                return _mockPromise;
            },
            error: function(fn) {
                fn(error);
                return _mockPromise;
            }
        };
        var data1 = {"type":"AccountDataUsageList","count":2,"totalCount":2,"groupByPeriod":"MONTH","totalDataUsedInBytes":99000000000,"totalDataUsageCost":0.0000,"list":[{"type":"DataUsageList","totalDataUsedInBytes":0,"totalDataUsageCost":0,"accountId":3,"accountName":"UberIndia","accountType":"GROUP","list":[{"type":"DataUsage","dataUsedInBytes":0,"fromDate":"2016-06-01 00:00:00","toDate":"2016-06-23 00:00:00","dataUsageCost":0}]},{"type":"DataUsageList","totalDataUsedInBytes":99000000000,"totalDataUsageCost":0.0000,"accountId":4,"accountName":"UberCalifornia","accountType":"GROUP","list":[{"type":"DataUsage","dataUsedInBytes":99000000000,"fromDate":"2016-06-20 00:00:00","toDate":"2016-06-20 00:00:00","dataUsageCost":0}]}]}
        var error1 = {errorStr:"errorStr"}
        var _mockPromise1 = {
            success: function(fn) {
                fn(data1);
                return _mockPromise;
            },
            error: function(fn) {
                fn(error1);
                return _mockPromise;
            }
        };
        this.callback = function(){

        };
        scope.graphObj.api ={};
        scope.graphObj.api.updateWithOptions = function(){

        };
        spyOn(this,'callback');
        spyOn(_AccountService,'getBillingCycle').and.returnValue(_mockPromise);
        spyOn(_requestGraphData,'getDailyDataUsageOfGroups').and.returnValue(_mockPromise1);
        scope.graphObj.requestData(this.callback);
        expect(this.callback).toHaveBeenCalled();
    }));
    it('should verify requestData refresh', inject(function ($controller) {

        var data ={"billingStart":"2015-11-01T00:00:00.000Z","billingEnd":"2015-11-30T23:59:59.000Z"};
        var error = {errorStr:"errorStr"}
        var _mockPromise = {
            success: function(fn) {
                fn(data);
                return _mockPromise;
            },
            error: function(fn) {
                fn(error);
                return _mockPromise;
            }
        };

        this.callback = function(){

        };
        scope.graphObj.api ={};
        scope.graphObj.api.updateWithOptions = function(){

        };
        spyOn(this,'callback');
        spyOn(_AccountService,'getBillingCycle').and.returnValue(_mockPromise);
        scope.graphObj.data ={};
        scope.graphObj.requestData(this.callback);
        expect(this.callback).toHaveBeenCalled();
    }));
});