describe('DefaultPerSIMPerGroupAlertsCtrl', function () {

    var _commonUtilityService,_alertsService,_alertsCommonFactory;
    var alert = {
        "type": "AlertConfiguration",
        "alertId": 4,
        "accountId": 4,
        "accountName": "UberCalifornia",
        "alertType": "DEFAULT_PER_SIM_PER_ACCOUNT",
        "alertHistoryActionStatus": "COMPLETED",
        "triggerCount": 1,
        "alertSpecification": {
            "type": "AlertSpecification",
            "enable": true,
            "limitValue": 1000.00,
            "limitType": "DATA",
            "actions": ["EMAIL"]
        },
        "updatedTime": "2016-06-27 01:42:28"
    };
    var MockAlertsResponse = {
        "type": "AlertConfigurationList",
        "count": 2,
        "totalCount": 2,
        "list": [alert,
            {
                "type": "AlertConfiguration",
                "alertId": 5,
                "accountId": 4,
                "accountName": "UberCalifornia",
                "alertType": "DEFAULT_PER_SIM_PER_ACCOUNT",
                "alertHistoryActionStatus": "COMPLETED",
                "triggerCount": 1,
                "alertSpecification": {
                    "type": "AlertSpecification",
                    "enable": true,
                    "limitValue": 5900.00,
                    "limitType": "DATA",
                    "actions": ["DISABLE"]
                }
            }]
    }

    beforeEach(module('app'));

    var groupDefaultPerSIMAlertsCtrl;
    var scope = {};
    scope.groupDefaultPerSIMAlerts = {};
    scope.$on= jasmine.createSpy('refreshDataTable spy method');
    beforeEach(inject(function ($injector, $controller, $compile, $timeout, dataTableConfigService, alertsService, commonUtilityService, alertsCommonFactory,SettingsService) {
        var expectedEnterpriseResponse = {"type":"Account","accountId":4,"accountName":"UberCalifornia"};
        var _mockEnterprisePromise = {
            success: function(fn) {
                fn(expectedEnterpriseResponse);
                return _mockEnterprisePromise;
            },
            error: function(fn) {
                fn(expectedEnterpriseResponse);
                return _mockEnterprisePromise;
            }
        };
        spyOn(SettingsService, 'getEnterpriseDetails').and.returnValue(_mockEnterprisePromise);

        groupDefaultPerSIMAlertsCtrl = $controller("GroupDefaultPerSIMAlertsCtrl", {
            $scope: scope,
            $compile: $compile,
            $timeout: $timeout,
            alertsService: alertsService,
            dataTableConfigService: dataTableConfigService,
            commonUtilityService: commonUtilityService,
            alertsCommonFactory: alertsCommonFactory
        });

        expect(scope.accountName).toBe('UberCalifornia');
        _commonUtilityService = commonUtilityService;
        _alertsService = alertsService;
        _alertsCommonFactory = alertsCommonFactory;

        scope.alertModalObj.updateAlertModal = jasmine.createSpy('updateAlertModal spy method');
        scope.alertModalObj.setShowSpend = jasmine.createSpy('setShowSpend spy method');
        scope.alertModalObj.setShowZones = jasmine.createSpy('setShowZones spy method');

        scope.dataTableObj.refreshDataTable = jasmine.createSpy('refreshDataTable spy method');

        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
        spyOn(_alertsCommonFactory, 'showError');
        spyOn(_alertsCommonFactory, 'setAlertCustomized');

    }));

    describe('Add alert', function () {

        it('should show success message', function () {
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };

            spyOn(_alertsService, 'addAlert').and.returnValue(_mockPromise);
            scope.addAlert(alert);
            expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Action has been added');
            expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();

        });

        it('should show appropriate error message on failure', function () {
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'addAlert').and.returnValue(_mockPromise);
            scope.addAlert(alert);
            expect(_alertsCommonFactory.showError).toHaveBeenCalled();
        });

    });


    describe('Edit alert', function () {
        scope.groupDefaultPerSIMAlertsList = MockAlertsResponse.list;

        it('modal should have Edit alert title message',function(){
            //Launch edit user modal
            scope.groupDefaultPerSIMAlerts.showAlertModal = function(alertType,isNew, isSpendAlert, isZoneAlert, isGroupAlert, editData){
                expect(editData).not.toEqual(null);
                expect(config.isNew).toBe(false);
                expect(config.isSpendAlert).toBe(false);
                expect(config.isZoneAlert).toBe(false);
                expect(config.isGroupAlert).toBe(false);

                scope.onAddCbk = scope.editAlert;
            };

            scope.showEditAlertModal(0);
        });

        it('should show success message', function () {
            scope.showEditAlertModal(0);

            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'editAlert').and.returnValue(_mockPromise);

            scope.editAlert(alert);
            expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Action has been updated');
            expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();

        });

        it('should show appropriate error message on failure', function () {
            scope.showEditAlertModal(0);
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'editAlert').and.returnValue(_mockPromise);
            scope.editAlert(alert);
            expect(_alertsCommonFactory.showError).toHaveBeenCalled();
        });


    });

    it('Should test data-table columnDefs 0',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[0].render(data,type,row,meta)).toContain(row.alertId);
    });

    it('Should test data-table columnDefs 1',function(){
        var data,type,row;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row)).toBe(row.alertSpecification.limitValue);
    });

    it('Should test data-table columnDefs 2',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data,type,row)).toContain("Warning");
    });

    it('Should test data-table columnDefs 3',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row)).toBe(row.triggerCount);
    });

    it('Should test data-table columnDefs 4(when alert status is enabled)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.alertSpecification.enable = true;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data,type,row)).toContain('Enabled');
    });

    it('Should test data-table columnDefs 4(when alert status is disabled)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.alertSpecification.enable = false;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data,type,row)).toContain('Disabled');
    });

    it('Should test data-table columnDefs 5',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row,meta)).toContain(row.alertId);
        expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row,meta)).toContain('Edit');
    });

    it('Should test fnServerData',function(){
        var sSource, aoData, fnCallback;
        var _mockPromise = {
            success: function(fn) {
                fn(MockAlertsResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(MockAlertsResponse);
                return _mockPromise;
            }
        };
        spyOn(_alertsService, 'getAlerts').and.returnValue(_mockPromise);
        scope.dataTableObj.dataTableRef ={};
        scope.dataTableObj.dataTableRef.fnSettings = function(){};

        scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
        expect(scope.groupDefaultPerSIMAlertsList.length).toBe(MockAlertsResponse.list.length);
        expect(_alertsCommonFactory.setAlertCustomized).toHaveBeenCalledWith(scope.groupDefaultPerSIMAlertsList);
    });

    it('Should test clearFilters',function(){
        scope.clearFilters();

        expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();
    });


});