describe('Alert modal controller', function() {

    var error = {errorStr:"error"};

    var AlertModalCtrl, scope,_controller,_rootScope,_sce, _modalDetails,_alertsService,_commonUtilityService,_SettingsService;

    beforeEach(module('app'));


    beforeEach(inject(function ($injector,$rootScope,$controller, $sce, modalDetails,alertsService,commonUtilityService,SettingsService) {

        scope = $rootScope.$new();
        _controller = $controller;
        _rootScope = $rootScope;
        _sce =$sce, _modalDetails =modalDetails ,_alertsService = alertsService,_commonUtilityService = commonUtilityService, _SettingsService = SettingsService;

        var alerts= {supportedAlertActionList:[]}
        var _mockPromise = {
            success: function(fn) {
                fn(alerts);
                return _mockPromise;
            },
            error: function(fn) {
                fn(error);
                return _mockPromise;
            }
        };
        spyOn(alertsService,'getAllowedAlertActionType').and.returnValue(_mockPromise);
    }));


    it('Should verify AccountAlerts controller init', function () {
        AlertModalCtrl = _controller("AlertModalCtrl", {
            $scope:scope,
            $sce:_sce,
            modalDetails:_modalDetails,
            alertsService:_alertsService,
            commonUtilityService:_commonUtilityService,
            SettingsService:_SettingsService
        });
        //expect(AccountAlertsCtrl.alertMessage).toEqual('Account alerts are generic and should be setup as the basis for alert configuration. If more flexibility to customize alerts is needed, alerts can added/edited at the group and/or individual SIM levels.');
    });
    it('Should verify AccountAlerts controller update alert modal function', function () {
        AlertModalCtrl = _controller("AlertModalCtrl", {
            $scope:scope,
            $sce:_sce,
            modalDetails:_modalDetails,
            alertsService:_alertsService,
            commonUtilityService:_commonUtilityService,
            SettingsService:_SettingsService
        });
        scope.$parent.alertModalObj.updateAlertModal(null,{
            header: 'Add Alert',
            cancel: 'Cancel',
            submit: 'Add Alert'
        }, function(){});
        //expect(AccountAlertsCtrl.alertMessage).toEqual('Account alerts are generic and should be setup as the basis for alert configuration. If more flexibility to customize alerts is needed, alerts can added/edited at the group and/or individual SIM levels.');
    });

    it('Should verify AccountAlerts controller update alert modal function', function () {
        AlertModalCtrl = _controller("AlertModalCtrl", {
            $scope:scope,
            $sce:_sce,
            modalDetails:_modalDetails,
            alertsService:_alertsService,
            commonUtilityService:_commonUtilityService,
            SettingsService:_SettingsService
        });
        scope.alertModal={alert:{}};
        var alert = {"type":"AlertConfiguration","alertId":2,"accountId":2,"accountName":"Uber1","alertType":"ACCOUNT_CUMULATIVE","triggerCount":0,"alertSpecification":{"type":"AlertSpecification","enable":true,"limitValue":1234,"limitType":"COST","actions":["DISABLE"]}};
        scope.$parent.alertModalObj.updateAlertModal(alert,{
            header: 'Add Alert',
            cancel: 'Cancel',
            submit: 'Add Alert'
        }, function(){});
        //expect(AccountAlertsCtrl.alertMessage).toEqual('Account alerts are generic and should be setup as the basis for alert configuration. If more flexibility to customize alerts is needed, alerts can added/edited at the group and/or individual SIM levels.');
    });

    it('Should verify AccountAlerts controller update alert modal function', function () {
        AlertModalCtrl = _controller("AlertModalCtrl", {
            $scope:scope,
            $sce:_sce,
            modalDetails:_modalDetails,
            alertsService:_alertsService,
            commonUtilityService:_commonUtilityService,
            SettingsService:_SettingsService
        });
        scope.alertModal={alert:{}};
        var alert = {"type":"AlertConfiguration","accountId":2,"accountName":"Uber1","alertType":"ACCOUNT_CUMULATIVE","triggerCount":0};
        scope.$parent.alertModalObj.updateAlertModal(alert,{
            header: 'Add Alert',
            cancel: 'Cancel',
            submit: 'Add Alert'
        }, function(){});
        //expect(AccountAlertsCtrl.alertMessage).toEqual('Account alerts are generic and should be setup as the basis for alert configuration. If more flexibility to customize alerts is needed, alerts can added/edited at the group and/or individual SIM levels.');
    });

    it('Should verify AccountAlerts controller update alert modal function', function () {
        AlertModalCtrl = _controller("AlertModalCtrl", {
            $scope:scope,
            $sce:_sce,
            modalDetails:_modalDetails,
            alertsService:_alertsService,
            commonUtilityService:_commonUtilityService,
            SettingsService:_SettingsService
        });
        scope.alertModal={alert:{}};
        var alert = {"type":"AlertConfiguration","zoneId":2,"accountId":2,"accountName":"Uber1","alertType":"ACCOUNT_CUMULATIVE","triggerCount":0,"alertSpecification":{"type":"AlertSpecification","enable":true,"limitType":"COST",triggerCount:1}};
        scope.$parent.alertModalObj.updateAlertModal(alert,{
            header: 'Add Alert',
            cancel: 'Cancel',
            submit: 'Add Alert'
        }, function(){});
        //expect(AccountAlertsCtrl.alertMessage).toEqual('Account alerts are generic and should be setup as the basis for alert configuration. If more flexibility to customize alerts is needed, alerts can added/edited at the group and/or individual SIM levels.');
    });
    it('Should verify AccountAlerts controller update alert modal function', function () {
        AlertModalCtrl = _controller("AlertModalCtrl", {
            $scope:scope,
            $sce:_sce,
            modalDetails:_modalDetails,
            alertsService:_alertsService,
            commonUtilityService:_commonUtilityService,
            SettingsService:_SettingsService
        });
        scope.alertModal={alert:{},modal:{}};
        scope.isFormValid = function(){
            return true;
        };
        //scope.alertModal={};
        spyOn(scope,'isFormValid').and.returnValue(true);
        var alertModalForm = {"$$parentForm":{},"$error":{},"$$success":{"required":[{"$viewValue":"DISABLE","$modelValue":"DISABLE","$$rawModelValue":"DISABLE","$validators":{},"$asyncValidators":{},"$parsers":[null],"$formatters":[null],"$viewChangeListeners":[],"$untouched":false,"$touched":true,"$pristine":false,"$dirty":true,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"parse":true},"$name":"alertAction","$options":null,"$$lastCommittedViewValue":"DISABLE","errorStr":null},{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}],"minlength":[{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}],"maxlength":[{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}],"parse":[{"$viewValue":"DISABLE","$modelValue":"DISABLE","$$rawModelValue":"DISABLE","$validators":{},"$asyncValidators":{},"$parsers":[null],"$formatters":[null],"$viewChangeListeners":[],"$untouched":false,"$touched":true,"$pristine":false,"$dirty":true,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"parse":true},"$name":"alertAction","$options":null,"$$lastCommittedViewValue":"DISABLE","errorStr":null}]},"$name":"alertModalForm","$dirty":true,"$pristine":false,"$valid":true,"$invalid":false,"$submitted":false,"alertAction":{"$viewValue":"DISABLE","$modelValue":"DISABLE","$$rawModelValue":"DISABLE","$validators":{},"$asyncValidators":{},"$parsers":[null],"$formatters":[null],"$viewChangeListeners":[],"$untouched":false,"$touched":true,"$pristine":false,"$dirty":true,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"parse":true},"$name":"alertAction","$options":null,"$$lastCommittedViewValue":"DISABLE","errorStr":null},"alertAllocationInCost":{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}};
        spyOn(_modalDetails,'submit').and.callFake(function(fn){
            fn(alertModalForm)
        });
        var alert = {"type":"AlertConfiguration","zoneId":2,"accountId":2,"accountName":"Uber1","alertType":"ACCOUNT_CUMULATIVE","triggerCount":0,"alertSpecification":{"type":"AlertSpecification","enable":true,"limitType":"COST",triggerCount:1}};
        scope.$parent.alertModalObj.updateAlertModal(alert,{
            header: 'Add Alert',
            cancel: 'Cancel',
            submit: 'Add Alert'
        }, function(){});
        //expect(AccountAlertsCtrl.alertMessage).toEqual('Account alerts are generic and should be setup as the basis for alert configuration. If more flexibility to customize alerts is needed, alerts can added/edited at the group and/or individual SIM levels.');
    });
    it('Should verify AccountAlerts controller update alert modal function', function () {
        AlertModalCtrl = _controller("AlertModalCtrl", {
            $scope:scope,
            $sce:_sce,
            modalDetails:_modalDetails,
            alertsService:_alertsService,
            commonUtilityService:_commonUtilityService,
            SettingsService:_SettingsService
        });
        scope.alertModal={alert:{},modal:{}};
        scope.isFormValid = function(){
            return true;
        };
        //scope.alertModal={};
        spyOn(scope,'isFormValid').and.returnValue(false);
        var alertModalForm = {"$$parentForm":{},"$error":{},"$$success":{"required":[{"$viewValue":"DISABLE","$modelValue":"DISABLE","$$rawModelValue":"DISABLE","$validators":{},"$asyncValidators":{},"$parsers":[null],"$formatters":[null],"$viewChangeListeners":[],"$untouched":false,"$touched":true,"$pristine":false,"$dirty":true,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"parse":true},"$name":"alertAction","$options":null,"$$lastCommittedViewValue":"DISABLE","errorStr":null},{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}],"minlength":[{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}],"maxlength":[{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}],"parse":[{"$viewValue":"DISABLE","$modelValue":"DISABLE","$$rawModelValue":"DISABLE","$validators":{},"$asyncValidators":{},"$parsers":[null],"$formatters":[null],"$viewChangeListeners":[],"$untouched":false,"$touched":true,"$pristine":false,"$dirty":true,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"parse":true},"$name":"alertAction","$options":null,"$$lastCommittedViewValue":"DISABLE","errorStr":null}]},"$name":"alertModalForm","$dirty":true,"$pristine":false,"$valid":true,"$invalid":false,"$submitted":false,"alertAction":{"$viewValue":"DISABLE","$modelValue":"DISABLE","$$rawModelValue":"DISABLE","$validators":{},"$asyncValidators":{},"$parsers":[null],"$formatters":[null],"$viewChangeListeners":[],"$untouched":false,"$touched":true,"$pristine":false,"$dirty":true,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"parse":true},"$name":"alertAction","$options":null,"$$lastCommittedViewValue":"DISABLE","errorStr":null},"alertAllocationInCost":{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}};
        spyOn(_modalDetails,'submit').and.callFake(function(fn){
            fn(alertModalForm)
        });
        var alert = {"type":"AlertConfiguration","zoneId":2,"accountId":2,"accountName":"Uber1","alertType":"ACCOUNT_CUMULATIVE","triggerCount":0,"alertSpecification":{"type":"AlertSpecification","enable":true,"limitType":"COST",triggerCount:1}};
        scope.$parent.alertModalObj.updateAlertModal(alert,{
            header: 'Add Alert',
            cancel: 'Cancel',
            submit: 'Add Alert'
        }, function(){});
        //expect(AccountAlertsCtrl.alertMessage).toEqual('Account alerts are generic and should be setup as the basis for alert configuration. If more flexibility to customize alerts is needed, alerts can added/edited at the group and/or individual SIM levels.');
    });

    it('Should verify AccountAlerts controller update alert modal function', function () {
        AlertModalCtrl = _controller("AlertModalCtrl", {
            $scope:scope,
            $sce:_sce,
            modalDetails:_modalDetails,
            alertsService:_alertsService,
            commonUtilityService:_commonUtilityService,
            SettingsService:_SettingsService
        });
        scope.alertModal={alert:{},modal:{}};
        scope.isFormValid = function(){
            return true;
        };
        scope.getUnformattedDataForAutonumericField = function(){
            return 123;
        }
        scope.$parent.alertModalObj.setShowSpend(true);
        //scope.alertModal={};
        spyOn(scope,'isFormValid').and.returnValue(true);
        var alertModalForm = {"$$parentForm":{},"$error":{},"$$success":{"required":[{"$viewValue":"DISABLE","$modelValue":"DISABLE","$$rawModelValue":"DISABLE","$validators":{},"$asyncValidators":{},"$parsers":[null],"$formatters":[null],"$viewChangeListeners":[],"$untouched":false,"$touched":true,"$pristine":false,"$dirty":true,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"parse":true},"$name":"alertAction","$options":null,"$$lastCommittedViewValue":"DISABLE","errorStr":null},{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}],"minlength":[{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}],"maxlength":[{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}],"parse":[{"$viewValue":"DISABLE","$modelValue":"DISABLE","$$rawModelValue":"DISABLE","$validators":{},"$asyncValidators":{},"$parsers":[null],"$formatters":[null],"$viewChangeListeners":[],"$untouched":false,"$touched":true,"$pristine":false,"$dirty":true,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"parse":true},"$name":"alertAction","$options":null,"$$lastCommittedViewValue":"DISABLE","errorStr":null}]},"$name":"alertModalForm","$dirty":true,"$pristine":false,"$valid":true,"$invalid":false,"$submitted":false,"alertAction":{"$viewValue":"DISABLE","$modelValue":"DISABLE","$$rawModelValue":"DISABLE","$validators":{},"$asyncValidators":{},"$parsers":[null],"$formatters":[null],"$viewChangeListeners":[],"$untouched":false,"$touched":true,"$pristine":false,"$dirty":true,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"parse":true},"$name":"alertAction","$options":null,"$$lastCommittedViewValue":"DISABLE","errorStr":null},"alertAllocationInCost":{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}};
        spyOn(_modalDetails,'submit').and.callFake(function(fn){
            fn(alertModalForm)
        });
        var alert = {"type":"AlertConfiguration","zoneId":2,"accountId":2,"accountName":"Uber1","alertType":"ACCOUNT_CUMULATIVE","triggerCount":0,"alertSpecification":{"type":"AlertSpecification","enable":true,"limitType":"COST",triggerCount:1}};
        scope.$parent.alertModalObj.updateAlertModal(alert,{
            header: 'Add Alert',
            cancel: 'Cancel',
            submit: 'Add Alert'
        }, function(){});
        //expect(AccountAlertsCtrl.alertMessage).toEqual('Account alerts are generic and should be setup as the basis for alert configuration. If more flexibility to customize alerts is needed, alerts can added/edited at the group and/or individual SIM levels.');
    });

    it('Should verify AccountAlerts controller update alert modal function', function () {
        AlertModalCtrl = _controller("AlertModalCtrl", {
            $scope:scope,
            $sce:_sce,
            modalDetails:_modalDetails,
            alertsService:_alertsService,
            commonUtilityService:_commonUtilityService,
            SettingsService:_SettingsService
        });
        scope.alertModal={alert:{},modal:{}};
        scope.isFormValid = function(){
            return true;
        };
        scope.getUnformattedDataForAutonumericField = function(){
            return 123;
        };
        AlertModalCtrl.zones=[];
        scope.$parent.alertModalObj.setShowZones(true);
        //scope.alertModal={};
        spyOn(scope,'isFormValid').and.returnValue(true);
        var alertModalForm = {"$$parentForm":{},"$error":{},"$$success":{"required":[{"$viewValue":"DISABLE","$modelValue":"DISABLE","$$rawModelValue":"DISABLE","$validators":{},"$asyncValidators":{},"$parsers":[null],"$formatters":[null],"$viewChangeListeners":[],"$untouched":false,"$touched":true,"$pristine":false,"$dirty":true,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"parse":true},"$name":"alertAction","$options":null,"$$lastCommittedViewValue":"DISABLE","errorStr":null},{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}],"minlength":[{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}],"maxlength":[{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}],"parse":[{"$viewValue":"DISABLE","$modelValue":"DISABLE","$$rawModelValue":"DISABLE","$validators":{},"$asyncValidators":{},"$parsers":[null],"$formatters":[null],"$viewChangeListeners":[],"$untouched":false,"$touched":true,"$pristine":false,"$dirty":true,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"parse":true},"$name":"alertAction","$options":null,"$$lastCommittedViewValue":"DISABLE","errorStr":null}]},"$name":"alertModalForm","$dirty":true,"$pristine":false,"$valid":true,"$invalid":false,"$submitted":false,"alertAction":{"$viewValue":"DISABLE","$modelValue":"DISABLE","$$rawModelValue":"DISABLE","$validators":{},"$asyncValidators":{},"$parsers":[null],"$formatters":[null],"$viewChangeListeners":[],"$untouched":false,"$touched":true,"$pristine":false,"$dirty":true,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"parse":true},"$name":"alertAction","$options":null,"$$lastCommittedViewValue":"DISABLE","errorStr":null},"alertAllocationInCost":{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}};
        spyOn(_modalDetails,'submit').and.callFake(function(fn){
            fn(alertModalForm)
        });
        var alert = {"type":"AlertConfiguration","zoneId":2,"accountId":2,"accountName":"Uber1","alertType":"ACCOUNT_CUMULATIVE","triggerCount":0,"alertSpecification":{"type":"AlertSpecification","enable":true,"limitType":"COST",triggerCount:1}};
        scope.$parent.alertModalObj.updateAlertModal(alert,{
            header: 'Add Alert',
            cancel: 'Cancel',
            submit: 'Add Alert'
        }, function(){});
        //expect(AccountAlertsCtrl.alertMessage).toEqual('Account alerts are generic and should be setup as the basis for alert configuration. If more flexibility to customize alerts is needed, alerts can added/edited at the group and/or individual SIM levels.');
    });
    it('Should verify AccountAlerts controller update alert modal function', function () {
        AlertModalCtrl = _controller("AlertModalCtrl", {
            $scope:scope,
            $sce:_sce,
            modalDetails:_modalDetails,
            alertsService:_alertsService,
            commonUtilityService:_commonUtilityService,
            SettingsService:_SettingsService
        });
        scope.alertModal={alert:{},modal:{}};
        scope.isFormValid = function(){
            return false;
        };
        scope.getUnformattedDataForAutonumericField = function(){
            return 123;
        };

        var zones= [];
        var _mockPromise1 = {
            success: function(fn) {
                fn(zones);
                return _mockPromise1;
            },
            error: function(fn) {
                fn(error);
                return _mockPromise1;
            }
        };
        scope.generalObj.isNoChangeError = true;

        spyOn(_SettingsService,'getPlanDetails').and.returnValue(_mockPromise1);
        scope.$parent.alertModalObj.setShowZones(true);
        //scope.alertModal={};
        spyOn(scope,'isFormValid').and.returnValue(false);
        var alertModalForm = {"$$parentForm":{},"$error":{},"$$success":{"required":[{"$viewValue":"DISABLE","$modelValue":"DISABLE","$$rawModelValue":"DISABLE","$validators":{},"$asyncValidators":{},"$parsers":[null],"$formatters":[null],"$viewChangeListeners":[],"$untouched":false,"$touched":true,"$pristine":false,"$dirty":true,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"parse":true},"$name":"alertAction","$options":null,"$$lastCommittedViewValue":"DISABLE","errorStr":null},{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}],"minlength":[{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}],"maxlength":[{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}],"parse":[{"$viewValue":"DISABLE","$modelValue":"DISABLE","$$rawModelValue":"DISABLE","$validators":{},"$asyncValidators":{},"$parsers":[null],"$formatters":[null],"$viewChangeListeners":[],"$untouched":false,"$touched":true,"$pristine":false,"$dirty":true,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"parse":true},"$name":"alertAction","$options":null,"$$lastCommittedViewValue":"DISABLE","errorStr":null}]},"$name":"alertModalForm","$dirty":true,"$pristine":false,"$valid":true,"$invalid":false,"$submitted":false,"alertAction":{"$viewValue":"DISABLE","$modelValue":"DISABLE","$$rawModelValue":"DISABLE","$validators":{},"$asyncValidators":{},"$parsers":[null],"$formatters":[null],"$viewChangeListeners":[],"$untouched":false,"$touched":true,"$pristine":false,"$dirty":true,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"parse":true},"$name":"alertAction","$options":null,"$$lastCommittedViewValue":"DISABLE","errorStr":null},"alertAllocationInCost":{"$viewValue":"1234","$modelValue":1234,"$$rawModelValue":1234,"$validators":{},"$asyncValidators":{},"$parsers":[],"$formatters":[null],"$viewChangeListeners":[],"$untouched":true,"$touched":false,"$pristine":true,"$dirty":false,"$valid":true,"$invalid":false,"$error":{},"$$success":{"required":true,"minlength":true,"maxlength":true},"$name":"alertAllocationInCost","$options":null,"$$lastCommittedViewValue":"1234","errorStr":null}};
        spyOn(_modalDetails,'submit').and.callFake(function(fn){
            fn(alertModalForm)
        });
        var alert = {"type":"AlertConfiguration","zoneId":2,"accountId":2,"accountName":"Uber1","alertType":"ACCOUNT_CUMULATIVE","triggerCount":0,"alertSpecification":{"type":"AlertSpecification","enable":true,"limitType":"COST",triggerCount:1}};
        scope.$parent.alertModalObj.updateAlertModal(alert,{
            header: 'Add Alert',
            cancel: 'Cancel',
            submit: 'Add Alert'
        }, function(){});
        //expect(AccountAlertsCtrl.alertMessage).toEqual('Account alerts are generic and should be setup as the basis for alert configuration. If more flexibility to customize alerts is needed, alerts can added/edited at the group and/or individual SIM levels.');
    });
});