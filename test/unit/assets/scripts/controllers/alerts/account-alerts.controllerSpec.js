describe('Account alerts controller', function() {

    var error = {errorStr:"error"};

    var AccountAlertsCtrl, scope,_controller,_rootScope,_location;

    beforeEach(module('app'));


    beforeEach(inject(function ($injector,$rootScope,$controller, $location) {

        scope = $rootScope.$new();
        _location=$location;
        _controller = $controller;
        _rootScope = $rootScope;

    }));


    it('Should verify AccountAlerts controller init', function () {
        AccountAlertsCtrl = _controller("AccountAlertsCtrl", {
            $location: _location
        });
        expect(AccountAlertsCtrl.alertMessage).toEqual('Account alerts are generic.If more flexibility is required, alerts can be added/edited at the group and/or individual SIM levels.');
    });
    it('Should verify AccountAlerts controller set message', function () {
        AccountAlertsCtrl = _controller("AccountAlertsCtrl", {
            $location: _location
        });
        AccountAlertsCtrl.setMessages({currentTarget:{href:'enterprise-spend'}});
        expect(AccountAlertsCtrl.alertMessage).toEqual('Account alerts are generic.If more flexibility is required, alerts can be added/edited at the group and/or individual SIM levels.');
    });

});