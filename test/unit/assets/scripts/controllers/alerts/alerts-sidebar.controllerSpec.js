describe('Alert sidebar controller', function() {

    var error = {errorStr:"error"};

    var AccountAlertsCtrl, scope,_controller,_rootScope,_timeout,_alertsService,_commonUtilityService;

    beforeEach(module('app'));


    beforeEach(inject(function ($injector,$rootScope,$controller, $timeout, alertsService, commonUtilityService) {

        scope = $rootScope.$new();
        _controller = $controller;
        _rootScope = $rootScope;
        _timeout =$timeout, _alertsService= alertsService, _commonUtilityService =commonUtilityService;

    }));


    it('Should verify AccountAlerts controller init', function () {
        var expectedResponse ={
            "type": "AlertHistoryResponse",
            "list": [
                {
                    "type": "AlertHistory",
                    "accountId": 1,
                    "accountName" : "Uber",
                    "alertId": 100,
                    "alertHistoryId": 101,
                    "alertType" : "ACCOUNT_CUMULATIVE",
                    "zoneId": 4,
                    "zoneName": "zone_1",
                    "zoneNickName": "USA",
                    "simId": "1234",
                    "limitValue": "20",
                    "limitType": "DATA/COST",
                    "occurredTime": "yyyy-MM-dd HH:mm:ss in UTC",
                    "action": "EMAIL",
                    "actionStatus" : "COMPLETED"
                }
            ],
            "count":1,
            "totalCount":1
        }
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_alertsService, 'getAlertHistory').and.returnValue(_mockPromise);

        AlertSidebarCtrl = _controller("AlertSidebarCtrl", {
            $scope : scope,
            $timeout : _timeout,
            alertsService : _alertsService,
            commonUtilityService : _commonUtilityService,
            $rootScope: _rootScope
        });
        _timeout.flush();
    });

});