describe('ZoneSpendAlertsCtrl', function () {

    var _commonUtilityService,_alertsService,_alertsCommonFactory;
    var alert = {
        "type": "AlertConfiguration",
        "alertId": 1,
        "accountId": 1,
        "alertType": "ZONE_CUMULATIVE",
        "zoneId": 1,
        "zoneName": "zone_1",
        "alertSpecification": {
            "enable": true,
            "limitValue": "$1000",
            "limitType": "COST",
            "triggerCount": 7,
            "actions": ["Email"]
        }
    };
    var MockAlertsResponse = {
        "type": "AlertConfigurationList",
        "count": 2,
        "totalCount": 2,
        "list": [{
            "type": "AlertConfiguration",
            "alertId": 2,
            "accountId": 2,
            "accountName": "Uber",
            "alertType": "ZONE_CUMULATIVE",
            "zoneId": 1,
            "zoneName": "2-Zone1",
            "zoneNickName": "2-Zone1",
            "triggerCount": 311,
            "alertSpecification": {
                "type": "AlertSpecification",
                "enable": true,
                "limitValue": 1000.00,
                "limitType": "COST",
                "actions": ["DISABLE"]
            },
            "updatedTime": "2016-06-21 11:48:55"
        }, {
            "type": "AlertConfiguration",
            "alertId": 5,
            "accountId": 2,
            "accountName": "Uber",
            "alertType": "ZONE_CUMULATIVE",
            "zoneId": 1,
            "zoneName": "2-Zone1",
            "zoneNickName": "2-Zone1",
            "triggerCount": 0,
            "alertSpecification": {
                "type": "AlertSpecification",
                "enable": true,
                "limitValue": 123.00,
                "limitType": "COST",
                "actions": ["EMAIL"]
            }
        }]
    };

    beforeEach(module('app'));

    var zoneSpendAlertsCtrl;
    var scope = {};
    scope.zoneSpendAlerts = {};
    scope.$on= jasmine.createSpy('refreshDataTable spy method');
    beforeEach(inject(function ($injector, $controller, $compile, $timeout, dataTableConfigService, alertsService, commonUtilityService, alertsCommonFactory) {
        zoneSpendAlertsCtrl = $controller("ZoneSpendAlertsCtrl", {
            $scope: scope,
            $compile: $compile,
            $timeout: $timeout,
            alertsService: alertsService,
            dataTableConfigService: dataTableConfigService,
            commonUtilityService: commonUtilityService,
            alertsCommonFactory: alertsCommonFactory
        });

        var accountId = 1;

        _commonUtilityService = commonUtilityService;
        _alertsService = alertsService;
        _alertsCommonFactory = alertsCommonFactory;
        scope.alertModalObj.updateAlertModal = jasmine.createSpy('updateAlertModal spy method');
        scope.alertModalObj.setShowSpend = jasmine.createSpy('setShowSpend spy method');
        scope.alertModalObj.setShowZones = jasmine.createSpy('setShowZones spy method');

        scope.dataTableObj.refreshDataTable = jasmine.createSpy('refreshDataTable spy method');

        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
        spyOn(_alertsCommonFactory, 'showError');
    }));

    describe('Add alert', function () {

        it('should show success message', function () {
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'addAlert').and.returnValue(_mockPromise);
            scope.addAlert(alert);
            expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Action has been added');
            expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();
        });

        it('should show appropriate error message on failure', function () {
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'addAlert').and.returnValue(_mockPromise);
            scope.addAlert(alert);
            expect(_alertsCommonFactory.showError).toHaveBeenCalled();
        });

    });


   describe('Edit alert', function () {
        scope.zoneSpendAlertsList = MockAlertsResponse.list;

        it('modal should have Edit alert title message',function(){
            //Launch edit user modal
            scope.zoneSpendAlerts.showAlertModal = function(alertType,isNew, isSpendAlert, isZoneAlert, isGroupAlert, editData){
                expect(editData).not.toEqual(null);
                expect(config.isNew).toBe(false);
                expect(config.isSpendAlert).toBe(true);
                expect(config.isZoneAlert).toBe(true);
                expect(config.isGroupAlert).toBe(false);
            };

            scope.showEditAlertModal(0);
        });

        it('should show success message', function () {
            scope.showEditAlertModal(0);
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'editAlert').and.returnValue(_mockPromise);

            scope.editAlert(alert);
            expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Action has been updated');
            expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();
        });

        it('should show appropriate error message on failure', function () {
            scope.showEditAlertModal(0);
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'editAlert').and.returnValue(_mockPromise);

            scope.editAlert(alert);
            expect(_alertsCommonFactory.showError).toHaveBeenCalled();
        });
    });

    it('Should test data-table columnDefs 0',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[0].render(data,type,row,meta)).toContain(row.alertId);
    });

    it('Should test data-table columnDefs 1',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toContain(row.zoneName);
    });

    it('Should test data-table columnDefs 2(when limitType is DATA)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.alertSpecification.limitType = 'DATA';
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data,type,row,meta)).toContain(row.alertSpecification.limitValue);
    });

    it('Should test data-table columnDefs 2(when limitType is COST)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        sessionStorage.setItem( "gs-enterpriseCurrency","USD");
        alert.alertSpecification.limitType = 'COST';
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data,type,row,meta)).toContain('$'+row.alertSpecification.limitValue);
    });

    it('Should test data-table columnDefs 3',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toContain(row.alertSpecification.actions[0]);
    });

    it('Should test data-table columnDefs 4(when triggered count is 0)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.triggerCount = 0;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data,type,row,meta)).toContain('NO');
    });

    it('Should test data-table columnDefs 4(when triggered count is greater then 0)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.triggerCount = 300;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data,type,row,meta)).toContain('YES');
    });

    it('Should test data-table columnDefs 5(when alert status is enabled)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.alertSpecification.enable = true;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row,meta)).toContain('Enabled');
    });

    it('Should test data-table columnDefs 5(when alert status is disabled)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.alertSpecification.enable = false;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row,meta)).toContain('Disabled');
    });

    it('Should test data-table columnDefs 6',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[6].render(data,type,row,meta)).toContain(row.alertId);
        expect(scope.dataTableObj.dataTableOptions.columnDefs[6].render(data,type,row,meta)).toContain('Edit');
    });

    it('Should test fnServerData',function(){
        var sSource, aoData, fnCallback;
        var _mockPromise = {
            success: function(fn) {
                fn(MockAlertsResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(MockAlertsResponse);
                return _mockPromise;
            }
        };
        spyOn(_alertsService, 'getAlerts').and.returnValue(_mockPromise);

        scope.zoneSpendAlerts.zoneId = 1;
        scope.dataTableObj.dataTableRef ={};
        scope.dataTableObj.dataTableRef.fnSettings = function(){};

        scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
        expect(scope.zoneSpendAlertsList.length).toBe(MockAlertsResponse.list.length);
    });

    it('Should test clearFilters',function(){
        scope.clearFilters();

        expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();
    });

});