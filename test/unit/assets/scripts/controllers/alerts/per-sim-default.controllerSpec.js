describe('DefaultPerSIMAlertsCtrl', function () {

    var _commonUtilityService,_alertsService,_alertsCommonFactory;
    var alert = {
        "type": "AlertConfiguration",
        "alertId": 16,
        "accountId": 2,
        "accountName": "Uber",
        "alertType": "DEFAULT_PER_SIM_PER_ACCOUNT",
        "triggerCount": 0,
        "alertSpecification": {
            "type": "AlertSpecification",
            "enable": true,
            "limitValue": 1000.00,
            "limitType": "DATA",
            "actions": ["EMAIL"]
        }
    };

    var MockAlertsResponse = {
        "type": "AlertConfigurationList",
        "count": 1,
        "totalCount": 1,
        "list": [alert]
    }

    beforeEach(module('app'));

    var defaultPerSIMAlertsCtrl;
    var scope = {};
    scope.defaultPerSIMAlerts = {};
    scope.$on= jasmine.createSpy('refreshDataTable spy method');
    beforeEach(inject(function ($injector, $controller, $compile, $timeout, dataTableConfigService, alertsService, commonUtilityService, alertsCommonFactory) {
        defaultPerSIMAlertsCtrl = $controller("DefaultPerSIMAlertsCtrl", {
            $scope: scope,
            $compile: $compile,
            $timeout: $timeout,
            alertsService: alertsService,
            dataTableConfigService: dataTableConfigService,
            commonUtilityService: commonUtilityService,
            alertsCommonFactory: alertsCommonFactory
        });

        _commonUtilityService = commonUtilityService;
        _alertsCommonFactory = alertsCommonFactory;
        _alertsService = alertsService;

        scope.alertModalObj.updateAlertModal = jasmine.createSpy('updateAlertModal spy method');
        scope.alertModalObj.setShowSpend = jasmine.createSpy('setShowSpend spy method');
        scope.alertModalObj.setShowZones = jasmine.createSpy('setShowZones spy method');

        scope.dataTableObj.refreshDataTable = jasmine.createSpy('refreshDataTable spy method');

        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
        spyOn(_alertsCommonFactory, 'showError');

    }));

    describe('Add alert', function () {

        it('should show success message', function () {
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'addAlert').and.returnValue(_mockPromise);
            scope.addAlert(alert);
            expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Action has been added');
            expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();

        });

        it('should show appropriate error message on failure', function () {
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'addAlert').and.returnValue(_mockPromise);
            scope.addAlert(alert);
            expect(_alertsCommonFactory.showError).toHaveBeenCalled();
        });

    });


    describe('Edit alert', function () {
        scope.defaultPerSIMAlertsList = MockAlertsResponse.list;

        it('modal should have Edit alert title message',function(){
            //Launch edit user modal
            scope.defaultPerSIMAlerts.showAlertModal = function(alertType,isNew, isSpendAlert, isZoneAlert, isGroupAlert, editData){
                expect(editData).not.toEqual(null);
                expect(config.isNew).toBe(false);
                expect(config.isSpendAlert).toBe(false);
                expect(config.isZoneAlert).toBe(false);
                expect(config.isGroupAlert).toBe(false);

                scope.onAddCbk = scope.editAlert;
            };

            scope.showEditAlertModal(0);
        });

        it('should show success message', function () {
            scope.showEditAlertModal(0);
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'editAlert').and.returnValue(_mockPromise);

            scope.editAlert(alert);
            expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Action has been updated');
            expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();
        });

        it('should show appropriate error message on failure', function () {
            scope.showEditAlertModal(0);
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_alertsService, 'editAlert').and.returnValue(_mockPromise);
            scope.editAlert(alert);
            expect(_alertsCommonFactory.showError).toHaveBeenCalled();
        });


    });

    it('Should test data-table columnDefs 0',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[0].render(data,type,row)).toContain(row.alertId);
    });

    it('Should test data-table columnDefs 1',function(){
        var data,type,row;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row)).toBe(row.alertSpecification.limitValue);
    });

   it('Should test data-table columnDefs 2',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data,type,row)).toContain("Warning");
    });

    it('Should test data-table columnDefs 3',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row)).toBe(row.triggerCount);
    });

    it('Should test data-table columnDefs 4(when alert status is enabled)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.alertSpecification.enable = true;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data,type,row)).toContain('Enabled');
    });

    it('Should test data-table columnDefs 4(when alert status is disabled)',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        alert.alertSpecification.enable = false;
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data,type,row)).toContain('Disabled');
    });

    it('Should test data-table columnDefs 5',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = alert;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row,meta)).toContain(row.alertId);
        expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row,meta)).toContain('Edit');
    });

    it('Should test fnServerData',function(){
        var sSource, aoData, fnCallback;
        var _mockPromise = {
            success: function(fn) {
                fn(MockAlertsResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(MockAlertsResponse);
                return _mockPromise;
            }
        };
        spyOn(_alertsService, 'getAlerts').and.returnValue(_mockPromise);
        scope.dataTableObj.dataTableRef ={};
        scope.dataTableObj.dataTableRef.fnSettings = function(){};

        scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
        expect(scope.defaultPerSIMAlertsList.length).toBe(MockAlertsResponse.list.length);
    });

});