describe('Triggered alerts controller', function() {

    var error = {errorStr:"error"};

    var AccountAlertsCtrl, scope,_controller,_rootScope,_timeout,_alertsService,_commonUtilityService,_sce, _compile, _loadingState, _dataTableConfigService, _alertsCommonFactory,_filter,_location;

    beforeEach(module('app'));


    beforeEach(inject(function ($injector,$rootScope,$controller, $sce, $compile, loadingState, dataTableConfigService, alertsService, commonUtilityService,alertsCommonFactory,$filter,$location) {

        scope = $rootScope.$new();
        _controller = $controller;
        _rootScope = $rootScope;
        _alertsService= alertsService, _commonUtilityService =commonUtilityService,_sce=$sce, _compile=$compile, _loadingState=loadingState, _dataTableConfigService=dataTableConfigService, _alertsCommonFactory=alertsCommonFactory,_filter=$filter,_location=$location;

        var expectedResponse ={};
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_alertsService, 'getAlertHistory').and.returnValue(_mockPromise);
        spyOn(dataTableConfigService,'prepareRequestParams').and.returnValue({});
        spyOn(dataTableConfigService,'prepareResponseData').and.returnValue({aaData:{}});
    }));


    it('Should verify AccountAlerts controller init', function () {

        TranspiredAlertsCtrl = _controller("TranspiredAlertsCtrl", {
            $scope : scope,
            $sce:_sce,
            $compile :_compile,
            loadingState :_loadingState,
            dataTableConfigService :_dataTableConfigService,
            alertsCommonFactory :_alertsCommonFactory,
            $filter :_filter,
            $location :_location,
            alertsService : _alertsService,
            commonUtilityService : _commonUtilityService
        });
        scope.dataTableObj.dataTableRef={fnSettings:function(){return{};}};
        scope.dataTableObj.dataTableOptions.fnServerData({},{},function(){});
    });

    it('Should verify AccountAlerts controller init', function () {
        spyOn(_location,'search').and.returnValue({alertId:1,simId:123,alertType:'ACCOUNT'});

        TranspiredAlertsCtrl = _controller("TranspiredAlertsCtrl", {
            $scope : scope,
            $sce:_sce,
            $compile :_compile,
            loadingState :_loadingState,
            dataTableConfigService :_dataTableConfigService,
            alertsCommonFactory :_alertsCommonFactory,
            $filter :_filter,
            $location :_location,
            alertsService : _alertsService,
            commonUtilityService : _commonUtilityService
        });
        scope.dataTableObj.dataTableRef={fnSettings:function(){return{};}};
        scope.dataTableObj.dataTableOptions.fnServerData({},{},function(){});
    });

    it('Should verify AccountAlerts controller init', function () {
        spyOn(_location,'search').and.returnValue({alertId:1,simId:123,alertType:'ACCOUNT'});

        TranspiredAlertsCtrl = _controller("TranspiredAlertsCtrl", {
            $scope : scope,
            $sce:_sce,
            $compile :_compile,
            loadingState :_loadingState,
            dataTableConfigService :_dataTableConfigService,
            alertsCommonFactory :_alertsCommonFactory,
            $filter :_filter,
            $location :_location,
            alertsService : _alertsService,
            commonUtilityService : _commonUtilityService
        });
        scope.refreshAlerts();
    });
    it('Should verify AccountAlerts controller init', function () {
        spyOn(_location,'search').and.returnValue({alertId:1,simId:123,alertType:'ACCOUNT'});
        TranspiredAlertsCtrl = _controller("TranspiredAlertsCtrl", {
            $scope : scope,
            $sce:_sce,
            $compile :_compile,
            loadingState :_loadingState,
            dataTableConfigService :_dataTableConfigService,
            alertsCommonFactory :_alertsCommonFactory,
            $filter :_filter,
            $location :_location,
            alertsService : _alertsService,
            commonUtilityService : _commonUtilityService
        });
        scope.dataTableObj.refreshDataTable = function(){
            return ;
        };
        scope.dataTableObj.alertType.dbRef = 'ABC';
        scope.refreshAlerts();
    });
});