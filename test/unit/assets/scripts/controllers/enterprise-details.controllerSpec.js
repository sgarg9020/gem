describe('Enterprise Details Controller', function() {

    var _CommonUtilityService,EnterpriseDetailsCtrl,scope,_SettingsService,_rootScope,_q,_timeout;
    var enterpriseId = "1234";

    beforeEach(module('app'));

    beforeEach(inject(function($injector,$rootScope,$sce,$q,$controller,$location,SettingsService,commonUtilityService,$timeout){
        scope ={};
        scope.vm={};
        $.fn.tab = function(){

        };
        sessionStorage.setItem("enterpriseId","1234");
        spyOn(SettingsService,"getEnterpriseDetails").and.callFake(function(){
            var expectedResponse = {
                "_id":"55efdc6360f02ca22c934e90",
                "type": "Account",
                "accountId": 2,
                "accountType": "ENTERPRISE",
                "accountName": "Uber",
                "parentAccountId": 1,
                "accountSubscriptionType": "ENTERPRISE_PRO",
                "alertVersionSupported": "1.1",
                "company": {
                    "type": "Company",
                    "name": "Uber",
                    "phone": "8888888888",
                    "currency": "USD",
                    "timeZone": "+00:00",
                    "timeZoneInfo": "American Samoa, Jarvis Island, Kingman Reef, Midway Atoll and Palmyra Atoll",
                    "address": {
                        "type": "Address",
                        "address1": "c/o Uber Technologies, Inc",
                        "address2": "182 Howard Street # 8",
                        "state": "California",
                        "city": "San Francisco",
                        "zipCode": "94105",
                        "country": "US"
                    }
                },
                "alertSetting": {
                    "type": "alertSetting",
                    "alertEnabled": false,
                    "alertEmailForAdminEnabled": false,
                    "alertEmailForUserEnabled": false,
                    "alertIntervalInSecs": 3600,
                    "defaultCreditLimitForSims": [{
                        "type": "CreditLimit",
                        "limitType": "SOFT",
                        "limitKB": 10240,
                        "limitDuration": "MONTH",
                        "alertPercentages": [75],
                        "alertUserByMail": true,
                        "alertEnabled": false
                    }]
                },
                "accountDescription": "Uber Enterprise Account"
            }

            var d = $q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });

        spyOn(commonUtilityService,"getCountries").and.callFake(function(){
            var expectedResponse1 =[{"_id":"561771933b27e844933060b9","alpha-2":"AF","name":"Afghanistan","countryId":"561771933b27e844933060b9"},{"_id":"561771933b27e844933060ba","name":"Åland Islands","alpha-2":"AX","countryId":"561771933b27e844933060ba"},{"_id":"561771933b27e844933060bb","name":"Albania","alpha-2":"AL","countryId":"561771933b27e844933060bb"},{"_id":"561771933b27e844933060bc","name":"Algeria","alpha-2":"DZ","countryId":"561771933b27e844933060bc"},{"_id":"561771933b27e844933060bd","alpha-2":"AS","name":"American Samoa","countryId":"561771933b27e844933060bd"},{"_id":"561771933b27e844933060be","name":"Andorra","alpha-2":"AD","countryId":"561771933b27e844933060be"},{"_id":"561771933b27e844933060bf","name":"Angola","alpha-2":"AO","countryId":"561771933b27e844933060bf"},{"_id":"561771933b27e844933060c0","alpha-2":"AI","name":"Anguilla","countryId":"561771933b27e844933060c0"},{"_id":"561771933b27e844933060c1","name":"Antarctica","alpha-2":"AQ","countryId":"561771933b27e844933060c1"},{"_id":"561771933b27e844933060c2","name":"Antigua and Barbuda","alpha-2":"AG","countryId":"561771933b27e844933060c2"},{"_id":"561771933b27e844933060c3","name":"Argentina","alpha-2":"AR","countryId":"561771933b27e844933060c3"},{"_id":"561771933b27e844933060c4","alpha-2":"AM","name":"Armenia","countryId":"561771933b27e844933060c4"},{"_id":"561771933b27e844933060c5","name":"Aruba","alpha-2":"AW","countryId":"561771933b27e844933060c5"}];

            var d1 = $q.defer();
            d1.resolve({data:expectedResponse1});
            return d1.promise;
        });

        EnterpriseDetailsCtrl = $controller("EnterpriseDetailsCtrl",{$scope:scope,$rootScope:$rootScope,$q:$q,$sce:$sce,$location:$location,SettingsService:SettingsService,commonUtilityService:commonUtilityService});

        _CommonUtilityService = commonUtilityService;
        _SettingsService = SettingsService;
        _rootScope = $rootScope;

        _rootScope.$apply();

        _q = $q;
        _timeout =$timeout;
        spyOn(_CommonUtilityService, 'showErrorNotification');
        spyOn(_CommonUtilityService, 'showSuccessNotification');
    }));

    it('details updateEnterpriseDetails',function(){
        var expectedResponse = {};
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        scope.isFormValid = function(){
            return true;
        };
        spyOn(_SettingsService, 'updateEnterpriseDetails').and.returnValue(_mockPromise);
        var enterpriseDetailsForm = {};
        EnterpriseDetailsCtrl.updateEnterpriseDetails(enterpriseDetailsForm);
        expect(_CommonUtilityService.showSuccessNotification).toHaveBeenCalledWith("Enterprise " + EnterpriseDetailsCtrl.account.companyName + " details have been updated successfully.");
    });

    it('details updateEnterpriseDetails (when form validation fails)',function(){
        var expectedResponse = {};
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        scope.isFormValid = function(){
            return false;
        };
        spyOn(_SettingsService, 'updateEnterpriseDetails').and.returnValue(_mockPromise);
        scope.generalObj.isNoChangeError = true;
        var enterpriseDetailsForm = {};
        EnterpriseDetailsCtrl.updateEnterpriseDetails(enterpriseDetailsForm);
        expect(EnterpriseDetailsCtrl.errorMessage).toBe('There is no change in the data to update.');
        expect(scope.generalObj.isNoChangeError).toBe(false);

        EnterpriseDetailsCtrl.updateEnterpriseDetails(enterpriseDetailsForm);

    });


});