describe('UsersCtrl', function() {

    var $httpBackend;
    var baseUrlPath = 'http://localhost:3000/api/v1';
    var userAddReqHandler,userUpdateReqHandler,userDeleteReqHandler,bulkUpdateReqHandler;
    var $commonUtilityService,_UserService,_DataTableConfigService,_GroupsService;
    var accountDetails = {
        "type": "EnterpriseAccountList",
        "enterpriseAccountDetails": [{
            "type": "Account",
            "rootAccountDetails": {
                "type": "Account",
                "accountId": 2,
                "accountType": "ENTERPRISE",
                "accountName": "Uber"
            },
            "parentAccountDetails": [{
                "type": "Account",
                "accountId": 2,
                "accountType": "ENTERPRISE",
                "accountName": "Uber",
                "role": "ENTERPRISE_ADMIN"
            }],
            "selfAccountDetails": {
                "type": "Account",
                "accountId": 22,
                "accountType": "USER"
            }
        }]
    }
    var user = {
        "type": "User",
        "userId": 2,
        "firstName": "uberAdmin",
        "lastName": "uberAdmin",
        "emailId": "uberAdmin@uber.com",
        "location": "Palo-Alto",
        "address": {
            "type": "Address",
            "addressId": 2,
            "address1": "Electronics city",
            "address2": "Hosur Road",
            "state": "Karnataka",
            "city": "Bangalore",
            "zipCode": "560093",
            "country": "IN"
        },
        "userStatus": "ACTIVE",
        "createdOn": "2015-12-02 08:28:16",
        "homePhone": "080-58974536",
        "mobile": "9886333515",
        "accountDetails": accountDetails,
        "totalSimsAssociated": "14",
        "dataUsedInBytes": 0
    }
    var MockUserList = [user,{firstName:'anant',lastName:'kulkarni',emailId:'anant@gigsky.com',userId:"1234", "group":null, "accountDetails": accountDetails },
        {firstName:'yogesh',lastName:'reddy',emailId:'yogesh@gigsky.com',userId:"1235", "group":null, "accountDetails": accountDetails }];
    var group = {accountId:2,name:'Asia'};

    beforeEach(module('app'));

    var usersCtrl;
    var scope = {};
    scope.users = {};
    scope.dataTableObj = {};
    scope.dataTableObj.refreshDataTable = function(drawFromPageOne){

    };


    $.fn.modal = function(){

    };

    beforeEach(inject(function($injector,$controller,$compile, $timeout,UserService,dataTableConfigService,commonUtilityService,GroupsService){
        var groupsResponse = {"type":"AccountList","list":[{"type":"Account","accountId":4,"accountType":"GROUP","accountName":"UberCalifornia","parentAccountId":2,"accountSubscriptionType":"ENTERPRISE_PRO","company":{"type":"Company","name":"Uber","phone":"8888888888","currency":"USD","timeZone":"+00:00","timeZoneInfo":"American Samoa, Jarvis Island, Kingman Reef, Midway Atoll and Palmyra Atoll","address":{"type":"Address","address1":"c/o Uber Technologies, Inc","address2":"182 Howard Street # 8","state":"California","city":"San Francisco","zipCode":"94105","country":"US"}},"accountDescription":"Uber California Sub Account"},{"type":"Account","accountId":3,"accountType":"GROUP","accountName":"UberIndia","parentAccountId":2,"accountSubscriptionType":"ENTERPRISE_PRO","company":{"type":"Company","name":"Uber","phone":"8888888888","currency":"USD","timeZone":"+00:00","timeZoneInfo":"American Samoa, Jarvis Island, Kingman Reef, Midway Atoll and Palmyra Atoll","address":{"type":"Address","address1":"c/o Uber Technologies, Inc","address2":"182 Howard Street # 8","state":"California","city":"San Francisco","zipCode":"94105","country":"US"}},"accountDescription":"Uber India Sub Account"}],"accountType":"ENTERPRISE","accountName":"Uber","accountDescription":"Uber Enterprise Account","parentAccountId":1,"accountSubscriptionType":"ENTERPRISE_PRO","alertVersionSupported":"1.1","accountId":2,"count":2,"totalCount":2};
        var _mockGroupsPromise = {
            success: function(fn) {
                fn(groupsResponse);
                return _mockGroupsPromise;
            },
            error: function(fn) {
                fn(groupsResponse);
                return _mockGroupsPromise;
            }
        };
        spyOn(GroupsService, 'getGroups').and.returnValue(_mockGroupsPromise);

        usersCtrl = $controller("UsersCtrl",{$scope:scope,$compile:$compile, $timeout:$timeout,UserService:UserService,dataTableConfigService:dataTableConfigService,commonUtilityService:commonUtilityService,GroupsService:GroupsService});
        $httpBackend = $injector.get('$httpBackend');


        $httpBackend.when('GET', baseUrlPath+'/users/:id')
            .respond(function(method, url, data, headers, params) {
                return [200, MockUserList[Number(params.id)]];
            });

        userAddReqHandler = $httpBackend.when('POST', baseUrlPath+'/users/');
        userUpdateReqHandler = $httpBackend.when('PUT', baseUrlPath+'/users/'+user.userId);
        bulkUpdateReqHandler = $httpBackend.when('PUT', baseUrlPath+'/users/');
        userDeleteReqHandler  = $httpBackend.when('DELETE', baseUrlPath+'/users/');
        $commonUtilityService = commonUtilityService;
        _UserService = UserService;
        _DataTableConfigService = dataTableConfigService
        _GroupsService = GroupsService;
        commonUtilityService.showErrorNotification = function(msg){

        };
        commonUtilityService.showSuccessNotification = function(msg){

        }
        expect(usersCtrl.groups.length).toBe(2);

        scope.dataTableObj.refreshDataTable = function(){};
    }));

    describe('Add user',function(){

        it('modal should have Add New User title message',function(){
            //Launch add user modal
            scope.userModalObj.updateUserModal = function(user,groups, attr, cbk){
                expect(user).toBeNull();
                expect(attr.header).toBe("Add New User");
                expect(attr.submit).toBe("Add");
                if(!(cbk instanceof Function))
                    expect(false).toBeTruthy();
                scope.onAddCbk = cbk;
            };

            usersCtrl.addUser();
        });

        it('should succeed with proper first name and last name',function(){

            scope.userModalObj.updateUserModal = function(user,groups, attr, cbk){

            };

            usersCtrl.addUser();
            userAddReqHandler.respond(function(method, url, data, headers, params){
                var d  = JSON.parse(data);
                expect(d.type).toBe("User");
                return [200,{}];
            });
            var user = {firstName:'jagdish',lastName:'dande',emailId:'jdande@gigsky.com'};
            scope.onAddCbk(user);
            $commonUtilityService.showSuccessNotification = function(msg){
                expect(msg).toBe("An account has been created for " + user.firstName + " " + " " + user.lastName + " and details have been sent to " + user.emailId + ".");
            };
        });

          it('should show appropriate error message on failure',function(){
            scope.userModalObj.updateUserModal = function(user,groups, attr, cbk){

            };

            usersCtrl.addUser();
            userAddReqHandler.respond(function(method, url, data, headers, params){
                var d  = JSON.parse(data);
                expect(d.type).toBe("User");
                return [500,{errorStr:"Failed to create an user"}];
            });
            scope.onAddCbk({firstName:'jagadish',lastName:'dande'});
            $commonUtilityService.showErrorNotification = function(msg){
                expect(msg).toBe("Failed to create an user");
            };
        });


    });
    describe('Edit user',function(){
        scope.usersList = MockUserList;

        it('modal should have Update User title message',function(){
            //Launch add user modal
            scope.userModalObj.updateUserModal = function(u,userGrp, attr, cbk){
                expect(attr.header).toBe("Update User");
                expect(attr.submit).toBe("Update");
                if(!(cbk instanceof Function))
                    expect(false).toBeTruthy();
                expect(u.firstName).toBe(user.firstName);
                expect(u.lastName).toBe(user.lastName);
                expect(u.emailId).toBe(user.emailId);
                scope.onUpdateCbk = cbk;
            };
            scope.editUser(0);
        });

        it('should succeed with proper first name and last name',function(){
            scope.userModalObj.updateUserModal = function(u,userGrp, attr, cbk){

            };

            scope.editUser(0);
            userUpdateReqHandler.respond(function(method, url, data, headers, params){
                var d  = JSON.parse(data);
                expect(d.type).toBe("User");
                return [200,{}];
            });
            scope.onUpdateCbk(user);
            $commonUtilityService.showSuccessNotification = function(msg){
                expect(msg).toBe("User " + user.firstName + " " + user.lastName + " updated successfully");
            };

            scope.userModalObj.resetUserModal = function( u, attr, cbk ){
              u = {};

            };

        });

        it('should show appropriate error message on failure',function(){
            scope.userModalObj.updateUserModal = function(u,userGrp, attr, cbk){

            };
            scope.editUser(0);
            userUpdateReqHandler.respond(function(method, url, data, headers, params){
                var d  = JSON.parse(data);
                expect(d.type).toBe("User");
                return [500,{errorStr:"Failed to update an user"}];
            });
            scope.onUpdateCbk(user);
            $commonUtilityService.showErrorNotification = function(msg){
                expect(msg).toBe("Failed to update an user");
            };
        });


    });


       describe('Bulk delete user',function(){

        it('modal should have Deleting user title',function(){
            scope.verificationModalObj.addVerificationModelMessage = function(msg){
                expect(msg).toBe('Deleting users cannot be undone! All SIMs associated to the users will be unassigned.');
            };

            scope.verificationModalObj.updateVerificationModel = function(attr, cbk){
                expect(attr.header).toBe("Confirm Deletion");
                expect(attr.submit).toBe("Delete");
                expect(attr.cancel).toBe("Cancel");
                if(!(cbk instanceof Function))
                    expect(false).toBeTruthy();
                scope.onRowDeletion = cbk;
            };

            usersCtrl.verifyDeletion(MockUserList);
        });

        it('should delete single user',function(){
            scope.verificationModalObj.addVerificationModelMessage = function(msg){
            };
            scope.verificationModalObj.updateVerificationModel = function(attr, cbk){
            };

            $commonUtilityService.showSuccessNotification = function(msg){
                expect(msg).toBe('Selected user has been deleted.');
            };
            userDeleteReqHandler.respond(function(method, url, data, headers, params){
                var d  = JSON.parse(data);
                var list = d.list;
                expect(list.length).toEqual(1);
                expect(d.type).toBe("Users");
                expect(list[0].userId).toBe(user.userId);
                return [200];
            });
           usersCtrl.verifyDeletion([user]);
           scope.onRowDeletion([user]);

        });

        it('should delete multiple users',function(){

            scope.verificationModalObj.addVerificationModelMessage = function(msg){
            };
            scope.verificationModalObj.updateVerificationModel = function(attr, cbk){
            };

            $commonUtilityService.showSuccessNotification = function(msg){
                expect(msg).toBe('All selected users have been deleted.');
            };
            userDeleteReqHandler.respond(function(method, url, data, headers, params){
                var d  = JSON.parse(data);
                var list = d.list;
                expect(list.length).toEqual(MockUserList.length);
                expect(d.type).toBe("Users");
                for(var i=0;i<MockUserList.length;i++){
                    expect(list[i].userId).toBe(MockUserList[i].userId);
                }
                return [200];
            });
            usersCtrl.verifyDeletion(MockUserList);
            scope.onRowDeletion(MockUserList);
        });

        it('should not trigger service call on rows selected are zero',function(){
            scope.verificationModalObj.addVerificationModelMessage = function(msg){
            };
            scope.verificationModalObj.updateVerificationModel = function(attr, cbk){
            };
            userDeleteReqHandler.respond(function(method, url, data, headers, params){
                return [500,{errorStr:"No users to delete"}];
            });
            usersCtrl.verifyDeletion([]);
            scope.onRowDeletion([]);
        });

        it('should display error on delete failure',function(){
            scope.verificationModalObj.addVerificationModelMessage = function(msg){
            };
            scope.verificationModalObj.updateVerificationModel = function(attr, cbk){
            };
            $commonUtilityService.showErrorNotification = function(msg){
                expect(msg).toBe('Failed to delete user');
            };
            userDeleteReqHandler.respond(function(method, url, data, headers, params){
               return [500,{errorStr:"Failed to delete user"}];
            });
            usersCtrl.verifyDeletion(MockUserList);
            scope.onRowDeletion(MockUserList);
        });

    });

    describe('Bulk update group',function(){

        it('modal should show ',function(){
            scope.groupModalObj.vfUpdateModal = function(rows,groups,cbk){

                expect(rows.length).toBeGreaterThan(0);
                if(!(cbk instanceof Function))
                    expect(false).toBeTruthy();
                scope.onUpdateGroups = cbk;
            };
            usersCtrl.bulkGroupUpdate(MockUserList);

        });

        it('should update user to new group (by selecting same group which is associated with user)',function(){
            scope.groupModalObj.vfUpdateModal = function(rows,groups,cbk){
                expect(rows.length).toBeGreaterThan(0);
                if(!(cbk instanceof Function))
                    expect(false).toBeTruthy();
                scope.onUpdateGroups = cbk;
            };
            spyOn($commonUtilityService, 'showErrorNotification');
            usersCtrl.bulkGroupUpdate([user]);
            scope.onUpdateGroups({ "group": group},[user]);
            expect($commonUtilityService.showErrorNotification).toHaveBeenCalledWith('Selected User is already associated with the same Group!!');


            usersCtrl.bulkGroupUpdate([user,user]);
            scope.onUpdateGroups({ "group": group},[user,user]);
            expect($commonUtilityService.showErrorNotification).toHaveBeenCalledWith('Selected User is already associated with the same Group!!');
        });

        it('should update user to new group (by selecting same group which is associated with multiple users)',function(){
            scope.groupModalObj.vfUpdateModal = function(rows,groups,cbk){
                expect(rows.length).toBeGreaterThan(0);
                if(!(cbk instanceof Function))
                    expect(false).toBeTruthy();
                scope.onUpdateGroups = cbk;
            };
            spyOn($commonUtilityService, 'showErrorNotification');
            usersCtrl.bulkGroupUpdate([user,user]);
            scope.onUpdateGroups({ "group": group},[user,user]);
            expect($commonUtilityService.showErrorNotification).toHaveBeenCalledWith('All Selected Users are already associated with the same Group!!');
        });

        it('should update single user to new group',function(){
            scope.groupModalObj.vfUpdateModal = function(rows,groups,cbk){
                scope.onUpdateGroups = cbk;
            };
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_UserService, 'updateUsers').and.returnValue(_mockPromise);
            spyOn($commonUtilityService, 'showSuccessNotification');
            var newGroup = {accountId:4,name:'America'};
            usersCtrl.bulkGroupUpdate([user]);
            scope.onUpdateGroups({ "group": newGroup},[user]);
            expect($commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Selected user has been updated.');
        });

        it('should update multiples users to new group',function(){
            scope.groupModalObj.vfUpdateModal = function(rows,groups,cbk){
                scope.onUpdateGroups = cbk;
            };
            var expectedResponse ={}
            var _mockPromise = {
                success: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(expectedResponse);
                    return _mockPromise;
                }
            };
            spyOn(_UserService, 'updateUsers').and.returnValue(_mockPromise);
            spyOn($commonUtilityService, 'showSuccessNotification');
            var newGroup = {accountId:4,name:'America'};
            usersCtrl.bulkGroupUpdate(MockUserList);
            scope.onUpdateGroups({ "group": newGroup},MockUserList);
            expect($commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('All selected users have been updated.');
        });

    });

    describe('Bulk remove group',function(){

        it('modal should show ',function(){
            scope.verificationModalObj.updateVerificationModel = function(attr,cbk){

                expect(attr.header).toBe('Remove Group Association');
                expect(attr.submit).toBe('Remove');
                expect(attr.cancel).toBe('Cancel');
                expect(attr.cancel).toBe('Cancel');


                if(!(cbk instanceof Function))
                    expect(false).toBeTruthy();
                scope.onUpdateGroups = cbk;
            };

            scope.verificationModalObj.addVerificationModelMessage =function(msg){
                expect(msg).toBe('Removing users from a group will not keep any association with the group.');
            };

            usersCtrl.bulkGroupRemove(MockUserList);

        });

       it('should update single user to new group',function(){
           scope.verificationModalObj.updateVerificationModel = function(attr,cbk){
           };

            bulkUpdateReqHandler.respond(function(method, url, data, headers, params){
                var d  = JSON.parse(data);
                var list = d.list;
                expect(list.length).toEqual(1);
                expect(d.type).toBe("Users");
                expect(list[0].userId).toBe(user.userId);
                expect(list[0].group).toBeNull();

                return [200];
            });
            $commonUtilityService.showSuccessNotification = function(msg){
                expect(msg).toBe('Selected user has been updated.');
            };
            usersCtrl.bulkGroupRemove([user]);
            scope.onUpdateGroups({},[user]);
        });

         it('should update multiples users to new group',function(){
             scope.verificationModalObj.updateVerificationModel = function(attr,cbk){
             };
             scope.verificationModalObj.addVerificationModelMessage =function(msg){
             };
            bulkUpdateReqHandler.respond(function(method, url, data, headers, params){
                var d  = JSON.parse(data);
                var list = d.list;
                expect(list.length).toEqual(MockUserList.length);
                expect(d.type).toBe("Users");
                for(var i=0;i<MockUserList.length;i++){
                    expect(list[i].userId).toBe(MockUserList[i].userId);
                    expect(list[i].group).toBeNull();
                }
                return [200];
            });

            $commonUtilityService.showSuccessNotification = function(msg){
                expect(msg).toBe('All selected users have been updated.');
            };

            usersCtrl.bulkGroupRemove(MockUserList);
            scope.onUpdateGroups({group:group},MockUserList);
        });


        it('should show error message on update failure',function(){
            scope.verificationModalObj.updateVerificationModel = function(attr,cbk){
            };
            scope.verificationModalObj.addVerificationModelMessage =function(msg){
            };
            bulkUpdateReqHandler.respond(function(method, url, data, headers, params){
                var d  = JSON.parse(data);
                var list = d.list;
                expect(list.length).toEqual(MockUserList.length);
                expect(d.type).toBe("Users");
                for(var i=0;i<MockUserList.length;i++){
                    expect(list[i].userId).toBe(MockUserList[i].userId);
                    expect(list[i].group).toBeNull();
                }
                return [500,{errorStr:'Failed to update group'}];
            });
            $commonUtilityService.showErrorNotification = function(msg){
                expect(msg).toBe('Failed to update group');
            };
            usersCtrl.bulkGroupRemove(MockUserList);
            scope.onUpdateGroups({group:group},MockUserList);
        });

    });

    it('Should test data-table columnDefs 0',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = user;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[0].render(data,type,row,meta)).toContain(row.userId);
    });

    it('Should test data-table columnDefs 1',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = user;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toContain(row.firstName);
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toContain(row.lastName);
    });

    it('Should test data-table columnDefs 2',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = user;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data,type,row,meta)).toContain(row.emailId);
    });

    it('Should test data-table columnDefs 3',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = user;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toContain(row.accountDetails.enterpriseAccountDetails[0].parentAccountDetails[0].accountName);
    });

    it('Should test data-table columnDefs 4',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = user;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data,type,row,meta)).toContain(row.location);
    });

    it('Should test data-table columnDefs 5',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = user;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row,meta)).toContain(row.totalSimsAssociated);
    });

    it('Should test data-table columnDefs 6',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = user;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[6].render(data,type,row,meta)).toContain(row.dataUsedInBytes ? row.dataUsedInBytes : 0);
    });

    it('Should test fnServerData',function(){
        var sSource, aoData, fnCallback;
        var MockUserList = [user,{firstName:'anant',lastName:'kulkarni',emailId:'anant@gigsky.com',userId:"1234", "group":null, "accountDetails": accountDetails },
            {firstName:'yogesh',lastName:'reddy',emailId:'yogesh@gigsky.com',userId:"1235", "group":null, "accountDetails": accountDetails }];
        var MockResponse = {list:MockUserList,totalCount:3};

        var _mockPromise = {
            success: function(fn) {
                fn(MockResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(MockResponse);
                return _mockPromise;
            }
        };
        spyOn(_UserService, 'getUsers').and.returnValue(_mockPromise);

        scope.dataTableObj.dataTableRef ={};
        scope.dataTableObj.dataTableRef.fnSettings = function(){};

        scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
        expect(scope.usersList.length).toBe(MockResponse.list.length);

        expect(scope.isDisableComponent()).toBe(false);
        expect(scope.isDisableRefreshComponent()).toBe(false);
    });

    it('Should test fnServerData (when list is empty)',function(){
        var sSource, aoData, fnCallback;
        var MockResponse = {list:[],totalCount:0};

        var _mockPromise = {
            success: function(fn) {
                fn(MockResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(MockResponse);
                return _mockPromise;
            }
        };
        spyOn(_UserService, 'getUsers').and.returnValue(_mockPromise);

        scope.dataTableObj.dataTableRef ={};
        scope.dataTableObj.dataTableRef.fnSettings = function(){};

        scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
        expect(scope.usersList.length).toBe(MockResponse.list.length);

        expect(scope.isDisableComponent()).toBe(true);
        expect(scope.isDisableRefreshComponent()).toBe(true);
    });

    it('Should test fnDrawCallback',function(){
        var oSettings = {aoColumns:[]};
        spyOn($commonUtilityService, 'preventDataTableActivePageNoClick');
        scope.dataTableObj.dataTableOptions.fnDrawCallback(oSettings);

        expect($commonUtilityService.preventDataTableActivePageNoClick).toHaveBeenCalled();
    });

    it('Should test checkBoxClicked',function(){
        var event = {};
        scope.dataTableObj.selectRow = jasmine.createSpy('selectRow spy method');
        scope.checkBoxClicked(event);

        expect(scope.dataTableObj.selectRow).toHaveBeenCalled();

    });

    it('Should test setUser',function(){
        scope.graphObj.setUser = jasmine.createSpy('setUser spy method');
        scope.setUser("124","ABCD");

        expect(scope.graphObj.setUser).toHaveBeenCalled();

    });

    it('Should test showSIM',function(){
        var MockUserList = [user,{firstName:'anant',lastName:'kulkarni',emailId:'anant@gigsky.com',userId:"1234", "group":null, "accountDetails": accountDetails },
            {firstName:'yogesh',lastName:'reddy',emailId:'yogesh@gigsky.com',userId:"1235", "group":null, "accountDetails": accountDetails }];
        scope.usersList = MockUserList;

        scope.showSIM(1);

        expect(sessionStorage.getItem("gs-showSimsAssignedToCurrentUser")).toBe('{"userId":"1234","firstName":"anant"}');

    });

    it('Should test refreshDataUsage success',function(){
        var expectedResponse ={sessionId:'1234'};
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_UserService, 'refreshDataUsage').and.returnValue(_mockPromise);

        usersCtrl.refreshDataUsage();
        $commonUtilityService.showSuccessNotification = function(msg){
            expect(msg).toBe("Refresh data usage is successfully triggered");
        };
        expect(scope.isDisableRefreshBtn).toBe(true);
    });

});