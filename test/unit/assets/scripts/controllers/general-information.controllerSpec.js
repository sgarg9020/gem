describe('Account information Controller', function() {

    var _commonUtilityService,GeneralInfoCtrl,scope,_AccountService,_UserService,rootScope;


    beforeEach(module('app'));


    beforeEach(inject(function($injector,$rootScope,$sce,$controller,$location,AccountService,commonUtilityService,SIMService){
        scope ={};
        sessionStorage.setItem('userId','25130');
        var expectedResponse = {"type":"SimList","totalCount":5,"count":5,"list":[{"type":"Sim","simId":"1001100000000000000060","simType":"GIGSKY_SIM","status":"INACTIVE","user":{"type":"User","userId":25130,"firstName":"Alan","lastName":"morte"},"account":{"type":"ENTERPRISE","accountId":2,"accountName":"Uber"},"supplier":"BLUEFISH","nickName":"SIM-0060","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":46080,"limitDuration":"MONTH","alertPercentages":[75],"alertUserByMail":true,"alertEnabled":false}]},"dataUsedInBytes":4269172},{"type":"Sim","simId":"1001100000000000000108","simType":"GIGSKY_SIM","status":"ACTIVE","user":{"type":"User","userId":25130,"firstName":"Alan","lastName":"morte"},"account":{"type":"ENTERPRISE","accountId":2,"accountName":"Uber"},"supplier":"BLUEFISH","nickName":"SIM-0108","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":46080,"limitDuration":"MONTH","alertPercentages":[75],"alertUserByMail":true,"alertEnabled":false}]},"dataUsedInBytes":4248523},{"type":"Sim","simId":"1001100000000000000250","simType":"GIGSKY_SIM","status":"INACTIVE","user":{"type":"User","userId":25130,"firstName":"Alan","lastName":"morte"},"account":{"type":"ENTERPRISE","accountId":2,"accountName":"Uber"},"supplier":"BLUEFISH","nickName":"SIM-0250","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":10240,"limitDuration":"MONTH","alertPercentages":[75],"alertUserByMail":true,"alertEnabled":true}]},"dataUsedInBytes":4245612},{"type":"Sim","simId":"1001100000000000000273","simType":"GIGSKY_SIM","status":"ACTIVE","user":{"type":"User","userId":25130,"firstName":"Alan","lastName":"morte"},"account":{"type":"ENTERPRISE","accountId":2,"accountName":"Uber"},"supplier":"BLUEFISH","nickName":"SIM-0273","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":46080,"limitDuration":"MONTH","alertPercentages":[75],"alertUserByMail":true,"alertEnabled":false}]},"dataUsedInBytes":4224949},{"type":"Sim","simId":"1001100000000000000280","simType":"GIGSKY_SIM","status":"ACTIVE","user":{"type":"User","userId":25130,"firstName":"Alan","lastName":"morte"},"account":{"type":"ENTERPRISE","accountId":2,"accountName":"Uber"},"supplier":"BLUEFISH","nickName":"SIM-0280","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":46080,"limitDuration":"MONTH","alertPercentages":[75],"alertUserByMail":true,"alertEnabled":false}]},"dataUsedInBytes":4207767}]};
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(SIMService, 'getSIMsByUserId').and.returnValue(_mockPromise);



        GeneralInfoCtrl = $controller("GeneralInfoCtrl",{$scope:scope,$rootScope:$rootScope,$sce:$sce,$location:$location,AccountService:AccountService,commonUtilityService:commonUtilityService});
        rootScope = $rootScope;
        rootScope.$apply();

        _commonUtilityService = commonUtilityService;
        _AccountService = AccountService;
        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');


    }));



    it('Shoud test show more sims',function(){

        var expectedResponse = {"type":"User","userId":1,"firstName":"gsAdmin","lastName":"gsAdmin","emailId":"gsAdmin@gigsky.com","location":"Palo-Alto","address":{"type":"Address","addressId":1,"address1":"Beverly Hills","address2":"Inside Den","state":"","city":"SanHouse","zipCode":"560076","country":"Ca"},"userStatus":"ACTIVE","createdOn":"2015-12-02 13:58:16","homePhone":"080-58974536","mobile":"9886333515","accountDetails":{"type":"EnterpriseAccountList","enterpriseAccountDetails":[{"type":"Account","rootAccountDetails":{"type":"Account","accountId":1,"accountType":"ENTERPRISE","accountName":"Gigsky"},"parentAccountDetails":[{"type":"Account","accountId":1,"accountType":"GIGSKY_ROOT","accountName":"Gigsky","role":"GIGSKY_ADMIN"}],"selfAccountDetails":{"type":"Account","accountId":111111,"accountType":"USER"}}]},"totalSimsAssociated":"1","dataUsedInBytes":0};
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_AccountService, 'getAccountDetails').and.returnValue(_mockPromise);

        GeneralInfoCtrl.generalInfo.showMoreSims();
        expect(sessionStorage.getItem("gs-showSimsAssignedToCurrentUser")).toContain("gsAdmin");
        expect(sessionStorage.getItem("gs-showSimsAssignedToCurrentUser")).toContain("25130");

        GeneralInfoCtrl.generalInfo.sims = [];
        GeneralInfoCtrl.generalInfo.showMoreSims();
    });

});