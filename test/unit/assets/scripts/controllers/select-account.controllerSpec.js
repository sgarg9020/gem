describe('select account Controller', function() {

    var   scope,  _rootScope, _q,_authenticationService,_controller,_location,SelectAccountCtrl;


    beforeEach(module('app'));

    beforeEach(inject(function ($injector, $rootScope, $q, $controller, $location, AuthenticationService) {

        sessionStorage.setItem("accountId", "55efdc6360f02ca22c934e90");
        sessionStorage.setItem("userId", "562dcda42668cc28027b5c95");
        $rootScope.isGSAdmin = true;
       _authenticationService =AuthenticationService
        scope = $rootScope.$new();
        _q = $q;
        _controller = $controller;
        _location = $location;
    }));

    it('get user account', function () {


        var response = {"type":"User","userId":"562dcda42668cc28027b5c95","firstName":"Jagadish","lastName":"Dande","emailId":"jdande@gigsky.com","location":"India","accountDetails":{"type":"EnterpriseAccountList","enterpriseAccountDetails":[{"type":"Accounts","selfAccountDetails":{"type":"Account","accountId":"55efdc6360f02ca22c934e90","accountName":"GigskyIndiaPvtLtd","accountType":"USER"}},{"type":"Accounts","selfAccountDetails":{"type":"Account","accountId":"55efdc6360f02ca22c934e90","accountName":"GigskyIndiaPvtLtd","accountType":"USER"}}]}};
        var _mockPromise = {
            success: function (fn) {
                fn(response);
                return _mockPromise;
            },
            error: function (fn) {
                fn(response);
                return _mockPromise;
            }
        };

        spyOn(_authenticationService,"getUserAccount").and.returnValue(_mockPromise);

        SelectAccountCtrl = _controller("SelectAccountCtrl", {
            $scope: scope,
            $location: _location,
            AuthenticationService:_authenticationService
        });
        function isAlertsFeatureEnabled(accountId){};

        var response1 = {"type":"AccountLoginResponse","token":"6e4abe90-8f86-11e5-97b2-6bcd3990a72d","expiryDate":"2015-11-21 18:28:44"};
        var _mockPromise1 = {
            success: function (fn) {
                fn(response1);
                return _mockPromise1;
            },
            error: function (fn) {
                fn(response1);
                return _mockPromise1;
            }
        };

        spyOn(_authenticationService,"enterpriseLogin").and.returnValue(_mockPromise1);


        var account = {
            accountId : '55efdc6360f02ca22c934e90'
        }
        scope.selectAccount(account);
    });
});