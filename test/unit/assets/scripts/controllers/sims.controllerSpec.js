describe('SIMs Controller', function() {

    var _commonUtilityService,_SIMService,_dataTableConfigService,deferred,_simsCommonFactory,_authenticationService;

    var SIMsCtrl,scope,rootScope;
    var group = { "group":{ "_id":"55efdc6360f02ca57c934e92","name":"California","parentGroupId":"55efdc6360f02ca57c934e90","type":"Group","groupId":"55efdc6360f02ca57c934e92"}};
    var account =  {
             "type": "GROUP",
            "accountId": 4,
            "accountName": "UberCalifornia"
    }
    var user = {"_id":"561771933b27e844933060d6","emailId":"anant@gigsky.com","group":{"_id":"55efdc6360f02ca57c934e92","name":"California","parentGroupId":"55efdc6360f02ca57c934e90","type":"Group","groupId":"55efdc6360f02ca57c934e92"},"location":"India","firstName":"Anant","lastName":"Kulkarni","country":null,"type":"User","userId":"561771933b27e844933060d6"};
    var SIM = {
        "type": "Sim",
        "simId": "561771933b27e844933060ec",
        "simType": "GIGSKY_SIM",
        "status": "ACTIVE",
        "user": {
            "type": "User",
            "userId": 2,
            "firstName": "uberAdmin",
            "lastName": "uberAdmin"
        },
        "account": {
            "type": "ENTERPRISE",
            "accountId": 2,
            "accountName": "Uber"
        },
        "supplier": "BLUEFISH",
        "nickName": "SIM-0010",
        "creditLimitList": {
            "type": "CreditLimitList",
            "list": [{
                "type": "CreditLimit",
                "limitType": "SOFT",
                "limitKB": 10240,
                "limitDuration": "MONTH",
                "alertPercentages": [25],
                "alertUserByMail": true,
                "alertEnabled": true
            }]
        },
        "dataUsedInBytes": 0
    };
    var SIMs = {"type":"SimList","totalCount":311,"count":10,"list":[{"type":"Sim","simId":"1001100000000000010","simType":"GIGSKY_SIM","status":"ACTIVE","user":{"type":"User","userId":2,"firstName":"uberAdmin","lastName":"uberAdmin"},"account":{"type":"ENTERPRISE","accountId":2,"accountName":"Uber"},"supplier":"BLUEFISH","nickName":"SIM-0010","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":10240,"limitDuration":"MONTH","alertPercentages":[25],"alertUserByMail":true,"alertEnabled":true}]},"dataUsedInBytes":0},{"type":"Sim","simId":"1001100000000000011","simType":"GIGSKY_SIM","status":"ACTIVE","user":{"type":"User","userId":3,"firstName":"uberUser1","lastName":"uberUser1"},"account":{"type":"ENTERPRISE","accountId":2,"accountName":"Uber"},"supplier":"BLUEFISH","nickName":"SIM-0011","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":10240,"limitDuration":"MONTH","alertPercentages":[25],"alertUserByMail":true,"alertEnabled":true}]},"dataUsedInBytes":0},{"type":"Sim","simId":"1001100000000000012","simType":"GIGSKY_SIM","status":"ACTIVE","user":{"type":"User","userId":4,"firstName":"uberUser2","lastName":"uberUser2"},"account":{"type":"ENTERPRISE","accountId":2,"accountName":"Uber"},"supplier":"BLUEFISH","nickName":"SIM-0012","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":10240,"limitDuration":"MONTH","alertPercentages":[25],"alertUserByMail":true,"alertEnabled":true}]},"dataUsedInBytes":0},{"type":"Sim","simId":"1001100000000000013","simType":"GIGSKY_SIM","status":"ACTIVE","user":{"type":"User","userId":5,"firstName":"uberUser3","lastName":"uberUser3"},"account":{"type":"ENTERPRISE","accountId":2,"accountName":"Uber"},"supplier":"BLUEFISH","nickName":"SIM-0013","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":10240,"limitDuration":"MONTH","alertPercentages":[25],"alertUserByMail":true,"alertEnabled":true}]},"dataUsedInBytes":0},{"type":"Sim","simId":"1001100000000000014","simType":"GIGSKY_SIM","addedToAccountOn":"2015-12-04 18:30:00","status":"ACTIVE","user":{"type":"User","userId":2,"firstName":"uberAdmin","lastName":"uberAdmin"},"account":{"type":"ENTERPRISE","accountId":2,"accountName":"Uber"},"supplier":"BLUEFISH","nickName":"SIM-0014","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":10240,"limitDuration":"MONTH","alertPercentages":[25],"alertUserByMail":true,"alertEnabled":true}]},"dataUsedInBytes":0},{"type":"Sim","simId":"1001100000000000015","simType":"GIGSKY_SIM","addedToAccountOn":"2015-12-04 18:30:00","status":"ACTIVE","user":{"type":"User","userId":2,"firstName":"uberAdmin","lastName":"uberAdmin"},"account":{"type":"ENTERPRISE","accountId":2,"accountName":"Uber"},"supplier":"BLUEFISH","nickName":"SIM-0015","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":10240,"limitDuration":"MONTH","alertPercentages":[25],"alertUserByMail":true,"alertEnabled":true}]},"dataUsedInBytes":0},{"type":"Sim","simId":"1001100000000000016","simType":"GIGSKY_SIM","addedToAccountOn":"2015-12-04 18:30:00","status":"ACTIVE","user":{"type":"User","userId":2,"firstName":"uberAdmin","lastName":"uberAdmin"},"account":{"type":"ENTERPRISE","accountId":2,"accountName":"Uber"},"supplier":"BLUEFISH","nickName":"SIM-0016","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":10240,"limitDuration":"MONTH","alertPercentages":[25],"alertUserByMail":true,"alertEnabled":true}]},"dataUsedInBytes":0},{"type":"Sim","simId":"1001100000000000017","simType":"GIGSKY_SIM","addedToAccountOn":"2015-12-04 18:30:00","status":"ACTIVE","user":{"type":"User","userId":2,"firstName":"uberAdmin","lastName":"uberAdmin"},"account":{"type":"ENTERPRISE","accountId":2,"accountName":"Uber"},"supplier":"BLUEFISH","nickName":"SIM-0017","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":10240,"limitDuration":"MONTH","alertPercentages":[25],"alertUserByMail":true,"alertEnabled":true}]},"dataUsedInBytes":0},{"type":"Sim","simId":"1001100000000000018","simType":"GIGSKY_SIM","addedToAccountOn":"2015-12-04 18:30:00","status":"ACTIVE","user":{"type":"User","userId":2,"firstName":"uberAdmin","lastName":"uberAdmin"},"account":{"type":"ENTERPRISE","accountId":2,"accountName":"Uber"},"supplier":"BLUEFISH","nickName":"SIM-0018","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":10240,"limitDuration":"MONTH","alertPercentages":[25],"alertUserByMail":true,"alertEnabled":true}]},"dataUsedInBytes":0},{"type":"Sim","simId":"1001100000000000019","simType":"GIGSKY_SIM","addedToAccountOn":"2015-12-04 18:30:00","status":"ACTIVE","user":{"type":"User","userId":2,"firstName":"uberAdmin","lastName":"uberAdmin"},"account":{"type":"ENTERPRISE","accountId":2,"accountName":"Uber"},"supplier":"BLUEFISH","nickName":"SIM-0019","creditLimitList":{"type":"CreditLimitList","list":[{"type":"CreditLimit","limitType":"SOFT","limitKB":10240,"limitDuration":"MONTH","alertPercentages":[25],"alertUserByMail":true,"alertEnabled":true}]},"dataUsedInBytes":0}]};

    beforeEach(module('app'));


    beforeEach(inject(function($injector,$controller,$compile,SIMService,dataTableConfigService,commonUtilityService,$rootScope,simsCommonFactory,AuthenticationService){
        scope ={};
        scope.sims={};
        SIMsCtrl = $controller("SIMsCtrl",{$scope:scope,$compile:$compile,dataTableConfigService:dataTableConfigService,SIMService:SIMService,commonUtilityService:commonUtilityService,$rootScope:$rootScope,simsCommonFactory:simsCommonFactory,AuthenticationService:AuthenticationService});

         rootScope = $rootScope;
        _commonUtilityService = commonUtilityService;
        _SIMService = SIMService;
        _dataTableConfigService = dataTableConfigService;
        _simsCommonFactory = simsCommonFactory;
        _authenticationService = AuthenticationService;
        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
        spyOn(_authenticationService, 'isOperationAllowed').and.returnValue(true);

        scope.dataTableObj.refreshDataTable = function(){};
    }));

    it('Should show modal with remove user association as header and process sims for assignee update',function(){
        var expectedResponse ={}
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_SIMService, 'updateSIMs').and.returnValue(_mockPromise);
        scope.verificationModalObj.updateVerificationModel = function(attr,callbackFunc){
            callbackFunc();
            expect(attr.header).toEqual("Remove User Association");
        };
        scope.verificationModalObj.addVerificationModelMessage = function(message){
            expect(message).toEqual("Unassigning a SIM will remove any association from user.");
        };
       SIMsCtrl.unassignSIM(SIMs.list);
       expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('All selected SIMs have been updated.');
    });


    it('Should show modal with remove user association as header and process sims for assignee update(when sims.length is 1)',function(){
        var expectedResponse ={}
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_SIMService, 'updateSIMs').and.returnValue(_mockPromise);
        scope.verificationModalObj.updateVerificationModel = function(attr,callbackFunc){
            callbackFunc();
            expect(attr.header).toEqual("Remove User Association");
        };
        scope.verificationModalObj.addVerificationModelMessage = function(message){
            expect(message).toEqual("Unassigning a SIM will remove any association from user.");
        };
        SIMsCtrl.unassignSIM(SIM);
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Selected SIM has been updated.');
    });

    it('Should show modal with Assign to SIM as header and process sims for assignee update',function(){
        var expectedResponse ={}
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_SIMService, 'updateSIMs').and.returnValue(_mockPromise);
        scope.updateAssigneeModalObj.asneAddModalMessage = function(message){
            expect(message).toEqual('This will assign all selected SIMs to a specific user.', 'alert-plain');
        }
        scope.updateAssigneeModalObj.updateAssigneeModalAttributes = function(attr){
            expect(attr.header).toEqual("Assign SIM to User");
        }
        scope.updateAssigneeModalObj.updateAssigneeModalSubmit = function(rows,callbackFunc){
            callbackFunc(user,rows);
        }
        SIMsCtrl.assignSIM(SIMs.list);
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('All selected SIMs have been updated.');
    });

    it('Should show modal with Deactivating SIM as header and process sims for group update',function(){
        var expectedResponse ={}
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_SIMService, 'updateSIMsStatus').and.returnValue(_mockPromise);
        scope.verificationModalObj.updateVerificationModel = function(attr,callbackFunc){
            callbackFunc();
            expect(attr.header).toEqual("Deactivate SIM");
        };
        scope.verificationModalObj.addVerificationModelMessage = function(message){
            expect(message).toEqual("Deactivating a SIM will block the SIM's access to the network. Deactivation can take a few minutes to complete.");
        };
        SIMsCtrl.bulkDeactivateSIM(SIMs.list);
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('All selected SIMs have been updated.');
    });

   it('Should show modal with Deactivating SIM as header and process sims for group update (when sims.length is 1)',function(){
        var expectedResponse ={}
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_SIMService, 'updateSIMsStatus').and.returnValue(_mockPromise);
        scope.verificationModalObj.updateVerificationModel = function(attr,callbackFunc){
            callbackFunc();
            expect(attr.header).toEqual("Deactivate SIM");
        };
        scope.verificationModalObj.addVerificationModelMessage = function(message){
            expect(message).toBe("Deactivating a SIM will block the SIM's access to the network. Deactivation can take a few minutes to complete.");
        };
        SIMsCtrl.bulkDeactivateSIM([SIM]);
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Selected SIM has been updated.');
    });

    it('Should show modal with activating SIM as header and process sims for group update',function(){
        var expectedResponse ={}
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_SIMService, 'updateSIMsStatus').and.returnValue(_mockPromise);
        scope.verificationModalObj.updateVerificationModel = function(attr,callbackFunc){
            callbackFunc();
            expect(attr.header).toEqual("Activate SIM");
        };
        scope.verificationModalObj.addVerificationModelMessage = function(message){
            expect(message).toEqual("Activating SIM");
        };
        SIMsCtrl.bulkActivateSIM(SIMs.list);
    });

    it('Activating sims which are already in activated state should show error (for multiple sims)',function(){
        SIM.status = "ACTIVE";
        SIMsCtrl.bulkActivateSIM([SIM,SIM]);
        expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith('Selected SIMs are already activated!');
    });

    it('Activating sim which are already in activated state should show error',function(){
        SIM.status = "ACTIVE";
        SIMsCtrl.bulkActivateSIM([SIM]);
        expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith('Selected SIM is already activated!');
    });

    it('Should show modal with Remove Group Association as header and process sims for group update',function(){
        var expectedResponse ={}
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_SIMService, 'updateSIMs').and.returnValue(_mockPromise);
        scope.verificationModalObj.updateVerificationModel = function(attr,callbackFunc){
            callbackFunc();
            expect(attr.header).toEqual("Remove Group Association");
        };
        scope.verificationModalObj.addVerificationModelMessage = function(message){
            expect(message).toEqual("The selected SIMs won't have any association with the group after the removal.");
        };
        SIMsCtrl.bulkGroupRemove(SIMs.list);
    });

    it('Removal from the group should throw an error if sim is associated to enterprise itself for simgle row',function(){
        SIMsCtrl.bulkGroupRemove([SIM]);
        expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith("Selected SIM can't be removed from Enterprise Group!!");
    });

    it('Removal from the group should throw an error if sim is associated to enterprise itself for multiple rows',function(){
        SIMsCtrl.bulkGroupRemove([SIM,SIM,SIM]);
        expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith("Selected SIMs can't be removed from Enterprise Group!!");
    });

    it('Should test bulkGroupUpdate',function(){
        var expectedResponse ={}
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };

        spyOn(_SIMService, 'updateSIMs').and.returnValue(_mockPromise);
        scope.groupModalObj.vfUpdateModal = function(rows,groups,callbackFunc){

            callbackFunc(group,rows);

        };
        SIMsCtrl.bulkGroupUpdate([SIM,SIM]);
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('All selected SIMs have been updated.');
    });

    it('Should update nickname in sim',function(){
        var SIMChangedName = 'SIM 6062 Changed';
        spyOn(_SIMService, 'updateSIMs').and.callFake(function(sim){
            expect(sim.list[0].nickName).toEqual(SIMChangedName)
        });

        scope.sim[SIM.simId] =  SIM;
        scope.updateSIMNickname("nickname-561771933b27e844933060ec",SIMChangedName);
    });

    it('Should update allocation in sim',function(){
        var newAllocation = 500;
        spyOn(_SIMService, 'updateSIMs').and.callFake(function(sim){
            expect(sim.list[0].creditLimitList.list[0].limitKB).toEqual(Number( newAllocation ) * 1024);
        });

        scope.sim[SIM.simId] =  SIM;
        scope.updateSIMAllocation("nickname-561771933b27e844933060ec",newAllocation);
    });

    it('Should show error message',function(){
        var errorMessage = 'error message';
        scope.showErrMsg(errorMessage);
        expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith(errorMessage);
    });
    it('Should show error message',function(){
        var errorMessage = "Woops! SIM nickname failed to update. Please try again.";
        scope.showErrMsg(errorMessage);
        expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith( "Woops! SIM nickname failed to update. Please try again.");
    });

    it('Should test data-table columnDefs 0',function(){
        var data,type,row,meta;
        meta = {
            row:0
        };
        row = SIM;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[0].render(data,type,row,meta)).toContain(row.simId);
    });
    it('Should test data-table columnDefs 1',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = SIM;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row)).toEqual(row.simId);
    });
    it('Should test data-table columnDefs 2',function(){
        var data,type,row,meta;
        meta = {
            row:2
        };
        row = SIM;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data,type,row)).toContain("Active");
    });
    it('Should test data-table columnDefs 3',function(){
        var data,type,row,meta;
        meta = {
            row:3
        };
        row = SIM;
        rootScope.isAlertEnabled = true;
        spyOn(_simsCommonFactory, 'getZoneStatusForSim').and.returnValue("Full Access");
        expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toContain("Full Access");
    });
    it('Should test data-table columnDefs 5',function(){
        var data,type,row,meta;
        meta = {
            row:5
        };
        row = SIM;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row)).toContain(row.nickName);
    });
    it('Should test data-table columnDefs 6',function(){
        var data,type,row,meta;
        meta = {
            row:6
        };
        row = SIM;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[6].render(data,type,row)).toContain(row.user.firstName);
        expect(scope.dataTableObj.dataTableOptions.columnDefs[6].render(data,type,row)).toContain(row.user.lastName);
    });
    it('Should test data-table columnDefs 7',function(){
        var data,type,row,meta;
        meta = {
            row:7
        };
        row = SIM;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[7].render(data,type,row)).toContain(row.account.accountName);
    });


    it('Should rowcallback',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = SIM;
        data = {
            iccId:'561771933b27e844933060ec'
            };

        scope.dataTableObj.showPreviousSelectedRows = jasmine.createSpy('showPreviousSelectedRows spy method');
        scope.dataTableObj.dataTableOptions.rowCallback(row,data);
        expect(scope.dataTableObj.showPreviousSelectedRows).toHaveBeenCalled();
    });
    it('Should verify selectRow is called',function(){
        var event ={

        };
        scope.dataTableObj.selectRow = jasmine.createSpy('selectRow spy method');
        scope.checkBoxClicked(event);
        expect(scope.dataTableObj.selectRow).toHaveBeenCalled();
    });

    it('should test zoneStatus', function(){
        scope.simsList = SIMs.list;
        spyOn(_simsCommonFactory, 'showZoneStatusModal');
        SIMsCtrl.zoneStatus(1);

        expect(_simsCommonFactory.showZoneStatusModal).toHaveBeenCalled();
    })

    it('should test getSimCreditLimit', function(){
       var limit = SIMsCtrl.getSimCreditLimit(SIM);
        expect(limit).toBe(500);
    });

    it('should test getSimCreditLimit when no credit limit is set', function(){
        var sim1 = {
            "type": "Sim",
            "simId": "561771933b27e844933060ec",
            "simType": "GIGSKY_SIM",
            "status": "ACTIVE",
            "user": {
                "type": "User",
                "userId": 2,
                "firstName": "uberAdmin",
                "lastName": "uberAdmin"
            },
            "account": {
                "type": "ENTERPRISE",
                "accountId": 2,
                "accountName": "Uber"
            },
            "supplier": "BLUEFISH",
            "nickName": "SIM-0010",
            "creditLimitList": {
                "type": "CreditLimitList",
                "list": [{}]
            },
            "dataUsedInBytes": 0
        };

        sessionStorage.setItem( "defaultCreditLimitForSims",10240);
        var limit = SIMsCtrl.getSimCreditLimit(sim1);
        expect(limit).toBe(10);
    });

});