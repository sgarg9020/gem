describe('home Controller', function() {

    var _commonUtilityService, _SIMService, _UserService, _requestGraphData, _dataUsageService,_accountService,_settingsService;
    var SIMs = {"type":"SIMs","startIndex":"0","totalCount":100,"filteredCount":100,"count":10,"list":[{"usage":990,"_id":"564ad0e378a316b989e45934","name":"SIM6144","status":"active","iccId":"8910300000000006144","assigned":false,"allocation":350,"user":null,"group":null,"type":"SIM","simId":"564ad0e378a316b989e45934"},{"usage":990,"_id":"564ad0e378a316b989e45926","allocation":350,"name":"SIM6130","status":"active","iccId":"8910300000000006130","assigned":false,"user":null,"group":null,"type":"SIM","simId":"564ad0e378a316b989e45926"},{"usage":969,"_id":"564ad0e378a316b989e4592c","allocation":350,"name":"SIM6136","status":"active","iccId":"8910300000000006136","assigned":false,"user":null,"group":null,"type":"SIM","simId":"564ad0e378a316b989e4592c"},{"usage":969,"_id":"564ad0e378a316b989e45942","name":"SIM6158","status":"active","iccId":"8910300000000006158","assigned":false,"allocation":350,"user":null,"group":null,"type":"SIM","simId":"564ad0e378a316b989e45942"},{"usage":966,"_id":"564ad0e378a316b989e4591e","name":"SIM6122","status":"active","iccId":"8910300000000006122","assigned":false,"allocation":350,"user":null,"group":null,"type":"SIM","simId":"564ad0e378a316b989e4591e"},{"usage":941,"_id":"564ad0e378a316b989e4593e","name":"SIM6154","status":"active","iccId":"8910300000000006154","assigned":false,"allocation":350,"user":null,"group":null,"type":"SIM","simId":"564ad0e378a316b989e4593e"},{"usage":925,"_id":"564ad0e378a316b989e458f4","allocation":350,"name":"SIM6080","status":"active","iccId":"8910300000000006080","assigned":false,"user":{"_id":"564ad0e378a316b989e458cf","firstName":"Sowjanya","lastName":"Maragal","type":"User","userId":"564ad0e378a316b989e458cf"},"group":{"_id":"55efdc6360f02ca57c934e92","name":"California","parentGroupId":"55efdc6360f02ca57c934e90","type":"Group","groupId":"55efdc6360f02ca57c934e92"},"type":"SIM","simId":"564ad0e378a316b989e458f4"},{"usage":917,"_id":"564ad0e378a316b989e458fa","name":"SIM6086","status":"active","iccId":"8910300000000006086","assigned":false,"allocation":350,"user":{"_id":"564ad0e378a316b989e458d5","firstName":"Kore","lastName":"Guru","type":"User","userId":"564ad0e378a316b989e458d5"},"group":{"_id":"55efdc6360f02ca57c934e92","name":"California","parentGroupId":"55efdc6360f02ca57c934e90","type":"Group","groupId":"55efdc6360f02ca57c934e92"},"type":"SIM","simId":"564ad0e378a316b989e458fa"},{"usage":911,"_id":"564ad0e378a316b989e45906","iccId":"8910300000000006098","assigned":false,"allocation":350,"name":"SIM6098","status":"active","user":null,"group":null,"type":"SIM","simId":"564ad0e378a316b989e45906"},{"usage":883,"_id":"564ad0e378a316b989e45902","iccId":"8910300000000006094","assigned":false,"allocation":350,"name":"SIM6094","status":"active","user":{"_id":"564ad0e378a316b989e458dd","firstName":"Vijaya lakshmi","lastName":"Ghatikar","type":"User","userId":"564ad0e378a316b989e458dd"},"group":{"_id":"55efdc6360f02ca57c934e92","name":"California","parentGroupId":"55efdc6360f02ca57c934e90","type":"Group","groupId":"55efdc6360f02ca57c934e92"},"type":"SIM","simId":"564ad0e378a316b989e45902"}]};
    var users = {"type":"Users","startIndex":"0","totalCount":37,"filteredCount":37,"count":10,"list":[{"usage":925,"sim":1,"_id":"564ad0e378a316b989e458cf","firstName":"Sowjanya","lastName":"Maragal","emailId":"smaragal@gigsky.com","group":{"_id":"55efdc6360f02ca57c934e92","name":"California","parentGroupId":"55efdc6360f02ca57c934e90","type":"Group","groupId":"55efdc6360f02ca57c934e92"},"location":"India","country":null,"type":"User","userId":"564ad0e378a316b989e458cf"},{"usage":917,"sim":1,"_id":"564ad0e378a316b989e458d5","firstName":"Kore","lastName":"Guru","emailId":"kore@gigsky.com","group":{"_id":"55efdc6360f02ca57c934e92","name":"California","parentGroupId":"55efdc6360f02ca57c934e90","type":"Group","groupId":"55efdc6360f02ca57c934e92"},"location":"India","country":null,"type":"User","userId":"564ad0e378a316b989e458d5"},{"usage":883,"sim":1,"_id":"564ad0e378a316b989e458dd","firstName":"Vijaya lakshmi","lastName":"Ghatikar","emailId":"vijayalaxmi@gigsky.com","group":{"_id":"55efdc6360f02ca57c934e92","name":"California","parentGroupId":"55efdc6360f02ca57c934e90","type":"Group","groupId":"55efdc6360f02ca57c934e92"},"location":"India","country":null,"type":"User","userId":"564ad0e378a316b989e458dd"},{"usage":855,"sim":1,"_id":"564ad0e378a316b989e458c7","location":"Palo Alto","firstName":"Sanket","lastName":"Pingle","emailId":"spingle@gigsky.com","group":null,"country":null,"type":"User","userId":"564ad0e378a316b989e458c7"},{"usage":815,"sim":1,"_id":"564ad0e378a316b989e458cd","lastName":"Reddy","emailId":"yreddy@gigsky.com","group":{"_id":"55efdc6360f02ca57c934e92","name":"California","parentGroupId":"55efdc6360f02ca57c934e90","type":"Group","groupId":"55efdc6360f02ca57c934e92"},"location":"India","firstName":"Yogesh","country":null,"type":"User","userId":"564ad0e378a316b989e458cd"},{"usage":812,"sim":1,"_id":"564ad0e378a316b989e458bf","location":"Palo Alto","firstName":"Andrew","lastName":"Fryett","emailId":"andrew@gigsky.com","group":{"_id":"55efdc6360f02ca57c934e92","name":"California","parentGroupId":"55efdc6360f02ca57c934e90","type":"Group","groupId":"55efdc6360f02ca57c934e92"},"country":null,"type":"User","userId":"564ad0e378a316b989e458bf"},{"usage":805,"sim":1,"_id":"564ad0e378a316b989e458df","firstName":"Iranna","lastName":"Hosakote","emailId":"iranna@gigsky.com","group":{"_id":"55efdc6360f02ca57c934e92","name":"California","parentGroupId":"55efdc6360f02ca57c934e90","type":"Group","groupId":"55efdc6360f02ca57c934e92"},"location":"India","country":null,"type":"User","userId":"564ad0e378a316b989e458df"},{"usage":799,"sim":1,"_id":"564ad0e378a316b989e458bd","firstName":"Curtis","lastName":"Morte","emailId":"cmorte@gigsky.com","group":{"_id":"55efdc6360f02ca57c934e92","name":"California","parentGroupId":"55efdc6360f02ca57c934e90","type":"Group","groupId":"55efdc6360f02ca57c934e92"},"location":"Palo Alto","enterprise":"55efdc6360f02ca22c934e90","country":null,"type":"User","userId":"564ad0e378a316b989e458bd"},{"usage":783,"sim":1,"_id":"564ad0e378a316b989e458cb","lastName":"Erberk","emailId":"eerberk@gigsky.com","group":{"_id":"55efdc6360f02ca57c934e92","name":"California","parentGroupId":"55efdc6360f02ca57c934e90","type":"Group","groupId":"55efdc6360f02ca57c934e92"},"location":"Palo Alto","firstName":"Ercan","country":null,"type":"User","userId":"564ad0e378a316b989e458cb"},{"usage":775,"sim":1,"_id":"564ad0e378a316b989e458c9","lastName":"Rishy-Maharaj","emailId":"rrishy@gigsky.com","group":{"_id":"55efdc6360f02ca57c934e90","name":"US West","type":"Group","groupId":"55efdc6360f02ca57c934e90"},"location":"Palo Alto","firstName":"Ravi","country":null,"type":"User","userId":"564ad0e378a316b989e458c9"}]};
    var currentDayData = {
        "type": "AccountDataUsageList",
        "count": 8,
        "totalCount": 8,
        "groupByPeriod": "MONTH",
        "totalDataUsedInBytes": 67646593,
        "totalDataUsageCost": 33.9392,
        "list": [{
            "type": "DataUsageList",
            "totalDataUsedInBytes": 6994620,
            "totalDataUsageCost": 0.6671,
            "zoneId": 2,
            "zoneName": "2-Zone2",
            "zoneNickName": "2-Zone2345",
            "list": [{
                "type": "DataUsage",
                "dataUsedInBytes": 6994620,
                "fromDate": "2016-07-01 00:00:00",
                "toDate": "2016-07-05 00:00:00",
                "dataUsageCost": 0.6671
            }]
        }, {
            "type": "DataUsageList",
            "totalDataUsedInBytes": 11756624,
            "totalDataUsageCost": 0.1121,
            "zoneId": 1,
            "zoneName": "2-Zone1",
            "zoneNickName": "zone1NickName",
            "list": [{
                "type": "DataUsage",
                "dataUsedInBytes": 11756624,
                "fromDate": "2016-07-01 00:00:00",
                "toDate": "2016-07-05 00:00:00",
                "dataUsageCost": 0.1121
            }]
        }, {
            "type": "DataUsageList",
            "totalDataUsedInBytes": 16283685,
            "totalDataUsageCost": 11.6470,
            "zoneId": 5,
            "zoneName": "2-Zone5",
            "zoneNickName": "2-Zone5",
            "list": [{
                "type": "DataUsage",
                "dataUsedInBytes": 16283685,
                "fromDate": "2016-07-01 00:00:00",
                "toDate": "2016-07-05 00:00:00",
                "dataUsageCost": 11.6470
            }]
        }, {
            "type": "DataUsageList",
            "totalDataUsedInBytes": 0,
            "totalDataUsageCost": 0,
            "zoneId": 4,
            "zoneName": "2-Zone4",
            "zoneNickName": "Myzone123",
            "list": [{
                "type": "DataUsage",
                "dataUsedInBytes": 0,
                "fromDate": "2016-07-01 00:00:00",
                "toDate": "2016-07-05 00:00:00",
                "dataUsageCost": 0
            }]
        }]
    };
    var tillDayData = {"type": "AccountDataUsageList", "count": 4, "totalCount": 4, "groupByPeriod": "MONTH", "totalDataUsedInBytes": 67778646593, "totalDataUsageCost": 133.9392, "list": [{"type": "DataUsageList", "totalDataUsedInBytes": 6994620, "totalDataUsageCost": 0.6671, "zoneId": 2, "zoneName": "2-Zone2", "zoneNickName": "2-Zone2345", "list": [{"type": "DataUsage", "dataUsedInBytes": 67646593, "fromDate": "2016-07-01 00:00:00", "toDate": "2016-07-05 00:00:00", "dataUsageCost": 100.6671}]}, {"type": "DataUsageList", "totalDataUsedInBytes": 11756624, "totalDataUsageCost": 0.1121, "zoneId": 1, "zoneName": "2-Zone1", "zoneNickName": "zone1NickName", "list": [{"type": "DataUsage", "dataUsedInBytes": 11756624, "fromDate": "2016-07-01 00:00:00", "toDate": "2016-07-05 00:00:00", "dataUsageCost": 0.1121}]}, {"type": "DataUsageList", "totalDataUsedInBytes": 16283685, "totalDataUsageCost": 11.6470, "zoneId": 5, "zoneName": "2-Zone5", "zoneNickName": "2-Zone5", "list": [{"type": "DataUsage", "dataUsedInBytes": 16283685, "fromDate": "2016-07-01 00:00:00", "toDate": "2016-07-05 00:00:00", "dataUsageCost": 11.6470}]}, {"type": "DataUsageList", "totalDataUsedInBytes": 0, "totalDataUsageCost": 0, "zoneId": 4, "zoneName": "2-Zone4", "zoneNickName": "Myzone123", "list": [{"type": "DataUsage", "dataUsedInBytes": 0, "fromDate": "2016-07-01 00:00:00", "toDate": "2016-07-05 00:00:00", "dataUsageCost": 0}]}]};
    var zonesList = [{"name":"zone 3","dataUsage":3519},{"name":"zone 5","dataUsage":9045},{"name":"zone 4","dataUsage":6939},{"name":"zone 2","dataUsage":4029},{"name":"zone 1","dataUsage":6983}]
    var billingCycle = {billingStart: "2016-08-01 00:00:00", billingEnd: "2016-08-31 23:59:59"};
    var error = {errorStr:"error"}

    var homeCtrl, scope,_controller,_rootScope;

    beforeEach(module('app'));


    beforeEach(inject(function ($injector,$rootScope,$controller, SIMService, UserService, requestGraphData,AccountService, commonUtilityService, dataUsageService,SettingsService) {

        scope = $rootScope.$new();
        _commonUtilityService = commonUtilityService;
        _SIMService = SIMService;
        _UserService = UserService;
        _accountService = AccountService;
        _requestGraphData = requestGraphData;
        _dataUsageService = dataUsageService;
        _controller = $controller;
        _rootScope = $rootScope;
        _settingsService = SettingsService;
        var _mockPromise = {
            success: function (fn) {
                fn(users);
                return _mockPromise;
            },
            error: function (fn) {
                fn(error);
                return _mockPromise;
            }
        };
        var _mockPromise1 = {
            success: function (fn) {
                fn(SIMs);
                return _mockPromise;
            },
            error: function (fn) {
                fn(error);
                return _mockPromise;
            }
        };
        var _mockPromise2 = {
            success: function (fn) {
                fn(currentDayData);
                return _mockPromise2;
            },
            error: function (fn) {
                return _mockPromise2;
            }
        };

        var _mockPromise3 = {
            success: function (fn) {
                fn(billingCycle);
                return _mockPromise3;
            },
            error: function (fn) {
                return _mockPromise3;
            }
        };
        sessionStorage.setItem("timezone","+05:30");

        spyOn(_UserService, 'getUsers').and.returnValue(_mockPromise);
        spyOn(_SIMService, 'getSIMs').and.returnValue(_mockPromise1);
        spyOn(_accountService, 'getBillingCycle').and.returnValue(_mockPromise3);
        spyOn(_requestGraphData,'getMonthlyDataUsageOfZones').and.returnValue(_mockPromise2);
        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');

    }));


    it('Should verify home controller init', function () {

        homeCtrl = _controller("HomeCtrl", {
            $scope: scope,
            SIMService:_SIMService,
            UserService:_UserService,
            requestGraphData:_requestGraphData,
            commonUtilityService:_commonUtilityService,
            dataUsageService:_dataUsageService
        });

        expect(_UserService.getUsers).toHaveBeenCalled();
        expect(_SIMService.getSIMs).toHaveBeenCalled();
      //  expect(_requestGraphData.getMonthlyDataUsageOfZones).toHaveBeenCalled();

    });

    it('Should verify home controller init', function () {
        homeCtrl = _controller("HomeCtrl", {
            $scope: scope,
            SIMService:_SIMService,
            UserService:_UserService,
            requestGraphData:_requestGraphData,
            commonUtilityService:_commonUtilityService,
            dataUsageService:_dataUsageService
        });
        var today = new Date();
        var currentDateStart = today.getFullYear() + '-' + ("0" +(today.getMonth() + 1)).slice(-2)+ '-' + ("0" +today.getDate()).slice(-2) + ' 00:00:00';
        var currentDateEnd = today.getFullYear() + '-' + ("0" +(today.getMonth() + 1)).slice(-2)+ '-' + ("0" +today.getDate()).slice(-2) + ' 23:59:59';
        var response ={
            data : currentDayData,
            startDate : currentDateStart,
            endDate : currentDateEnd
        }
        _rootScope.$emit('onGetMonthlyDataUsagesOfZones',response);
        expect(_UserService.getUsers).toHaveBeenCalled();
        expect(_SIMService.getSIMs).toHaveBeenCalled();
      //  expect(_requestGraphData.getMonthlyDataUsageOfZones).toHaveBeenCalled();

    });
    it('Should verify home controller init', function () {
        sessionStorage.setItem("accountSubscriptionType",'ENTERPRISE_LITE');
        homeCtrl = _controller("HomeCtrl", {
            $scope: scope,
            SIMService:_SIMService,
            UserService:_UserService,
            requestGraphData:_requestGraphData,
            commonUtilityService:_commonUtilityService,
            dataUsageService:_dataUsageService
        });
        var today = new Date();
        var currentDateStart = today.getFullYear() + '-' + ("0" +(today.getMonth() + 1)).slice(-2)+ '-' + ("0" +today.getDate()).slice(-2) + ' 00:00:00';
        var currentDateEnd = today.getFullYear() + '-' + ("0" +(today.getMonth() + 1)).slice(-2)+ '-' + ("0" +today.getDate()).slice(-2) + ' 23:59:59';
        expect(homeCtrl.usageFilter({dataUsedInBytes:5})).toEqual(true);

        expect(_UserService.getUsers).toHaveBeenCalled();
        expect(_SIMService.getSIMs).toHaveBeenCalled();
       // expect(_requestGraphData.getMonthlyDataUsageOfZones).toHaveBeenCalled();

    });

    it('should test enterprise spend, totalCurrentSpend, todaysSpend dailyAverageSpend', function () {
        var plans = {"type":"AccountZoneList","totalCount":8,"count":8,"list":[{"type":"ZoneInfo","zoneId":1,"name":"2-Zone1","nickName":"2-Zone1","pricePerMB":0.01,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"},{"type":"country","name":"United Kingdom","code":"GB"},{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":310,"mnc":170},{"type":"mccmnc","mcc":310,"mnc":260},{"type":"mccmnc","mcc":404,"mnc":31},{"type":"mccmnc","mcc":405,"mnc":46},{"type":"mccmnc","mcc":234,"mnc":15}]},{"type":"ZoneInfo","zoneId":2,"name":"2-Zone2","nickName":"2-Zone2","pricePerMB":0.1,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Singapore","code":"SG"},{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":525,"mnc":5},{"type":"mccmnc","mcc":310,"mnc":250},{"type":"mccmnc","mcc":310,"mnc":240}]},{"type":"ZoneInfo","zoneId":3,"name":"2-Zone3","nickName":"2-Zone3","pricePerMB":0.45,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":310,"mnc":380},{"type":"mccmnc","mcc":310,"mnc":680},{"type":"mccmnc","mcc":310,"mnc":410}]},{"type":"ZoneInfo","zoneId":4,"name":"2-Zone4","nickName":"2-Zone4","pricePerMB":1.01,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"}],"mccmncList":[{"type":"mccmnc","mcc":405,"mnc":809},{"type":"mccmnc","mcc":405,"mnc":808},{"type":"mccmnc","mcc":405,"mnc":804},{"type":"mccmnc","mcc":405,"mnc":807},{"type":"mccmnc","mcc":405,"mnc":805},{"type":"mccmnc","mcc":404,"mnc":25},{"type":"mccmnc","mcc":404,"mnc":28}]},{"type":"ZoneInfo","zoneId":5,"name":"2-Zone5","nickName":"2-Zone5","pricePerMB":0.75,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"}],"mccmncList":[{"type":"mccmnc","mcc":404,"mnc":37},{"type":"mccmnc","mcc":404,"mnc":91},{"type":"mccmnc","mcc":404,"mnc":42},{"type":"mccmnc","mcc":404,"mnc":13},{"type":"mccmnc","mcc":404,"mnc":17},{"type":"mccmnc","mcc":404,"mnc":41},{"type":"mccmnc","mcc":404,"mnc":56}]},{"type":"ZoneInfo","zoneId":6,"name":"2-Zone6","nickName":"2-Zone6","pricePerMB":0.9,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Thailand","code":"TH"}],"mccmncList":[{"type":"mccmnc","mcc":520,"mnc":1}]},{"type":"ZoneInfo","zoneId":7,"name":"2-Zone7","nickName":"2-Zone7","pricePerMB":0.09,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Malaysia","code":"MY"},{"type":"country","name":"South Africa","code":"ZA"}],"mccmncList":[{"type":"mccmnc","mcc":502,"mnc":19},{"type":"mccmnc","mcc":655,"mnc":7}]},{"type":"ZoneInfo","zoneId":8,"name":"2-Zone8","nickName":"2-Zone8","pricePerMB":0.04,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Colombia","code":"CO"}],"mccmncList":[{"type":"mccmnc","mcc":732,"mnc":101}]}]};
        var _mockPromise3 = {
            success: function (fn) {
                fn(plans);
                return _mockPromise3;
            },
            error: function (fn) {
                return _mockPromise3;
            }
        };
        spyOn(_settingsService, 'getPlanDetails').and.returnValue(_mockPromise3);
        sessionStorage.setItem('gs-enterpriseCurrency',"USD");

        var response ={
            data : tillDayData,
            startDate : "2016-08-01 00:00:00",
            endDate : "2016-08-31 23:59:59"
        }


        homeCtrl = _controller("HomeCtrl", {
            $scope: scope,
            SIMService:_SIMService,
            UserService:_UserService,
            AccountService: _accountService,
            requestGraphData:_requestGraphData,
            commonUtilityService:_commonUtilityService,
            dataUsageService:_dataUsageService,
            SettingsService: _settingsService
        });

        _rootScope.$emit('onGetMonthlyDataUsagesOfZones',response);
        expect(homeCtrl.todaysSpend).toBe('33.94');
        expect(homeCtrl.dailyAverageSpend).toBe('3.68');
        expect(homeCtrl.totalCurrentSpend).toBe('133.94');
        expect(homeCtrl.totalEstimatedSpend).toBe('15.49');
    });

    it('should handle when the getPlanDetails service fails ', function () {
        var plans = {"type":"AccountZoneList","totalCount":8,"count":8,"list":[{"type":"ZoneInfo","zoneId":1,"name":"2-Zone1","nickName":"2-Zone1","pricePerMB":0.01,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"},{"type":"country","name":"United Kingdom","code":"GB"},{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":310,"mnc":170},{"type":"mccmnc","mcc":310,"mnc":260},{"type":"mccmnc","mcc":404,"mnc":31},{"type":"mccmnc","mcc":405,"mnc":46},{"type":"mccmnc","mcc":234,"mnc":15}]},{"type":"ZoneInfo","zoneId":2,"name":"2-Zone2","nickName":"2-Zone2","pricePerMB":0.1,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Singapore","code":"SG"},{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":525,"mnc":5},{"type":"mccmnc","mcc":310,"mnc":250},{"type":"mccmnc","mcc":310,"mnc":240}]},{"type":"ZoneInfo","zoneId":3,"name":"2-Zone3","nickName":"2-Zone3","pricePerMB":0.45,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":310,"mnc":380},{"type":"mccmnc","mcc":310,"mnc":680},{"type":"mccmnc","mcc":310,"mnc":410}]},{"type":"ZoneInfo","zoneId":4,"name":"2-Zone4","nickName":"2-Zone4","pricePerMB":1.01,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"}],"mccmncList":[{"type":"mccmnc","mcc":405,"mnc":809},{"type":"mccmnc","mcc":405,"mnc":808},{"type":"mccmnc","mcc":405,"mnc":804},{"type":"mccmnc","mcc":405,"mnc":807},{"type":"mccmnc","mcc":405,"mnc":805},{"type":"mccmnc","mcc":404,"mnc":25},{"type":"mccmnc","mcc":404,"mnc":28}]},{"type":"ZoneInfo","zoneId":5,"name":"2-Zone5","nickName":"2-Zone5","pricePerMB":0.75,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"}],"mccmncList":[{"type":"mccmnc","mcc":404,"mnc":37},{"type":"mccmnc","mcc":404,"mnc":91},{"type":"mccmnc","mcc":404,"mnc":42},{"type":"mccmnc","mcc":404,"mnc":13},{"type":"mccmnc","mcc":404,"mnc":17},{"type":"mccmnc","mcc":404,"mnc":41},{"type":"mccmnc","mcc":404,"mnc":56}]},{"type":"ZoneInfo","zoneId":6,"name":"2-Zone6","nickName":"2-Zone6","pricePerMB":0.9,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Thailand","code":"TH"}],"mccmncList":[{"type":"mccmnc","mcc":520,"mnc":1}]},{"type":"ZoneInfo","zoneId":7,"name":"2-Zone7","nickName":"2-Zone7","pricePerMB":0.09,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Malaysia","code":"MY"},{"type":"country","name":"South Africa","code":"ZA"}],"mccmncList":[{"type":"mccmnc","mcc":502,"mnc":19},{"type":"mccmnc","mcc":655,"mnc":7}]},{"type":"ZoneInfo","zoneId":8,"name":"2-Zone8","nickName":"2-Zone8","pricePerMB":0.04,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Colombia","code":"CO"}],"mccmncList":[{"type":"mccmnc","mcc":732,"mnc":101}]}]};
        var _mockPromise3 = {
            success: function (fn) {
                return _mockPromise3;
            },
            error: function (fn) {
                fn(error);
                return _mockPromise3;
            }
        };
        spyOn(_settingsService, 'getPlanDetails').and.returnValue(_mockPromise3);
        sessionStorage.setItem('gs-enterpriseCurrency',"USD");

        var response ={
            data : tillDayData,
            startDate : "2016-08-01 00:00:00",
            endDate : "2016-08-31 23:59:59"
        }


        homeCtrl = _controller("HomeCtrl", {
            $scope: scope,
            SIMService:_SIMService,
            UserService:_UserService,
            AccountService: _accountService,
            requestGraphData:_requestGraphData,
            commonUtilityService:_commonUtilityService,
            dataUsageService:_dataUsageService,
            SettingsService: _settingsService
        });

        _rootScope.$emit('onGetMonthlyDataUsagesOfZones',response);

        expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith(error.errorStr);
    });

    it('should test formatDataCost function', function () {
        homeCtrl = _controller("HomeCtrl", {
            $scope: scope,
            SIMService:_SIMService,
            UserService:_UserService,
            AccountService: _accountService,
            requestGraphData:_requestGraphData,
            commonUtilityService:_commonUtilityService,
            dataUsageService:_dataUsageService,
            SettingsService: _settingsService
        });
        sessionStorage.setItem('gs-enterpriseCurrency',"USD");
        var cost = homeCtrl.formatDataCost(32.343434);
        expect(cost).toBe('32.34');

        homeCtrl.currency = 'JPY';
        var cost1 = homeCtrl.formatDataCost(32.343434);
        expect(cost1).toBe('32');

        homeCtrl.currency = 'EUR';
        var cost2 = homeCtrl.formatDataCost(32.398434);
        expect(cost2).toBe('32.40');

        homeCtrl.currency = 'GBP';
        var cost3 = homeCtrl.formatDataCost(0);
        expect(cost3).toBe(0);
    });



    it('should test enterprise spend, totalCurrentSpend, todaysSpend dailyAverageSpend', function () {
        var plans = {"type":"AccountZoneList","totalCount":8,"count":8,"list":[{"type":"ZoneInfo","zoneId":1,"name":"2-Zone1","nickName":"2-Zone1","pricePerMB":0.01,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"},{"type":"country","name":"United Kingdom","code":"GB"},{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":310,"mnc":170},{"type":"mccmnc","mcc":310,"mnc":260},{"type":"mccmnc","mcc":404,"mnc":31},{"type":"mccmnc","mcc":405,"mnc":46},{"type":"mccmnc","mcc":234,"mnc":15}]},{"type":"ZoneInfo","zoneId":2,"name":"2-Zone2","nickName":"2-Zone2","pricePerMB":0.1,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Singapore","code":"SG"},{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":525,"mnc":5},{"type":"mccmnc","mcc":310,"mnc":250},{"type":"mccmnc","mcc":310,"mnc":240}]},{"type":"ZoneInfo","zoneId":3,"name":"2-Zone3","nickName":"2-Zone3","pricePerMB":0.45,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":310,"mnc":380},{"type":"mccmnc","mcc":310,"mnc":680},{"type":"mccmnc","mcc":310,"mnc":410}]},{"type":"ZoneInfo","zoneId":4,"name":"2-Zone4","nickName":"2-Zone4","pricePerMB":1.01,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"}],"mccmncList":[{"type":"mccmnc","mcc":405,"mnc":809},{"type":"mccmnc","mcc":405,"mnc":808},{"type":"mccmnc","mcc":405,"mnc":804},{"type":"mccmnc","mcc":405,"mnc":807},{"type":"mccmnc","mcc":405,"mnc":805},{"type":"mccmnc","mcc":404,"mnc":25},{"type":"mccmnc","mcc":404,"mnc":28}]},{"type":"ZoneInfo","zoneId":5,"name":"2-Zone5","nickName":"2-Zone5","pricePerMB":0.75,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"}],"mccmncList":[{"type":"mccmnc","mcc":404,"mnc":37},{"type":"mccmnc","mcc":404,"mnc":91},{"type":"mccmnc","mcc":404,"mnc":42},{"type":"mccmnc","mcc":404,"mnc":13},{"type":"mccmnc","mcc":404,"mnc":17},{"type":"mccmnc","mcc":404,"mnc":41},{"type":"mccmnc","mcc":404,"mnc":56}]},{"type":"ZoneInfo","zoneId":6,"name":"2-Zone6","nickName":"2-Zone6","pricePerMB":0.9,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Thailand","code":"TH"}],"mccmncList":[{"type":"mccmnc","mcc":520,"mnc":1}]},{"type":"ZoneInfo","zoneId":7,"name":"2-Zone7","nickName":"2-Zone7","pricePerMB":0.09,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Malaysia","code":"MY"},{"type":"country","name":"South Africa","code":"ZA"}],"mccmncList":[{"type":"mccmnc","mcc":502,"mnc":19},{"type":"mccmnc","mcc":655,"mnc":7}]},{"type":"ZoneInfo","zoneId":8,"name":"2-Zone8","nickName":"2-Zone8","pricePerMB":0.04,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Colombia","code":"CO"}],"mccmncList":[{"type":"mccmnc","mcc":732,"mnc":101}]}]};
        var _mockPromise3 = {
            success: function (fn) {
                fn(plans);
                return _mockPromise3;
            },
            error: function (fn) {
                return _mockPromise3;
            }
        };
        spyOn(_settingsService, 'getPlanDetails').and.returnValue(_mockPromise3);
        sessionStorage.setItem('gs-enterpriseCurrency',"USD");

        var response ={
            data : tillDayData,
            startDate : "2016-08-01 00:00:00",
            endDate : "2016-08-31 23:59:59"
        }


        homeCtrl = _controller("HomeCtrl", {
            $scope: scope,
            SIMService:_SIMService,
            UserService:_UserService,
            AccountService: _accountService,
            requestGraphData:_requestGraphData,
            commonUtilityService:_commonUtilityService,
            dataUsageService:_dataUsageService,
            SettingsService: _settingsService
        });

        _rootScope.$emit('onGetMonthlyDataUsagesOfZones',response);
        expect(homeCtrl.todaysSpend).toBe('33.94');
        expect(homeCtrl.dailyAverageSpend).toBe('3.68');
        expect(homeCtrl.totalCurrentSpend).toBe('133.94');
        expect(homeCtrl.totalEstimatedSpend).toBe('15.49');
    });

    it('should handle when the getPlanDetails service fails ', function () {
        var plans = {"type":"AccountZoneList","totalCount":8,"count":8,"list":[{"type":"ZoneInfo","zoneId":1,"name":"2-Zone1","nickName":"2-Zone1","pricePerMB":0.01,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"},{"type":"country","name":"United Kingdom","code":"GB"},{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":310,"mnc":170},{"type":"mccmnc","mcc":310,"mnc":260},{"type":"mccmnc","mcc":404,"mnc":31},{"type":"mccmnc","mcc":405,"mnc":46},{"type":"mccmnc","mcc":234,"mnc":15}]},{"type":"ZoneInfo","zoneId":2,"name":"2-Zone2","nickName":"2-Zone2","pricePerMB":0.1,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Singapore","code":"SG"},{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":525,"mnc":5},{"type":"mccmnc","mcc":310,"mnc":250},{"type":"mccmnc","mcc":310,"mnc":240}]},{"type":"ZoneInfo","zoneId":3,"name":"2-Zone3","nickName":"2-Zone3","pricePerMB":0.45,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":310,"mnc":380},{"type":"mccmnc","mcc":310,"mnc":680},{"type":"mccmnc","mcc":310,"mnc":410}]},{"type":"ZoneInfo","zoneId":4,"name":"2-Zone4","nickName":"2-Zone4","pricePerMB":1.01,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"}],"mccmncList":[{"type":"mccmnc","mcc":405,"mnc":809},{"type":"mccmnc","mcc":405,"mnc":808},{"type":"mccmnc","mcc":405,"mnc":804},{"type":"mccmnc","mcc":405,"mnc":807},{"type":"mccmnc","mcc":405,"mnc":805},{"type":"mccmnc","mcc":404,"mnc":25},{"type":"mccmnc","mcc":404,"mnc":28}]},{"type":"ZoneInfo","zoneId":5,"name":"2-Zone5","nickName":"2-Zone5","pricePerMB":0.75,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"}],"mccmncList":[{"type":"mccmnc","mcc":404,"mnc":37},{"type":"mccmnc","mcc":404,"mnc":91},{"type":"mccmnc","mcc":404,"mnc":42},{"type":"mccmnc","mcc":404,"mnc":13},{"type":"mccmnc","mcc":404,"mnc":17},{"type":"mccmnc","mcc":404,"mnc":41},{"type":"mccmnc","mcc":404,"mnc":56}]},{"type":"ZoneInfo","zoneId":6,"name":"2-Zone6","nickName":"2-Zone6","pricePerMB":0.9,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Thailand","code":"TH"}],"mccmncList":[{"type":"mccmnc","mcc":520,"mnc":1}]},{"type":"ZoneInfo","zoneId":7,"name":"2-Zone7","nickName":"2-Zone7","pricePerMB":0.09,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Malaysia","code":"MY"},{"type":"country","name":"South Africa","code":"ZA"}],"mccmncList":[{"type":"mccmnc","mcc":502,"mnc":19},{"type":"mccmnc","mcc":655,"mnc":7}]},{"type":"ZoneInfo","zoneId":8,"name":"2-Zone8","nickName":"2-Zone8","pricePerMB":0.04,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Colombia","code":"CO"}],"mccmncList":[{"type":"mccmnc","mcc":732,"mnc":101}]}]};
        var _mockPromise3 = {
            success: function (fn) {
                return _mockPromise3;
            },
            error: function (fn) {
                fn(error);
                return _mockPromise3;
            }
        };
        spyOn(_settingsService, 'getPlanDetails').and.returnValue(_mockPromise3);
        sessionStorage.setItem('gs-enterpriseCurrency',"USD");

        var response ={
            data : tillDayData,
            startDate : "2016-08-01 00:00:00",
            endDate : "2016-08-31 23:59:59"
        }


        homeCtrl = _controller("HomeCtrl", {
            $scope: scope,
            SIMService:_SIMService,
            UserService:_UserService,
            AccountService: _accountService,
            requestGraphData:_requestGraphData,
            commonUtilityService:_commonUtilityService,
            dataUsageService:_dataUsageService,
            SettingsService: _settingsService
        });

        _rootScope.$emit('onGetMonthlyDataUsagesOfZones',response);

        expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith(error.errorStr);
    });

    it('should test formatDataCost function', function () {
        homeCtrl = _controller("HomeCtrl", {
            $scope: scope,
            SIMService:_SIMService,
            UserService:_UserService,
            AccountService: _accountService,
            requestGraphData:_requestGraphData,
            commonUtilityService:_commonUtilityService,
            dataUsageService:_dataUsageService,
            SettingsService: _settingsService
        });
        sessionStorage.setItem('gs-enterpriseCurrency',"USD");
        var cost = homeCtrl.formatDataCost(32.343434);
        expect(cost).toBe('32.34');

        homeCtrl.currency = 'JPY';
        var cost1 = homeCtrl.formatDataCost(32.343434);
        expect(cost1).toBe('32');

        homeCtrl.currency = 'EUR';
        var cost2 = homeCtrl.formatDataCost(32.398434);
        expect(cost2).toBe('32.40');

        homeCtrl.currency = 'GBP';
        var cost3 = homeCtrl.formatDataCost(0);
        expect(cost3).toBe(0);
    });


});