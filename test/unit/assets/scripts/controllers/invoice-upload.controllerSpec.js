describe('Invoice upload controller', function() {

   var error = {errorStr:"error"};

    var InvoiceUploadCtrl, scope,_controller,_rootScope,_modalDetails;

    beforeEach(module('app'));


    beforeEach(inject(function ($injector,$rootScope,$controller, modalDetails) {

        scope = $rootScope.$new();
        _modalDetails=modalDetails;
        _controller = $controller;
        _rootScope = $rootScope;
        scope.$parent={dataTableObj:{refreshDataTable : function(){
            return;
        }}};

    }));


    it('Should verify InvoiceUpload controller init', function () {
        InvoiceUploadCtrl = _controller("InvoiceUploadCtrl", {
            $scope: scope,
            $rootScope:_rootScope,
            modalDetails:_modalDetails
        });
    });

    it('Should verify InvoiceUpload controller submit', function () {
        spyOn(_modalDetails,'submit').and.callFake(function(fn){
            fn();
        });
        InvoiceUploadCtrl = _controller("InvoiceUploadCtrl", {
            $scope: scope,
            $rootScope:_rootScope,
            modalDetails:_modalDetails
        });
    });

    it('Should verify InvoiceUpload controller reset', function () {
        spyOn(_modalDetails,'cancel').and.callFake(function(fn){
            fn();
        });
        InvoiceUploadCtrl = _controller("InvoiceUploadCtrl", {
            $scope: scope,
            $rootScope:_rootScope,
            modalDetails:_modalDetails
        });
    });
});