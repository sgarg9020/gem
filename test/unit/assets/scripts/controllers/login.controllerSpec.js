describe('login Controller', function() {

    var   scope,  _rootScope, _q,_authenticationService,_controller,_location;

    var authenticateResponse = {"type":"UserLoginResponse","token":"7wnKoJIjviy6moi3YbpIBNUiIwMBUZ9wLLzlts3S23E=","userId":101,"expiryDate":"2016-06-23 10:51:32"};

    var getUserAccountsResponse = {
        "type": "User",
        "userId": 1,
        "firstName": "gsAdmin",
        "lastName": "gsAdmin",
        "emailId": "gsAdmin@gigsky.com",
        "location": "Palo-Alto",
        "address": {
            "type": "Address",
            "addressId": 1,
            "address1": "Beverly Hills",
            "address2": "Inside Den",
            "state": "",
            "city": "SanHouse",
            "zipCode": "560076",
            "country": "Ca"
        },
        "userStatus": "ACTIVE",
        "createdOn": "2015-12-02 13:58:16",
        "homePhone": "080-58974536",
        "mobile": "9886333515",
        "accountDetails": {
            "type": "EnterpriseAccountList",
            "enterpriseAccountDetails": [{
                "type": "Account",
                "rootAccountDetails": {
                    "type": "Account",
                    "accountId": 1,
                    "accountType": "ENTERPRISE",
                    "accountName": "Gigsky"
                },
                "parentAccountDetails": [{
                    "type": "Account",
                    "accountId": 1,
                    "accountType": "GIGSKY_ROOT",
                    "accountName": "Gigsky",
                    "role": "GIGSKY_ADMIN"
                }],
                "selfAccountDetails": {
                    "type": "Account",
                    "accountId": 111111,
                    "accountType": "USER"
                }
            }]
        },
        "totalSimsAssociated": "1",
        "dataUsedInBytes": 0
    }

    var enterpriseLoginResponse = {"type":"AccountLoginResponse","token":"2etbmj4CBYav3vlcUF7Cqdr/t1+MDCl8Qz+r02j24JA=","expiryDate":"2016-06-23 10:55:27"}

    beforeEach(module('app'));

    beforeEach(inject(function ($injector, $rootScope, $q, $controller, $location, AuthenticationService) {

        sessionStorage.setItem("accountId", "1234");

        _authenticationService = AuthenticationService
        _rootScope = $rootScope;
        scope = $rootScope.$new();
        _q = $q;
        _controller = $controller;
        _location = $location;

        spyOn(_location, 'path');
    }));

    it('verify authenticate form invalid ', function () {
        var _mockPromise = {
            success: function (fn) {
                fn(authenticateResponse);
                return _mockPromise;
            },
            error: function (fn) {
                fn(authenticateResponse);
                return _mockPromise;
            }
        };

        spyOn(_authenticationService,"authenticate").and.returnValue(_mockPromise);
        scope.isFormValid = function(){
            return false;
        };
        LoginCtrl = _controller("LoginCtrl", {
            $scope: scope,
            $location: _location,
            AuthenticationService:_authenticationService
        });
        var loginForm = {emailId:{$modelValue:'jdande@gigsky.com'},password:{$modelValue:''}};
        LoginCtrl.authenticate(loginForm);

        expect(_authenticationService.authenticate).not.toHaveBeenCalled();
    });

    it('verify authenticate form is valid and its gsAdmin(both from admin.gigsky.com and enterprise.gigsky.com) ', function () {
        var _mockPromise = {
            success: function (fn) {
                fn(authenticateResponse);
                return _mockPromise;
            },
            error: function (fn) {
                fn(authenticateResponse);
                return _mockPromise;
            }
        };
        spyOn(_authenticationService,"authenticate").and.returnValue(_mockPromise);

        var _mockPromise1 = {
            success: function (fn) {
                fn(getUserAccountsResponse);
                return _mockPromise1;
            },
            error: function (fn) {
                fn(getUserAccountsResponse);
                return _mockPromise1;
            }
        };
        spyOn(_authenticationService,"getUserAccount").and.returnValue(_mockPromise1);

        var _mockPromise2 = {
            success: function (fn) {
                fn(enterpriseLoginResponse);
                return _mockPromise2;
            },
            error: function (fn) {
                fn(enterpriseLoginResponse);
                return _mockPromise2;
            }
        };
        spyOn(_authenticationService,"enterpriseLogin").and.returnValue(_mockPromise2);

        scope.isFormValid = function(){
            return true;
        };
        LoginCtrl = _controller("LoginCtrl", {
            $scope: scope,
            $location: _location,
            AuthenticationService:_authenticationService
        });
        var loginForm = {emailId:{$modelValue:'jdande@gigsky.com'},password:{$modelValue:''}};
        //once through admin.gigsky.com and second time through enterprise.gigsky.com
        spyOn(_location, 'absUrl').and.returnValues("https://admin.gigsky.com","https://enterprise.gigsky.com");

        //Gsadmin logged in through admin.gigsky.com
        LoginCtrl.authenticate(loginForm);

        expect(_authenticationService.authenticate).toHaveBeenCalled();
        expect(_authenticationService.getUserAccount).toHaveBeenCalled();
        expect(_authenticationService.enterpriseLogin).toHaveBeenCalled();
        expect(_rootScope.isGSAdmin).toBe(true);
        expect(sessionStorage.getItem('isGSAdmin')).toBe('true');
        expect(_rootScope.hasMultiAccount).toBe(true);
        expect(sessionStorage.getItem('hasMultiAccount')).toBe('true');
        expect(sessionStorage.getItem('rootAccountId')).toBe('1');
        expect(sessionStorage.getItem('accountId')).toBe('1');
        expect(sessionStorage.getItem('token')).toBe("2etbmj4CBYav3vlcUF7Cqdr/t1+MDCl8Qz+r02j24JA=");
        expect(_location.path).toHaveBeenCalledWith('/support/search');

        //Gsadmin logged in through enterprise.gigsky.com
        LoginCtrl.authenticate(loginForm);
        expect(_authenticationService.authenticate).toHaveBeenCalled();
        expect(_authenticationService.getUserAccount).toHaveBeenCalled();
        expect(_authenticationService.enterpriseLogin).toHaveBeenCalled();
        expect(_rootScope.formErrorMsg).toBe('Admin user is not allowed. For any enterprise support please use admin.gigsky.com');
        expect(_location.path).toHaveBeenCalledWith('/app/login');
        expect(sessionStorage.getItem('isGSAdmin')).toBe(null);
        expect(sessionStorage.getItem('hasMultiAccount')).toBe(null);
        expect(sessionStorage.getItem('rootAccountId')).toBe(null);
        expect(sessionStorage.getItem('accountId')).toBe(null);
        expect(sessionStorage.getItem('token')).toBe(null);
    });

    it('verify authenticate enterprise admin with single account', function () {
        var _mockPromise = {
            success: function (fn) {
                fn(authenticateResponse);
                return _mockPromise;
            },
            error: function (fn) {
                fn(authenticateResponse);
                return _mockPromise;
            }
        };

        spyOn(_authenticationService,"authenticate").and.returnValue(_mockPromise);


        var getUserAccountsResponse1 = {"type":"User","userId":2,"firstName":"uberAdmin","lastName":"uberAdmin","emailId":"uberAdmin@uber.com","location":"Palo-Alto","address":{"type":"Address","addressId":2,"address1":"Electronics city","address2":"Hosur Road","state":"Karnataka","city":"Bangalore","zipCode":"560093","country":"IN"},"userStatus":"ACTIVE","createdOn":"2015-12-02 08:28:16","homePhone":"080-58974536","mobile":"9886333515","accountDetails":{"type":"EnterpriseAccountList","enterpriseAccountDetails":[{"type":"Account","rootAccountDetails":{"type":"Account","accountId":2,"accountType":"ENTERPRISE","accountName":"Uberr"},"parentAccountDetails":[{"type":"Account","accountId":2,"accountType":"ENTERPRISE","accountName":"Uberr","role":"ENTERPRISE_ADMIN"},{"type":"Account","accountId":81871,"accountType":"GROUP","accountName":"fdsfdsf","role":"ENTERPRISE_USER"}],"selfAccountDetails":{"type":"Account","accountId":22,"accountType":"USER"}}]},"totalSimsAssociated":"14","dataUsedInBytes":0};
        var _mockPromise1 = {
            success: function (fn) {
                fn(getUserAccountsResponse1);
                return _mockPromise1;
            },
            error: function (fn) {
                fn(getUserAccountsResponse1);
                return _mockPromise1;
            }
        };
        spyOn(_authenticationService,"getUserAccount").and.returnValue(_mockPromise1);

        var _mockPromise2 = {
            success: function (fn) {
                fn(enterpriseLoginResponse);
                return _mockPromise2;
            },
            error: function (fn) {
                fn(enterpriseLoginResponse);
                return _mockPromise2;
            }
        };
        spyOn(_authenticationService,"enterpriseLogin").and.returnValue(_mockPromise2);
        scope.isFormValid = function(){
            return true;
        };

        LoginCtrl = _controller("LoginCtrl", {
            $scope: scope,
            $location: _location,
            AuthenticationService:_authenticationService
        });
        var loginForm = {emailId:{$modelValue:'uberAdmin@uber.com'},password:{$modelValue:'gigsky123'}};
        LoginCtrl.authenticate(loginForm);

        expect(_authenticationService.authenticate).toHaveBeenCalled();
        expect(_authenticationService.getUserAccount).toHaveBeenCalled();
        expect(_authenticationService.enterpriseLogin).toHaveBeenCalled();

        var isGsAdmin = sessionStorage.getItem('isGSAdmin');
        expect(isGsAdmin).toBe('false');
        expect(_rootScope.isGSAdmin).toBe(false);
        expect(_rootScope.hasMultiAccount).toBe(false);
        expect(_location.path).toHaveBeenCalledWith('/app/home');
    });

    it('verify authenticate enterprise admin with multiple accounts', function () {
        var _mockPromise = {
            success: function (fn) {
                fn(authenticateResponse);
                return _mockPromise;
            },
            error: function (fn) {
                fn(authenticateResponse);
                return _mockPromise;
            }
        };

        spyOn(_authenticationService,"authenticate").and.returnValue(_mockPromise);


        var getUserAccountsResponse1 = {"type":"User","userId":52,"firstName":"infy Admin","lastName":"infyAdmin","emailId":"infyAdmin@infy.com","location":"india","address":{"type":"Address","addressId":664,"address1":"Beverly Hills","address2":"Inside Den","state":"","city":"SanHouse","zipCode":"560076","country":"AF"},"userStatus":"ACTIVE","createdOn":"2015-12-02 13:58:16","accountDetails":{"type":"EnterpriseAccountList","enterpriseAccountDetails":[{"type":"Account","rootAccountDetails":{"type":"Account","accountId":2,"accountType":"ENTERPRISE","accountName":"Uber"},"parentAccountDetails":[{"type":"Account","accountId":2,"accountType":"ENTERPRISE","accountName":"Uber","role":"ENTERPRISE_ADMIN"}],"selfAccountDetails":null},{"type":"Account","rootAccountDetails":{"type":"Account","accountId":7,"accountType":"ENTERPRISE","accountName":"Infosys1"},"parentAccountDetails":[{"type":"Account","accountId":7,"accountType":"ENTERPRISE","accountName":"Infosys1","role":"ENTERPRISE_ADMIN"},{"type":"Account","accountId":111112,"accountType":"GROUP","accountName":"Maisuru - infy1","role":"ENTERPRISE_USER"}],"selfAccountDetails":{"type":"Account","accountId":752,"accountType":"USER"}}]},"totalSimsAssociated":"6","dataUsedInBytes":0};
        var _mockPromise1 = {
            success: function (fn) {
                fn(getUserAccountsResponse1);
                return _mockPromise1;
            },
            error: function (fn) {
                fn(getUserAccountsResponse1);
                return _mockPromise1;
            }
        };
        spyOn(_authenticationService,"getUserAccount").and.returnValue(_mockPromise1);

        var _mockPromise2 = {
            success: function (fn) {
                fn(enterpriseLoginResponse);
                return _mockPromise2;
            },
            error: function (fn) {
                fn(enterpriseLoginResponse);
                return _mockPromise2;
            }
        };
        spyOn(_authenticationService,"enterpriseLogin").and.returnValue(_mockPromise2);
        scope.isFormValid = function(){
            return true;
        };

        LoginCtrl = _controller("LoginCtrl", {
            $scope: scope,
            $location: _location,
            AuthenticationService:_authenticationService
        });
        var loginForm = {emailId:{$modelValue:'uberAdmin@uber.com'},password:{$modelValue:'gigsky123'}};
        LoginCtrl.authenticate(loginForm);

        expect(_authenticationService.authenticate).toHaveBeenCalled();
        expect(_authenticationService.getUserAccount).toHaveBeenCalled();

        var isGsAdmin = sessionStorage.getItem('isGSAdmin');
        expect(isGsAdmin).toBe('false');
        expect(_rootScope.isGSAdmin).toBe(false);
        expect(_rootScope.hasMultiAccount).toBe(true);
        expect(_location.path).toHaveBeenCalledWith('/auth/select-account');
    });

    it('verify authenticate enterprise user', function () {
        var _mockPromise = {
            success: function (fn) {
                fn(authenticateResponse);
                return _mockPromise;
            },
            error: function (fn) {
                fn(authenticateResponse);
                return _mockPromise;
            }
        };

        spyOn(_authenticationService,"authenticate").and.returnValue(_mockPromise);


        var getUserAccountsResponse1 = {"type":"User","userId":23,"firstName":"uberCalUser1","lastName":"uberCalUser1","emailId":"uberCalUser1@uber.com","address":{"type":"Address","addressId":29,"address1":"Kalmeshwar gudi oni","address2":"Revan peth","state":"Karnatak","city":"Hebballi","zipCode":"580112","country":"IN"},"userStatus":"ACTIVE","createdOn":"2015-12-02 08:28:16","accountDetails":{"type":"EnterpriseAccountList","enterpriseAccountDetails":[{"type":"Account","rootAccountDetails":{"type":"Account","accountId":2,"accountType":"ENTERPRISE","accountName":"Uberr"},"parentAccountDetails":[{"type":"Account","accountId":2,"accountType":"ENTERPRISE","accountName":"Uberr","role":"ENTERPRISE_USER"}],"selfAccountDetails":{"type":"Account","accountId":423,"accountType":"USER"}}]},"totalSimsAssociated":"1","dataUsedInBytes":990000000};
        var _mockPromise1 = {
            success: function (fn) {
                fn(getUserAccountsResponse1);
                return _mockPromise1;
            },
            error: function (fn) {
                fn(getUserAccountsResponse1);
                return _mockPromise1;
            }
        };
        spyOn(_authenticationService,"getUserAccount").and.returnValue(_mockPromise1);

        var _mockPromise2 = {
            success: function (fn) {
                fn(enterpriseLoginResponse);
                return _mockPromise2;
            },
            error: function (fn) {
                fn(enterpriseLoginResponse);
                return _mockPromise2;
            }
        };
        spyOn(_authenticationService,"enterpriseLogin").and.returnValue(_mockPromise2);
        scope.isFormValid = function(){
            return true;
        };

        LoginCtrl = _controller("LoginCtrl", {
            $scope: scope,
            $location: _location,
            AuthenticationService:_authenticationService
        });
        var loginForm = {emailId:{$modelValue:'uberAdmin@uber.com'},password:{$modelValue:'gigsky123'}};
        LoginCtrl.authenticate(loginForm);

        expect(_authenticationService.authenticate).toHaveBeenCalled();
        expect(_authenticationService.getUserAccount).toHaveBeenCalled();

        var isGsAdmin = sessionStorage.getItem('isGSAdmin');
        expect(isGsAdmin).toBe(null);
        expect(_rootScope.isGSAdmin).toBe(false);
        expect(sessionStorage.getItem('token')).toBe(null);
    });

    it('should search for the Gigsky Admin role for user associated with multiple enterprise', function () {
        var getUserAccountsResponse1 = {"type": "User", "userId": 101, "firstName": "gsAdmin", "lastName": "gsAdmin", "emailId": "gsAdmin@gigsky.com", "location": "Palo-Alto", "address": {"type": "Address", "addressId": 1, "address1": "Beverly Hills", "address2": "Inside Den", "state": "", "city": "SanHouse", "zipCode": "560076", "country": "Ca"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 13:58:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": {"type": "EnterpriseAccountList",
            "enterpriseAccountDetails": [
                {"type": "Account", "rootAccountDetails": {"type": "Account", "accountId": 1, "accountType": "ENTERPRISE", "accountName": "Gigsky"}, "parentAccountDetails": [{"type": "Account", "accountId": 4, "accountType": "GROUP", "accountName": "group1", "role": "ENTERPRISE_USER"}], "selfAccountDetails": {"type": "Account", "accountId": 111112, "accountType": "USER"}},
                {"type": "Account", "rootAccountDetails": {"type": "Account", "accountId": 2, "accountType": "ENTERPRISE", "accountName": "Gigsky"}, "parentAccountDetails": [{"type": "Account", "accountId": 5, "accountType": "GIGSKY_ROOT", "accountName": "Gigsky", "role": "GIGSKY_ADMIN"}], "selfAccountDetails": {"type": "Account", "accountId": 111111, "accountType": "USER"}},
                {"type": "Account", "rootAccountDetails": {"type": "Account", "accountId": 3, "accountType": "ENTERPRISE", "accountName": "Gigsky"}, "parentAccountDetails": [{"type": "Account", "accountId": 6, "accountType": "GROUP", "accountName": "group1", "role": "ENTERPRISE_USER"}], "selfAccountDetails": {"type": "Account", "accountId": 111113, "accountType": "USER"}}]}, "totalSimsAssociated": "1", "dataUsedInBytes": 0};

        var _mockPromise = {
            success: function (fn) {
                fn(authenticateResponse);
                return _mockPromise;
            },
            error: function (fn) {
                return _mockPromise;
            }
        };
        spyOn(_authenticationService,"authenticate").and.returnValue(_mockPromise);

        var _mockPromise1 = {
            success: function (fn) {
                fn(getUserAccountsResponse1);
                return _mockPromise1;
            },
            error: function (fn) {
                return _mockPromise1;
            }
        };
        spyOn(_authenticationService,"getUserAccount").and.returnValue(_mockPromise1);

        var _mockPromise2 = {
            success: function (fn) {
                fn(enterpriseLoginResponse);
                return _mockPromise2;
            },
            error: function (fn) {
                return _mockPromise2;
            }
        };
        spyOn(_authenticationService,"enterpriseLogin").and.returnValue(_mockPromise2);

        scope.isFormValid = function(){
            return true;
        };
        var LoginCtrl = _controller("LoginCtrl", {
            $scope: scope,
            $location: _location,
            AuthenticationService:_authenticationService
        });
        var loginForm = {emailId:{$modelValue:'jdande@gigsky.com'},password:{$modelValue:'password'}};

        LoginCtrl.authenticate(loginForm);

        expect(_rootScope.isGSAdmin).toBeTruthy();
        expect(_rootScope.hasMultiAccount).toBeTruthy();
        expect(_authenticationService.enterpriseLogin).toHaveBeenCalledWith(5,101);
        expect(_rootScope.formErrorMsg).toBe('Admin user is not allowed. For any enterprise support please use admin.gigsky.com');

    });

    it('should search for the Gigsky Admin role for user associated with multiple accounts in a enterprise', function () {
        var getUserAccountsResponse1 = {"type": "User", "userId": 101, "firstName": "gsAdmin", "lastName": "gsAdmin", "emailId": "gsAdmin@gigsky.com", "location": "Palo-Alto", "address": {"type": "Address", "addressId": 1, "address1": "Beverly Hills", "address2": "Inside Den", "state": "", "city": "SanHouse", "zipCode": "560076", "country": "Ca"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 13:58:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": {"type": "EnterpriseAccountList",
            "enterpriseAccountDetails": [
                {"type": "Account", "rootAccountDetails": {"type": "Account", "accountId": 3, "accountType": "ENTERPRISE", "accountName": "Gigsky"},
                    "parentAccountDetails": [
                        {"type": "Account", "accountId": 6, "accountType": "GROUP", "accountName": "group1", "role": "ENTERPRISE_USER"},{"type": "Account", "accountId": 7, "accountType": "GROUP", "accountName": "group2", "role": "ENTERPRISE_USER"},{"type": "Account", "accountId": 8, "accountType": "GIGSKY_ROOT", "accountName": "group1", "role": "GIGSKY_ADMIN"}], "selfAccountDetails": {"type": "Account", "accountId": 111113, "accountType": "USER"}}]}, "totalSimsAssociated": "1", "dataUsedInBytes": 0};

        var _mockPromise = {
            success: function (fn) {
                fn(authenticateResponse);
                return _mockPromise;
            },
            error: function (fn) {
                return _mockPromise;
            }
        };
        spyOn(_authenticationService,"authenticate").and.returnValue(_mockPromise);

        var _mockPromise1 = {
            success: function (fn) {
                fn(getUserAccountsResponse1);
                return _mockPromise1;
            },
            error: function (fn) {
                return _mockPromise1;
            }
        };
        spyOn(_authenticationService,"getUserAccount").and.returnValue(_mockPromise1);

        var _mockPromise2 = {
            success: function (fn) {
                fn(enterpriseLoginResponse);
                return _mockPromise2;
            },
            error: function (fn) {
                return _mockPromise2;
            }
        };
        spyOn(_authenticationService,"enterpriseLogin").and.returnValue(_mockPromise2);

        scope.isFormValid = function(){
            return true;
        };
        var LoginCtrl = _controller("LoginCtrl", {
            $scope: scope,
            $location: _location,
            AuthenticationService:_authenticationService
        });
        var loginForm = {emailId:{$modelValue:'jdande@gigsky.com'},password:{$modelValue:'password'}};

        LoginCtrl.authenticate(loginForm);

        expect(_rootScope.isGSAdmin).toBeTruthy();
        expect(_rootScope.hasMultiAccount).toBeTruthy();
        expect(_authenticationService.enterpriseLogin).toHaveBeenCalledWith(8,101);
        expect(_rootScope.formErrorMsg).toBe('Admin user is not allowed. For any enterprise support please use admin.gigsky.com');

    });

    it('should handle when there is no Gigsky Admin role for user', function () {
        var getUserAccountsResponse1 = {"type": "User", "userId": 101, "firstName": "gsAdmin", "lastName": "gsAdmin", "emailId": "gsAdmin@gigsky.com", "location": "Palo-Alto", "address": {"type": "Address", "addressId": 1, "address1": "Beverly Hills", "address2": "Inside Den", "state": "", "city": "SanHouse", "zipCode": "560076", "country": "Ca"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 13:58:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": {"type": "EnterpriseAccountList",
            "enterpriseAccountDetails": [
                {"type": "Account", "rootAccountDetails": {"type": "Account", "accountId": 1, "accountType": "ENTERPRISE", "accountName": "Gigsky"}, "parentAccountDetails": [{"type": "Account", "accountId": 4, "accountType": "GROUP", "accountName": "group1", "role": "ENTERPRISE_USER"}], "selfAccountDetails": {"type": "Account", "accountId": 111112, "accountType": "USER"}},
                {"type": "Account", "rootAccountDetails": {"type": "Account", "accountId": 2, "accountType": "ENTERPRISE", "accountName": "Gigsky"}, "parentAccountDetails": [{"type": "Account", "accountId": 8, "accountType": "ENTERPRISE", "accountName": "Gigsky", "role": "ENTERPRISE_ADMIN"}], "selfAccountDetails": {"type": "Account", "accountId": 111111, "accountType": "USER"}},
                {"type": "Account", "rootAccountDetails": {"type": "Account", "accountId": 3, "accountType": "ENTERPRISE", "accountName": "Gigsky"}, "parentAccountDetails": [{"type": "Account", "accountId": 6, "accountType": "GROUP", "accountName": "group1", "role": "ENTERPRISE_USER"}], "selfAccountDetails": {"type": "Account", "accountId": 111113, "accountType": "USER"}}]}, "totalSimsAssociated": "1", "dataUsedInBytes": 0};

        var _mockPromise = {
            success: function (fn) {
                fn(authenticateResponse);
                return _mockPromise;
            },
            error: function (fn) {
                return _mockPromise;
            }
        };
        spyOn(_authenticationService,"authenticate").and.returnValue(_mockPromise);

        var _mockPromise1 = {
            success: function (fn) {
                fn(getUserAccountsResponse1);
                return _mockPromise1;
            },
            error: function (fn) {
                return _mockPromise1;
            }
        };
        spyOn(_authenticationService,"getUserAccount").and.returnValue(_mockPromise1);

        var _mockPromise2 = {
            success: function (fn) {
                fn(enterpriseLoginResponse);
                return _mockPromise2;
            },
            error: function (fn) {
                return _mockPromise2;
            }
        };
        spyOn(_authenticationService,"enterpriseLogin").and.returnValue(_mockPromise2);

        scope.isFormValid = function(){
            return true;
        };
        var LoginCtrl = _controller("LoginCtrl", {
            $scope: scope,
            $location: _location,
            AuthenticationService:_authenticationService
        });
        var loginForm = {emailId:{$modelValue:'jdande@gigsky.com'},password:{$modelValue:'password'}};

        LoginCtrl.authenticate(loginForm);

        expect(_rootScope.isGSAdmin).toBeFalsy();
        expect(_rootScope.hasMultiAccount).toBeFalsy();
        expect(_authenticationService.enterpriseLogin).toHaveBeenCalledWith(8,101);

    });

    it('should handle when the user is not associated with any enterprise', function () {
        var getUserAccountsResponse1 = {"type": "User", "userId": 101, "firstName": "gsAdmin", "lastName": "gsAdmin", "emailId": "gsAdmin@gigsky.com", "location": "Palo-Alto", "address": {"type": "Address", "addressId": 1, "address1": "Beverly Hills", "address2": "Inside Den", "state": "", "city": "SanHouse", "zipCode": "560076", "country": "Ca"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 13:58:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": {"type": "EnterpriseAccountList",
            "enterpriseAccountDetails": []}, "totalSimsAssociated": "1", "dataUsedInBytes": 0};

        var _mockPromise = {
            success: function (fn) {
                fn(authenticateResponse);
                return _mockPromise;
            },
            error: function (fn) {
                return _mockPromise;
            }
        };
        spyOn(_authenticationService,"authenticate").and.returnValue(_mockPromise);

        var _mockPromise1 = {
            success: function (fn) {
                fn(getUserAccountsResponse1);
                return _mockPromise1;
            },
            error: function (fn) {
                return _mockPromise1;
            }
        };
        spyOn(_authenticationService,"getUserAccount").and.returnValue(_mockPromise1);


        spyOn(_authenticationService,"enterpriseLogin");
        spyOn(_authenticationService,"clearSession");

        scope.isFormValid = function(){
            return true;
        };
        var LoginCtrl = _controller("LoginCtrl", {
            $scope: scope,
            $location: _location,
            AuthenticationService:_authenticationService
        });
        var loginForm = {emailId:{$modelValue:'jdande@gigsky.com'},password:{$modelValue:'password'}};

        LoginCtrl.authenticate(loginForm);

        expect(LoginCtrl.errorMessage).toBe("User doesn't have privileges to access Enterprise portal.");
        expect(_authenticationService.enterpriseLogin).not.toHaveBeenCalled();
        expect(_authenticationService.clearSession).toHaveBeenCalled();

    });
});