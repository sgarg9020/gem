describe('Invoices Controller', function() {

    var _commonUtilityService, _InvoiceService, _dataTableConfigService, deferred;

    var InvoicesCtrl, scope;
    var invoices = [
        {
            "invoiceNumber": "sample-invoice-1.pdf",
            "url": "/enterprise/app/data/sample-invoice-1.pdf",
            "generatedDate": "2015-11-01 00:00:00"
        },
        {
            "invoiceNumber": "sample-invoice-2.pdf",
            "url": "/enterprise/app/data/sample-invoice-2.pdf",
            "generatedDate": "2015-11-02 00:00:00"
        }];

    var invoice =  {
        "invoiceNumber": "sample-invoice-1.pdf",
        "url": "/enterprise/app/data/sample-invoice-1.pdf",
        "generatedDate": "2015-11-01 00:00:00"
    };


    beforeEach(module('app'));


    beforeEach(inject(function ($injector, $controller, $compile, InvoiceService, dataTableConfigService, commonUtilityService) {
        scope = {};
        scope.vm = {};
        InvoicesCtrl = $controller("InvoicesCtrl", {$scope: scope, $compile: $compile, dataTableConfigService: dataTableConfigService, InvoiceService: InvoiceService, commonUtilityService: commonUtilityService});
        _commonUtilityService = commonUtilityService;
        _InvoiceService = InvoiceService;
        _dataTableConfigService = dataTableConfigService;
        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');


    }));

    it('Should test data-table columnDefs 0',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = invoice;
        expect((scope.dataTableObj.dataTableOptions.columnDefs[0].render(data,type,row,meta)).indexOf(row.invoiceNumber)).not.toBe(-1);
    });

    it('Should test data-table columnDefs 1',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = invoice;
        var val = new Date(row.generatedDate.replace(/-/g, "/"));

        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toContain((val.getMonth()+1)+"/"+val.getDate()+"/"+val.getFullYear());
    });

    it('Should process data-table parameters and construct request for rest and populate response to data-table accordingly',function(){
        var sSource, aoData, fnCallback;
        var expectedResponse = {"type":"Invoices","startIndex":"0","totalCount":2,"count":2,"list":invoices};
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_InvoiceService, 'getInvoices').and.returnValue(_mockPromise);
        fnCallback = function(data){
            expect(data.recordsTotal).toEqual(expectedResponse.totalCount);
            expect(data.recordsFiltered).toEqual(expectedResponse.totalCount);
            expect(data.aaData).toEqual(expectedResponse.list);
        };
        scope.dataTableObj.dataTableRef ={};
        scope.dataTableObj.dataTableRef.fnSettings = function(){};
        sSource = "URL";
        aoData = [{"name":"draw","value":1},{"name":"columns","value":[{"data":0,"name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":1,"name":"","searchable":true,"orderable":true,"search":{"value":"","regex":false}}]},{"name":"order","value":[{"column":1,"dir":"desc"}]},{"name":"start","value":0},{"name":"length","value":10},{"name":"search","value":{"value":"","regex":false}}];
        scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
        expect(scope.invoicesList.length).toBe(2);

    });


});