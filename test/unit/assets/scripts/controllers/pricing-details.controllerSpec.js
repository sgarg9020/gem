describe('Pricing details controller', function () {

    describe('successful initialization of the controller', function () {
        var _scope, PricingDetailsCtrl, _$q, _SettingsService, _commonUtilityService, loadingState, _authenticationService, _rootScope;

        beforeEach(module('app'));

        var plans = {"type":"AccountZoneList","totalCount":8,"count":8,"list":[{"type":"ZoneInfo","zoneId":1,"name":"2-Zone1","nickName":"2-Zone1","pricePerMB":0.01,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"},{"type":"country","name":"United Kingdom","code":"GB"},{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":310,"mnc":170},{"type":"mccmnc","mcc":310,"mnc":260},{"type":"mccmnc","mcc":404,"mnc":31},{"type":"mccmnc","mcc":405,"mnc":46},{"type":"mccmnc","mcc":234,"mnc":15}]},{"type":"ZoneInfo","zoneId":2,"name":"2-Zone2","nickName":"2-Zone2","pricePerMB":0.1,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Singapore","code":"SG"},{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":525,"mnc":5},{"type":"mccmnc","mcc":310,"mnc":250},{"type":"mccmnc","mcc":310,"mnc":240}]},{"type":"ZoneInfo","zoneId":3,"name":"2-Zone3","nickName":"2-Zone3","pricePerMB":0.45,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":310,"mnc":380},{"type":"mccmnc","mcc":310,"mnc":680},{"type":"mccmnc","mcc":310,"mnc":410}]},{"type":"ZoneInfo","zoneId":4,"name":"2-Zone4","nickName":"2-Zone4","pricePerMB":1.01,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"}],"mccmncList":[{"type":"mccmnc","mcc":405,"mnc":809},{"type":"mccmnc","mcc":405,"mnc":808},{"type":"mccmnc","mcc":405,"mnc":804},{"type":"mccmnc","mcc":405,"mnc":807},{"type":"mccmnc","mcc":405,"mnc":805},{"type":"mccmnc","mcc":404,"mnc":25},{"type":"mccmnc","mcc":404,"mnc":28}]},{"type":"ZoneInfo","zoneId":5,"name":"2-Zone5","nickName":"2-Zone5","pricePerMB":0.75,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"}],"mccmncList":[{"type":"mccmnc","mcc":404,"mnc":37},{"type":"mccmnc","mcc":404,"mnc":91},{"type":"mccmnc","mcc":404,"mnc":42},{"type":"mccmnc","mcc":404,"mnc":13},{"type":"mccmnc","mcc":404,"mnc":17},{"type":"mccmnc","mcc":404,"mnc":41},{"type":"mccmnc","mcc":404,"mnc":56}]},{"type":"ZoneInfo","zoneId":6,"name":"2-Zone6","nickName":"2-Zone6","pricePerMB":0.9,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Thailand","code":"TH"}],"mccmncList":[{"type":"mccmnc","mcc":520,"mnc":1}]},{"type":"ZoneInfo","zoneId":7,"name":"2-Zone7","nickName":"2-Zone7","pricePerMB":0.09,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Malaysia","code":"MY"},{"type":"country","name":"South Africa","code":"ZA"}],"mccmncList":[{"type":"mccmnc","mcc":502,"mnc":19},{"type":"mccmnc","mcc":655,"mnc":7}]},{"type":"ZoneInfo","zoneId":8,"name":"2-Zone8","nickName":"2-Zone8","pricePerMB":0.04,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Colombia","code":"CO"}],"mccmncList":[{"type":"mccmnc","mcc":732,"mnc":101}]}]};
        var enterprise = {
            "type": "Account",
            "accountId": 2,
            "company": {
                "type": "Company",
                "name": "Uber",
                "phone": "8888888888",
                "currency": "USD",
                "timeZone": "+00:00",
                "timeZoneInfo": "American Samoa, Jarvis Island, Kingman Reef, Midway Atoll and Palmyra Atoll",
                "address": {
                    "type": "Address",
                    "address1": "c/o Uber Technologies, Inc",
                    "address2": "182 Howard Street # 8",
                    "state": "California",
                    "city": "San Francisco",
                    "zipCode": "94105",
                    "country": "US"
                }
            }
        };

        beforeEach(inject(function ($injector, $controller, $q, SettingsService, commonUtilityService, loadingState, AuthenticationService,$rootScope) {

            _scope = $rootScope.$new();
            _$q = $q;
            _commonUtilityService = commonUtilityService;
            _SettingsService = SettingsService;
            _authenticationService = AuthenticationService;
            _rootScope = $rootScope;

            spyOn(SettingsService,"getEnterpriseDetails").and.callFake(function(){
                var expectedResponse = enterprise;
                var d = $q.defer();
                d.resolve({data:expectedResponse});
                return d.promise;
            });

            spyOn(SettingsService,"getPlanDetails").and.callFake(function(){
                var expectedResponse = plans;
                var d = $q.defer();
                d.resolve({data:expectedResponse});
                return d.promise;
            });

            spyOn(_commonUtilityService, 'showErrorNotification');
            spyOn(_commonUtilityService, 'showSuccessNotification');

            PricingDetailsCtrl = $controller("PricingDetailsCtrl", {
                $scope: _scope,
                $q:$q,
                SettingsService:_SettingsService,
                commonUtilityService:_commonUtilityService,
                loadingState:loadingState,
                AuthenticationService:_authenticationService,
                $rootScope:_rootScope
            });

            _rootScope.$apply();

        }));

        it('should test _pricingDetails',function(){
            expect(PricingDetailsCtrl.zones).toBe(plans.list);
            expect(PricingDetailsCtrl.account.currency).toBe('$');
        });

        it('should test showSuccessMsg function', function () {
            var plansResponse = 'Zone Nickname updated successfully!';
            _scope.showSuccessMsg(plansResponse);

            expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith(plansResponse);
        });

        it('should test showErrMsg function', function () {
            var plansErrorResponse = {"errorStr":'Pricing details not available'};
            _scope.showErrMsg(plansErrorResponse.errorStr);

            expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith(plansErrorResponse.errorStr);
        });


        it('should test updateZone function', function () {

            expect(PricingDetailsCtrl.zones[0].zoneId).toBe(1);
            expect(PricingDetailsCtrl.zones[0].nickName).toBe('2-Zone1');

            _scope.updateZone(1,'testZone');

            expect(PricingDetailsCtrl.zones[0].zoneId).toBe(1);
          //  expect(PricingDetailsCtrl.zones[0].nickName).toBe('testZone');
        });

        describe('should test preparePricingInfoTable function', function () {
            /**
             * @func preparePricingInfoTable
             * @desc should prepare the pricing info table
             * should test :
             * 1. zone minimum commitment cost
             * 2. zone minimum commitment data
             * 3. zone pricing range
             * 4. committed data for the tier
             * 5. pricePerMb model
             * @param zonesList
             * @validInputs
             * 1. zonesList with empty list
             * 2. zonesList with and without bucketPricingInfo property
             * 3. bucketPricingInfo with or without minimumCommitment property
             * 4. bucketPricingInfo with list or empty list
             */

            var zoneList1 =[];

            var zoneList2 = [
                {
                    "type":"ZoneInfo","zoneId":55,"name":"5-Zone1","nickName":"5-zone1","lastUpdatedDate":"2016-12-05 13:41:56",pricePerMB:'0.4500',
                    "countryList":[{"type":"country","name":"United States","code":"US"}]
                },
                {
                    "type":"ZoneInfo","zoneId":56,"name":"5-Zone2","nickName":"5-zone2","lastUpdatedDate":"2016-12-05 13:41:56",pricePerMB:'2.4500',
                    "countryList":[{"type":"country","name":"United States","code":"US"}]
                }
            ];

            var zoneList3 = [
                {
                    "type":"ZoneInfo","zoneId":55,"name":"5-Zone1","nickName":"5-zone1","lastUpdatedDate":"2016-12-05 13:41:56",
                    "bucketPricingInfo": {
                        "minimumCommitment":6000.0,
                        "list":[
                            {"bucketId":1,"unitType":"MB","unitValue":1,"pricePerUnit":10.0,"startRange":0,"endRange":100,"validityStart":201611,"validityUpto":null,"status":"ACTIVE"},
                            {"bucketId":2,"unitType":"MB","unitValue":10,"pricePerUnit":25.0,"startRange":100,"endRange":500,"validityStart":201611,"validityUpto":null,"status":"ACTIVE"},
                            {"bucketId":3,"unitType":"MB","unitValue":4,"pricePerUnit":30.0,"startRange":500,"endRange":1024,"validityStart":201611,"validityUpto":null,"status":"ACTIVE"},
                            {"bucketId":4,"unitType":"GB","unitValue":1,"pricePerUnit":100.0,"startRange":1,"endRange":null,"validityStart":201611,"validityUpto":null,"status":"ACTIVE"}]},
                    "countryList":[{"type":"country","name":"United States","code":"US"}]
                },
                {
                    "type":"ZoneInfo","zoneId":56,"name":"5-Zone2","nickName":"5-zone2","lastUpdatedDate":"2016-12-05 13:41:56",
                    "bucketPricingInfo":{
                        "list":[
                            {"bucketId":5,"unitType":"MB","unitValue":1,"pricePerUnit":10.0,"startRange":0,"endRange":100,"validityStart":201611,"validityUpto":null,"status":"ACTIVE"},
                            {"bucketId":6,"unitType":"MB","unitValue":10,"pricePerUnit":25.0,"startRange":100,"endRange":500,"validityStart":201611,"validityUpto":null,"status":"ACTIVE"},
                            {"bucketId":7,"unitType":"MB","unitValue":4,"pricePerUnit":30.0,"startRange":500,"endRange":1024,"validityStart":201611,"validityUpto":null,"status":"ACTIVE"},
                            {"bucketId":8,"unitType":"GB","unitValue":1,"pricePerUnit":100.0,"startRange":1,"endRange":null,"validityStart":201611,"validityUpto":null,"status":"ACTIVE"}]},
                    "countryList":[{"type":"country","name":"United States","code":"US"}]
                }
            ];

            var zoneList4 = [
                {
                    "type":"ZoneInfo","zoneId":57,"name":"5-Zone3","nickName":"5-zone3","lastUpdatedDate":"2016-12-05 13:41:56",
                    "bucketPricingInfo":{
                        "minimumCommitment":550.0,
                        "list":[
                            {"bucketId":9,"unitType":"MB","unitValue":10,"pricePerUnit":50.0,"startRange":0,"endRange":100,"validityStart":201611,"validityUpto":null,"status":"ACTIVE"},
                            {"bucketId":10,"unitType":"MB","unitValue":20,"pricePerUnit":10.0,"startRange":100,"endRange":800,"validityStart":201611,"validityUpto":null,"status":"ACTIVE"},
                            {"bucketId":11,"unitType":"GB","unitValue":1,"pricePerUnit":100.0,"startRange":1,"endRange":null,"validityStart":201611,"validityUpto":null,"status":"ACTIVE"}]},
                    "countryList":[{"type":"country","name":"India","code":"IN"}]
                },
                {
                    "type":"ZoneInfo","zoneId":58,"name":"5-Zone4","nickName":"5-zone4","lastUpdatedDate":"2016-12-05 13:41:56",
                    "bucketPricingInfo":{
                        "minimumCommitment":10.0,
                        "list":[
                            {"bucketId":13,"unitType":"MB","unitValue":1,"pricePerUnit":10.0,"startRange":0,"endRange":100,"validityStart":201611,"validityUpto":null,"status":"ACTIVE"},
                            {"bucketId":14,"unitType":"MB","unitValue":10,"pricePerUnit":25.0,"startRange":100,"endRange":500,"validityStart":201611,"validityUpto":null,"status":"ACTIVE"},
                            {"bucketId":15,"unitType":"MB","unitValue":4,"pricePerUnit":30.0,"startRange":500,"endRange":1024,"validityStart":201611,"validityUpto":null,"status":"ACTIVE"},
                            {"bucketId":16,"unitType":"GB","unitValue":1,"pricePerUnit":100.0,"startRange":1,"endRange":null,"validityStart":201611,"validityUpto":null,"status":"ACTIVE"}]},
                    "countryList":[{"type":"country","name":"India","code":"IN"}]
                },
                {
                    "type":"ZoneInfo","zoneId":58,"name":"5-Zone4","nickName":"5-zone4","lastUpdatedDate":"2016-12-05 13:41:56",
                    "bucketPricingInfo":{
                        "minimumCommitment":10.0,
                        "list":[]
                    },
                    "countryList":[{"type":"country","name":"India","code":"IN"}]
                }


            ];


            it('should handle when zonesList is empty ', function () {

                PricingDetailsCtrl.zones = zoneList1;

                PricingDetailsCtrl.preparePricingInfoTable(zoneList1);
                expect(PricingDetailsCtrl.zones.length).toBe(0);
            });

            it('should handle when the bucketPricingInfo is absent', function () {

                PricingDetailsCtrl.zones = zoneList2;

                PricingDetailsCtrl.preparePricingInfoTable(zoneList2);
                expect(PricingDetailsCtrl.zones[0].bucket).toBeUndefined();
                expect(PricingDetailsCtrl.zones[0].pricePerMB).toBe('0.4500');
                expect(PricingDetailsCtrl.zones[1].bucket).toBeUndefined();
                expect(PricingDetailsCtrl.zones[1].pricePerMB).toBe('2.4500');

            });

            it('should handle the when the pricePerMb model', function () {
                _rootScope.pricingModelVersion = '1.1';
                PricingDetailsCtrl.zones = zoneList2;

                PricingDetailsCtrl.preparePricingInfoTable(zoneList2);
                expect(PricingDetailsCtrl.zones[0].bucket).toBeUndefined();
                expect(PricingDetailsCtrl.zones[0].pricePerMB).toBe('0.4500');
                expect(PricingDetailsCtrl.zones[1].bucket).toBeUndefined();
                expect(PricingDetailsCtrl.zones[1].pricePerMB).toBe('2.4500');

            });


            it('should verify the minimum commitment & committedData in the tier', function () {
                _rootScope.pricingModelVersion = '1.2';
                PricingDetailsCtrl.zones = zoneList3;

                PricingDetailsCtrl.preparePricingInfoTable(zoneList3);
                expect(PricingDetailsCtrl.zones[0].minimumCommitmentCost).toBe(6000.0);
                expect(PricingDetailsCtrl.zones[0].minimumCommitmentData).toBe('1.7 GB');

                expect(PricingDetailsCtrl.zones[0].buckets).toBeDefined();
                expect(PricingDetailsCtrl.zones[0].buckets.length).toBe(4);

                expect(PricingDetailsCtrl.zones[0].buckets[0].committedData).toBe('100 MB');
                expect(PricingDetailsCtrl.zones[0].buckets[1].committedData).toBe('400 MB');
                expect(PricingDetailsCtrl.zones[0].buckets[2].committedData).toBe('524 MB');
                expect(PricingDetailsCtrl.zones[0].buckets[3].committedData).toBe('716.8 MB');




            });

            it('should verify the minimum commitment & committedData in the tier when the minimum commitment is not present', function () {
                _rootScope.pricingModelVersion = '1.2';
                PricingDetailsCtrl.zones = zoneList3;

                PricingDetailsCtrl.preparePricingInfoTable(zoneList3);
                expect(PricingDetailsCtrl.zones[1].minimumCommitmentCost).toBeUndefined();
                expect(PricingDetailsCtrl.zones[1].minimumCommitmentData).toBe(0);

                expect(PricingDetailsCtrl.zones[1].buckets).toBeDefined();
                expect(PricingDetailsCtrl.zones[1].buckets.length).toBe(4);

                expect(PricingDetailsCtrl.zones[1].buckets[0].committedData).toBe('NA');
                expect(PricingDetailsCtrl.zones[1].buckets[1].committedData).toBe('NA');
                expect(PricingDetailsCtrl.zones[1].buckets[2].committedData).toBe('NA');
                expect(PricingDetailsCtrl.zones[1].buckets[3].committedData).toBe('NA');

            });

            it('should handle the partial committed data in the zone tier', function () {
                PricingDetailsCtrl.zones = zoneList4;

                PricingDetailsCtrl.preparePricingInfoTable(zoneList4);

                expect(PricingDetailsCtrl.zones[0].minimumCommitmentCost).toBe(550.0);
                expect(PricingDetailsCtrl.zones[0].minimumCommitmentData).toBe('200 MB');

                expect(PricingDetailsCtrl.zones[0].buckets[0].committedData).toBe('100 MB');
                expect(PricingDetailsCtrl.zones[0].buckets[1].committedData).toBe('100 MB');
                expect(PricingDetailsCtrl.zones[0].buckets[2].committedData).toBe('NA');

            });

            it('should test the pricing range of the buckets in the zone', function () {
                PricingDetailsCtrl.zones = zoneList4;

                PricingDetailsCtrl.preparePricingInfoTable(zoneList4);

                expect(PricingDetailsCtrl.zones[0].minimumCommitmentCost).toBe(550.0);
                expect(PricingDetailsCtrl.zones[0].minimumCommitmentData).toBe('200 MB');

                expect(PricingDetailsCtrl.zones[0].buckets[0].range).toBe('0 - 100 MB');
                expect(PricingDetailsCtrl.zones[0].buckets[1].range).toBe('100 MB - 800 MB');
                expect(PricingDetailsCtrl.zones[0].buckets[2].range).toBe('More than 1 GB');
            });

        });
    });



    /*describe('unsuccessful initialization of the controller', function () {

        var _scope, PricingDetailsCtrl, _$q, _SettingsService, _commonUtilityService, loadingState, _authenticationService, _rootScope;

        it('should handle when the getPlanDetails or getEnterpriseDetails fails', function () {

            var plansErrorResponse = {"errorStr":'Pricing details not available'};
            var enterpriseDetailsErrorResponse = {"errorStr":'Enterprise details not available'};

            beforeEach(module('app'));

            beforeEach(inject(function ($injector, $controller, $q, SettingsService, commonUtilityService, loadingState, AuthenticationService,$rootScope) {
                _scope = $rootScope.$new();
                _$q = $q;
                _commonUtilityService = commonUtilityService;
                _SettingsService = SettingsService;
                _authenticationService = AuthenticationService;
                _rootScope = $rootScope;


                spyOn(_SettingsService,"getEnterpriseDetails").and.callFake(function(){
                    var d = $q.defer();
                    d.reject({data:enterpriseDetailsErrorResponse});
                    return d.promise;
                });

                spyOn(_SettingsService,"getPlanDetails").and.callFake(function(){
                    var d = $q.defer();
                    d.reject({data:plansErrorResponse});
                    return d.promise;
                });

                spyOn(_commonUtilityService, 'showErrorNotification');
                spyOn(_commonUtilityService, 'showSuccessNotification');

                var PricingDetailsCtrl = $controller("PricingDetailsCtrl", {
                    $scope: _scope,
                    $q:$q,
                    SettingsService:_SettingsService,
                    commonUtilityService:_commonUtilityService,
                    loadingState:loadingState,
                    AuthenticationService:_authenticationService,
                    $rootScope:_rootScope
                });

                _rootScope.$apply();
            }));



            expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith(plansErrorResponse.errorStr);
        });

    });*/



});