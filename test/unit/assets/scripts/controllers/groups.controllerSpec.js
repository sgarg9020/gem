describe('GroupsCtrl', function() {

    var _commonUtilityService, _authenticationService,_groupsService;
    var group = {
        "type": "Account",
        "accountId": 3,
        "accountType": "GROUP",
        "accountName": "UberIndia",
        "parentAccountId": 2,
        "accountSubscriptionType": "ENTERPRISE_PRO",
        "company": {
            "type": "Company",
            "name": "Uber",
            "phone": "8888888888",
            "currency": "USD",
            "timeZone": "+00:00",
            "timeZoneInfo": "American Samoa, Jarvis Island, Kingman Reef, Midway Atoll and Palmyra Atoll",
            "address": {
                "type": "Address",
                "address1": "c/o Uber Technologies, Inc",
                "address2": "182 Howard Street # 8",
                "state": "California",
                "city": "San Francisco",
                "zipCode": "94105",
                "country": "US"
            }
        },
        "accountDescription": "Uber India Sub Account"
    };
    var groups = {"type":"AccountList","list":[{"type":"Account","accountId":90336,"accountType":"GROUP","accountName":"Test","parentAccountId":2},{"type":"Account","accountId":3,"accountType":"GROUP","accountName":"UberIndia","parentAccountId":2,"accountSubscriptionType":"ENTERPRISE_PRO","company":{"type":"Company","name":"Uber","phone":"8888888888","currency":"USD","timeZone":"+00:00","timeZoneInfo":"American Samoa, Jarvis Island, Kingman Reef, Midway Atoll and Palmyra Atoll","address":{"type":"Address","address1":"c/o Uber Technologies, Inc","address2":"182 Howard Street # 8","state":"California","city":"San Francisco","zipCode":"94105","country":"US"}},"accountDescription":"Uber India Sub Account"}],"accountType":"ENTERPRISE","accountName":"Uber","accountDescription":"Uber Enterprise Account","parentAccountId":1,"accountSubscriptionType":"ENTERPRISE_PRO","alertVersionSupported":"1.1","accountId":2,"count":2,"totalCount":2};

    beforeEach(module('app'));

    var GroupsCtrl;
    var scope = {};
    beforeEach(inject(function($injector,$controller, $rootScope, $compile, $timeout,GroupsService,commonUtilityService,dataTableConfigService,AuthenticationService){

        GroupsCtrl = $controller("GroupsCtrl",{$scope:scope, $rootScope:$rootScope, $compile:$compile, $timeout:$timeout,GroupsService:GroupsService,commonUtilityService:commonUtilityService, dataTableConfigService:dataTableConfigService,AuthenticationService:AuthenticationService});

        _commonUtilityService = commonUtilityService;
        _authenticationService = AuthenticationService;
        _groupsService = GroupsService;
        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
        scope.dataTableObj.refreshDataTable = jasmine.createSpy('refreshDataTable spy method');
        scope.groupModalObj.resetGroupModal = jasmine.createSpy('resetGroupModal spy method');
    }));

    it('Should test data-table columnDefs 0', function () {
        var data, type, row, meta;
        meta = {
            row: 0
        };
        row = group;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[0].render(data, type, row, meta)).toContain(row.accountId);
    });

    it('Should test data-table columnDefs 1', function () {
        var data, type, row, meta;
        meta = {
            row: 1
        };
        row = group;
        spyOn(_authenticationService, 'isOperationAllowed').and.returnValue(true);
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data, type, row, meta)).toContain(row.accountName);
    });

    it('Should test data-table columnDefs 2', function () {
        var data, type, row, meta;
        meta = {
            row: 2
        };
        row = group;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data, type, row, meta)).toContain(row.accountId);
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data, type, row, meta)).toContain(row.accountName);
    });

    it('Should test fnServerData', function () {
        var sSource, aoData, fnCallback;
        var _mockPromise = {
            success: function (fn) {
                fn(groups);
                return _mockPromise;
            },
            error: function (fn) {
                fn(groups);
                return _mockPromise;
            }
        };
        spyOn(_groupsService, 'getGroups').and.returnValue(_mockPromise);
        scope.dataTableObj.dataTableRef = {};
        scope.dataTableObj.dataTableRef.fnSettings = function () {
        };

        scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
        expect(scope.groupsList.length).toBe(groups.list.length);
        expect(scope.isDisableComponent()).toBe(false);
    });

    it('Should test fnServerData when group list is empty', function () {
        var sSource, aoData, fnCallback;
        var response = {
            "type": "AccountList",
            "list" : [] ,
            "count": 0,
            "totalCount": 0};
        var _mockPromise = {
            success: function (fn) {
                fn(response);
                return _mockPromise;
            },
            error: function (fn) {
                fn(response);
                return _mockPromise;
            }
        };
        spyOn(_groupsService, 'getGroups').and.returnValue(_mockPromise);
        scope.dataTableObj.dataTableRef = {};
        scope.dataTableObj.dataTableRef.fnSettings = function () {
        };

        scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
        expect(scope.groupsList.length).toBe(0);
        expect(scope.isDisableComponent()).toBe(true);
    });

    it('Should test checkBoxClicked',function(){
        var event = {};
        scope.dataTableObj.selectRow = jasmine.createSpy('selectRow spy method');
        scope.checkBoxClicked(event);

        expect(scope.dataTableObj.selectRow).toHaveBeenCalled();

    });

    it('Should test checkBoxClicked',function(){
        var event = {};
        scope.dataTableObj.selectRow = jasmine.createSpy('selectRow spy method');
        scope.checkBoxClicked(event);

        expect(scope.dataTableObj.selectRow).toHaveBeenCalled();

    });



    it('modal should have Add New Group title message',function(){
        //Launch add group modal
        scope.groupModalObj.updateGroupModal = function(group,attr, cbk){
            expect(group).toBeNull();
            expect(attr.header).toBe("Add New Group");
            expect(attr.submit).toBe("Add");
            if(!(cbk instanceof Function))
                expect(false).toBeTruthy();
            scope.onAddCbk = cbk;
        };

        GroupsCtrl.addGroup();
    });

    it('should test update group modal',function(){
        //Launch update group modal
        scope.groupModalObj.updateGroupModal = function(group,attr, cbk){
            expect(typeof group).toBe("object");
            expect(attr.header).toBe("Update Group");
            expect(attr.submit).toBe("Update");
            if(!(cbk instanceof Function))
                expect(false).toBeTruthy();
            scope.onAddCbk = cbk;
        };
        scope.groupsList = groups.list;
        scope.editGroup(1);
    });

    it('should test verifyDeletion',function(){
        scope.verificationModalObj.updateVerificationModel = function(attr, cbk){
            expect(attr.header).toBe("Confirm Deletion");
            expect(attr.submit).toBe("Delete");
            expect(attr.cancel).toBe("Cancel");
            if(!(cbk instanceof Function))
                expect(false).toBeTruthy();
        };

        scope.verificationModalObj.addVerificationModelMessage = function(msg){
            expect(msg).toBe('Deleting groups cannot be undone! All SIMs and Users associated with deleted groups will be disassociated.');
        }
        GroupsCtrl.verifyDeletion(groups.list);
    });

    it('should test processTableRowDeletion when row=1 and row > 1  ',function(){
        var expectedResponse ={}
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_groupsService, 'deleteGroups').and.returnValue(_mockPromise);

        scope.processTableRowDeletion([group]);
        expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Selected group has been deleted.');

        scope.processTableRowDeletion([group,group,group]);
        expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('All selected groups have been deleted.');
    });

    it('Should test onAddNewGroup', function () {
        var expectedResponse ={}
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_groupsService, 'addGroup').and.callFake(function(){
            expect(arguments[0].type).toEqual('AccountList');
            expect(arguments[0].list[0].type).toEqual('Account');
            return _mockPromise;
        });
        scope.onAddNewGroup(group);
        expect(scope.groupModalObj.resetGroupModal).toHaveBeenCalled();
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith("Group "+group.accountName+ " has been created.");
    });

    it('Should test onEditExistingGroup', function () {
        var expectedResponse ={}
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_groupsService, 'updateGroup').and.returnValue(_mockPromise);
        scope.currentGroup = group;
        group.accountName = "XYZ";
        scope.onEditExistingGroup(group,"UberIndia");
        expect(scope.groupModalObj.resetGroupModal).toHaveBeenCalled();
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith("Group " + group.accountName + " has been updated successfully.");
    });

    it('Should test onEditExistingGroup (Without changing group name)', function () {
        var expectedResponse ={}
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_groupsService, 'updateGroup').and.returnValue(_mockPromise);

        scope.currentGroup = group;
        scope.onEditExistingGroup(group,"XYZ");
        expect(scope.groupModalObj.resetGroupModal).toHaveBeenCalled();
        expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith("New group name cannot be the same as existing group name.");
    });


});