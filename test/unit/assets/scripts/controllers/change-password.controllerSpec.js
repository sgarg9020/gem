describe('Profile Controller', function() {

    var _commonUtilityService,_SIMService,ChangePasswordCtrl,scope,_AccountService,_UserService,_AuthenticationService;


    beforeEach(module('app'));


    beforeEach(inject(function($injector,$controller,$location,commonUtilityService,loadingState,AccountService,AuthenticationService){
        scope ={};

        ChangePasswordCtrl = $controller("ChangePasswordCtrl",{$scope:scope,$location:$location,commonUtilityService:commonUtilityService,loadingState:loadingState,AccountService:AccountService,AuthenticationService:AuthenticationService});

        _commonUtilityService = commonUtilityService;
        _AccountService = AccountService;
        _AuthenticationService = AuthenticationService;
        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
    }));

    it('updatePW updatePassword',function(){
        var expectedResponse ={"firstName":"Jagadish","lastName":"Dande","emailId":"jdande@gigsky.com","country":null,"userId":"561771933b27e844933060c6"};
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_AccountService,"updatePassword").and.returnValue(_mockPromise);

        var expectedOkResponse ="OK";
        var _mockOkPromise = {
            success: function(fn) {
                fn(expectedOkResponse);
                return _mockOkPromise;
            },
            error: function(fn) {
                fn(expectedOkResponse);
                return _mockOkPromise;
            }
        };
        spyOn(_AuthenticationService,"enterpriseLogin").and.returnValue(_mockOkPromise);
        scope.isFormValid = function(){
            return true;
        };
        ChangePasswordCtrl.updatePW.updatePassword();
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith("Password has been changed successfully.");
        //expect(_commonUtilityService.showErrorNotification).toHaveBeenCalled();
    });
});