describe('Pricing Controller', function() {

    var PricingCtrl, scope, rootScope;

    beforeEach(module('app'));

    var plans = {"type":"AccountZoneList","totalCount":8,"count":8,"list":[{"type":"ZoneInfo","zoneId":1,"name":"2-Zone1","nickName":"2-Zone1","pricePerMB":0.01,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"},{"type":"country","name":"United Kingdom","code":"GB"},{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":310,"mnc":170},{"type":"mccmnc","mcc":310,"mnc":260},{"type":"mccmnc","mcc":404,"mnc":31},{"type":"mccmnc","mcc":405,"mnc":46},{"type":"mccmnc","mcc":234,"mnc":15}]},{"type":"ZoneInfo","zoneId":2,"name":"2-Zone2","nickName":"2-Zone2","pricePerMB":0.1,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Singapore","code":"SG"},{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":525,"mnc":5},{"type":"mccmnc","mcc":310,"mnc":250},{"type":"mccmnc","mcc":310,"mnc":240}]},{"type":"ZoneInfo","zoneId":3,"name":"2-Zone3","nickName":"2-Zone3","pricePerMB":0.45,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"United States","code":"US"}],"mccmncList":[{"type":"mccmnc","mcc":310,"mnc":380},{"type":"mccmnc","mcc":310,"mnc":680},{"type":"mccmnc","mcc":310,"mnc":410}]},{"type":"ZoneInfo","zoneId":4,"name":"2-Zone4","nickName":"2-Zone4","pricePerMB":1.01,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"}],"mccmncList":[{"type":"mccmnc","mcc":405,"mnc":809},{"type":"mccmnc","mcc":405,"mnc":808},{"type":"mccmnc","mcc":405,"mnc":804},{"type":"mccmnc","mcc":405,"mnc":807},{"type":"mccmnc","mcc":405,"mnc":805},{"type":"mccmnc","mcc":404,"mnc":25},{"type":"mccmnc","mcc":404,"mnc":28}]},{"type":"ZoneInfo","zoneId":5,"name":"2-Zone5","nickName":"2-Zone5","pricePerMB":0.75,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"India","code":"IN"}],"mccmncList":[{"type":"mccmnc","mcc":404,"mnc":37},{"type":"mccmnc","mcc":404,"mnc":91},{"type":"mccmnc","mcc":404,"mnc":42},{"type":"mccmnc","mcc":404,"mnc":13},{"type":"mccmnc","mcc":404,"mnc":17},{"type":"mccmnc","mcc":404,"mnc":41},{"type":"mccmnc","mcc":404,"mnc":56}]},{"type":"ZoneInfo","zoneId":6,"name":"2-Zone6","nickName":"2-Zone6","pricePerMB":0.9,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Thailand","code":"TH"}],"mccmncList":[{"type":"mccmnc","mcc":520,"mnc":1}]},{"type":"ZoneInfo","zoneId":7,"name":"2-Zone7","nickName":"2-Zone7","pricePerMB":0.09,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Malaysia","code":"MY"},{"type":"country","name":"South Africa","code":"ZA"}],"mccmncList":[{"type":"mccmnc","mcc":502,"mnc":19},{"type":"mccmnc","mcc":655,"mnc":7}]},{"type":"ZoneInfo","zoneId":8,"name":"2-Zone8","nickName":"2-Zone8","pricePerMB":0.04,"lastUpdatedDate":"2016-08-09 05:07:52","countryList":[{"type":"country","name":"Colombia","code":"CO"}],"mccmncList":[{"type":"mccmnc","mcc":732,"mnc":101}]}]};
    var enterprise = {
        "type": "Account",
        "accountId": 2,
        "company": {
            "type": "Company",
            "name": "Uber",
            "phone": "8888888888",
            "currency": "USD",
            "timeZone": "+00:00",
            "timeZoneInfo": "American Samoa, Jarvis Island, Kingman Reef, Midway Atoll and Palmyra Atoll",
            "address": {
                "type": "Address",
                "address1": "c/o Uber Technologies, Inc",
                "address2": "182 Howard Street # 8",
                "state": "California",
                "city": "San Francisco",
                "zipCode": "94105",
                "country": "US"
            }
        }
    };

    beforeEach(inject(function ($rootScope,$injector, $controller, $compile, commonUtilityService, SettingsService, $q, $rootScope) {
        scope = $rootScope.$new();

        spyOn(SettingsService,"getEnterpriseDetails").and.callFake(function(){
            var expectedResponse = enterprise;
            var d = $q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });

        spyOn(SettingsService,"getPlanDetails").and.callFake(function(){
            var expectedResponse = plans;
            var d = $q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });

        PricingCtrl = $controller("PricingCtrl", {$scope: scope, $compile: $compile,SettingsService:SettingsService, $rootScope:$rootScope});
        rootScope = $rootScope;
        rootScope.$apply();


    }));

    it('should test _pricingDetails',function(){
        expect(PricingCtrl.zones).toBe(plans.list);
        expect(PricingCtrl.account.currency).toBe('$');
    });

});