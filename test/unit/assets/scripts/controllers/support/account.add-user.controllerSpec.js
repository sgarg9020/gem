describe('Add user controller', function () {

    var  gapAddUserCtrl,scope,_$sce, _modalDetails,_formMessage,_$q,_commonUtilityService,_$controller;

    var accountDetails = {
        "type": "EnterpriseAccountList",
        "enterpriseAccountDetails": [{
            "type": "Account",
            "rootAccountDetails": {
                "type": "Account",
                "accountId": 2,
                "accountType": "ENTERPRISE",
                "accountName": "Uber"
            },
            "parentAccountDetails": [{
                "type": "Account",
                "accountId": 2,
                "accountType": "ENTERPRISE",
                "accountName": "Uber",
                "role": "ENTERPRISE_ADMIN"
            }],
            "selfAccountDetails": {
                "type": "Account",
                "accountId": 22,
                "accountType": "USER"
            }
        }]
    };

    var user = {"type": "User", "userId": 2, "firstName": "uberAdmin", "lastName": "uberAdmin", "emailId": "uberAdmin@uber.com", "location": "Palo-Alto","address": {"type": "Address", "addressId": 2, "address1": "Electronics city", "address2": "Hosur Road", "state": "Karnataka", "city": "Bangalore", "zipCode": "560093", "country": "IN"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 08:28:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": accountDetails, "totalSimsAssociated": "14", "dataUsedInBytes": 0};

    beforeEach(module('app'));

    beforeEach(inject(function ($rootScope,$injector, $controller, commonUtilityService, $sce,$q,modalDetails,formMessage) {
        _$controller=$controller;
        _$sce=$sce;
        _$q=$q;
        scope = {};
        _commonUtilityService=commonUtilityService;
        _modalDetails=modalDetails;
        _formMessage = formMessage;
        scope.$parent={};
        scope.$parent.gapUserModalObj={};
        scope.addUser={};
        scope.addUser.modal ={id: 'addOrEditUserModal'};
        scope.addUser.user = {};
        scope.generalObj = {};

        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');

    }));

    it('should test gapAddUserCtrl initialization', function () {
        spyOn(_modalDetails, 'scope');
        spyOn(_modalDetails, 'attributes');
        spyOn(_modalDetails, 'cancel');
        spyOn(_modalDetails, 'submit');

        gapAddUserCtrl = _$controller('GapAddUserCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

        expect(_modalDetails.scope).toHaveBeenCalledWith(gapAddUserCtrl,true);
        expect(_modalDetails.attributes).toHaveBeenCalledWith({
            id: 'addOrEditUserModal',
            formName: 'addOrEditUser',
            cancel: 'Cancel',
            submit: 'Add User'
        });

        expect(_modalDetails.cancel).toHaveBeenCalled();

    });

    it('should verify the trustAsHtml function', function () {
        gapAddUserCtrl = _$controller('GapAddUserCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

        gapAddUserCtrl.trustAsHtml();

        var actualMsg = _$sce.getTrustedHtml('USER');
        expect(actualMsg).toBe('USER');

    });

    describe('it should test updateModal function', function () {

        it('should test for add user modal', function () {

            gapAddUserCtrl = _$controller('GapAddUserCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

            spyOn(_modalDetails, 'scope');
            spyOn(_modalDetails, 'attributes');

            scope.isFormValid = function(){
                return true;
            };

            spyOn(_modalDetails,'submit').and.callFake(function(fn){
                fn();
            });

            var rolesList = ['ENTERPRISE_ADMIN', 'ENTERPRISE_USER'];
            var modalObj = {
                header: "Add New User",
                submit: "Add"
            };
            var enterpriseId =2;

            scope.$parent.gapUserModalObj.updateUserModal(null,enterpriseId,rolesList,modalObj, function(){});

            expect(_modalDetails.scope).toHaveBeenCalledWith(gapAddUserCtrl,false);
            expect(_modalDetails.attributes).toHaveBeenCalledWith(modalObj);

            expect(gapAddUserCtrl.errorMessage).toBe('');
            expect(scope.generalObj.previousData).toBeNull();

        });

        it('should test for add user modal when rolesList is not present', function () {

            gapAddUserCtrl = _$controller('GapAddUserCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

            spyOn(_modalDetails, 'scope');
            spyOn(_modalDetails, 'attributes');

            scope.isFormValid = function(){
                return true;
            };

            spyOn(_modalDetails,'submit').and.callFake(function(fn){
                fn()
            });

            var modalObj = {
                header: "Add New User",
                submit: "Add"
            };
            var enterpriseId =2;

            scope.$parent.gapUserModalObj.updateUserModal(null,enterpriseId,null,modalObj, function(){});

            expect(_modalDetails.scope).toHaveBeenCalledWith(gapAddUserCtrl,false);
            expect(_modalDetails.attributes).toHaveBeenCalledWith(modalObj);

            expect(gapAddUserCtrl.errorMessage).toBe('Unable to process the request. Please reload the page.');
            expect(scope.generalObj.previousData).toBeNull();

        });

        it('should test for update user modal', function () {

            gapAddUserCtrl = _$controller('GapAddUserCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

            spyOn(_modalDetails, 'scope');
            spyOn(_modalDetails, 'attributes');

            scope.isFormValid = function(){
                return true;
            };

            spyOn(_modalDetails,'submit').and.callFake(function(fn){
                fn();
            });

            var rolesList = ['ENTERPRISE_ADMIN', 'ENTERPRISE_USER'];
            var modalObj = {
                header: "Add New User",
                submit: "Add"
            };
            var enterpriseId =2;

            scope.$parent.gapUserModalObj.updateUserModal(user,enterpriseId,rolesList,modalObj, function(){});

            expect(_modalDetails.scope).toHaveBeenCalledWith(gapAddUserCtrl,false);
            expect(_modalDetails.attributes).toHaveBeenCalledWith(modalObj);

            expect(gapAddUserCtrl.errorMessage).toBe('');
            //expect(scope.generalObj.previousData).toEqual(user);

        });

        it('should test for update user modal when there is no change in user data', function () {

            gapAddUserCtrl = _$controller('GapAddUserCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

            spyOn(_modalDetails, 'scope');
            spyOn(_modalDetails, 'attributes');

            scope.isFormValid = function(){
                return false;
            };

            spyOn(_modalDetails,'submit').and.callFake(function(fn){
                fn();
            });

            var rolesList = ['ENTERPRISE_ADMIN', 'ENTERPRISE_USER'];
            var modalObj = {
                header: "Add New User",
                submit: "Add"
            };
            var enterpriseId =2;
            scope.generalObj.previousData = user;
            scope.generalObj.isNoChangeError = true;

            expect(scope.generalObj.isNoChangeError).toBeTruthy();

            scope.$parent.gapUserModalObj.updateUserModal(user,enterpriseId,rolesList,modalObj, function(){});

            expect(_modalDetails.scope).toHaveBeenCalledWith(gapAddUserCtrl,false);
            expect(_modalDetails.attributes).toHaveBeenCalledWith(modalObj);

            expect(gapAddUserCtrl.errorMessage).toBe('There is no change in the data to update.');
            expect(scope.generalObj.isNoChangeError).toBeFalsy();
            //expect(scope.generalObj.previousData).toEqual(user);
        });

    });

    it('should test resetUserModal function ', function () {

        gapAddUserCtrl = _$controller('GapAddUserCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

        spyOn(_formMessage, 'message');
        spyOn(_formMessage, 'scope');

        scope.$parent.gapUserModalObj.resetUserModal();

        expect(gapAddUserCtrl.user).toEqual({});
        expect(_formMessage.scope).toHaveBeenCalledWith(gapAddUserCtrl);
        expect(_formMessage.message).toHaveBeenCalledWith( false, '');

    });


    it('should test updateData function when the user is admin of an enterprise', function () {

        gapAddUserCtrl = _$controller('GapAddUserCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

        var enterpiseId = 2;
        gapAddUserCtrl.updateData(user,enterpiseId);

        expect(gapAddUserCtrl.user.userId).toBe(user.userId);
        expect(gapAddUserCtrl.user.firstName).toBe(user.firstName);
        expect(gapAddUserCtrl.user.lastName).toBe(user.lastName);
        expect(gapAddUserCtrl.user.emailId).toBe(user.emailId);
        expect(gapAddUserCtrl.user.roleName).toBe('ADMIN');
        expect(gapAddUserCtrl.user.location).toBe(user.location);
    });

    it('should test updateData function when the user is user of an enterprise', function () {

        var accountDetails = {
            "type": "EnterpriseAccountList",
            "enterpriseAccountDetails": [{
                "type": "Account",
                "rootAccountDetails": {
                    "type": "Account",
                    "accountId": 2,
                    "accountType": "ENTERPRISE",
                    "accountName": "Uber"
                },
                "parentAccountDetails": [{
                    "type": "Account",
                    "accountId": 2,
                    "accountType": "ENTERPRISE",
                    "accountName": "Uber",
                    "role": "ENTERPRISE_USER"
                }],
                "selfAccountDetails": {
                    "type": "Account",
                    "accountId": 22,
                    "accountType": "USER"
                }
            }]
        };

        var user = {"type": "User", "userId": 2, "firstName": "user1", "lastName": "user", "emailId": "uber1@uber.com", "location": "Palo-Alto","address": {"type": "Address", "addressId": 2, "address1": "Electronics city", "address2": "Hosur Road", "state": "Karnataka", "city": "Bangalore", "zipCode": "560093", "country": "IN"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 08:28:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": accountDetails, "totalSimsAssociated": "14", "dataUsedInBytes": 0};


        gapAddUserCtrl = _$controller('GapAddUserCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

        var enterpiseId = 2;
        gapAddUserCtrl.updateData(user,enterpiseId);

        expect(gapAddUserCtrl.user.userId).toBe(user.userId);
        expect(gapAddUserCtrl.user.firstName).toBe(user.firstName);
        expect(gapAddUserCtrl.user.lastName).toBe(user.lastName);
        expect(gapAddUserCtrl.user.emailId).toBe(user.emailId);
        expect(gapAddUserCtrl.user.roleName).toBe('USER');
        expect(gapAddUserCtrl.user.location).toBe(user.location);
    });


    it('should test updateData function when the user is admin of an enterprise and member of a group', function () {

        var accountDetails = {
            "type": "EnterpriseAccountList",
            "enterpriseAccountDetails": [{
                "type": "Account",
                "rootAccountDetails": {
                    "type": "Account",
                    "accountId": 2,
                    "accountType": "ENTERPRISE",
                    "accountName": "Uber"
                },
                "parentAccountDetails": [{
                    "type": "Account",
                    "accountId": 2,
                    "accountType": "ENTERPRISE",
                    "accountName": "Uber",
                    "role": "ENTERPRISE_ADMIN"
                },{
                    "type": "Account",
                    "accountId": 3,
                    "accountType": "GROUP",
                    "accountName": "Uber",
                    "role": "ENTERPRISE_USER"
                }
                ],
                "selfAccountDetails": {
                    "type": "Account",
                    "accountId": 22,
                    "accountType": "USER"
                }
            }]
        };

        var user = {"type": "User", "userId": 2, "firstName": "user1", "lastName": "user", "emailId": "uber1@uber.com", "location": "Palo-Alto","address": {"type": "Address", "addressId": 2, "address1": "Electronics city", "address2": "Hosur Road", "state": "Karnataka", "city": "Bangalore", "zipCode": "560093", "country": "IN"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 08:28:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": accountDetails, "totalSimsAssociated": "14", "dataUsedInBytes": 0};


        gapAddUserCtrl = _$controller('GapAddUserCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

        var enterpiseId = 2;
        gapAddUserCtrl.updateData(user,enterpiseId);

        expect(gapAddUserCtrl.user.userId).toBe(user.userId);
        expect(gapAddUserCtrl.user.firstName).toBe(user.firstName);
        expect(gapAddUserCtrl.user.lastName).toBe(user.lastName);
        expect(gapAddUserCtrl.user.emailId).toBe(user.emailId);
        expect(gapAddUserCtrl.user.roleName).toBe('ADMIN');
        expect(gapAddUserCtrl.user.location).toBe(user.location);
    });

    it('should test updateData function when the user is user of an enterprise and admin of a group', function () {

        var accountDetails = {
            "type": "EnterpriseAccountList",
            "enterpriseAccountDetails": [{
                "type": "Account",
                "rootAccountDetails": {
                    "type": "Account",
                    "accountId": 2,
                    "accountType": "ENTERPRISE",
                    "accountName": "Uber"
                },
                "parentAccountDetails": [{
                    "type": "Account",
                    "accountId": 2,
                    "accountType": "ENTERPRISE",
                    "accountName": "Uber",
                    "role": "ENTERPRISE_USER"
                },{
                    "type": "Account",
                    "accountId": 3,
                    "accountType": "GROUP",
                    "accountName": "Uber",
                    "role": "ENTERPRISE_ADMIN"
                }
                ],
                "selfAccountDetails": {
                    "type": "Account",
                    "accountId": 22,
                    "accountType": "USER"
                }
            }]
        };

        var user = {"type": "User", "userId": 2, "firstName": "user1", "lastName": "user", "emailId": "uber1@uber.com", "location": "Palo-Alto","address": {"type": "Address", "addressId": 2, "address1": "Electronics city", "address2": "Hosur Road", "state": "Karnataka", "city": "Bangalore", "zipCode": "560093", "country": "IN"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 08:28:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": accountDetails, "totalSimsAssociated": "14", "dataUsedInBytes": 0};


        gapAddUserCtrl = _$controller('GapAddUserCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

        var enterpiseId = 2;
        gapAddUserCtrl.updateData(user,enterpiseId);

        expect(gapAddUserCtrl.user.userId).toBe(user.userId);
        expect(gapAddUserCtrl.user.firstName).toBe(user.firstName);
        expect(gapAddUserCtrl.user.lastName).toBe(user.lastName);
        expect(gapAddUserCtrl.user.emailId).toBe(user.emailId);
        expect(gapAddUserCtrl.user.roleName).toBe('USER');
        expect(gapAddUserCtrl.user.location).toBe(user.location);
    });


    it('should test updateData function when the user is user of an enterprise and a group', function () {

        var accountDetails = {
            "type": "EnterpriseAccountList",
            "enterpriseAccountDetails": [{
                "type": "Account",
                "rootAccountDetails": {
                    "type": "Account",
                    "accountId": 2,
                    "accountType": "ENTERPRISE",
                    "accountName": "Uber"
                },
                "parentAccountDetails": [{
                    "type": "Account",
                    "accountId": 2,
                    "accountType": "ENTERPRISE",
                    "accountName": "Uber",
                    "role": "ENTERPRISE_USER"
                },{
                    "type": "Account",
                    "accountId": 3,
                    "accountType": "GROUP",
                    "accountName": "Uber",
                    "role": "ENTERPRISE_USER"
                }
                ],
                "selfAccountDetails": {
                    "type": "Account",
                    "accountId": 22,
                    "accountType": "USER"
                }
            }]
        };

        var user = {"type": "User", "userId": 2, "firstName": "user1", "lastName": "user", "emailId": "uber1@uber.com", "location": "Palo-Alto","address": {"type": "Address", "addressId": 2, "address1": "Electronics city", "address2": "Hosur Road", "state": "Karnataka", "city": "Bangalore", "zipCode": "560093", "country": "IN"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 08:28:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": accountDetails, "totalSimsAssociated": "14", "dataUsedInBytes": 0};


        gapAddUserCtrl = _$controller('GapAddUserCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

        var enterpiseId = 2;
        gapAddUserCtrl.updateData(user,enterpiseId);

        expect(gapAddUserCtrl.user.userId).toBe(user.userId);
        expect(gapAddUserCtrl.user.firstName).toBe(user.firstName);
        expect(gapAddUserCtrl.user.lastName).toBe(user.lastName);
        expect(gapAddUserCtrl.user.emailId).toBe(user.emailId);
        expect(gapAddUserCtrl.user.roleName).toBe('USER');
        expect(gapAddUserCtrl.user.location).toBe(user.location);
    });


    it('should test updateData function when the user is admin of multiple enterprises', function () {

        var accountDetails = {
            "type": "EnterpriseAccountList",
            "enterpriseAccountDetails": [{
                "type": "Account",
                "rootAccountDetails": {
                    "type": "Account",
                    "accountId": 2,
                    "accountType": "ENTERPRISE",
                    "accountName": "Uber"
                },
                "parentAccountDetails": [{
                    "type": "Account",
                    "accountId": 2,
                    "accountType": "ENTERPRISE",
                    "accountName": "Uber",
                    "role": "ENTERPRISE_ADMIN"
                },{
                    "type": "Account",
                    "accountId": 3,
                    "accountType": "ENTERPRISE",
                    "accountName": "Uber",
                    "role": "ENTERPRISE_ADMIN"
                }
                ],
                "selfAccountDetails": {
                    "type": "Account",
                    "accountId": 22,
                    "accountType": "USER"
                }
            }]
        };

        var user = {"type": "User", "userId": 2, "firstName": "user1", "lastName": "user", "emailId": "uber1@uber.com", "location": "Palo-Alto","address": {"type": "Address", "addressId": 2, "address1": "Electronics city", "address2": "Hosur Road", "state": "Karnataka", "city": "Bangalore", "zipCode": "560093", "country": "IN"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 08:28:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": accountDetails, "totalSimsAssociated": "14", "dataUsedInBytes": 0};


        gapAddUserCtrl = _$controller('GapAddUserCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

        var enterpiseId = 2;
        gapAddUserCtrl.updateData(user,enterpiseId);

        expect(gapAddUserCtrl.user.userId).toBe(user.userId);
        expect(gapAddUserCtrl.user.firstName).toBe(user.firstName);
        expect(gapAddUserCtrl.user.lastName).toBe(user.lastName);
        expect(gapAddUserCtrl.user.emailId).toBe(user.emailId);
        expect(gapAddUserCtrl.user.roleName).toBe('ADMIN');
        expect(gapAddUserCtrl.user.location).toBe(user.location);
    });

    it('should test updateData function when the role is not present', function () {

        var accountDetails = {
            "type": "EnterpriseAccountList",
            "enterpriseAccountDetails": [{
                "type": "Account",
                "rootAccountDetails": {
                    "type": "Account",
                    "accountId": 2,
                    "accountType": "ENTERPRISE",
                    "accountName": "Uber"
                },
                "parentAccountDetails": [],
                "selfAccountDetails": {
                    "type": "Account",
                    "accountId": 22,
                    "accountType": "USER"
                }
            }]
        };

        var user = {"type": "User", "userId": 2, "firstName": "user1", "lastName": "user", "emailId": "uber1@uber.com", "location": "Palo-Alto","address": {"type": "Address", "addressId": 2, "address1": "Electronics city", "address2": "Hosur Road", "state": "Karnataka", "city": "Bangalore", "zipCode": "560093", "country": "IN"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 08:28:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": accountDetails, "totalSimsAssociated": "14", "dataUsedInBytes": 0};


        gapAddUserCtrl = _$controller('GapAddUserCtrl',{$scope: scope,$sce:_$sce,modalDetails:_modalDetails,formMessage:_formMessage,$q:_$q,commonUtilityService:_commonUtilityService});

        var enterpiseId = 2;
        gapAddUserCtrl.updateData(user,enterpiseId);

        expect(gapAddUserCtrl.user.userId).toBe(user.userId);
        expect(gapAddUserCtrl.user.firstName).toBe(user.firstName);
        expect(gapAddUserCtrl.user.lastName).toBe(user.lastName);
        expect(gapAddUserCtrl.user.emailId).toBe(user.emailId);
        expect(gapAddUserCtrl.user.roleName).toBe('USER');
        expect(gapAddUserCtrl.user.location).toBe(user.location);
    });


});