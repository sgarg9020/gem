describe('Search Controller', function() {

    var SearchCtrl, scope, _dataTableConfigService, _authenticationService;

    beforeEach(module('app'));

    var enterprise = {
        "currency" : "USD",
        "country" : "US",
        "accountName": "AirIndia",
        "accountDescription": "Air India Enterprise Account",
        "simList":[{
            "iccId" : "1211000000000000181",
            "imsi" :  "6767676",
            "msisdn" : "45454545"
        },
            {
                "iccId" : "1211000000000000182",
                "imsi" :  "6767676",
                "msisdn" : "45454545"
            },
            {
                "iccId" : "1211000000000000183",
                "imsi" :  "6767676",
                "msisdn" : "45454545"
            },
            {
                "iccId" : "1001100000000000112",
                "imsi" :  "6767676",
                "msisdn" : "45454545"
            }]
    }

    var enterprises = {
        list : [enterprise,enterprise]
    };


    beforeEach(inject(function ($rootScope,$injector, $controller, $compile, dataTableConfigService, commonUtilityService, AuthenticationService) {
        scope = $rootScope.$new();
        window.clearDataTableStatus = jasmine.createSpy();
        SearchCtrl = $controller("SearchCtrl", {$scope: scope, $compile: $compile, dataTableConfigService: dataTableConfigService, authenticationService: AuthenticationService});
        _dataTableConfigService = dataTableConfigService;
        _authenticationService = AuthenticationService;
    }));

    it('Should test data-table columnDefs 0',function(){
        var data,type,row,meta;
        meta = {
            row:0
        };
        row = enterprise;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[0].render(data,type,row,meta)).toContain(row.accountName);
    });

    it('Should test data-table columnDefs 1',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = enterprise;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toContain(row.country);
    });

    it('Should test data-table columnDefs 2',function(){
        var data,type,row,meta;
        meta = {
            row:2
        };
        row = enterprise;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data,type,row,meta)).toContain('Not Available');
    });

    it('Should test data-table columnDefs 3',function(){
        var data,type,row,meta;
        meta = {
            row:3
        };
        row = enterprise;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toContain(row.currency);
    });

    it('Should test data-table columnDefs 4',function(){
        var data,type,row,meta;
        meta = {
            row:4
        };
        row = enterprise;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data,type,row,meta)).toContain(row.simList[0].iccid);
    });

    it('Should test fnServerData',function(){
        var sSource, aoData, fnCallback;
        var _mockPromise = {
            success: function(fn) {
                fn(enterprises);
                return _mockPromise;
            },
            error: function(fn) {
                fn(enterprises);
                return _mockPromise;
            }
        };
        spyOn(_authenticationService, 'getSubAccounts').and.returnValue(_mockPromise);
        scope.dataTableObj.dataTableRef ={};
        scope.dataTableObj.dataTableRef.fnSettings = function(){};

        scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
        expect(scope.companiesList.length).toBe(enterprises.list.length);
    });

    it('Should test searchIt',function(){

        var searchForm = {};
        scope.isFormValid = function(){
            return true;
        }
        SearchCtrl.searchIt(searchForm);
        expect(SearchCtrl.searchErrorStr).toBe("Search value is required");

        scope.dataTableObj.searchValue = "1211000000000000181";
        scope.dataTableObj.searchOn.value = "iccId"
        SearchCtrl.searchIt(searchForm);
        expect(scope.showDataTable).toBe(true);
        expect(sessionStorage.getItem('sOn')).toBe(scope.dataTableObj.searchOn.value);
        expect(sessionStorage.getItem('sVal')).toBe(scope.dataTableObj.searchValue);

    });

});