describe('Location history Controller', function() {

    var LocationHistoryCtrl, scope, _dataTableConfigService, _simService;

    beforeEach(module('app'));

    var locationHistory = {
        "country": "Japan",
        "networkName": "NTT Docomo",
        "mcc": 440,
        "mnc": 10,
        "iccId": "8910410000002003615",
        "imsi": "238013950026446",
        "msisdn": "4592813502",
        "imsiProvider": "TDC",
        "source": "procera",
        "connectionTechnologyType": "",
        "connectionType": "CA",
        "locationInfo": "Tokyo, Japan",
        "timeAtGSBackend": "2016-05-03 14:02:42"
    };

    var locationHistoryList = {
        list : [locationHistory,locationHistory,locationHistory]
    };
    beforeEach(inject(function ($injector, $controller, $compile, dataTableConfigService, commonUtilityService, SIMService) {
        scope = {};
        LocationHistoryCtrl = $controller("LocationHistoryCtrl", {$scope: scope, $compile: $compile, dataTableConfigService: dataTableConfigService, SIMService: SIMService});
        _dataTableConfigService = dataTableConfigService;
        _simService = SIMService;
    }));

    it('Should test data-table columnDefs 0',function(){
        var data,type,row,meta;
        meta = {
            row:0
        };
        row = locationHistory;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[0].render(data,type,row,meta)).toContain(row.timeAtGSBackend);
    });

    it('Should test data-table columnDefs 1',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = locationHistory;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toContain(row.locationInfo);
    });

    it('Should test data-table columnDefs 2',function(){
        var data,type,row,meta;
        meta = {
            row:2
        };
        row = locationHistory;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data,type,row,meta)).toContain(row.imsi);
    });

    it('Should test data-table columnDefs 3',function(){
        var data,type,row,meta;
        meta = {
            row:3
        };
        row = locationHistory;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toContain(row.imsiProvider);
    });

    it('Should test data-table columnDefs 4',function(){
        var data,type,row,meta;
        meta = {
            row:4
        };
        row = locationHistory;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data,type,row,meta)).toContain(row.msisdn);
    });

    it('Should test data-table columnDefs 5',function(){
        var data,type,row,meta;
        meta = {
            row:5
        };
        row = locationHistory;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row,meta)).toContain(row.networkName);
    });

    it('Should test data-table columnDefs 6',function(){
        var data,type,row,meta;
        meta = {
            row:6
        };
        row = locationHistory;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[6].render(data,type,row,meta)).toContain(row.country);
    });

    it('Should test data-table columnDefs 7',function(){
        var data,type,row,meta;
        meta = {
            row:7
        };
        row = locationHistory;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[7].render(data,type,row,meta)).toContain(row.mcc);
        expect(scope.dataTableObj.dataTableOptions.columnDefs[7].render(data,type,row,meta)).toContain(row.mnc);
    });

    it('Should test data-table columnDefs 8',function(){
        var data,type,row,meta;
        meta = {
            row:8
        };
        row = locationHistory;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[8].render(data,type,row,meta)).toContain(row.connectionType);
    });

    it('Should test data-table columnDefs 9',function(){
        var data,type,row,meta;
        meta = {
            row:9
        };
        row = locationHistory;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[9].render(data,type,row,meta)).toContain(row.source);
    });

    /*it('Should test fnServerData',function(){
        var sSource, aoData, fnCallback;
        var _mockPromise = {
            success: function(fn) {
                fn(locationHistoryList);
                return _mockPromise;
            },
            error: function(fn) {
                fn(locationHistoryList);
                return _mockPromise;
            }
        };
        spyOn(_simService, 'getSIMLocationHistory').and.returnValue(_mockPromise);
        spyOn($.fn, "DataTable").and.callThrough();
        scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
        expect(scope.locationHistoryList.length).toBe(locationHistoryList.list.length);
    });*/

});