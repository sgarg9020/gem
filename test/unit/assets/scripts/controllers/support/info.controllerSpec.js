describe('Enterprise info Controller', function() {

    var InfoCtrl, scope, rootScope;
    beforeEach(module('app'));

    var countries = [{"_id":"561771933b27e844933060b9","alpha-2":"AF","name":"Afghanistan","countryId":"561771933b27e844933060b9"},{"_id":"561771933b27e844933060ba","name":"Åland Islands","alpha-2":"AX","countryId":"561771933b27e844933060ba"},{"_id":"561771933b27e844933060bb","name":"Albania","alpha-2":"AL","countryId":"561771933b27e844933060bb"},{"_id":"561771933b27e844933060bc","name":"Algeria","alpha-2":"DZ","countryId":"561771933b27e844933060bc"},{"_id":"561771933b27e844933060bd","alpha-2":"AS","name":"American Samoa","countryId":"561771933b27e844933060bd"},{"_id":"561771933b27e844933060be","name":"Andorra","alpha-2":"AD","countryId":"561771933b27e844933060be"},{"_id":"561771933b27e844933060bf","name":"Angola","alpha-2":"AO","countryId":"561771933b27e844933060bf"},{"_id":"561771933b27e844933060c0","alpha-2":"AI","name":"Anguilla","countryId":"561771933b27e844933060c0"},{"_id":"561771933b27e844933060c1","name":"Antarctica","alpha-2":"AQ","countryId":"561771933b27e844933060c1"},{"_id":"561771933b27e844933060c2","name":"Antigua and Barbuda","alpha-2":"AG","countryId":"561771933b27e844933060c2"},{"_id":"561771933b27e844933060c3","name":"Argentina","alpha-2":"AR","countryId":"561771933b27e844933060c3"},{"_id":"561771933b27e844933060c4","alpha-2":"AM","name":"Armenia","countryId":"561771933b27e844933060c4"},{"_id":"561771933b27e844933060c5","name":"Aruba","alpha-2":"AW","countryId":"561771933b27e844933060c5"}];
    var enterprise = {
        "type": "Account",
        "accountId": 2,
        "company": {
            "type": "Company",
            "name": "Uber",
            "phone": "8888888888",
            "currency": "USD",
            "timeZone": "+00:00",
            "timeZoneInfo": "American Samoa, Jarvis Island, Kingman Reef, Midway Atoll and Palmyra Atoll",
            "address": {
                "type": "Address",
                "address1": "c/o Uber Technologies, Inc",
                "address2": "182 Howard Street # 8",
                "state": "California",
                "city": "San Francisco",
                "zipCode": "94105",
                "country": "US"
            }
        }
    };

    beforeEach(inject(function ($rootScope,$injector, $controller, $compile, commonUtilityService, SettingsService, $q, $rootScope) {
        scope = $rootScope.$new();

        spyOn(commonUtilityService,"getCountries").and.callFake(function(){
            var expectedResponse1 = {"countryList": countries};
            var d1 = $q.defer();
            d1.resolve({data:expectedResponse1});
            return d1.promise;
        });

        spyOn(SettingsService,"getEnterpriseDetails").and.callFake(function(){
            var expectedResponse = enterprise;
            var d = $q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });
        var country = {code : 'US' ,name : 'AMERICA'}
        spyOn(commonUtilityService,"getCountryObject").and.returnValue(country);

        InfoCtrl = $controller("InfoCtrl", {$scope: scope, $compile: $compile, $rootScope:$rootScope});
        rootScope = $rootScope;
        rootScope.$apply();
    }));

    it('should test _pricingDetails',function(){
        expect(InfoCtrl.countries).toBe(countries);
        expect(InfoCtrl.account.companyName).toBe(enterprise.company.name);
        expect(InfoCtrl.account.country.code).toBe('US');
        expect(InfoCtrl.account.accountSubscriptionType).toBe('GEM PRO');
        expect(InfoCtrl.account.alertVersionSupported).toBe(enterprise.alertVersionSupported);
        expect(InfoCtrl.account.timezone).toBe(enterprise.company.timeZoneInfo);
    });

});