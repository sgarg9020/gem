describe('GapUserManagementCtrl', function () {

    var _GapUserManagementCtrl,scope, _$controller, _rootScope, _compile,_UserService, _dataTableConfigService, _commonUtilityService,_loadingState,_$sanitize,_AuthenticationService,_stateParams,_location,_sce,_filter,_AccountService;

    var accountDetails = {
        "type": "EnterpriseAccountList",
        "enterpriseAccountDetails": [{
            "type": "Account",
            "rootAccountDetails": {
                "type": "Account",
                "accountId": 2,
                "accountType": "ENTERPRISE",
                "accountName": "Uber"
            },
            "parentAccountDetails": [{
                "type": "Account",
                "accountId": 2,
                "accountType": "ENTERPRISE",
                "accountName": "Uber",
                "role": "ENTERPRISE_ADMIN"
            }],
            "selfAccountDetails": {
                "type": "Account",
                "accountId": 22,
                "accountType": "USER"
            }
        }]
    };

    var user = {"type": "User", "userId": 2, "firstName": "uberAdmin", "lastName": "uberAdmin", "emailId": "uberAdmin@uber.com", "location": "Palo-Alto","address": {"type": "Address", "addressId": 2, "address1": "Electronics city", "address2": "Hosur Road", "state": "Karnataka", "city": "Bangalore", "zipCode": "560093", "country": "IN"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 08:28:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": accountDetails, "totalSimsAssociated": "14", "dataUsedInBytes": 0};

    var MockUserList = [user,{firstName:'anant',lastName:'kulkarni',emailId:'anant@gigsky.com',userId:"1234", "group":null, "accountDetails": accountDetails,"totalSimsAssociated": "0" },
        {firstName:'yogesh',lastName:'reddy',emailId:'yogesh@gigsky.com',userId:"1235", "group":null, "accountDetails": accountDetails }];

    var rolesList = { "type" : "RoleList", "list" : ["ENTERPRISE_ADMIN", "ENTERPRISE_USER"]};

    beforeEach(module('app'));

    beforeEach(inject(function ($injector, $controller, $rootScope, $compile,UserService, dataTableConfigService, commonUtilityService,loadingState,$sanitize,AuthenticationService,$stateParams,$location,$sce,$filter,AccountService) {

        _$controller = $controller;
        scope = {};
        _rootScope = $rootScope;
        _compile = $compile;
        _UserService = UserService;
        _dataTableConfigService = dataTableConfigService;
        _commonUtilityService = commonUtilityService;
        _loadingState = loadingState;
        _$sanitize = $sanitize;
        _AuthenticationService = AuthenticationService;
        _stateParams = $stateParams;
        _location = $location;
        _sce = $sce;
        _filter = $filter;
        _AccountService = AccountService;

        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');

    }));



    it('Should test GapUserManagementCtrl controller initialization',function(){

        window.clearDataTableStatus = jasmine.createSpy();
        _stateParams.accountId =1;

        var _mockPromise = {
            success: function(fn) {
                fn(rolesList);
                return _mockPromise;
            },
            error: function(fn) {
                return _mockPromise;
            }
        };
        spyOn(_AccountService, 'getRoles').and.returnValue(_mockPromise);

        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});


        expect(clearDataTableStatus).toHaveBeenCalledWith(['DataTables_gapUsersDataTable_']);
        expect(scope.emptyTableMessage).toBe('No Users available.');
        expect(scope.enterpriseId).toBe(_stateParams.accountId);


    });

    it('Should test controller initialization when getRoles API is successful',function(){

        window.clearDataTableStatus = jasmine.createSpy();
        _stateParams.accountId =1;

        var _mockPromise = {
            success: function(fn) {
                fn(rolesList);
                return _mockPromise;
            },
            error: function(fn) {
                return _mockPromise;
            }
        };
        spyOn(_AccountService, 'getRoles').and.returnValue(_mockPromise);

        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});


        expect(clearDataTableStatus).toHaveBeenCalledWith(['DataTables_gapUsersDataTable_']);
        expect(scope.emptyTableMessage).toBe('No Users available.');
        expect(scope.enterpriseId).toBe(_stateParams.accountId);
        expect(scope.rolesList).toEqual(rolesList.list);
        expect(scope.userRoleFilters).toEqual([{ name: 'ALL',dbRef: '',index: 0 },{ index: 1, dbRef: 'ENTERPRISE_ADMIN', name: 'ADMIN' },{ index: 2, dbRef: 'ENTERPRISE_USER', name: 'USER' }]);
        expect(_GapUserManagementCtrl.userRoleFilter).toEqual({ name: 'ALL', dbRef: '', index: 0 });
    });

    it('Should test controller initialization when getRoles API fails',function(){

        window.clearDataTableStatus = jasmine.createSpy();
        _stateParams.accountId =1;

        var errorResponse = {errorStr:'getRoles API failed'};
        var _mockPromise = {
            success: function() {
                return _mockPromise;
            },
            error: function(fn) {
                fn(errorResponse);
                return _mockPromise;
            }
        };
        spyOn(_AccountService, 'getRoles').and.returnValue(_mockPromise);


        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});


        expect(clearDataTableStatus).toHaveBeenCalledWith(['DataTables_gapUsersDataTable_']);
        expect(scope.emptyTableMessage).toBe('No Users available.');
        expect(scope.enterpriseId).toBe(_stateParams.accountId);
        expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith(errorResponse.errorStr);

    });

    it('Should test userRoleFilter initialization on controller active / when no filter is selected',function(){

        window.clearDataTableStatus = jasmine.createSpy();
        _stateParams.accountId =1;

        var _mockPromise = {
            success: function(fn) {
                fn(rolesList);
                return _mockPromise;
            },
            error: function(fn) {
                return _mockPromise;
            }
        };
        spyOn(_AccountService, 'getRoles').and.returnValue(_mockPromise);

        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

        expect(scope.userRoleFilters).toEqual([{ name: 'ALL',dbRef: '',index: 0 },{ index: 1, dbRef: 'ENTERPRISE_ADMIN', name: 'ADMIN' },{ index: 2, dbRef: 'ENTERPRISE_USER', name: 'USER' }]);
        expect(_GapUserManagementCtrl.userRoleFilter).toEqual({ name: 'ALL', dbRef: '', index: 0 });
    });

    it('Should test userRoleFilter initialization when ADMIN filter is selected',function(){
        _location.search('urf',"1");

        window.clearDataTableStatus = jasmine.createSpy();
        _stateParams.accountId =1;

        var _mockPromise = {
            success: function(fn) {
                fn(rolesList);
                return _mockPromise;
            },
            error: function(fn) {
                return _mockPromise;
            }
        };
        spyOn(_AccountService, 'getRoles').and.returnValue(_mockPromise);

        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

        expect(scope.userRoleFilters).toEqual([{ name: 'ALL',dbRef: '',index: 0 },{ index: 1, dbRef: 'ENTERPRISE_ADMIN', name: 'ADMIN' },{ index: 2, dbRef: 'ENTERPRISE_USER', name: 'USER' }]);
        expect(_GapUserManagementCtrl.userRoleFilter).toEqual({ index: 1, dbRef: 'ENTERPRISE_ADMIN', name: 'ADMIN' });
    });

    it('Should test userRoleFilter initialization when USER filter is selected',function(){
        _location.search('urf',"2");

        window.clearDataTableStatus = jasmine.createSpy();
        _stateParams.accountId =1;

        var _mockPromise = {
            success: function(fn) {
                fn(rolesList);
                return _mockPromise;
            },
            error: function(fn) {
                return _mockPromise;
            }
        };
        spyOn(_AccountService, 'getRoles').and.returnValue(_mockPromise);

        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

        expect(scope.userRoleFilters).toEqual([{ name: 'ALL',dbRef: '',index: 0 },{ index: 1, dbRef: 'ENTERPRISE_ADMIN', name: 'ADMIN' },{ index: 2, dbRef: 'ENTERPRISE_USER', name: 'USER' }]);
        expect(_GapUserManagementCtrl.userRoleFilter).toEqual({ index: 2, dbRef: 'ENTERPRISE_USER', name: 'USER' });
    });

    it('Should test userRoleFilter initialization when default filter is selected',function(){
        _location.search('urf',"0");

        window.clearDataTableStatus = jasmine.createSpy();
        _stateParams.accountId =1;

        var _mockPromise = {
            success: function(fn) {
                fn(rolesList);
                return _mockPromise;
            },
            error: function(fn) {
                return _mockPromise;
            }
        };
        spyOn(_AccountService, 'getRoles').and.returnValue(_mockPromise);

        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

        expect(scope.userRoleFilters).toEqual([{ name: 'ALL',dbRef: '',index: 0 },{ index: 1, dbRef: 'ENTERPRISE_ADMIN', name: 'ADMIN' },{ index: 2, dbRef: 'ENTERPRISE_USER', name: 'USER' }]);
        expect(_GapUserManagementCtrl.userRoleFilter).toEqual({ name: 'ALL', dbRef: '', index: 0 });
    });


    it('should verify the trustAsHtml function', function () {
        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

        _GapUserManagementCtrl.trustAsHtml();

        var actualMsg = _sce.getTrustedHtml('USER');
        expect(actualMsg).toBe('USER');

    });

    describe('it should test the dataTable configurations', function () {

        it('should set dataTable object headers', function () {
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            expect(scope.dataTableObj.tableHeaders[0].value).toBe('');
            expect(scope.dataTableObj.tableHeaders[0].DbValue).toBe('');
            expect(scope.dataTableObj.tableHeaders[1].value).toBe('Name');
            expect(scope.dataTableObj.tableHeaders[1].DbValue).toBe('fullName');
            expect(scope.dataTableObj.tableHeaders[2].value).toBe('Email');
            expect(scope.dataTableObj.tableHeaders[2].DbValue).toBe('emailId');
            expect(scope.dataTableObj.tableHeaders[3].value).toBe('Role');
            expect(scope.dataTableObj.tableHeaders[3].DbValue).toBe('role');
            expect(scope.dataTableObj.tableHeaders[4].value).toBe('Location');
            expect(scope.dataTableObj.tableHeaders[4].DbValue).toBe('location');
            expect(scope.dataTableObj.tableHeaders[5].value).toBe('SIM(s)');
            expect(scope.dataTableObj.tableHeaders[5].DbValue).toBe('totalSimsAssociated');
            expect(scope.dataTableObj.tableHeaders[6].value).toBe('Group');
            expect(scope.dataTableObj.tableHeaders[6].DbValue).toBe('accountName');
            expect(scope.dataTableObj.tableHeaders[7].value).toBe('Usage');
            expect(scope.dataTableObj.tableHeaders[7].DbValue).toBe('dataUsage');
        });

        it('should set tableID', function () {
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            expect(scope.dataTableObj.tableID).toBe('gapUsersDataTable');
        });

        it('should set selectedDisplayLength', function () {
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            expect(scope.dataTableObj.filters.selectedDisplayLength).toBe(10);
        });

        it('should compile data table rows', function (){
            var data,row,index;
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});
            row=user;
            data=user;

            scope.dataTableObj.dataTableOptions.createdRow(row,data,index);
            _compile(row)(scope);
        });

        it('should test the orderable property for the dataTables', function () {

            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            expect(scope.dataTableObj.dataTableOptions.columns[0].orderable).toBeFalsy();
            expect(scope.dataTableObj.dataTableOptions.columns[1].orderable).toBeTruthy();
            expect(scope.dataTableObj.dataTableOptions.columns[2].orderable).toBeTruthy();
            expect(scope.dataTableObj.dataTableOptions.columns[3].orderable).toBeFalsy();
            expect(scope.dataTableObj.dataTableOptions.columns[4].orderable).toBeTruthy();
            expect(scope.dataTableObj.dataTableOptions.columns[5].orderable).toBeTruthy();
            expect(scope.dataTableObj.dataTableOptions.columns[6].orderable).toBeTruthy();
            expect(scope.dataTableObj.dataTableOptions.columns[7].orderable).toBeTruthy();

        });

        it('should test data-table columnDefs 0',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var data,type,row,meta;
            meta = {
                row:0
            };
            row = MockUserList[0];
            expect(scope.dataTableObj.dataTableOptions.columnDefs[0].render(data,type,row,meta)).toBe('<div gs-has-permission="USER_DELETE" class="checkbox check-success large no-label-text hideInReadOnlyMode"> <input type="checkbox" value="1" id="checkbox_'+row.userId+'" gs-index="'+meta.row+'"><label ng-click="checkBoxClicked($event)" id="label_'+row.userId+'"></label></div>');
        });

        it('should test data-table columnDefs 1 when editing is allowed',function(){

            _AuthenticationService.isOperationAllowed = function(){
                return true;
            };

            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var data,type,row,meta;
            row = MockUserList[0];

            meta = {
                row:1
            };

            expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toBe('<a id="edit_user_'+row.userId+'" class="nowrap" ng-click="editUser('+meta.row+');" href="javascript:void(0)" title="Click to edit user '+row.firstName + ' ' + row.lastName+'"><i class="fa fa-pencil edit-ico"></i>'+ ' ' + row.firstName + ' ' + row.lastName+'</a>');

        });


        it('should test data-table columnDefs 1 when editing is not allowed',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var data,type,row,meta;
            row = MockUserList[0];

            _AccountService.isOperationAllowed = function(){
                return false;
            };

            expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toBe(row.firstName + ' ' + row.lastName);

        });


        it('should test data-table columnDefs 2',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var data,type,row,meta;

            row = MockUserList[0];

            expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data,type,row,meta)).toBe('<a href="mailto:'+row.emailId+'" title="Send an email to '+row.emailId+'" class="nowrap">'+row.emailId+'</a>');
        });

        it('should test data-table columnDefs 3 when user is admin of the enterprise',function(){

            _stateParams.accountId =2;
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var data,type,row,meta;

            row = MockUserList[0];

            expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toBe("<span class='nowrap'>ADMIN</span>");
        });

        it('should test data-table columnDefs 3 when user is user of the enterprise',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var accountDetails = {
                "type": "EnterpriseAccountList",
                "enterpriseAccountDetails": [{
                    "type": "Account",
                    "rootAccountDetails": {
                        "type": "Account",
                        "accountId": 2,
                        "accountType": "ENTERPRISE",
                        "accountName": "Uber"
                    },
                    "parentAccountDetails": [{
                        "type": "Account",
                        "accountId": 2,
                        "accountType": "ENTERPRISE",
                        "accountName": "Uber",
                        "role": "ENTERPRISE_USER"
                    }],
                    "selfAccountDetails": {
                        "type": "Account",
                        "accountId": 22,
                        "accountType": "USER"
                    }
                }]
            };

            var user = {"type": "User", "userId": 2, "firstName": "user1", "lastName": "user", "emailId": "uber1@uber.com", "location": "Palo-Alto","address": {"type": "Address", "addressId": 2, "address1": "Electronics city", "address2": "Hosur Road", "state": "Karnataka", "city": "Bangalore", "zipCode": "560093", "country": "IN"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 08:28:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": accountDetails, "totalSimsAssociated": "14", "dataUsedInBytes": 0};

            var data,type,row,meta;

            row = user;

            expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toBe("<span class='nowrap'>USER</span>");
        });


        it('should test data-table columnDefs 3 when no role is present',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var accountDetails = {
                "type": "EnterpriseAccountList",
                "enterpriseAccountDetails": [{
                    "type": "Account",
                    "rootAccountDetails": {
                        "type": "Account",
                        "accountId": 2,
                        "accountType": "ENTERPRISE",
                        "accountName": "Uber"
                    },
                    "parentAccountDetails": [],
                    "selfAccountDetails": {
                        "type": "Account",
                        "accountId": 22,
                        "accountType": "USER"
                    }
                }]
            };

            var user = {"type": "User", "userId": 2, "firstName": "user1", "lastName": "user", "emailId": "uber1@uber.com", "location": "Palo-Alto","address": {"type": "Address", "addressId": 2, "address1": "Electronics city", "address2": "Hosur Road", "state": "Karnataka", "city": "Bangalore", "zipCode": "560093", "country": "IN"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 08:28:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": accountDetails, "totalSimsAssociated": "14", "dataUsedInBytes": 0};

            var data,type,row,meta;

            row = user;

            expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toBe("<span class='nowrap'>USER</span>");
        });


        it('should test data-table columnDefs 4',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var data,type,row,meta;

            row = MockUserList[0];

            expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data,type,row,meta)).toBe("<span class='nowrap'>"+row.location+"</span>");
        });

        it('should test data-table columnDefs 5 when sims are associated to the user',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var data,type,row,meta;
            meta = {
                row:5
            };

            row = MockUserList[0];

            expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row,meta)).toBe('<a ui-sref="support.enterprise.simCards"  ng-click=\'showSIM('+meta.row+');\'><span>' + row.totalSimsAssociated + '</span></a>');
        });

        it('should test data-table columnDefs 6 when user associated to the group',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var accountDetails = {
                "type": "EnterpriseAccountList",
                "enterpriseAccountDetails": [{
                    "type": "Account",
                    "rootAccountDetails": {
                        "type": "Account",
                        "accountId": 2,
                        "accountType": "ENTERPRISE",
                        "accountName": "Uber"
                    },
                    "parentAccountDetails": [{
                        "type": "Account",
                        "accountId": 2,
                        "accountType": "GROUP",
                        "accountName": "Uber East",
                        "role": "ENTERPRISE_USER"
                    }],
                    "selfAccountDetails": {
                        "type": "Account",
                        "accountId": 22,
                        "accountType": "USER"
                    }
                }]
            };

            var user = {"type": "User", "userId": 2, "firstName": "user1", "lastName": "user", "emailId": "uber1@uber.com", "location": "Palo-Alto","address": {"type": "Address", "addressId": 2, "address1": "Electronics city", "address2": "Hosur Road", "state": "Karnataka", "city": "Bangalore", "zipCode": "560093", "country": "IN"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 08:28:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": accountDetails, "totalSimsAssociated": "14", "dataUsedInBytes": 0};
            var data,type,row,meta;

            row = user;

            expect(scope.dataTableObj.dataTableOptions.columnDefs[6].render(data,type,row,meta)).toBe("<span class='nowrap'>Uber East</span>");
        });

        it('should test data-table columnDefs 6 when user is associated to the enterprise',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var data,type,row,meta;

            row = MockUserList[0];

            expect(scope.dataTableObj.dataTableOptions.columnDefs[6].render(data,type,row,meta)).toBe("<span class='nowrap'>Uber</span>");
        });

        it('should test data-table columnDefs 6 when user has no association',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var accountDetails = {
                "type": "EnterpriseAccountList",
                "enterpriseAccountDetails": [{
                    "type": "Account",
                    "rootAccountDetails": {
                        "type": "Account",
                        "accountId": 2,
                        "accountType": "ENTERPRISE"
                    },
                    "parentAccountDetails": [],
                    "selfAccountDetails": {
                        "type": "Account",
                        "accountId": 22,
                        "accountType": "USER"
                    }
                }]
            };


            var data,type,row,meta;
            row = {"type": "User", "userId": 2, "firstName": "user1", "lastName": "user", "emailId": "uber1@uber.com", "location": "Palo-Alto","address": {"type": "Address", "addressId": 2, "address1": "Electronics city", "address2": "Hosur Road", "state": "Karnataka", "city": "Bangalore", "zipCode": "560093", "country": "IN"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 08:28:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": accountDetails, "totalSimsAssociated": "14", "dataUsedInBytes": 0};

            expect(scope.dataTableObj.dataTableOptions.columnDefs[6].render(data,type,row,meta)).toBe("<span class='nowrap'>Default</span>");

            delete accountDetails.enterpriseAccountDetails.parentAccountDetails;

            row = {"type": "User", "userId": 2, "firstName": "user1", "lastName": "user", "emailId": "uber1@uber.com", "location": "Palo-Alto","address": {"type": "Address", "addressId": 2, "address1": "Electronics city", "address2": "Hosur Road", "state": "Karnataka", "city": "Bangalore", "zipCode": "560093", "country": "IN"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 08:28:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": accountDetails, "totalSimsAssociated": "14", "dataUsedInBytes": 0};

            expect(scope.dataTableObj.dataTableOptions.columnDefs[6].render(data,type,row,meta)).toBe("<span class='nowrap'>Default</span>");

        });

        it('should test data-table columnDefs 7 when there is dataUsage',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var data,type,row,meta;

            row = {"type": "User", "userId": 2, "firstName": "user1", "lastName": "user", "emailId": "uber1@uber.com", "location": "Palo-Alto","address": {"type": "Address", "addressId": 2, "address1": "Electronics city", "address2": "Hosur Road", "state": "Karnataka", "city": "Bangalore", "zipCode": "560093", "country": "IN"}, "userStatus": "ACTIVE", "createdOn": "2015-12-02 08:28:16", "homePhone": "080-58974536", "mobile": "9886333515", "accountDetails": accountDetails, "totalSimsAssociated": "14", "dataUsedInBytes": 10240};

            expect(scope.dataTableObj.dataTableOptions.columnDefs[7].render(data,type,row,meta)).toBe('<span>0.01</span>');
        });

        it('should test data-table columnDefs 7 when there is no dataUsage',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var data,type,row,meta;

            row = MockUserList[1];

            expect(scope.dataTableObj.dataTableOptions.columnDefs[7].render(data,type,row,meta)).toBe('<span>0</span>');
        });

        it('should set oLanguage values', function () {
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            expect(scope.dataTableObj.dataTableOptions.oLanguage.sLengthMenu).toBe('_MENU_ ');
            expect(scope.dataTableObj.dataTableOptions.oLanguage.sInfo).toBe('Showing <b>_START_ to _END_</b> of _TOTAL_ entries');
            expect(scope.dataTableObj.dataTableOptions.oLanguage.sEmptyTable).toBe('No Users available.');
            expect(scope.dataTableObj.dataTableOptions.oLanguage.sZeroRecords).toBe('No Users available.');

        });

        it('should set processing values', function () {
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            expect(scope.dataTableObj.dataTableOptions.processing).toBeTruthy();
        });

        it('should set rowCallback values', function () {
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var data,type,row,meta;

            row = MockUserList[0];
            data = MockUserList[0];

            scope.dataTableObj.showPreviousSelectedRows = function (userId,row) {
                expect(userId).toEqual( MockUserList[0].userId);
                expect(row).toEqual( MockUserList[0]);
            };

            scope.dataTableObj.dataTableOptions.rowCallback(row,data);
        });


        it('should test fnServerData UserService.getUsers success',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var sSource, aoData;
            var _mockPromise = {
                success: function(fn) {
                    fn({count:10,totalCount:100,start:0,list:MockUserList});
                    return _mockPromise;
                },
                error: function(fn) {
                    return _mockPromise;
                }
            };
            spyOn(_UserService, 'getUsers').and.returnValue(_mockPromise);

            function fnCallback(){

            }
            scope.dataTableObj.dataTableRef={};
            scope.dataTableObj.dataTableRef.fnSettings = function (){
                return {oLanguage:{}};
            };

            scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
            expect(scope.usersList.length).toBe(MockUserList.length);
            expect(_UserService.getUsers).toHaveBeenCalledWith({},2);

            scope.dataTableObj.dataTableRef.fnSettings = function(){};
            aoData = [{name:'draw',value:1},{name:"columns",value:1},{name:'order',value:1},{name:'start',value:0},{name:'length',value:10},{name:'search',value:{value:'Uber'}}];
            scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
            expect(_UserService.getUsers).toHaveBeenCalledWith({ startIndex: 0, count: 10, search: 'Uber' },2);
            expect(_GapUserManagementCtrl.searchValue).toBe('Uber');
            expect(scope.isDisable).toBeFalsy();
        });

        it('should test fnServerData when UserService.getUsers fails',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var sSource, aoData;
            var errorResponse = {errorStr:'getUsers are failed'};
            var _mockPromise = {
                success: function(fn) {
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(errorResponse);
                    return _mockPromise;
                }
            };
            spyOn(_UserService, 'getUsers').and.returnValue(_mockPromise);
            scope.dataTableObj.dataTableRef ={};

            scope.dataTableObj.dataTableRef.fnSettings = function (){
                return {oLanguage:{}};
            };

            function fnCallback(){

            }

            scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
            expect(_UserService.getUsers).toHaveBeenCalledWith({},2);
            expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith(errorResponse.errorStr);
        });

        it('should test fnServerData UserService.getUsers success when ALL role filter is used',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var sSource, aoData;
            var _mockPromise = {
                success: function(fn) {
                    fn({count:10,totalCount:100,start:0,list:MockUserList});
                    return _mockPromise;
                },
                error: function(fn) {
                    return _mockPromise;
                }
            };
            spyOn(_UserService, 'getUsers').and.returnValue(_mockPromise);

            function fnCallback(){

            }
            scope.dataTableObj.dataTableRef={};
            scope.dataTableObj.dataTableRef.fnSettings = function (){
                return {oLanguage:{}};
            };

            scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
            expect(scope.usersList.length).toBe(MockUserList.length);
            expect(_UserService.getUsers).toHaveBeenCalledWith({},2);

        });

        it('should test fnServerData UserService.getUsers success when ADMIN role filter is used',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var sSource, aoData;
            var _mockPromise = {
                success: function(fn) {
                    fn({count:10,totalCount:100,start:0,list:MockUserList});
                    return _mockPromise;
                },
                error: function(fn) {
                    return _mockPromise;
                }
            };
            spyOn(_UserService, 'getUsers').and.returnValue(_mockPromise);

            function fnCallback(){

            }
            scope.dataTableObj.dataTableRef={};
            scope.dataTableObj.dataTableRef.fnSettings = function (){
                return {oLanguage:{}};
            };

            _GapUserManagementCtrl.userRoleFilter = { index: 1, dbRef: 'ENTERPRISE_ADMIN', name: 'ADMIN' };

            scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
            expect(scope.usersList.length).toBe(MockUserList.length);
            expect(_UserService.getUsers).toHaveBeenCalledWith({ roleName: 'ENTERPRISE_ADMIN', depth: 'IMMEDIATE' },2);

        });

        it('should test fnServerData UserService.getUsers success when USER role filter is used',function(){
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            var sSource, aoData;
            var _mockPromise = {
                success: function(fn) {
                    fn({count:10,totalCount:100,start:0,list:MockUserList});
                    return _mockPromise;
                },
                error: function(fn) {
                    return _mockPromise;
                }
            };
            spyOn(_UserService, 'getUsers').and.returnValue(_mockPromise);

            function fnCallback(){

            }
            scope.dataTableObj.dataTableRef={};
            scope.dataTableObj.dataTableRef.fnSettings = function (){
                return {oLanguage:{}};
            };

            _GapUserManagementCtrl.userRoleFilter = { index: 2, dbRef: 'ENTERPRISE_USER', name: 'USER' };

            scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
            expect(scope.usersList.length).toBe(MockUserList.length);
            expect(_UserService.getUsers).toHaveBeenCalledWith({ roleName: 'ENTERPRISE_USER' },2);

        });



        it('should test fnDrawCallback', function () {
            _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

            scope.usersList = MockUserList;

            _dataTableConfigService.disableTableHeaderOps = function(length,oSettings){
                expect(length).toBe(3);
            };

            scope.dataTableObj.dataTableOptions.fnDrawCallback('oSettings');
        });
    });

    it('should test isDisableComponent function', function () {

        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

        scope.isDisable = true;

        var isDisabled = scope.isDisableComponent();
        expect(isDisabled).toBeTruthy();

        scope.isDisable = false;

        var isDisabled = scope.isDisableComponent();
        expect(isDisabled).toBeFalsy();

    });

    it('should test checkBoxClicked function', function () {

        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

        var event = {};
        scope.dataTableObj.selectRow = jasmine.createSpy('selectRow spy method');
        scope.checkBoxClicked(event);

        expect(scope.dataTableObj.selectRow).toHaveBeenCalled();
    });

    it('should test scope.editUser function', function () {

        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

        scope.usersList = MockUserList;

        function editUserModal(user){
            expect(user).toEqual(MockUserList[0]);
        }

        scope.gapUserModalObj.updateUserModal = function(userObj, enterpriseId, rolesList, attr, cbk){

        };

        scope.editUser(0);

    });

    it('should test showSIM',function(){
        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

        scope.usersList = [user,{firstName:'anant',lastName:'kulkarni',emailId:'anant@gigsky.com',userId:"1234", "group":null, "accountDetails": accountDetails },
            {firstName:'yogesh',lastName:'reddy',emailId:'yogesh@gigsky.com',userId:"1235", "group":null, "accountDetails": accountDetails }];

        scope.showSIM(1);

        expect(sessionStorage.getItem("gs-showSimsAssignedToCurrentGapUser")).toBe('{"userId":"1234","firstName":"anant"}');

        sessionStorage.removeItem("gs-showSimsAssignedToCurrentGapUser");

    });

    it('should test addUser function', function () {
        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

        scope.gapUserModalObj.updateUserModal = function(userObj, enterpriseId, rolesList, attr, cbk){
            expect(attr.header).toBe("Add New User");
            expect(attr.submit).toBe("Add");
            if(!(cbk instanceof Function))
                expect(false).toBeTruthy();
            expect(userObj).toBeNull();
            scope.onUpdateCbk = cbk;
        };
        _GapUserManagementCtrl.addUser();

    });

    it('should test onAddNewUser function on UserService.addUser success', function () {
        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

        var _mockPromise = {
            success: function(fn) {
                fn();
                return _mockPromise;
            },
            error: function(fn) {
                return _mockPromise;
            }
        };
        spyOn(_UserService, 'addUser').and.returnValue(_mockPromise);

        scope.dataTableObj.refreshDataTable = function (drawFromPageOne) {
          expect(drawFromPageOne).toBeFalsy();
        };

        scope.gapUserModalObj.resetUserModal = function () {

        };
        scope.onAddNewUser(MockUserList[1]);

        expect(_UserService.addUser).toHaveBeenCalledWith({type: 'UserList', list:[{type:'User',firstName:'anant',lastName:'kulkarni',emailId:'anant@gigsky.com',userId:"1234", "group":null, "accountDetails": accountDetails,"totalSimsAssociated": "0" }]},2);
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith("An account has been created for anant  kulkarni");
    });

    it('should test onAddNewUser function on UserService.addUser fails', function () {
        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

        var errorResponse = {errorStr:'addUser API failed'};
        var _mockPromise = {
            success: function(fn) {
                return _mockPromise;
            },
            error: function(fn) {
                fn(errorResponse);
                return _mockPromise;
            }
        };
        spyOn(_UserService, 'addUser').and.returnValue(_mockPromise);

        scope.gapUserModalObj.resetUserModal = function () {

        };
        scope.onAddNewUser(MockUserList[1]);

        expect(_UserService.addUser).toHaveBeenCalledWith({type: 'UserList', list:[{type:'User',firstName:'anant',lastName:'kulkarni',emailId:'anant@gigsky.com',userId:"1234", "group":null, "accountDetails": accountDetails,"totalSimsAssociated": "0" }]},2);
        expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith(errorResponse.errorStr);
    });


    it('should test editUserModal function', function () {
        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

        scope.usersList = MockUserList;

        //Launch add user modal
        scope.gapUserModalObj.updateUserModal = function(userObj, enterpriseId, rolesList, attr, cbk){
            expect(attr.header).toBe("Update User");
            expect(attr.submit).toBe("Update");
            if(!(cbk instanceof Function))
                expect(false).toBeTruthy();
            expect(userObj.firstName).toBe(user.firstName);
            expect(userObj.lastName).toBe(user.lastName);
            expect(userObj.emailId).toBe(user.emailId);
            scope.onUpdateCbk = cbk;
        };
        scope.editUser(0);
    });

    
    it('should test onEditExistingUser function when UserService.updateUsers API is success', function () {
        _GapUserManagementCtrl = _$controller("GapUserManagementCtrl", {$scope:scope,$rootScope:_rootScope ,$compile:_compile, UserService:_UserService,dataTableConfigService:_dataTableConfigService,commonUtilityService:_commonUtilityService,loadingState:_loadingState,$sanitize:_$sanitize,AuthenticationService:_AuthenticationService,$stateParams:_stateParams,$location:_location,$sce:_sce,$filter:_filter,AccountService:_AccountService});

        var _mockPromise = {
            success: function(fn) {
                fn();
                return _mockPromise;
            },
            error: function(fn) {
                return _mockPromise;
            }
        };
        spyOn(_UserService, 'updateUsers').and.returnValue(_mockPromise);

        scope.dataTableObj.refreshDataTable = function (drawFromPageOne) {
            expect(drawFromPageOne).toBeFalsy();
        };

        scope.gapUserModalObj.resetUserModal = function () {

        };

       // expect(_UserService.updateUsers).toHaveBeenCalledWith({type: 'UserList', list:[{type:'User',firstName:'anant',lastName:'kulkarni',emailId:'anant@gigsky.com',userId:"1234", "group":null, "accountDetails": accountDetails,"totalSimsAssociated": "0" }]},2);

    });


});