describe('Sim cdr Controller', function() {

    var CallDateRecordsCtrl, scope, _dataTableConfigService, _commonUtilityService,_simService;

    beforeEach(module('app'));

    var cdr = {
        "cdrUsageType": "NORMAL",
        "cdrTrafficType": "DATA",
        "mcc": "440",
        "mnc": "10",
        "dataInBytes": "2048",
        "recordStartTime": "2016-04-22 09:41:39",
        "uplinkVolume": "17988",
        "downlinkVolume": "44670",
        "msisdn": "4592813502",
        "imsi": "238013950026446",
        "iccId": "8910300000000006171",
        "id": "57b7e1a59f5faa82cc408e2f"
    };

    var cdrList = {
        "startIndex": "0",
        "totalCount": 2,
        "count": 2,
        "list": [cdr,cdr]
    }

    beforeEach(inject(function ($rootScope,$injector, $controller, $compile, dataTableConfigService, commonUtilityService, SIMService) {
        scope = $rootScope.$new();
        CallDateRecordsCtrl = $controller("CallDateRecordsCtrl", {$scope: scope, $compile: $compile, dataTableConfigService: dataTableConfigService, commonUtilityService: commonUtilityService, SIMService:SIMService});
        _dataTableConfigService = dataTableConfigService;
        _commonUtilityService = commonUtilityService;
        _simService = SIMService;

        scope.dataTableObj.refreshDataTable = jasmine.createSpy('refreshDataTable spy method');
    }));

    it('Should test data-table columnDefs 0',function(){
        var data,type,row,meta;
        meta = {
            row:0
        };
        row = cdr;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[0].render(data,type,row,meta)).toContain(row.recordStartTime);
    });

    it('Should test data-table columnDefs 1',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = cdr;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toContain(2);
    });

    it('Should test data-table columnDefs 2',function(){
        var data,type,row,meta;
        meta = {
            row:2
        };
        row = cdr;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data,type,row,meta)).toContain('Not Available');
    });

    it('Should test data-table columnDefs 3',function(){
        var data,type,row,meta;
        meta = {
            row:3
        };
        row = cdr;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toContain(row.mcc);
        expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toContain(row.mnc);
    });

    it('Should test data-table columnDefs 4',function(){
        var data,type,row,meta;
        meta = {
            row:4
        };
        row = cdr;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[4].data).toBe('imsi');
    });

    it('Should test data-table columnDefs 5',function(){
        var data,type,row,meta;
        meta = {
            row:5
        };
        row = cdr;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[5].data).toBe('msisdn');
    });

    /*it('Should test fnServerData',function(){
        var sSource, aoData, fnCallback;
        var _mockPromise = {
            success: function(fn) {
                fn(cdrList);
                return _mockPromise;
            },
            error: function(fn) {
                fn(cdrList);
                return _mockPromise;
            }
        };
        spyOn(_simService, 'getSIMCdrs').and.returnValue(_mockPromise);
        scope.dataTableObj.dataTableRef ={};
        scope.dataTableObj.dataTableRef.fnSettings = function(){};
        var col = {};
        function DataTable(){};
        spyOn($("#cdrListDataTable"), 'DataTable').andReturn(col);

        scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
        expect(scope.cdrs.length).toBe(cdrList.list.length);
    });*/

});