describe('Sim Details Controller', function() {

    var SIMDetailCtrl, scope, _simService,_commonUtilityService,rootScope;

    beforeEach(module('app'));

    var sim = {
        "type": "Sim",
        "simId": "1001100000000000010",
        "simType": "GIGSKY_SIM",
        "status": "ACTIVE",
        "user": {
            "type": "User",
            "userId": 2,
            "firstName": "uberAdmin",
            "lastName": "uberAdmin"
        },
        "account": {
            "type": "ENTERPRISE",
            "accountId": 2,
            "accountName": "Uber"
        },
        "supplier": "BLUEFISH",
        "nickName": "SIM-0010",
        "creditLimitList": {
            "type": "CreditLimitList",
            "list": [{
                "type": "CreditLimit",
                "limitType": "SOFT",
                "limitKB": 10240,
                "limitDuration": "MONTH",
                "alertPercentages": [25],
                "alertUserByMail": true,
                "alertEnabled": true
            }]
        },
        "dataUsedInBytes": 0,
        "imsis" : [
            {
                "type" : "imsi",
                "imsi" : 123456789012345,
                "index" : 1,
                "profile" : "TDC",
                 "activationStatus" : "ACTIVE",
    "status" : "BOUND | INSTALLED | FAILED | DELIVERED",
        "installationType" : "BURNEDT | OTA",
        "msisdn" : "1234567890",
        "imsiKeys" : {
        "type" : "imsiKeys",
            "pin1" : "12343abcd", /* pin1, pin2, puk1, puk2, ki, opc values are optional */
            "pin2" : "12334abcd",
            "puk1" : "11234abcd",
            "puk2" : "12334abcd",
            "ki" : "1233abcd",
            "opc" : "123abcd"
    },
    "lastSeenLocation" : {
        "type" : "locationData",
            "mccmnc" : {
            "type" : "mccmnc",
                "mcc" : "300",
            "mnc" : "02"
        },
        "imsi" : 123456789012345,
            "lastSeenTime" : "2016-05-03 14:02:42"
    }
}
]
    };

    beforeEach(inject(function ($injector, $controller, $compile, commonUtilityService, SIMService, $rootScope) {
        scope = {};
        var response = sim;
        var _mockPromise = {
            success: function(fn) {
                fn(response);
                return _mockPromise;
            },
            error: function(fn) {
                fn(response);
                return _mockPromise;
            }
        };
        spyOn(SIMService, 'getSimDetail').and.returnValue(_mockPromise);
        scope.$parent = {};
        SIMDetailCtrl = $controller("SIMDetailCtrl", {$scope: scope, $compile: $compile,commonUtilityService:commonUtilityService,SIMService:SIMService,$rootScope:$rootScope});

        _simService = SIMService;
        _commonUtilityService = commonUtilityService;

        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
    }));

    it('should test getSIMDetails',function(){
        expect(SIMDetailCtrl.SIM).toBe(sim);
        expect(scope.isSIMExpanded).toBe(false);
    });

    it('modal should show',function(){
        scope.imsiModalObj.vfUpdateModal = function(imsis,cbk){
            expect(imsis.length).toBeGreaterThan(0);
            if(!(cbk instanceof Function))
                expect(false).toBeTruthy();
        };
        SIMDetailCtrl.switchIMSI();

    });

    it('should test processSwitchIMSI', function(){
        var response = {};
        var _mockPromise = {
            success: function(fn) {
                fn(response);
                return _mockPromise;
            },
            error: function(fn) {
                fn(response);
                return _mockPromise;
            }
        };
        spyOn(_simService, 'switchIMSI').and.returnValue(_mockPromise);

        scope.processSwitchIMSI("TDC");
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith("Successfully switched to imsi TDC");
    });

    it('modal should have Change Switch IMSI Mode title, when current mode is AUTO',function(){
        scope.verificationModalObj.addVerificationModelMessage = function(msg){
            expect(msg).toBe('WARNING! Changing mode to MANUAL will prevent the GigSky system from changing IMSI automatically for this SIM card. In some cases, this may mean that the IMSI selected on the SIM is not active in that location, resulting in a loss of connectivity. For normal operation, change mode to AUTOMATIC.');
        };

        scope.verificationModalObj.updateVerificationModel = function(attr, cbk){
            expect(attr.header).toBe("Change Switch IMSI Mode to MANUAL");
            expect(attr.submit).toBe("submit");
            expect(attr.cancel).toBe("Cancel");
            if(!(cbk instanceof Function))
                expect(false).toBeTruthy();
        };

        SIMDetailCtrl.verifySwitchIMSIMOde('AUTO');
    });

    it('modal should have Change Switch IMSI Mode title, when current mode is MANUAL',function(){
        scope.verificationModalObj.addVerificationModelMessage = function(msg){
            expect(msg).toBe('Changing mode to AUTOMATIC is required for normal operation.');
        };

        scope.verificationModalObj.updateVerificationModel = function(attr, cbk){
            expect(attr.header).toBe("Change Switch IMSI Mode to AUTOMATIC");
            expect(attr.submit).toBe("submit");
            expect(attr.cancel).toBe("Cancel");
            if(!(cbk instanceof Function))
                expect(false).toBeTruthy();
            scope.processChangeMode = cbk;
        };

        SIMDetailCtrl.verifySwitchIMSIMOde('MANUAL');
    });

    it('should test switch imsi mode method',function(){
        var response = {};
        var _mockPromise = {
            success: function(fn) {
                fn(response);
                return _mockPromise;
            },
            error: function(fn) {
                fn(response);
                return _mockPromise;
            }
        };
        spyOn(_simService, 'switchIMSIMode').and.returnValue(_mockPromise);

        scope.switchIMSIMode("1001100000000000010","MANUAL");
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Switch IMSI mode changed successfully to MANUAL');
    })
});