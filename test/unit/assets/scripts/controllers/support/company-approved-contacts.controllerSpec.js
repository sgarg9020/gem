describe('Company Approved Contacts Controller', function () {

    var ContactsCtrl, scope, _dataTableConfigService, _commonUtilityService, _settingsService;

    beforeEach(module('app'));

    var contact = {
        "firstName": "Priyanka",
        "lastName": "Patil",
        "emailId": "test@test",
        "phoneNumber": "9611556560",
        "jobTitle": "Manager",
        "contactId": "1234"
    };

    var contacts = {
        "type": "AccountApprovedContactResponse",
        "startIndex": 0,
        "totalCount": 2,
        "count": 2,
        "list": [contact, contact]
    };

    beforeEach(inject(function ($rootScope,$injector, $controller, $compile, dataTableConfigService, commonUtilityService, SettingsService) {
        scope = $rootScope.$new();
        ContactsCtrl = $controller("ContactsCtrl", {
            $scope: scope,
            $compile: $compile,
            dataTableConfigService: dataTableConfigService,
            commonUtilityService: commonUtilityService,
            SettingsService: SettingsService
        });
        _dataTableConfigService = dataTableConfigService;
        _commonUtilityService = commonUtilityService;
        _settingsService = SettingsService;

        scope.contactModalObj = {};
        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');

        scope.dataTableObj.refreshDataTable = jasmine.createSpy('refreshDataTable spy method');
        scope.contactModalObj.resetContactModal = jasmine.createSpy('resetContactModal spy method');
    }));

    it('Should test data-table columnDefs 0', function () {
        var data, type, row, meta;
        meta = {
            row: 0
        };
        row = contact;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[0].render(data, type, row, meta)).toContain(row.contactId);
    });

    it('Should test data-table columnDefs 1', function () {
        var data, type, row, meta;
        meta = {
            row: 1
        };
        row = contact;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data, type, row, meta)).toContain(row.firstName);
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data, type, row, meta)).toContain(row.lastName);
    });

    it('Should test data-table columnDefs 2', function () {
        var data, type, row, meta;
        meta = {
            row: 2
        };
        row = contact;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data, type, row, meta)).toContain(row.emailId);
    });

    it('Should test data-table columnDefs 3', function () {
        var data, type, row, meta;
        meta = {
            row: 3
        };
        row = contact;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data, type, row, meta)).toContain(row.jobTitle);
    });

    it('Should test data-table columnDefs 4', function () {
        var data, type, row, meta;
        meta = {
            row: 4
        };
        row = contact;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data, type, row, meta)).toContain(row.phoneNumber);
    });

    it('Should test fnServerData', function () {
        var sSource, aoData, fnCallback;
        var _mockPromise = {
            success: function (fn) {
                fn(contacts);
                return _mockPromise;
            },
            error: function (fn) {
                fn(contacts);
                return _mockPromise;
            }
        };
        spyOn(_settingsService, 'getApprovedContacts').and.returnValue(_mockPromise);
        scope.dataTableObj.dataTableRef = {};
        scope.dataTableObj.dataTableRef.fnSettings = function () {
        };

        scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
        expect(scope.contactsList.length).toBe(contacts.list.length);
    });

    it('Should test addContactModal', function () {
        //Launch add contact modal
        scope.contactModalObj.updateContactModal = function(contact,attr, cbk){
            expect(contact).toBeNull();
            expect(attr.header).toBe("Add New Contact");
            expect(attr.submit).toBe("Add");
            if(!(cbk instanceof Function))
                expect(false).toBeTruthy();
            scope.onAddCbk = cbk;
        };
        ContactsCtrl.addContact();
    });

    it('Should test editContactModal', function () {
        //Launch update contact modal
        scope.contactModalObj.updateContactModal = function(contact,attr, cbk){
            expect(typeof contact).toBe("object");
            expect(attr.header).toBe("Update Contact");
            expect(attr.submit).toBe("Update");
            if(!(cbk instanceof Function))
                expect(false).toBeTruthy();
        };
        scope.contactsList = contacts.list;
        scope.editContact(0);
    });

    it('Should test onAddNewContact', function () {
        var expectedResponse ={}
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_settingsService, 'addContact').and.callFake(function(){
            expect(arguments[0].type).toEqual('AddOrUpdateApprovedContactRequest');
            expect(arguments[0].list[0].type).toEqual('ApprovedContactInfo');
            return _mockPromise;
        });
        scope.onAddNewContact(contact);
        expect(scope.contactModalObj.resetContactModal).toHaveBeenCalled();
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith("An contact has been created for " + contact.firstName + " " + " " + contact.lastName);
    });

    it('Should test onEditExistingContact', function () {
        var expectedResponse ={}
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_settingsService, 'updateContacts').and.callFake(function(){
            expect(arguments[0].type).toEqual('AddOrUpdateApprovedContactRequest');
            return _mockPromise;
        });
        contact.firstName = "XYZ";
        scope.onEditExistingContact(contact);
        expect(scope.contactModalObj.resetContactModal).toHaveBeenCalled();
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith("Contact " + contact.firstName + " " + contact.lastName + " details have been updated successfully.");
    });

    it('should test checkBoxClicked', function(){
        var event = {};
        scope.dataTableObj.selectRow = jasmine.createSpy('selectRow spy method');
        scope.checkBoxClicked(event);

        expect(scope.dataTableObj.selectRow).toHaveBeenCalled();
    });

    it('should test verifyDeletion',function(){
        scope.verificationModalObj.updateVerificationModel = function(attr, cbk){
            expect(attr.header).toBe("Confirm Deletion");
            expect(attr.submit).toBe("Delete");
            expect(attr.cancel).toBe("Cancel");
            if(!(cbk instanceof Function))
                expect(false).toBeTruthy();
        };

        scope.verificationModalObj.addVerificationModelMessage = function(msg){
            expect(msg).toBe('Deleting contacts cannot be undone!');
        }
        ContactsCtrl.verifyDeletion(contacts.list);
    });

    it('should test processTableRowDeletion when row=1 and row > 1  ',function(){
        var expectedResponse ={}
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        spyOn(_settingsService, 'deleteContacts').and.callFake(function(){
            expect(arguments[0].type).toEqual('DeleteApprovedContactRequest');
            return _mockPromise;
        });

        scope.processTableRowDeletion([contact]);
        expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('Selected contact has been deleted.');

        scope.processTableRowDeletion([contact,contact,contact]);
        expect(scope.dataTableObj.refreshDataTable).toHaveBeenCalled();
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith('All selected contacts have been deleted.');
    });

});