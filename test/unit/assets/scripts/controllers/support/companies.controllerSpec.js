describe('Enterprises Controller', function() {

    var CompaniesCtrl, scope, _dataTableConfigService, _authenticationService,_$controller,_$compile,_$sce,_$location,_$q,_commonUtilityService,_compile,_$rootScope,_AccountService;

    beforeEach(module('app'));

    var countries = [{"_id":"561771933b27e844933060b9","alpha-2":"AF","name":"Afghanistan","countryId":"561771933b27e844933060b9"},{"_id":"561771933b27e844933060ba","name":"Åland Islands","alpha-2":"AX","countryId":"561771933b27e844933060ba"},{"_id":"561771933b27e844933060bb","name":"Albania","alpha-2":"AL","countryId":"561771933b27e844933060bb"},{"_id":"561771933b27e844933060bc","name":"Algeria","alpha-2":"DZ","countryId":"561771933b27e844933060bc"},{"_id":"561771933b27e844933060bd","alpha-2":"AS","name":"American Samoa","countryId":"561771933b27e844933060bd"},{"_id":"561771933b27e844933060be","name":"Andorra","alpha-2":"AD","countryId":"561771933b27e844933060be"},{"_id":"561771933b27e844933060bf","name":"Angola","alpha-2":"AO","countryId":"561771933b27e844933060bf"},{"_id":"561771933b27e844933060c0","alpha-2":"AI","name":"Anguilla","countryId":"561771933b27e844933060c0"},{"_id":"561771933b27e844933060c1","name":"Antarctica","alpha-2":"AQ","countryId":"561771933b27e844933060c1"},{"_id":"561771933b27e844933060c2","name":"Antigua and Barbuda","alpha-2":"AG","countryId":"561771933b27e844933060c2"},{"_id":"561771933b27e844933060c3","name":"Argentina","alpha-2":"AR","countryId":"561771933b27e844933060c3"},{"_id":"561771933b27e844933060c4","alpha-2":"AM","name":"Armenia","countryId":"561771933b27e844933060c4"},{"_id":"561771933b27e844933060c5","name":"Aruba","alpha-2":"AW","countryId":"561771933b27e844933060c5"}];

    var enterprise = {
        "type": "Account",
        "accountId": 5,
        "accountType": "ENTERPRISE",
        "accountName": "Monarch",
        "parentAccountId": 1,
        "accountSubscriptionType": "ENTERPRISE_LITE",
        status:'ACTIVE',
        "company": {
            "type": "Company",
            "name": "Monarch",
            "phone": "8888888888",
            "currency": "USD",
            "timeZone": "-08:00",
            "timeZoneInfo": "the states on the Pacific coast plus Nevada and parts of Idaho",
            "address": {
                "type": "Address",
                "address1": "Monarch Airlines",
                "address2": "Prospect House, Prospect Way",
                "state": "Luton",
                "city": "London",
                "zipCode": "7667",
                "country": "UK"
            }
        },
        "accountDescription": "Monarch Enterprise Account",
        alertVersionSupported:'1.2',
        pricingModelVersionSupported:'1.1'
    };

    var enterprises = {
        "type":"AccountList","list":[
            {"type":"Account","accountId":5,"accountType":"ENTERPRISE","accountName":"Monarch","parentAccountId":1,"accountSubscriptionType":"ENTERPRISE_LITE", status:'ACTIVE',"company":{"type":"Company","name":"Monarch","phone":"8888888888","currency":"USD","timeZone":"-08:00","timeZoneInfo":"the states on the Pacific coast plus Nevada and parts of Idaho","address":{"type":"Address","address1":"Monarch Airlines","address2":"Prospect House, Prospect Way","state":"Luton","city":"London","zipCode":"7667","country":"UK"}},"accountDescription":"Monarch Enterprise Account"},
            {"type":"Account","accountId":9,"accountType":"ENTERPRISE","accountName":"Test","parentAccountId":1,"accountSubscriptionType":"ENTERPRISE_LITE", status:'INACTIVE',"company":{"type":"Company","name":"Test","phone":"8888888888","currency":"USD","timeZone":"+01:00","timeZoneInfo":"main territory of the Netherlands","address":{"type":"Address","address1":"Test Account Address","address2":"Address2","state":"TestState","city":"TestCity","zipCode":"555555","country":"TE"}},"accountDescription":"Limited access Enterprise Account"},
            {"type":"Account","accountId":2,"accountType":"ENTERPRISE","accountName":"Uberr","parentAccountId":1,"accountSubscriptionType":"ENTERPRISE_PRO", status:'SUSPENDED',totalSimCount:3,"company":{"type":"Company","name":"Uberr","phone":"8888888888","currency":"USD","timeZone":"+00:00","timeZoneInfo":"American Samoa, Jarvis Island, Kingman Reef, Midway Atoll and Palmyra Atoll","address":{"type":"Address","address1":"c/o Uber Technologies, Inc","address2":"182 Howard Street # 8","state":"California","city":"San Francisco","zipCode":"94105","country":"US"}},"accountDescription":"Uber Enterprise Account"},
            {"type":"Account","accountId":4,"accountType":"ENTERPRISE","accountName":"Uber","parentAccountId":1,status:'DELETED',"company":{"type":"Company","name":"Uberr","phone":"8888888888","currency":"USD","timeZone":"+00:00","timeZoneInfo":"American Samoa, Jarvis Island, Kingman Reef, Midway Atoll and Palmyra Atoll","address":{"type":"Address","address1":"c/o Uber Technologies, Inc","address2":"182 Howard Street # 8","state":"California","city":"San Francisco","zipCode":"94105","country":"US"}},"accountDescription":"Uber Enterprise Account"},
            {"type":"Account","accountId":3,"accountType":"ENTERPRISE","accountName":"goAir","parentAccountId":1,"accountSubscriptionType":"ENTERPRISE_PRO", status:'SUSPENDED',totalSimCount:3,"accountDescription":"GoAir Enterprise Account"},
            {"type":"Account","accountId":3,"accountType":"ENTERPRISE","accountName":"goAir","parentAccountId":1,"accountSubscriptionType":"ENTERPRISE_PRO",totalSimCount:3,"accountDescription":"GoAir Enterprise Account"}
        ],
        "accountType":"GIGSKY_ROOT","accountName":"Gigsky","accountDescription":"Gigsky Root account","alertVersionSupported":"1.1","accountId":1,
        "count":3,
        "totalCount":3
    };


    beforeEach(inject(function ($rootScope,$injector, $controller, $compile, dataTableConfigService, commonUtilityService, AuthenticationService,$sce,$location,$q,_$compile_,AccountService) {
        _$controller=$controller;
        _$compile=$compile;
        _$sce=$sce;
        _$location=$location;
        _$q=$q;
        _compile=_$compile_;
        _$rootScope=$rootScope;
        scope = $rootScope.$new();
        window.clearDataTableStatus = jasmine.createSpy();
        _dataTableConfigService = dataTableConfigService;
        _authenticationService = AuthenticationService;
        _commonUtilityService=commonUtilityService;
        _AccountService=AccountService;


        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');
        //scope.dataTableObj.refreshDataTable = function(){};
    }));

    it('should verify the trustAsHtml function', function () {
        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService,$sce:_$sce});

        CompaniesCtrl.trustAsHtml();

        var actualMsg = _$sce.getTrustedHtml(CompaniesCtrl.accStatusFilter.name);
        expect(actualMsg).toBe('ACTIVE');

    });

    it('should verify the emptyTableMessage set', function () {
        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService,$sce:_$sce});


        CompaniesCtrl.trustAsHtml();

        expect(scope.emptyTableMessage).toBe('No companies available.');

    });

    it('should test the accStatusFilter set by default', function () {
        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService,$sce:_$sce,$location:_$location});

        expect(CompaniesCtrl.accStatusFilter.name).toBe('ACTIVE');
    });

    it('should test the accStatusFilter set on page refresh for ALL filter', function () {
        _$location.search('asf',"0");

        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService,$sce:_$sce,$location:_$location});

        expect(CompaniesCtrl.accStatusFilter.name).toBe('ALL');
    });

    it('should test the accStatusFilter set on page refresh for ACTIVE filter', function () {
        _$location.search('asf',"1");

        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService,$sce:_$sce,$location:_$location});

        expect(CompaniesCtrl.accStatusFilter.name).toBe('ACTIVE');
    });

    it('should test the accStatusFilter set on page refresh for INACTIVE filter', function () {
        _$location.search('asf',"2");

        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService,$sce:_$sce,$location:_$location});

        expect(CompaniesCtrl.accStatusFilter.name).toBe('INACTIVE');
    });

    it('should test the accStatusFilter set on page refresh for SUSPENDED filter', function () {
        _$location.search('asf',"3");

        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService,$sce:_$sce,$location:_$location});

        expect(CompaniesCtrl.accStatusFilter.name).toBe('SUSPENDED');
    });

    it('should test getCountries on success', function () {
        var expectedResponse = {"countryList": countries};

        var _mockPromise={
            success:function (fn){
                fn(expectedResponse);
                return _mockPromise;
            },
            error:function (fn){
                return _mockPromise;
            }
        };

        spyOn(_commonUtilityService,"getCountries").and.returnValue(_mockPromise);

        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService,commonUtilityService:_commonUtilityService,$sce:_$sce,$location:_$location});

        expect(CompaniesCtrl.countries).toEqual(expectedResponse.countryList)
    });

    it('should test getCountries on failure', function () {

        var errorResponse = {errorStr:'getCountries are failed'};

        var _mockPromise={
            success:function (fn){
                return _mockPromise;
            },
            error:function (fn){
                fn(errorResponse);
                return _mockPromise;
            }
        };

        spyOn(_commonUtilityService,"getCountries").and.returnValue(_mockPromise);

        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService,commonUtilityService:_commonUtilityService,$sce:_$sce,$location:_$location});

        expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith(errorResponse.errorStr);

    });



    describe('it should test the dataTable configurations', function () {

        it('should set dataTable object headers', function () {
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            expect(scope.dataTableObj.tableHeaders[0].value).toBe('Name');
            expect(scope.dataTableObj.tableHeaders[0].DbValue).toBe('accountName');
            expect(scope.dataTableObj.tableHeaders[1].value).toBe('Status');
            expect(scope.dataTableObj.tableHeaders[1].DbValue).toBe('status');
            expect(scope.dataTableObj.tableHeaders[2].value).toBe('Country');
            expect(scope.dataTableObj.tableHeaders[2].DbValue).toBe('country');
            expect(scope.dataTableObj.tableHeaders[3].value).toBe('Subscription');
            expect(scope.dataTableObj.tableHeaders[3].DbValue).toBe('accountSubscriptionType');
            expect(scope.dataTableObj.tableHeaders[4].value).toBe('Currency');
            expect(scope.dataTableObj.tableHeaders[4].DbValue).toBe('currency');
            expect(scope.dataTableObj.tableHeaders[5].value).toBe('No. of SIMs');
            expect(scope.dataTableObj.tableHeaders[5].DbValue).toBe('totalSimCount');
            expect(scope.dataTableObj.tableHeaders[6].value).toBe('View As');
            expect(scope.dataTableObj.tableHeaders[6].DbValue).toBe('');
        });

        it('should set tableID', function () {
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            expect(scope.dataTableObj.tableID).toBe('companiesDataTable');
        });

        it('should set selectedDisplayLength', function () {
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            expect(scope.dataTableObj.filters.selectedDisplayLength).toBe(100);
        });

        it('should compile data table rows', function (){
            var data,row,index;
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});
            row=enterprise;
            data=enterprise;

            scope.dataTableObj.dataTableOptions.createdRow(row,data,index);
            _compile(row)(scope);
        });

        it('should test the orderable property for the dataTables', function () {
            var data,row,index;
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            expect(scope.dataTableObj.dataTableOptions.columns[0].orderable).toBeTruthy();
            expect(scope.dataTableObj.dataTableOptions.columns[1].orderable).toBeFalsy();
            expect(scope.dataTableObj.dataTableOptions.columns[2].orderable).toBeFalsy();
            expect(scope.dataTableObj.dataTableOptions.columns[3].orderable).toBeFalsy();
            expect(scope.dataTableObj.dataTableOptions.columns[4].orderable).toBeFalsy();
            expect(scope.dataTableObj.dataTableOptions.columns[5].orderable).toBeFalsy();
            expect(scope.dataTableObj.dataTableOptions.columns[6].orderable).toBeFalsy();

        });

        it('should test data-table columnDefs 0',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var data,type,row,meta;
            meta = {
                row:0
            };
            row = enterprise;
            expect(scope.dataTableObj.dataTableOptions.columnDefs[0].render(data,type,row,meta)).toBe('<a ui-sref="support.enterprise.info({accountId: '+row.accountId+'})" class="cursor">'+row.accountName+'</a>');
        });

        it('should test data-table columnDefs 1 when status is ACTIVE',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var data,type,row,meta;
            meta = {
                row:1
            };
            row = enterprises.list[0];
            var entStatus = 'active';
            expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toBe('<span class="enterprise-status '+entStatus+'">'+entStatus+'</span>');
        });

        it('should test data-table columnDefs 1 when status is INACTIVE',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var data,type,row,meta;
            meta = {
                row:1
            };
            row = enterprises.list[1];
            var entStatus = 'inactive';
            expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toBe('<span class="enterprise-status '+entStatus+'">'+entStatus+'</span>');
        });

        it('should test data-table columnDefs 1 when status is SUSPENDED',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var data,type,row,meta;
            meta = {
                row:1
            };
            row = enterprises.list[2];
            var entStatus = 'suspended';
            expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toBe('<span class="enterprise-status '+entStatus+'">'+entStatus+'</span>');
        });

        it('should test data-table columnDefs 1 when status is other than active, inactive or suspended',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var data,type,row,meta;
            meta = {
                row:1
            };
            row = enterprises.list[3];
            var entStatus = 'DELETED';
            expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toBe('<span class="enterprise-status '+entStatus+'">'+entStatus+'</span>');
        });

        it('should test data-table columnDefs 1 when status is not defined',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var data,type,row,meta;
            meta = {
                row:1
            };
            row = enterprises.list[5];
            var entStatus = 'Not Available';
            expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toBe('<span class="enterprise-status '+entStatus+'">'+entStatus+'</span>');
        });


        it('should test data-table columnDefs 2',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var data,type,row,meta;
            meta = {
                row:2
            };
            row = enterprise;

            var countryObj = {code : 'UK' ,name : 'UK'};
            spyOn(_commonUtilityService,"getCountryObject").and.returnValue(countryObj);

            var country = row.company.address.country;
            expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data,type,row,meta)).toBe('<span>'+country+'</span>');
        });

        it('should test data-table columnDefs 3 for account subscription of  Enterprise Lite',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var data,type,row,meta;
            meta = {
                row:3
            };
            row = enterprise;
            expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toBe('<span>GEM LITE</span>');
        });

        it('should test data-table columnDefs 3 for account subscription of  Enterprise Pro',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var data,type,row,meta;
            meta = {
                row:3
            };
            row = enterprises.list[2];
            expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toBe('<span>GEM PRO</span>');
        });

        it('should test data-table columnDefs 3 when account subscription is not defined',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var data,type,row,meta;
            meta = {
                row:3
            };
            row = enterprises.list[3];
            expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toBe('<span>GEM PRO</span>');
        });

        it('should test data-table columnDefs 4 ',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var data,type,row,meta;
            meta = {
                row:4
            };
            row = enterprise;
            expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data,type,row,meta)).toBe('<span>'+row.company.currency+'</span>');
        });

        it('should test data-table columnDefs 5 when totalSimCount is present',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var data,type,row,meta;
            meta = {
                row:5
            };
            row = enterprises.list[2];
            expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row,meta)).toBe('<span>'+row.totalSimCount+'</span>');
        });

        it('should test data-table columnDefs 5 when totalSimCount is absent',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var data,type,row,meta;
            meta = {
                row:5
            };
            row = enterprise;
            expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row,meta)).toBe('<span>0</span>');
        });

        it('should test data-table columnDefs 6 ',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var data,type,row,meta;
            meta = {
                row:6
            };
            row = enterprise;
            expect(scope.dataTableObj.dataTableOptions.columnDefs[6].render(data,type,row,meta)).toBe('<a ng-click=\"prepareNavigationToGem('+ meta.row +')\" class="cursor">View As</a>');
        });

        it('should test fnServerData _authenticationService.getSubAccounts success',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var sSource, aoData;
            var _mockPromise = {
                success: function(fn) {
                    fn(enterprises);
                    return _mockPromise;
                },
                error: function(fn) {
                    return _mockPromise;
                }
            };
            spyOn(_authenticationService, 'getSubAccounts').and.returnValue(_mockPromise);
            scope.dataTableObj.dataTableRef ={};
            sessionStorage.setItem("rootAccountId",1);

            function fnCallback(){

            }
            scope.dataTableObj.dataTableRef.fnSettings = function (){
                return {oLanguage:{}};
            };

            scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
            expect(scope.companiesList.length).toBe(enterprises.list.length);
            expect(_authenticationService.getSubAccounts).toHaveBeenCalledWith('1',{depth:'IMMEDIATE', status:'ACTIVE'});

            scope.dataTableObj.dataTableRef.fnSettings = function(){};
            aoData = [{name:'draw',value:1},{name:"columns",value:1},{name:'order',value:1},{name:'start',value:0},{name:'length',value:10},{name:'search',value:{value:'Uber'}}];
            scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
            expect(_authenticationService.getSubAccounts).toHaveBeenCalledWith('1',{startIndex: 0, count: 10, search: 'Uber',depth:'IMMEDIATE', status:'ACTIVE'});
            expect(CompaniesCtrl.searchValue).toBe('Uber');


            sessionStorage.removeItem("rootAccountId");
        });

        it('should test fnServerData when _authenticationService.getSubAccounts fails',function(){
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

            var sSource, aoData;
            var errorResponse = {errorStr:'getSubAccounts are failed'};
            var _mockPromise = {
                success: function(fn) {
                    return _mockPromise;
                },
                error: function(fn) {
                    fn(errorResponse);
                    return _mockPromise;
                }
            };
            spyOn(_authenticationService, 'getSubAccounts').and.returnValue(_mockPromise);
            scope.dataTableObj.dataTableRef ={};
            sessionStorage.setItem("rootAccountId",1);

            scope.dataTableObj.dataTableRef.fnSettings = function (){
                return {oLanguage:{}};
            };

            function fnCallback(){

            }

            scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
            expect(_authenticationService.getSubAccounts).toHaveBeenCalledWith('1',{depth:'IMMEDIATE', status:'ACTIVE'});
            expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith(errorResponse.errorStr);

            sessionStorage.removeItem("rootAccountId");
        });


        it('should test fnDrawCallback', function () {
            CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});
            scope.companiesList = enterprises.list;

            _dataTableConfigService.disableTableHeaderOps = function(length,oSettings){
                expect(length).toBe(6);
            };

            scope.dataTableObj.dataTableOptions.fnDrawCallback('oSettings');

        })
    });

    it('should test prepareNavigationToGem function', function () {
        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});
        scope.companiesList = enterprises.list;
        scope.prepareNavigationToGem(0);

        expect(_$rootScope.isNavigatedThroughAdmin).toBeTruthy();
        expect(sessionStorage.getItem('isNavigatedThroughAdmin')).toBeTruthy();
        expect(sessionStorage.getItem('accountId')).toBe(scope.companiesList[0].accountId+'');
        expect(sessionStorage.getItem('gs-enterpriseCurrency')).toBe(scope.companiesList[0].company.currency);
        expect(sessionStorage.getItem('exit-to-state')).toBe('support.accounts.companies');

        sessionStorage.removeItem('isNavigatedThroughAdmin');
        sessionStorage.removeItem('accountId');
        sessionStorage.removeItem('gs-enterpriseCurrency');
        sessionStorage.removeItem('exit-to-state');
    });

    it('should test prepareNavigationToGem function when no currency found', function () {
        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});
        scope.companiesList = enterprises.list;
        scope.prepareNavigationToGem(4);

        expect(_$rootScope.isNavigatedThroughAdmin).toBeTruthy();
        expect(sessionStorage.getItem('isNavigatedThroughAdmin')).toBeTruthy();
        expect(sessionStorage.getItem('accountId')).toBe(scope.companiesList[4].accountId+'');
        expect(sessionStorage.getItem('gs-enterpriseCurrency')).toBe('');
        expect(sessionStorage.getItem('exit-to-state')).toBe('support.accounts.companies');

        sessionStorage.removeItem('isNavigatedThroughAdmin');
        sessionStorage.removeItem('accountId');
        sessionStorage.removeItem('gs-enterpriseCurrency');
        sessionStorage.removeItem('exit-to-state');
    });

    it('should test navigateToGem function when authenticationService.getEnterpriseAccountDetails is success', function () {
        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

        var _mockPromise = {
            success: function(fn) {
                fn(enterprise);
                return _mockPromise;
            },
            error: function(fn) {
                return _mockPromise;
            }
        };
        spyOn(_authenticationService, 'getEnterpriseAccountDetails').and.returnValue(_mockPromise);

        spyOn(_authenticationService,'setAlertsFeatureEnabled');
        spyOn(_authenticationService,'setPricingModelVersion');

        CompaniesCtrl.navigateToGem(5);
        expect(_authenticationService.getEnterpriseAccountDetails).toHaveBeenCalledWith(5);
        expect(_authenticationService.setAlertsFeatureEnabled).toHaveBeenCalledWith(1.2+'');
        expect(_authenticationService.setPricingModelVersion).toHaveBeenCalledWith(1.1+'');
    });

    it('should test navigateToGem function when authenticationService.getEnterpriseAccountDetails fails', function () {
        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});

        var errorResponse = {errorStr:'getEnterpriseAccountDetails are failed'};
        var _mockPromise = {
            success: function(fn) {
                return _mockPromise;
            },
            error: function(fn) {
                fn(errorResponse);
                return _mockPromise;
            }
        };
        spyOn(_authenticationService, 'getEnterpriseAccountDetails').and.returnValue(_mockPromise);

        CompaniesCtrl.navigateToGem(5);
        expect(_authenticationService.getEnterpriseAccountDetails).toHaveBeenCalledWith(5);
        expect(CompaniesCtrl.errorMessage).toBe(errorResponse.errorStr);

    });

    it('should test addEnterpriseModal function', function () {
        var expectedResponse = {"countryList": countries};

        var _mockPromise={
            success:function (fn){
                fn(expectedResponse);
                return _mockPromise;
            },
            error:function (fn){
                return _mockPromise;
            }
        };

        spyOn(_commonUtilityService,"getCountries").and.returnValue(_mockPromise);

        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});
        scope.enterpriseModalObj.addEnterpriseModal = function (countries, obj, submit) {
            expect(countries).toEqual(expectedResponse.countryList);
            expect(obj).toEqual({header: "Add Enterprise", submit: "Create", cancel: "Cancel"});
            expect(submit).toBe(CompaniesCtrl.onAddNewEnterprise);
        };
        CompaniesCtrl.addEnterprise();

    });

    it('should test refreshEnterpriseTable function when no change in accStatusFilter', function () {
        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});
        scope.dataTableObj.refreshDataTable = function (refresh) {
            expect(refresh).toBeTruthy();
        };
        CompaniesCtrl.refreshEnterpriseTable();
    });

    it('should test refreshEnterpriseTable function when there is change in accStatusFilter', function () {
        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService});
        scope.dataTableObj.refreshDataTable = function (refresh) {
            expect(refresh).toBeTruthy();
        };

        _$location.search('asf',"2");

        CompaniesCtrl.refreshEnterpriseTable();
    });

    it('should test onAddNewEnterprise function when addEnterprise is success', function () {
        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService,AccountService:_AccountService});

        var _mockPromise = {
            success: function(fn) {
                fn(enterprise);
                return _mockPromise;
            },
            error: function(fn) {
                return _mockPromise;
            }
        };
        spyOn(_AccountService, 'addEnterprise').and.returnValue(_mockPromise);

        scope.dataTableObj.refreshDataTable = function (refresh) {
            expect(refresh).toBeFalsy();
        };
        scope.enterpriseModalObj.resetEnterpriseModal = function(){

        };

        CompaniesCtrl.onAddNewEnterprise(enterprise);
        expect(_commonUtilityService.showSuccessNotification).toHaveBeenCalledWith("Enterprise "+enterprise.accountName+ " has been created.");

    });

    it('should test onAddNewEnterprise function when addEnterprise fails', function () {
        CompaniesCtrl = _$controller("CompaniesCtrl", {$scope: scope, $compile: _$compile, dataTableConfigService: _dataTableConfigService, authenticationService: _authenticationService,AccountService:_AccountService});
        var errorResponse = {errorStr:'getEnterpriseAccountDetails are failed'};

        var _mockPromise = {
            success: function(fn) {
                return _mockPromise;
            },
            error: function(fn) {
                fn(errorResponse);
                return _mockPromise;
            }
        };
        spyOn(_AccountService, 'addEnterprise').and.returnValue(_mockPromise);

        scope.enterpriseModalObj.resetEnterpriseModal = function(){

        };

        CompaniesCtrl.onAddNewEnterprise(enterprise);
        expect(_commonUtilityService.showErrorNotification).toHaveBeenCalledWith(errorResponse.errorStr);

    });
});