describe('Sim cards Controller', function() {

    var SIMCardsCtrl, scope, _dataTableConfigService, _simService, _settingsService, _simsCommonFactory;

    beforeEach(module('app'));

    var sim = {
        "type": "Sim",
        "simId": "1001100000000000010",
        "simType": "GIGSKY_SIM",
        "status": "ACTIVE",
        "user": {
            "type": "User",
            "userId": 2,
            "firstName": "uberAdmin",
            "lastName": "uberAdmin"
        },
        "account": {
            "type": "ENTERPRISE",
            "accountId": 2,
            "accountName": "Uber"
        },
        "supplier": "BLUEFISH",
        "nickName": "SIM-0010",
        "creditLimitList": {
            "type": "CreditLimitList",
            "list": [{
                "type": "CreditLimit",
                "limitType": "SOFT",
                "limitKB": 10240,
                "limitDuration": "MONTH",
                "alertPercentages": [25],
                "alertUserByMail": true,
                "alertEnabled": true
            }]
        },
        "dataUsedInBytes": 0
    };

    var sims = {
        "type": "SimList",
        "totalCount": 3,
        "count": 3,
        "list": [sim,sim,sim]
    };

    beforeEach(inject(function ($rootScope,$injector, $controller, $compile, dataTableConfigService, commonUtilityService, SIMService, SettingsService, simsCommonFactory) {
        scope = $rootScope.$new();

        var response = 2;
        var _mockPromise = {
            success: function(fn) {
                fn(response);
                return _mockPromise;
            },
            error: function(fn) {
                fn(response);
                return _mockPromise;
            }
        };
        spyOn(SettingsService, 'getZonesCount').and.returnValue(_mockPromise);
        window.clearDataTableStatus = jasmine.createSpy();

        SIMCardsCtrl = $controller("SIMCardsCtrl", {$scope: scope, $compile: $compile, dataTableConfigService: dataTableConfigService, SIMService:SIMService, SettingsService:SettingsService,simsCommonFactory :simsCommonFactory});
        _dataTableConfigService = dataTableConfigService;
        _simService = SIMService;
        _settingsService = SettingsService;
        _simsCommonFactory = simsCommonFactory;

        //expect(SIMCardsCtrl.zonesCount).toBe(2);
        scope.dataTableObj.refreshDataTable = jasmine.createSpy('refreshDataTable spy method');

    }));

    it('Should test data-table columnDefs 0',function(){
        var data,type,row,meta;
        meta = {
            row:0
        };
        row = sim;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[0].render(data,type,row,meta)).toContain(row.simId);
    });

    it('Should test data-table columnDefs 1',function(){
        var data,type,row,meta;
        meta = {
            row:1
        };
        row = sim;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[1].render(data,type,row,meta)).toContain('Active');
    });

    it('Should test data-table columnDefs 2',function(){
        var data,type,row,meta;
        meta = {
            row:2
        };
        row = sim;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data,type,row,meta)).toContain(row.user.firstName);
        expect(scope.dataTableObj.dataTableOptions.columnDefs[2].render(data,type,row,meta)).toContain(row.user.lastName);
    });

    it('Should test data-table columnDefs 3',function(){
        var data,type,row,meta;
        meta = {
            row:3
        };
        row = sim;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[3].render(data,type,row,meta)).toContain("Active");
    });

    it('Should test data-table columnDefs 4',function(){
        var data,type,row,meta;
        meta = {
            row:4
        };
        row = sim;
        expect(scope.dataTableObj.dataTableOptions.columnDefs[4].render(data,type,row,meta)).toContain(row.dataUsedInBytes);
    });

    it('Should test data-table columnDefs 5',function(){
        var data,type,row,meta;
        meta = {
            row:5
        };
        row = sim;
        spyOn(_simsCommonFactory, 'getZoneStatusForSim').and.returnValue("Full Access");
        expect(scope.dataTableObj.dataTableOptions.columnDefs[5].render(data,type,row,meta)).toContain("Full Access");
    });

    it('Should test fnServerData',function(){
        var sSource, aoData, fnCallback;
        var _mockPromise = {
            success: function(fn) {
                fn(sims);
                return _mockPromise;
            },
            error: function(fn) {
                fn(sims);
                return _mockPromise;
            }
        };
        spyOn(_simService, 'getSIMs').and.returnValue(_mockPromise);
        scope.dataTableObj.dataTableRef ={};
        scope.dataTableObj.dataTableRef.fnSettings = function(){};

        scope.dataTableObj.dataTableOptions.fnServerData(sSource, aoData, fnCallback);
        expect(scope.simList.length).toBe(sims.list.length);
    });

    it('should test zoneStatus function',function(){
        scope.simList = sims.list;
        spyOn(_simsCommonFactory, 'showZoneStatusModal');
        SIMCardsCtrl.zoneStatus(1);
        expect(_simsCommonFactory.showZoneStatusModal).toHaveBeenCalledWith(sim,2);
    });

});