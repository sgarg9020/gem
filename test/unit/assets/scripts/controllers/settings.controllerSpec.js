describe('settings Controller', function() {

    var _commonUtilityService,_SIMService,SettingsCtrl,scope,_SettingsService,_rootScope,_q,_timeout;
    var enterpriseId = "1234";

    beforeEach(module('app'));

    beforeEach(inject(function($injector,$rootScope,$sce,$q,$controller,$location,SettingsService,SIMService,commonUtilityService,$timeout){
        scope ={};

        SettingsCtrl = $controller("SettingsCtrl",{$scope:scope,$rootScope:$rootScope,$q:$q,$sce:$sce,$location:$location,SettingsService:SettingsService,SIMService:SIMService,commonUtilityService:commonUtilityService});

        _commonUtilityService = commonUtilityService;
        _SIMService = SIMService;
        _SettingsService = SettingsService;
        _rootScope = $rootScope;
        _q = $q;
        _timeout =$timeout;
        spyOn(_commonUtilityService, 'showErrorNotification');
        spyOn(_commonUtilityService, 'showSuccessNotification');

    }));

    it('Should test controller',function(){
        expect(true).toBe(true);
    });
/*
    it('On pricing detail',function(){
        var expectedResponse ={"_id":"561771933b27e844933060c6","lastName":"Dande","emailId":"jdande@gigsky.com","group":{"_id":"55efdc6360f02ca57c934e92","name":"California","parentGroupId":"55efdc6360f02ca57c934e90","type":"Group","groupId":"55efdc6360f02ca57c934e92"},"location":"India","enterprise":"55efdc6360f02ca22c934e90","firstName":"Jagadish","country":null,"type":"User","userId":"561771933b27e844933060c6"};
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };

        spyOn(_SettingsService, 'getPlanDetails').and.returnValue(_mockPromise);
        SettingsCtrl.onPricingDetails();

        expect(_SettingsService.getPlanDetails).toHaveBeenCalledWith();
    });

    it('details updateEnterpriseDetails',function(){
        var expectedResponse ={"_id":"561771933b27e844933060c6","lastName":"Dande","emailId":"jdande@gigsky.com","group":{"_id":"55efdc6360f02ca57c934e92","name":"California","parentGroupId":"55efdc6360f02ca57c934e90","type":"Group","groupId":"55efdc6360f02ca57c934e92"},"location":"India","enterprise":"55efdc6360f02ca22c934e90","firstName":"Jagadish","country":null,"type":"User","userId":"561771933b27e844933060c6"};
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        scope.isFormValid = function(){
            return true;
        };
        spyOn(_SettingsService, 'updateEnterpriseDetails').and.returnValue(_mockPromise);
        SettingsCtrl.details.updateEnterpriseDetails()
        expect(_SettingsService.updateEnterpriseDetails).toHaveBeenCalled();
    });

    it('On alert config',function(){
        var expectedResponse ={"alertIntervals":[{"_id":"5649a02954b038822a8ebcf5","time":1,"interval":"Every Hour"},{"time":2,"interval":"Every 2 Hours","_id":"5649a02954b038822a8ebcf4"},null,{"time":3,"interval":"Every 3 Hours","_id":"5649a02954b038822a8ebcf3"},{"time":4,"interval":"Every 4 Hours","_id":"5649a02954b038822a8ebcf2"},{"time":6,"interval":"Every 6 Hours","_id":"5649a02954b038822a8ebcf1"},{"interval":"Every 8 Hours","_id":"5649a02954b038822a8ebcf0","time":8},{"time":12,"interval":"Every 12 Hours","_id":"5649a02954b038822a8ebcef"},{"time":24,"interval":"Every 24 Hours","_id":"5649a02954b038822a8ebcee"}],"simAlertUsage":[{"_id":"5649a02954b038822a8ebcf8","percentage":0.7,"qualifier":"Usage is >= 70% of SIM allocation"},{"qualifier":"Usage is >= 80% of SIM allocation","_id":"5649a02954b038822a8ebcf7","percentage":0.8},{"percentage":0.9,"qualifier":"Usage is >= 90% of SIM allocation","_id":"5649a02954b038822a8ebcf6"}]};
        spyOn(_SettingsService,"getPossibleAlertConfigs").and.callFake(function(){
           // var expectedResponse ={"_id":"55efdc6360f02ca22c934e90","companyName":"GigskyIndiaPvtLtd","address":"Aditya Plaza, N-33, 10th Cross, 10th Main Rd, HAL 3rd Stage, LIC Colony, Sector 11, New ThippasandraBengaluru","city":"Bengaluru","state":"Karnataka","postalCode":"560075","currency":"U.S.Dollar","country":null,"enterpriseId":"55efdc6360f02ca22c934e90"};

            var d = _q.defer();
            d.resolve({data:expectedResponse});
            return d.promise;
        });

        spyOn(_SettingsService,"getAlertConfig").and.callFake(function(){
            var expectedResponse1 ={"_id":"5649a029c8f04894645ec911","enterprise":{"_id":"55efdc6360f02ca22c934e90","companyName":"GigskyIndiaPvtLtd","enterpriseId":"55efdc6360f02ca22c934e90"},"alertAccount":true,"alertuser":true,"trigger":{"percentage":0.8,"qualifier":"Usage is >= 80% of SIM allocation"},"frequency":{"interval":"Every 4 Hours","time":4}};

            var d1 = _q.defer();
            d1.resolve({data:expectedResponse1});
            return d1.promise;
        });

        //spyOn(_SettingsService, 'getPlanDetails').and.returnValue(_mockPromise);
        SettingsCtrl.onAlertConfig();
        _timeout(function(){expect(SettingsCtrl.alertIntervals).toEqual(expectedResponse.alertIntervals)});
    });

    it('varify submit alert config',function(){
        var enterpriseSettings = {
            "alertConfig" : {
                "interval" : {
                    "time":1,
                    "interval":"Every Hour"
                } ,
                "qualifier" : {
                    "percentage" : 75,
                    "qualifie" : "Usage is >= 75% of SIM allocation"
                }
            }

        };
        SettingsCtrl.alertConfig = enterpriseSettings.alertConfig;
        var expectedResponse ="OK";
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        scope.isFormValid = function(){
            return true;
        };
        spyOn(_SettingsService, 'updateEnterpriseDetails').and.returnValue(_mockPromise);
        SettingsCtrl.details.submitAlertConfig();
        expect(_SettingsService.updateEnterpriseDetails).toHaveBeenCalled();
    });
    it('varify submit alert config form invalid',function(){
        var expectedResponse ="OK";
        var _mockPromise = {
            success: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            },
            error: function(fn) {
                fn(expectedResponse);
                return _mockPromise;
            }
        };
        scope.isFormValid = function(){
            return false;
        };
        spyOn(_SettingsService, 'submitAlertConfig').and.returnValue(_mockPromise);
        SettingsCtrl.details.submitAlertConfig();
        expect(_SettingsService.submitAlertConfig).not.toHaveBeenCalled();
    });*/
});