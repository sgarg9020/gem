describe('Testing the dashboard directive', function() {

    var scope,compile,location,id,rootScope,_debounce,_loadingState;
    id = "tab-ent-day";
    beforeEach(function() {
        module('app', function ($provide) {

        });
    });

    beforeEach(inject(function($rootScope, $compile,$location,debounce,loadingState) {
            compile = $compile;
            location = $location;
            rootScope = $rootScope;
            _debounce = debounce;
            _loadingState=loadingState;
            scope = $rootScope.$new();
            scope.vm = {};
            scope.vm.options = {chart: {}};

            scope.vm.api = {};
            scope.vm.requestData = function (callback) {
                callback(scope.vm, scope.vm);
            };
        }
    ));

    it('should verify init', function () {
        var elementString = '<div class="pie-or-donut" gs-dashboard-graph></div>';
        var element = angular.element(elementString);
        scope.$parent.graphObj={};
        scope.$parent.graphObj.options={chart:{}};
        scope.$parent.graphObj.updateGraphOptions = function()
        {
            return;
        };
        scope.graphObj.requestData = function()
        {
            return;
        };
        compile(element)(scope);
        scope.$digest();
    });
    it('should verify init', function () {
        var elementString = '<div class="pie-or-donut" gs-dashboard-graph></div>';
        var element = angular.element(elementString);
        scope.$parent.graphObj={};
        scope.$parent.graphObj.options={chart:{}};
        scope.$parent.graphObj.updateGraphOptions =undefined;
        scope.graphObj.requestData = function()
        {
            return;
        };
        compile(element)(scope);
        scope.$digest();
    });
    it('should verify init', function () {
        var elementString = '<div class="pie-or-donut" gs-dashboard-graph></div>';
        var element = angular.element(elementString);
        scope.$parent.graphObj={};
        scope.$parent.graphObj.options={chart:{}};
        scope.$parent.graphObj.updateGraphOptions =function()
        {
            return;
        };;
        scope.graphObj.requestData = function()
        {
            return;
        };
        compile(element)(scope);
        scope.$digest();
        scope.$parent.graphObj.options.chart.callback();
    });
    it('should verify init', function () {
        var elementString = '<div class="pie-or-donut" gs-dashboard-graph></div>';
        var element = angular.element(elementString);
        scope.$parent.graphObj={};
        scope.$parent.graphObj.options={chart:{}};
        scope.$parent.graphObj.updateGraphOptions =function()
        {
            return;
        };;
        scope.graphObj.requestData = function()
        {
            return;
        };
        compile(element)(scope);
        scope.$digest();
        scope.$parent.graphObj.options.chart.callback();
        scope.$parent.graphObj.options.chart.callback();
    });
});