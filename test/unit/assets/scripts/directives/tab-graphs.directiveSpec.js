describe('Testing the tab graphs directive', function() {

    var scope,compile,location,id,rootScope,_debounce,_loadingState;
    id = "tab-ent-day";
    beforeEach(function() {
        module('app', function ($provide) {

        });
    });

    beforeEach(inject(function($rootScope, $compile,$location,debounce,loadingState) {
            compile = $compile;
            location = $location;
            rootScope = $rootScope;
            _debounce = debounce;
            _loadingState=loadingState;
            scope = $rootScope.$new();
            scope.vm = {};
            scope.vm.options = {chart: {}};

            scope.vm.api = {};
            scope.vm.requestData = function (callback) {
                callback(scope.vm, scope.vm);
            };
        }
    ));

    it('should verify init', function () {
        var elementString = '<div class="active"><div gs-tab-graphs gs-graph-id="tab-ent-day"></div></div>';
        var element = angular.element(elementString);
        scope.$parent.graphObj={};
        scope.$parent.graphObj.options={chart:{}};
        scope.$parent.graphObj.updateGraphOptions = function()
        {
            return;
        };
        scope.graphObj.requestData = function()
        {
            return;
        };
        spyOn(scope.$parent.graphObj,"updateGraphOptions");
        spyOn(scope.graphObj,"requestData");
        compile(element)(scope);
        scope.$digest();
        //expect(location.search).toHaveBeenCalled();
        expect(scope.$parent.graphObj.updateGraphOptions).toHaveBeenCalled();
    });
    it('should verify callback call', function () {
        var elementString = '<div class="active"><div gs-tab-graphs gs-graph-id="tab-ent-day"></div></div>';
        var element = angular.element(elementString);
        scope.$parent.graphObj={};
        scope.$parent.graphObj.options={chart:{}};
        scope.$parent.graphObj.updateGraphOptions = function()
        {
            return;
        };
        scope.graphObj.requestData = function()
        {
            return;
        };
        spyOn(_loadingState,'hide');
        spyOn(scope.$parent.graphObj,"updateGraphOptions");
        spyOn(scope.graphObj,"requestData");
        compile(element)(scope);
        scope.$digest();
        scope.$parent.graphObj.options.chart.callback();
        //expect(location.search).toHaveBeenCalled();
        expect(_loadingState.hide).toHaveBeenCalled();
        expect(scope.$parent.graphObj.updateGraphOptions).toHaveBeenCalled();
    });
   it('should verify callback call twice', function () {
        var elementString = '<div class="active"><div gs-tab-graphs gs-graph-id="tab-ent-day"></div></div>';
        var element = angular.element(elementString);
        scope.$parent.graphObj={};
        scope.$parent.graphObj.options={chart:{}};
        scope.$parent.graphObj.updateGraphOptions = function()
        {
            return;
        };
        scope.graphObj.requestData = function()
        {
            return;
        };
        spyOn(_loadingState,'hide');
        spyOn(scope.$parent.graphObj,"updateGraphOptions");
        spyOn(scope.graphObj,"requestData");
        compile(element)(scope);
        scope.$digest();
        scope.$parent.graphObj.options.chart.callback();
        scope.$parent.graphObj.options.chart.callback();
        //expect(location.search).toHaveBeenCalled();
        expect(_loadingState.hide).toHaveBeenCalled();
        expect(scope.$parent.graphObj.updateGraphOptions).toHaveBeenCalled();
    });
    it('should verify resize callback', function () {
        var elementString = '<div class="active"><div gs-tab-graphs gs-graph-id="tab-ent-day"></div></div>';
        var element = angular.element(elementString);
        scope.$parent.graphObj={};
        scope.$parent.graphObj.options={chart:{}};
        scope.$parent.graphObj.updateGraphOptions = function()
        {
            return;
        };
        scope.graphObj.requestData = function()
        {
            return;
        };
        spyOn(scope.$parent.graphObj,"updateGraphOptions");
        spyOn(scope.graphObj,"requestData");
        spyOn(_debounce, 'set').and.callFake(function(myParam) {
            myParam();
        });
        compile(element)(scope);
        scope.$digest();
        window.innerHeight =200;
        window.resizeTo(1000,window.innerWidth);

        expect(_debounce.set).toHaveBeenCalled();
        expect(scope.$parent.graphObj.updateGraphOptions).toHaveBeenCalled();
    });
    it('should verify redrawGraph callback', function () {
        var elementString = '<div class="active"><div gs-tab-graphs gs-graph-id="tab-ent-day"></div></div>';
        var element = angular.element(elementString);
        scope.$parent.graphObj={};
        scope.$parent.graphObj.options={chart:{}};
        var viewModel={api:{getScope:function(){return {chart:true}},updateWithOptions:function(){},options:{}}};
        scope.$parent.graphObj.updateGraphOptions = function()
        {
            return;
        };
        scope.graphObj.requestData = function()
        {
            return;
        };
        spyOn(_loadingState,'hide');
        spyOn(viewModel.api,"updateWithOptions");
        spyOn(scope.$parent.graphObj,"updateGraphOptions");
        spyOn(scope.graphObj,"requestData").and.callFake(function(myParam) {
            myParam(viewModel);
        });
        compile(element)(scope);
        scope.$digest();
        //expect(location.search).toHaveBeenCalled();
        expect(_loadingState.hide).toHaveBeenCalled();
        expect(viewModel.api.updateWithOptions).not.toHaveBeenCalled();
        expect(scope.$parent.graphObj.updateGraphOptions).toHaveBeenCalled();
    });
    it('should verify redrawGraph callback false', function () {
        var elementString = '<div class="active"><div gs-tab-graphs gs-graph-id="tab-ent-day"></div></div>';
        var element = angular.element(elementString);
        scope.$parent.graphObj={};
        scope.$parent.graphObj.options={chart:{}};
        var viewModel={api:{getScope:function(){return {chart:false}},updateWithOptions:function(){},options:{}}};
        scope.$parent.graphObj.updateGraphOptions = function()
        {
            return;
        };
        scope.graphObj.requestData = function()
        {
            return;
        };
        spyOn(_loadingState,'hide');
        spyOn(viewModel.api,"updateWithOptions");
        spyOn(scope.$parent.graphObj,"updateGraphOptions");
        spyOn(scope.graphObj,"requestData").and.callFake(function(myParam) {
            myParam(viewModel);
        });
        compile(element)(scope);
        scope.$digest();
        //expect(location.search).toHaveBeenCalled();
        expect(_loadingState.hide).toHaveBeenCalled();
        expect(viewModel.api.updateWithOptions).toHaveBeenCalled();
        expect(scope.$parent.graphObj.updateGraphOptions).toHaveBeenCalled();
    });
    it('should verify on menu pin', function () {
        var elementString = '<div class="active"><div gs-tab-graphs gs-graph-id="tab-ent-day"></div></div>';
        var element = angular.element(elementString);
        scope.$parent.graphObj={};
        scope.$parent.graphObj.options={chart:{}};
        var viewModel={api:{getScope:function(){return {chart:false}},updateWithOptions:function(){},options:{}}};
        scope.$parent.graphObj.updateGraphOptions = function()
        {
            return;
        };
        scope.graphObj.requestData = function()
        {
            return;
        };
        spyOn(scope.$parent.graphObj,"updateGraphOptions");
        spyOn(scope.graphObj,"requestData").and.callFake(function(myParam) {
            myParam(viewModel);
        });
        compile(element)(scope);
        scope.$digest();
        $('body').trigger("onMenuPin");
        //expect(location.search).toHaveBeenCalled();
        expect(scope.$parent.graphObj.updateGraphOptions).toHaveBeenCalled();
    });
    it('should verify on menu pin', function () {
        var elementString = '<div class="active"><div gs-tab-graphs gs-graph-id="tab-ent-day"></div></div>';
        var element = angular.element(elementString);
        scope.$parent.graphObj={};
        scope.$parent.graphObj.options={chart:{}};
        var viewModel={api:{getScope:function(){return {chart:false}},updateWithOptions:function(){},options:{}}};
        scope.$parent.graphObj.updateGraphOptions = function()
        {
            return;
        };
        scope.graphObj.requestData = function()
        {
            return;
        };
        spyOn(scope.$parent.graphObj,"updateGraphOptions");
        spyOn(scope.graphObj,"requestData").and.callFake(function(myParam) {
            myParam(viewModel);
        });
        compile(element)(scope);
        scope.$digest();
        $('body').trigger("onMenuPin");
        //expect(location.search).toHaveBeenCalled();
        expect(scope.$parent.graphObj.updateGraphOptions).toHaveBeenCalled();
    });
});