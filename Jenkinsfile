def buildComponent = "gem-frontend"
def buildFileName = "gem.zip"
def buildFolderName = "gem"
def ecrName = "076544065911.dkr.ecr.us-east-1.amazonaws.com"
def ecrProject = "gem-frontend"
def newImage = ''
pipeline {
    agent{ 
        node{
            label 'build01-node'
        }
    }
    options {
	    //Only keep the 30 most recent builds
	    buildDiscarder(logRotator(numToKeepStr:'30'))
	    disableConcurrentBuilds()
    } 
    stages {
        stage ('choose pipeline stages'){
            steps {
                script {
                    env.DEPLOY_STAGE_CHOICE = input message: 'User input required', ok: 'Continue',
                        parameters: [choice(name: 'DEPLOY_STAGE_CHOICE', choices: 'execute\ndo not execute', description: 'Do you want to execute the Deploy stage?')]
                    env.TESTING_STAGE_CHOICE = input message: 'User input required', ok: 'Continue',
                        parameters: [choice(name: 'TESTING_STAGE_CHOICE', choices: 'execute\ndo not execute', description: 'Do you want to execute the Testing stage?')]
                }
                echo "Deploy stage: ${env.DEPLOY_STAGE_CHOICE}"
                echo "Testing stage: ${env.TESTING_STAGE_CHOICE}"
            }
        }
        stage ('initialization & preparation') {
            steps {          	 					
                sh """
                echo "Starting CI for gem frontend"
                """
            }
        }        
        stage('building frontend-gem and upload to nexus') {
            steps {
                echo "Building release ${env.BUILD_ID}"
                sh """
                ./build.sh ${env.BUILD_ID}
                pwd
                ls -ltrh
                """
                script{
                    nexusArtifactUploader artifacts: [[artifactId: 'gem', classifier: '', file: "${buildFileName}", type: 'zip']], credentialsId: 'Nexus-Stage', groupId: 'com.gigsky', nexusUrl: 'nexus.gskystaging.com', nexusVersion: 'nexus3', protocol: 'http', repository: 'frontend-releases', version: "1.0.0-${env.BUILD_ID}"
                }
            }
            post {
                success {
		    archiveArtifacts artifacts:"${buildFileName}"
                    stash includes: "${buildFileName}", name: "${buildComponent}"
                    echo "Build successfully completed. Artifacts archived locally"
	        }
                failure {
                    echo "Build failed. Please check logs"
                }
            }    
        }
        stage ('dockerize gem application and push to ECR') {
            when {
		expression {
                   return env.DEPLOY_STAGE_CHOICE=='execute';
		}
            }
            steps{
                unstash "${buildComponent}"
                script{
                    newImage = "${ecrName}/${ecrProject}:1.0.0-${env.BUILD_ID}"
                }
                sh """
                pwd
                mkdir ${buildFolderName}
                unzip -o ${buildFileName} -d ${buildFolderName}
                aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin ${ecrName}
                docker build -t ${newImage} .
                docker push ${newImage}
                pwd
                rm -r ${buildFolderName}
                ls -lhrt
                """
            }
        }
    }
    post {
        cleanup {
            echo "cleaning the workspace for this branch"
            cleanWs deleteDirs: true
        }
    }
}
